$ticketFilters = null;
$(document).ready(function(){
  
    if(bankFolder!='ncbtrans' && bankFolder!='ncbbranding'){   
      $('#scrollbar_atm').tinyscrollbar({
          sizethumb: 40
      }); 
    }   
    
    $("#admin").trigger('click');
    
    $("#reportTab a").click(function(e){
            "\color{clGreen}click\color{clBlack}(", "Function fn;", ")";
            $(this).tab('show');
    }); 
   
    $('.img_popup').click(function(){
        
        $popupObject.setWidth("800px");
        $popupObject.initialize(this,'#ticketDetailPopup','getTicketSliderPopupDetail');
    });
    $('.arb_good_img_popup').click(function(){

        $popupObject.setWidth("600px");
        $popupObject.initialize(this,'#ticketDetailPopup','getArbGoodImagePopupDetail');
    });
    
    
    $('.goodissues_popup').click(function(){
    
        $popupObject.setWidth("480px");
        $popupObject.initialize(this,'#ticketDetailPopup','getImageFoeGoodQuestion');
          $popupObject.setWidth("800px");
    });
    
    
    $('.slider-image').click(function(){
      
        $popupObject.setWidth("800px");
        $popupObject.initialize(this,'#ticketDetailPopup','getTicketSliderPopupDetail');
        
    });
    
    
     $('.slider-image-ncbatmV5').click(function(){
         $popupObject.setWidth("800px");
         $popupObject.initialize(this,'#ticketDetailPopup','getTicketSliderPopupDetail');
     });
     
     
     $('.slider-image-independent').click(function(){
         $popupObject.setWidth("800px");
         $popupObject.initialize(this,'#ticketDetailPopup','getTicketSliderPopupDetail');
     });
    
   
   
    $('.alertTicketViewLink').click(function(){
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("500px");
        
        $popupObject.initialize(this,'#alertTicketDetailPopup','getAlertTicketPopupDetail');
        
        $popupObject.setWidth("1000px");
        $popupObject.setHeight("650px");
        
    });
    
    
     $(document).delegate(".alertTicketViewLinkAnohter","click",function(e){
         
         $popupObject.setWidth("600px");
         $popupObject.setHeight("500px");
        
         $popupObject.initialize(this,'#alertTicketDetailPopup','getAlertTicketPopupDetail');
        
         $popupObject.setWidth("1000px");
         $popupObject.setHeight("650px");
         
     });
    
  
    $('#healthReportContainer .dept_popup').click(function(){
        $popupObject.setWidth("1100px");
        $popupObject.initialize(this,'#questionDetailPopup','getQuestionTickets');
    
    });
    
    $('.popup-img-drill').click(function(){
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#questionDetailPopup','getQuestionTickets');
    });
    
    
    
    $('.dept_popup_issue').click(function(){
        alret('clicked');
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#questionDetailPopup','getQuestionTickets');
    
    });
    
    
    
    $('#healthReportContainerSmallShop .dept_popup').click(function(){
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#questionDetailPopupSmallShop','getQuestionTicketsSmallShop');
    
    });
    
    $('#kioskIssueContainer').on("click",".dept_popup", function(){
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#kioskDetailPopup','kisokQuestionDetail');
    });
    
    /* TICKET ANALYSIS POPUP */
    $('#marketingMetricsDiv .dept_popup').click(function(){
        $popupObject.setWidth("1100px");
        $popupObject.initialize(this,'#questionDetailPopup','getAnalysisTickets');
    
    });
    
    
    $('#healthReportContainer .transfer_popup').click(function(){
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#questionDetailPopup','getTransferedTickets');
    });
    
    $('#healthReportContainer .transfer_popup_updated').click(function(){
        $popupObject.setWidth("1100px");
        $popupObject.initialize(this,'#questionDetailPopup','getTransferedDepartmentTickets');
    });
    
    
    $('#healthReport_atm .metrics_number').click(function(){ 
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#atmQuestionDetailPopup','getATMQuestionTickets');
        $popupObject.setWidth("800px");
    });
    
    /* MCP Popup For EVP Report*/
    $('#mcpMetricesDiv .metrics_number').click(function(){ 
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#mcpQuestionDetailPopup','getMCPQuestionTickets');
        $popupObject.setWidth("800px"); 
    });
    
   
  if(bankFolder=='riyad' || bankFolder=='rbatm'){    
     $('.indexdialAnother').click(function(){
        $popupObject.setWidth("600px");
        $popupObject.setHeight("470px");
        
        $popupObject.initialize(this,'#scoreDetailPopup','getPreviousScoreDetail');
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("650px");
     });
   }  
  
  $('#regionKnobDivSmallShop').find('#regionScoreHistory').on("click", function(e){
        e.preventDefault();
        $popupObject.setWidth("600px");
        $popupObject.setHeight("470px");
        
        $popupObject.initialize(this,'#regionScoreDetailPopupSmallSop','getPreviousRegionScoreDetailSmallShop');
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("650px");
     });
     
  $('#regionKnobDivSmallShop').find('#regionScoreCompareHistory').on("click", function(e){
        e.preventDefault();
        $popupObject.setWidth("600px");
        $popupObject.setHeight("470px");
        
        $popupObject.initialize(this,'#regionScoreDetailPopupSmallSop','comparePreviousRegionScoreDetailSmallShop');
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("650px");
     });
        
  $('#regionKnobDiv').find('#regionScoreHistory').on("click", function(e){
        e.preventDefault();
        $popupObject.setWidth("600px");
        $popupObject.setHeight("470px");
        
        $popupObject.initialize(this,'#regionScoreDetailPopup','getPreviousRegionScoreDetail');
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("650px");
     });
      
   
   $('#regionKnobDiv').find('#regionScoreCompareHistory').on("click", function(e){
        e.preventDefault();
        $popupObject.setWidth("600px");
        $popupObject.setHeight("470px");
        
        $popupObject.initialize(this,'#regionScoreDetailPopup','comparePreviousRegionScoreDetail');
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("650px");
     });
     
   
   if(bankFolder=='riyad' || bankFolder=='rbatm' || (bankFolder == 'mobily' && bankVersion == '5')){    
     
     $('.departmentScoreDiv').on("click", function(){
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("470px");
        
        $popupObject.initialize(this,'#departmentScoreDetailPopup','getPreviousDepartmentScoreDetail');
        
        $popupObject.setWidth("600px");
        $popupObject.setHeight("650px");
     });
   } 
  
    
    function downloadExcelResponse(){
        alert('report downloaded')
    }
    
    initializeTicketPopup();
    
    $("#indexscoreKnobMe").knob({
        'min':0,
        'max':100
    });
    
    $(".dial").knob({
        'min':0
        ,'max':100
    }); 
    
});

function pieChartClick()
{  
   $popupObject.setWidth("1000px");
   $popupObject.initialize(this,'#questionDetailPopup','getQuestionTicketsForPieChart'); 
}

String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};

String.prototype.ltrim=function(){return this.replace(/^\s+/,'');};

String.prototype.rtrim=function(){return this.replace(/\s+$/,'');};

String.prototype.fulltrim=function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};

function initializeTicketPopup()
{
    $('.ticketViewButton').click(function(){
         $popupObject.setWidth("800px");
         $popupObject.initialize(this,'#ticketDetailPopup', 'getTicketPopupDetail');
    });

      $('.ATMticketViewButton').click(function(){
         $popupObject.setWidth("800px");
         $popupObject.initialize(this,'#ticketDetailPopupATM','getTicketPopupDetailATM');
    });
}

function getQuestionTickets($element)
{
    $postData = {
        'requestFor'     :  'question-ticket',
        'group_name'     :   group_name,
        'survey_id'      :   survey_id,
        'isScoreCard'    :   'true',
        'detailFor'    :   $($element).attr('request-type'),
        //'ticket-type1'   :  $($element).attr('ticket-type'),
        'requestParam'   :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup",
                'ticket-type'   :  $($element).attr('ticket-type')

        }
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}

function getQuestionTicketsSmallShop($element)
{
    $postData = {
        'requestFor'    :  'question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup",
                'ticket-type'   :  $($element).attr('ticket-type')
        }   
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopupSmallShop');
}


/*

function getZoneMapingContent($zoneName,$star)
{
    $postData = {
        'requestFor'    :  'question-group',
        'requestParam'  :   {
                'section'    :  $zoneName,
                'star'       :  $star
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#zone-maping-content');
} 

 */

 
 function kisokQuestionDetail($element){
      $postData = {
        'requestFor'    :  'question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup",
                'outlet-code'   :  $("#kiosk-ouletcode").val()
        }   
    };
    
    $requestObject.initialize($postData,'fillKioskData',null);
 }
 
 
 function fillKioskData($result){
     
     $("#kioskDetailPopup").html($result);
 }
function setQuestionPopupPie($deptName,$ticketColor,$ticketStatus,$requestType){
    $requestParam = {
        'questionId'    : 6,
        'requestType'       :  'question-popup'
    };
    
    $popupObject.setWidth("1000px");
    $popupObject.open('#questionDetailPopup1','getQuestionTicketsForPieChart',$requestParam); 
    $popupObject.setWidth("800px");
}


function getQuestionTicketsForPieChart()
{
 
     $postData = {
        'requestFor'    :  'question-ticket',
         'group_name'   :   group_name,
         'survey_id'    :   survey_id,
        'requestParam'  :   {
                'questionId'    :  6,
                'requestType'   :  "question-popup" 
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup1'); 
}



function getTransferedTickets($element)
{
    $postData = {
        'requestFor'    :  'transfered-ticket',
        'group_name'    :   group_name,
        'survey_id'     :   survey_id,
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup" 
        }   
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}

function getTransferedDepartmentTickets($element)
{ 
    $postData = {
        
        'requestFor'    :  'transfered-ticket-department',
        'group_name'    :   group_name,
        'survey_id'     :   survey_id,

        'requestParam'  :   {
                'departmentId'    :  $($element).attr('department-id'),
                'departmentName'  :  $($element).attr('department-name'),
                'requestType'     :  "question-popup" 
        }
           
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}


function activatePopupFunction($isForVendor)
{
   $('#questionsListPopup .btn-setting').click(function(e){
   e.preventDefault();
   currentGraphToEmail = $(this);
    $('#dashboardEmailIcon').css('display','none');
       if($isForVendor){
           $('#vendorDetailPopupEmailIcon').css('display','inline');
       }else{
           $('#qustionDetailPopupEmailIcon').css('display','inline');
       }
    $('#branchReportMailFunction').css('display','none');
    $('#assetTicketMailFunction').css('display','none');
    $('#ticketListMailFunction').css('display','none');
    $('#evpReportMailFunction').css('display','none');
    $('#branchReportPopupEmailIcon').css('display','none');
    $('#uploadRow').css('display','inline');
    $('#ccRow').css('display','inline'); 
    
    $('#emailSuccessMsg').hide();
    $('#emailSendSuccess').hide();
    $('#graphEmailAddress').val('');
    $('#graphMessage').val('');
    $('#myModal').modal('show');
 });
}



function popupCallBackCustom()
{
     activatePopupFunction();
     $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({
              "placement":"bottom",
              delay: {
                 show: 400, 
                 hide: 200
              }
    }); 
  
}

function popupCallBackCustomVendor()
{
     activatePopupFunction(true);
     $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({
              "placement":"bottom",
              delay: {
                 show: 400,
                 hide: 200
              }
    });

}



function getQuestionTicketsContent($requestParam){

    $ticketFilters  =   $requestParam;
    $postData = {
        'requestFor'    :  'question-ticket',
        'requestParam'  :   $requestParam,
        'group_name'    :   group_name,
        'survey_id'     :   survey_id

    };
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}

function getPendingResolveContent($requestParam){
    
    $ticketFilters  =   $requestParam;
    $postData = {
        'requestFor'    :  'pending-resolve-ticket',
        'requestParam'  :   $requestParam
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
    
}

function getVendorTicketsContent($requestParam){


    $ticketFilters  =   $requestParam;
    $postData = {
        'requestFor'    :  'vendor-ticket',
        'requestParam'  :   $requestParam,
        'group_name'    :   group_name,
        'survey_id'    :   survey_id

    };

    //$.extend(true,$postData,$requestParam);

    $requestObject.initialize($postData,'popupCallBackCustomVendor','#questionDetailPopup');
}




function getATMQuestionTickets($element){
   
    $postData = {
        'requestFor'    :  'atm-question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup",
                'group_name'    :   group_name,
                'survey_id'     :   survey_id
        }
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#atmQuestionDetailPopup');
}

function getMCPQuestionTickets($element){
   
    $postData = {
        'requestFor'    :  'mcp-question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup",
                'group_name'    :   group_name,
                'survey_id'     :   survey_id
        }
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#mcpQuestionDetailPopup');
}

function getPreviousScoreDetail($element)
{
    $postData = {
        'requestFor'    :  'previous-score',
        'requestParam'  :   {
                'currentBranchId' :  $($element).attr('branch-id'),
                'branchCode'      :  $($element).attr('branch-code'),
                'requestType'     :  "score-popup" 
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#scoreDetailPopup');
}

function getPreviousRegionScoreDetail($element)
{
    $postData = {
        'requestFor'    :  'region-previous-score',
        'requestParam'  :   {
                'zoneName' :  $($element).attr('data-text'),
                'requestType'     :  "score-popup" 
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#regionScoreDetailPopup');
}

function comparePreviousRegionScoreDetail($element)
{
    $postData = {
        'requestFor'    :  'region-previous-score',
        'requestParam'  :   {
                'actionName' :  $($element).attr('data-text'),
                'requestType'     :  "score-popup" 
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#regionScoreDetailPopup');
}


function getPreviousRegionScoreDetailSmallShop($element)
{
    $postData = {
        'requestFor'    :  'region-previous-score',
        'requestParam'  :   {
                'zoneName' :  $($element).attr('data-text'),
                'requestType'     :  "score-popup",
                'shopType'     :  "MFBO" 
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#regionScoreDetailPopupSmallSop');
}

function comparePreviousRegionScoreDetailSmallShop($element)
{
    $postData = {
        'requestFor'    :  'region-previous-score',
        'requestParam'  :   {
                'actionName' :  $($element).attr('data-text'),
                'requestType'     :  "score-popup",
                'shopType'     :  "MFBO" 
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#regionScoreDetailPopupSmallSop');
}


function getPreviousDepartmentScoreDetail($element)
{
   $postData = {
        'requestFor'    :  'previous-department-score',
        'requestParam'  :   {
                'departmentId'      :  $($element).attr('department-id'),
                'departmentName'    :  $($element).attr('department-name'),
                'requestType'       :  "department-score-popup" 
        }   
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#departmentScoreDetailPopup'); 
}

function gotoFilterPageNumber(element){

    ticketQuestionFilterData.destroy();
    getTicketQuestionFilter();
    getCurrentPageNumber();
    
    ticketQuestionFilterData.set('pageNumber',Number($(element).val()));
    ticketQuestionFilterData.set('departmentName',$ticketFilters.departmentName);
    ticketQuestionFilterData.set('ticketColor',$ticketFilters.ticketColor);
    ticketQuestionFilterData.set('ticketStatus',$ticketFilters.ticketStatus);
    ticketQuestionFilterData.set('requestType',$ticketFilters.requestType);
    ticketQuestionFilterData.set('ticketAge',$("#ticketQuestionList").attr('ticket-age'));
    
   ticketQuestionFilterData.set('regionName',$ticketFilters.regionName);
    
    /* new filter values for sorting order */
    $orderFields = ticketQuestionFilterData.getOrderType();
    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    /* new filter values for sorting order */
    
    ticketQuestionFilterData.set('branchCode',$branchCode);
    ticketQuestionFilterData.set('branchType',$branchType);
  
    $requestData = {
        "requestFor"     :  "ticket-question-pagination",
        "ticket-filters" :  ticketQuestionFilterData.get()   
      };
    $requestObject.initialize($requestData,"fillTicketQuestionFilterResult",null);
}

function gotoTicketAnalysisFilterPageNumber(element){

    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    $requestData = {
        'requestFor'     :  'ticket-question-pagination',
        'ticket-filters' : {        
                'ticket-current-status' :  $("#ticketQuestionList").attr('ticket-current-status'),
                'requestType'           :  $("#ticketQuestionList").attr('ticket-current-status')+' Tickets',
                'ticket-time'           :  $("#ticketQuestionList").attr('ticket-time'),
                'department-id'         :  $("#ticketQuestionList").attr('department-id'),
                'pageNumber'            :  Number($(element).val())
         }
      };
    $requestObject.initialize($requestData,"fillTicketQuestionFilterResult",null);
}

function setQuestionPopup($deptName,$ticketColor,$ticketStatus,$requestType,$regionName){
    $requestParam = {
        'departmentName'    :  $deptName,
        'ticketColor'       :  $ticketColor,
        'ticketStatus'      :  $ticketStatus,
        'requestType'       :  $requestType,
        'branchCode'        :   $branchCode,
        'branchType'        :   $branchType,
        'regionName'        :   $regionName
    };
    $popupObject.setWidth("1100px");
    $popupObject.open('#questionDetailPopup','getQuestionTicketsContent',$requestParam);
    $popupObject.setWidth("800px");
}


/**
 * @description To get records form Drop Down pagination
 * @param dropdown object , request type
 * @return filtered html content
 */

    function getPageNumberUghi(page, requestTicketType, container){
            console.log(requestTicketType);
           var url = '<?php echo $this->getUrl()."/".$this->getBankFolder();?>/request.php';
           
            pageNumber = $("#getPageNumber"+container).val();
            
            if(pageNumber == 1){
               $("#switchPageNumberPrevButton"+container).hide();
               $("#switchPageNumberNextButton"+container).show();  
            }else{
               $("#switchPageNumberPrevButton"+container).show(); 
            }
            
            if(pageNumber == $('#getPageNumber'+container+' option:last-child').val()){
               $("#switchPageNumberNextButton"+container).hide(); 
            }else{
               $("#switchPageNumberNextButton"+container).show(); 
            }
            
            
            result = $("#ticketLoder").html();
            $("#"+container).html(result);
           var requestData = {
               'requestFor' : requestTicketType,
               'pageNumber' : $(page).val(),
               'is-drill-page'    : true,
               'container-id' : container
           };
           
            /* Pagination param carrying logic*/
           if($("#urlParmToForward_"+container)){
                var urlParams = ($("#urlParmToForward_"+container).val());
                if(urlParams){
                    urlParams = JSON.parse(urlParams);
                    requestData = $.extend(requestData, urlParams);
                }
           }
           
           $.post('request.php', requestData, function(result){
                
                        $("#"+container).html(result);
                //console.log(requestTicketType);
               
           });
    }
    

function drillPendingVsResolve($filter,$category,$container,$status){
    console.log("hi" + $container);
    $ticketFilterData = getFilterData($filter);
  
        requestData = {
            'requestFor'    : 'pending-resolve-ticket-pagination',
            'facility'      :  $status,
            'container'     :  $container,
            'status'        :  $category,
            'filters'       :  $ticketFilterData,
            'container-id'  :  $container,
            'requestType'   :  'pending-resolve-ticket'
        };

        $.post('request.php', requestData, function(result){
            
            requestData1 = {
               'requestFor'       : 'pending-resolve-ticket',
               'filter-data'      : $ticketFilterData,
               'is-drill'         : true,
               'drill-pagination' : result,
               'facility'         :  $status,
               'status'           :  $category,
               'container'        : $container,
               'container-id'     : $container
            };
        
        $popupObject.setWidth("1000px");
        $popupObject.setHeight("650px");
        $popupObject.open('#ticketDetailPopup','fillNcbPopup',requestData1);
        $popupObject.setWidth("800px");                             
        });
}

function fillNcbPopup($requestParam){
      $requestObject.initialize($requestParam, null, '#ticketDetailPopup');
 }

function getQuestionTicket($requestParam){
    
    $ticketFilters  =   $requestParam;
        $postData = {
        'requestFor'     :  'indexPageMonthlyScorePopUp',
        'ticket'         :  $ticketFilters
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}


function setQuestionPopupTry($deptName,$branchCode,$floor){
    if($floor === undefined || $floor === "" || $floor === null)
        $floor = "";
    $requestParam = {
        'departmentName'    :  $deptName,
        'branchCode'        :  $branchCode,
        'floor'             :  $floor
    };
    if(bankFolder == "mobily"  && bankVersion == "5"){
    $popupObject.setWidth("900px");
    $popupObject.open('#questionDetailPopup','getQuestionTicket',$requestParam);
    $popupObject.setWidth("800px");
    }
}


/**
 * @description To get records form Next and Previous Button pagination
 * @param button type (next, previous)) , request type
 * @return filtered html content
 */    
    
    function switchPageNumberUghi(buttonType, requestTicketType, container){
           console.log(requestTicketType + " :: " + container);
           var url = '<?php echo $this->getUrl()."/".$this->getBankFolder();?>/request.php';
            
            if(buttonType == 'next'){
                 pageNumber = $("#getPageNumber"+container).val();
                 pageNumber++;
               $("#switchPageNumberPrevButton"+container).show(); 
            }else{
                
                 pageNumber = $("#getPageNumber"+container).val();
                 pageNumber--;
            } 
            $("#getPageNumber"+container).val(pageNumber);
            
            if(pageNumber == 1){
               $("#switchPageNumberPrevButton"+container).hide();
               $("#switchPageNumberNextButton"+container).show();  
            }
            
            
            if(pageNumber == $('#getPageNumber'+container+' option:last-child').val()){
               $("#switchPageNumberNextButton"+container).hide(); 
            }else{
               $("#switchPageNumberNextButton"+container).show(); 
            }
            //console.log(pageNumber);       
            /*
            if(requestTicketType == 'compliance-tickets'){
                    result = $("#ticketLoder").html();
                    //console.log(result);
                    $("#compliance_ticket_results").html(result);
                    var container = 'compliance_ticket_results';
            }else{
                result = $("#ticketLoder").html();
                $("#stock_ticket_results").html(result);
                var container = 'stock_ticket_results';
            }
            */
            result = $("#ticketLoder").html();
            $("#"+container).html(result);     
           var requestData = {
               'requestFor' : requestTicketType,
               'pageNumber' : pageNumber,
               'is-drill-page'    : true,
               'container-id' : container
                
           };
           
            /* Pagination param carrying logic*/
           if($("#urlParmToForward_"+container)){
                var urlParams = ($("#urlParmToForward_"+container).val());
                if(urlParams){
                    urlParams = JSON.parse(urlParams);
                    requestData = $.extend(requestData, urlParams);
                }
           }
           //$requestObject.initialize(requestData,null,$containerId);
           $.post('request.php', requestData, function(result){
                
                    $("#"+container).html(result);
                    
                //console.log(requestTicketType);
               
           });
    }

function setAgeGraphQuestionPopup($ticketAge,$requestFor,$requestType){
if(bankFolder){   
    $requestParam = {
        'ticket-age'     :  $ticketAge,
        'requestType'    :  $requestType
    };
    $popupObject.setWidth("1100px");
    $popupObject.open('#questionDetailPopup','getQuestionTicketsContent',$requestParam);
    $popupObject.setWidth("800px");
  }  
}

function setVendorTicketPopup($vendorId,$vendorName,$vendorTitle){
if(bankFolder){
    $requestParam = {
        'vendorId'     :  $vendorId,
        'vendorName'   :  $vendorName,
        'vendorTitle'  :  $vendorTitle,
    };
    //console.log($requestParam);
    $popupObject.setWidth("1100px");
    $popupObject.open('#questionDetailPopup','getVendorTicketsContent',$requestParam);
    $popupObject.setWidth("800px");
  }
}


function setATMQuestionPopup($deptName,$ticketColor,$ticketStatus,$requestType){
    
    $requestParam = {
        'departmentName'    :  $deptName,
        'ticketColor'       :  $ticketColor,
        'ticketStatus'      :  $ticketStatus,
        'requestType'       :  $requestType,
        'branchCode'        :   $branchCode,
        'branchType'        :   $branchType
    };
    $popupObject.setWidth("1000px");
    $popupObject.open('#questionDetailPopup','getATMQuestionTicketsContent',$requestParam);
    $popupObject.setWidth("800px");
}

function getATMQuestionTicketsContent($requestParam){
    $ticketFilters  =   $requestParam;
    $postData = {
        'requestFor'    :  'atm-question-ticket',
        'requestParam'  :   $requestParam,
        'group_name'     :   group_name,
        'survey_id'     :   survey_id
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}


function getNextTicket($filterType, $element){
    findTicketFilter($filterType);
    console.log("here");
    $ticketFilterData = ticketFilterData.get();
    
    $requestData = {
        "requestFor"     :   "next-ticket",
        "group_name"     :   $("#group_name").val(),
        "ticket-filters" :   ticketFilterData.get(),
        "other-filters"  :   $element
    };
    
    $("#main-ticket-slider").html($("#poploader").html());
    $requestObject.initialize($requestData, "getTicketNextPopupDetail", null);
}

function getTicketNextPopupDetail($element){
    $element = JSON.parse($element);
  
    console.log($element);
  
    $ticketPanelPageNumber  =   0;
    $ticketPanelPageValue   =   0;
    $databaseName = '';
    if(bankFolder == 'ncbatm' || bankFolder == 'desktopapp' || bankFolder == 'ncbindependent')
        $databaseName = $("#database_name").val();
    
    if($("#gotoPageNumber")){
        $ticketPanelPageNumber =  $("#gotoPageNumber option:selected").text();
        $ticketPanelPageValue  =  $("#gotoPageNumber").val();
    }
    $("#ticketDetailPopupFirst").html($("#popupLoader").html());
    $("#ticketDetailPopup").html($("#popupLoader").html());
    $postData = {
        'requestFor'     :   'ticket-detail',
        'ticketNumber'   :   $element['ticket-number'],
        'process'        :   "first",
        'controllerName' :   $currentPage,
        'group_name'     :   $element['group-name'],
        'db-name'        :   $databaseName,
        'ticket-at'      :   $element['ticket-at'],
        'ticket-page'    :   ($("#gotoPageNumber").length) ? $("#gotoPageNumber").val() : 0,
        'ticket-total'   :   $element['ticket-total']
    };
    
    $requestObject.initialize($postData, "initializePopupTab", '#ticketDetailPopup');
}

function getTicketPopupDetail($element){
    console.log($element);
    console.log(typeof $element);
    $ticketPanelPageNumber  =   0;
    $ticketPanelPageValue   =   0;
    $databaseName = '';
    $ticketStatus = 1;
    if(bankFolder == 'ncbatm' || bankFolder == 'desktopapp' || bankFolder == 'ncbindependent')
        $databaseName = $("#database_name").val();
    if(bankFolder == 'hrdf') {
        $ticketStatus = (($($element).attr('ticket-status')) && ($($element).attr('ticket-status') == 'close'))? 0 : 1;
    }
    if($("#gotoPageNumber")){
        $ticketPanelPageNumber =  $("#gotoPageNumber option:selected").text();
        $ticketPanelPageValue  =  $("#gotoPageNumber").val();
    }
    $("#ticketDetailPopupFirst").html($("#popupLoader").html());
    $("#ticketDetailPopup").html($("#popupLoader").html());


    $showActionOption = $($element).attr('action-button');
    $showActionOption =  ( typeof($showActionOption) == 'undefined' ) ? true : $showActionOption,

    $postData = {
        'requestFor'     :   'ticket-detail',
        'ticketNumber'   :   $($element).attr('ticket-number'),
        'process'        :   "first",
        'controllerName' :   $currentPage,
        'group_name'     :   (($($element).attr('ticket-number')) ? $($element).attr('group-name'): ''),
        'db-name'        :   $databaseName,
        'ticket-at'      :   $($element).attr('ticket-at'),
        'ticket-page'    :   ($("#gotoPageNumber").length) ? $("#gotoPageNumber").val() : 0,
        'ticket-total'   :   $(".ticket-counter").html(),
        'ticket-status'  :   $ticketStatus,
        'show-action'    :   $showActionOption
    };
    
    $requestObject.initialize($postData,"initializePopupTab",'#ticketDetailPopup');
}

function getTicketSliderPopupDetail($element){
   
    $ticketPanelPageNumber  =   0;
    $ticketPanelPageValue   =   0;
    
    if($("#gotoPageNumber")){
        $ticketPanelPageNumber =  $("#gotoPageNumber option:selected").text();
        $ticketPanelPageValue  =  $("#gotoPageNumber").val();
    }
    
     if(bankFolder == 'hrdf') {
        $ticketStatus = (($($element).attr('ticket-status')) && ($($element).attr('ticket-status') == 'close'))? 0 : 1;
    }else{
        $ticketStatus = ''
    }
    
    $("#ticketDetailPopupFirst").html($("#popupLoader").html());
    $("#ticketDetailPopup").html($("#popupLoader").html());
    $postData = {
        'requestFor'     :   'ticket-detail',
        'ticketNumber'   :   $($element).attr('ticket-number'),
        'process'        :   "first",
        'controllerName' :   $currentPage,
        'dbName'         :   $($element).attr('gallery-slider-db'),
        'group_name'     :   ($($element).attr('ticket-number')) ? $($element).attr('group-name'): '',
        'ticket-status'  :   $ticketStatus,
        'show-action'   :   true,
        'branchId'       :  (($branchId !== undefined)? $branchId : null)
    };
    
    $requestObject.initialize($postData,"initializePopupTab",'#ticketDetailPopup');
}
function getArbGoodImagePopupDetail($element){

    $("#ticketDetailPopup").html($("#popupLoader").html());
    $postData = {
        'requestFor'     :   'arb-good-image',
        'branch_id'      :   $($element).attr('branch_id'),
        'question_id'    :   $($element).attr('question_id'),
        'answer_text'    :   $($element).attr('answer_text'),
        'image_for'      :   $($element).attr('image_for')
    };

    $requestObject.initialize($postData,null,'#ticketDetailPopup');
}

function getImageFoeGoodQuestion($element){
    $ticketPanelPageNumber  =   0;
    $ticketPanelPageValue   =   0;
    
    if($("#gotoPageNumber")){
        $ticketPanelPageNumber =  $("#gotoPageNumber option:selected").text();
        $ticketPanelPageValue  =  $("#gotoPageNumber").val();
    }
    $("#ticketDetailPopupFirst").html($("#popupLoader").html());
    $("#ticketDetailPopup").html($("#popupLoader").html());
    $postData = {
        'requestFor'     :   'mobily-good-image-detail',
        'imagepath'      :   $($element).attr('image-refrence'),
        'process'        :   "first",
        'controllerName' :   $currentPage
    };
    $requestObject.initialize($postData,"initializePopupTab",'#ticketDetailPopup');
}



function getAlertTicketPopupDetail($element)
{
    $("#alertTicketpopupLoader").html($("#alertTicketpopupLoader").html());
    $postData = {
        'requestFor'     :   'alert-ticket-detail',
        'alertticketNumber' : $($element).attr('alert_ticket_number'),
        'gender'         :   $($element).attr('centergender'),
        'center_code'    :   $($element).attr('center_code'),
        'status'         :   $($element).attr('status'),
        'gap-count'      :   $($element).attr('gap-count'),
        'base-count'     :   $($element).attr('base-count'),
        'controllerName' :   $currentPage
    };
    
    $requestObject.initialize($postData,"initializePopupTab",'#alertTicketDetailPopup'); 
}


function getTicketPopupDetailATM($element){
    $ticketPanelPageNumber  =   0;
    $ticketPanelPageValue   =   0;
    if($("#gotoPageNumber")){
        $ticketPanelPageNumber =  $("#gotoPageNumber option:selected").text();
        $ticketPanelPageValue  =  $("#gotoPageNumber").val();
    }
    $("#ticketDetailPopupATM").html($("#popupLoader").html());
    $postData = {
        'requestFor'    :   'ticket-detail-atm',
        'ticketNumber'  :   $($element).attr('ticket-number'),
        'process'       :   "first",
        'group_name'    :   group_name,
        'survey_id'     :   survey_id
    };
    $requestObject.initialize($postData,"initializePopupTab",'#ticketDetailPopupATM');
}


function getTicketPopupContent($ticketNumber){
     $("#ticketDetailPopup").html($("#popupLoader").html());
     $("#ticketDetailPopupFirst").html($("#popupLoader").html());
    $postData = {
        'requestFor'    :  'ticket-detail',
        'ticketNumber'  :  $ticketNumber,
        'group_name'    : group_name,
        'survey_id'     : survey_id
    };
    $requestObject.initialize($postData,"clearAndFillPopup",null);
}

function clearAndFillPopup($content){
    $("#ticketDetailPopup").html($content);
    $("#ticketDetailPopupFirst").html($content);
    initializePopupTab();
}

function tabSwitchCreator($tabElement){
    $($tabElement).click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
}

function initializePopupTab() {
    $("#popupActionTab li a").click(function (e) {
        $("#ticketDetailPopupATM").html("");
        e.preventDefault();
        $(this).tab('show');
        if($(this).attr('href') == '#ticket_comment_containor'){
            initializeCommentScrollbar();
        }
    });

     $("#popupActionTabATM li a").click(function (e) {
        $("#ticketDetailPopup").html("");
        e.preventDefault();
        $(this).tab('show');
        if($(this).attr('href') == '#ticket_comment_containor'){
            initializeCommentScrollbar();
        }
    });
 
    
    /* var previous=0;
     $("#dummy123").on("click",function (e){ 
         var s = "dummy123"; 
         $('#'+s).css({'position':'absolute'});
         $('#'+s).css({'cursor':'zoom-out'});
         $('#'+s).css({'z-index':'99999'});
         $('#'+s).css({'border-radious':'25px'});
         $('#'+s).animate({
            height:565,
            width: 540
        }, "medium");
      
        if($('#'+previous).width()!=341){
         $('#'+s).css({'position':'relative'});
         $('#'+previous).animate({'width':'341px'});
         $('#'+s).animate({'height':'500px'}); 
         $('#'+previous).css({'cursor':'zoom-in'});
         $('#'+s).css({'z-index':'1'});
         $('#'+s).css({'border-radious':'0px'});
     
        }
        previous=s;
     }); */
     

}

function toTitleCase(str) { 
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function branchProfileDisplayName($branchName, $region, $city){
    var $branchName = $branchName.toLowerCase();
    var $region = $region.toLowerCase();
    var $city = $city.toLowerCase();

    var $branchProfileDisplayName = $branchName;

    if (!($branchProfileDisplayName.indexOf($region) >= 0)) {
        $branchProfileDisplayName += ', ' + $region;
    }

    if (!($branchProfileDisplayName.indexOf($city) >= 0)) {
        $branchProfileDisplayName += ', ' + $city;
    }

    return toTitleCase($branchProfileDisplayName);
}


function branchTypeToDisplay(branchType){
    if(branchType === 'gents'){
        return 'Men';
    } else if(branchType === 'gents and ladies'){
        return 'Men and Ladies';
    }
    return branchType;
}

function sendToUrl($link){
    alert($link);
}

/**
 * function::getAreaByZone()
 * 
 * @param string thisObj
 * @return lsit of area wise based on zone id
 */
function getAreaByZone(thisObj){
     $isMCP     = $("#hasMCP").val();
     $zone_id   = $(thisObj).attr('value');
     $zone_type =  $(thisObj).find('option:selected').attr('data-text');
     if($isMCP == 'true')
         $id = "#area_name_select_mcp";
     else
        $id = "#area_name_select"; 
     
     if($zone_id == '') {
            $('#area_name_select').html('<option value="">-- ' + getArabicText('Select') + ' ' + getArabicText('Area Name') + ' --</option>');
        } else {
           $('#area_name_select').html('<option value="">-- ' + getArabicText('Loading_Data') + ' --</option>'); 
           $('#area_select_loader').show();
           $requestParam = {
                'zone_id'  : $zone_id,
                'zone_type': $zone_type
                
           };
           $postData = {
                'requestFor'    :  'get-area-by-zone',
                'requestParam'  :   $requestParam
           };
          $requestObject.initialize($postData,null, $id);
          //$('#area_select_loader').show();
          setTimeout(function(){$('#area_select_loader').hide();},1000);
     }
}


function getAreaByZoneId(thisObj){
     $zone_id = $(thisObj).attr('value');
     if($zone_id == '') {
            $('#selected_area').html('<option value="">-- ' + getArabicText('Select') + ' ' + getArabicText('Area Name') + ' --</option>');
        } else {
           $('#selected_area').html('<option value="">-- ' + getArabicText('Loading_Data') + ' --</option>'); 
           $('#selected_area_loader').show();
           $requestParam = {
                'zone_id': $zone_id
                
           };
           $postData = {
                'requestFor'    :  'get-area-by-zone',
                'requestParam'  :   $requestParam
           };
          $requestObject.initialize($postData,null,'#selected_area');
          //$('#area_select_loader').show();
          setTimeout(function(){$('#selected_area_loader').hide();},1000);
     }
}

 function getFilteredAreaResult(thisObj, dropdown) {
    // console.log(thisObj);
    
        $isMCP     = $("#hasMCP").val();
        if($isMCP == 'true'){
            $zone_id   = $('#zone_name_select_mcp').val();
            $area_id   = $('#area_name_select_mcp').val();
            //$area_id   = $('#area_name_select').val();
            $area_name = $("#area_name_select_mcp :selected").text();
             $city_name = $('#mobilyMcpCity').val();
              $fromDate = $('#mcpFrom').val();
            $toDate = $('#mcpTo').val();
            $department_name = "";
        } else {
            
            $zone_id   = $('#zone_name_select').val();
            $area_id   = $('#area_name_select').val();
            $area_name = $("#area_name_select :selected").text();
            $city_name = $('#mobilyZoneCity').val();
            $department_name = $('#ticketDepartment').val();
            $fromDate = $('#zoneReportFrom').val();
            $toDate = $('#zoneReportTo').val();
       
        }
        
        if($isMCP == 'true'){
            $zone_name = $("#zone_name_select_mcp :selected").text();
            
        } else{
            $zone_name = $("#zone_name_select :selected").text();
            
        }
        $shopType = '';
        $outletType = '';
        if($isMCP == 'true'){
            $shopType = $("#mcpReportShopTypeSelect").val();
            $outletType = $("#mcpReportOutletTypeSelect").val();
        }
        
        else if($("#region_report_outlet_type").val()){
            
            $outletType = $("#region_report_outlet_type").val();
        }

        if(dropdown == 'true') {
            $page = $(thisObj).val();
        } else {
           
            
            $page      = $(thisObj).attr('value');
            
        }
        
        $orderBy   = $(thisObj).attr('orderBy');
        $order     = $(thisObj).attr('order');
        
        $('#area_filter_result').html('<div style="margin-left:160px;padding: 20px 0px; text-align: center" >'+
            '<b>'+ getArabicText('Instant Intelligence') + '</b> <br/><img title="'+ getArabicText('Loading Data') + '" src="../public/img/ajax-loaders/bsfmap-loader1.gif">'+
            '</div>'
        );
        
        $('#mcp_content_div').html('<div style="margin-left:160px; padding: 20px 0px; text-align: center">'+
            '<b>'+ getArabicText('Instant Intelligence') + '</b> <br/><img title="'+ getArabicText('Loading Data') + '" src="../public/img/ajax-loaders/bsfmap-loader1.gif">'+
            '</div>'
        );
        
        if($zone_id == '') {
            $('#area_name_mail_report_button').attr('area-name', '');   
            $('#area_name_mail_report_button').attr('zone-name', '');   
            $('#area_name_mail_report_button').attr('area-id', '');   
            $('#area_name_mail_report_button').attr('zone-id', '');   
            $('#area_name_mail_report_button').attr('graph-title', '');  
        } else {
            if($area_id == '') {
                $('#area_name_mail_report_button').attr('area-name', '');   
                $('#area_name_mail_report_button').attr('area-id', '');   
                $('#area_name_mail_report_button').attr('graph-title', 'Branch Score for '+$zone_name+ ' zone');  
                
            } else {
                $('#area_name_mail_report_button').attr('area-name', $area_name);   
                $('#area_name_mail_report_button').attr('area-id', $area_id);
                $('#area_name_mail_report_button').attr('graph-title', 'Branch Score for '+$area_name+ ' area');  
            }
            
            $('#area_name_mail_report_button').attr('zone-name', $zone_name);   
            $('#area_name_mail_report_button').attr('zone-id', $zone_id);   
           
            $('#area_name_pdf_report_button').attr('href', '../Pdf/create-area-pdf.php?bank_name=riyad&area_name='+$area_name+'&zone_name='+$zone_name+'&area_id='+$area_id+'&zone_id='+$zone_id);  
        }
        
         $requestParam = {
                'area_id'     : $area_id,
                'page'        : $page,
                'orderBy'     : $orderBy,
                'order'       : $order,
                'zone_id'     : $zone_id,
                'area_name'   : $area_name,
                'zone_name'   : $zone_name,
                'outlet_type' : $outletType,
                'isMCP'       : $isMCP,
                'shopType'    : $shopType,
               'city_name'    : $city_name,
             'department_name':$department_name,
                    'fromDate': $fromDate,
                      'toDate': $toDate
           };
//    console.log("------------------------------");
//     console.log($requestParam);
         $postData = {
                'requestFor'    :  'get-area-wise-result',
                'requestParam'  :   $requestParam
          };
         $requestObject.initialize($postData,'initializeMCPPopup','#area_filter_result');
  }
  
  
  
  
  
  
  function getFilteredAreaResultBranches(){
     
   $("#branch_filter_result").html($("#branchesReportLoader").html());
        $area_id   = $('#area_name_select_branches').val();
        $area_name = $("#area_name_select_branches :selected").text();
        $city =$('#mobilyBranchCity').val();
        $outletType =$('#outletTypeSelector').val();
        $toDate =$('#branchFrom').val();
        $fromDate =$('#branchTo').val();
        
        
         $requestParam = {
                'area_id'  : $area_id,
                'area_name': $area_name,
                'city'     :$city,
                'outletType':$outletType,
                'toDate':$toDate,
                'fromDate':$fromDate
             
           };
        
         $postData = {
                'requestFor'    :  'get-area-wise-result-branches',
                'requestParam'  :   $requestParam
          };
         $requestObject.initialize($postData,null,'#branch_filter_result');
  }


 function initializeMCPPopup(){
     /*MCP POpup For Area Wise Report */
     $('#mcp_content_div .metrics_number').click(function(){ 
          $popupObject.setWidth("1100px");
          $popupObject.initialize(this,'#mcpQuestionDetailPopupAreaWise','getAreaMCPQuestionTickets');
          $popupObject.setWidth("800px");  
    }); 
 }
 
 function getAreaMCPQuestionTickets($element){
   
     $outeltType = "";
     if(baseBank == "mobily_v5"){
        $outeltType = ($("#mcpReportOutletTypeSelect").val()) ? $("#mcpReportOutletTypeSelect").val() : "";
    }
    $postData = {
        'requestFor'    :  'mcp-question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'branchCode'    :  $($element).attr('branch-code'),
                'requestType'   :  "question-popup",
                'zone-name'     :  $($element).attr('zone-name'),
                'zone-id'       :  $($element).attr('zone-id'),
                'area-name'     :  $($element).attr('area-name'),
                'area-id'       :  $($element).attr('area-id'),
                'from-date'     :  $($element).attr('fromDate'),
                'to-date'       :  $($element).attr('toDate'),
                'outlet-type'   :  $outeltType,
                'isMCP'         :  "true"
        }   
    };
    $requestObject.initialize($postData,'popupCallBackCustom','#mcpQuestionDetailPopupAreaWise'); 
 }
  
 function getAreaByZone123(thisObj){
     $zone_id = $(thisObj).attr('value');
     if($zone_id == '') {
            $('#area_name_select').html('<option value="">-- ' + getArabicText('Select') + ' ' + getArabicText('Area Name') + ' --</option>');
        } else {
           $('#area_name_select').html('<option value="">-- ' + getArabicText('Loading_Data') + ' --</option>'); 
           $('#area_select_loader').show();
           $requestParam = {
                'zone_id': $zone_id
                
           };
           $postData = {
                'requestFor'    :  'get-area-by-zone',
                'requestParam'  :   $requestParam
           };
          $requestObject.initialize($postData,null,'#area_name_select');
          //$('#area_select_loader').show();
          setTimeout(function(){$('#area_select_loader').hide();},1000);
     }
}

 function getAreaWiseScore($month,$month_name,$bankName, $year){
           $requestParam = {
                'month': $month,
                'year' : $year
           };
          
           $postData = {
                'requestFor'    :  'get-area-wise-score',
                'requestParam'  :   $requestParam
           };
           
           $requestObject.initialize($postData,"areaWiseScoreGraph",null);
 }
 
 
  function getRegionWiseScore($year){
   
    $requestParam = {
        'year' : $year
    };
    
    $postData = {
        'requestFor'    :  'get-region-wise-score',
        'requestParam'  :   $requestParam
    };
    
    $requestObject.initialize($postData,"RegionWiseHistoryGraph",null);
 }
 
 function getBranchScoreForBranchProfilePage($year, $branchCode, $branchType,$month,$floor)
 {
   
    $requestParam = {
        'year'        : $year,
        'branch_code' : $branchCode,
        'branch_type' : $branchType,
        'month'       : $month,
        'floor'       : $floor
    };
    
    $postData = {
        'requestFor'    :  'get-branch-score-for-branch-profile-page',
        'group_name'    :  group_name,
        'survey_id'    :  survey_id,
        'requestParam'  :  $requestParam
    };
    $requestObject.initialize($postData,"BranchScoreGraphForBranchProfilePage",null);
    
 }
 
 function getMetricesHtml($deptValueName,groupName,surveyId){
     
     if(typeof surveyId === undefined){
         surveyId = survey_id;
     }
     if(typeof groupName === undefined){
         groupName=group_name;
     }
     $requestParam = {
                'deptName': $deptValueName
     };
     $outletType = '';
     if(baseBank == "ncbatm_v7"){
         console.log("getMetricesHtml");
         $outletType = ($("#database_name").val()) ? $("#database_name").val() : '';
     }
     if(baseBank == 'mobily')
         $outletType = $("#typeofOutlet").val();
     
     $postDeptData = {
                'requestFor'    :  'get-department-wise-metrices',
                'group_name'    :   groupName,
                'survey_id'     :   surveyId,
                'outletType'    :   $outletType,
                'requestParam'  :   $requestParam
      };
     if(baseBank == 'mobily'){
         if($outletType == ''){
             $("#metrices_select_loader").hide();
             alert("Please select Outlet Type");
         }else{
              $requestObject.initialize($postDeptData,"getDepartmentHTMLMetrices",null);
         }
     }else{
          $requestObject.initialize($postDeptData,"getDepartmentHTMLMetrices",null);
     }
    
 }
 
 function getDepartmentHtml(branchValue,groupName,surveyId){
    if(typeof surveyId === undefined){
         surveyId = survey_id;
     }
     if(typeof groupName === undefined){
         groupName=group_name;
     }
     $requestParam = {
                'branchName': branchValue
     };
     $outletType = '';
     if(baseBank == "ncbatm_v7"){
         console.log("getDepartmentHtml");
         $outletType = ($("#database_name").val()) ? $("#database_name").val() : '';
     }
          $postDeptData = {
                'requestFor'    :  'get-branch-wise-department',
                'group_name'    :   groupName,
                'survey_id'    :   surveyId,
                'outletType'    :   $outletType,
                'requestParam'  :   $requestParam
      };
     $requestObject.initialize($postDeptData,"getBranchHtmlDepartment",null);
 }

function getDepartmentHTMLMetrices($content){
   var content = $.parseJSON($content);
   $('#metrices_select_loader').css('display','none');
   $('#metrices_select_loader_arabic').css('display','none');
   $('#metrices_select_loader').hide();
   $('#selectMetricesDepartment').html(content);
   $('#selectmetrices').html(content);  
} 
function getBranchHtmlDepartment($content){
   $('#metrices_select_loader').css('display','none');
   $('#metrices_select_loader_arabic').css('display','none');
   $('#metrices_select_loader').hide();
   $('.ticketDepartment').html($content);
     
    
}


function getMetricesHtmlPending($deptValueName,groupName,surveyId){
     if(typeof surveyId === undefined){
         surveyId = survey_id;
     }
    if(typeof groupName === undefined){
         groupName=group_name;
     }
     $requestParam = {
                'deptName': $deptValueName
     };
     $outletType = '';
    if(baseBank == "ncbatm_v7"){
        $outletType = ($("#database_name").val()) ? $("#database_name").val() : '';
    }
     if(baseBank == 'mobily')
         $outletType = $("#typeofOutlet").val();
     
     $postDeptData = {
                'requestFor'    :  'get-department-wise-metrices',
                'group_name'    :   groupName,
                'survey_id'     :   survey_id,
                'outletType'    :   $outletType,
                'requestParam'  :   $requestParam
      };
     if(baseBank == 'mobily'){
         if($outletType == ''){
             $("#metrices_select_loader").hide();
             alert("Please select Outlet Type");
         }else{
              $requestObject.initialize($postDeptData,"getDepartmentHTMLMetricesPending",null);
         }
     }else{
          $requestObject.initialize($postDeptData,"getDepartmentHTMLMetricesPending",null);
     }
    
 }

 function getMetricesHtmlRejected($deptValueName,groupName,surveyId){
     if(typeof surveyId === undefined){
         surveyId = survey_id;
     }
    if(typeof groupName === undefined){
         groupName=group_name;
     }
     $requestParam = {
                'deptName': $deptValueName
     };
     $outletType = '';
     if(baseBank == 'mobily')
         $outletType = $("#typeofOutlet").val();

     $postDeptData = {
                'requestFor'    :  'get-department-wise-metrices',
                'group_name'    :   groupName,
                'survey_id'     :   survey_id,
                'outletType'    :   $outletType,
                'requestParam'  :   $requestParam
      };
     if(baseBank == 'mobily'){
         if($outletType == ''){
             $("#metrices_select_loader").hide();
             alert("Please select Outlet Type");
         }else{
              $requestObject.initialize($postDeptData,"getDepartmentHTMLMetricesRejected",null);
         }
     }else{
          $requestObject.initialize($postDeptData,"getDepartmentHTMLMetricesRejected",null);
     }

 }

function getDepartmentHTMLMetricesPending($content){
   var content = $.parseJSON($content);
   $('#metrices_select_loader').css('display','none');
   $('#metrices_select_loader_arabic').css('display','none');
   $('#metrices_select_loader').hide();
   $('#selectmetricesPending').html(content);  
} 

function getDepartmentHTMLMetricesRejected($content){
   var content = $.parseJSON($content);
   $('#metrices_select_loader').css('display','none');
   $('#metrices_select_loader_arabic').css('display','none');
   $('#metrices_select_loader').hide();
   $('#selectmetricesRejected').html(content);
}


function areaWiseScoreGraph($content)
{
  $content = $.parseJSON($content);
    if($content.data_found) {
        popupCallBackCustom();
        $('#area_score_label').html($content.areaScoreLabel);
        $('#area_wise_score_excel_link').html($content.excel_download);
        makeAreaGraph($content.areaScoreRowset,$month_name);
    }  else {
        $('#area_graph').html('<div align="center" style="margin-left:20px;">'+
         '<div align="center" style="font-weight: bold;padding-bottom:10px;padding-top: 130px;font-size: 36px;color:red;">' + getArabicText('No Data') + '</div>'+
         '</div>');
    }  
    
    /*Download Excel Report */
    $('#area_wise_score_excel_link .btn btn-round').click(function(){
        $postData = {
        'requestFor'    :  'area-wise-score-excel',
        'requestParam'  :   {
                'bank name' : 'Riyad Bank',
                'month'   : "11" 
        }   
      };
      $requestObject.initialize($postData,'downloadExcelResponse',null);
    });
}
  
 
function BranchScoreGraphForBranchProfilePage($content){
    
    $content = $.parseJSON($content);
    if($content.data_found) {
        popupCallBackCustom($content);
        makeBranchScoreGraph($content.regionScoreRowset);
    }  else {
        $('#BranchScoreByGraph').html('<div align="center" style="margin-left:20px;">'+
            '<div align="center" style="font-weight: bold;padding-bottom:10px;padding-top: 130px;font-size: 36px;color:red;">' + getArabicText('Nothing Found') + '</div>'+
            '</div>'
        );
    }  
    
}

function RegionWiseHistoryGraph($content){
    
    $content = $.parseJSON($content);
    if($content.data_found) {
        popupCallBackCustom();
        makeRegionGraph($content.regionScoreRowset);
    }  else {
        $('#ScoreByGraph').html('<div align="center" style="margin-left:20px;">'+
            '<div align="center" style="font-weight: bold;padding-bottom:10px;padding-top: 130px;font-size: 36px;color:red;">' + getArabicText('Nothing Found') + '</div>'+
            '</div>'
        );
    }  
    
}
  
 function reloadPopUpATM(){
    //alert('clicked');
 }
 
 /* function downloadProfilePdf(code,type,bank,action) {
  // alert('clicked');
   $.post('../helper/hasSurvey.php',{
        'code'  :   code,
        'type'  :   type,
        'bank'  :   bank
    },function(result){
        result = JSON.parse(result);
        if(!result.success){
            alert("No survey found for Branch - "+code+","+type);
        }else{
            if(action == 'download')
                downloadPdf(code,type,bank);
            else
                emailPdf(code,type,bank);
        }
    }) 
} */

 
function downloadProfilePdf(code,type,bank,action) {
     var month = (new Date).getMonth() + 1;
     var year  = (new Date).getFullYear();
     if( $("#branch-report #profileReportMonthYearPicker").val() ){
        month = $("#branch-report #profileReportMonthYearPicker").val().split('/')[0];
        year  = $("#branch-report #profileReportMonthYearPicker").val().split('/')[1];
     }
     $requestParam = {
        'code'  :   code,
        'type'  :   type,
        'bank'  :   bank,
        'action':   action,
        'month' :   month,
        'year' :   year
     };
   if($requestParam['action']=='download'){
       $postData = {
             'requestFor'    :  'get-indivdual-branch-pdf-report',
             'requestParam'  :   $requestParam
       };
     
      $requestObject.initialize($postData,"mailOrDownload",null);
    } else {
      emailPdf($content['code'],$content['type'],$content['bank'],$content['bank'],$content['action']);  
    } 
 }

function mailOrDownload($content){
  $content = $.parseJSON($content);
  if(!$content['result']){
            alert("No survey found for Branch - "+$content['code']+","+$content['type']);
  } else {  
    if($content['action'] =='download'){
        downloadPdf($content['code'],$content['type'],$content['bank'],$content['action']);
    } else {
      //  emailPdf($content['code'],$content['type'],$content['bank'],$content['bank'],$content['action']); 
    }
   }
 }

 function downloadPdf(code,type,bank,action){
    /*$requestParam = {
        'code'  : code,
        'type'  : type,
        'bank'  : bank,
        'action': action
    } 
    $postData = {
        'requestFor'   :'download-branch-individual-pdf',
        'requestParam' : $requestParam
    } */
  //  return false; 
 
   //var upDatedbaseUrl = location.protocol + '//' + location.host;
   
   // param here used for testing
   var param = {};
   var upDatedbaseUrl = bankAdminBaseUrl;
   
   var actionInput = document.createElement("input");
    actionInput.setAttribute("name", 'action');
    actionInput.setAttribute("value", 'download');
    actionInput.setAttribute("type", "hidden");
    
    param['action'] = 'download';
    
    var codeInput = document.createElement("input");
    codeInput.setAttribute("name", 'code');
    codeInput.setAttribute("value", code);
    codeInput.setAttribute("type", "hidden");
    
    param['code'] = code;
    
    var typeInput = document.createElement("input");
    typeInput.setAttribute("name", 'type');
    typeInput.setAttribute("value", type);
    typeInput.setAttribute("type", "hidden");
    
     param['type'] = type;
    
    /*
    if(bankFolder == 'arbbranches_v2'){
     param['bank'] = "arbbranches_v2";
    }else if(bankFolder == 'arbatm'){
         param['bank'] = 'arbatm';
    }else{
        param['bank'] = bank;
    }*/
        
    var bankInput = document.createElement("input");
    bankInput.setAttribute("name", 'bank');
    bankInput.setAttribute("value", bank);
    bankInput.setAttribute("type", "hidden");
    
    if(bankFolder == 'arbbranches_v2'){
      bankInput.setAttribute("value", "arbbranches_v2");
    }else if(bankFolder == 'arbatm'){
          bankInput.setAttribute("value", "arbatm");
    }

    var month = (new Date).getMonth() + 1;
    var year  = (new Date).getFullYear();


    if( $("#profileReportMonthYearPicker").val() ){
        month = $("#profileReportMonthYearPicker").val().split('/')[0];
        year  = $("#profileReportMonthYearPicker").val().split('/')[1];
    }

  //   console.log( $("#branch-report #profileReportMonthYearPicker"));
    
    var monthInput = document.createElement("input");
    monthInput.setAttribute("name", 'month');
    monthInput.setAttribute("value", month);
    monthInput.setAttribute("type", "hidden");
    
     param['month'] = month;
    
    var yearInput = document.createElement("input");
    yearInput.setAttribute("name", 'year');
    yearInput.setAttribute("value", year);
    yearInput.setAttribute("type", "year");
    
    var langTypeInput = document.createElement("input");
                        langTypeInput.setAttribute("name", 'langType');
                        langTypeInput.setAttribute("value", 'english');
                        langTypeInput.setAttribute("type", "hidden");
    
      param['year'] = year;
    
    var myForm = document.createElement("form");
    myForm.method = 'post';
    if(isArabic){
        myForm.action = upDatedbaseUrl + '/Bank-Admin/pdf-creator/download-profile-pdf_arabic.php';
    }else{
        if(bankFolder == 'hrdf' || bankFolder == 'arbatm'){
            langType = '';
            myForm.action = upDatedbaseUrl + '/Bank-Admin/pdf-creator/download-profile-pdf.php';
            $.ajaxSetup( {async: false });
            $.post(baseUrl+''+baseBank+'/request.php',
                   {'requestFor'    :  'get-lang',
                    'requestParam'  :   {
                                'arabic-check'    : true
                        }   },function(response){
                    if(response){
                        response = JSON.parse(response);
                        langType = response.langType;
                        langTypeInput.setAttribute("value", langType);
                        $.ajaxSetup( {async: true });                        
                    }
                });
           
        }else{
            myForm.action = upDatedbaseUrl + '/Bank-Admin/pdf-creator/download-profile-pdf.php';    
        }
        
    }

   console.log(myForm.action + "?" + $.param( param ));

    
    myForm.appendChild(actionInput);
    myForm.appendChild(codeInput);
    myForm.appendChild(typeInput);
    myForm.appendChild(bankInput);
    myForm.appendChild(monthInput);
    myForm.appendChild(yearInput);
    myForm.appendChild(langTypeInput);
    //console.log(myForm);
    //return;
    document.body.appendChild(myForm);
    myForm.submit();
    document.body.removeChild(myForm);
 
 }

function downloadProfileExcel(code,type,bank,action, reportType) {
    var month = (new Date).getMonth() + 1;
    var year  = (new Date).getFullYear();
    if( $("#branch-report #profileReportMonthYearPicker").val() ){
        month = $("#branch-report #profileReportMonthYearPicker").val().split('/')[0];
        year  = $("#branch-report #profileReportMonthYearPicker").val().split('/')[1];
    }
    $requestParam = {
        'code'  :   code,
        'type'  :   type,
        'bank'  :   bank,
        'action':   action,
        'month' :   month,
        'year' :   year,
        'reportType': reportType
    };
    if($requestParam['action']=='download'){
        $postData = {
            'requestFor'    :  'get-indivdual-branch-pdf-report',
            'requestParam'  :   $requestParam
        };

        $requestObject.initialize($postData,"mailOrExcelDownload",null);
    } else {
        emailPdf($content['code'],$content['type'],$content['bank'],$content['bank'],$content['action']);
    }
}

function mailOrExcelDownload($content){
    $content = $.parseJSON($content);
    if(!$content['result']){
        alert("No survey found for Branch - "+$content['code']+","+$content['type']);
    } else {
        if($content['action'] =='download'){
            downloadExcel($content['code'],$content['type'],$content['bank'],$content['action'],$content['reportType']);
        } else {
            //  emailPdf($content['code'],$content['type'],$content['bank'],$content['bank'],$content['action']);
        }
    }
}

function downloadExcel(code,type,bank,action, reportType){

    var param = {};

    var bankName = (bankFolder == 'riyad') ? "riyad_V3" : bankFolder;
    bankName = (bankName == 'hrdf') ? "hrdf_v2" : bankName;

    if(bankFolder == 'rbatm'){
        bankName = 'rbatm_v3';
    }

    var upDatedbaseUrl = baseUrl + "/"+ bankName;

    var actionInput = document.createElement("input");
    actionInput.setAttribute("name", 'request');
    actionInput.setAttribute("value", 'create-branch-profile-pdf');
    actionInput.setAttribute("type", "hidden");

    param['request'] = 'create-branch-profile-pdf';

    var codeInput = document.createElement("input");
    codeInput.setAttribute("name", 'code');
    codeInput.setAttribute("value", code);
    codeInput.setAttribute("type", "hidden");

    param['branchCode'] = code;

    var typeInput = document.createElement("input");
    typeInput.setAttribute("name", 'type');
    typeInput.setAttribute("value", type);
    typeInput.setAttribute("type", "hidden");

    param['branchType'] = type;


     if(bankFolder == 'arbbranches_v2'){
     param['bank'] = "arbbranches_v2";
     }else if(bankFolder == 'arbatm'){
     param['bank'] = 'arbatm';
     }else{
     param['bank'] = bank;
     }

    var bankInput = document.createElement("input");
    bankInput.setAttribute("name", 'bank');
    bankInput.setAttribute("value", bank);
    bankInput.setAttribute("type", "hidden");

    if(bankFolder == 'arbbranches_v2'){
        bankInput.setAttribute("value", "arbbranches_v2");
    }else if(bankFolder == 'arbatm'){
        bankInput.setAttribute("value", "arbatm");
    }

    var month = (new Date).getMonth() + 1;
    var year  = (new Date).getFullYear();
    if( $("#branch-report #profileReportMonthYearPicker").val() ){
        month = $("#branch-report #profileReportMonthYearPicker").val().split('/')[0];
        year  = $("#branch-report #profileReportMonthYearPicker").val().split('/')[1];
    }

    var monthInput = document.createElement("input");
    monthInput.setAttribute("name", 'month');
    monthInput.setAttribute("value", month);
    monthInput.setAttribute("type", "hidden");

    param['month'] = month;

    var yearInput = document.createElement("input");
    yearInput.setAttribute("name", 'year');
    yearInput.setAttribute("value", year);
    yearInput.setAttribute("type", "year");

    var langTypeInput = document.createElement("input");
    langTypeInput.setAttribute("name", 'langType');
    langTypeInput.setAttribute("value", 'english');
    langTypeInput.setAttribute("type", "hidden");

    param['year'] = year;

    var myForm = document.createElement("form");
    myForm.method = 'post';
    if(isArabic){
        myForm.action = upDatedbaseUrl + '/SnappyPdf.php';
    }else{
        if(bankFolder == 'hrdf' || bankFolder == 'arbatm'){
            langType = '';
            myForm.action = upDatedbaseUrl + '/SnappyPdf.php';
            $.ajaxSetup( {async: false });
            $.post(baseUrl+''+baseBank+'/request.php',
                {'requestFor'    :  'get-lang',
                    'requestParam'  :   {
                        'arabic-check'    : true
                    }   },function(response){
                    if(response){
                        response = JSON.parse(response);
                        langType = response.langType;
                        langTypeInput.setAttribute("value", langType);
                        $.ajaxSetup( {async: true });
                    }
                });

        }else{
            myForm.action = upDatedbaseUrl + '/SnappyPdf.php';
        }

    }

    console.log(reportType);

    $excelUrl = myForm.action + "?" + $.param( param );

    if(reportType == 'excel'){
        $excelUrl = $excelUrl + "&isExcel=true";
    }


    console.log($excelUrl);
    //return;

    window.location.assign($excelUrl);
    //


}
 
 function emailPdf(code,type,bank,action){
    $requestParam = {
        'code'   :code,
        'type'   :type,
        'bank'   :bank,
        'action' :action
    };
    $postData = {
        'requestFor'  :'email-pdf-report',
        'requestParam': $requestParam
    };
    return false;
 }
 
function emailPdfOrignal(code,type,bank)
{
  $.post(
        baseUrl + 'pdf-creator/download-profile-pdf.php',
        {
            "action"    : 'email',
            "bank"      :  bank,
            "type"      :  type,
            "code"      :  code,
            "email"     :  $("#graphEmailAddress").val(),
            "message"   :  $("#graphMessage").val()
        },function(){
            $("#graphEmailLoader").hide();
            $("#graphEmailSuccess").show();
            $("#graphEmailSuccessMsg").show();
        });
}


/* Assest Related Function */
function getAssestCityList($provinceName){
    
    $requestData = {
        "requestFor"    :   "city-data",
        "map-filters"       :   {
                                    "region"        :   $provinceName,
                                    "requestType"   :   'branch'
                                }
    };
    $requestObject.initialize($requestData,"fillAssestCityOption",null);
    
}

function fillAssestCityOption($data)
{
     $("#city_assest_branch").html($data);
     $('#assest_province_select_loader').css('display','none');
}

function filterAssestData($byStatus,$type)
{
   if($byStatus=='status'){
         $status = $("#status_assest_branch :selected").text();
         $assestMetrices = $("#metrices_assest_branch_id :selected").val();
         $answerText = $("#answer_assest_branch :selected").text();
         $pageNumber = $("#gotoAssestPageNumber :selected").val();
         
         
         $('#assestTicketFileterloader').css('display','inline');
        // $('#assestAlertTicketContent').html('');
            
         $requestData = {
            "requestFor"       :   "assest-ticket",
            "assest-filters"   :  {
                                  "status"        :  $status,
                                  "metrices-name" :  $assestMetrices,
                                  "page-number"   :  $pageNumber,
                                  "answer"        :  $answerText,
                                  "by"            :  "status"
                                }
          };
            
          $requestObject.initialize($requestData,"assestDetailCallBack",null);
   }
   
   if($byStatus=='ticketNumber'){
         $ticketNumber = $("#search_assest_ticket").val();
         $pageNumber = $("#gotoAssestPageNumber :selected").val();
       
         $('#assestTicketFileterloader').css('display','inline');
        // $('#assestAlertTicketContent').html('');
            
         $requestData = {
            "requestFor"       :   "assest-ticket",
            "assest-filters"   :  {
                                  "ticketnumber"  :  $ticketNumber,
                                  "page-number"   :  $pageNumber,
                                  "by"            :  "alert_ticket_number"
                                }
          };

      $requestObject.initialize($requestData,"assestDetailCallBack",null);
   }
   
    if($byStatus=='code'){
         $centerCode = $("#search_assest_code").val();
         $pageNumber = $("#gotoAssestPageNumber :selected").val();
         $pageNumber = 0;
         $('#assestTicketFileterloader').css('display','inline');
        // $('#assestAlertTicketContent').html('');
            
         $requestData = {
            "requestFor"       :   "assest-ticket",
            "assest-filters"   :  {
                                  "center-code"   :  $centerCode,
                                  "page-number"   :  $pageNumber,
                                  "by"            :  "alert_center-code"
                                }
          };

      $requestObject.initialize($requestData,"assestDetailCallBack",null);
   }
}


var $type = 'next';
function changeAssestPageNumber($stepType, $totalPage)
{
      $type = $stepType;
      $pageNumber = $('#gotoAssestPageNumber option:selected').val();
      if($stepType == 'next'){
        $pageNumber =Number($pageNumber)+10;
      }else if($stepType == 'prev'){
        $pageNumber =Number($pageNumber)-10;
      }else{
        $pageNumber = Number($pageNumber);
      }
      $status = $("#status_assest_branch :selected").text();
      $assestMetrices = $("#metrices_assest_branch_id :selected").val();
      $answerText = $("#answer_assest_branch :selected").text();
      $ticketNumber = $("#search_assest_ticket").val();
      
      $('#assestTicketFileterloader').css('display','inline');
      //$('#assestAlertTicketContent').html('');
     
     
      if($ticketNumber){
            $requestData = {
            "requestFor"       :   "assest-ticket",
            "assest-filters"   :{
                                  "ticketnumber"  :  $ticketNumber,
                                  "page-number"   :  $pageNumber,
                                  "by"            :  "alert_ticket_number"
                                }
            }
             
      } else {
          $requestData = {
          "requestFor"       :   "assest-ticket",
          "assest-filters"   :  {
                                  "status"        :  $status,
                                  "metrices-name" :  $assestMetrices,
                                  "page-number"   :  $pageNumber,
                                  "answer"        :  $answerText,
                                  "by"            :  "status"
                                }
          }
       }
      $requestObject.initialize($requestData,"assestDetailCallBack",null);
  }
  
  function managePrevNext($type)
  {  
     if($('#gotoAssestPageNumber option:selected').attr('pageClass') == 'last'){
        $('#changeAssestPageNumberNextButton').hide();
        $('#changeAssestPageNumberPrevButton').show();
     }else if($('#gotoAssestPageNumber option:selected').attr('value') == 0){
        $('#changeAssestPageNumberNextButton').show();
        $('#changeAssestPageNumberPrevButton').hide();
     }else{
        $('#changeAssestPageNumberNextButton').show();
        $('#changeAssestPageNumberPrevButton').show();
     }
  }
  
  function gotoAssestPageNumber()
  {
    $pageNumber = $('#gotoAssestPageNumber option:selected').val();
    changeAssestPageNumber('other', $pageNumber);
  }



function assestDetailCallBack($result)
{ 
  
  /* $('.alertTicketViewLink').click(function(){
        $popupObject.setWidth("800px");
        $popupObject.initialize(this,'#alertTicketDetailPopup','getAlertTicketPopupDetail');
   }) */
    
  $content = $.parseJSON($result);
  $('#assestTicketFileterloader').css('display','none');   
  if($result){
    $('#assestAlertTicketContent').html($content['html']);
  } else {
    $('#assestAlertTicketContent').html('<b>No Data</b>');
  }
  managePrevNext($type);
}
function resetAssest($type)
{ 
    $('#assestTicketFileterloader').css('display','inline');
        // $('#assestAlertTicketContent').html('');
    $('#search_assest_code').attr('value', '');
      
         $requestData = {
            "requestFor"       :   "assest-ticket",
            "assest-filters"   :  {
                                  "status"        :  '',
                                  "metrices-name" :  '',
                                  "page-number"   :  0,
                                  "answer"        :  '',
                                  "by"            :  "status"
                                }
          };
       
       
       
       $("#metrices_assest_branch_id option:first").attr('selected','selected');
       $("#status_assest_branch option:first").attr('selected','selected');
       $("#answer_assest_branch option:first").attr('selected','selected');
       $('#search_assest_ticket').val('');     
       
       $requestObject.initialize($requestData,"assestDetailCallBack",null);
}

function sortByAssestTicketQuestion($sorOn, $sortOrder)
{
     $assestMetrices = $("#metrices_assest_branch_id :selected").val();
     $answerText = $("#answer_assest_branch :selected").text();
     $pageNumber = $("#gotoAssestPageNumber :selected").val();
     $status = $("#status_assest_branch :selected").text();
     
     
     $('#assestTicketFileterloader').css('display','inline');
   //  $('#assestAlertTicketContent').html('');
        
     $requestData = {
        "requestFor"       :   "assest-ticket",
        "assest-filters"   :  {
                              "status"        :  $status,
                              "metrices-name" :  $assestMetrices,
                              "page-number"   :  $pageNumber,
                              "answer"        :  $answerText,
                              "by"            :  "status",
                              "sortOn"        :  $sorOn,
                              "sortOrder"     :  $sortOrder
                            }
      };
        
      $requestObject.initialize($requestData,"assestDetailCallBack",null);
}

/* End Assest Related Function */

/* Pulse Panel */
$(document).ready(function(){
    $('.drill_button').click(function(){ 
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#drillPopUp','getDrillContent');
        $popupObject.setWidth("800px");
    });

});

function getDrillContent($element){
    $filterDivId = $($element).attr("filter-div");
    $filterData = getFilterData($filterDivId);
    
      $requestData = {
            "requestFor"       :   "drill-content",
            "drill-for"        :   $($element).attr("drill-for"),
            "filter-data"      :   $filterData
            
      }; 
      //console.log($requestData);
      $requestObject.initialize($requestData,"fillDrillContent",null);
}

function fillDrillContent(result){
  
         $('#drillPopUp').html(result);
 
}
$elementList = ['input','select','textarea'];
function getFilterData($parentElement){
   $filterData ={
    
   };
    for(var i in $elementList){
        $($parentElement).find($elementList[i]).each(function(){
            $filterData[$(this).attr('name')] = $(this).val();
        })
    }
    return $filterData;
}

/* SD TICKET MERGING POPUP */
    
    $(document).delegate(".sd_ticket_info","click",function(e){
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#sdTicketPopup','getSdTicketContent');
        $popupObject.setWidth("800px");
    });



function getSdTicketContent($element){
    $requestData = {
            "requestFor"       :   "sd-ticket-info",
            "sd-ticket-number" :   $($element).attr("ticket-number")
      }; 
    $requestObject.initialize($requestData,"fillSdTicketInfoContent",null);
}

function fillSdTicketInfoContent($result){
    $("#sdTicketPopup").html($result);
}

/* NCB TRANS IMAGE POPUP */

$(document).ready(function(){
    if(bankFolder == "arbasset"){
        $("#asset_gallery_container").on("click", ".gallery_image_popup", function(){
            $popupObject.setWidth("500px");
            $popupObject.initialize(this,'#imageGallerypopup','getGalleryImage');
            $popupObject.setWidth("800px");
        });
    }else{
        $(".gallery_image_popup").on("click", function(){
            $popupObject.setWidth("500px");
            $popupObject.initialize(this,'#imageGallerypopup','getGalleryImage');
            $popupObject.setWidth("800px");
        });
    }
    
});

function getGalleryImage($element){
    $requestData = {
            "requestFor"  :   "gallery-images",
            "image-path"  :   $($element).attr("image-path"),
            "image-text"  :   $($element).attr("image-text"),
            "image-style" :   $($element).attr("image-style"),
            "branch-code" :   $($element).attr("branch-code")
      }; 
    $requestObject.initialize($requestData,"fillgalleryImage",null);
}
function fillgalleryImage($result){
    $("#imageGallerypopup").html($result); 
}

/* TICKET ANALYSIS POPUP */

function getAnalysisTickets($element){
    $postData = {
        'requestFor'    :  'question-ticket',
        'requestParam'  :   {
                'ticket-current-status' :  $($element).attr('ticket-current-status'),
                'requestType'           :  $($element).attr('ticket-current-status')+' Tickets',
                'ticket-time'           :  $($element).attr('ticket-time'),
                'department-id'         :  $($element).attr('department-id')
        }   
    };
    
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}

function filterMobilydata($filterBy, $filterFor){
    var $requestData;
    switch($filterFor){
        case 'kiosk':
            if($filterBy == "outlet"){
                $requestData = {
                    'requestFor' : 'health-metrices',
                    'filter-data' :{
                        'department-id' : $("#kiosk-department-id").val(),
                        'outlet-code' : $("#kiosk-ouletcode").val()
                    } 
                }

            }else if($filterBy == "region"){
                $requestData = {
                    'requestFor' : 'health-metrices',
                    'filter-data' :{
                        'department-id' : $("#kiosk-department-id").val(),
                        'province-name' : $("#kioskRegion option:selected").val(),
                        'city-name'     : $("#kioskCity option:selected").val()
                    } 
                }

            }
            break;
        
         case 'shopinshop':
             if($filterBy == "outlet"){
                $requestData = {
                    'requestFor' : 'health-metrices',
                    'filter-data' :{
                        'department-id' : $("#shopinshop-department-id").val(),
                        'outlet-code' : $("#shopinshop-ouletcode").val()
                    } 
                }

            }else if($filterBy == "region"){
                $requestData = {
                    'requestFor' : 'health-metrices',
                    'filter-data' :{
                        'department-id' : $("#shopinshop-department-id").val(),
                        'province-name' : $("#shopinshop-province-name").val(),
                        'city-name'     : $("#shopinshop-city-name").val()
                    } 
                }

            }
            break;
         default:
            alert("wrong choice");
            
    }
   
    if($requestData['filter-data']['outlet-code'] == '' && $filterBy == 'outlet'){
        alert("Please enter oultet code");
        
    }else if($filterBy == 'region' && (!$requestData['filter-data']['province-name'])){
        alert("Please select region filter");
    }else{
        $(".loading_mobily_data").show();
        if($filterBy == 'reset')
            $("#kiosk-ouletcode").val('');
        $requestObject.initialize($requestData,'fillKioskShopinshopData',null);
    }
}

function resetMobilydata($filterFor){
    $("#kioskIssueContainer select").prop("selectedIndex","0");
    $("#kioskIssueContainer input").val('');
    if($filterFor == "kiosk"){
        $requestData = {
            'requestFor' : 'health-metrices',
            'filter-data' :{
                'department-id' : $("#kiosk-department-id").val()
            } 
        }
    }
    $(".loading_mobily_data").show();
    $requestObject.initialize($requestData,'fillKioskShopinshopData',null);
}

function fillKioskShopinshopData($result){
    var $issueRowset = JSON.parse($result);
    //console.log($issueRowset);
    $.each($issueRowset, function($index, $value){
           $("#info_box_"+$index).html($value);
       
     
    });
    $(".loading_mobily_data").hide();
}
$(document).ready(function(){
    $("#myTab li a#campaign_management").on("click",function(){
        $("#campaign-outlet-search").trigger("click");
    });
});

function getCampaignData($filterBy, $filterFor){
    var $requestData;
    findMobilyFilter('campaign');
    if($filterBy == "search"){
        mobilyFilterData.set('orderField', 'campaignOutlet');
        mobilyFilterData.set('orderType', 'ASC');
    }else{
        mobilyFilterData.destroy();
        $("#campaign_filters input").val('');
        $('#campaign_filters select').val('');
    }
    $requestData ={
                'requestFor' : 'campaign-survey-detail',
                'filter-data' : mobilyFilterData.get()
            };
    $("#campaign-survey-loader").show();
    $("#campaign-survey-loader-image").show();
    $requestObject.initialize($requestData,'fillCampaignData',null);
}

function fillCampaignData($result){
    //var $result = JSON.parse($result);
    $("#campaign_survey_result").html($result);
    $("#campaign-survey-loader-image").hide();
    $("#campaign-survey-loader").hide();
}

function getMobilyCityList($dataFor){ 
    var $region = $("#"+$dataFor+"Region").val();
    if($region === undefined){
        $region = $("#"+$dataFor+"select").val();
    }
    var $outlet = $("#group_name").val();
    $requestData = {
        "requestFor"    :   "city-data",
        "map-filters"   :     {
                                    "region"        :   $region,
                                    "requestType"   :   $dataFor,
                                    "outletType"    : $outlet
                              }
    };
    $(".mobilyCityLoader").css("display", "inline");
    $requestObject.initialize($requestData,"fillMobilyCityOption",null);
    
}

function getMobilyCity($datafor){

    var $region = $("#area_name_select"+$datafor).val();
    var $dataFor='mobilyTicket';
    var $outlet = $('#mcpReportOutletTypeSelect').val();

   
    $requestData = {
        "requestFor"    :   "city-data",
        "map-filters"   :     {
                                    "region"        :   $region,
                                    "requestType"   :   $dataFor,
                                    "outletType"    : $outlet
                              }
    };
    $(".mobilyCityLoader").css("display", "inline");
    $requestObject.initialize($requestData,"fillMobilyCityOption",null);
    
}
function fillMobilyCityOption($result){

    $(".mobilyCityLoader").css("display", "none");
    $('#message-inventory').css("display", "none");
    
    $("#campaignCity").html($result);
    $("#kioskCity").html($result);
    $("#mobilyTicketCity").html($result);
    $("#outletCity").html($result);
    $("#mobilyZoneCity").html($result);
    $("#mobilyScorecardCity").html($result);
    $("#mobilyTopLevelCity").html($result);
    $("#mobilyVisitReportCity").html($result);
     $("#mobilyMcpCity").html($result);
      $("#mobilyAdvanceScorecardCity").html($result);
      $("#mobilyBranchCity").html($result);

}


function advanceScoreCard(){
  var $region =$('#area_name_select_advancescorecard').val();
  var  $city = $('#mobilyAdvanceScorecardCity').val();
  var $outlet = $('#departmentWiseAdvanceScoreCardOutletTypeSelect').val();
  var $fromDate = $('#advanceScoreReportFrom').val();
  var $toDate = $('#advanceScoreReportTo').val();
  $("#fillScorecardresult").html($("#AdvanceScorecardLoader").html());
         $postData = {
                'requestFor'    :  'advance-scorecard',
                'requestParam'  :  {
                                    "region"   :   $region,
                                    "city"   :   $city,
                                    "outlet"    :   $outlet,
                                    "fromDate"    :   $fromDate,
                                    "toDate" :$toDate
                                   }
           };
           
           $requestObject.initialize($postData,null,"#fillScorecardresult");


}

function advance($result){
  $result=JSON.parse($result);

}

 function updateInventoryScore($indexString,$valueString,$branchCode,$branchType){
          
           $('#loader-image-inventory').css("display", "inline");
           $postData = {
                'requestFor'    :  'inventory-updation',
                'requestParam'  :  {
                                    "indexString"   :   $indexString,
                                    "valueString"   :   $valueString,
                                    "branchCode"    :   $branchCode,
                                    "branchType"    :   $branchType
                                   }
           };
           
           $requestObject.initialize($postData,"inventoryScoreCallBack",null);
 }
 
 function inventoryScoreCallBack()
 {   
      $('#loader-image-inventory').css("display", "none");
      $('#message-inventory').css("display", "inline");
 }
 
 function drillNcbRebrandingData($region, $status,phase, surveyStatus){
    
        $requestParam = {
            'requestFor'  : 'ncbrebranding-graph-drill',
            'region'      :  $region,
            'status'      :  $status,
            'phase'       :  phase,
            'surveyStatus': surveyStatus
        };
        $popupObject.setWidth("1000px");
        $popupObject.open('#alertTicketDetailPopup','getNcbRebrandingGraphContent',$requestParam);
        $popupObject.setWidth("800px");
 }
 
 function getNcbRebrandingGraphContent($requestParam){
      $requestObject.initialize($requestParam,'popupCallBackCustom','#alertTicketDetailPopup');
 }
   
function ughiTicketDetail($element){
    //console.log($element);
    ticketNumber = $($element).attr("ticket-number");
    requestType  = $($element).attr("request-type");
    
    $requestData = {
        'requestFor'   : 'ughi-ticket-detail',
        'ticketNumber' : ticketNumber,
        'request-type' : requestType
    };
    $requestObject.initialize($requestData, null, '#ticketDetailPopup');
}

function getFacilityProvince(requestId, fillId){
    $requestData = {
        "requestFor"        :   "get-facility-province",
        "facilityID"        :   $(requestId).val()
         };
    $requestObject.initialize($requestData,null,fillId);
}
