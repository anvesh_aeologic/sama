
// --------------------Global data filter ---------

$elementList = ['input','select','textarea'];
$errorMessage = [];
$errorMessage['start_date'] = 'Please enter start date';
$errorMessage['end_date']   = 'Please enter end date';

/**
 * @description  
 * @param Id parent element
 * @return array of object of filters
 */
var ughiFilterData = function($parentElement){
    var filterData = {};

    for(var i in $elementList){
        $($parentElement).find($elementList[i]).each(function(){

            filterData[$(this).attr('name')] = $(this).val();
        })
    }
   // console.log(filterData);
    return filterData;
};

/**
 * @param filter parent element, corresponding go button id
 * @return reset all filters of parent element and trigger click to go button
 */

var resetUghiFilters = function($parentElement, $correspondingGoButtonId,$isDateResetRequest){
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var date = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month;
    var date_year = d.getFullYear();
    var date_month = d.getMonth()+1;

    //var startDateTotalVisit = $.datepicker.formatDate('mm/01/yy', new Date());
    //var lastDateTotalVisit  = $.datepicker.formatDate('mm/dd/yy', new Date());
    //$("#ughi_total_visit_start_date_selector").datepicker( "setDate", startDateTotalVisit);
    //$("#ughi_total_visit_end_date_selector").datepicker( "setDate", lastDateTotalVisit);
    for(var i in $elementList){
        $($parentElement).find($elementList[i]).each(function(){
            if(!$(this).hasClass('do_not_reset')){
                idName = $(this).attr("id");
                if(idName == 'brand_month' || idName == 'product_month' || idName == 'activity_month'){
                    $('#'+idName).val(date_month+"/"+date_year)
                }else{
                    $(this).val('');
                    $('#merchandiser_name').val('').change();
                    $('#merchandiser_name1').val('').change();
                    $('#brand_id').val('').change();
                    $('#merchandiser_name_workforce').val('').change();
                    $('#merchandiser_nameWF').val('').change();
                    $('#report-outlet-type-selector').val('').change();
                    $('#other-outlet-type-selector').val('').change();
                    $("#close_ticket_type").hide();
                    $("#close_shelveNl_ticket_type").hide();
                    $("#close_shelve_ticket_type").hide();
                    $("#period-filter-selector").val("This Week");
                    $("#outletcode-filter-selector").val();
                    $('#ughiReportDatePickerFilter').css('display','none');
                    //masterTicketDetailDate();
                }
                if($parentElement == '#ughi_total_orders_piechart_filters'){

                    var startDate = new Date();
                    var numberOfDaysToSub = 30;
                    startDate.setDate(startDate.getDate() - numberOfDaysToSub);
                    var lastDate = $.datepicker.formatDate('mm/dd/yy', new Date());
                    $("#ughi_total_order_start_date_selector").datepicker( "setDate", startDate);
                    $("#ughi_total_order_end_date_selector").datepicker( "setDate", lastDate);
                }
                if($parentElement == '#ughi_total_visit_piechart_filter'){

                    var startDate = $.datepicker.formatDate('mm/01/yy', new Date());
                    var lastDate  = $.datepicker.formatDate('mm/dd/yy', new Date());
                    $("#ughi_total_visit_start_date_selector").datepicker( "setDate", startDate);
                    $("#ughi_total_visit_end_date_selector").datepicker( "setDate", lastDate);
                }
            }
            if($isDateResetRequest){
                if($(this).hasClass('do_not_reset')){
                    var startdate = $.datepicker.formatDate('yy-mm-01', new Date());
                    var lastDate  = $.datepicker.formatDate('yy-mm-dd', new Date());
                    idName = $(this).attr("id");
                    
                    if(idName == 'reportDateFrom' || idName == 'otherDateFrom'){
                        $('#'+idName).val(startdate);
                    }
                    if(idName == 'reportDateTo' || idName == 'otherDateTo'){
                        $('#'+idName).val(lastDate);
                    }
                    
                }
            }
        })
    }
    $($correspondingGoButtonId).trigger("click");

    if($correspondingGoButtonId == '#ughi_inventory_go') {

        getCustomFilterByOutletType($('.branch_type_ughi_inventory_container').val(),'ughi_inventory_container');
    }

    if($correspondingGoButtonId == '#ughi_shelve_compliance_go') {

        getCustomFilterByOutletType($('.branch_type_ughi_compliance_container').val(),'ughi_compliance_container');
    }
};

/**
 * 
 * @param filter data Array
 * @return boolen(true/false)
 */
var validateUghiFilter = function(filterData){
    if(filterData['end_date'] && !filterData['start_date'])
        return $errorMessage['start_date'];
    else
        return false;
};

var ughiTotalVisitPieChart = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);

    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'total-visit-data',
             'filter-data' : filterData
        };


        // $.post('request.php', requestData, function(result){
        //   totalVistPiecChart(result);
        // });

        $requestObject.initialize(requestData,'totalVistPiecChart',null);
        return filterData;
    }
   // getCustomFilterByOutletType($('.branch_type_total_visit').val(),'total_visit');
};


 var ughiWorkforcefilter = function($parentElement, $containerId) {
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
     var startDate = filterData['start_date']; 
     if(filterData['end_date']){
       var endDate = filterData['end_date'];
     }else {
         var endDate = $.datepicker.formatDate('mm/dd/yy', new Date());
     }
       var period = 'Today';
       if(startDate) {
           period = startDate;
           if(endDate)
           period += ' To '+endDate;
       }
       $('#work-force-period').html(period);
       
    var validate   = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'ughi-work-force-map-data',
             'filter-data' : filterData
        };
       
       $requestObject.initialize(requestData,'createUghiWorkForceMap',null);
    }
    
 };


/**
 * @description Regionwise total order place on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */
var ughiTotalOrderPlacedPieChart = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    } else {
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'total-order-placed-data',
             'filter-data' : filterData
        };

        // $.post('request.php', requestData, function(result){
        //   totalOrderPlacedPiecChart(result);
        // });

        $requestObject.initialize(requestData,'totalOrderPlacedPiecChart',null);
        return filterData;
    }
};

/**
 * @description As per planogram per brand on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */
var ughiProductAsPerPlanogramBarChart = function($parentElement, $containerId,$isProfilePage){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    } else {
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'product-as-per-planogram-per-brand-data',
             'filter-data' : filterData,
             'isProfilePage' : $isProfilePage
        };

        // $.post('request.php', requestData, function(result){
        //   productAsPerPlanogramChart(result);
        // });

        $requestObject.initialize(requestData,'productAsPerPlanogramChart',null);
        return filterData;
    }
};



/**
 * @description Regionwise total Stock Tickets on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */
var ughiTotalStockTicketsPieChart = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'total-stock-ticket-data',
             'filter-data' : filterData
        };

        // $.post('request.php', requestData, function(result){
        //   createTotalStockTicketsPieChart(result);
        // });
        $requestObject.initialize(requestData,'createTotalStockTicketsPieChart',null);
        return filterData;
    }
    //getCustomFilterByOutletType($('.branch_type_total_stock').val(),'total_stock');
};

/**
 * @description Regionwise total Stock Tickets on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */
var ughiShelveCategoryBarChart = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'shelve-category',
             'filter-data' : filterData
        };

        // $.post('request.php', requestData, function(result){
        //   createCategoryBarChart(result);
        // });

        $requestObject.initialize(requestData,'createCategoryBarChart',null);
    }
};

/**
 * @description Regionwise total Stock Tickets on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */
var ughiBrandCategoryBarChart = function($parentElement, $containerId,$isProfilePage){
    
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'brand-category',
             'filter-data' : filterData,
             'isProfilePage' : $isProfilePage
        };

        // $.post('request.php', requestData, function(result){
        //   createBrandCategoryBarChart(result);
        // });

        $requestObject.initialize(requestData,'createBrandCategoryBarChart',null);
        return filterData;
    }
};

/**
 * @description Regionwise total Stock Tickets on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */
var ughiOtherActivityChart = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor' : 'other-activity',
             'filter-data' : filterData
        };

        // $.post('request.php', requestData, function(result){
        //   otherActivityBarChart(result);
        // });

        $requestObject.initialize(requestData,'otherActivityBarChart',null);
        return filterData;

    }
};


/**
 * @description Regionwise total Compliance tickets on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */

var ughiTotalComplianceTickets = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor'  : 'total-compliance-ticket-data',
             'filter-data' : filterData
        };

        // $.post('request.php', requestData, function(result){
        //   createTotalComplianceTicketsPieChart(result);
        // });

        $requestObject.initialize(requestData,'createTotalComplianceTicketsPieChart',null);
        return filterData;

    }
   //getCustomFilterByOutletType($('.branch_type_total_compliance').val(),'total_compliance')
};
/**
 * @description Regionwise total Compliance tickets on pie chart
 * @param filter paraent div id, chart container
 * @return dataRowset
 */

var ughiOrderToDate = function($parentElement, $containerId,$isProfilePage){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor'  : 'total-order-to-date',
             'filter-data' : filterData,
             'isProfilePage' : $isProfilePage
        };
//        console.log(requestData['filter-data']['brand_id']);
        var brand_id = requestData['filter-data']['brand_id'];

        // $.post('request.php', requestData, function(result){
        //   createOrderTODateChart(result,brand_id);
        // });

        $requestObject.initialize(requestData,'createOrderTODateChart',null);
        return filterData;
    }
 };

/**
 * @description Ticket filter on ticket panel
 * @param filter paraent div id, result container, request type
 * @return filtered html content
 */
    
var ughiTickets = function($parentElement, $containerId, $requestTicketType){


    $('#close_reason').val('');
    $('#shelve_close_reason').val('');
    $('#shelveNl_close_reason').val('');
    $('#ughi-closebutton').css('display','none');
    $('#close_reason').css('display','none');
    $('#shelve_close_reason').css('display','none');
    $('#ughi-shelve-closebutton').css('display','none');
    $('#ughi-shelve-nl-closebutton').css('display','none');
    $('#shelveNl_close_reason').css('display','none');

    var requestData = {};
    var otherData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);

    if($parentElement == '#stock_ticket_filters'){
        $("#ughi_ticket_panel_container .reportSection").find("a").each(function() {
            newSrc = $(this).attr('myval') + "&region="+filterData['region']+ "&status="+filterData['outlet_ticket_status']+ "&start_date="+filterData['start_date']+"&end_date=" +filterData['end_date']+ "&outlet_code=" +filterData['outlet_code']+ "&ticket_number="+filterData['stock_ticket_number']+"&close_type=" + filterData['close_ticket_type']+"&branch_type=" + filterData['branch_type'];
            $(this).attr('href', newSrc);
            $(".emailReportSection a").attr('myval', newSrc);
        });
        $("#ughi_ticket_panel_container .pdfReportSection").find("a").each(function() {
            newPdfSrc = $(this).attr('myPdfVal') + "&region="+filterData['region']+ "&status="+filterData['outlet_ticket_status']+ "&start_date="+filterData['start_date']+"&end_date=" +filterData['end_date']+ "&outlet_code=" +filterData['outlet_code']+ "&ticket_number="+filterData['stock_ticket_number']+"&close_type=" + filterData['close_ticket_type']+"&branch_type=" + filterData['branch_type'];
            $(this).attr('href', newPdfSrc);
        });
    }else if($parentElement == '#stock_ticket_number_filters') {
        $("#ughi_ticket_panel_container .reportSection").find("a").each(function() {
            newSrc = $(this).attr('myval') + "&ticket_number="+filterData['stock_ticket_number'];
            $(this).attr('href', newSrc);
            $(".emailReportSection a").attr('myval', newSrc);
        });
        $("#ughi_ticket_panel_container .pdfReportSection").find("a").each(function() {
            newPdfSrc = $(this).attr('myPdfVal') + "&ticket_number="+filterData['stock_ticket_number'];
            $(this).attr('href', newPdfSrc);
        });
    }else if($parentElement == '#compliance_ticket_filters'){
        
        var ticket_type = $("#ticketType input:radio:checked").val(); 
        
         $("#ughi_ticket_panel_container .shelveReportSection").find("a").each(function() {
            newSrc = $(this).attr('myval') + "&region="+filterData['region']+ "&status="+filterData['complaince_ticket_status']+ "&start_date="+filterData['start_date']+"&end_date=" +filterData['end_date']+ "&outlet_code=" +filterData['outlet_code']+ "&ticket_type=" +ticket_type+"&ticket_number="+filterData['shelve_ticket_number']+"&close_type=" + filterData['close_shelve_ticket_type']+"&branch_type=" + filterData['branch_type'];
            $(this).attr('href', newSrc);
             $('.shelveEmailReportSection a').attr('myval', newSrc);
        });
        $("#ughi_ticket_panel_container .shelvePdfReportSection").find("a").each(function() {
            newPdfSrc = $(this).attr('myPdfVal') + "&region="+filterData['region']+ "&status="+filterData['complaince_ticket_status']+ "&start_date="+filterData['start_date']+"&end_date=" +filterData['end_date']+ "&outlet_code=" +filterData['outlet_code']+ "&ticket_type=" +ticket_type+"&ticket_number="+filterData['shelve_ticket_number']+"&close_type=" + filterData['close_shelve_ticket_type']+"&branch_type=" + filterData['branch_type'];
            $(this).attr('href', newPdfSrc);
        });
    } else if($parentElement == '#compliance_ticket_number_filters'){
        $("#ughi_ticket_panel_container .shelveReportSection").find("a").each(function() {
            newSrc = $(this).attr('myval') + "&ticket_number="+filterData['shelve_ticket_number']+ "&ticket_type=" +ticket_type;
            $(this).attr('href', newSrc);
            $('.shelveEmailReportSection a').attr('myval', newSrc);
        });
        $("#ughi_ticket_panel_container .shelvePdfReportSection").find("a").each(function() {
            newPdfSrc = $(this).attr('myPdfVal') + "&ticket_number="+filterData['shelve_ticket_number']+ "&ticket_type=" +ticket_type;
            $(this).attr('href', newPdfSrc);
        });
    }else if($parentElement == '#compliance_NL_ticket_filters'){

        $("#ughi_ticket_panel_container .notListedReportSection").find("a").each(function() {
            newSrc = $(this).attr('myval') + "&region="+filterData['region']+ "&status="+filterData['complaince_nl_ticket_status']+ "&start_date="+filterData['start_date']+"&end_date=" +filterData['end_date']+ "&outlet_code=" +filterData['outlet_code']+ "&ticket_number="+filterData['shelve_nl_ticket_number']+"&close_type=" + filterData['close_shelveNl_ticket_type']+"&branch_type=" + filterData['branch_type'];
            $(this).attr('href', newSrc);
            $('.emailNotListedReportSection a').attr('myval', newSrc);

        });

        $("#ughi_ticket_panel_container .notListedPdfReportSection").find("a").each(function() {
            newPdfSrc = $(this).attr('myPdfVal') + "&region="+filterData['region']+ "&status="+filterData['complaince_nl_ticket_status']+ "&start_date="+filterData['start_date']+"&end_date=" +filterData['end_date']+ "&outlet_code=" +filterData['outlet_code']+ "&ticket_number="+filterData['shelve_nl_ticket_number']+"&close_type=" + filterData['close_shelveNl_ticket_type']+"&branch_type=" + filterData['branch_type'];
            $(this).attr('href', newPdfSrc);

        });

    } else if($parentElement == '#compliance_ticket_nl_number_filters'){

        $("#ughi_ticket_panel_container .notListedReportSection").find("a").each(function() {
            newSrc = $(this).attr('myval') + "&ticket_number="+filterData['shelve_nl_ticket_number'];
            $(this).attr('href', newSrc);
            $('.emailNotListedReportSection a').attr('myval', newSrc);
        });

        $("#ughi_ticket_panel_container .notListedPdfReportSection").find("a").each(function() {
            newPdfSrc = $(this).attr('myPdfVal') + "&region="+filterData['region']+ "&status="+filterData['complaince_nl_ticket_status']+ "&start_date="+filterData['start_date']+"&end_date=" +filterData['end_date']+ "&outlet_code=" +filterData['outlet_code'];
            $(this).attr('href', newPdfSrc);
        });

    }
    
    

    if(validate){
        //alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        
        $requestContainer = $containerId.split('#');
        
        
        requestData = {
             'requestFor'  : $requestTicketType,
             'filter-data' : filterData,
             'container-id': $requestContainer[1] 
        };

        // $.post('request.php', requestData, function(result){
        //  $($containerId).html(result);
        // });

        otherData = {
            'container_render_id' : $containerId
        };
        $requestObject.initialize(requestData,'renderResult',null,otherData);
        
        
        requestData = {
             'requestFor'  : 'ughi-ticket-pagination',
             'filter-data' : filterData,
             'request-type': $requestTicketType,
             'container-id': $requestContainer[1],
             'penal'       : 'tickets'
        };

        // $.post('request.php', requestData, function(result){
        //      result = JSON.parse(result);
        //      $("#"+$requestTicketType+"-pagination-div").html(result['html']);
        //      $("#"+$requestTicketType+"-counter").html(result['counter']);
        //      //$("#"+$requestTicketType+"-counter").html(result);
        // });

        otherData = {
            'container_render_id_1' : "#"+$requestTicketType+"-pagination-div",
            'container_render_id_1_packet' : 'html',
            'container_render_id_2' : "#"+$requestTicketType+"-counter",
            'container_render_id_2_packet' : 'counter',

        };
        $requestObject.initialize(requestData,'renderResultWithPacket',null,otherData);

    }
    
};

var getTicketType = function($value){

    $("#ughi_ticket_panel_container .shelveReportSection").find("a").each(function() {
        var xlsLink =$(this).attr('href');
        if($value=='other'){
            newSrc = xlsLink.replace('ticket_type=shelve','ticket_type='+$value);

        }else{
            newSrc = xlsLink.replace('ticket_type=other','ticket_type='+$value);
        }

        $(this).attr('href', newSrc);
        $('.shelveEmailReportSection a').attr('myval', newSrc);
    });

    $("#ughi_ticket_panel_container .shelvePdfReportSection").find("a").each(function() {
        var xlsLink =$(this).attr('href');
        if($value=='other'){
            newPdfSrc = xlsLink.replace('ticket_type=shelve','ticket_type='+$value);
        }else{
            newPdfSrc = xlsLink.replace('ticket_type=other','ticket_type='+$value);
        }
        $(this).attr('href', newPdfSrc);
    });
};



/**
 * @description Ticket Closing on ticket panel
 * @param filter paraent div id, result container, request type
 * @return filtered html content
 */
    
var changeUGHITicketStatusTo = function($containerId, $requestTicketType,$outletTicketIdArray,$outletTicketArray,$ticketType,$stcSurveyIdList,$closeReason){
    
    var requestData = {};
    var otherData = {};
        $($containerId).html($("#dashbordLoader").html());
       
       if($ticketType=='STC'){
        
         requestData = {
             'requestFor'  : $requestTicketType,
             'requestParam'  :   {
                'outlet_ticket'    :$outletTicketArray,
                'outlet_ticket_id' :$outletTicketIdArray,
                'ticket_type'      :$ticketType,
                 'reason'          :$closeReason,
                'survey_id'        :$stcSurveyIdList
             }   
         };
         
       } else {
        
        requestData = {
             'requestFor'  : $requestTicketType,
             'requestParam'  :   {
                'outlet_ticket'    :$outletTicketArray,
                'shelve_survey_id' :$outletTicketIdArray,
                'ticket_type'      :$ticketType,
                 'reason'          :$closeReason
             }   
        };
      } 
    
      // $.post('request.php', requestData, function(result){
      //     $($containerId).html(result);
      // });

    otherData = {
        'container_render_id' : $containerId
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
};

/**
 * @description To get records form Drop Down pagination
 * @param dropdown object , request type
 * @return filtered html content
 */

    function getPageNumberUghi(page, requestTicketType, container){
            
           var url = '<?php echo $this->getUrl()."/".$this->getBankFolder();?>/request.php';
           
            pageNumber = $("#getPageNumber"+container).val();
            
            if(pageNumber == 1){
               $("#switchPageNumberPrevButton"+container).hide();
               $("#switchPageNumberNextButton"+container).show();  
            }else{
               $("#switchPageNumberPrevButton"+container).show(); 
            }
            
            if(pageNumber == $('#getPageNumber'+container+' option:last-child').val()){
               $("#switchPageNumberNextButton"+container).hide(); 
            }else{
               $("#switchPageNumberNextButton"+container).show(); 
            }
            
            
            result = $("#ticketLoder").html();
            $("#"+container).html(result);
           var otherData = {};
           var requestData = {
               'requestFor' : requestTicketType,
               'pageNumber' : $(page).val(),
               'is-drill-page'    : true,
               'container-id' : container
           };
           
            /* Pagination param carrying logic*/
           if($("#urlParmToForward_"+container)){
                var urlParams = ($("#urlParmToForward_"+container).val());
                if(urlParams){
                    urlParams = JSON.parse(urlParams);
                    requestData = $.extend(requestData, urlParams);
                }
           }
           
           // $.post('request.php', requestData, function(result){
           //
           //              $("#"+container).html(result);
           //      //console.log(requestTicketType);
           //
           // });

            otherData = {
                'container_render_id' : "#"+container
            };
            $requestObject.initialize(requestData,'renderResult',null,otherData);

    }
    
/**
 * @description To get records form Next and Previous Button pagination
 * @param button type (next, previous)) , request type
 * @return filtered html content
 */    
    
    function switchPageNumberUghi(buttonType, requestTicketType, container){
           
           var url = '<?php echo $this->getUrl()."/".$this->getBankFolder();?>/request.php';
            
            if(buttonType == 'next'){
                 pageNumber = $("#getPageNumber"+container).val();
                 pageNumber++;
               $("#switchPageNumberPrevButton"+container).show(); 
            }else{
                
                 pageNumber = $("#getPageNumber"+container).val();
                 pageNumber--;
            } 
            $("#getPageNumber"+container).val(pageNumber);
            
            if(pageNumber == 1){
               $("#switchPageNumberPrevButton"+container).hide();
               $("#switchPageNumberNextButton"+container).show();  
            }
            
            
            if(pageNumber == $('#getPageNumber'+container+' option:last-child').val()){
               $("#switchPageNumberNextButton"+container).hide(); 
            }else{
               $("#switchPageNumberNextButton"+container).show(); 
            }
            //console.log(pageNumber);       
            /*
            if(requestTicketType == 'compliance-tickets'){
                    result = $("#ticketLoder").html();
                    //console.log(result);
                    $("#compliance_ticket_results").html(result);
                    var container = 'compliance_ticket_results';
            }else{
                result = $("#ticketLoder").html();
                $("#stock_ticket_results").html(result);
                var container = 'stock_ticket_results';
            }
            */
            result = $("#ticketLoder").html();
            $("#"+container).html(result);     
           var requestData = {
               'requestFor' : requestTicketType,
               'pageNumber' : pageNumber,
               'is-drill-page'    : true,
               'container-id' : container
                
           };
           
            /* Pagination param carrying logic*/
           if($("#urlParmToForward_"+container)){
                var urlParams = ($("#urlParmToForward_"+container).val());
                if(urlParams){
                    urlParams = JSON.parse(urlParams);
                    requestData = $.extend(requestData, urlParams);
                }
           }
           //$requestObject.initialize(requestData,null,$containerId);
           // $.post('request.php', requestData, function(result){
           //
           //          $("#"+container).html(result);
           //
           //      //console.log(requestTicketType);
           //
           // });

            var otherData = {
                'container_render_id' : "#"+container
            };
            $requestObject.initialize(requestData,'renderResult',null,otherData);

    }
    
var ughiInventory = function($parentElement, $containerId){

    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    $excelValue = '';
for(var index in filterData){

    $excelValue += '&'+index+'='+filterData[index];

}
$("#ughi-inventory-excel-report").attr('href','report.php?type=ughi-inventory-excel-report'+$excelValue);

    var validate = validateUghiFilter(filterData);
    if($containerId == 'excelReport'){
        filterData['isExcel'] = true;
        $requestFor = 'ughi-inventory-excel';
    }else{
        $requestFor = 'ughi-inventory';
    }

    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor'  : $requestFor,
             'filter-data' : filterData,
             'filter-container' : $parentElement
        };
        $requestObject.initialize(requestData,null,$containerId);
    }

    //getCustomFilterByOutletType($('.branch_type_ughi_inventory_container').val(),'ughi_inventory_container');

    return filterData;
};
var ughiCompliance = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
             'requestFor'  : 'ughi-shelve-compliance',
             'filter-data' : filterData
        };
        $requestObject.initialize(requestData,null,$containerId);
    }
    //getCustomFilterByOutletType($('.branch_type_ughi_compliance_container').val(),'ughi_compliance_container');
    return filterData;
};

/**
 * @description Ticket filter on ticket panel
 * @param filter paraent div id, result container, request type
 * @return filtered html content
 */
    
var drillUghiTickets = function(startDate,endDate,requestType, region, containerID, type, colorCode,pdfRequest,branch_type,salesmen_code,merchandiser_name,filterId,chartType){

    var requestData = {};
    var filterData = {};

        filterData['region'] = region;
        filterData['branch_type'] = branch_type;
        filterData['start_date'] = startDate;
        filterData['end_date'] = endDate;
        filterData['salesmenCode'] = salesmen_code;
        filterData['merchandiser_name'] = merchandiser_name;

        if(filterId){
            filterData = ughiFilterData(filterId);

        }

    if(chartType == 'stock'){

        filterData = ughiTotalStockTicketsDataSet;
        filterData['branch_type'] = branch_type;
    }

    else if(chartType == 'compliance'){

        filterData = ughiTotalComplianceTicketsDataSet;
        filterData['branch_type'] = branch_type;
    }

    else if(chartType == 'ageGraph'){

        filterData = ageGraphDataSet;
    }

        requestData = {
             'requestFor'  : 'ughi-ticket-pagination',
             'filter-data' : filterData,
             'color'        : typeof colorCode !== 'undefined' ? colorCode : '',
             'type'        : type,
             'request-type': requestType,
             'is-drill'    : true,
             'container-id' : containerID
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'  : requestType,
        //      'filter-data' : filterData,
        //      'color'        : typeof colorCode !== 'undefined' ? colorCode : '',
        //      'drill-pagination' : result,
        //      'is-drill'    : true,
        //      'container-id' : containerID
        // };
        //
        // if(typeof pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        //
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        var otherData = {
            'requestFor'  : requestType,
            'filter-data' : filterData,
            'color'        : typeof colorCode !== 'undefined' ? colorCode : '',
            'is-drill'    : true,
            'container-id' : containerID
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);

       
};
function drawAgeGraphUghi($parentElement,$containerId){
    $("#flagWiseTicketContainer").html($("#ageWiseTicketLoadingSection").html());
    $("#flagWiseTicketContainerSmallShop").html($("#ageWiseTicketLoadingSectionSmallShop").html());

    var filterData = ughiFilterData($parentElement);

    console.log(filterData);

    $requestData = {
        "requestFor"    :   "graph-data-ughi",
        "graphType"     :   "AGE-GRAPH",
        "graph-filters" :   filterData
    };
    $requestObject.initialize($requestData,"fillAgeGraphUghi",null);
    return filterData;
}

// fill age graph for ughi flags
function fillAgeGraphUghi($data) {
    
    $data = JSON.parse($data);
//    $regionName = $data.regionName;
    $region     = $data.region;
    $greenFlag = flagDataFiller($data.greenFlag);
    $yellowFlag = flagDataFiller($data.yellowFlag);
    $redFlag  = flagDataFiller($data.redFlag);
    $blackFlag  = flagDataFiller($data.blackFlag);
    createAgeGraph($greenFlag,$yellowFlag,$redFlag,$blackFlag,$region);
}
function fillUghiPopup($requestParam){
      $requestObject.initialize($requestParam, null, '#ticketDetailPopup');
 }
 
 $('#ughi_shelve_compliance_container, #ughi_index_shelve_compliance').on("click", ".ughi_shelve_compliance_detail", function(){ 
    $popupObject.setWidth("1000px");
    $popupObject.setHeight("650px");
    $popupObject.initialize(this,'#ticketDetailPopup','getShelveComplianceDetail');
   // $popupObject.setWidth("800px"); 
});

var getShelveComplianceDetail = function($element){
    var requestFor = 'dashboard';
    if($($element).attr('request-for') == 'index'){
        var filterData = {};
        filterData['outlet_code'] = $($element).attr('outlet-code');
        requestFor = 'index';
        
    }else{
        var parentFilterId = $($element).attr('parent-filter');
        var filterData     = ughiFilterData(parentFilterId);
        
    }
    if(requestFor != 'index'){

        filterData = complianceDataSet;
    }

    filterData['product-id']    = $($element).attr('product-id');
    filterData['per-planogram'] = $($element).attr('per-planogram');
    filterData['product-name']  = $($element).attr('product-name');
    filterData['product-type']  = $($element).attr('product-type');
    var requestData = {
        'requestFor'  : 'shelve-compliance-detail',
        'filter-data' :  filterData,
        'request-for':   requestFor
    };
    
    $requestObject.initialize(requestData,null,'#ticketDetailPopup');
};
currentPageValue = 0;

function changeUghiPageNumber($action, $dataPerPage , $currentPage){
    
    
   if($action == 'prev') {
        currentPageValue = Number((currentPageValue) - 5);
        $("#gotoUghiPageNumber").val(currentPageValue).change();
    }
   else if($action == 'next') {
        currentPageValue = Number($("#gotoUghiPageNumber").val()) + 5; 
//         console.log($("#gotoUghiPageNumber").val());
//         console.log(currentPageValue);
        $("#gotoUghiPageNumber").val(Number($("#gotoUghiPageNumber").val()) + 5).change();
    }
    
}

function gotoUghiPageNumber($element){
     currentPageValue = $($element).val();
     var filterElementId = $("#ughi_compliance_detail_result_container").attr("filter-parent");
     //console.log(activetabId);
     
     if($('#gotoUghiPageNumber option:selected').attr('pageClass') == 'last'){
        $('#changeUghiPageNumberNextButton').hide();
        $('#changeUghiPageNumberPrevButton').show();
     }else if($('#gotoUghiPageNumber option:selected').val() == 0){
        $('#changeUghiPageNumberNextButton').show();
        $('#changeUghiPageNumberPrevButton').hide();
     }else{
        $('#changeUghiPageNumberNextButton').show();
        $('#changeUghiPageNumberPrevButton').show();
     }
     $("#ughi_compliance_detail_result_container").html("<tr><td colspan='6'>"+$("#dashbordLoader").html()+"</td></tr>");
     var filterData = ughiFilterData(filterElementId);
     if($("#ughi_compliance_detail_result_container").attr("per-planogram"))
         filterData['per-planogram'] = $("#ughi_compliance_detail_result_container").attr("per-planogram");
     if($("#ughi_compliance_detail_result_container").attr("product-id"))
         filterData['product-id'] = $("#ughi_compliance_detail_result_container").attr("product-id");
         
     var requestData = {
         'requestFor'    : 'shelve-compliance-detail-result',
         'filter-data'   : filterData,
         'page-no'       : currentPageValue
     };
     $requestObject.initialize(requestData,null,'#ughi_compliance_detail_result_container');
}

var drillTotalVisit = function($parentElement, $region, $container,$pdfRequest){
    var requestData = {};
    var filterData = $parentElement;
    var validate = validateUghiFilter(filterData);
    if(validate){
        //alert(validate);
    }else{
        //$($containerId).html($("#dashbordLoader").html());
        
        requestData = {
             'requestFor'  : 'ughi-visit-pagination',
             'filter-data' : filterData,
             'request-region': $region,
             'is-drill'    : true,
             'request-type': 'drill-ughi-total-visit',
             'container-id' : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'  : 'drill-ughi-total-visit',
        //      'filter-data' : filterData,
        //      'is-drill'    : true,
        //      'drill-pagination' : result,
        //      'request-region': $region,
        //      'container-id'  : $container
        // };
        //
        // if(typeof $pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        //
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        var otherData = {
            'requestFor'  : 'drill-ughi-total-visit',
            'filter-data' : filterData,
            'is-drill'    : true,
            'request-region': $region,
            'container-id'  : $container
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);
        
    }
    
};

var drillShelvesPerCategory = function($parentElement, $category, $container,$brand,$pdfRequest){
    var requestData = {};
    var filterData = $parentElement;
    var validate = validateUghiFilter(filterData);
    if(validate){
        //alert(validate);
    }else{
        //$($containerId).html($("#dashbordLoader").html());
        
        requestData = {
             'requestFor'       : 'ughi-visit-pagination',
             'filter-data'      : filterData,
             'request-category' : $category,
             'request-brand'    : $brand,
             'is-drill'         : true,
             'request-type'     : 'drill-ughi-shelves-per-category',
             'container-id'     : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'       : 'drill-ughi-shelves-per-category',
        //      'filter-data'      : filterData,
        //      'is-drill'         : true,
        //      'drill-pagination' : result,
        //      'request-category' : $category,
        //      'request-brand'    : $brand,
        //      'container-id'     : requestData['container-id']
        // };
        //
        // if(typeof $pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        //
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        var otherData = {
            'requestFor'       : 'drill-ughi-shelves-per-category',
            'filter-data'      : filterData,
            'is-drill'         : true,
            'request-category' : $category,
            'request-brand'    : $brand,
            'container-id'     : requestData['container-id']
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);
        
    }
    
};
                                    
var drillTotalOrderPlaced = function($parentElement, $region, $container, profileDetail,$pdfRequest){
    var requestData = {};
    var filterData = $parentElement;
    var validate = validateUghiFilter(filterData);
    
    if(typeof profileDetail == 'undefined'){
        profileDetail = "";
    }
    if(validate){
        //alert(validate);
    }else{
        //$($containerId).html($("#dashbordLoader").html());
        
        requestData = {
             'requestFor'  : 'ughi-visit-pagination',
             'filter-data' : filterData,
             'request-region': $region,
             'request-profile': profileDetail,
             'is-drill'    : true,
             'request-type': 'drill-ughi-order-placed',
             'container-id' : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //          'requestFor'  : 'drill-ughi-order-placed',
        //          'filter-data' : requestData['filter-data'],
        //          'is-drill'    : true,
        //          'drill-pagination' : result,
        //          'request-region': requestData['request-region'],
        //          'request-profile': requestData['request-profile'],
        //          'container-id'  : requestData['container-id']
        //     };
        //
        // if(typeof $pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        //
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        var otherData = {
            'requestFor'  : 'drill-ughi-order-placed',
            'filter-data' : requestData['filter-data'],
            'is-drill'    : true,
            'request-region': requestData['request-region'],
            'request-profile': requestData['request-profile'],
            'container-id'  : requestData['container-id']
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);
        
                                     
        
    }
    
};

var drillOrderToDate = function($parentElement, $category, $container, $month, $pdfRequest){
    var requestData = {};
    var otherData = {};
        var filterData = $parentElement;
        var validate = validateUghiFilter(filterData);

    
    if(validate){
        //alert(validate);
    }else{
        //$($containerId).html($("#dashbordLoader").html());

        requestData = {
             'requestFor'       : 'ughi-order-to-date-pagination',
             'filter-data'      : filterData,
             'request-category' : $category,
             'request-month'    : $month,
             'is-drill'         : true,
             'request-type'     : 'drill-ughi-order-to-date',
             'container-id'     : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'       : 'drill-ughi-order-to-date',
        //      'filter-data'      : filterData,
        //      'is-drill'         : true,
        //      'drill-pagination' : result,
        //      'request-category' : $category,
        //      'request-month'    : $month,
        //      'container-id'     : $container
        //     };
        //
        //      if(typeof $pdfRequest != 'undefined'){
        //          requestData1['pdf-request'] = true;
        //      }
        //
        //     // $popupObject.setWidth("1000px");
        //     // $popupObject.setHeight("650px");
        //     // $popupObject.open('#ticketDetailPopup','fillUghiPopup', requestData1);
        //     // $popupObject.setWidth("800px");
        // });

        otherData = {
            'requestFor'       : 'drill-ughi-order-to-date',
            'filter-data'      : filterData,
            'is-drill'         : true,
            'request-category' : $category,
            'request-month'    : $month,
            'container-id'     : $container
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);
        
    }
};

var renderRequestResult = function(result,otherData) {

    otherData['drill-pagination'] = result;

    $popupObject.setWidth("1000px");
    $popupObject.setHeight("650px");
    $popupObject.open('#ticketDetailPopup', 'fillUghiPopup', otherData);
    $popupObject.setWidth("800px");
}

var drillClosedTicket = function($parentElement, $category, $container,$month,$pdfRequest){
    var requestData = {};
    if($parentElement) {
        var filterData = ughiFilterData($parentElement);
        var validate = validateUghiFilter(filterData);
    }
    if(validate){
        //alert(validate);
    }else{
        //$($containerId).html($("#dashbordLoader").html());

        requestData = {
            'requestFor'       : 'ughi-other-activity-pagination',
            'filter-data'      : filterData,
            'request-status'   : $category,
            'request-region'   : $month,
            'is-drill'         : true,
            'request-type'     : 'drill-ughi-closed-ticket',
            'container-id'     : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //     requestData1 = {
        //         'requestFor'       : 'drill-ughi-closed-ticket',
        //         'filter-data'      : filterData,
        //         'is-drill'         : true,
        //         'drill-pagination' : result,
        //         'request-status'   : $category,
        //         'request-region'   : $month,
        //         'container-id'     : $container
        //     };
        //
        //     if(typeof $pdfRequest != 'undefined'){
        //          requestData1['pdf-request'] = true;
        //     }
        //
        //     $popupObject.setWidth("1000px");
        //     $popupObject.setHeight("650px");
        //     $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        //     $popupObject.setWidth("800px");
        // });

        otherData = {
                    'requestFor'       : 'drill-ughi-closed-ticket',
                    'filter-data'      : filterData,
                    'is-drill'         : true,
                    'request-status'   : $category,
                    'request-region'   : $month,
                    'container-id'     : $container
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);

    }
};

var renderRequestResult = function(result,otherData) {

    otherData['drill-pagination'] = result;

    $popupObject.setWidth("1000px");
    $popupObject.setHeight("650px");
    $popupObject.open('#ticketDetailPopup', 'fillUghiPopup', otherData);
    $popupObject.setWidth("800px");
}

var drillOtherActivity = function($parentElement, $category, $container,$month,$pdfRequest){
    var requestData = {};
    var otherData = {};
    var filterData = $parentElement;
    var validate = validateUghiFilter(filterData);
    if(validate){
        //alert(validate);
    }else{
        //$($containerId).html($("#dashbordLoader").html());
        
        requestData = {
             'requestFor'       : 'ughi-other-activity-pagination',
             'filter-data'      : filterData,
             'request-question' : $category,
             'request-answer'    : $month,
             'is-drill'         : true,
             'request-type'     : 'drill-ughi-other-activity',
             'container-id'     : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'       : 'drill-ughi-other-activity',
        //      'filter-data'      : filterData,
        //      'is-drill'         : true,
        //      'drill-pagination' : result,
        //      'request-question' : $category,
        //      'request-answer'    : $month,
        //      'container-id'     : $container
        // };
        //
        // if(typeof $pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        //
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        otherData = {
            'requestFor'       : 'drill-ughi-other-activity',
            'filter-data'      : filterData,
            'is-drill'         : true,
            'request-question' : $category,
            'request-answer'    : $month,
            'container-id'     : $container
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);
        
    }
};

var drillAsPerPlanogram = function($parentElement, $brand, $container,$type,$pdfRequest){
    var requestData = {};
    var filterData = $parentElement;
    var validate = validateUghiFilter(filterData);
    if(validate){
        //alert(validate);
    }else{
        //$($containerId).html($("#dashbordLoader").html());
        
        requestData = {
             'requestFor'       : 'ughi-as-per-planogram-pagination',
             'filter-data'      : filterData,
             'request-brand'    : $brand,
             'request-answer'     : $type,
             'is-drill'         : true,
             'request-type'     : 'drill-ughi-as-per-planogram',
             'container-id'     : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'       : 'drill-ughi-as-per-planogram',
        //      'filter-data'      : filterData,
        //      'is-drill'         : true,
        //      'drill-pagination' : result,
        //      'request-brand'    : $brand,
        //      'request-answer'     : $type,
        //      'container-id'     : $container
        // };
        //
        // if(typeof $pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        otherData = {
            'requestFor'       : 'drill-ughi-as-per-planogram',
            'filter-data'      : filterData,
            'is-drill'         : true,
            'request-brand'    : $brand,
            'request-answer'     : $type,
            'container-id'     : $container
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);
        
    }
};


var drillInventory = function($brand, $skuNumber, $container, $filterContainer, $outletCode,$pdfRequest,$isProfilePage){
    var requestData = {};
    var otherData = {};

        var filterData = ughiFilterData($filterContainer);

        if(!$isProfilePage){

            filterData = ughiInventoryDataSet;
        }

        var validate = validateUghiFilter(filterData);
        
        if(validate){
            alert(validate);
        }else{
           requestData = {
             'requestFor'  : 'ughi-inventory-pagination',
             'request-brand' : $brand,
             'filter-data' : filterData,
             'request-skuNumber': $skuNumber,
             'request-outlet'   : $outletCode,
             'is-drill'    : true,
             'request-type': 'drill-ughi-inventory',
             'container-id' : $container,
             'isProfilePage' : $isProfilePage
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'  : 'drill-ughi-inventory',
        //      'request-brand' : $brand,
        //      'filter-data' : filterData,
        //      'request-outlet'   : $outletCode,
        //      'is-drill'    : true,
        //      'drill-pagination' : result,
        //      'request-skuNumber': $skuNumber,
        //      'container-id'  : $container,
        //      'isProfilePage' : $isProfilePage
        // };
        //
        // if(typeof $pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        //
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        otherData = {
                 'requestFor'  : 'drill-ughi-inventory',
                 'request-brand' : $brand,
                 'filter-data' : filterData,
                 'request-outlet'   : $outletCode,
                 'is-drill'    : true,
                 'request-skuNumber': $skuNumber,
                 'container-id'  : $container,
                 'isProfilePage' : $isProfilePage
        };
        
        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);

        }
        
        
};
//function to show the merchandiser remaing target list of outlets
    var showMrchRemainTargets = function($merchandiser,$monthYearFilter, $container,$pdfRequest) {
     var requestData = {};
     var otherData = {};

           requestData = {
             'requestFor'  : 'mrch-remains-target-pagination',
             'request-merch' : $merchandiser,
             'monthYearFilter': $monthYearFilter,
             'is-drill'    : true,
             'request-type': 'drill-mrch-remains-target',
             'container-id' : $container
        };

        // $.post('request.php', requestData, function(result){
        //
        //      requestData1 = {
        //      'requestFor'  : 'drill-mrch-remains-target',
        //      'request-merch' : $merchandiser,
        //      'monthYearFilter': $monthYearFilter,
        //      'is-drill'    : true,
        //      'panel'        : 'remaining-target',
        //      'drill-pagination' : result,
        //      'container-id'  : $container
        // };
        //
        // if(typeof $pdfRequest != 'undefined'){
        //     requestData1['pdf-request'] = true;
        // }
        // $popupObject.setWidth("1000px");
        // $popupObject.setHeight("650px");
        // $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        // $popupObject.setWidth("800px");
        // });

        var otherData = {
            'requestFor'  : 'drill-mrch-remains-target',
            'request-merch' : $merchandiser,
            'monthYearFilter': $monthYearFilter,
            'is-drill'    : true,
            'panel'        : 'remaining-target',
            'container-id'  : $container
        };

        if(typeof $pdfRequest != 'undefined'){
            otherData['pdf-request'] = true;
        }

        $requestObject.initialize(requestData,'renderRequestResult',null,otherData);
      
};
// function for showing the unauthorized survyes
var showMrchUnAuthSurveys = function($merchandiser,$monthYearFilter, $container) {
     var requestData = {};
            
     requestData = {
             'requestFor'  : 'drill-mrch-remains-target',
             'request-merch' : $merchandiser,
             'is-drill'    : true,
             'panel'        : 'an-auth-survey',
             'monthYearFilter' : $monthYearFilter,
             'container-id'  : $container
        };
        
        $popupObject.setWidth("1000px");
        $popupObject.setHeight("650px");
        $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData);
        $popupObject.setWidth("800px");                             
       
};

//function to show the total survey done by merchandisers
var showMrchTotalSurveys = function($merchandiser,$monthYearFilter, $container,$outletCode) {
        requestData1 = {
             'requestFor'  : 'drill-mrch-remains-target',
             'request-merch' : $merchandiser,
             'monthYearFilter': $monthYearFilter,
             'outlet_code'  : $outletCode,
             'is-drill'    : true,
             'panel'        : 'total-survey',
             'container-id'  : $container
        };
        
        $popupObject.setWidth("1000px");
        $popupObject.setHeight("650px");
        $popupObject.open('#ticketDetailPopup','fillUghiPopup',requestData1);
        $popupObject.setWidth("800px"); 
    
};

var getMerchendiserName = function($outlet, $merchendiser){
    var requestData = {};
    var otherData = {};
    outletName = $("#outlet_name").val();
           
        $('#myUGHILoader').css({
            height: $('#myUGHILoader').parent().height(), 
            width: $('#myUGHILoader').parent().width()
        });
        $('#myUGHILoader').show();
    
        requestData = {
             'requestFor' : 'get-merchandiser-name',
             'filter-data' : outletName
        };

        // $.post('request.php', requestData, function(result){
        //
        //   $('#myUGHILoader').hide();
        //   $("#outlet_merchandiser").html(result);
        // });

    otherData = {
        'container_render_id' : '#outlet_merchandiser',
        'container_hide_id'      : '#myUGHILoader'
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
   
};


var getCustomCode = function(){
    
    $('#myUGHILoader').css({
        height: $('#myUGHILoader').parent().height(), 
        width: $('#myUGHILoader').parent().width()
    });
    
    $('#myUGHILoader').show();
   

    var requestData = {};
   
    outletType = $("#outlet_type").val();
    $('#outlet_name_image').css('display','inline');
    requestData = {
         'requestFor' : 'get-custom-code',
         'filter-data' : outletType
    };

    // $.post('request.php', requestData, function(result){
    //    //$('#outlet_name_image').css('display','none');
    //    $('#myUGHILoader').hide();
    //    $("#checus_cust").html(result);
    // });

    var otherData = {
        'container_render_id' : "#checus_cust",
        'container_hide_id' : "#myUGHILoader"
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
    
};


var getOutletName = function(){
    var requestData = {};
    outletName = $("#checus_cust").val();
       
    $('#myUGHILoader').css({
        height: $('#myUGHILoader').parent().height(), 
        width: $('#myUGHILoader').parent().width()
    });
    $('#myUGHILoader').show();

    requestData = {
         'requestFor' : 'get-outlet-name',
         'filter-data' : outletName
    };

    // $.post('request.php', requestData, function(result){
    //    $('#myUGHILoader').hide();
    //   $("#outlet_name").html(result);
    // });

    var otherData = {
        'container_render_id' : "#outlet_name",
        'container_hide_id' : "#myUGHILoader"
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
};


var getOutletNameForMerchandiser = function(){
    var requestData = {};
    var otherData = {};
    outletName = $("#merchandiser_name").val();
       
    $('#myUghiTotalVisitLoader').css({
        //height: $('#myUghiTotalVisitLoader').parent().height(), 
        //width: $('#myUghiTotalVisitLoader').parent().width()
        height: 86,
        width: 613
    });
    $('#myUghiTotalVisitLoader').show();

    requestData = {
         'requestFor' : 'get-ughi-outlet-name-total-visit',
         'filter-data' : outletName
    };

    // $.post('request.php', requestData, function(result){
    //    $('#myUghiTotalVisitLoader').hide();
    //   $("#outlet_name_ughi").html(result);
    // });

    otherData = {
        'container_render_id' : '#outlet_name_ughi',
        'container_hide_id'      : '#myUghiTotalVisitLoader'
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
};

var getOutletNameForMerchandiserWorkForce = function(){
    var requestData = {};
    var otherData = {};
    outletName = $("#merchandiser_name_workforce").val();

    $('#myUghiTotalVisitLoader').css({
        //height: $('#myUghiTotalVisitLoader').parent().height(),
        //width: $('#myUghiTotalVisitLoader').parent().width()
        height: 86,
        width: 613
    });
    $('#myUghiTotalVisitLoader').show();

    requestData = {
        'requestFor' : 'get-ughi-outlet-name-total-visit',
        'filter-data' : outletName
    };

    // $.post('request.php', requestData, function(result){
    //     $('#myUghiTotalVisitLoader').hide();
    //     $("#outlet_code_workforce").html(result);
    // });


    otherData = {
        'container_render_id' : '#outlet_code_workforce',
        'container_hide_id'      : '#myUghiTotalVisitLoader'
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);


};


var getUghiBrandName = function(){

    var requestData = {};
    var otherData = {};

    sku = $("#brand_id").val();

    $('#myUghiSKUQuantity').css({
        //height: $('#myUghiSKUQuantity').parent().height(), 
        //width: $('#myUghiSKUQuantity').parent().width()
        height: 86,
        width: 613
    });

    $('#myUghiSKUQuantity').show();

    requestData = {
         'requestFor' : 'get-ughi-sku-code-quantity-missing',
         'filter-data' : sku
    };

    // $.post('request.php', requestData, function(result){
    //     //console.log(result);
    //    $('#myUghiSKUQuantity').hide();
    //   $("#sku_ughi").html(result);
    // });

    otherData = {
        'container_render_id' : '#sku_ughi',
        'container_hide_id'      : '#myUghiSKUQuantity'
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);

};

var sortUghiTickets = function($orderField, $orderType, $requestFor){

          $containerId   = "stock_ticket_results";
          $parentElement = "#stock_ticket_filters";
          $selectedPage  = "#getPageNumberstock_ticket_results";

          if($requestFor == 'compliance-tickets'){
              console.log('here');
              $containerId   = "compliance_ticket_results";
              $parentElement = "#compliance_ticket_filters";
              $selectedPage  = "#getPageNumbercompliance_ticket_results";
          }

            if($requestFor == 'compliance-nl-tickets'){
                $containerId   = "compliance_NL_ticket_results";
                $parentElement = "#compliance_NL_ticket_filters";
                $selectedPage  = "#getPageNumbercompliance_NL_ticket_results";
            }

          var filterData = ughiFilterData($parentElement);  
          
          var requestData = {
               'requestFor'  : $requestFor,
               'filter-data' : filterData,
               'order-field' : $orderField,
               'order-type'  : $orderType,
               'pageNumber'  : $($selectedPage).val()
          };

          $(".ughi_ticket_filters").html('<img title="Loading" src="http://localhost/4c-dashboard/public/img/ajax-loaders/bsfmap-loader1.gif" style="max-height: 60px;width: 64px;">');
          $requestObject.initialize(requestData,fillTicketPanelData,'#'+$containerId);
           
};

var fillTicketPanelData = function(){
     $(".ughi_ticket_filters").html('');
};
var getReportMerchandisers = function(type) {
     var requestData = {};
     var otherData = {};
    outletType = $("#"+type+"-outlet-type-selector").val();
//      $('#myUGHILoader-'+type).css({
//            height: $('#filters').height(), 
//            width: '100%',
//            display: 'block'
//        });
//    
    requestData = {
             'requestFor' : 'get-merchandiser-name',
             'penal'      : 'reports',
             'outletType' : outletType
        };

        // $.post('request.php', requestData, function(result){
        //   $("#"+type+"-visit-type-selector").html(result);
        // });

    otherData = {
        'container_render_id' : "#"+type+"-visit-type-selector"
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);

        requestData = {
             'requestFor' : 'get-outletcode-with-name',
             'outletType' : outletType
        };

//         $.post('request.php', requestData, function(result){
// //          $('#myUGHILoader-'+type).css('display','none');
//           $("#"+type+"-outlet_name_reports").html(result);
//         });

    otherData = {
        'container_render_id' : "#"+type+"-outlet_name_reports"
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);

};

var getReportBranchTypes = function(type) {
    var requestData = {};
    var otherData = {};
    outletType = $("#"+type+"-branch-type-selector").val();
//      $('#myUGHILoader-'+type).css({
//            height: $('#filters').height(),
//            width: '100%',
//            display: 'block'
//        });
//
    requestData = {
        'requestFor' : 'get-merchandiser-name',
        'penal'      : 'reports',
        'outletType' : outletType
    };

    // $.post('request.php', requestData, function(result){
    //     $("#"+type+"-visit-type-selector").html(result);
    // });

    otherData = {
        'container_render_id' : "#"+type+"-visit-type-selector"
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
    requestData = {
        'requestFor' : 'get-outletcode-with-name',
        'outletType' : outletType
    };

//     $.post('request.php', requestData, function(result){
// //          $('#myUGHILoader-'+type).css('display','none');
//         $("#"+type+"-outlet_name_reports").html(result);
//     });

    otherData = {
        'container_render_id' : "#"+type+"-outlet_name_reports",
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);

};

var getReportOutletCodes = function(type){
    var requestData = {};
    var otherData = {};
     merchandiserCode = $("#"+type+"-visit-type-selector").val();
//      $('#myUGHILoader-'+type).css({
//            height: $('#filters').height(), 
//            width: '100%',
//            display: 'block'
//        });
         requestData = {
             'requestFor' : 'get-outletcode-with-name',
             'penal'      : 'reports',
             'merchandiserCode' : merchandiserCode
        };

//         $.post('request.php', requestData, function(result){
// //          $('#myUGHILoader-'+type).css('display','none');
//           $("#"+type+"-outlet_name_reports").html(result);
//         });

    otherData = {
        'container_render_id' : "#"+type+"-outlet_name_reports"
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
};

var getCustomFilterByOutletType = function(outletType,section){
   var requestData = {};
    var otherData = {};
        $('#myUGHILoader').css({
            height: $('#myUGHILoader').parent().height(), 
            width: $('#myUGHILoader').parent().width()
        });
        $('#myUGHILoader').show();
    
        requestData = {
             'requestFor' : 'get-merchandiser-name',
             'penal'      : 'map',
             'outletType' : outletType
        };

        // $.post('request.php', requestData, function(result){
        //
        //     $(".outlet_merchandiser_"+section).html(result);
        // });
    otherData = {
        'container_render_id' : '.outlet_merchandiser_'+section
    };
        $requestObject.initialize(requestData,'renderResult',null,otherData);

        requestData = {
             'requestFor' : 'get-outletcode-with-name',
             'outletType' : outletType
        };

        // $.post('request.php', requestData, function(result){
        //
        //   $('#myUGHILoader').hide();
        //   $("#outlet_name").html(result);
        // });

    if(section == 'wf_performance') {

        id_outlet_name = '#outlet_code_workforce';
    } else {

        id_outlet_name = '#outlet_name';
    }

        otherData = {
            'container_render_id' : id_outlet_name,
            'container_hide_id'      : '#myUGHILoader'
        };
        $requestObject.initialize(requestData,'renderResult',null,otherData);


    if(section == 'total_stock') {
        requestData = {
            'requestFor': 'get-salesmen-name',
            'outletType': outletType
        };

        // $.post('request.php', requestData, function (result) {
        //
        //     $(".salesmen_code_total_stock").html(result);
        // });
        otherData = {
            'container_render_id' : '.salesmen_code_total_stock'
        };
        $requestObject.initialize(requestData,'renderResult',null,otherData);
    }


};


var renderResult = function(result,idArray) {

    if(idArray['container_render_id'] && idArray['container_render_id'] != undefined) {
        $(idArray['container_render_id']).html(result);
    }

    if(idArray['container_hide_id'] && idArray['container_hide_id'] != undefined){
        $(idArray['container_hide_id']).hide();
    }
}

var renderResultWithPacket = function(result,idArray) {

    result = JSON.parse(result);
    if(idArray['container_render_id_1'] && idArray['container_render_id_1'] != undefined) {
        $(idArray['container_render_id_1']).html(result['html']);
    }

    if(idArray['container_render_id_2'] && idArray['container_render_id_2'] != undefined) {
        $(idArray['container_render_id_2']).html(result['counter']);
    }
}

var getWorkForceMerchandisers = function(outletType,section) {
    var requestData = {};
    var otherData = {};
    $('#myUGHILoader').css({
        height: $('#myUGHILoader').parent().height(),
        width: $('#myUGHILoader').parent().width()
    });
    $('#myUGHILoader').show();

    requestData = {
        'requestFor': 'get-merchandiser-name',
        'penal': 'map',
        'outletType': outletType
    };

    // $.post('request.php', requestData, function (result) {
    //
    //     $("."+section).html(result);
    // });


    otherData = {
        'container_render_id' : "."+section
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);

};

var getOutletTypeByRegion = function(type){

    var requestData = {};
    var otherData= {};
    region = $("#"+type+"-branch-type-selector").val();

    $('#myUGHILoader').css({
        height: $('#myUGHILoader').parent().height(),
        width: $('#myUGHILoader').parent().width()
    });
    $('#myUGHILoader').show();

    requestData = {
        'requestFor' : 'get-outlet-type-ughi',
        'penal'      : 'map',
        'region' : region
    };
    // $.post('request.php', requestData, function(result){
    //
    //     $('#myUGHILoader').hide();
    //     $("#" + type +"-outlet-type-selector").html(result);
    // });

    otherData = {
        'container_render_id' : "#" + type +"-outlet-type-selector",
        'container_hide_id' : "#myUGHILoader"
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);

    getCustomFilterByOutletType();
};
//
//var getUghiBranchTypeByRegion = function(){
//
//    var requestData = {};
//    region = $("#region_ughi").val();
//
//    $('#myUGHILoader').css({
//        height: $('#myUGHILoader').parent().height(),
//        width: $('#myUGHILoader').parent().width()
//    });
//    $('#myUGHILoader').show();
//
//    requestData = {
//        'requestFor' : 'get-outlet-type-ughi',
//        'region' : region
//    };
//
//    $.post('request.php', requestData, function(result){
//
//        $('#myUGHILoader').hide();
//        $("#branch_type").html(result);
//    });
//}

var getOutletCodeWithName = function(){
    var requestData = {};
    var otherData = {};
    merchendiserName = $("#outlet_merchandiser").val();
           
        $('#myUGHILoader').css({
            height: $('#myUGHILoader').parent().height(), 
            width: $('#myUGHILoader').parent().width()
        });
        $('#myUGHILoader').show();
    
        requestData = {
             'requestFor' : 'get-outletcode-with-name',
             'filter-data' : merchendiserName
        };

        // $.post('request.php', requestData, function(result){
        //
        //   $('#myUGHILoader').hide();
        //   $("#outlet_name").html(result);
        // });

    otherData = {
        'container_render_id' : '#outlet_name',
        'container_hide_id'      : '#myUGHILoader'
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);
};

function downloadUghiExcelReport(){
    
    var requestedData = JSON.parse($("#requestData_fordownload").val());
    requestedData['excel-download'] = true;
    $("#download-button-excel").attr('href', baseUrl+''+baseBank+'/request.php?' + $.param(requestedData));
    var target=$("#download-button-excel");
    window.open(target.attr('href'));
}

function downloadUghiTicketExcelReport($parentElement,$parentElement2){

    var name = $("#"+$parentElement).attr("name");
    console.log(filterData2);
    //
    //$("#download-button-excel").attr('href', baseUrl+''+baseBank+'/request.php?' + $.param(requestedData));
    //var target=$("#download-button-excel");
    //window.open(target.attr('href'));
}

function downloadUghiExcelReport1(){
    /* var requestedData = JSON.parse($("#requestData_fordownload").val());  
     requestedData['excel-download'] = true;
     $.post(baseUrl+''+baseBank+'/request.php', requestedData,
                function($result){
                   console.log($result);
                }
      );
    
    
    return;*/

    var myForm = document.createElement("form");
    myForm.method = 'post';
    myForm.action = baseUrl+''+baseBank+'/request.php';
    
    
    myForm = preapreFormData(myForm, requestedData);
        
    
    
    document.body.appendChild(myForm);
    myForm.submit();
    document.body.removeChild(myForm); 
 }
 
 
 function preapreFormData(myForm, requestedData, fieldName){
     for(var index in requestedData){
              
        if(typeof requestedData[index] == 'Object'){
          myForm = preapreFormData(myForm, requestedData[index], index)  
        }else{
           var bankInput = document.createElement("input");
        
           if(typeof fieldName != 'undefined'){
             bankInput.setAttribute("name", fieldName +"["+index+"]");
           }else{
                bankInput.setAttribute("name", index);
           }
           bankInput.setAttribute("value", requestedData[index]);
           bankInput.setAttribute("type", "hidden");
           myForm.appendChild(bankInput);
        }
        
    }
    return myForm;
 }
 
function changeUghiCheckbokStatus() {
    
    var value = $('#select_all_stock_ticket_checkbox').prop("checked");
    if(value) {
        $('.stock_ticket_panel_checkbox').prop("checked", true);
    } else {
        $('.stock_ticket_panel_checkbox').prop("checked", false);
    }
    $('.stock_ticket_panel_checkbox').trigger("onchange");
    
}

function changeUghiComplianceCheckbokStatus() {
    
    
    var value = $('#select_all_compliance_ticket_checkbox').prop("checked");
    if(value) {
        $('.compliance_ticket_panel_checkbox').prop("checked", true);
    } else {
        $('.compliance_ticket_panel_checkbox').prop("checked", false);
    }
    $('.compliance_ticket_panel_checkbox').trigger("onchange");
    
}

function changeUghiComplianceUlCheckbokStatus() {

    

    var value = $('#select_all_compliance_nl_ticket_checkbox').prop("checked");
    if(value) {
        $('.compliance_li_ticket_panel_checkbox').prop("checked", true);
    } else {
        $('.compliance_li_ticket_panel_checkbox').prop("checked", false);
    }
    $('.compliance_li_ticket_panel_checkbox').trigger("onchange");

}

function activeUghiStockActionButtons(){

    $ticketList = getUghiSelectedTickets('current-status');

    actionUghiButtonVisibilityControl($ticketList);

}

function activeUghiComplianceActionButtons(){

    $ticketList = getUghiComplianceSelectedTickets('current-comstatus');
    actionUghiCompButtonVisibilityControl($ticketList);

}

function activeUghiComplianceNlActionButtons(){
    $ticketList = getUghiComplianceNlSelectedTickets('current-ulcomstatus');

    actionUghiCompNlButtonVisibilityControl($ticketList);
    

}

function getUghiSelectedTickets($attribute){
    $ticketList = [];
    $("#ticketingBody").find("input.stock_ticket_panel_checkbox").each(function(){
        if($(this).is(":checked")){
            $ticketList.push($(this).data($attribute));
        }
    });
    return $ticketList;
}

function getUghiComplianceSelectedTickets($attribute){
    $ticketList = [];
    
    $("#ticketBody").find("input.compliance_ticket_panel_checkbox").each(function(){
        
        if($(this).is(":checked")){
            $ticketList.push($(this).data($attribute));
        }
    });
    return $ticketList;
}

function getUghiComplianceNlSelectedTickets($attribute){
    $ticketList = [];
    console.log('reached here');
    $("#ticketNlBody").find("input.compliance_li_ticket_panel_checkbox").each(function(){
        console.log('Fine here');
        if($(this).is(":checked")){
            $ticketList.push($(this).data($attribute));
        }
    });
    return $ticketList;
}

function actionUghiButtonVisibilityControl($ticketStatusList){
       
    $buttonList =   getUghiActionButtons();

    $buttonVisibilityFlag = {};
    for(var buttonListIndex in $buttonList){
        $buttonVisibilityFlag[buttonListIndex] = true;
    }
    
    activateButton(['closeButton'],$buttonVisibilityFlag,'');

    activateButton(['reasonButton'],$buttonVisibilityFlag,'');

     if(!($ticketStatusList.length === 0)){
        for(var ticketListIndex in $ticketStatusList){
            
            switch($ticketList[ticketListIndex].toUpperCase()){
                case "PENDING"          : $buttonVisibilityFlag = activateButton(['reasonButton','closeButton'],$buttonVisibilityFlag,'activate');
                                          $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                                          break;  
                                          
//                case "RESOLVED"          : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
//                                          $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
//                                          break;
//                                          
//                case "WAITING TRANSFER" : $buttonVisibilityFlag = activateButton(['closeButton','approvalButton','rejectButton'],$buttonVisibilityFlag,'activate');
//                                          $buttonVisibilityFlag = activateButton(['resolveButton','transferButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
//                                          break;
//                                          
//                case "TRANSFER APPROVED" : $buttonVisibilityFlag = activateButton(['closeButton','resolveButton','transferButton'],$buttonVisibilityFlag,'activate');
//                                           $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
//                                           break;
            } 
        }
     }
}

function actionUghiCompButtonVisibilityControl($ticketStatusList){
    
    $buttonList =   getUghiCompActionButtons();

    $buttonVisibilityFlag = {};
    for(var buttonListIndex in $buttonList){
        $buttonVisibilityFlag[buttonListIndex] = true;
    }

    activateCompButton(['closeButton'],$buttonVisibilityFlag,'');
    activateCompButton(['reasonButton'],$buttonVisibilityFlag,'');
     if(!($ticketStatusList.length === 0)){
        for(var ticketListIndex in $ticketStatusList){
            
            switch($ticketList[ticketListIndex].toUpperCase()){
                case "PENDING"          : $buttonVisibilityFlag = activateCompButton(['reasonButton','closeButton'],$buttonVisibilityFlag,'activate');
                                          $buttonVisibilityFlag = activateCompButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                                          break;  
                                          
//                case "RESOLVED"          : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
//                                          $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
//                                          break;
//                                          
//                case "WAITING TRANSFER" : $buttonVisibilityFlag = activateButton(['closeButton','approvalButton','rejectButton'],$buttonVisibilityFlag,'activate');
//                                          $buttonVisibilityFlag = activateButton(['resolveButton','transferButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
//                                          break;
//                                          
//                case "TRANSFER APPROVED" : $buttonVisibilityFlag = activateButton(['closeButton','resolveButton','transferButton'],$buttonVisibilityFlag,'activate');
//                                           $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
//                                           break;
            } 
        }
     }
}

function actionUghiCompNlButtonVisibilityControl($ticketStatusList){

    $buttonList =   getUghiCompNlActionButtons();

    $buttonVisibilityFlag = {};
    for(var buttonListIndex in $buttonList){
        $buttonVisibilityFlag[buttonListIndex] = true;
    }

    activateCompNlButton(['closeButton'],$buttonVisibilityFlag,'');
    activateCompNlButton(['reasonButton'],$buttonVisibilityFlag,'');

    if(!($ticketStatusList.length === 0)){
        for(var ticketListIndex in $ticketStatusList){

            switch($ticketList[ticketListIndex].toUpperCase()){
                case "PENDING"          : $buttonVisibilityFlag = activateCompNlButton(['reasonButton','closeButton'],$buttonVisibilityFlag,'activate');
                                            $buttonVisibilityFlag = activateCompNlButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                                            break;

//                case "RESOLVED"          : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
//                                          $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
//                                          break;
//
//                case "WAITING TRANSFER" : $buttonVisibilityFlag = activateButton(['closeButton','approvalButton','rejectButton'],$buttonVisibilityFlag,'activate');
//                                          $buttonVisibilityFlag = activateButton(['resolveButton','transferButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
//                                          break;
//
//                case "TRANSFER APPROVED" : $buttonVisibilityFlag = activateButton(['closeButton','resolveButton','transferButton'],$buttonVisibilityFlag,'activate');
//                                           $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
//                                           break;
            }
        }
    }
}

function activateButton($buttonList,$buttonVisibilityFlag,$action){
    $availableButtonList =   getUghiActionButtons();

    $classToAdd = 'btn btn-disabled';
    if($action == 'activate'){
      $classToAdd = 'btn btn-success';
   }
    
    for($buttonListIndex in $buttonList){
        if($buttonVisibilityFlag[ $buttonList[$buttonListIndex] ]){
            if($availableButtonList[ $buttonList[$buttonListIndex] ]){
               if(!$availableButtonList.reasonButton) {
                   $($availableButtonList[$buttonList[$buttonListIndex]]).attr('class', $classToAdd);
               }

               $("#ticket_close_div").find('a').each(function(){
                      if($(this).attr('status')=='Close'){
                         $(this).attr('class', 'btn btn-danger')
                      }
               });
     
               /* Change the button status */
               if($buttonList[$buttonListIndex] == 'transferButton'){
               
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).unbind('click');
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class',$($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class')+" btn-dashboard");
                    activateTransferButton();
               }
               if($action == 'activate'){
                   $($availableButtonList[ $buttonList[$buttonListIndex] ]).show(); 
               }else{
                   $($availableButtonList[ $buttonList[$buttonListIndex] ]).hide(); 
               }
            }
        }
        
        if($action == 'deactivate'){
            $buttonVisibilityFlag[ $buttonList[$buttonListIndex] ] = false;
        }
    }
    return $buttonVisibilityFlag;
}

function activateCompButton($buttonList,$buttonVisibilityFlag,$action){
    $availableButtonList =   getUghiCompActionButtons();

    $classToAdd = 'btn btn-disabled';
    if($action == 'activate'){
      $classToAdd = 'btn btn-success';
   }
    
    for($buttonListIndex in $buttonList){
       
        if($buttonVisibilityFlag[ $buttonList[$buttonListIndex] ]){
            if($availableButtonList[ $buttonList[$buttonListIndex] ]){
                  if(!$availableButtonList.reasonButton) {
                      $($availableButtonList[$buttonList[$buttonListIndex]]).attr('class', $classToAdd);
                  }
                    $("#ticket_shelve_div").find('a').each(function(){
                      if($(this).attr('status')=='Close'){
                         $(this).attr('class', 'btn btn-danger')
                      }

                    });
     
               /* Change the button status */
               if($buttonList[$buttonListIndex] == 'transferButton'){
               
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).unbind('click');
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class',$($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class')+" btn-dashboard");
                    activateTransferButton();
               }
               if($action == 'activate'){
                   $($availableButtonList[ $buttonList[$buttonListIndex] ]).show(); 
               }else{
                   $($availableButtonList[ $buttonList[$buttonListIndex] ]).hide(); 
               }
            }
        }
        
        if($action == 'deactivate'){
            $buttonVisibilityFlag[ $buttonList[$buttonListIndex] ] = false;
        }
    }
    return $buttonVisibilityFlag;
}


function activateCompNlButton($buttonList,$buttonVisibilityFlag,$action){

    $availableButtonList =   getUghiCompNlActionButtons();

    $classToAdd = 'btn btn-disabled';
    if($action == 'activate'){

        $classToAdd = 'btn btn-success';
    }

    for($buttonListIndex in $buttonList){

        if($buttonVisibilityFlag[ $buttonList[$buttonListIndex] ]){
            if($availableButtonList[ $buttonList[$buttonListIndex] ]){
                if(!$availableButtonList.reasonButton) {
                    $($availableButtonList[$buttonList[$buttonListIndex]]).attr('class', $classToAdd);
                }

                $("#ticket_shelve_nl_div").find('a').each(function(){
                    if($(this).attr('status')=='Close'){
                        $(this).attr('class', 'btn btn-danger')
                    }
                });

                /* Change the button status */
                if($buttonList[$buttonListIndex] == 'transferButton'){

                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).unbind('click');
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class',$($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class')+" btn-dashboard");
                    activateTransferButton();
                }
                if($action == 'activate'){
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).show();
                }else{
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).hide();
                }
            }
        }

        if($action == 'deactivate'){
            $buttonVisibilityFlag[ $buttonList[$buttonListIndex] ] = false;
        }
    }
    return $buttonVisibilityFlag;
}
 
function getUghiActionButtons(){
    $buttonList =   {
        "closeButton"   :   0,
        "reasonButton"  :   0,
//        "resolveButton" :   0,
//        "reopenButton"  :   0,
//        "approvalButton":   0,
//        "rejectButton"  :   0,
//        "transferButton" : 0
    };
    $("#ticket_close_div").find('a').each(function(){
          switch($(this).attr('status')){
            case "Close"       :   $buttonList.closeButton = this; break;
//            case "Resolve"      :   $buttonList.resolveButton = this; break;
//            case "Pending"      :   $buttonList.reopenButton = this; break;
//            case "Reject"     :   $buttonList.rejectButton = this; break;
//            case "Approve"     :   $buttonList.approvalButton = this; break;
//            case "Transfer"     : $buttonList.transferButton = this; break;
          }
    });

    $("#ticket_close_div").find('input').each(function(){
        switch($(this).attr('status')){
            case "CloseReason"       :   $buttonList.reasonButton = this; break;
//            case "Resolve"      :   $buttonList.resolveButton = this; break;
//            case "Pending"      :   $buttonList.reopenButton = this; break;
//            case "Reject"     :   $buttonList.rejectButton = this; break;
//            case "Approve"     :   $buttonList.approvalButton = this; break;
//            case "Transfer"     : $buttonList.transferButton = this; break;
        }
    });

    return $buttonList;
}

function getUghiCompActionButtons(){
    $buttonList =   {
        "closeButton"   :   0,
        "reasonButton"   :   0

//        "resolveButton" :   0,
//        "reopenButton"  :   0,
//        "approvalButton":   0,
//        "rejectButton"  :   0,
//        "transferButton" : 0
    };
    $("#ticket_shelve_div").find('a').each(function(){
          switch($(this).attr('status')){
            case "Close"       :   $buttonList.closeButton = this; break;
//            case "Resolve"      :   $buttonList.resolveButton = this; break;
//            case "Pending"      :   $buttonList.reopenButton = this; break;
//            case "Reject"     :   $buttonList.rejectButton = this; break;
//            case "Approve"     :   $buttonList.approvalButton = this; break;
//            case "Transfer"     : $buttonList.transferButton = this; break;
          }
    });

    $("#ticket_shelve_div").find('input').each(function(){
        switch($(this).attr('status')){
            case "CloseReason"       :   $buttonList.reasonButton = this; break;
//            case "Resolve"      :   $buttonList.resolveButton = this; break;
//            case "Pending"      :   $buttonList.reopenButton = this; break;
//            case "Reject"     :   $buttonList.rejectButton = this; break;
//            case "Approve"     :   $buttonList.approvalButton = this; break;
//            case "Transfer"     : $buttonList.transferButton = this; break;
        }

    });


    return $buttonList;
}


function getUghiCompNlActionButtons(){
    $buttonList =   {
        "closeButton"   :   0,
        "reasonButton"   :   0
//        "resolveButton" :   0,
//        "reopenButton"  :   0,
//        "approvalButton":   0,
//        "rejectButton"  :   0,
//        "transferButton" : 0
    };

    $("#ticket_shelve_nl_div").find('a').each(function(){
        switch($(this).attr('status')){
            case "Close"       :   $buttonList.closeButton = this; break;
//            case "Resolve"      :   $buttonList.resolveButton = this; break;
//            case "Pending"      :   $buttonList.reopenButton = this; break;
//            case "Reject"     :   $buttonList.rejectButton = this; break;
//            case "Approve"     :   $buttonList.approvalButton = this; break;
//            case "Transfer"     : $buttonList.transferButton = this; break;
        }
    });

    $("#ticket_shelve_nl_div").find('input').each(function(){
        switch($(this).attr('status')){
            case "CloseReason"       :   $buttonList.reasonButton = this; break;
//            case "Resolve"      :   $buttonList.resolveButton = this; break;
//            case "Pending"      :   $buttonList.reopenButton = this; break;
//            case "Reject"     :   $buttonList.rejectButton = this; break;
//            case "Approve"     :   $buttonList.approvalButton = this; break;
//            case "Transfer"     : $buttonList.transferButton = this; break;
        }
    });

    return $buttonList;
}



// JD126_I_280616-4    RD235_I_280616-11

 function changeUGHITicketStatusToCall($element){
     
    $outletTicketList =   getUghiSelectedTickets('outlet_ticket');
    $stcTicketList    =   getUghiSelectedTickets('stc_ticket_id');
    $stcSurveyIdList  =   getUghiSelectedTickets('stc_survey_id');
    $closingReason    =   $("#close_reason").val();
    console.log($closingReason);
//    if (confirm('Do you want to close the ticket?')) {
       changeUGHITicketStatusTo('#stock_ticket_results', 'ughi-tickets-close',$stcTicketList,$outletTicketList,'STC',$stcSurveyIdList,$closingReason);
        $('#ughi-closebutton').css('display','none');
        $('#close_reason').val('');
        $('#close_reason').css('display','none');
        $('#stock_ticket_go').trigger('click');

 //   }
      
   }
   
function changeShelveTicketStatus($element)
{
    $outletTicketList =   getUghiComplianceSelectedTickets('outlet_ticket');
    $stcTicketList    =   getUghiComplianceSelectedTickets('slv_survey_id');
    $ticketType       =   getUghiComplianceSelectedTickets('outlet_ticket');
    $closingReason = $("#shelve_close_reason").val();
    
 //  if (confirm('Do you want to close the ticket?')) {
        changeUGHITicketStatusTo('#compliance_ticket_results', 'ughi-tickets-close',$stcTicketList,$outletTicketList,'SLV','',$closingReason);
        $('#ughi-shelve-closebutton').css('display','none');
        $('#shelve_close_reason').val('');
        $('#shelve_close_reason').css('display','none');
        $('#compliance_ticket_go').trigger('click');
  // }
}

function changeShelveNlTicketStatus($element)
{
    $outletTicketList =   getUghiComplianceNlSelectedTickets('outlet_ul_ticket');
    $stcTicketList    =   getUghiComplianceNlSelectedTickets('slv_ul_survey_id');
    $reason = $("#shelveNl_close_reason").val();

  //  if (confirm('Do you want to close the ticket?')) {
        changeUGHITicketStatusTo('#compliance_NL_ticket_results', 'ughi-tickets-close',$stcTicketList,$outletTicketList,'NLSLV','',$reason);
        $('#ughi-shelve-nl-closebutton').css('display','none');
        $('#shelveNl_close_reason').val('');
        $('#shelveNl_close_reason').css('display','none');
        $('#compliance_NL_ticket_go').trigger('click');

  //  }
}

var drawStockTicketPendingUghi = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);

    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
            'requestFor' : 'pending-stock-tickets-bar-chart',
            'filter-data' : filterData
        };


        // $.post('request.php', requestData, function(result){
        //     stockPendingBarChart(result);
        // });

        $requestObject.initialize(requestData,"stockPendingBarChart",null);

    }
};


var stockPendingBarChart=  function ($result) {
    var chart;
    console.log('here');
    var displayData = [];
    var category = [];
    $result = JSON.parse($result);
    console.log('Pending Tickets');
    console.log($result);
    for ($i = 0, $total = 0; $i < $result.length; $i++) {
        $total += parseInt($result[$i]['per_region']);
    }
    $.each($result, function ($index, $value) {
        displayData.push({
            name: $value['branch_type'],
            y: parseInt($value['per_region'])
        });

        category.push($value['branch_type']);
    });
    title = 'Total Tickets: ' + $total;

    if (displayData.length === 0) {
        title = 'No ticket found';
    } else {
        title = '';
    }
    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'ughi_pending_stock_tickets_bar_chart_container',
            type: 'column'
        },

        title: {
            text: title
        },

        subtitle: {
            text: ''
        },

        xAxis: {
            categories: category
        },

        yAxis: {
            min: 0,
            title: {
                text: 'Tickets'
            },
            labels: {
                formatter: function () {
                    return this.value
                }
            }
        },
        tooltip: {},
        credits: {
            enabled: false
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 2,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            },
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                colorByPoint: false
            }
        },
        series: [{
            name: 'Branches',
            color: 'red',
            marker: {
                symbol: 'square'

            },
            data: displayData.concat(),
            dataLabels: {
                enabled: false,
                color: '#fff',
                align: 'right',
                format: '{point.y}', // one decimal
                x: -35,
                y: 50, // 10 pixels down from the top

            }
        }]
    });
    var chart_svg = chart.getSVG();
    var chart_canvas = document.getElementById('ughi_pending_stock_tickets_bar_chart_canvas');
    canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
    var chart_img = chart_canvas.toDataURL('image/png');
    $('#ughi_pending_stock_tickets_bar_chart_div').html('<img id="ughi_pending_stock_tickets_bar_chart_image" src="' + chart_img + '" />');

};

var drawStockTicketResolutionUghi = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);

    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
            'requestFor' : 'resolved-stock-tickets-bar-chart',
            'filter-data' : filterData
        };


        // $.post('request.php', requestData, function(result){
        //     stockResoBarChart(result);
        // });

        $requestObject.initialize(requestData,"stockResoBarChart",null);
    }
};

var drawComplianceTicketPendingUghi = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);

    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
            'requestFor' : 'pending-compliance-tickets-bar-chart',
            'filter-data' : filterData
        };


        // $.post('request.php', requestData, function(result){
        //     compPendingBarChart(result);
        // });
        $requestObject.initialize(requestData,"compPendingBarChart",null);
    }
};

var drawComplianceTicketResolutionUghi = function($parentElement, $containerId){
    var requestData = {};
    var filterData = ughiFilterData($parentElement);

    var validate = validateUghiFilter(filterData);
    if(validate){
        alert(validate);
    }else{
        $($containerId).html($("#dashbordLoader").html());
        requestData = {
            'requestFor' : 'resolved-compliance-tickets-bar-chart',
            'filter-data' : filterData
        };


        $requestObject.initialize(requestData,"compResoBarChart",null);

        // $.post('request.php', requestData, function(result){
        //     compResoBarChart(result);
        // });
    }
};

// var ughiLanding = function($parentElement, $containerLossOfSales, $containerPieChart, $containerStaffPerformance, $containerPendingTickets, $containerBranchNetwork, $containerVisits){
//
//     var requestData = {};
//     var filterData = ughiFilterData($parentElement);
//     console.log('filter data :: ');
//     console.log(filterData);
//     var validate = validateUghiFilter(filterData);
//     if(validate){
//         alert(validate);
//     }
//     else{
//         // $($containerPieChart).html($("#dashbordLoader").html());
//         // $($containerPendingTickets).html($("#dashbordLoader").html());
//         // $($containerBranchNetwork).html($("#dashbordLoader").html());
//         // $($containerVisits).html($("#dashbordLoader").html());
//         // $($containerLossOfSales).html($("#dashbordLoader").html());
//         // $($containerStaffPerformance).html($("#dashbordLoader").html());
//         requestData = {
//             'requestFor'  : 'ughi-landing',
//             'filter-data' : filterData
//         };
//
//         $.post('../request.php', requestData, function(result){
//
//             listedNotListedPieChart();
//             pendingTicketsChart();
//             branchNetworkChart();
//             numberOfVisitsChart(result);
//         });
//     }
// };

var ughiLanding = function($parentElement, $containerLossOfSales, $containerPieChart, $containerStaffPerformance, $containerPendingTickets, $containerBranchNetwork, $containerVisits){

    var requestData = {};
    var filterData = ughiFilterData($parentElement);
    var validate = validateUghiFilter(filterData);

    var defaultLink = $('#lossOfSalesLink').attr('myVal');
    for(var i in filterData){
         if(filterData[i] != 'ALL') {
             defaultLink += '&'+i+'='+filterData[i]+'';
         }
    }
    defaultLink += '&is-excel=true';

    $('#lossOfSalesLink').attr('href',defaultLink);

    if(validate){
        alert(validate);
    } else{

         requestData = {
            'requestFor'  : 'ughi-landing',
            'filter-data' : filterData
        };

        $('#containerPieChart').html($('#mainLoader').html());
        $('#containerPendingTickets').html($('#mainLoader').html());
        $('#containerBranchNetwork').html($('#mainLoader').html());
        $('#containerVisits').html($('#mainLoader').html());
        $('#inventory_price_location').html($('#loader-sm').html());
        $('#merchant_score_location').html($('#loader-sm').html());

        $.post('../request.php', requestData, function(landingResult){

            landingResult = JSON.parse(landingResult);

            $("#inventory_price_location").html(landingResult['inventory_price_v1']);
            $("#merchant_score_location").html(landingResult['merchant_score_v1']);

            $('.numberCountingEffect').each(function () {
                $(this).prop({Counter: 0}).animate({Counter: $(this).text()}, {
                    duration: 5000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text((Math.ceil(now * 100) / 100).toLocaleString());
                    }
                });
            });

            $('.percentageCountingEffect').each(function() {
                var $this = $(this);
                jQuery({Counter: 0}).animate({Counter: $this.text()}, {
                    duration: 5000,
                    easing: 'swing',
                    step: function() {
                        $this.text(this.Counter.toFixed(2)+'%');
                    }
                });
            });



            listedNotListedPieChart(landingResult['notListed_v1'], landingResult['other_v1']);
            pendingTicketsChart(JSON.parse(landingResult['totalPendingTickets_v1']), JSON.parse(landingResult['pendingTicketsGT2_v1']));
            branchNetworkChart(JSON.parse(landingResult['growth_result_v1']));
            numberOfVisitsChart(JSON.parse(landingResult['number_of_visits_v1']));

        });
    }

    return true;
};

var selectUGHIReportPeriodFilter = function($this) {

    period = $($this).val();

    if(period == 'Custom') {

        $('#ughiReportDatePickerFilter').css('display','block');
    } else {

        $('#ughiReportDatePickerFilter').css('display','none');
    }
};

var getOutletTypeByBranchName = function(outletType){

    var requestData = {};
    var otherData = {};

    $('#myUGHILoader').css({
        height: $('#myUGHILoader').parent().height(),
        width: $('#myUGHILoader').parent().width()
    });
    $('#myUGHILoader').show();

    requestData = {
        'requestFor' : 'get-outletcode-with-name',
        'outletType' : outletType
    };

    otherData = {
        'container_render_id' : '#outletcode-filter-selector',
        'container_hide_id'      : '#myUGHILoader'
    };
    $requestObject.initialize(requestData,'renderResult',null,otherData);

};