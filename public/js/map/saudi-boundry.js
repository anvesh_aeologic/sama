var first = "";
var previousCenter = new google.maps.LatLng(23.422928455065257, 45.32958984375);
var saudiPolygon;
var everythingElse = [
    new google.maps.LatLng(66.61914531954804,-20.892187499999977) ,
    new google.maps.LatLng(-37.708132520499916,-20.892187499999977),
    new google.maps.LatLng(-37.708132520499916, 115.3046875) ,
    new google.maps.LatLng(66.61914531954804, 115.3046875) ,
    new google.maps.LatLng(66.61914531954804,-20.892187499999977) ,
    new google.maps.LatLng(29.363020000000006,34.956661),new google.maps.LatLng(29.190530999999964,36.05529),
    new google.maps.LatLng(29.487419000000045,36.494751),new google.maps.LatLng(29.89779999999996,36.758419),
    new google.maps.LatLng(30.00251000000003,37.494499),new google.maps.LatLng(30.33494900000005,37.67028),
    new google.maps.LatLng(30.486548999999968,37.99987),new google.maps.LatLng(31.503619999999955,37.000118),
    new google.maps.LatLng(31.99875099999997,38.977661),new google.maps.LatLng(32.175610000000006,39.230339),
    new google.maps.LatLng(31.95216000000005,40.40588),new google.maps.LatLng(31.372390999999993,41.449581),
    new google.maps.LatLng(31.12818900000002,42.097771),new google.maps.LatLng(29.171340999999984,44.732658),
    new google.maps.LatLng(29.056170000000066,46.468498),new google.maps.LatLng(29.11376999999993,46.622311),
    new google.maps.LatLng(29.017740000000003,47.435299),new google.maps.LatLng(28.806170000000066,47.589111),
    new google.maps.LatLng(28.67130999999995,47.601921),new google.maps.LatLng(28.51696000000004,47.689812),
    new google.maps.LatLng(28.545919000000026,48.38195),new google.maps.LatLng(28.497659999999996,48.491821),
    new google.maps.LatLng(28.40106000000003,48.546749),new google.maps.LatLng(28.343060000000037,48.51379),
    new google.maps.LatLng(28.275351,48.579708),new google.maps.LatLng(28.197920000000067,48.63464),
    new google.maps.LatLng(28.13981100000001,48.612671),new google.maps.LatLng(28.042891000000054,48.667599),
    new google.maps.LatLng(27.984698999999978,48.788448),new google.maps.LatLng(27.926469999999995,48.788448),
    new google.maps.LatLng(27.85850000000005,48.84338),new google.maps.LatLng(27.751600000000053,48.887321),
    new google.maps.LatLng(27.683518999999933,48.909302),new google.maps.LatLng(27.60567100000003,48.942261),
    new google.maps.LatLng(27.566719000000035,49.030151),new google.maps.LatLng(27.537500000000023,49.151001),
    new google.maps.LatLng(27.52774999999997,49.26086),new google.maps.LatLng(27.430290000000014,49.315788),
    new google.maps.LatLng(27.420529999999985,49.18396),new google.maps.LatLng(27.35224900000003,49.26086),
    new google.maps.LatLng(27.293679999999995,49.359741),new google.maps.LatLng(27.205780000000004,49.436642),
    new google.maps.LatLng(27.332728999999972,49.458611),new google.maps.LatLng(27.35224900000003,49.57946),
    new google.maps.LatLng(27.303449999999998,49.7113),new google.maps.LatLng(27.264390999999932,49.634392),
    new google.maps.LatLng(27.264390999999932,49.535519),new google.maps.LatLng(27.147141000000033,49.634392),
    new google.maps.LatLng(27.019979000000035,49.733269),new google.maps.LatLng(26.912270000000035,49.821159),
    new google.maps.LatLng(26.863279000000034,49.985958),new google.maps.LatLng(26.73578999999995,50.106812),
    new google.maps.LatLng(26.57869900000003,50.172722),new google.maps.LatLng(26.55905000000007,50.12878),
    new google.maps.LatLng(26.51972999999998,50.24963),new google.maps.LatLng(26.38202100000001,50.271599),
    new google.maps.LatLng(26.165291000000025,50.205681),new google.maps.LatLng(25.94816000000003,50.161739),
    new google.maps.LatLng(25.819669999999974,50.139771),new google.maps.LatLng(25.671230000000037,50.26062),
    new google.maps.LatLng(25.532519999999977,50.32653),new google.maps.LatLng(25.423428999999942,50.46936),
    new google.maps.LatLng(25.27449999999999,50.546261),new google.maps.LatLng(25.115440000000035,50.57922),
    new google.maps.LatLng(24.986049999999977,50.65612),new google.maps.LatLng(24.796699999999987,50.755001),
    new google.maps.LatLng(24.65700000000004,50.875851),new google.maps.LatLng(24.487141000000065,51.029659),
    new google.maps.LatLng(24.507139000000052,51.293331),new google.maps.LatLng(24.63702999999998,51.425171),
    new google.maps.LatLng(24.557109999999966,51.52404),new google.maps.LatLng(24.43714,51.381222),
    new google.maps.LatLng(24.32707000000005,51.32629),new google.maps.LatLng(24.307051,51.43615),
    new google.maps.LatLng(24.246959999999945,51.589958),new google.maps.LatLng(24.076549999999997,51.622921),
    new google.maps.LatLng(23.765229999999974,51.886589),new google.maps.LatLng(22.93816000000004,52.578732),
    new google.maps.LatLng(22.62414899999999,55.149529),new google.maps.LatLng(22.705249999999978,55.20446),
    new google.maps.LatLng(21.993979999999965,55.687859),new google.maps.LatLng(19.99398999999994,54.99572),
    new google.maps.LatLng(19.01018899999997,51.99646),new google.maps.LatLng(18.615008999999986,49.107052),
    new google.maps.LatLng(18.166731000000027,48.1842),new google.maps.LatLng(17.46595000000002,47.590939),
    new google.maps.LatLng(17.09878900000001,47.481071),new google.maps.LatLng(16.941209999999955,47.173458),
    new google.maps.LatLng(16.951719000000026,46.997681),new google.maps.LatLng(17.266719999999964,46.734001),
    new google.maps.LatLng(17.22475099999997,46.294552),new google.maps.LatLng(17.32965999999999,45.393669),
    new google.maps.LatLng(17.455468999999994,45.206902),new google.maps.LatLng(17.434508999999935,44.657589),
    new google.maps.LatLng(17.392569999999978,44.580681),new google.maps.LatLng(17.44498999999996,44.448849),
    new google.maps.LatLng(17.42402100000004,44.1632),new google.maps.LatLng(17.37161100000003,44.09729),
    new google.maps.LatLng(17.40306099999998,44.042351),new google.maps.LatLng(17.32965999999999,43.95446),
    new google.maps.LatLng(17.34015099999999,43.811642),new google.maps.LatLng(17.382090000000062,43.668819),
    new google.maps.LatLng(17.539289000000053,43.460079),new google.maps.LatLng(17.549769999999967,43.306271),
    new google.maps.LatLng(17.455468999999994,43.21838),new google.maps.LatLng(17.37161100000003,43.240349),
    new google.maps.LatLng(17.30867999999998,43.33923),new google.maps.LatLng(17.203769999999963,43.163448),
    new google.maps.LatLng(17.10928899999999,43.163448),new google.maps.LatLng(17.035769999999957,43.240349),
    new google.maps.LatLng(16.962230999999974,43.185421),new google.maps.LatLng(16.899170000000026,43.130489),
    new google.maps.LatLng(16.825568999999973,43.21838),new google.maps.LatLng(16.50983199999996,43.220215),
    new google.maps.LatLng(16.383389999999963,42.978516),new google.maps.LatLng(16.299050999999963,42.780762),
    new google.maps.LatLng(16.846604999999954,42.51709),new google.maps.LatLng(17.119793000000072,42.297363),
    new google.maps.LatLng(17.455472999999984,42.275391),new google.maps.LatLng(17.81145700000002,41.77002),
    new google.maps.LatLng(18.166731000000027,41.52832),new google.maps.LatLng(18.68787999999995,41.19873),
    new google.maps.LatLng(18.895893,41.132812),new google.maps.LatLng(18.99980400000004,41.044922),
    new google.maps.LatLng(19.248921999999993,40.979004),new google.maps.LatLng(19.518374999999992,40.913086),
    new google.maps.LatLng(19.539083000000005,40.759277),new google.maps.LatLng(19.766705,40.693359),
    new google.maps.LatLng(19.725341999999955,40.539551),new google.maps.LatLng(19.993998000000033,40.45166),
    new google.maps.LatLng(20.09720600000003,40.297852),new google.maps.LatLng(20.282807999999932,40.078125),
    new google.maps.LatLng(20.220965999999976,39.902344),new google.maps.LatLng(20.406420000000026,39.682617),
    new google.maps.LatLng(20.715014999999994,39.484863),new google.maps.LatLng(21.02298399999995,39.221191),
    new google.maps.LatLng(21.330314999999928,39.001465),new google.maps.LatLng(21.596150999999963,39.089355),
    new google.maps.LatLng(22.004175000000032,38.95752),new google.maps.LatLng(22.390715,39.067383),
    new google.maps.LatLng(23.039297000000033,38.759766),new google.maps.LatLng(23.785345000000007,38.342285),
    new google.maps.LatLng(24.046465000000012,38.056641),new google.maps.LatLng(24.246965000000046,37.705078),
    new google.maps.LatLng(24.226928999999927,37.529297),new google.maps.LatLng(24.427144999999996,37.331543),
    new google.maps.LatLng(24.66698599999995,37.287598),new google.maps.LatLng(24.846565000000055,37.133789),
    new google.maps.LatLng(25.025885000000017,37.199707),new google.maps.LatLng(25.36388199999999,37.023926),
    new google.maps.LatLng(25.40358500000002,36.782227),new google.maps.LatLng(25.621715999999992,36.452637),
    new google.maps.LatLng(25.958043999999973,36.650391),new google.maps.LatLng(26.234302999999954,36.408691),
    new google.maps.LatLng(26.509905000000003,36.23291),new google.maps.LatLng(26.765229999999974,36.057129),
    new google.maps.LatLng(26.941659999999956,35.969238),new google.maps.LatLng(27.078690999999935,35.749512),
    new google.maps.LatLng(27.371766999999977,35.617676),new google.maps.LatLng(27.722437000000014,35.419922),
    new google.maps.LatLng(27.83907499999998,35.266113),new google.maps.LatLng(28.033196999999973,35.134277),
    new google.maps.LatLng(28.052591000000007,34.936523),new google.maps.LatLng(28.052591000000007,34.716797),
    new google.maps.LatLng(27.974998000000028,34.562988),new google.maps.LatLng(28.16887500000007,34.606934),
    new google.maps.LatLng(28.516970000000015,34.782715),new google.maps.LatLng(28.825423999999998,34.760742),
    new google.maps.LatLng(29.363020000000006,34.956661),new google.maps.LatLng(66.61914531954804,-20.892187499999977)
]

var saudiPath = [
    new google.maps.LatLng(29.363020000000006,34.956661),new google.maps.LatLng(29.190530999999964,36.05529),new google.maps.LatLng(29.487419000000045,36.494751),new google.maps.LatLng(29.89779999999996,36.758419),new google.maps.LatLng(30.00251000000003,37.494499),new google.maps.LatLng(30.33494900000005,37.67028),new google.maps.LatLng(30.486548999999968,37.99987),new google.maps.LatLng(31.503619999999955,37.000118),new google.maps.LatLng(31.99875099999997,38.977661),new google.maps.LatLng(32.175610000000006,39.230339),new google.maps.LatLng(31.95216000000005,40.40588),new google.maps.LatLng(31.372390999999993,41.449581),new google.maps.LatLng(31.12818900000002,42.097771),new google.maps.LatLng(29.171340999999984,44.732658),new google.maps.LatLng(29.056170000000066,46.468498),new google.maps.LatLng(29.11376999999993,46.622311),new google.maps.LatLng(29.017740000000003,47.435299),new google.maps.LatLng(28.806170000000066,47.589111),new google.maps.LatLng(28.67130999999995,47.601921),new google.maps.LatLng(28.51696000000004,47.689812),new google.maps.LatLng(28.545919000000026,48.38195),new google.maps.LatLng(28.497659999999996,48.491821),new google.maps.LatLng(28.40106000000003,48.546749),new google.maps.LatLng(28.343060000000037,48.51379),new google.maps.LatLng(28.275351,48.579708),new google.maps.LatLng(28.197920000000067,48.63464),new google.maps.LatLng(28.13981100000001,48.612671),new google.maps.LatLng(28.042891000000054,48.667599),new google.maps.LatLng(27.984698999999978,48.788448),new google.maps.LatLng(27.926469999999995,48.788448),new google.maps.LatLng(27.85850000000005,48.84338),new google.maps.LatLng(27.751600000000053,48.887321),new google.maps.LatLng(27.683518999999933,48.909302),new google.maps.LatLng(27.60567100000003,48.942261),new google.maps.LatLng(27.566719000000035,49.030151),new google.maps.LatLng(27.537500000000023,49.151001),new google.maps.LatLng(27.52774999999997,49.26086),new google.maps.LatLng(27.430290000000014,49.315788),new google.maps.LatLng(27.420529999999985,49.18396),new google.maps.LatLng(27.35224900000003,49.26086),new google.maps.LatLng(27.293679999999995,49.359741),new google.maps.LatLng(27.205780000000004,49.436642),new google.maps.LatLng(27.332728999999972,49.458611),new google.maps.LatLng(27.35224900000003,49.57946),new google.maps.LatLng(27.303449999999998,49.7113),new google.maps.LatLng(27.264390999999932,49.634392),new google.maps.LatLng(27.264390999999932,49.535519),new google.maps.LatLng(27.147141000000033,49.634392),new google.maps.LatLng(27.019979000000035,49.733269),new google.maps.LatLng(26.912270000000035,49.821159),new google.maps.LatLng(26.863279000000034,49.985958),new google.maps.LatLng(26.73578999999995,50.106812),new google.maps.LatLng(26.57869900000003,50.172722),new google.maps.LatLng(26.55905000000007,50.12878),new google.maps.LatLng(26.51972999999998,50.24963),new google.maps.LatLng(26.38202100000001,50.271599),new google.maps.LatLng(26.165291000000025,50.205681),new google.maps.LatLng(25.94816000000003,50.161739),new google.maps.LatLng(25.819669999999974,50.139771),new google.maps.LatLng(25.671230000000037,50.26062),new google.maps.LatLng(25.532519999999977,50.32653),new google.maps.LatLng(25.423428999999942,50.46936),new google.maps.LatLng(25.27449999999999,50.546261),new google.maps.LatLng(25.115440000000035,50.57922),new google.maps.LatLng(24.986049999999977,50.65612),new google.maps.LatLng(24.796699999999987,50.755001),new google.maps.LatLng(24.65700000000004,50.875851),new google.maps.LatLng(24.487141000000065,51.029659),new google.maps.LatLng(24.507139000000052,51.293331),new google.maps.LatLng(24.63702999999998,51.425171),new google.maps.LatLng(24.557109999999966,51.52404),new google.maps.LatLng(24.43714,51.381222),new google.maps.LatLng(24.32707000000005,51.32629),new google.maps.LatLng(24.307051,51.43615),new google.maps.LatLng(24.246959999999945,51.589958),new google.maps.LatLng(24.076549999999997,51.622921),new google.maps.LatLng(23.765229999999974,51.886589),new google.maps.LatLng(22.93816000000004,52.578732),new google.maps.LatLng(22.62414899999999,55.149529),new google.maps.LatLng(22.705249999999978,55.20446),new google.maps.LatLng(21.993979999999965,55.687859),new google.maps.LatLng(19.99398999999994,54.99572),new google.maps.LatLng(19.01018899999997,51.99646),new google.maps.LatLng(18.615008999999986,49.107052),new google.maps.LatLng(18.166731000000027,48.1842),new google.maps.LatLng(17.46595000000002,47.590939),new google.maps.LatLng(17.09878900000001,47.481071),new google.maps.LatLng(16.941209999999955,47.173458),new google.maps.LatLng(16.951719000000026,46.997681),new google.maps.LatLng(17.266719999999964,46.734001),new google.maps.LatLng(17.22475099999997,46.294552),new google.maps.LatLng(17.32965999999999,45.393669),new google.maps.LatLng(17.455468999999994,45.206902),new google.maps.LatLng(17.434508999999935,44.657589),new google.maps.LatLng(17.392569999999978,44.580681),new google.maps.LatLng(17.44498999999996,44.448849),new google.maps.LatLng(17.42402100000004,44.1632),new google.maps.LatLng(17.37161100000003,44.09729),new google.maps.LatLng(17.40306099999998,44.042351),new google.maps.LatLng(17.32965999999999,43.95446),new google.maps.LatLng(17.34015099999999,43.811642),new google.maps.LatLng(17.382090000000062,43.668819),new google.maps.LatLng(17.539289000000053,43.460079),new google.maps.LatLng(17.549769999999967,43.306271),new google.maps.LatLng(17.455468999999994,43.21838),new google.maps.LatLng(17.37161100000003,43.240349),new google.maps.LatLng(17.30867999999998,43.33923),new google.maps.LatLng(17.203769999999963,43.163448),new google.maps.LatLng(17.10928899999999,43.163448),new google.maps.LatLng(17.035769999999957,43.240349),new google.maps.LatLng(16.962230999999974,43.185421),new google.maps.LatLng(16.899170000000026,43.130489),new google.maps.LatLng(16.825568999999973,43.21838),new google.maps.LatLng(16.50983199999996,43.220215),new google.maps.LatLng(16.383389999999963,42.978516),new google.maps.LatLng(16.299050999999963,42.780762),new google.maps.LatLng(16.846604999999954,42.51709),new google.maps.LatLng(17.119793000000072,42.297363),new google.maps.LatLng(17.455472999999984,42.275391),new google.maps.LatLng(17.81145700000002,41.77002),new google.maps.LatLng(18.166731000000027,41.52832),new google.maps.LatLng(18.68787999999995,41.19873),new google.maps.LatLng(18.895893,41.132812),new google.maps.LatLng(18.99980400000004,41.044922),new google.maps.LatLng(19.248921999999993,40.979004),new google.maps.LatLng(19.518374999999992,40.913086),new google.maps.LatLng(19.539083000000005,40.759277),new google.maps.LatLng(19.766705,40.693359),new google.maps.LatLng(19.725341999999955,40.539551),new google.maps.LatLng(19.993998000000033,40.45166),new google.maps.LatLng(20.09720600000003,40.297852),new google.maps.LatLng(20.282807999999932,40.078125),new google.maps.LatLng(20.220965999999976,39.902344),new google.maps.LatLng(20.406420000000026,39.682617),new google.maps.LatLng(20.715014999999994,39.484863),new google.maps.LatLng(21.02298399999995,39.221191),new google.maps.LatLng(21.330314999999928,39.001465),new google.maps.LatLng(21.596150999999963,39.089355),new google.maps.LatLng(22.004175000000032,38.95752),new google.maps.LatLng(22.390715,39.067383),new google.maps.LatLng(23.039297000000033,38.759766),new google.maps.LatLng(23.785345000000007,38.342285),new google.maps.LatLng(24.046465000000012,38.056641),new google.maps.LatLng(24.246965000000046,37.705078),new google.maps.LatLng(24.226928999999927,37.529297),new google.maps.LatLng(24.427144999999996,37.331543),new google.maps.LatLng(24.66698599999995,37.287598),new google.maps.LatLng(24.846565000000055,37.133789),new google.maps.LatLng(25.025885000000017,37.199707),new google.maps.LatLng(25.36388199999999,37.023926),new google.maps.LatLng(25.40358500000002,36.782227),new google.maps.LatLng(25.621715999999992,36.452637),new google.maps.LatLng(25.958043999999973,36.650391),new google.maps.LatLng(26.234302999999954,36.408691),new google.maps.LatLng(26.509905000000003,36.23291),new google.maps.LatLng(26.765229999999974,36.057129),new google.maps.LatLng(26.941659999999956,35.969238),new google.maps.LatLng(27.078690999999935,35.749512),new google.maps.LatLng(27.371766999999977,35.617676),new google.maps.LatLng(27.722437000000014,35.419922),new google.maps.LatLng(27.83907499999998,35.266113),new google.maps.LatLng(28.033196999999973,35.134277),new google.maps.LatLng(28.052591000000007,34.936523),new google.maps.LatLng(28.052591000000007,34.716797),new google.maps.LatLng(27.974998000000028,34.562988),new google.maps.LatLng(28.16887500000007,34.606934),new google.maps.LatLng(28.516970000000015,34.782715),new google.maps.LatLng(28.825423999999998,34.760742),new google.maps.LatLng(29.363020000000006,34.956661)
]

var israil8 = [
            new google.maps.LatLng(31.071756,34.627991),new google.maps.LatLng(31.076462,35.083923),new google.maps.LatLng(30.939924,35.089417),new google.maps.LatLng(30.944635,34.638977),new google.maps.LatLng(31.071756,34.627991)
];

var israil7 = [
    new google.maps.LatLng(31.090574,34.528198),new google.maps.LatLng(31.099981,35.198364),new google.maps.LatLng(30.939924,35.198364),new google.maps.LatLng(30.921076,34.561157),new google.maps.LatLng(31.090574,34.528198)
];

var israil6 = [
    new google.maps.LatLng(31.165810,34.163818),new google.maps.LatLng(31.184608,35.526123),new google.maps.LatLng(30.864510,35.526123),new google.maps.LatLng(30.864510,34.185791),new google.maps.LatLng(31.165810,34.163818)
];

var israil5 = [
    new google.maps.LatLng(31.203405,34.182129),new google.maps.LatLng(31.240986,35.764160),new google.maps.LatLng(30.789038,35.764160),new google.maps.LatLng(30.713505,34.226074),new google.maps.LatLng(31.203405,34.182129)
];
var israil4 = [
    new google.maps.LatLng(32.620869,31.977537),
    new google.maps.LatLng(32.694866,34.965820),
    new google.maps.LatLng(31.578535,34.877930),
    new google.maps.LatLng(31.578535,31.977537),
    new google.maps.LatLng(32.620869,31.977537)
]


function changeIsrailLayer(zoomLevel,currentMap){
            var changedFlag = false;
           
            switch(zoomLevel){
                        case 4: setIsrailPath(israil4,currentMap);
                                changedFlag = true;
                                break;
                        case 5: setIsrailPath(israil5,currentMap);
                                changedFlag = true;
                                break;
                        case 6:setIsrailPath(israil6,currentMap);
                                changedFlag = true;
                                break;
                        case 7: setIsrailPath(israil7,currentMap);
                                changedFlag = true;
                                break;
                        case 8: setIsrailPath(israil8,currentMap);
                                changedFlag = true;
                                break;
                       
                        
                        
                        
            }
            if(!changedFlag){
                israilePoly('hide',null,currentMap);
            }
 }
        
function setIsrailPath(israilPath,currentMap){
            if(israilePolygon){
                israilePoly('change',israilPath,currentMap)    
            }else{
                israilePoly('create',israilPath,currentMap);
            }
}
var israilePolygon; 
var israileLines = [];

function israileLine(israilpath,linecolor,currentMap){
            var lineSymbol = {
              path: 'M 0,-1 0,1',
              strokeOpacity: 1,
              scale: 1
            };
            var previous;
            for(var i in israileLines){
                israileLines[i].setMap(null);
            }
            israileLines.length = 0;
            if(linecolor){
                    for(j in israilpath){
                        if(j == 0){
                            previous = israilpath[j];
                        }else{
                            var line = new google.maps.Polyline({
                                  path: [previous,israilpath[j]],
                                  strokeOpacity: 0,
                                  strokeColor:linecolor,
                                  icons: [{
                                    icon: lineSymbol,
                                    offset: '0',
                                    repeat: '5px'
                                  }],
                                  map: currentMap
                                });
                             previous = israilpath[j];
                             israileLines.push(line);
                        }
                    }
            }
}
        
var isrileBoundryMarker = [];
function israilePoly(type,israilpath,currentMap){
            for(var i in israilpath){
                if(i>1){
                    israilpath[i] = new google.maps.LatLng(israilpath[i].lat()-.10,israilpath[i].lng());
                }else{
                    israilpath[i] = new google.maps.LatLng(israilpath[i].lat()+.10,israilpath[i].lng());
                }
            }
            
            var fillcolor = '#F4F3F0';
            var linecolor = '#F4F3F0';
            if(currentMap.getZoom()<=4){
                fillcolor = "#A5BFDD";
                linecolor = null;
            }
            if(currentMap.getMapTypeId() != 'roadmap'){
                fillcolor = '#9E8D71';
                linecolor = 'white';
                if(currentMap.getZoom()<=4){
                    fillcolor = "#394889";
                    linecolor = null;
                }
            }
             
            if(type == 'create'){
                if(israilePolygon){
                    israilePolygon.setOptions({fillColor: fillcolor});
                    israilePolygon.setPath(israilpath);
                    israileLine(israilpath,linecolor);
                    israilePolygon.setMap(currentMap);
                }else{
                    israilePolygon = new google.maps.Polygon({
                          paths: israilpath,
                          strokeWeight:0,
                          fillColor: fillcolor,
                          fillOpacity: 1,
                          map:currentMap,
                          
                    });
                    israileLine(israilpath,linecolor);
                }
            }
            if(type == 'change'){
                israilePolygon.setOptions({fillColor: fillcolor});
                israilePolygon.setPath(israilpath);
                israileLine(israilpath,linecolor);
                israilePolygon.setMap(currentMap);
            }
            
            if(type == 'hide'){
                if(israilePolygon)
                    israilePolygon.setMap(null);
                israilePolygon = null;
            }
            
             if(type == 'show'){
                if(israilePolygon)
                    israilePolygon.setMap(currentMap);
            }
        
            
}
var previousMapType;    

function initializeMapEvents(){
    google.maps.event.addListener(map, 'zoom_changed', function (event) { 
       if(currentMap.getZoom()<5){
            map.setZoom(5);
            map.setOptions({draggable: false});
       }else{
            if(currentMap.getZoom()==5){
                map.setOptions({draggable: false});
            }else{
                 map.setOptions({draggable: true});
            }
       }
       
  });
  
  google.maps.event.addListener(map, 'tilesloaded', function (event) { 
    if(first == ""){
            closeBoundry();
            saudiPoly();
            first = 1;
    }
  });
  
  google.maps.event.addListener(map, 'dragend', function()
   {
        
        if(!saudiPolygon.Contains(map.getCenter())){
            map.setCenter(previousCenter);
        }else{
            previousCenter = map.getCenter();
        }
        return;
        var mapBounds = map.getBounds(),
        map_SW_lat = mapBounds.getSouthWest().lat(),
        map_SW_lng = mapBounds.getSouthWest().lng(),
        map_NE_lat = mapBounds.getNorthEast().lat(),
        map_NE_lng = mapBounds.getNorthEast().lng(),
        maxX = strictBounds.getNorthEast().lng(),
        maxY = strictBounds.getNorthEast().lat(),
        minX = strictBounds.getSouthWest().lng(),
        minY = strictBounds.getSouthWest().lat();

        if (strictBounds.contains(mapBounds.getNorthEast()) && strictBounds.contains(mapBounds.getSouthWest())) 
        {
            return;
        }

        // We're out of bounds - Move the map back within the bounds
        if (map_SW_lng < minX) map_SW_lng = minX;
        if (map_SW_lng > maxX) map_SW_lng = maxX;
        if (map_NE_lng < minX) map_NE_lng = minX;
        if (map_NE_lng > maxX) map_NE_lng = maxX;

        if (map_SW_lat < minY) map_SW_lat = minY;
        if (map_SW_lat > maxY) map_SW_lat = maxY;
        if (map_NE_lat < minY) map_NE_lat = minY;
        if (map_NE_lat > maxY) map_NE_lat = maxY;

        map.panToBounds(new google.maps.LatLngBounds(
        new google.maps.LatLng(map_SW_lat, map_SW_lng), 
        new google.maps.LatLng(map_NE_lat, map_NE_lng)
        ));
    });
}


function closeBoundry(){
    var poly = new google.maps.Polygon({
      paths: everythingElse,
      strokeWeight:0,
      fillColor: fillColor,
      fillOpacity: fillOpacity
    });

    poly.setMap(map);
}


function saudiPoly(){
    saudiPolygon = new google.maps.Polygon({path:saudiPath});
}
