/*
 * Global map object 
 */
var map;
var circleArray = [];
var styles = [];
var centerLatLongs;
var defaultZoom = 5;

/*
 *stores all branch codes
 */
var codeList_ = new Array();

/*mapOptions
 *Default map options to initialise with
 */
var mapOptions = { center: new google.maps.LatLng(22.268763552489748, 47.87841796875),zoom: defaultZoom, mapTypeId: google.maps.MapTypeId.ROADMAP };

var objMarkers = [];
var markersArray = [];
var obj = null;
var fusionObjectList = new ArrayObject();

function parseLatLng(lat, lng) { 
    return new google.maps.LatLng(lat, lng); 
}

google.maps.event.addDomListener(window, 'load', initializeMap);
function initializeMap() {
    map = new google.maps.Map(document.getElementById('map_section'), mapOptions);
    if (map == undefined || map == null)
        return;
     initializeIsraileEvents();    
     setSQLWhere(_global.inventory.WHERE);
    
    /*Draw heat map*/
    getPopulationData(); 
}


/**setSQLWhere()
 * create url based on the selected condition
 * Request for data from server
 * @return  calls callback function createFusionObject();
 */
function setSQLWhere($where) {
    $("#loading_screen").show();
    objMarkers = [];
    codeList_ = [];
    markersArray = [];
    obj = null;
    fusionObjectList.clear();
    var select =  _global.inventory.SELECT_COLUMN;
    getGISData(select, $where);
}

function getGISData(select, where) {
    $.ajax({
        url: 'model/tbl_branch.php',
        type: 'POST',
        datatype: 'JSON',
        data: {action:'GISDATA', select:select, where:where},
        success: executeFusionData,
        error: function(){
            alert();
        }
    });
}

function executeFusionData(data){
    data = $.parseJSON(data);
    if(!data.rows)
        data.rows = [];
    $("#resultCounter").html(data.rows.length);

    createFusionObject(data.rows);
    getBankBranches(data, dashboardMap);
}

function riyadFusionResult(data){
    $("#resultCounter").html(data.rows.length);
    createFusionObject(data.rows);
    getBankBranches(data, dashboardMap);
}

function createFusionObject(objList) {
    if(!objList.length){
        alert('No data found!');
    }
    for (i in objList) {        
        var c = 0;
        obj = new AtmObject();
        for (j in obj) {
            obj[j] = objList[i][c++];
        }
        if (!isBlank(obj.Location) && fusionObjectList.get(obj.Location, "Location") == null){
            objMarkers.push(obj);                    
            codeList_.push(obj.BranchCode);
        }
        fusionObjectList.add(obj);
    }
    addMarkers(objMarkers);
}