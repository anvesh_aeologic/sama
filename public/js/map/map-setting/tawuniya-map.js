var styles = [
    [{
        url: baseUrl + "public/img/cluster/tawuniya/m1.png",
        height: 35,
        width: 35,
        anchor: [9, 0],
        textColor: "#FFFFFF",
        textSize: 10
    }, {
        url: baseUrl + "public/img/cluster/tawuniya/m2.png",
        height: 40,
        width: 40,
        anchor: [12, 0],
        textColor: "#FFFFFF",
        textSize: 11
    }, {
        url: baseUrl + "public/img/cluster/tawuniya/m3.png",
        height: 52,
        width: 52,
        anchor: [19, 0],
        textColor: "#FFFFFF",
        textSize: 12
    }, {
        url: baseUrl + "public/img/cluster/tawuniya/m4.png",
        height: 62,
        width: 62,
        anchor: [24, 0],
        textColor: "#FFFFFF",
        textSize: 13
    }]
]

var CloseButtonImg = 'close-button.png';
var BankColor   = 'white';
var markerIcons = new Array(
    {image: baseUrl + "public/img/logo/tawunia_logo.png", width: 58,height: 66},
    {image: baseUrl + "public/img/logo/tawunia_logo.png", width: 32,height: 60}
);

var locatorMarkerIcons = new Array({
    'ALBILAD_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/al_bilad_logo.png",
        width: 32,
        height: 40
    },
    'NATIONAL_COMMERCIAL_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/ncb_logo.png",
        width: 32,
        height: 40
    },
    'NATIONAL_COMMERCIAL_BANK_CAPITAL': {
        image: baseUrl + "public/img/map-markers/bank-markers/ncb_logo.png",
        width: 32,
        height: 40
    },
    'ALINMA_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/alinma_logo.png",
        width: 32,
        height: 40
    },
    'AL_JAZIRA_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/aljazeera_logo.png",
        width: 32,
        height: 40
    },
    'ALRAJHI_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/alrajhi_logo.png",
        width: 32,
        height: 40
    },
    'ARAB_NATIONAL_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/anb_logo.png",
        width: 32,
        height: 40
    },
    'SAUDI_FRANSI_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/bsf_logo.png",
        width: 32,
        height: 40
    },
    'SHB_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/shb_logo.png",
        width: 32,
        height: 40
    },
    'RIYAD_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/riyad_logo.png",
        width: 32,
        height: 40
    },
    'SABB_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/sabb_logo.png",
        width: 32,
        height: 40
    },
    'SAMBA_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/samba_logo.png",
        width: 32,
        height: 40
    },
    'SAUDI_INVESTMENT_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/sib_logo.png",
        width: 32,
        height: 40
    },
    'HRDF_BANK': {
        image: baseUrl + "public/img/map-markers/bank-markers/hrdf_logo.png",
        width: 32,
        height: 40
    },
    'TAWUNIYA_INSURANCE': {
        image: baseUrl + "public/img/map-markers/bank-markers/tawuniya_logo.png",
        width: 32,
        height: 40
    },
    'HOSPITAL': {
        image: baseUrl + "public/img/map-markers/bank-markers/hospital.png",
        width: 32,
        height: 40
    },
    'UNIVERSITY': {
        image: baseUrl + "public/img/map-markers/bank-markers/university.png",
        width: 32,
        height: 40
    },
    'RESTAURANT': {
        image: baseUrl + "public/img/map-markers/bank-markers/hotel.png",
        width: 32,
        height: 40
    },
    'DEFAULT': {
        image: baseUrl + "public/img/logo/survey.png",
        width: 32,
        height: 40
    }
});