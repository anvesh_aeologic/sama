var _MarkerCollection = new ArrayObject();
var cluster;

function clearCluster() {
    if (cluster != undefined) {
        cluster.clearMarkers();
    }
}

function newMarker(latLng, obj) {    
    return new google.maps.Marker({ position: latLng, icon: createIcon(obj.Type)}); 
}


/**createIcon()
*  
*  return marker image as per suppplied type
*  @param type type defines ATM or Marker
*  @return marker image
*/
function createIcon(type){
    var imagePosition = (type=='ATM') ? 1 : 0;
    return new google.maps.MarkerImage(baseUrl + '' + markerIcons[imagePosition].image, null, null, null, new google.maps.Size(markerIcons[imagePosition].width, markerIcons[imagePosition].height));
}



function addMarkers(objList) {
    clearCluster();
    for (i in objList) {
        addMarker(objList[i]);
    }
    createClusters(markersArray);
}


function addMarker(obj) {
    var latLng = obj.Location.split(',');    
    var marker = newMarker(parseLatLng(latLng[0], latLng[1]), obj);
    bindMarkerEvent(marker,obj);
    markersArray.push(marker);
}

function bindMarkerEvent(m, obj) {
    google.maps.event.addListener(m, 'click', function () { openInfoWindow(obj, m); });
}

var styles = [
[{
    url: baseUrl + "img/cluster/riyad/m1.png",
    height: 35,
    width: 35,
    anchor: [9, 0],
    textColor: "#FFFFFF",
    textSize: 10
}, {
    url: baseUrl + "img/cluster/riyad/m2.png",
    height: 40,
    width: 40,
    anchor: [12, 0],
    textColor: "#FFFFFF",
    textSize: 11
}, {
    url: baseUrl + "img/cluster/riyad/m3.png",
    height: 52,
    width: 52,
    anchor: [19, 0],
    textColor: "#FFFFFF",
    textSize: 12
}, {
    url: baseUrl + "img/cluster/riyad/m4.png",
    height: 62,
    width: 62,
    anchor: [24, 0],
    textColor: "#FFFFFF",
    textSize: 13
}]
]

var cOptions = {  styles: styles[0] }
function fitMapBound(markers){
    if(markers.length){
        var bounds = new google.maps.LatLngBounds();
        for(var markerIndex in markers){
            bounds.extend(markers[markerIndex].getPosition());
        }
        map.fitBounds(bounds);
    }else{
        map.setCenter(new google.maps.LatLng(22.268763552489748, 47.87841796875));
        map.setZoom(5);
    }
}
function createClusters(markers) {
    fitMapBound(markers);
    if (cluster == undefined) { cluster = new MarkerClusterer(map, markers, cOptions);} else { cluster.addMarkers(markers, false); }
    $("#loading_screen").hide();
}

