$(document).ready(function () {
   $('.tab-container > li').click(function () {
           
       var showClass = $(this).data('show-class');

//        hide all class of current container
       $($(this).parent('ul').find('li')).each(function () {
           $(this).removeClass('active');
           if ($(this).data('show-class')) {
               $('.' + $(this).data('show-class')).addClass('hide');
           }

       });

//        apply action for current tab
       if (showClass) {
           $('.' + showClass).removeClass('hide');
       }
       $(this).addClass('active');



   });
   
   $(".table_name").change(function() {
            var $dropdown = $(this);
            var data; 
            var key = $dropdown.val();
            var vals = [];
            var field_camel_case;
            data = $("#"+key).val();
            vals = data.split(",");
            var code = $(this).attr('id').split("_");
            var $field = $("#field_"+code[1]);
            $field.empty();
            $.each(vals, function(index, value) {
                    opt_val = value.replace(/\_/g, ' ');
                    // change the name in camel case
                    field_camel_case = opt_val.split(" ").map(function(i){return i[0].toUpperCase() + i.substring(1)}).join(" ");
                    $field.append("<option value='"+value+"'>" + field_camel_case + "</option>");
            });
            
            if( $("#is_photoproof_"+code[1]).val() == '0' ) {
                if( key != 'survey_answers' ) {
                    $("#metric_type_"+code[1]).val('Other');
                } else {
                    $("#metric_type_"+code[1]).val('Question');
                }
            }

    });
   /* 
    if($("#photoproofs").val() != ""){
        var photoproofs = $("#photoproofs").val().split(',');
        
        $.each(photoproofs, function(index, code){
            var data; 
            var vals = [];
            var field_camel_case;
            data = $("#survey_other_info").val();
            vals = data.split(",");
            var $field = $("#field_"+code);
            $field.empty();
            $("#table_"+code).val('survey_other_info');
            $.each(vals, function(index, value) {
                    opt_val = value.replace(/\_/g, ' ');
                    // change the name in camel case
                    field_camel_case = opt_val.split(" ").map(function(i){return i[0].toUpperCase() + i.substring(1)}).join(" ");
                    $field.append("<option value='"+value+"'>" + field_camel_case + "</option>");
            });
        })
    }
    */
    $("#client_list").change(function () {
        var client_id = $(this).val();
        if(client_id != "") {
            $("#metrice_list").show();
        } else {
            $("#metrice_list").hide();
        }
    });
    
    $("#global_client").change(function(){
        
        var client_id = $(this).val();
        var base_url = $('#base_url').val();
		console.log(base_url+client_id);
        $.ajax({
            method: 'POST',
            url: base_url+'/admin/ajax/setclient',
            data: {client : client_id },
            success: function(data){
                console.log(data);
               // location.reload();
            }
        })
        
    });
    
    
    $("#multi_survey_dd").change(function(){
        
        var survey_pass = $(this).val();
        var base_url = $('#base_url').val();
        $.ajax({
            method: 'POST',
            url: base_url+'/admin/ajax/set-selected-survey',
            data: { survey : survey_pass },
            success: function(data){
                //console.log(data);
                location.reload();
            }
        })
        
    });
   
   
   
    
});