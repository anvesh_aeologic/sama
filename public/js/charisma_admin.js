$(document).keyup(function(e) {
    if (e.keyCode === 27) {
        showHideDropDown("hide");
        $(document).find('.popover').each(function() {
            $(this).remove();
        });
    }
});

$(document).ready(function() {
    //themes, change CSS with JS
    //default theme(CSS) is cerulean, change it if needed
    var current_theme = 'spacelab';
    //switch_theme(current_theme);

    $('#themes a[data-value="' + current_theme + '"]').find('i').addClass('icon-ok');



    function switch_theme(theme_name)
    {
        
        $('#bs-css').attr('href', 'https://4c-dashboard/public/css/bootstrap-' + theme_name + '.css');
    }


    //disbaling some functions for Internet Explorer
    if ($.browser.msie)
    {
        $('#is-ajax').prop('checked', false);
        $('#for-is-ajax').hide();
        $('#toggle-fullscreen').hide();
        $('.login-box').find('.input-large').removeClass('span10');

    }

    //animating menus on hover
    $('ul.main-menu li:not(.nav-header)').hover(function() {
        $(this).animate({'margin-left': '+=5'}, 300);
    },
            function() {
                $(this).animate({'margin-left': '-=5'}, 300);
            });

    //other things to do on document ready, seperated for ajax calls
    docReady();
});


function selectMainTab() {
    $('#myTab a:nth-child(2)').tab('show');
}

function docReady() {
    //prevent # links from moving to top
    $('a[href="#"][data-top!=true]').click(function(e) {
        e.preventDefault();
    });


    //datepicker
    $('.datepicker').datepicker();



    //uniform - styler for checkbox, radio and file input
    $("input:checkbox, input:radio, input:file").not('[data-no-uniform="true"],#uniform-is-ajax').uniform();

    //chosen - improves select
    $('[data-rel="chosen"],[rel="chosen"]').chosen();

    $('.btn-close').click(function(e) {
        e.preventDefault();
        $(this).parent().parent().parent().fadeOut();
    });
    $('.btn-minimize').click(function(e) {
        e.preventDefault();
        var $target = $(this).parent().parent().next('.box-content');
        if ($target.is(':visible'))
            $('i', $(this)).removeClass('icon-chevron-up').addClass('icon-chevron-down');
        else
            $('i', $(this)).removeClass('icon-chevron-down').addClass('icon-chevron-up');
        $target.slideToggle();
    });
    $('.btn-setting').click(function(e) {
        e.preventDefault();
        $('#myModal').modal('show');
    });

}


function eventFire(el, etype) {
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}


