var UserPanelRequestCreator = function(){
    var data;
    return {
        postUserData : function($requestData){
           var that = this; 
           $.ajax({
            type: 'POST',
            url: baseUrl+''+baseBank+'/request.php',
            data: $requestData,
            success: function(response){
                 $userDeleteResposnse = $.parseJSON(response);
                 if($userDeleteResposnse){
                    performPostAction($userDeleteResposnse);
                 }
             }
           });
        }
    }; 
};

function performPostAction($responseData){
   
   $userId = $responseData['userId'];
   
   $responseText = $responseData['response'];
   if($responseText=='Deletion Successfull'){
     $('#image_'+$userId).css('display','none');
     $('#userData_'+$userId).hide("slow");
   }
}


var userAjaxRequest = UserPanelRequestCreator();
var isRequestedByClient = true;
$(document).ready(function(){
    $('.iphone-toggle').iphoneStyle({
        onChange : function(){
            if(isRequestedByClient){
                $userId  = $(this.elem).data('id');
                $checkedFlag = $(this.elem).is(':checked');
                $postData = {
                    'requestFor'    :  'update-user-availibily',
                    'requestParam'  :   {
                            'userId'      :  $(this.elem).data('id'),
                            'requestType' :  "user-updation"
                    }   
                }
                
                $confirm = confirm("Are you sure?");
                //console.log($confirm);
                if ($confirm === true){  
                    $userActivationResposnse =  userAjaxRequest.postUserData($postData);
                }else{     
                    isRequestedByClient = false;
                    if($checkedFlag){
                        $(this.elem).removeAttr('checked');
                    }else{
                        $(this.elem).attr('checked','checked');
                    }
                }
            }
            isRequestedByClient = true;
            //console.log("here");
        }
    });
 initializeListFilter("#searchField");
    
});

 $formData = {};
    $(document).ready(function(){
        
    });

function deleteUser($userId){
  if (confirm("Are you sure?")) {
      $('#image_'+$userId).css('display','inline'); 
      $postData = {
            'requestFor'    :  'update-user-availibily',
            'requestParam'  :   {
                    'userId'      :  $userId,
                    'requestType' :  "user-deletion"
            }   
     }
    userAjaxRequest.postUserData($postData);
    }
 }
    
function departmentVisibility($currentElement){
    $(".department-box").each(function(){
        if( $($currentElement).val() ){
            if( $(this).hasClass('dept-'+$($currentElement).val()) ){
                $(this).show();
            }else{
                $(this).hide();   
            }
        }else{
            $(this).show();
        }
    })
}

function userRolesSelect($currentElement)
{
    $("#user-region").prop('selectedIndex',0);
    $("#user-department").prop('selectedIndex',0);
    if($($currentElement).val()=='Department Admin'){
          $("#department-name").html("Department Name");
          $("#user-department").css("display","inline");
          $("#user-region").css("display","none");
     } else if($($currentElement).val()=='Guest'){
        // $("#user-department").prop("disabled", true);
            $("#department-name").html("Department Name");
            $("#user-department").css("display","none");
            $("#user-region").css("display","none");
     } else if($($currentElement).val()=='Super User'){
             $("#department-name").html("Department Name");
             $("#user-department").css("display","none");
             $("#user-region").css("display","none");
     } else if($($currentElement).val()=='Region User'){
            $("#department-name").html("Regional Office");
            $("#user-region").css("display","inline");
            $("#user-department").css("display","none");
     }else {
         $("#department-name").html("Department Name");
         $("#user-department").css("display","inline");
         $("#user-region").css("display","none");
   }
}

function userTypeVisibility($currentElement)
{
   $(".department-box").each(function(){
        //('#loder').fadeIn(100);
        if( $($currentElement).val() =='Department Admin'){
            $('.dept-1').show();$('.dept-2').show();$('.dept-3').show();$('.dept-4').show();$('.dept-5').show();$('.dept-6').show();$('.dept-123').hide();$('.dept-456').hide();$('.dept-678').hide(); 
           
          //$('#department-list').empty();
          /*
          
          setTimeout('$("#loder").hide()',1500);
          */
          $("#department-list").show();
          $('#loder').fadeIn(100);
            $('#loder').fadeOut(100);
         
            //$('#tableLoder').fadeIn(100);
            //$('#tableLoder').fadeOut(100);
         
         // $('#department-list').append( 
        // $('<option></option>').val('loading').html('....Loding....'),
      /*    $('<option></option>').val('').html('Select Department'),
          $('<option></option>').val('1').html('Admin'),
          $('<option></option>').val('2').html('ATM Operations'),
          $('<option></option>').val('6').html('Branch and Atm Support'),
          $('<option></option>').val('3').html('Branches'),
          $('<option></option>').val('5').html('Marketing'),
          $('<option></option>').val('4').html('Engineering')
          
         );*/ // $("#department-list option[value='loading']").remove();
           
            // $("select>option:eq('....Loding....')").fadeOut(100);
           
            
        } else  if( $($currentElement).val() =='Guest User'){
            $('.dept-1').hide();$('.dept-2').hide();$('.dept-3').hide();$('.dept-4').hide();$('.dept-5').hide();$('.dept-6').hide();$('.dept-123').show();$('.dept-456').hide();$('.dept-678').hide();
            $("#department-list").hide();
            //$('#loder').fadeIn(100);
           // $('#loder').fadeOut(100);
           /* 
            $('#department-list').empty();
            $('#department-list').append(
            $('<option></option>').val('Select Department').html('Select Department')
           );*/
        } else if( $($currentElement).val() =='Super Admin') {
            $('.dept-1').hide();$('.dept-2').hide();$('.dept-3').hide();$('.dept-4').hide();$('.dept-5').hide();$('.dept-6').hide();$('.dept-123').hide();$('.dept-456').show();$('.dept-678').hide();  
            $("#department-list").hide();
            /*$('#loder').fadeIn(100);
            $('#loder').fadeOut(100);
            
            $('#department-list').empty();
            $('#department-list').append(
            $('<option></option>').val('Select Department').html('Select Department')
           );*/
        } else if( $($currentElement).val() =='Region User'){
            $('.dept-1').hide();$('.dept-2').hide();$('.dept-3').hide();$('.dept-4').hide();$('.dept-5').hide();$('.dept-6').hide();$('.dept-123').hide();$('.dept-456').hide();$('.dept-678').show();
            $("#department-list").hide();
            /*$('#loder').fadeIn(100);
            $('#loder').fadeOut(100);
            
            $('#department-list').empty();
            $('#department-list').append(
            $('<option></option>').val('Select Department').html('Select Department')
            );*/
        } else {
            $(this).show();  
        }
    }) 
}

  
 function manageUser($userData)
 {
    $('#userupdationMessage').html('');
    $('#usercreationMessage').html('');
   
    $('#userupdationMessage').html('<img src="'+baseUrl+'public/img/ajax-loaders/ajax-loader-1.gif" title="Loading" style="/* margin-top:-4px; *//* display:none; *//* margin-left: -18px; */">');
    $('#usercreationMessage').html('<img src="'+baseUrl+'public/img/ajax-loaders/ajax-loader-1.gif" title="Loading" style="/* margin-top:-4px; *//* display:none; *//* margin-left: -18px; */">');
   
    $postData = {
     
     'requestFor'    :  'manage-user-roles',
     'requestParam'  :   {
            'departMentId'  : $userData['user-department'],
            'userEmail'     : $userData['user-email'],
            'userName'      : $userData['user-name'],
            'userPassword'  : $userData['user-password'],
            'sendMailStatus': $userData['send-email'],
            'currentStatus' : $userData['current-status'],
            'userOperation' : $userData['user-request'],
            'userId'        : $userData['user-id'],
            'userRole'      : $userData['user-roles'],
            'userRegionId'  : $userData['user-region'],
            'requestType'   : "user-profile-action"
           
     }   
    }
    
    $.ajax({
        type: 'POST',
        url: baseUrl+''+baseBank+'/request.php',
        data: $postData,
        success: function(response){
            $('#userupdationMessage').css('display','none');
            $('#usercreationMessage').css('display','none');
             $userProfileResponse = JSON.parse(response);
             if($userProfileResponse){
                performPostProfileAction($userProfileResponse);
             }
         }
       });
 }
function updateUserProfile($userData)
 {
    $('#userupdationMessage').html('');
    $('#usercreationMessage').html('');

    $('#userupdationMessage').html('<img src="'+baseUrl+'public/img/ajax-loaders/ajax-loader-1.gif" title="Loading" style="/* margin-top:-4px; *//* display:none; *//* margin-left: -18px; */">');
    $('#usercreationMessage').html('<img src="'+baseUrl+'public/img/ajax-loaders/ajax-loader-1.gif" title="Loading" style="/* margin-top:-4px; *//* display:none; *//* margin-left: -18px; */">');



    $postData = {

     'requestFor'    :  'update-user-profile',
     'requestParam'  :   {
         userFullName    : $formData['user-full-name'],
         emailAddress    : $formData['user-email'],
         userMobile      : $formData['user-mobile'],
         userSmsNubmer   : $formData['user-sms-number'],
         userCostCenter  : $formData['user-cost-center']
        }
    };

    $.ajax({
        type: 'POST',
        url: baseUrl+''+baseBank+'/request.php',
        data: $postData,
        success: function(response){
            $('#userupdationMessage').css('display','none');
            $('#usercreationMessage').css('display','none');
             $userProfileResponse = JSON.parse(response);
             if($userProfileResponse){
                performPostProfileAction($userProfileResponse);
             }
         }
       });
 }
 
 function performPostProfileAction($userProfileResponse)
 {
   $operationResponse = $userProfileResponse.operation; 
   $status = $userProfileResponse.status;
   console.log($operationResponse+"  "+$status);
   if($operationResponse=='UserUpdation' && $status=='Updated'){
       
        $('#userupdationMessage').css('display','inline');
        $('#userupdationMessage').html('<span class="label label-success" style="font-size:14px;">User Updated Successfully!</span>');
   }
   
   if($operationResponse=='UserCreation' && $status=='Created'){
        console.log("here");
        $('#usercreationMessage').css('display','inline');
        $('#usercreationMessage').html('<span class="label label-success" style="font-size:14px;">User Created Successfully!</span>');
   }
   
   if($operationResponse=='UserCreation' && $status=='Already Exist'){
       
        $('#usercreationMessage').css('display','inline');
        $('#usercreationMessage').html('<span class="label label-important" style="font-size:14px;">User Already Exist!</span>');
   }
   
}


 function validateFormData($formData,$type)
 {
       $departMentVal = $formData['user-department'];
       $emailAddress  = $formData['user-email'];
       $userNameVal   = $formData['user-name'];
       $userPassword  = $formData['user-password'];
       $userRole      = $formData['user-roles'];
       
       $isValidationError = 0;
      
      if($type=='profile'){
        if($userRole == 'Department Admin'){
            if($departMentVal==''){
                $('#deprtment_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#deprtment_error').css('display','none');
            }
        } else{
           $('#deprtment_error').css('display','none');
        }
      } 
     
     if($userRole==''){
             $('#userrole_error').css('display','inline');
             $isValidationError = 1;
      } else {
             $('#userrole_error').css('display','none');
      }
      
      if($emailAddress==''){
             $('#email_error').css('display','inline');
             $isValidationError = 1;
       } else{
             var emailRegularExpression = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
             if (!$emailAddress.match(emailRegularExpression)) {
               $('#email_error').html('Please Enter Valid Email Address!');
               $('#email_error').css('display','inline');
               $isValidationError = 1;
             } else{
                $('#email_error').css('display','none');
             }    
       }
       
       if($userNameVal==''){
             $('#username_error').css('display','inline');
             $isValidationError = 1;
       } else {
             $('#username_error').css('display','none');
             
       }
       
       if($userPassword==''){
           $('#userpassword_error').css('display','inline');
           $isValidationError = 1;
       } else {
           $('#userpassword_error').css('display','none');
       }
       //console.log($isValidationError);
       return $isValidationError == 0;
 }
function validateEditProfileFormData($formData)
 {
       $userFullName = $formData['user-full-name'];
       $emailAddress  = $formData['user-email'];
       $userMobile   = $formData['user-mobile'];
       $userSmsNubmer  = $formData['user-sms-number'];
       $userCostCenter      = $formData['user-cost-center'];

       $isValidationError = 0;



            if($userFullName==''){
                $('#user_full_name_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#user_full_name_error').css('display','none');
            }
            if($emailAddress==''){
                $('#user_email_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#user_email_error').css('display','none');
            }
            if($userMobile==''){
                $('#user_mobile_error').css('display','inline');
                $isValidationError = 1;
            } else {
                var mobileRegularExpression = new RegExp(/^[0-9]{8,10}$/);
                if (!$userMobile.match(mobileRegularExpression)) {
                    $('#user_mobile_error').html('Please enter upto 10 digit valid mobile number!');
                    $('#user_mobile_error').css('display','inline');
                    $isValidationError = 1;
                } else{
                    $('#user_mobile_error').css('display','none');
                }
            }
            if($userSmsNubmer==''){
                $('#sms_number_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#sms_number_error').css('display','none');
            }
            if($userCostCenter==''){
                $('#user_cost_center_error').css('display','inline');
                $isValidationError = 1;
            } else {
                var numberRegularExpression = new RegExp(/^\d+$/);
                if (!$userCostCenter.match(numberRegularExpression)) {
                    $('#user_cost_center_error').html('Please enter valid cost center! Only number please.');
                    $('#user_cost_center_error').css('display','inline');
                    $isValidationError = 1;
                } else{
                    $('#user_cost_center_error').css('display','none');
                }
            }


      if($emailAddress==''){
             $('#user_email_error').css('display','inline');
             $isValidationError = 1;
       } else{
             var emailRegularExpression = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
             if (!$emailAddress.match(emailRegularExpression)) {
               $('#user_email_error').html('Please enter valid email address!');
               $('#user_email_error').css('display','inline');
               $isValidationError = 1;
             } else{
                $('#user_email_error').css('display','none');
             }
       }

       //console.log($isValidationError);
       return $isValidationError == 0;
 }
 
 function validateCreateFormData($formData,$type)
 {
       
       $departMentVal = $formData['user-department'];
       $emailAddress  = $formData['user-email'];
       $userNameVal   = $formData['user-name'];
       $userPassword  = $formData['user-password'];
       $userRole      = $formData['user-roles'];
       $userRegion    = $formData['user-region'];
       
       $isValidationError = 0;
       
       if($type=='create'){
        if($userRole == 'Department Admin'){
            if($departMentVal==''){
                $('#deprtment_error').html('Please select department');
                $('#deprtment_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#deprtment_error').css('display','none');
            }
        } else if($userRole == 'Region User'){
            if($userRegion == ''){
                $('#deprtment_error').html('Please select regional office');
                $('#deprtment_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#deprtment_error').css('display','none');
            }
        }else{
           $('#deprtment_error').css('display','none');
        }
      } 
      
      if($userRole==''){
             $('#userrole_error').css('display','inline');
             $isValidationError = 1;
      } else {
             $('#userrole_error').css('display','none');
      }
     
      if($emailAddress==''){
             $('#email_error').css('display','inline');
             $isValidationError = 1;
       } else{
             var emailRegularExpression = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
             if (!$emailAddress.match(emailRegularExpression)) {
               $('#email_error').html('Please Enter Valid Email Address!');
               $('#email_error').css('display','inline');
               $isValidationError = 1;
             } else{
                $('#email_error').css('display','none');
             }    
       }
       
       if($userNameVal==''){
             $('#username_error').css('display','inline');
             $isValidationError = 1;
       } else {
             $('#username_error').css('display','none');
             
       }
       
       if($userPassword==''){
           $('#userpassword_error').css('display','inline');
           $isValidationError = 1;
       } else {
           $('#userpassword_error').css('display','none');
       }
       
       return $isValidationError == 0;
 }


 function initializeListFilter($parentInputBox){
    $($parentInputBox).val("");
    $($parentInputBox).keyup(function () {
                //first we create a variable for the value from the search-box
                var searchTerm = $($parentInputBox).val();
            
                //then a variable for the list-items (to keep things clean)
                var listItem = $('.result-item').children('span');
                
                //extends the default :contains functionality to be case insensitive
                //if you want case sensitive search, just remove this next chunk
                $.extend($.expr[':'], {
                  'containsi': function(elem, i, match, array)
                  {
                    return (elem.textContent || elem.innerText || '').toLowerCase()
                    .indexOf((match[3] || "").toLowerCase()) >= 0;
                  }
                });//end of case insensitive chunk
            
            
                //this part is optional
                //here we are replacing the spaces with another :contains
                //what this does is to make the search less exact by searching all words and not full strings
                var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
                
                if(searchSplit){
                    //here is the meat. We are searching the list based on the search terms
                    $(".result-item .item").not(":containsi('" + searchSplit + "')").each(function(e)   {
                          $(this).removeClass('highlight');
                          //add a "hidden" class that will remove the item from the list
                          $(this).parent().parent().addClass('hidden');
                    });
                    
                    //this does the opposite -- brings items back into view
                    $(".result-item .item:containsi('" + searchSplit + "')").each(function(e) {
                          //remove the hidden class (reintroduce the item to the list)
                          $(this).addClass('highlight');
                          $(this).parent().parent().removeClass('hidden');
                    });
                }else{
                    $(".result-item .item").each(function(e) {
                          //remove the hidden class (reintroduce the item to the list)
                          $(this).removeClass('highlight');
                          $(this).parent().parent().removeClass('hidden');
                    });
                }
        });
}