alert('i am still here');
var bankCode,bankSurveyDate,bankLatitude,bankLongitude;
function calculateChar(mainStr){
   if((String(mainStr).split("")).length==1){
      return '&nbsp;&nbsp;&nbsp;&nbsp;';
   }else{
      if((String(mainStr).split("")).length==2)
         return '&nbsp;&nbsp;';
      else
         return '&nbsp;';
   }
}


//loads the bank data from fusion table for dashboard map
//function getBankBranches(where){
function getBankBranches(data){
//   if(!where) 
//      where = "'Name'+=+'"+bankName+"'";
//         
//   var url = "http://www.google.com/fusiontables/api/query?sql=SELECT+BranchName,Location,BranchCode,BranchType+FROM+4033903+";
//   if (where != null && where != undefined && where != "") 
//      url += "WHERE+" + where+"+";
//   url += "ORDER+BY+BranchCode+ASC+&jsonCallback=?";
//   $.getJSON(url,function(data){
   
      //loader for branch loading

      
      var fusionRows = data.table.rows;
      var addressBranch;
      $content = '<ul class="dashboard-sublist">';
      for(i in fusionRows){
         if(fusionRows[i][7] && fusionRows[i][2]){
            addressBranch = fusionRows[i][7];
            if(addressBranch.length>30){
               addressBranch = addressBranch.substring(0,30);
               addressBranch = addressBranch+"..."
            }
            if(fusionRows[i][6]){
               switch(fusionRows[i][6]){
                  case 'Ladies':
                     $content += '<li id="bankCode_'+fusionRows[i][2]+'" style="height:25px;"><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+addressBranch+'</span></a>'+
                     '<i>'+fusionRows[i][2]+'</i>'+calculateChar(fusionRows[i][2])+'<a href="index.php?code='+fusionRows[i][2]+'&type=Ladies" target="_blank"><img src="../img/ladies.jpg"  title="Ladies Branch" style="height:20px;width:20px;"/></a>'+    
                     '</li>';
                     break;
                  case 'Gents':
                     $content += '<li id="bankCode_'+fusionRows[i][2]+'" style="height:25px;"><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+addressBranch+'</span></a>'+
                     '<i>'+fusionRows[i][2]+'</i>'+calculateChar(fusionRows[i][2])+'<a href="index.php?code='+fusionRows[i][2]+'&type=Man" target="_blank"><img src="../img/men.jpg"   title="Gents Branch" style="height:20px;width:20px;"/></a>'+    
                     '</li>';
                     break;
                  case 'Gents and Ladies':
                     $content += '<li id="bankCode_'+fusionRows[i][2]+'" style="height:25px;"><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+addressBranch+'</span></a>'+
                     '<i>'+fusionRows[i][2]+'</i>'+calculateChar(fusionRows[i][2])+'<a href="index.php?code='+fusionRows[i][2]+'&type=Man" target="_blank"><img src="../img/men.jpg"   title="Gents Branch" style="height:20px;width:20px;"/></a>&nbsp;<a href="index.php?code='+fusionRows[i][2]+'&type=Ladies" target="_blank"><img src="../img/ladies.jpg"  title="Ladies Branch" style="height:20px;width:20px;"/></a>'+    
                     '</li>';
                     break;
                  default:
                     $content += '<li id="bankCode_'+fusionRows[i][2]+'" style="height:25px;"><a href="index.php?code='+fusionRows[i][2]+'" target="_blank"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+addressBranch+'</span></a>'+
                     '<i>'+fusionRows[i][2]+'</i>'+    
                     '</li>';
                     break;

               }
            }else{
               $content += '<li id="bankCode_'+fusionRows[i][2]+'" style="height:25px;"><a href="index.php?code='+fusionRows[i][2]+'" target="_blank"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+addressBranch+'</span></a>'+
               '<i>'+fusionRows[i][2]+'</i>'+    
               '</li>';
            }
         } 
                    
         if(fusionRows[i][0]){   
            var latLng =(fusionRows[i][0]).split(',');
            bankLatitude = latLng[0];
            bankLongitude = latLng[1]; 
         //createMarker(new google.maps.LatLng(bankLatitude,bankLongitude));
         }
                    
      }
      $content += '</ul>' 
      $("#overview").html($content);
      if($("#overview").height()>500){
         $(".scrollbar").show();
         $(".scrollbar").height(500);
         $(".viewport").height(100);
         $(".scrollbar").show();
         $('#scrollbar2').tinyscrollbar({
            sizethumb: 45
         });
         $('#scrollbar2').attr('hasScroll','1');
      }else{
         $(".scrollbar").hide();
      }
              
      $("#loader").hide();
   //});
}

//this function show/hide loader images between AT/Bank tab switching over branch list on right panel
function ajaxRequestLoader(loaderType)
{
   $(".scrollbar").hide();
   $("#overview").html(
               '<div align="center" style="margin-top:180px;margin-left:20px;">'+
               '<div align="center" style="font-weight: bold;padding-bottom:10px;">Loading '+loaderType+'</div>'+
               '<img title="Loader" src="'+baseUrl+'public/img/ajax-loaders/bsfmap-loader1.gif">'+
            '</div>'
         )
}