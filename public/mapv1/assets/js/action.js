//***********************************************************************************************//
//            Author :  SIMBA
//            Content : Custom Javascript for sidebar
//            Date : 2016.11.24 (Thanksgiving day)
//***********************************************************************************************//

function resetMapOnSwitching() {
  $('.sidebar-census-select').val('').trigger('change');
  $("#city_branch").removeAttr("disabled");
  $(".counterClass").each(function() {
     $(this).html("&nbsp(0)");
  });

  $("#map-data-go").trigger('click');
}

function showNetworkTab() {
  $(".sidebar-form").show();
  $(".sidebar-contest").hide();
  $(".sidebar-network").hide();
  $(".sidebar-incident").hide();
  $(".sidebar-census").hide();
  $(".sidebar-landing").show();
}

function titleCase(str) {
    str = str.toLowerCase().split(' ');
    for (var i = 0; i < str.length; i++) {
        str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    return str.join(' ');
}

$(document).ready(function(){

  $(".sidebar-landing").show();
  $(".sidebar-network").hide();
  $(".sidebar-incident").hide();
  $(".sidebar-census").hide();
  $(".sidebar-contest").hide();

  $("#nav-network").click(function(){
    // $(".sidebar-form").show();
    // $(".sidebar-contest").hide();
    // $(".sidebar-landing").hide();
    // $(".sidebar-incident").hide();
    // $(".sidebar-census").hide();
    // $(".sidebar-network").show();

    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-network").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").hide();
    $(".sidebar-landing").show();

    resetMapOnSwitching();
  });

  $("#nav-atm-detail").click(function(){
    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-landing").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").hide();
    $(".sidebar-network").show();

  });



  $(".nav-network").click(function(){
    // $(".sidebar-form").show();
    // $(".sidebar-contest").hide();
    // $(".sidebar-landing").hide();
    // $(".sidebar-incident").hide();
    // $(".sidebar-census").hide();
    // $(".sidebar-network").show();

    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-network").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").hide();
    $(".sidebar-landing").show();
    getMapData('#map-filters-div', 'map-data-go', false);
  });

  $("#nav-incidents").click(function(){
    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-landing").hide();
    $(".sidebar-network").hide();
    $(".sidebar-census").hide();
    $(".sidebar-incident").show();

    resetMapOnSwitching();
  });

  $(".nav-incidents").click(function(){
    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-landing").hide();
    $(".sidebar-network").hide();
    $(".sidebar-census").hide();
    $(".sidebar-incident").show();
  });

  $("#nav-census").click(function(){
    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-landing").hide();
    $(".sidebar-network").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").show();

    resetMapOnSwitching();
  });

  $(".nav-census").click(function(){
    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-landing").hide();
    $(".sidebar-network").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").show();

  });

  $(".sidebar-header-logo").click(function(){
    $(".sidebar-form").show();
    $(".sidebar-contest").hide();
    $(".sidebar-network").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").hide();
    $(".sidebar-landing").show();
    getMapData('#map-filters-div', 'map-data-go', false);
  });

  $("#nav-contest").click(function(){
    $(".sidebar-network").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").hide();
    $(".sidebar-landing").hide();
    $(".sidebar-form").hide();
    $(".sidebar-contest").show();

    resetMapOnSwitching();
  });

  $(".nav-contest").click(function(){
    $(".sidebar-network").hide();
    $(".sidebar-incident").hide();
    $(".sidebar-census").hide();
    $(".sidebar-landing").hide();
    $(".sidebar-form").hide();
    $(".sidebar-contest").show();
  });

});
