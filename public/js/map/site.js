/**ArrayObject()
 *  create array object and
 *  defines array related functions
 */
function ArrayObject() {
    this._array = [];

    this.add = function (elm) { this._array.push(elm); }

    this.remove = function (elm, key) {
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { var a = this._array[i]; this._array.splice(i, 1); return a; } } return null; }
        else { for (i in this._array) { if (this._array[i][key] == elm) { var a = this._array[i]; this._array.splice(i, 1); return a; } } return null; }
    }

    this.clear = function () { this._array.length = 0; }

    this.count = function () { return this._array.length; }

    this.contains = function (elm, key) { return this.get(elm, key) != null; }

    this.get = function (elm, key) {
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { return this._array[i]; } } return null; }
        else { for (i in this._array) { if (this._array[i][key] == elm) { return this._array[i]; } } return null; }
    }

    this.getAll = function (elm, key) {
        var items = [];
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { return this._array[i]; } } return null; }
        else { for (i in this._array) { if (this._array[i][key] == elm) {items.push(this._array[i]); } } return items; }
    }

    this.update = function (elm, key) {
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { this._array[i] = elm; } } }
        else { for (i in this._array) { if (this._array[i][key] == elm) { this._array[i] = elm; } } }
    }

    this.All = function () {
        return this._array;
    }
}

function AtmObject() {
    this.Location = null;
    this.Image = null;
    this.BranchCode = null;
    this.Type = null;
    this.Address = null;
    this.BranchName = null;
    this.BranchType = null;
    this.Central = null;
}

function isBlank(str) { if (Number(str)) return false; return (str == undefined || $.trim(str) === ""); }