var globalCallbackFunction = null;
var RequestCreator = function(){
    return {
        initialize  : function($requestData, $returnFunction, $fillElement,$otherData){
           var that = this; 
           var requestedFile = "request.php";
           if(baseBank == 'samaatm' || baseBank == "samaatm_v1" || baseBank == "samaatm_v2" || baseBank == "samaatm_v3" || baseBank == "samaatm_qa"){
               
               requestedFile = "samarequest.php"
           }
           
           $.post(baseUrl+''+baseBank+'/'+requestedFile, $requestData,
                function($result){
                    if($result.trim() == 'invalidsession'){
                        if(baseBank == 'samaatm' || baseBank == "samaatm_v1" || baseBank == "samaatm_v2" || baseBank == "samaatm_v3" || baseBank == "samaatm_qa"){
                            window.location = baseUrl+''+baseBank+'/'+'login.php';
                        }else{
                            window.location = 'login.php';
                        }
                        
                    }
                    

                    if($fillElement){
                        that.fillHtml($fillElement, $result, $returnFunction, $requestData)
                    }else{
                        if($returnFunction)
                            window[$returnFunction]($result,$otherData);
                        else
                        {//alert($result);
                            
                        }
                    }
                }
           );
        },
        
        initialize1  : function($requestData,$returnFunction,$fillElement){
           var that = this; 
           var requestedFile = "request.php";
           if(baseBank == 'samaatm' || baseBank == "samaatm_v1" || baseBank == "samaatm_v2" || baseBank == "samaatm_v3" || baseBank == "samaatm_qa"){
              
               requestedFile = "samarequest.php"
           }
             $.post(baseUrl+''+baseBank+'/'+requestedFile,$requestData,
                function($result){
                    if($fillElement){
                        that.fillHtml($fillElement,$result,$returnFunction)
                    }else{
                        if($returnFunction)
                            $returnFunction.call($result);
                        else
                            alert($result);
                    }
                }
           );
        },
        
        fillHtml : function($element, $result, $htmlInitializers, $requestData){
            
            if(baseBank == 'samaatm' || baseBank == "samaatm_v1" || baseBank == "samaatm_v2" || baseBank == "samaatm_v3" || baseBank == "samaatm_qa"){
                if(typeof $requestData != 'undefiend'){
                    $requestData['drill-pagination'] = "";
                    $("#sama_RequestData_fordownload").val(JSON.stringify($requestData));
                }
            }else{
                if(typeof $requestData != 'undefiend'){
                    $requestData['drill-pagination'] = "";
                    $("#requestData_fordownload").val(JSON.stringify($requestData));
                }
            }
            
           
            
            $($element).html($result);
            if( typeof $htmlInitializers == 'function' ){
                globalCallbackFunction = $htmlInitializers;
                $htmlInitializers = "globalCallbackFunction";
            }
            if($htmlInitializers){
                setTimeout(function(){
                    window[$htmlInitializers]();
                },100);
            }
        }
   }; 
};


