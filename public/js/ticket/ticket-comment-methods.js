//--------------------------------------------- Function related to comment section ----------------------------------------//

$commentParamObject = new commentParam();

function commentParam(){
    this.saveComment = function($relatedImageId){
        if($("#comment_box_"+$relatedImageId).val()){
            $requestData = {
                    "requestFor"     :   "ticket-comment-action",
                    "ticket-param"   :  { 
                        "action"         :   "ADD-COMMENT",
                        "content"        :   $("#comment_box_"+$relatedImageId).val(),
                        "relatedId"      :   $relatedImageId   
                    }
            }
            $("#comment_box_"+$relatedImageId).val("");
            this.sendRequest($requestData,"updateCommentBox",null);
            
            imageCommentCount('saveComment',$relatedImageId);
        }else{
            $("#comment_loader").hide();
            alert("No comment found");
        }
    }
    
    this.clearComment = function($relatedImageId){
        $("#comment_loader").hide();
        $("#comment_box_"+$relatedImageId).val("");
        
    }
    
    this.deleteComment  =  function($relatedCommentId){
        $relatedId = this.saparateId($relatedCommentId);
        $requestData = {
                "requestFor"     :   "ticket-comment-action",
                "ticket-param"   :  {   
                        "action"         :   "DELETE-COMMENT",
                        "relatedId"      :   $relatedId.commentId,
                        "imageId"        :   $relatedId.imageId 
                } 
        }
        
        this.sendRequest($requestData,"updateCommentBox",null);
        $("#comment_loader").hide();
         imageCommentCount('deleteComment',$relatedId.imageId);
    }
    
    this.updateComment  =   function($relatedCommentId){
        $relatedId = this.saparateId($relatedCommentId);
        if($("#comment_edit_box_"+$relatedId.imageId+"_"+$relatedId.commentId).val()){
            $requestData = {
                    "requestFor"     :   "ticket-comment-action",
                    "ticket-param"   :  { 
                        "action"         :   "UPDATE-COMMENT",
                        "content"        :   $("#comment_edit_box_"+$relatedId.imageId+"_"+$relatedId.commentId).val(),
                        "relatedId"      :   $relatedId.commentId,
                        "imageId"        :   $relatedId.imageId  
                    }
                        
            }
            $("#comment_text_span_"+$relatedId.imageId+"_"+$relatedId.commentId).html($("#comment_edit_box_"+$relatedId.imageId+"_"+$relatedId.commentId).val());
            this.cancelEdit($relatedCommentId); 
            this.sendRequest($requestData,"updateCommentBox",null);
            $("#comment_loader").hide();
        } else {
            alert("Nothing to update, Restoring old data");
            $("#comment_edit_box_"+$relatedId.imageId+"_"+$relatedId.commentId).val($("#comment_text_span_"+$relatedId.imageId+"_"+$relatedId.commentId).html());
            $("#comment_loader").hide();
        }   
    }
    
    this.editComment    =   function($relatedCommentId){
        $relatedId = this.saparateId($relatedCommentId);
        $("#comment_edit_box_"+$relatedId.imageId+"_"+$relatedId.commentId).val($("#comment_text_span_"+$relatedId.imageId+"_"+$relatedId.commentId).html());
        $("#comment_span_"+$relatedId.imageId+"_"+$relatedId.commentId).hide();
        $("#comment_edit_span_"+$relatedId.imageId+"_"+$relatedId.commentId).show();
        $("#comment_loader").hide();
        initializeCommentScrollbar();
    }
    
    this.cancelEdit     =   function($relatedCommentId){
        $relatedId = this.saparateId($relatedCommentId);
        $("#comment_edit_box_"+$relatedId.imageId+"_"+$relatedId.commentId).val($("#comment_text_span_"+$relatedId.imageId+"_"+$relatedId.commentId).html());
        $("#comment_span_"+$relatedId.imageId+"_"+$relatedId.commentId).show();
        $("#comment_edit_span_"+$relatedId.imageId+"_"+$relatedId.commentId).hide();
        $("#comment_loader").hide();
        initializeCommentScrollbar();
    }
    
    this.saparateId     =   function($relatedId){
        $relatedIds = $relatedId.split("_");
        return {
            "imageId"   :   $relatedIds[0],
            "commentId" :   $relatedIds[1],
        }
        
    }
    
    this.countComments  =   function($imageId){
        $totalComments = 0;
        $("#comments_section_"+$imageId).find('li.comment-box-section').each(function(){
            $totalComments++;
        })
        if($totalComments){
            $("#loaded_comment_span").html($totalComments);
        }else{
            $("#loaded_comment_span").html(0);
            $("#total_comment_span").html(0);
            $("#comment_count_div").hide();
        }
    }
    
    this.loadComment = function($imageId){
        $requestData = {
                "requestFor"     :   "ticket-comment-action",
                "ticket-param"   :  { 
                    "action"        :   "LOAD-COMMENT",
                    "relatedId"     :   $imageId,
                    "fromPage"      :   Number($("#loaded_comment_span").html())
                }
        };
        this.sendRequest($requestData,"updateCommentBox",null);
    }
    
    this.sendRequest  = function($requestData,$forwardFunction){
        $requestObject.initialize($requestData,$forwardFunction,null);
    }
}

function updateCommentBox($result){
    $("#comment_loader").hide();
    $result = JSON.parse($result);
    if($result){
        $("#total_comment_span").html($result.totalComments);
        switch($result.requestDetail){
            case "ADD-COMMENT"      : $("#comments_section_"+$result.imageId).html($result.content);break;
            case "DELETE-COMMENT"   : $("#comment_span_"+$result.imageId+"_"+$result.commentId).parent().remove();break;
            case "UPDATE-COMMENT"   : break; 
            case "LOAD-COMMENT"     : $("#commentLoaderLi").remove(); $("#comments_section_"+$result.imageId).append($result.content); break; 
        }
    }
    $commentParamObject.countComments($result.imageId);
    initializeCommentScrollbar();
}

function handleCommentAction($relatedId,$requestFor){
      $("#comment_loader").show();
      switch($requestFor.toUpperCase()){
            case "ADD-COMMENT"      :   $commentParamObject.saveComment($relatedId);break;
            case "DELETE-COMMENT"   :   (confirm('Are you sure!'))?$commentParamObject.deleteComment($relatedId):$("#comment_loader").hide();break;
            case "CANCEL-ADD"       :   $commentParamObject.clearComment($relatedId);break;
            case "CANCEL-UPDATE"    :   $commentParamObject.cancelEdit($relatedId);break;
            case "UPDATE-COMMENT"   :   $commentParamObject.updateComment($relatedId);break;
            case "EDIT-COMMENT"     :   $commentParamObject.editComment($relatedId);break;
            case "LOAD-COMMENT"     :   $commentParamObject.loadComment($relatedId);break;
        }
}


function initializeCommentScrollbar(){
    
    if($('#comment_container_scrollbar').length){
        $('#comment_container_scrollbar').tinyscrollbar({
                    sizethumb: 40
        });
    }else{
        $('#comment_container_scrollbar_small').tinyscrollbar({
                    sizethumb: 40
        });
    }
   
}

function imageCommentCount($action, $imageId) {
    TotalComments = $("#cotantgallary .image_Comments_Count_" + $imageId).text();
    if ($action == 'saveComment') {
        TotalComments++;
    }
    else if ($action == 'deleteComment') {
        --TotalComments;
    }
    $("#cotantgallary .image_Comments_Count_" + $imageId).text(TotalComments);
}
//---------------------------------------------End Function Related To Comment Section ----------------------------------------//