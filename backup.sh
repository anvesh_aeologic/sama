#!/bin/bash

mkdir -p sql_database_auto_backup/{4c360,ughi,sama}
OUTPUT="sql_database_auto_backup"

FOURCOUTPUT="sql_database_auto_backup/4c360"
databases=`mysql -h localhost -e "SHOW DATABASES;" | tr -d "| " | egrep -v 'information_schema|mysql|performance_schema|Database|4c360_alrajhi_vm_backup_12_July_2016|4c360_alrajhi_vm_backup_29June|4c360_ncb_28-04-16|4c360_ncb_rebranding_db_backup|4c360_qsr_6may|arbbranches_v2_db_backup_may18_2016|cloud9_v1_db_back_up|ncbatm_v7_db_4_oct|ncbatmBackup28032016|sama_db_backup|sama_v1_db_back_up|sama_v2_db_backup|ughi_v4_db_13_nov_16|ughi_v4_db_27dec|ughi_v4_db_backup_may04_16|ughi_v3_db|ughi_v4_db|sama_atm_db|sama_qa_db|sama_v1_db|sama_v2_db'`
for db in $databases; do
    mysqldump -h localhost --force --opt --databases $db > $FOURCOUTPUT/$db.sql
done

UGHIOUTPUT="sql_database_auto_backup/ughi"
ughidatabases=`mysql -h ughi.4c360.solutions -e "SHOW DATABASES;" | tr -d "| " | egrep -v 'information_schema|mysql|performance_schema|Database'`
for ughidb in $ughidatabases; do
    mysqldump -h ughi.4c360.solutions --force --opt --databases $ughidb > $UGHIOUTPUT/$ughidb.sql
done

SAMAOUTPUT="sql_database_auto_backup/sama"
samadatabases=`mysql -h sama.4c360.solutions -e "SHOW DATABASES;" | tr -d "| " | egrep -v 'information_schema|mysql|performance_schema|Database|cphulkd|leechprotect|modsec|roundcube|sama_qa_db1|sama_v1_db1|sama_v2_db1'`
for samadb in $samadatabases; do
    mysqldump -h ughi.4c360.solutions --force --opt --databases $samadb > $SAMAOUTPUT/$samadb.sql
done

rm weekly_db_backup/*zip > /dev/null 2>&1
zip -r -P 4c#labs weekly_db_backup/AllDbBackup.zip $OUTPUT/*
rm -rf $OUTPUT

