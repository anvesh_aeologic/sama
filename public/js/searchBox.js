(function($){ 
    $.fn.searchbox = function(options) { 
        options = $.extend({}, $.fn.searchbox.defaultOptions, options);
        var me = this;
        var elementValue;
        var element;
        var mainTime;
        var selectedElements = [];
        var elementId ;
        var oldValue;
        this.init = function(){
            this.each(function() {
                element = $(this);
                element.find('option').each(function(){
                    elementValue = this;
                    oldValue = $(this).text();
                    $(this).css('display','none');
                });
            });
            element = $(this);
            elementId = $(me).attr("id");
            var mainElement = document.createElement('span');
            mainElement.style['width'] = '280px'; 
            mainElement.style['cursor'] = 'pointer'; 
            $(mainElement).bind( "mouseover", function( event ){ 
                me.setTimer(0);
            });
            $(mainElement).bind( "mouseout", function( event ){ 
                me.setTimer(3600);
            });
            var subElement = document.createElement('span');
            subElement.id = "searchboxValues_"+$(me).attr("id");
            subElement.style['width'] = '280px'; 
            subElement.style['min-height'] = '20px'; 
            subElement.style['height'] = '32px'; 
            subElement.style['cursor'] = 'pointer'; 
            subElement.style['display'] = 'none'; 
            subElement.style['zIndex'] = '99999'; 
            subElement.style['position'] = 'absolute'; 
            
            subElement.style['border'] = '1px solid #BBBBBB'; 
            subElement.style['background'] = 'white';
            var subList = document.createElement('ul');
            $(subList).attr('class',"dashboard-sublist");
            subList.style['width'] = '270px';
            var subListElement = document.createElement('li');   
            subListElement.className = 'last'; 
            var inputbox = document.createElement('input');
            inputbox.id = $(me).attr("id")+"_inputbox";
            inputbox.type = "text";
            inputbox.value = "";
            inputbox.style["width"] = "180px";
            subListElement.appendChild(inputbox);
            var e = document.createElement("span");
            e.innerHTML = "&nbsp;";
            subListElement.appendChild(e);
            var buttonbox =  document.createElement('button');
                buttonbox.className = 'btn btn-mini';
                buttonbox.style['marginBottom'] = '10px';
               buttonbox.style['position'] = 'relative';
            var spanbox = document.createElement('span');
                spanbox.className = "icon icon-green icon-check";
            buttonbox.appendChild(spanbox);
            subListElement.appendChild(buttonbox);
            subList.appendChild(subListElement);  
            subElement.appendChild(subList);
            subElement.appendChild(subList);
            mainElement.appendChild(subElement);
            var e = document.createElement("span");
            e.innerHTML = "&nbsp;";
            subListElement.appendChild(e);
            subList.appendChild(subListElement);  
            subElement.appendChild(subList);
            subElement.appendChild(subList);
            mainElement.appendChild(subElement);
            
            var buttonbox1 =  document.createElement('button');
                buttonbox1.className = 'btn btn-mini';
                buttonbox1.style['marginBottom'] = '10px';
               buttonbox1.style['position'] = 'relative';
            var spanbox1 = document.createElement('span');
                spanbox1.className = "icon icon-red icon-close";
            buttonbox1.appendChild(spanbox1);
            subListElement.appendChild(buttonbox1);
            subList.appendChild(subListElement);  
            subElement.appendChild(subList);
            subElement.appendChild(subList);
            mainElement.appendChild(subElement);
                 
    
            element.parent().append(mainElement);
            $(buttonbox).bind( "click", function( event ){ 
                if($("#"+$(me).attr("id")+"_inputbox").attr('value')){
                    var thisValue = $("#"+$(me).attr("id")+"_inputbox").attr('value')
                    $(elementValue).attr('value',thisValue);
                    if(thisValue.length>10){
                        $(elementValue).text((thisValue).substr(0,10)+"..");
                    }else{
                        $(elementValue).text(thisValue);
                        
                    }
                    
                }else{
                    $(elementValue).attr('value','');
                    $(elementValue).text(oldValue);
                }
                me.showHideDropDown('hide');
            });
            
            $(buttonbox1).bind( "click", function( event ){ 
                 
                 $("#"+$(me).attr("id")+"_inputbox").attr('value',"");
                 $(elementValue).attr('value','');
                 $(elementValue).text(oldValue);
                 me.showHideDropDown('hide');
            });
            
            $(this).bind( "click", function( event ){ 
                me.showHideDropDown();
            });
            return this;
        }
        
        
        this.showHideDropDown = function($type){
            if(!$type){
                if($("#searchboxValues_"+$(this).attr("id")).css('display') == 'none'){
                    $("#searchboxValues_"+$(this).attr("id")).css('display','block');
                    
                }else{
                    $("#searchboxValues_"+$(this).attr("id")).css('display','none');
                     
                }
            }
            if($type == 'hide'){
                $("#searchboxValues_"+$(this).attr("id")).css('display','none');
            }
        };
        
      
        this.setTimer = function(currentTime){
            mainTime = currentTime;
            if(mainTime){
                setTimeout(function(){
                    if(mainTime){
                        me.showHideDropDown('hide');
                    }
                },currentTime);
            }
        };
        return this.init();
    }
    $.fn.searchbox.defaultOptions = {
        class: 'searchbox'
    } 
})(jQuery);