var currentGraphToEmail = null;
function downloadDashboardGraph(type,imageSource,imageName, folderName){
        var typeInput = document.createElement("input") ;
        typeInput.setAttribute("name", 'type') ;
        typeInput.setAttribute("value", type);
        typeInput.setAttribute("type", "hidden");
        
        var dataInput = document.createElement("input") ;
        dataInput.setAttribute("name", 'imgdata') ;
        dataInput.setAttribute("value",$(imageSource).attr('src'));
        dataInput.setAttribute("type", "hidden");
         
        var nameInput = document.createElement("input") ;
        nameInput.setAttribute("name", 'name') ;
        nameInput.setAttribute("value",imageName);
        
        if(folderName === undefined || !folderName) {
            folderName = 'riyadDashboard_V2.1';
        }
        
        var folderNameInput = document.createElement("input") ;
        folderNameInput.setAttribute("name", 'folder_name') ;
        folderNameInput.setAttribute("value",folderName);
         
        var myForm = document.createElement("form");
        myForm.method = 'post';
        myForm.action = baseUrl+'helper/graphActionModule.php';
             
        myForm.appendChild(typeInput);
        myForm.appendChild(dataInput);
        myForm.appendChild(nameInput);
        myForm.appendChild(folderNameInput);
         
        document.body.appendChild(myForm) ;
        myForm.submit() ;
        document.body.removeChild(myForm) ;
}

function emailDashboardGraph($folder_name){
    var graphMessage  = currentGraphToEmail.attr('graph-message');
    
    if($("#graphMessage").attr('value')){
        graphMessage  = $("#graphMessage").attr('value'); 
    }
    $.post(
            '../helper/graphActionModule.php',
            {
                "type"        :   'email',
                "title"       :   currentGraphToEmail.attr('graph-title'),
                "message"     :   graphMessage,
                "imgdata"     :   $("#"+currentGraphToEmail.attr('graph-link')).attr('src'),
                "name"        :   currentGraphToEmail.attr('graph-image'),
                "email"       :   $("#graphEmailAddress").attr('value'),
                "folder_name" :   $folder_name
            },function(result){
                   $("#graphEmailLoader").hide(); 
                   $("#graphEmailSuccess").show();
                   $("#graphEmailSuccessMsg").show(); 
            }
    );
}