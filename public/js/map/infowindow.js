var infoWindows = [];
var infobubble;
//fusionObjectList

function clearInfoWindows() {
    for (i in infoWindows) {
        infoWindows[i].setMap(null);
    }
    infoWindows.length = 0;
}


 var _currentWindow;
function openInfoWindow(obj,marker) {
    clearInfoWindows();
    infobubble = new InfoBubble({maxWidth: 500,borderColor: '#CCCCCC'});
    infobubble.setContent(infoWindowHtml(obj,0));
    infobubble.open(map, marker);
    _currentWindow = infobubble; 
    infoWindows.push(infobubble);    
} 

var _currentWindow;

/* function openInfoWindow(obj,marker) {
    alert('here');
    clearInfoWindows();
    infobubble = new InfoBubble({maxWidth: 500,borderColor: '#CCCCCC'});
   
    infobubble.addTab('Branch Info', infoWindowHtml(obj,0));
    infobubble.addTab('Center Info',tabInfo1());
      
    infobubble.setContent(infoWindowHtml(obj,0));
    infobubble.open(map, marker);
    _currentWindow = infobubble; 
    infoWindows.push(infobubble);    
} */



function infoWindowHtml(obj, index) {
    var html = '';
    var pagination = '';
    var length = 0;
    var atmStyle = '';
    
    length = fusionObjectList.getAll(obj.Location, "Location").length;
    
    if (length > index + 1 || index > 0) {
        atmStyle =  'height:70px;';
        pagination += createPagination(length, index, obj);
    }
    
    if(obj.Type === 'ATM'){
        html= "<div style='"+atmStyle+"width:100%'>";
        html += "<div class='title' align='center'>ATM Code&nbsp;:&nbsp;" + obj.BranchCode + "&nbsp;</div>";        
    } else{
        //html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + "<div style='height:165px;'><div style='float:left;'><img src='" + imagePreview(obj.Image) + "' style='height:161px;width:180px;'/></div><div style='float:left;margin-left:22px;width:125px;line-height:30px;margin-top:10px;'><div>(<span style='color:#3D77BC'>" + obj.BranchCode + "</span>)&nbsp;<span class='title'>" + obj.BranchType.toLowerCase() + "</span></div><div><span class='title'>"+obj.Central+"</span></div></div></div>" + "<div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + "<div align='center' class='title'>"+obj.BranchName+"</div><div style='height:165px;width:220px;'><div><img src='" + imagePreview(obj.Image) + "' style='height:160px;width:235px;'/></div></div>" + "<div style='width:240px;line-height:10px;margin-top:5px;'><div><span class='nameTitle'>Branch Type :</span>&nbsp;<span class='title'>" + obj.BranchType.toLowerCase()+"</span></div><div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
    }
    html += pagination;
    html += "</div>";
    return html;
}

function createPagination(length, index, obj){
    var $html = '';
    if(length > 1){
        if(index){
            if(index == length - 1){
                $html += "<div style='width:100%; margin-top:10px;'><a href='javascript:changeContent(\"" + obj.Location + "\"," + (index - 1) + ")' style='float:left;'><button class='btn btn-mini' style='float:left;'><i class='icon-arrow-left'></i></button></a></div>";
            } else {
                $html += "<div style='margin-top:10px;'><a href='javascript:changeContent(\"" + obj.Location + "\"," + (index - 1) + ")'><button class='btn btn-mini' style='float:left;'><i class='icon-arrow-left'></i></button></a>";
                $html += "<a href='javascript:changeContent(\"" + obj.Location + "\"," + (index + 1) + ")' style='margin-left:5px;'><button class='btn btn-mini' style='float:right;'><i class='icon-arrow-right'></i></button></a></div>";
            }
        } else{
            $html += "<div style='margin-top:10px;'><a href='javascript:changeContent(\"" + obj.Location + "\"," + (index + 1) + ")'><button class='btn btn-mini' style='float:right;'><i class='icon-arrow-right'></i></button></a></div>";
        }
    }
    return $html;
}

function changeContent(loc, index) {
    var obj = fusionObjectList.getAll(loc, "Location")[index];
    _currentWindow.setContent(infoWindowHtml(obj, index));
}