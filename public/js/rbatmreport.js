
function changePage($id, $paginationNo, $departmentId){ 
    $('#loading_screen'+$id).show();
    $('#table'+$id).hide();
    var position = $('#selectPage'+$id+'1').offset().top;
    var language = $('#language').val();
    $('body').animate({scrollTop: position}, 1000);
    var $selctedPage = $('#selectPage'+$id+$paginationNo+' option:selected').val();
    $requestData = {
        "requestFor"       :   "admin-report-pagination",
        "pageNo"           :   $selctedPage,
        "questionId"       :   $id,
        "endDate"          :   paginationDate,
        "departmentId"     :   $departmentId
      }
    $.post(baseUrl+'/'+baseBank+'-'+language+'/request.php',$requestData,
                function($result){
                    $('#loading_screen'+$id).hide();
                    $('#selectPage'+$id+'1 option[value="'+$selctedPage+'"]').prop('selected', true);
                    $('#selectPage'+$id+'2 option[value="'+$selctedPage+'"]').prop('selected', true);
                    $('#table'+$id).show();
                    $('#table'+$id).html($result); 
                } 
            )
}