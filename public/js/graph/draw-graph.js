function drawAgeGraph($region,$regionName,$type){
    $("#flagWiseTicketContainer").html($("#ageWiseTicketLoadingSection").html());
    $("#flagWiseTicketContainerSmallShop").html($("#ageWiseTicketLoadingSectionSmallShop").html());
    $requestData = {
        "requestFor"    :   "graph-data",
        "graphType"     :   "AGE-GRAPH",
        "graph-filters" :   {
                                    "region"     : $region,
                                    "regionName" : $regionName
                            }
    }
    if($type=='small-shop'){
        $requestObject.initialize($requestData,"fillAgeGraphSmallShop",null);
    }else
    $requestObject.initialize($requestData,"fillAgeGraph",null);
}

function drawComapreAgeGraph($region,$type)
{   
   if($type=='small-shop'){
      $("#flagWiseTicketContainerSmallShop").html($("#ageWiseTicketLoadingSectionSmallShop").html());
   } else {
      $("#flagWiseTicketContainer").html($("#ageWiseTicketLoadingSection").html());
   }
    $requestData = {
        "requestFor"    :   "graph-data",
        "graphType"     :   "COMPARE-AGE-GRAPH",
        "graph-filters" :   {
                                    "region" : $region
                            }
    }
  if($type=='small-shop'){  
   $requestObject.initialize($requestData,"fillSmallCompareGraph",null);
   } else {
   $requestObject.initialize($requestData,"fillCompareGraph",null);
   }
}

function drawDepartmentComapreAgeGraph($region)
{
    $("#flagWiseTicketContainer").html($("#ageWiseTicketLoadingSection").html());
    $("#flagWiseTicketContainerSmallShop").html($("#ageWiseTicketLoadingSectionSmallShop").html());
    $requestData = {
        "requestFor"    :   "graph-data",
        "graphType"     :   "COMPARE-AGE-GRAPH-DEPARTMENT",
        "graph-filters" :   {
                                    "region" : $region
                            }
    }
   $requestObject.initialize($requestData,"fillDepartmentCompareGraph",null);
}

function fillCompareGraph($data)
{
    $data = JSON.parse($data);
    $centralArray = $data.centralArray;
    $easternArray = $data.easternArray;
    $westernArray = $data.westernArray;
   
    createCompareGraph($centralArray,$easternArray,$westernArray);
}

function fillSmallCompareGraph($data)
{
    $data = JSON.parse($data);
    $centralArray = $data.centralArray;
    $easternArray = $data.easternArray;
    $westernArray = $data.westernArray;
   
    createSmallShopCompareGraph($centralArray,$easternArray,$westernArray);
}



function fillDepartmentCompareGraph($data)
{
    $data = JSON.parse($data);
    createDepartmentCompareGraph($data);
    createDepartmentCompareGraphSmallShop($data);
}

function fillAgeGraph($data){
   
    $data = JSON.parse($data);
    $regionName = $data.regionName;
    $yellowFlag = flagDataFiller($data.yellowFlag);
    $brownFlag  = flagDataFiller($data.brownFlag);
    $blackFlag  = flagDataFiller($data.blackFlag);
    createAgeGraph($yellowFlag,$brownFlag,$blackFlag,$regionName);
}
function fillAgeGraphSmallShop($data){

    $data = JSON.parse($data);
    $yellowFlag = flagDataFiller($data.yellowFlag);
    $brownFlag  = flagDataFiller($data.brownFlag);
    $blackFlag  = flagDataFiller($data.blackFlag);

    createSmallShopAgeGraph($yellowFlag,$brownFlag,$blackFlag);
}

function flagDataFiller($flagColorData){
    $flagData = [];
    for($dataPocket in  $flagColorData){
        if( $flagColorData[$dataPocket]["totalFlag"] === undefined ) {
            $flagData.push(parseInt($flagColorData[$dataPocket][0]));
        }else{
            $flagData.push(parseInt($flagColorData[$dataPocket]["totalFlag"]));
            
        }
    }
    return $flagData;
}