var tour1 = null;  
var tour2 = null;  
var tour3 = null;  
var mytour = null;


function mainPageTourFirst(){
    setTimeout(function() {
        window.scrollTo(0,0);
    }, 200);
   
    if(tour1) {           
        //tour1 = mytour.destroy();
    } 
    
    tour1 = new Tour({name:'tour1'}
     );
   
     tour1.addStep({
        name:'tour1',
        element: "#dashboard_map_first",
        placement: "bottom",
        title: null,
        animation: false,
        content: "<table><tr><td><div><h3 style='color:#007DC1!important;'>Welcome to the Demo</h3></div>&nbsp;<div style='float:right;margin-top:-71px;margin-right:-44px;'><img style='height: 83px;width: 119px;' src='../public/img/4c360-logo-transparent.png'></div></td></tr><br/><tr><td colspan='2'><button class='myButtonTour' onclick='mainPageGuidedTour()' style='margin-left:12px;margin-top: 19px;'>Take the Guided Tour</button>&nbsp;&nbsp;<button class='myButtonTour' onclick='testDriveDashboard()' style='margin-top:19px;'>Test Drive the Dashboard</button></td></tr></table>"
    });
    
   tour1.restart();
}   

function testDriveDashboard(){
    isfirstTourOccurence = false;
    isGuidedTour = false;
    endTour();
}

function mainPageTour(){
    //endTour();
    setTimeout(function(){
        window.scrollTo(0,0);
    }, 200);
    
   
    tour1 = new Tour({name:'tour1'});
    
    tour1.addStep({
        name:'tour1',
        element: "#dashboard_map",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Branch Network</h3></div><div><span style='color:#80828A;'>Branch Network Panel track the information of all the branch performance and their department issues . </span> <br/></div></td></tr></table>"
                 // content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Branch Network</h3></div><div><span style='color:#80828A;'>Branch Network Panel track the information of all the branch performance and their department issues .</span></div></td></tr></table>"
                 // content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Branch Network</h3></div><div><span style='color:#80828A;'>Branch Network Panel track the information of all the branch performance and their department issues . </span> <br/><br/><span style='color:#4183D8;'>- Search for a group of branches using filters</span><br/><span style='color:#4183D8'>-The Scorecard</span><br/><span style='color:#4183D8'>- Check the Health Report of a particular Branch</span><br/><span style='color:#4183D8'>- What is the Branch Voice</span></div></td></tr></table>"
    });
        
    tour1.addStep({
        name:'tour1',
        element: "#Schedule",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Visits Schedule</h3></div><div><span style='color:#80828A;'>Visits Schedule Panel contains the information about schedules .</span></div></td></tr></table>"
                //content: "<span style='color:#7E7E7E;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span>" 
    });
    
    tour1.addStep({
        name:'tour1',
        element: "#AtmContainerTab",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>ATM</h3></div><div><span style='color:#80828A;'>ATM Panel contains the information about ATM's .</span></div></td></tr></table>"
                //content: "<span style='color:#7E7E7E;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span>" 
    });
        
    tour1.addStep({
        name:'tour1',
        element: "#Planogram",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Planogram</h3></div><div><span style='color:#80828A;'>Planogram Panel contains the information about planogram .</span></div></td></tr></table>"
                //content: "<span style='color:#7E7E7E;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span>" 
    });
    
    tour1.addStep({
        name:'tour1',
        element: "#ReportContainer",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Reports</h3></div><div><span style='color:#80828A;'>Reports Panel contains the information about reports .</span><br/></div></td></tr></table>"
                  //content: "<span style='color:#7E7E7E;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span>" 
                  //content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Reports</h3></div><div><span style='color:#80828A;'>Reports Panel contains the information about reports .</span><br/><br/><span style='color:#4183D8;'> - Monthly reports </span><br/><span style='color:#4183D8;'> - Department reports </span><br/><span style='color:#4183D8;'> - Area reports </span><br/><span style='color:#4183D8;'> - Branch Report </span><br/><span style='color:#4183D8;'> - EVP report </span></div></td></tr></table>"
    });
        
    tour1.addStep({
        name:'tour1',
        element: "#Ticket",
        placement: "left",
        title: null,
        animation: true,
       content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Ticket Management</h3></div><div><span style='color:#80828A;'>Tickets Panel contains the information about tickets .</span><br/></div></td></tr></table>"
                //content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Ticket Management</h3></div><div><span style='color:#80828A;'>Tickets Panel contains the information about tickets .</span><br/><br/><span style='color:#4183D8;'>- How to search and filter tickets </span><br/><span style='color:#4183D8;'>- How to Resolve a ticket (link)</span><br/><span style='color:#4183D8;'> - How to Share a list of tickets</span><br/><span style='color:#4183D8;'> - Ticket Editor</span><br/><span style='color:#4183D8;'> - Score and KPIs</span></div></td></tr></table>"
                //content: "<span style='color:#7E7E7E;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span>" 
    });
    
    tour1.addStep({
        name:'tour1',
        element: "#tour-Network",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Network</h3></div><div><span style='color:#80828A;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span></div></td></tr></table>"
                //content: "<span style='color:#7E7E7E;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span>" 
    });
        
    tour1.addStep({
        name:'tour1',
        element: "#tour-SearchMap",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Search By</h3></div><div><span style='color:#80828A;'>Filter your search using multiple criteria. So if you need to know how many Business banking branches, for example, you have in the western region, you can do so - at the push of a button.</span></div></td></tr></table>"
    });

    tour1.addStep({
        name:'tour1',
        element: "#tour-branch",
        placement: "bottom",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Branch</h3></div><div><span style='color:#80828A;'>It is very common for departments within the same bank to mail and share different and outdated versions of branches and ATM lists. 4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span></div></td></tr></table>"
    });

    tour1.addStep({
        name:'tour1',
        element: "#tour-bankreport",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Health Report</h3></div><div><span style='color:#80828A;'>Maintenance and unplanned capacity loss are among the biggest cost drivers in service based industries. The volume and velocity of maintenance records and equipment data are overwhelming traditional quality control department. Your Health report shows you, in real-time, resource and equipment performance and identify those in need of maintenance, out of thousands of different assets.</span></div></td></tr></table>"
    });
    
    tour1.addStep({
        name:'tour1',
        element: "#tour-region-score",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Region and Month Wise Score</h3></div><div><span style='color:#80828A;'>To track the performance monthly wise and shows the consolided score of all branch under Kingdom Wise Score and also shows the score by zone under Zone Wise Score.</span></div></td></tr></table>"
    });
        
    tour1.addStep({
        name:'tour1',
        element: "#tour-flag-color-graph",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Tickets by Flag color</h3></div><div><span style='color:#80828A;'>Graph shows the department wise total tickets group by ticket flag color, different colors category shows the current ticket age . It also has a filter zone under zone name tab to show the number of tickets to their respective zones.   </span></div></td></tr></table>"
    });
        
    tour1.addStep({
        name:'tour1',
        element: "#tour-vendor-graph",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Vendor Score</h3></div><div><span style='color:#80828A;'>Vendor score graph tracks the performance of vendor . it is usefull to find the performance of vendor.  </span></div></td></tr></table>"
    });
    
    tour1.addStep({
        name:'tour1',
        element: "#tour-deptproblem",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Problems by Department</h3></div><div><span style='color:#80828A;'>Track progress and identify bottlenecks. See how many problems have been resolved and how many more are pending, by department, lightning-fast insights without the complexity to effectively manage associated risks.</span></div></td></tr></table>"
    });

    tour1.addStep({
        name:'tour1',
        element: "#tour-ageing",
        placement: "top",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Ageing of Problems</h3></div><div><span style='color:#80828A;'>4C's Visual Analytics delivers crucial information at the right time. Get an up to date report of the total number of incidents, pending at the end of every month.</span></div></td></tr></table>"
    });
    tour1.restart();
    
}

function mainPageGuidedTour(){
    isfirstTourOccurence = false;
    isGuidedTour = true;
    endTour();
    setTimeout(function(){
        window.scrollTo(0,0);
    }, 200);
    
   
    tour1 = new Tour({name:'tour1'});
    
    tour1.addStep({
        name:'tour1',
        element: "#dashboard_map_tour",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'></td><td><div><h3 style='color:#0061A7!important;'>Branch Network</h3></div><div><br/><a href='https://youtu.be/iNnIAzfCIJ4' target='_blank'><span style='color:#4183D8'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Cut, Slice, Dice and Drill with 4c360</span></a><br/><a href=https:///youtu.be/uq0xZYbG8RQ' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Search for a group of branches using filters</span></a><br/><a hrefhttps:////youtu.be/F6YWAeXt5Kk' target='_blank'><span style='color:#4183D8'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Drill down the Branch Health Report</span></a><br/><a hrehttps://://youtu.be/UjkqwBLg2vQ' target='_blank'><span style='color:#4183D8'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Check the Health Report of a particular Branch</span><a/><br/></div></td><td style='float:right;margin-right:-27px;margin-top: -27px;'><img style='height: 98px;width: 116px;' src='../public/img/4c360-logo-transparent.png' /></td></tr></table>&nbsp;&nbsp;<button style='margin-top:28px;cursor:pointer;float:right;margin-left:10px;' class='endButton' onclick='endTour()'><b> End Tour </b></button>&nbsp;&nbsp;<button style='margin-top:28px;cursor:pointer;' class='tour-button' onclick='customNextPrevTour(1)'><b> Report  </b></button>&nbsp;&nbsp;<button style='margin-top:28px;cursor:pointer;' class='tour-button' onclick='customNextPrevTour(2)'><b> Ticket  </b></button>"
    });
    
    tour1.addStep({
        name:'tour1',
        element: "#ReportContainerForGuideTour",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'></td><td><div><h3 style='color:#0061A7!important;'>Reporting</h3></div><div><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Monthly reports </span></a><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Department reports </span><a/><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Area reports </span></a><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Branch Report </span></a><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> EVP report </span></a></div></td><td style='float:right;'><img style='height:98px;width:116px;margin-left:155px;margin-top: -26px;' src='../public/img/4c360-logo-transparent.png' /></td></tr></table><button style='margin-top:28px;cursor:pointer;float:right;margin-left:10px;' class='endButton' onclick='endTour()'><b> End Tour </b></button>&nbsp;&nbsp;<button style='margin-top:28px;cursor:pointer;' class='tour-button' onclick='customNextPrevTour(-1)'><b> Branch Network </b></button>&nbsp;&nbsp;<button style='margin-top:28px;cursor:pointer;' class='tour-button' onclick='customNextPrevTour(1)'><b> Ticket  </b></button>&nbsp;&nbsp;"
    });
    
     tour1.addStep({
        name:'tour1',
        element: "#TicketGuidedTour",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'></td><td><div><h3 style='color:#0061A7!important;'>Ticket Management</h3></div><div><br/><a hrhttps://p://youtu.be/_aOlbjS9QWU' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> How to search and filter tickets </span></a><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> How to Resolve a ticket (link)</span><a/><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> How to Share a list of tickets</span><a/><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Ticket Editor</span></a><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Score and KPIs</span></a><br/><a href='#' target='_blank'><span style='color:#4183D8;'><span title='Click to view video' style='margin-top:3px;' class='red-icon-facetime-video'></span> Search for a ticket by Department. </span></a></div></td><td style='float:right;margin-left:59px;'><img style='height:98px;width:116px;margin-top:-26px;' src='../public/img/4c360-logo-transparent.png' /></td></tr></table><button style='margin-top:28px;cursor:pointer;float:right;margin-left:10px;' class='endButton' onclick='endTour()'><b> End Tour </b></button>&nbsp;&nbsp;<button style='margin-top:28px;cursor:pointer;' class='tour-button' onclick='customNextPrevTour(-2)'><b> Branch Network </b></button>&nbsp;&nbsp;<button style='margin-top:28px;cursor:pointer;' class='tour-button' onclick='customNextPrevTour(-1)'><b> Report  </b></button>&nbsp;&nbsp;"
    });
    
     tour1.addStep({
        name:'tour1',
        element: "#FAQContainerGuidedTour",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'></td><td><div><h3 style='color:#0061A7!important;'> FAQ </h3></div><div><br/></div></td><td style='float:right;'><img style='height:98px;width:116px;margin-top:-26px;' src='../public/img/4c360-logo-transparent.png' /></td></tr></table>"
    });
        
    tour1.restart();
}

function indexPageTour(){
    if(tour1) {
        tour1 = tour1.destroy(); 
    }
    
    $("#start_tour_button").attr("tour-tab", "index_tour");
    
    tour1 = new Tour({name:'tour2'});

    tour1.addStep({
        name:'tour2',
        element: "#tour-healthreport",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Branch Health Report</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Maintenance and unplanned capacity loss are among the biggest cost drivers in service based industries. The volume and velocity of maintenance records and equipment data are overwhelming traditional quality control department. Your Health report shows you, in real-time, resource and equipment performance and identify those in need of maintenance, out of thousands of different assets.</span>"
    });

    tour1.addStep({
        name:'tour2',
        element: "#tour-map",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;My Map</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>My Map  shows the branch on map that you have selected from the dashboard.</span>"
    });

    tour1.addStep({
        name:'tour2',
        element: "#tour-scorecard",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Branch Score</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Scorecard to show performance of branch.</span>"
    });
    
    tour1.addStep({
        name:'tour2',
        element: "#tour-gallart-slider",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Gallery</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Gallary shows department wise problem images .</span>"
    });
    
    tour1.addStep({
        name:'tour2',
        element: "#tour-branch-score-graph",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Branch Score</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>To track the performance of branch on monthly basis . Branch Score graph shows the monthly score of branch . </span>"
    });
    
    tour1.addStep({
        name:'tour2',
        element: "#tour-branch-manager",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Branch Manager</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Branch Manager section shows the signature of branch manager and his feedback under Branch Voice . </span>"
    });
    
    tour1.addStep({
        name:'tour2',
        element: "#tour-error-chart",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Tickets by Department</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Tickets By Department shows the total number of tickets by department . </span>"
    });
    
     tour1.addStep({
        name:'tour2',
        element: "#tour-error-meter",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Number of Tickets</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Number of Tickets shows the total number of pending issues in Issues meter . Upper limit of error meter is the total number of tickets and cursur shows the pending tickets. </span>"
    });
    
    tour1.addStep({
        name:'tour2',
        element: "#tour-flag-color-graph-branch",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Tickets by Flag color</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>This panel shows branch tickets by flag color which helps to determine the department wise tickets of that branch . </span>"
    });
    
    tour1.addStep({
        name:'tour2',
        element: "#tour-vendor-graph-branch",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Vendor Score</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>This panel shows branch vendors scores which helps to determine the performance of vendors of that branch . </span>"
    });
    
    tour1.addStep({
        name:'tour2',
        element: "#tour-brach-score-history",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Score History</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>This panel shows branch score month wise which helps to determine the reputation of that branch . </span>"
    });
    
    tour1.restart();
}

function ticketTour(){
    endTour();
    tour1 = tour1.destroy(); 
    setTimeout(function() {
        window.scrollTo(0,0);
    }, 200);
    
    tour1 = new Tour({name:'tour3'});
    
    tour1.addStep({
        name:'tour3',
        element: "#TicketForATMTour",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Tickets</h3></div><div><span style='color:#80828A;'>Tickets Panel contains the information about tickets .</span></div></td></tr></table>"
    });
    
    tour1.addStep({
        name:'tour3',
        element: "#tour-ticket-list",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Ticket List</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Ticket List section show the list of all tickets with their tickets number and ticket status. </span>"
    });
    
    tour1.addStep({
        name:'tour3',
        element: "#departmentTab",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Department</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Department tab contains the filter and user can filter the tickets. </span>"
    });
    
    tour1.addStep({
         name:'tour3',
        element: "#firstTab",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Ticket Number</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Ticket Number tab contains the filter of ticker number which will search the details of enter ticket number. </span>"
    });
    
    tour1.restart();
}

function reportTour(){
    endTour();
    tour1 = tour1.destroy(); 
    setTimeout(function() {
        window.scrollTo(0,0);
    }, 200);
    
    tour1 = new Tour({name:'tour4'});
            
    tour1.addStep({
        name:'tour4',
        element: "#ReportContainerForATMTour",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>Reports</h3></div><div><span style='color:#80828A;'>Reports Panel contains the information about reports .</span></div></td></tr></table>"
                //content: "<span style='color:#7E7E7E;'>4C manages the Geocoding, curation and visualization of all your touchpoints through this cloud-based, location intelligence application so the teams are always on the same page and up to date.</span>" 
    });
    
    tour1.addStep({
        name:'tour4',
        element: "#tour-report-panel",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Report</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Report section show the reports and user can also download the reports. </span>"
    });
    
    tour1.addStep({
        name:'tour4',
        element: "#monthly-report-tab",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Monthly Report</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Monthly Report tab contains the reports of the departments and sections and user can download the reports on monthly basic or till represent date . </span>"
    });
    
    tour1.addStep({
        name:'tour4',
        element: "#area-report-tab",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Area Report</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Area Report tab contains the reports of all areas and zones . </span>"
    });
    
    tour1.addStep({
        name:'tour4',
        element: "#branch-report-tab",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Branch Report</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Branch Report tab contains the reports of all the branches. </span>"
    });
    
    tour1.addStep({
        name:'tour4',
        element: "#evp_report_tab",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;EVP Report</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>EVP Report tab contains the Average score of all area by month and Black flags by Dept by area . </span>"
    });
    
    tour1.restart();
            
}

function atmTour(){
    tour1 = tour1.destroy(); 
    setTimeout(function() {
        window.scrollTo(0,0);
    }, 200);
    
    tour1 = new Tour({name:'tour5'});
    
    tour1.addStep({
        name:'tour5',
        element: "#AtmContainerTabForATMTour",
        placement: "left",
        title: null,
        animation: true,
        content: "<table><tr><td valign='top'><img src='"+$url+"public/img/4clogo.png' style='height: 30px;width: 40px;'></td><td>&nbsp;</td><td><div><h3 style='color:#80828A!important;'>ATM</h3></div><div><span style='color:#80828A;'>ATM Panel contains the information about ATM's .</span></div></td></tr></table>"
    });
    
    tour1.addStep({
        name:'tour5',
        element: "#atm-tour-network",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Network</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Network section show the location of all the branches on map and user can filter the branches by filter tabs. </span>"
    });
    
    tour1.addStep({
        name:'tour5',
        element: "#atm_region_filter",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Region</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Region tab contains the filter by province and their respective city .  </span>"
    });
    
    tour1.addStep({
        name:'tour5',
        element: "#atm_code_filter",
        placement: "right",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Code</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Code tab contains the filter by branch code . </span>"
    });
    
    tour1.addStep({
        name:'tour5',
        element: "#atm-tour-branch-list",
        placement: "top",
        title: "<h3 style='color:#FFF;'><img src='"+$url+"public/img/4clogo.png' style='height: 29px;width: 41px;'><div style='display: inline-block'><h3 style='color:#80828A!important;'>&nbsp;&nbsp;Visit a branch</h3></div></h3>",
        animation: true,
        content: "<span style='color:#7E7E7E;'>Visit a branch section contains the list of all the branches of ATM. </span>"
    });
    
    tour1.restart();
}

function gotoReports(){
    $( "#ReportContainer" ).trigger( "click" );
}

function gotoTickets(){
    $( "#Ticket" ).trigger( "click" );
}

$(document).ready(function() {
   
   if ($('.tour').length) {
        //  mainPageTour();   //START DEFAULT TOUR ON PAGELOAD
        mainPageTourFirst();
   } else if ($('.tourindex').length) {
        indexPageTour();
    }
    
    $("#dashboard_map").click(function() {
        $("#start_tour_button").attr("tour-tab", "dashboard_map_tour");
        $("#start_tour_button").attr("disabled", "disabled");
        mainPageTour();
        
    });
    
    $("#Ticket").click(function() {
        
        $("#start_tour_button").attr("tour-tab", "Ticket_tour");
        $("#start_tour_button").attr("disabled", "disabled");
        ticketTour();
    });
    
    $("#ReportContainer").click(function() {
        
        $("#start_tour_button").attr("tour-tab", "ReportContainer_tour");
        $("#start_tour_button").attr("disabled", "disabled");
        reportTour();
    });
    
     $("#AtmContainerTab").click(function() {
        alert('ATM tab clicked');
        $("#start_tour_button").attr("tour-tab", "AtmContainerTab_tour");
        $("#start_tour_button").attr("disabled", "disabled");
        atmTour();
    });
    
    $("#start_tour_button").click(function() {
        selected_tab = $(this).attr("tour-tab");
        $("#start_tour_button").attr("disabled", "disabled");
        
        if(selected_tab == 'dashboard_map_tour') {
            //mainPageTour();
             mainPageTourFirst();
        } else if(selected_tab == 'Ticket_tour') {
            ticketTour();
        } else if(selected_tab == 'ReportContainer_tour') {
            reportTour();
        }  else if(selected_tab == 'AtmContainerTab_tour') {
            atmTour();
        } 
        else if(selected_tab == 'index_tour') {
            indexPageTour();
        } 
        
    });
});