var styles = [
    [{
        url: baseUrl + "public/img/cluster/demo/m1.png",
        height: 35,
        width: 35,
        anchor: [9, 0],
        textColor: "#FFFFFF",
        textSize: 10
    }, {
        url: baseUrl + "public/img/cluster/demo/m2.png",
        height: 40,
        width: 40,
        anchor: [12, 0],
        textColor: "#FFFFFF",
        textSize: 11
    }, {
        url: baseUrl + "public/img/cluster/demo/m3.png",
        height: 52,
        width: 52,
        anchor: [19, 0],
        textColor: "#FFFFFF",
        textSize: 12
    }, {
        url: baseUrl + "public/img/cluster/demo/m4.png",
        height: 62,
        width: 62,
        anchor: [24, 0],
        textColor: "#FFFFFF",
        textSize: 13
    }]
]

var CloseButtonImg = 'close-button.png';
var BankColor   = 'white';
var markerIcons = new Array(
    {image: baseUrl + "public/img/logo/hrdf_logo.png", width: 58,height: 66},
    {image: baseUrl + "public/img/logo/hrdf_logo.png", width: 36,height: 60}
); 