var mobilyFilterData = new mobilyFilter();
var globalId;


function findMobilyFilter($filterType){
    
    mobilyFilterData.destroy();
    mobilyFilterData.set('filterBy',$filterType);
    
    if ($filterType == 'campaign') {
        $selectId = 'campaign_filters';
    } else if ($filterType == 'campaignRegion') {
        $selectId = 'byOutletRegionDiv';
    } else if ($filterType == 'campaignOutletType') {
        $selectId = 'byOutletTypeDiv';
    } else if($filterType == 'campaignData') {
        $selectId = 'bydateDiv';
    }
    
    $("#"+$selectId).find('select').each(function(){
            mobilyFilterData.set($(this).attr('id'),($(this).val()).trim());
    });
    
    $("#"+$selectId).find('input').each(function(){
            mobilyFilterData.set($(this).attr('id'),($(this).val()).trim());
    });
    
    $("#ticketFilterCodeRow").find('input').each(function(){
            mobilyFilterData.set($(this).attr('id'),($(this).val()).trim());
    });
    
    if($("#gotoCampaignPageNumber").length)
        mobilyFilterData.set('pageNumber',$("#gotoCampaignPageNumber").val());
    else
        mobilyFilterData.set('pageNumber',$ticketPanelPageValue);
}
function mobilyFilter(){
    this.data = {
        "pageNumber"    :   0
    };

    this.set = function($filterIndex,$filterValue) {
      
        switch($filterIndex){
            case "campaignOutlet"               :   this.data.campaignOutlet                = $filterValue;break;
            case "campaignRegion"               :   this.data.campaignRegion                = $filterValue;break;
            case "campaignCity"                 :   this.data.campaignCity                  = $filterValue;break;
            case "campaignId"                   :   this.data.campaignId                    = $filterValue;break;
            case "campaignDate"                 :   this.data.campaignDate                  = $filterValue;break;
            case "outletType"                   :   this.data.outletType                    = $filterValue;break;
            case "orderField"                   :   this.data.orderField                    = $filterValue;break;
            case "orderType"                    :   this.data.orderType                     = $filterValue;break;
            case "pageNumber"                   :   this.data.pageNumber                    = $filterValue;break;
            case "outletCampaignValidation"     :   this.data.outletCampaignValidation      = $filterValue;break;
            case "regionCampaignValidation"     :   this.data.regionCampaignValidation      = $filterValue;break;
            case "outletTypeCampaignValidation" :   this.data.outletTypeCampaignValidation  = $filterValue;break;
            case "campaignValidationDate"       :   this.data.campaignValidationDate        = $filterValue;break;
        }
    }

    this.get = function(){
        return this.data;
    }

    this.destroy = function(){
         this.data.campaignOutlet               =   '';
         this.data.campaignRegion               =   '';
         this.data.campaignCity                 =   '';
         this.data.campaignId                   =   '';
         this.data.campaignDate                 =   '';
         this.data.outletType                   =   '';
         this.data.orderField                   =   '';
         this.data.orderType                    =   '';
         this.data.pageNumber                   =   '';
         this.data.outletCampaignValidation     =   '';
         this.data.regionCampaignValidation     =   '';
         this.data.outletTypeCampaignValidation =   '';
         this.data.campaignValidationDate       =   '';
    }

}
function changeCampaignPageNumber($type,$totalPage){
    $doPagination = false;
    $currentPage = $('#gotoCampaignPageNumber option:selected').text();
    $currentPage = $currentPage-1;
    if($type == 'next'){
        $currentPage++;
        
        if($('#gotoCampaignPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeCampaignPageNumberNextButton").hide();
            $("#changeCampaignPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        } else{
            $("#changeCampaignPageNumberNextButton").show();
            $("#changeCampaignPageNumberPrevButton").show();
            $doPagination = true;
        }
    } else{
        $currentPage--;
        if($currentPage <= 0){
            $("#changeCampaignPageNumberPrevButton").hide();
            $currentPage = 0;
            $doPagination = true;
        }else{
            $("#changeCampaignPageNumberNextButton").show();
            $("#changeCampaignPageNumberPrevButton").show();
            $doPagination = true;
        }
        
    }
    
    
    if($doPagination){
            $('#gotoCampaignPageNumber option:eq('+$currentPage+')').attr('selected', 'selected');
            $('#gotoCampaignPageNumber').trigger('change');
        if($('#gotoCampaignPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeCampaignPageNumberNextButton").hide();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }
    }
}

function gotoCampaignPageNumber(element, $currentType){
    if($('#gotoCampaignPageNumber option:selected').attr('pageClass') == 'last'){
        $("#changeCampaignPageNumberNextButton").hide();
        $("#changeCampaignPageNumberPrevButton").show();
    } else if($('#gotoCampaignPageNumber option:selected').text() == '1') {
        $("#changeCampaignPageNumberNextButton").show();
        $("#changeCampaignPageNumberPrevButton").hide();
    }else{
        $("#changeCampaignPageNumberNextButton").show();
        $("#changeCampaignPageNumberPrevButton").show();
    }
    
    $("#campaign-survey-loader").show();
    $("#campaign-survey-loader-image").show();
    
    findMobilyFilter('campaign');
    $requestData = {
        "requestFor"     :   "campaign-survey-result",
        "filter-data"    :   mobilyFilterData.get()
            
    }
    $requestObject.initialize($requestData,"fillCampaignFilterResult",null);
}  

function sortCampaignData($orderField, $orderType){
    findMobilyFilter('campaign');
    mobilyFilterData.set('orderField', $orderField);
    mobilyFilterData.set('orderType', $orderType);
    
    $requestData ={
                'requestFor' : 'campaign-survey-result',
                'filter-data' : mobilyFilterData.get()
            }
    $("#campaign-survey-loader").show();
    $("#campaign-survey-loader-image").show();
    $requestObject.initialize($requestData,'fillCampaignFilterResult',null);
}

function fillCampaignFilterResult($result){
   $("#campaign-survey-loader").hide();
    $("#campaign-survey-loader-image").hide();
    $("#campaign-survey-data-table").html($result);
}