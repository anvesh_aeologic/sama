var ticketFilterData = new ticketFilter();
var globalId;
var $filterType;
function resetTicketFilter($currentType){
    if(bankFolder != 'arbatm' && bankFolder != 'arbbranches_v2' ){
        $('#ticketAreas').html('<option value="">--  Area --</option>');
        $('#cityBranch').html('<option value="">-- City --</option>');

    }


    if(bankFolder == 'riyad'){

        if( ($("#riyadDatesFilter").is(":visible")) && ($("#riyadDatesFilter1").is(":visible"))){
            $("#pendingDateType").hide();
            $("#riyadDatesFilter").hide();
            $("#riyadDatesFilter1").hide();
            $("#riyad_start_date").val('');
            $("#riyad_end_date").val('');
        }
    }

    $('#selectMetricesDepartment').html('<option value="">-- Metrics --</option>');
    $autoSwitchFlag = false;
    $ticketPanelPageValue = 0;

    if ($filterType == 'ticketEditor') {
        $selectId = 'ticketFilterRowTicketEditor';
    } else {
        $selectId = 'ticketFilterRow';
    }

    if($currentType == 'pendingOrder'){
        $selectId = 'pendingOrderTab';
    }
    if($currentType == 'rejectedTicket'){
        $selectId = 'rejectedTicketTab';
    }

    $("#"+$selectId).find('select').each(function(){
        if($(this).attr('id') != 'database_name' && $(this).attr('id') != 'survey_id' && $(this).attr('id') != 'currentDatabase' ){

            $(this).attr('value','');
            if((baseBank == 'rbatm_v3' || baseBank == 'rbatm_v3')&& $(this).attr('id')=='group_name'){
                $('#group_name').val('DRIVEUP');
            }
            if((baseBank == 'arbatm')&& $(this).attr('id')=='group_name'){
                $('#group_name').val('');
            }
            if(baseBank == 'mobily_v5' && $(this).attr('id')=='group_name'){
                $('#group_name').val('FS');
            }
        }
    });

    $("#"+$selectId).find('input').each(function(){
		if($(this).attr('type') != 'hidden')
			$(this).attr('value','');
    });

    $("#ticketFilterCodeRow").find('input').each(function(){
        $(this).attr('value','');
    });
    /*************************************************************SetHereDateValueForATMFilter*********************************************************/
    $("#myATMDatesForFilter").find('input').each(function(){
             $(this).attr('value','');
    });
    /*************************************************************SetHereDateValueForATMFilter*********************************************************/

    filterTickets($currentType);
}

function findTicketFilter($filterType){

    ticketFilterData.destroy();
    ticketFilterData.set('filterBy', $filterType);

    if ($filterType == 'ticketEditor') {
        $selectId = 'ticketFilterRowTicketEditor';
    } else if ($filterType == 'Small Shop') {
        $selectId = 'smallshopticket';
    } else if ($filterType == 'Kiosk') {
        $selectId = 'kioskticket';
    } else if ($filterType == 'Big Shop') {
        $selectId = 'BigShopContainerTab';
    }else if ($filterType == 'pendingOrder') {
        $selectId = 'pendingOrderTab';
    }else if ($filterType == 'rejectedTicket') {
        $selectId = 'rejectedTicketTab';
    } else if($filterType == 'ticketNumber') {
        $selectId = 'ticketFilterCodeRow';
    } else {
        $selectId = 'ticketFilterRow';
    }
    console.log($selectId);
    $("#"+$selectId).find('select').each(function(){

            if($(this).val()!= null){
                ticketFilterData.set($(this).attr('id'),($(this).val()).trim());
            }
    });

    $("#"+$selectId).find('input').each(function(){

          ticketFilterData.set($(this).attr('id'),($(this).val()).trim());

    });

//    $("#ticketFilterCodeRow").find('input').each(function(){
//            ticketFilterData.set($(this).attr('id'),($(this).val()).trim());
//    });

    /*************************************************************SetHereDateValueForATMFilter*********************************************************/
    $("#myATMDatesForFilter").find('input').each(function(){
            ticketFilterData.set($(this).attr('id'),($(this).val()).trim());
    });
    /*************************************************************SetHereDateValueForATMFilter*********************************************************/
    $("#ticketOrderRow").find(".orderSpan img").each(function(){
        if($(this).attr('orderType')){
            ticketFilterData.set('orderField',$(this).attr('orderField'));
            ticketFilterData.set('orderType',$(this).attr('orderType'));
        }
    })

//    if($("#questionBranchCode")){
//        ticketFilterData.set('questionBranchCode', $("#questionBranchCode").val());
//    }
//
//    if($("#questionBranchName")){
//        ticketFilterData.set('questionBranchName', $("#questionBranchName").val());
//    }

    if($("#gotoPagenumber").length)
        ticketFilterData.set('pageNumber',$("#gotoPagenumber").val());
    else
        ticketFilterData.set('pageNumber',$ticketPanelPageValue);
}


function filterTickets($currentType){
    $("#filterTicket").click(function(e){
        if($("#ticketNumber").val().trim() == '') {
            alert('Please Enter Ticket Number');
            e.PreventDefault();
            return false;
        }
    });




    $autoSwitchFlag = false;
    $ticketPanelPageValue = 0;
    $filterType = $currentType;
    $("#ticketFileterloader").show();

    findTicketFilter($filterType);

    $requestData = {
        "requestFor"     :   "ticket-filter",
        "ticket-filters" :   ticketFilterData.get()
    }

    if(bankFolder == 'riyad'){

        var status = $requestData['ticket-filters']['ticketCurrentStatus'];
        $("#ticket_wise_list_excel_link").find("a").each(function() {
            newSrc = $(this).attr('myval') ;
            $(this).attr('href', newSrc);
            newSrc = $(this).attr('href') + "&status=" + status;
            $(this).attr('href', newSrc);
        });
    }

    $requestObject.initialize($requestData, "fillTicketFilterResult", null);

    if($("#gotoPagenumber").length)
        ticketFilterData.set('pageNumber', $("#gotoPagenumber option:selected").text());
    else
        ticketFilterData.set('pageNumber', $ticketPanelPageNumber);

    $requestData = {
        "requestFor"     :   "ticket-pagination",
        "ticket-filters" :   ticketFilterData.get()
    }

    $requestObject.initialize($requestData,"fillTicketPaginationResult",null);
}

function observationDetail(){
    $("#observationResult").html($("#observation-detail-loader").html());

    $requestData = {
        "requestFor"     :   "observation-detail"
           };

    $requestObject.initialize($requestData, null,"#observationResult");

}


function fillTicketFilterResult(result){

    if($autoSwitchFlag !== true) {
        $("#ticketFilterResultDiv").html(result);
    }

    $("#ticketFileterloader").hide();
    $("#ticketFilterResultDiv").removeClass('hide');
    activeActionButtons();
    initializeTicketPopup();
    initializeIMTicketButtons();

}

function initializeIMTicketButtons()
{

}


function editTicketNumberForIm($rowId){

  /*  $('#ticketSuccessMessage_'+globalId).css('display','none');
    $('#ticketWithoutTextArea_'+$rowId).css('display','none');
    $('#ticketWithTextArea_'+$rowId).css('display','inline');

    $('#saveTicketNumber_'+$rowId).css('display','inline');
    $('#cancelTicketNumber_'+$rowId).css('display','inline');
    $('#editTicketNumber_'+$rowId).css('display','none'); */

    $('#ticketSuccessMessage_'+globalId).css('display','none');
    $ticketNumber = $('#ticketWithoutTextArea_'+$rowId).text();

    $('#ticketWithoutTextArea_'+$rowId).css('display','none');
    $('#textAreaSpan_'+$rowId).css('display','inline');

    $ticketTextAreaHtml = '<textarea id="ticketWithTextArea_'+$rowId+'" cols="4" rows="1">'+$ticketNumber.trim()+'</textarea>';
    $('#textAreaSpan_'+$rowId).html($ticketTextAreaHtml);

    $('#revertTicketNumber_'+$rowId).show("slow");
    $('#saveTicketNumber_'+$rowId).show("slow");
    $('#cancelTicketNumber_'+$rowId).show("slow");
    $('#editTicketNumber_'+$rowId).css('display','none');
    $('#ticketExistMessage_'+$rowId).css('display','none');

}
globalRevert = false;
function saveTicketNumberForIm($rowId){
   globalId = $rowId;
   $("#saveImTicket_"+globalId).attr('disabled', true);
   $('#imTicketLoader_'+$rowId).css('display','inline');
   $ticketId = $("#ticketId_"+$rowId).val();
   editTicketNumber_0 = $('textarea#ticketWithTextArea_'+$rowId).val();
   $orignalTicketNumber = $('#ticketWithoutTextArea_'+$rowId).text();

   if(editTicketNumber_0.length >= 6){
       if($('#ticket_status'+$rowId+' span').text() == 'Pending'){
            $requestParam = {
                'ticket-id'              :  $ticketId,
                'ticket-number-updated'  :  editTicketNumber_0,
                'ticket-number-orignal'  :  $orignalTicketNumber,
                'branch-code'            :  $("#branchCode_"+$rowId).val(),
                'department-id'          :  $("#departmentId_"+$rowId).val(),
                'revert-flag'            :  globalRevert
            };

           $requestData = {
                "requestFor"     :   "im-ticket-updation",
                "requestParam"    :   $requestParam
           }

           $requestObject.initialize($requestData,"updatedTicketCallBack",null);

       } else{
            alert('Only pending tickets can be tranformed to SD tickets.');
            $('#imTicketLoader_'+$rowId).css('display','none');
            return false;
       }
   } else{
      alert('IM Ticket length should be 6 digit or more!');
      $('#imTicketLoader_'+$rowId).css('display','none');
      return false;
   }
}

function updatedTicketCallBack(result)
{
     globalRevert = false;
     $("#saveImTicket_"+globalId).removeAttr('disabled');

    console.log(isNaN(result));

    if(result.trim() == 'ticketNameAlreadyExist'){
         $('#imTicketLoader_'+globalId).css('display','none');
         $('#ticketWithTextArea_'+globalId).css('display','none');
         $('#ticketExistMessage_'+globalId).css('display','inline');
         $('#revertTicketNumber_'+globalId).css('display','none');
         $('#saveTicketNumber_'+globalId).css('display','none');
         $('#cancelTicketNumber_'+globalId).css('display','none');
         $('#ticketWithoutTextArea_'+globalId).html($ticketNumber);
         $('#ticketWithoutTextArea_'+globalId).css('display','inline');
         $('#editTicketNumber_'+globalId).css('display','inline');
     }else if(!isNaN(result)){

            var _href = $('#ticketWithoutTextArea_'+globalId).attr("href");
            $('#ticketId_'+globalId).val(result);

            $('#imTicketLoader_'+globalId).css('display','none');
            $('#ticketSuccessMessage_'+globalId).css('display','inline');

            $('#ticketWithoutTextArea_'+globalId).html(editTicketNumber_0);
            $('#ticketWithoutTextArea_'+globalId).attr("href", _href + '&forTicket=' + editTicketNumber_0);
            $('#ticketWithoutTextArea_'+globalId).css('display','inline');

            $('#ticketWithTextArea_'+globalId).css('display','none');
            $('#ticketExistMessage_'+globalId).css('display','none');

            $('#revertTicketNumber_'+globalId).css('display','none');
            $('#saveTicketNumber_'+globalId).css('display','none');
            $('#cancelTicketNumber_'+globalId).css('display','none');
            $('#editTicketNumber_'+globalId).css('display','inline');

            $('#SdTicketPopup_'+globalId).attr('ticket-number',editTicketNumber_0);
            $indexPageLink = $('#sdTicketIndexLink_'+globalId).attr('my-val');
            $('#sdTicketIndexLink_'+globalId).attr('href',$indexPageLink+'&forTicket='+editTicketNumber_0);
     }else{
        if(result != 'departmentError'){
            var _href = $('#ticketWithoutTextArea_'+globalId).attr("href");

            $('#imTicketLoader_'+globalId).css('display','none');
            $('#ticketSuccessMessage_'+globalId).css('display','inline');

            $('#ticketWithoutTextArea_'+globalId).html(editTicketNumber_0);
            $('#ticketWithoutTextArea_'+globalId).attr("href", _href + '&forTicket=' + editTicketNumber_0);
            $('#ticketWithoutTextArea_'+globalId).css('display','inline');

            $('#ticketWithTextArea_'+globalId).css('display','none');
            $('#ticketExistMessage_'+globalId).css('display','none');

            $('#revertTicketNumber_'+globalId).css('display','none');
            $('#saveTicketNumber_'+globalId).css('display','none');
            $('#cancelTicketNumber_'+globalId).css('display','none');
            $('#editTicketNumber_'+globalId).css('display','inline');

            $('#SdTicketPopup_'+globalId).attr('ticket-number',editTicketNumber_0);
            $indexPageLink = $('#sdTicketIndexLink_'+globalId).attr('my-val');
            $('#sdTicketIndexLink_'+globalId).attr('href',$indexPageLink+'&forTicket='+editTicketNumber_0);
        }else{
            $('#imTicketLoader_'+globalId).css('display','none');
            alert('SD Tickets having same department and Branch Code can be merged');
        }
    }
}

function cancelTicketNumberForIm($rowId)
{
   globalRevert = false;
   $('#ticketWithoutTextArea_'+$rowId).css('display','inline');
   $('#textAreaSpan_'+$rowId).css('display','none');

   $('#saveTicketNumber_'+$rowId).css('display','none');
   $('#revertTicketNumber_'+$rowId).css('display','none');
   $('#cancelTicketNumber_'+$rowId).css('display','none');
   $('#editTicketNumber_'+$rowId).css('display','inline');
   $('#imTicketLoader_'+$rowId).css('display','none');
}

function revertTicketNumberForIm($rowId){
    globalId = $rowId;
    $('#imTicketLoader_'+$rowId).css('display','inline');
    $ticketID     = $("#ticketId_"+$rowId).val();
    $requestData = {
            "requestFor"     :   "im-ticket-revert",
            "requestParam"   :   {
                "ticket-id"  :  $ticketID
             }
       }
    $requestObject.initialize($requestData,"revertTicketCallBack",null);
}
function revertTicketCallBack($result){
    globalRevert = true;
    $('#imTicketLoader_'+globalId).css('display','none');
    $('textarea#ticketWithTextArea_'+globalId).val($result);
    //$('#ticketWithoutTextArea_'+globalId).html($result);

}
function fillTicketPaginationResult(result){
    $("#ticketPaginationDiv").html(result);
    $("#ticketFileterloader").hide();
    activeActionButtons();
}

function sortByTicketFilter($orderField,$orderType){
    console.log($orderField + " : " + $orderType );
    ticketFilterData.set('orderField',$orderField);
    ticketFilterData.set('orderType',$orderType);
}

//Previously Written Code.$
//function gotoPageNumber(element, $currentType){
//
//    $("#ticketFileterloader").show();
//    $filterType = $currentType;
//    findTicketFilter($filterType);
//    ticketFilterData.set('pageNumber',$(element).val());
//    //alert($(element).find('option:selected').attr("zone_name"));
//    if($(element).find('option:selected').attr("zone_name")){
//       ticketFilterData.set('zone_name',$(element).find('option:selected').attr("zone_name"));
//    }
//    ticketFilterData.set('changeOrder',false);
//    if($('#gotoPageNumber option:selected').text() == 1){
//        $('#').hide();
//    }
//    if($('#'))
//    $requestData = {
//        "requestFor"     :   "ticket-filter",
//        "ticket-filters" :   ticketFilterData.get()
//
//    }
//    $requestObject.initialize($requestData, "fillTicketFilterResult", null);
//}

function gotoPageNumber(element, $currentType){

 $lastPage = $('#gotoPageNumber option:last-child').text();

    $("#ticketFileterloader").show();
    $filterType = $currentType;
    findTicketFilter($filterType);
    ticketFilterData.set('pageNumber',$(element).val());
    //alert($(element).find('option:selected').attr("zone_name"));
    if($(element).find('option:selected').attr("zone_name")){
       ticketFilterData.set('zone_name',$(element).find('option:selected').attr("zone_name"));
    }
    ticketFilterData.set('changeOrder',false);
    if($('#gotoPageNumber option:selected').text() == 1){
        $('#changePageNumberPrevButton').hide();
        $('#changePageNumberNextButton').show();
    }

    if ($('#gotoPageNumber option:selected').text() == $lastPage) {
        $('#changePageNumberNextButton').hide();
        $('#changePageNumberPrevButton').show();
    }

    if ($('#gotoPageNumber option:selected').text() > 1 && $('#gotoPageNumber option:selected').text() < $lastPage) {
        $('#changePageNumberPrevButton').show();
        $('#changePageNumberNextButton').show();
    }


    if($('#'))
    $requestData = {
        "requestFor"     :   "ticket-filter",
        "ticket-filters" :   ticketFilterData.get()

    }
    $requestObject.initialize($requestData, "fillTicketFilterResult", null);
}


function sortByTicketFilter($orderField,$orderType){
    console.log($orderField + " : "+ $orderType);
    $("#ticketFileterloader").show();
    findTicketFilter('');
    ticketFilterData.set('orderField',$orderField);
    ticketFilterData.set('orderType',$orderType);

    if($("#gotoPageNumber").length)
        ticketFilterData.set('pageNumber',$("#gotoPageNumber").val());
    else
        ticketFilterData.set('pageNumber',$ticketPanelPageNumber);

    $requestData = {
        "requestFor"     :   "ticket-filter",
        "ticket-filters" :   ticketFilterData.get()

    }
    $requestObject.initialize($requestData,"fillTicketFilterResult",null);
}


function movePendingOrder( $status, $orderId,$category){
    if( confirm("Do you really want to " + $status + " this ticket? ") ){
        $("#ticketFileterloader").show();
        $requestData = {
            "requestFor"     :   "move-pending-order",
            "ticket-status"  : {
                "changeStatus" :   $status,
                "orderId"      :   $orderId,
                "category"     :   $category
            }
        }
        $requestObject.initialize($requestData, "refreshPendingOrder", null);
    }
}

function moveRejectedTicket( $status, $orderId,$category){
    if( confirm("Do you really want to " + $status + " this ticket? ") ){
        $("#ticketFileterloader").show();
        $requestData = {
            "requestFor"     :   "move-pending-order",
            "ticket-status"  : {
                "changeStatus" :   $status,
                "orderId"      :   $orderId,
                "category"     :   $category,
                'rejectedTicket' : true
            }
        }
        $requestObject.initialize($requestData, "refreshRejectedTicket", null);
    }
}

function refreshRejectedTicket($response){
    $response = JSON.parse($response);
    $("#rejected_" + $response.orderId).remove();
    if($response.success)
		alert("Ticket has been published");
	else
		alert("Ticket has been removed");

    filterTickets('rejectedTicket');
}

function refreshPendingOrder($response){
    $response = JSON.parse($response);
    $("#pendingOrder_" + $response.orderId).remove();
    if($response.success)
		alert("Ticket has been published");
	else
		alert("Ticket has been removed");

    filterTickets('pendingOrder');
}

function ticketFilter(){
    this.data = {
        "pageNumber"    :   0,
        "orderType"     :   "DESC",
        "orderField"    :   "ticketNumber",
        "changeOrder"   :   true,
        "ticketType"    :   "branch"
    };

    this.set = function($filterIndex,$filterValue) {

        switch($filterIndex){
            case "ticketBranch"         :   this.data.ticketBranch        = $filterValue;break;
            case "ticketRegion"         :   this.data.ticketRegion        = $filterValue;break;
            case "ticketZone"           :   this.data.ticketZone          = $filterValue;break;
            case "ticketAreas"          :   this.data.ticketAreas         = $filterValue;break;
            case "ticketColor"          :   this.data.ticketColor         = $filterValue;break;
            case "ticketDepartment"     :   this.data.ticketDepartment    = $filterValue;break;
            case "selectmetrices"       :   this.data.selectmetrices      = $filterValue;break;
            case "ticketAge"            :   this.data.ticketAge           = $filterValue;break;
            case "ticketCurrentStatus"  :   this.data.ticketCurrentStatus = $filterValue;break;
            case "atmTicketType"        :   this.data.atmTicketType       = $filterValue;break;
            case "ticketImportance"     :   this.data.ticketImportance    = $filterValue;break;
            case "ticketNumber"         :   this.data.ticketNumber        = $filterValue;break;
            case "filterBy"             :   this.data.filterBy            = $filterValue;break;
            case "orderField"           :   this.data.orderField          = $filterValue;break;
            case "orderType"            :   this.data.orderType           = $filterValue;break;
            case "pageNumber"           :   this.data.pageNumber          = $filterValue;break;
            case "changeOrder"          :   this.data.changeOrder         = $filterValue;break;
            case "ticketType"           :   this.data.ticketType          = $filterValue;break;
            case "startDate"            :   this.data.startDate           = $filterValue;break;
            case "endDate"              :   this.data.endDate             = $filterValue;break;
            case "ageatm"               :   this.data.ageatm              = $filterValue;break;
            case "functionName"         :   this.data.functionName        = $filterValue;break;
            case "atmLocation"          :   this.data.atmLocation         = $filterValue;break;
            case "atmPlace"             :   this.data.atmPlace            = $filterValue;break;
            case "typeofOutlet"         :   this.data.typeofOutlet        = $filterValue;break;
            case "mobilyTicketRegion"   :   this.data.mobilyTicketRegion  = $filterValue;break;
            case "mobilyTicketCity"     :   this.data.mobilyTicketCity    = $filterValue;break;
            case "outletPartner"        :   this.data.outletPartner       = $filterValue;break;
            case "group_name"           :   this.data.group_name          = $filterValue;break;
            case "survey_id"            :   this.data.survey_id           = $filterValue;break;
            case "ticket_branch_type"   :   this.data.ticket_branch_type  = $filterValue;break;
            case "zone_name"            :   this.data.zone_name           = $filterValue;break;
            case "database_name"        :   this.data.database_name       = $filterValue;break;
            case "currentDatabase"      :   this.data.currentDatabase       = $filterValue;break;
            case "isPendingOrder"       :   this.data.isPendingOrder      = $filterValue;break;
            case "selectMetricesDepartment"    :   this.data.selectMetricesDepartment   = $filterValue;break;
            case "ticketRegionPending"         :   this.data.ticketRegionPending        = $filterValue;break;
            case "ticketZonePending"           :   this.data.ticketZonePending          = $filterValue;break;
            case "ticketAreasPending"          :   this.data.ticketAreasPending         = $filterValue;break;
            case "ticketColorPending"          :   this.data.ticketColorPending         = $filterValue;break;
            case "ticketDepartmentPending"     :   this.data.ticketDepartmentPending    = $filterValue;break;
            case "selectmetricesPending"       :   this.data.selectmetricesPending      = $filterValue;break;
            case "groupNamePending"            :   this.data.groupNamePending           = $filterValue;break;
            case "questionZone"                :   this.data.questionZone               = $filterValue;break;
            case "questionBranchCode"          :   this.data.questionBranchCode         = $filterValue;break;
            case "questionBranchName"          :   this.data.questionBranchName         = $filterValue;break;
            case "ticketProvience"             :   this.data.ticketProvience            = $filterValue;break;
            case "cityBranch"                  :   this.data.cityBranch                 = $filterValue;break;
            case "riyad_start_date"            :   this.data.riyad_start_date           = $filterValue;break;
            case "riyad_end_date"              :   this.data.riyad_end_date             = $filterValue;break;
            case "pendingDateFilterType"       :   this.data.pendingDateFilterType      = $filterValue;break;
            case "minMajCategory"              :   this.data.minMajCategory             = $filterValue;break;
            case "ticketDepartmentRejected"    :   this.data.ticketDepartmentRejected   = $filterValue;break;
            case "selectmetricesRejected"      :   this.data.selectmetricesRejected     = $filterValue;break;
            case "category"                    :   this.data.category                   = $filterValue;break;

        }
    }

    this.get = function(){
        return this.data;
    }

    this.destroy = function(){
         this.data.ticketRegion        =   '';
         this.data.ticketZone          =   '';
         this.data.ticketAreas         =   '';
         this.data.ticketColor         =   '';
         this.data.ticketDepartment    =   '';
         this.data.selectmetrices      =   '';
         this.data.ticketAge           =   '';
         this.data.ticketNumber        =   '';
         this.data.ticketCurrentStatus =   '';
         this.data.atmTicketType       =   '';
         this.data.ticketImportance    =   '';
         this.data.filterBy            =   '';
         this.data.pageNumber          =   0;
         this.data.changeOrder         =   false;
         this.data.orderField          =   '';
         this.data.orderType           =   '';
         this.data.startDate           =   '';
         this.data.endDate             =   '';
         this.data.ageatm              =   '';
         this.data.atmFunctionName     =   '';
         this.data.atmLocation         =   '';
         this.data.atmPlace            =   '';
         this.data.mobilyTicketRegion  =   '';
         this.data.mobilyTicketCity    =   '';
         this.data.outletPartner       =   '';
         this.data.group_name          =   '';
         this.data.survey_id          =   '1';
         this.data.ticketProvience     =   '';
         this.data.cityBranch          =   '';
         this.data.category             =   '';

        if(baseBank == 'rbatm_v3' || baseBank == 'bajatm' || baseBank == 'bsfatm'){
            this.data.group_name          = 'DRIVEUP';
        }
        if(baseBank == 'arbatm'){
            this.data.group_name          = '';
        }
        if(baseBank == 'arbbranches_v2'){
            this.data.survey_id          = 1;
            this.data.ticket_branch_type = '';
        }
        if(baseBank == 'mobily_v5'){
            this.data.group_name          = 'FS';
        }
         this.data.zone_name           =    '';
         this.data.database_name       =    '';
         this.data.currentDatabase       =    '';
         this.data.isPendingOrder      =    0;
         this.data.ticketRegionPending        ='';
         this.data.ticketZonePending          ='';
         this.data.ticketAreasPending         = '';
         this.data.ticketColorPending         ='';
         this.data.ticketDepartmentPending    = '';
         this.data.selectmetricesPending      = '';
         this.data.groupNamePending = "";
         this.data.questionZone = "";
         this.data.questionBranchCode = "";
         this.data.questionBranchName = "";
    }

}

function changePageNumber($type,$totalPage){

    $doPagination = false;
    $currentPage = $('#gotoPageNumber option:selected').text();
    $currentPage = $currentPage-1;
    if($type == 'next'){
        $currentPage++;

        if($('#gotoPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changePageNumberNextButton").hide();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        } else{
            $("#changePageNumberNextButton").show();
            $("#changePageNumberPrevButton").show();
            $doPagination = true;
        }
    } else{
        $currentPage--;
        if($currentPage <= 0){
            $("#changePageNumberPrevButton").hide();
            $currentPage = 0;
            $doPagination = true;
        }else{
            $("#changePageNumberNextButton").show();
            $("#changePageNumberPrevButton").show();
            $doPagination = true;
        }

    }


    if($doPagination){
            $('#gotoPageNumber option:eq('+$currentPage+')').attr('selected', 'selected');
            $('#gotoPageNumber').trigger('change');
        if($('#gotoPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changePageNumberNextButton").hide();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }
    }
}


    /**toggleTicketFlag(elem, elem_selected) *  *
     *Ticket Container on dashboard
     * @param elem : text to be changed(selector) from dropdown
     * @param  elem_selected : text to change in(selector) in button
     * @return : reset the ticket to load with currently selected ticketFlag(ATM/Branch)
     */
  var ticketFlag  = 'Branch';
  function toggleTicketFlag(elem, elem_selected){
        if(ticketFlag == 'Branch'){
            ticketFlag = 'ATM';
            type = 'ATM';
            ticketFilterData.set('ticketType','ATM');
            $('#atmTicketGoButton').show();
            $('#ticketGoButton').hide();
            $('#department_section').hide();
            $('#departmentTab').html(getArabicText('By Date'));
            $('#myATMTicketTabContent').show();
            $('#myBranchTicketTabContent').hide();
            $('#ticket_selected_for_contnr').text(getArabicText('Branch'));
        }else{
            ticketFlag = 'Branch';
             ticketFilterData.set('ticketType','BRANCH');
            $('#atmTicketGoButton').hide();
            $('#ticketGoButton').show();
            $('#department_section').show();
            $('#departmentTab').html(getArabicText('Department'));
            $('#myATMTicketTabContent').hide();
            $('#myBranchTicketTabContent').show();
            $('#ticket_selected_for_contnr').text(getArabicText('ATM'));
        }

         $(elem_selected).html(getArabicText(ticketFlag));
         resetTicketFilter();

  }

function fillTicketFilterArea($result){
    $("#ticketAreas").html($result);
}

function fillTicketFilterAreaPending($result){
    $("#ticketAreasPending").html($result);
}
function fillCityOption($result){
    $("#cityBranch").html($result);
}

$(document).ready(function(){
 /*  $(".td-filter-region").find('select#ticketZone').each(function(){
        $(this).change(function(){
            if($(this).val()){
                $requestData = {
                    "requestFor"     :   "ticket-area",
                    "ticket-zone" :   $(this).val()

                }
                $requestObject.initialize($requestData,"fillTicketFilterArea",null);
            }else{
                fillTicketFilterArea("<option value=''>-Select Area-</option>");
            }
        })
   }) */

   $autoSwitchFlag = false;
  // filterTickets('');
   $('#ticketTab12  a').click(function (e) {
      // e.preventDefault();
      if($(this).attr('id') == 'ignoreThis') {
        return false;
      }

      $('#ticketFilterResultDiv').html('');
      $('#ticketPaginationDiv').html('');
      $currentType = $(this).parent('li').attr('type');

      if($currentType == 'ticketEditor') {
        $('#ticketPaginationDiv').html('');
        $('#statusButtonSpan').hide();
        $('#branch-atm-switch-section').hide();
        ticketImagePopupInitilizer(false);
      } else {
        $('#statusButtonSpan').show();
        $('#branch-atm-switch-section').show();
        $autoSwitchFlag = false;
      }

      if($autoSwitchFlag !== true) {
            resetTicketFilter($currentType);
      }

      $(this).tab('show');
   });

})



