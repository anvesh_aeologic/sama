/**
 * function :: circleonimage()
 * 
 * @return
 */
 var isDragActive = false;
(function($){ 
    $.fn.circleonimage = function(options) { 
        options = $.extend({}, $.fn.circleonimage.defaultOptions, options);
        var IE = document.all?true:false;
        var me = this;
        me.IE = document.all?true:false;
        this.element;
        this.elementParent;
        this.circleRadius  =  (options.circleR)?options.circleR:40;
        this.circleCenterX =  (options.circleX)?options.circleX:140;
        this.circleCenterY =  (options.circleY)?options.circleY:140;
        this.circleBorder  =  (options.circleB)?options.circleB:1;
        this.circleColor   =  (options.circleC)?options.circleC:'red';
        this.imageId = (options.imageId);
        
        this.init = function(){
          this.each(function() {
                me.element = $(this);
                me.elementParent = me.element.parent();
                
                var elementDiv = me.createElementDiv();
                elementDiv.appendChild(me.canvasElement());
                me.elementParent.append(elementDiv);
                
                var elementDiv = me.createElementDiv();
                elementDiv.appendChild(me.imageElement(me.element.attr('src')));
                
                me.elementParent.append(elementDiv);
                
                $(me.elementParent.parent()).click(function(e){
                         me.detectPositionAndDraw(e);
                });
                me.createCircle(me.circleX,me.circleY,me.circleRadius);
                me.initializeCircleEvent();
            });
        }
        this.createElementDiv = function(){
            var elementDiv = document.createElement('DIV');
            elementDiv.style['display'] = 'none';
            return elementDiv;
        }
        
        this.canvasElement = function(){
            var canvasElement = document.createElement('CANVAS');
            canvasElement.id = 'imageCanvas';
            canvasElement.height = '640'; 
            canvasElement.width =  '480'; 
            return canvasElement;
        }
        
        this.imageElement = function(src){
            var imageElement    =  document.createElement('IMG');
            imageElement.id = 'canvasImage';
            imageElement.src    =  src;
            return imageElement;
        }
        
        this.getCircleBorder = function(){
            var border = ''+me.addPx(me.getBorderWidth())+' '+me.getBorderType()+' '+me.getBorderColor()+'';
            return border;
        }
        this.getBorderType = function(){
            return 'solid';
        }
        
        this.setBorderWidth = function(width){
            me.circleBorder = width;
        }
        
        this.getBorderWidth = function(){
            return Number(me.circleBorder);
        }
        
        this.setBorderColor = function(color){
            me.circleColor = color;
        }
        
        this.getBorderColor = function(){
            return me.circleColor;
        }
        
        this.updateCircleBorder = function(){
            $("#image-circle").css('border',me.getCircleBorder());
        }
        
        this.addPx = function(value){
            return value+"px";
        }
        
        this.getBorderRadius = function(){
            return (me.getCircleRadius())*(me.getBorderWidth());
        }
        
        this.getCircleCenter = function(){
            return me.circleCenterX+","+me.circleCenterY;
        }
        
        this.setCircleX = function(circleX){
            me.circleCenterX = circleX;
        }
        this.setCircleY = function(circleY){
            me.circleCenterY = circleY;
        }
        this.setCircleRadius = function(circleRadius){
            me.circleRadius = circleRadius;
        }
        this.getCircleRadius = function(){
            return Number(me.circleRadius);
        }
        
       
        this.createCircle = function() {
            var documentBody = $(document.body);
            
            if($("#image-circle-contaier").length){
                $("#image-circle").remove();
                $("#image-circle-contaier").remove();
            }
            
            var parentdiv = $(document.createElement('div'));
            parentdiv.attr('id','image-circle-contaier');
            parentdiv.width('100%');
            parentdiv.height('100%');
            parentdiv.css({
                        position: 'absolute',left: 1, 
                        top:68
                    });
                                    
            var circlediv = $(document.createElement('div')).attr('id','image-circle');
            
            circlediv.width(2*Number(me.circleRadius));
            circlediv.height(2*Number(me.circleRadius));
                        
            circlediv.css({
                'cursor':'pointer',
                'border':me.getCircleBorder(),
                'background':'transparent',
                'position': 'absolute', 
                'left': me.addPx(me.circleCenterX), 
                'top': me.addPx(me.circleCenterY),
                'border-radius':Number(me.circleRadius*me.circleBorder)
            });
           
            parentdiv.append(circlediv);
           
            documentBody.append(parentdiv);
            
            $("#image-circle").click(function(e){
                me.detectPositionAndDraw(e);
            })
            $("#image-circle-contaier").click(function(e){
                me.detectPositionAndDraw(e);
            });
        }
        
        this.initializeCircleEvent = function(){
            $("#image-circle").bind('mousedown',function(e){
                isDragActive = true;
                this.style.cursor = "move";
            })
            $("#image-circle").bind('mouseup',function(e){
               isDragActive = false;
               this.style.cursor = "move";
               
            })
            
            $("#image-circle").bind('mousemove',function(e){
               this.style.cursor = "move";
               var newPos =(me.getPosition(e));
               console.log(parseInt($(this).width())+"   "+parseInt(this.style.width));
               if(newPos){
                    this.style.left = (newPos.x - (parseInt(this.style.width))/2)+"px";
                    this.style.top = (newPos.y - (parseInt(this.style.height))/2)+"px";
                    
               }
            })
            
            $("#image-circle").bind('mouseover',function(e){
                this.style.cursor = "move";
                isDragActive = false;
                
            })
            
            $("#image-circle").bind('mouseout',function(e){
                this.style.cursor = "";
                 isDragActive = false;
            })
           
        }   
        
        this.getPosition = function(eventObj){
               if(isDragActive){
                  if (me.IE) { // grab the x-y pos.s if browser is IE
                    tempX = event.clientX + document.body.scrollLeft
                    tempY = event.clientY + document.body.scrollTop
                  } else {  // grab the x-y pos.s if browser is NS
                    tempX = eventObj.pageX
                    tempY = eventObj.pageY
                  }  
                  // catch possible negative values in NS4
                  if (tempX < 0){tempX = 0}
                  if (tempY < 0){tempY = 0}
                  return {
                    x : tempX,
                    y : tempY
                  }; 
              }
              return null;
        } 
        
        this.changeCirclePosition = function(){
            $("#image-circle").css('left',Number(me.circleCenterX)-Number(me.circleRadius));
            $("#image-circle").css('top',Number(me.circleCenterY)-Number(me.circleRadius));
        }
        this.detectPositionAndDraw = function(e){
              if (me.IE) { // grab the x-y pos.s if browser is IE
                tempX = event.clientX + document.body.scrollLeft
                tempY = event.clientY + document.body.scrollTop
              } else {  // grab the x-y pos.s if browser is NS
                tempX = e.pageX
                tempY = e.pageY
              }  
              // catch possible negative values in NS4
              if (tempX < 0){tempX = 0}
              if (tempY < 0){tempY = 0} 
              if(!$disableCircleMovement){
                  me.setCircleRadius($("#circle-rad").val());
                  me.setCircleX(tempX-(me.getCircleRadius()+1));
                  me.setCircleY(tempY-(me.getCircleRadius()+68));
                  me.createCircle();
              }
        }
        
        this.drawCanvasCircle = function(ctx){
            ctx.beginPath();
            ctx.arc((Number(me.circleCenterX)+Number(me.circleRadius)+15),(Number(me.circleCenterY)+Number(me.circleRadius) + 80), me.circleRadius,0, Math.PI*2, false); 
            ctx.lineWidth   = me.getBorderWidth();
            ctx.strokeStyle = me.getBorderColor();
            ctx.stroke();
        }
        
        this.saveCanvasImage = function(){
            var canvas = document.getElementById("imageCanvas");
            var ctx = canvas.getContext("2d");
            
            baseimage        = new Image();
            baseimage.onload = function() {
                ctx.drawImage(baseimage,1,1);
                me.drawCanvasCircle(ctx);
                   
                var dataURL = canvas.toDataURL("image/png");
                document.getElementById('canvasImage').src = dataURL;
                
                me.saveImageCircleOnServer();
            }
            baseimage.src    = me.element.attr('src');
        }
        
        this.saveCanvasImageOnServer = function(){
             $.post(baseUrl+''+baseBank+'/request.php',{
                "requestFor" : 'save-image',
                "img_source" : $("#canvasImage").attr('src'),
                "img_name"   : me.element.attr('src')
             },function(result){
                $("#canvasWaitIcon").hide();
                alert("Image has been updated!");
                $imageSrc = result;
                $ranDate = new Date();
                window.parent.document.getElementById('image_div_'+me.imageId).innerHTML = "<img class='changed' id='img_"+me.imageId+"' src='"+$imageSrc+"?rand="+$ranDate+"' height='100%' width='100%' />";
             })
        }
        
        this.saveImageCircleOnServer = function(){
            $("#canvasWaitIcon").show();
            $.post(baseUrl+''+baseBank+'/request.php',{
                "requestFor" :   'save-image-div',
                "imageId"    :   me.imageId,
                "divCenter"  :   me.getCircleCenter(),
                "divRadius"  :   me.getCircleRadius(),
                "divColor"   :   me.getBorderColor(),
                "divBorder"  :   me.getBorderWidth()
            },function(){
                 me.saveCanvasImageOnServer();
            })
        }
        
        this.init();
        return this;
    }
    $.fn.circleonimage.defaultOptions = {
        myclass: 'circleonimage'
    } 
})(jQuery);
