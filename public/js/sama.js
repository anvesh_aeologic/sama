/**
 * ticket info for each metrice graph on click
 */
$elementList = ['select', 'input', 'textarea'];
$sd = 0;
$ed = 0;
$st = 0;
$et = 0;
$selectedSurvey = 0;
$('#sama_ticket_result,#mada_ticket_result,#complaint-details').on("click", ".samaTicketDetail", function (that) {
    $popupObject.setWidth("800px");
    $popupObject.initialize(this, '#ticketDetailPopup', 'getSamaTicketDetail');

});

$('#mada_ticket_result,#complaint-details').on("click", ".madaComplaintDetail", function (that) {
    $popupObject.setWidth("600px");
    $popupObject.initialize(this, '#ticketDetailPopup', 'getMadaComplaitDetail');

});

$('.samaProfileTicketDetail').on("click",  function (that) {
   $popupObject.setWidth("800px");
   $popupObject.initialize(this, '#ticketDetailPopup', 'getSamaTicketDetail');
});

$('#sama_ticket_result1').on("click", ".samaTicketDetail", function(that){
    $popupObject.setWidth("800px");
    $popupObject.initialize(this,'#ticketDetailPopup','getSamaTicketDetailForPendingOrder');
        
});
var changeFilter = function(){
    $('#department_name').val('2');
};

$(document).ready(function(){
    //$('#sama_start_date').hide();
    //$('#sama_end_date').hide();
    $('#status_start_date').hide();
    $('#status_end_date').hide();
});

var optionCreater = function (that){


    if($(that).attr('name') == 'ticket_status') {
        // console.log(that.value);
        //if (that.value == '' || that.value == 'Pending') {
        //    $('#sama_start_date').show();
        //    $('#sama_end_date').show();
        //    $('#status_start_date').hide();
        //    $('#status_end_date').hide();
        //}else{
            $('#sama_start_date').show();
            $('#sama_end_date').show();
            $('#status_start_date').hide();
            $('#status_end_date').hide();

        //}
    }
    
    if($(that).attr('name') == 'survey_type'){
        var surveyType = that.value;
        if(that.value == 1){
            $('#sector').hide();
            $('#terminal_brand').hide();
            $('#survey_priority').hide();
            $('#break').hide();
            $value ='ATM';
        }else if(that.value == 2){
            $('#sector').show();
            $('#terminal_brand').show();
            $('#survey_priority').show();
            $('#break').show();
            $value ='POS';
        }else{
            $('#sector').show();
            $('#terminal_brand').show();
            $('#survey_priority').hide();
            $('#break').show();
            $value ='MERCHANT';
        }
        $("#metrices_name").html("<option value>-- All Metrics --</option>");
        //$requestObject.initialize($requestData, null,'#bank_name');

        $requestData = {
            'requestFor' : 'optionCreator',
            'val'        : $value,
            'name'       : 'department_name'
        };
        $requestObject.initialize($requestData, null,'#department_name');

        $("#city").html("<option value>-- Select City --</option>");
        //$requestObject.initialize($requestData, null,'#bank_name');

        $requestData = {
            'requestFor' : 'sama-city-list',
            'val'        :  surveyType,
            'name'       : 'city'
        };
        $requestObject.initialize($requestData, null,'#city');
    }
    
     
    if($(that).attr('name') == 'department_name'){
         var optionValues = [];
         $('#department_name option').each(function() {
             optionValues.push($(this).val());
         });

         $requestData = {
            'requestFor' : 'optionCreator',
            'val'        : that.value,
            'name'       : 'metrices_name',
            'allval'     : optionValues
         };
         $requestObject.initialize($requestData, null,'#metrices_name');
    }

    if($(that).attr('name') == 'usertype-list'){
        var optionValues = [];
        $('#usertype-list option').each(function() {
            optionValues.push($(this).val());
        });
        console.log(optionValues);
        $requestData = {
            'requestFor' : 'optionCreator',
            'val'        : that.value,
            'name'       : 'user_name',
            'allval'     : optionValues
        };
        $requestObject.initialize($requestData, null,'#user_name');
    }
    if($(that).attr('name') == 'metrics'){
        var optionValues = [];
        $('#metrics option').each(function() {
            optionValues.push($(this).val());
        });

        $requestData = {
            'requestFor' : 'optionCreator',
            'val'        : that.value,
            'name'       : 'metrics',
            'allval'     : optionValues
        };
        $requestObject.initialize($requestData, null,'#metrics');
    }

    if($(that).attr('name') == 'region' || $(that).attr('name') == 'region_name') {
        $value = that.value;
        var surveyType = $('#survey_type').val();
        $("#city").html("<option value>-- Select city --</option>");

        $requestData = {
            'requestFor': 'region-wise-city-list',
            'val': $value,
            'surveyType': surveyType
        };
        $requestObject.initialize($requestData, null, '#city');
    }


};

var verifyRegion = function (value){
    if(!value) {
        swal({
            type: 'warning',
            title: 'Please select a Region first!'
        });
        $('#city').val('');
    }
};

var vendorOptionCreater = function (that){

    if($(that).attr('name') == 'survey_type'){
        if(that.value == 1){
            $('#terminal_brand').show();
            $('#survey_priority').show();
            $('#break').hide();
            $value ='ATM';
        }else if(that.value == 2){
            $('#terminal_brand').show();
            $('#survey_priority').show();
            $('#break').show();
            $value ='POS';
        }else{
            $('#terminal_brand').show();
            $('#survey_priority').hide();
            $('#break').show();
            $value ='MERCHANT';
        }
        $("#vendors").html("<option value>-- Select Vendor --</option>");
        //$requestObject.initialize($requestData, null,'#bank_name');
        $("#terminal_brand").html("<option value>-- Select Terminal Brand --</option>");
        $("#city").html("<option value>-- Select City --</option>");
        $requestData = {
            'requestFor' : 'vendorOptionCreator',
            'val'        : that.value,
            'name'       : 'surveyType'
        };
        console.log($requestData);
        $requestObject.initialize($requestData, null,'#vendors');
        $requestData = {
            'requestFor' : 'vendorOptionCreator',
            'val'        : that.value,
            'name'       : 'brands'
        };
        console.log($requestData);
        $requestObject.initialize($requestData, null,'#terminal_brand');
        $requestData = {
            'requestFor' : 'sama-city-list',
            'val'        : that.value,
            'name'       : 'city'
        };
        console.log($requestData);
        $requestObject.initialize($requestData, null,'#city');
    }

    if($(that).attr('name') == 'vendors'){

        $("#bank_name").html("<option value>-- Select Bank --</option>");
        $("#terminal_brand").html("<option value>-- Select Terminal Brand --</option>");

        $requestData = {
            'requestFor' : 'vendorOptionCreator',
            'val'        : that.value,
            'name'       : 'vendors'
        };

        $requestObject.initialize($requestData, null,'#terminal_brand');
        $requestData = {
            'requestFor' : 'vendorOptionCreator',
            'val'        : that.value,
            'name'       : 'vendorsBank'
        };
        // console.log($requestData);
        $requestObject.initialize($requestData, null,'#bank_name');

    }


};

var madaOptionCreater = function (that){


    if($(that).attr('name') == 'complaint_status') {
        // console.log(that.value);
        //if (that.value == '' || that.value == 'Pending') {
        //    $('#sama_start_date').show();
        //    $('#sama_end_date').show();
        //    $('#status_start_date').hide();
        //    $('#status_end_date').hide();
        //}else{
        //    $('#mada_start_date').hide();
        //    $('#mada_end_date').hide();
        //    $('#status_start_date').show();
        //    $('#status_end_date').show();

        //}
    }

    if($(that).attr('name') == 'survey_type'){
        if(that.value == 'ATM'){
            $('#sector').hide();
            $('#terminal_brand').hide();
            $('#survey_priority').hide();
            $('#break').hide();
            $value ='ATM';
            $('#downloadxls').attr('href',$('#downloadxls').attr('myval')+'&survey_type=ATM');
        }else if(that.value == 'POS'){
            $('#sector').show();
            $('#terminal_brand').show();
            $('#survey_priority').show();
            $('#break').show();
            $value ='POS';
            $('#downloadxls').attr('href',$('#downloadxls').attr('myval')+'&survey_type=POS');
        }else{
            $('#sector').show();
            $('#terminal_brand').hide();
            $('#survey_priority').hide();
            $('#break').show();
            $value ='MERCHANT';
            $('#downloadxls').attr('href',$('#downloadxls').attr('myval'));
        }
        $("#metrices_name").html("<option value>-- All Metrics --</option>");
        //$requestObject.initialize($requestData, null,'#bank_name');

        $requestData = {
            'requestFor' : 'madaOptionCreator',
            'val'        : $value,
            'name'       : 'metrices_name'
        };
        $requestObject.initialize($requestData, null,'#metrices_name');
    }


    if($(that).attr('name') == 'department_name'){
         var optionValues = [];
         $('#department_name option').each(function() {
             optionValues.push($(this).val());
         });

         $requestData = {
            'requestFor' : 'madaOptionCreator',
            'val'        : that.value,
            'name'       : 'metrices_name',
            'allval'     : optionValues
         };
         $requestObject.initialize($requestData, null,'#metrices_name');
    }

    if($(that).attr('name') == 'usertype-list'){
        var optionValues = [];
        $('#usertype-list option').each(function() {
            optionValues.push($(this).val());
        });
        console.log(optionValues);
        $requestData = {
            'requestFor' : 'madaOptionCreator',
            'val'        : that.value,
            'name'       : 'user_name',
            'allval'     : optionValues
        };
        $requestObject.initialize($requestData, null,'#user_name');
    }


 };

var getSamaTicketDetail = function(that){

    $requestData = {
        'requestFor': 'sama-ticket-detail',
        'question-id': $(that).attr('question-id'),
        'ticket-number' : $(that).attr('ticket-number'),
        'status'        :$(that).attr('status'),
        'status-id'        :$(that).attr('status-id'),
        'filter-ticket-status' :$(that).attr('filter-ticket-status')
    };
    $requestObject.initialize($requestData, 'popupCallBackCustom','#ticketDetailPopup');
};
var getMadaComplaitDetail = function(that){

    $requestData = {
        'requestFor': 'mada-complaint-detail',
        'complaint-number' : $(that).attr('complaint-number'),
        'detail-for' :       $(that).attr('detail-for')
    };
    $requestObject.initialize($requestData, 'popupCallBackCustom','#ticketDetailPopup');
};

var getSamaTicketDetailForPendingOrder = function(that){
    // console.log($(that).attr('question'));
    $requestData = {
        'requestFor' : 'sama-ticket-detail-pending-order',
        'question-id': $(that).attr('question-id'),
        'ticket-number' : $(that).attr('ticket-number')
    };
    $requestObject.initialize($requestData, 'popupCallBackCustom', '#ticketDetailPopup');
};

var resetSamaTicketFilters = function ($parentFilterDiv, $requestFor) {
    $('#survey_search').val("");
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if($(this).attr('id')=='survey_type'){
               //$(this).attr('value', '1');
                $(this).attr('value', $(this).val());
               $(this).trigger('onchange');
            }else if($(this).attr('id')=='bank_name'){
                $(this).attr('value', $(this).val());
            }else{
            $(this).attr('value', '');
        }
        });
    }
//    alert("here");
    getSamaTickets($parentFilterDiv, $requestFor);
};

var resetMadaTicketFilters = function ($parentFilterDiv, $requestFor) {
    $('#survey_search').val("");
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if($(this).attr('id')=='survey_type'){
               //$(this).attr('value', '1');
                $(this).attr('value', $(this).val());
               $(this).trigger('onchange');
            }else{
                $(this).attr('value', '');
            }
        });
    }
//    alert("here");
    getMadaTickets($parentFilterDiv, $requestFor);
};
var getSamaFilter = function ($parentFilterDiv) {
    var filterData = {};
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if ($(this).val())
                filterData[$(this).attr('name')] = $(this).val();
        });
    }
    $('#statusButtonSpan').show();
    if(typeof filterData['incident_type'] == 'undefined'){
        if(typeof filterData['selectItemincident_type'] != 'undefined'){
            filterData['incident_type'] = [];
            $("[name='selectItemincident_type']").each(function(){
                if($(this).is(':checked'))
                    filterData['incident_type'].push($(this).val());
            });
            delete filterData['selectItemincident_type'];
        }
    }

    $(".excelReport").find("a").each(function() {
        //console.log("herer");
        newSrc = $(this).attr('myval') + "&surveyType=" + filterData['survey_type'];
        //console.log("newSrc");
        $(this).attr('href', newSrc);
        //newSrc = $(this).attr('href') + "&reportTime=" + fromYear + "-" + fromMonth + "&fromDate=" + fromYear + "-" + fromMonth + "-" + fromDay + "&toDate=" + toYear + "-" + toMonth + "-" + toDay;
        //$(this).attr('href', newSrc);
    });


    return filterData;
};

var getSamaTickets = function($parentFilterDiv, $requestFor,$type){
    $('#survey_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $requestDataPagination = {
        'requestFor'  : 'ticket-list-pagination',
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),

    };
    $("#sama_ticket_result_body").html("<tr><td colspan='6'>"+$("#sama_ticket_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_ticket_pagination_div');



    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),
    };
    $requestObject.initialize($requestData, null, '#sama_ticket_result');
};

var getMadaTickets = function($parentFilterDiv, $requestFor,$type){
    $('#survey_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $requestDataPagination = {
        'requestFor'  : 'mada-ticket-list-pagination',
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),

    };
    $("#mada_ticket_result_body").html("<tr><td colspan='13'>"+$("#sama_ticket_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#mada_ticket_pagination_div');



    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),
    };
    $requestObject.initialize($requestData, null, '#mada_ticket_result');
};

var sortSamaTicketList = function($orderField, $orderType){
    $filterData = getSamaFilter(".metric_detail_index_filters");
    $("#sama_ticket_result_body").html("<tr><td colspan='6'>" + $("#sama_ticket_loader").html() + "</td></tr>");
    $requestData = {
        'requestFor'  : 'ticket-list',
        'filterData' : $filterData,
        'question_id' : $("#question_id").val(),
        'orderField'  : $orderField,
        'orderType'   : $orderType,
        'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),
        'changeOrder' : true
    };
    $requestObject.initialize($requestData, null, '#sama_ticket_result');
};
var gotoSamaTicketListPage = function($element,$type){

    $("#sama_ticket_result_body").html("<tr><td colspan='6'>"+$("#sama_ticket_loader").html()+"</td></tr>");
    $filterData = getSamaFilter(".metric_detail_index_filters");

    manageNextprevButton('changeSamaTicketPage','gotoSamaTicketListPage');
    $requestData = {
        'requestFor'  : 'ticket-list',
        'filterData' : $filterData,
        'question_id' : $("#question_id").val(),
        'orderField'  : $("#ticketOrderField").val(),
        'orderType'   : $("#ticketOrderType").val(),
        'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#sama_ticket_result');
};
var gotoMadaTicketListPage = function($element,$type){

    $("#mada_ticket_result_body").html("<tr><td colspan='13'>"+$("#sama_ticket_loader").html()+"</td></tr>");
    $filterData = getSamaFilter(".metric_detail_index_filters");

    manageNextprevButton('changeMadaTicketPage','gotoMadaTicketListPage');
    $requestData = {
        'requestFor'  : 'mada-ticket-list',
        'filterData' : $filterData,
        'question_id' : $("#question_id").val(),
        'orderField'  : $("#ticketOrderField").val(),
        'orderType'   : $("#ticketOrderType").val(),
        'pageNumber'  : $("#gotoMadaTicketListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#mada_ticket_result');
};
var $pageChangeAction;
var $page;
var changeSamaTicketPage = function($pageChangeAction, $page){

    $selectedpageNumber = Number($('#gotoSamaTicketListPage option:selected').val());
    if($pageChangeAction == 'next'){
//        alert("#gotoSamaTicketListPage"+$a);
        $selectedpageNumber = $selectedpageNumber + 10;

        $("#gotoSamaTicketListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaTicketListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
//        $('select option[value="10"]').prop('selected', 'selected');
//        alert("here");
    }
    if($pageChangeAction == 'prev'){
//        alert("#gotoSamaTicketListPage"+$a);
        $selectedpageNumber = $selectedpageNumber - 10;
//        $("#gotoSamaTicketListPage"+$a).val($selectedpageNumber - 10);
        $("#gotoSamaTicketListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaTicketListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }

    gotoSamaTicketListPage('changeSamaPageNumber',$type);
};

var changeMadaTicketPage = function($pageChangeAction, $page){

    $selectedpageNumber = Number($('#gotoMadaTicketListPage option:selected').val());
    if($pageChangeAction == 'next'){
//        alert("#gotoSamaTicketListPage"+$a);
        $selectedpageNumber = $selectedpageNumber + 10;

        $("#gotoMadaTicketListPage").find('option:selected').removeAttr("selected");
        $('#gotoMadaTicketListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
//        $('select option[value="10"]').prop('selected', 'selected');
//        alert("here");
    }
    if($pageChangeAction == 'prev'){
//        alert("#gotoSamaTicketListPage"+$a);
        $selectedpageNumber = $selectedpageNumber - 10;
//        $("#gotoSamaTicketListPage"+$a).val($selectedpageNumber - 10);
        $("#gotoMadaTicketListPage").find('option:selected').removeAttr("selected");
        $('#gotoMadaTicketListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }

    gotoMadaTicketListPage('changeMadaPageNumber',$type);
};

var manageNextprevButton = function($id,$cc){
    $selectedpageNumber = Number($("#"+$cc+" "+"option:selected").val());
    if($selectedpageNumber == 0){
        $("#"+$id+"PrevButton").hide();
        $("#"+$id+"NextButton").show();
    }else if($("#"+$cc+" "+"option:selected").attr('pageclass') == 'last'){
        $("#"+$id+"PrevButton").show();
        $("#"+$id+"NextButton").hide();
    }else{
        $("#"+$id+"PrevButton").show();
        $("#"+$id+"NextButton").show();
    }
};

$(".change_selected_standard").on("change", function () {
    var baseFilterClass = $(this).attr('base-filter-id');
    var $query = "";
    $query = filterQueryString("." + baseFilterClass, true);
    if ($query)
        $query = '?' + $query;

    if ($(this).attr('value'))
        window.open(baseUrl + baseBank + $(this).attr('value') + $query);
});

var filterQueryString = function ($parentFilterDiv, returnData) {
    var queryString = "";
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if ($(this).val()) {
                if (queryString)
                    queryString += "&";
                queryString += $(this).attr('name') + "=" + encodeURIComponent($(this).val());
            }
        });
    }
    //console.log(queryString);
    if (returnData) {
        return queryString;
    } else {
        $(".box-content").find('a').each(function () {
            $surveyIdString = "";
            if ($(this).attr("href") && $(this).parent().prop('tagName') != "LI") {
                $url = $(this).attr("href").split("?");
                if ($(this).attr("survey-type") && $(this).attr("survey-name")) {
                    if (queryString)
                        $surveyIdString += "&";
                    $surveyIdString += "survey_id=" + $(this).attr("survey-type") + '&' + "survey_name=" + $(this).attr("survey-name");
                }
//              $(this).attr("href", $url[0]+"?"+queryString+$surveyIdString);
//                console.log($());
                var finalQuery = "";
                if (queryString)
                    finalQuery = queryString;
                if ($surveyIdString)
                    finalQuery += $surveyIdString;
                if (finalQuery && !($(this).find('i').attr("class"))) {
                    $(this).attr("href", $url[0] + "?" + finalQuery);
                } else {
                    $(this).attr("href", $url[0]);
                }
            }
        });
    }

};
var getSamaUserReviewDetail = function ($parentFilterDiv, $requestFor) {
    $filterData = getSamaFilter($parentFilterDiv);

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }
    $('#review_search').val('');
    delete $filterData.review_search;


    var url = baseUrl + baseBank + "/samarequest.php?requestFor=sama-user-review-detail&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#reviewExcelDownload a").attr('href', newSrc);
    $("#reviewExcelDownload a").attr('myval', newSrc);
    $("#samaReviewMailBtn").attr('myval', newSrcMail);


    // if (validateSamaFilter($filterData)) {
        var requestData = {
            'requestFor': $requestFor,
            'filterData': $filterData
        };
        $("#sama_review_panel").html($("#sama_ticket_loader").html());
        $requestObject.initialize(requestData, null, '#sama_review_panel');
    // }

};

var resetSamaUserReviewDetail = function ($parentFilterDiv, $requestFor) {
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            //console.log($(this).val());
            if($(this).attr('id')=='usertype-list'){
                $(this).attr('value', 'ALL');
                $(this).trigger('onchange');
            } else {
                $(this).attr('value', '');
            }
        });
    }
    getSamaUserReviewDetail($parentFilterDiv, $requestFor);
};

var getSamaUserLoginDetail = function ($parentFilterDiv, $requestFor) {
    $filterData = getSamaFilter($parentFilterDiv);
    if (validateSamaFilter($filterData)) {
        var requestData = {
            'requestFor': $requestFor,
            'filterData': $filterData
        };
        $("#user_login_detail_result").html($("#sama_ticket_loader").html());
        $requestObject.initialize(requestData, null, '#user_login_detail_result');
    }

};

var resetSamaUserLoginDetail = function ($parentFilterDiv, $requestFor) {
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if ($(this).prop("tagName") == "SELECT") {
                if (userBank) {
                    if ($(this).attr('name') == "quarter_name")
                        $(this).attr('value', '');
                } else {
                    $(this).attr('value', '');
                }

            } else {
                $(this).attr('value', '');
            }
        });
    }
    getSamaUserLoginDetail($parentFilterDiv, $requestFor);
};

var getSamaUserLoginHistoryDetail = function ($parentFilterDiv, $requestFor) {
    $filterData = getSamaFilter($parentFilterDiv);

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=sama-user-login-history&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#loginHistoryExcelDownload a").attr('href', newSrc);
    $("#loginHistoryExcelDownload a").attr('myval', newSrc);
    $("#qaLoginHistoryEmail").attr('myval', newSrcMail);

    // if (validateSamaFilter($filterData)) {
        var requestData = {
            'requestFor': $requestFor,
            'filterData': $filterData
        };
        $("#user_login_history_result").html($("#sama_ticket_loader").html());
        $requestObject.initialize(requestData, null, '#user_login_history_result');
    // }

};

var resetSamaUserLoginHistoryDetail = function ($parentFilterDiv, $requestFor) {
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            console.log($(this).attr('id'));
            if($(this).attr('id')=='usertype-list'){
                $(this).attr('value', 'ALL');
                $(this).trigger('onchange');
            } else  {
                $(this).attr('value', '');
            }
        });
    }
    getSamaUserLoginHistoryDetail($parentFilterDiv, $requestFor);
};


function initialize()
{
    var mapProp = {
        center: new google.maps.LatLng(22.268763552489748, 47.87841796875),
        zoom: 5,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("heatMap"), mapProp);
}


function dayOfWeek() {

    var checkedValues = $('.day_of_week:checked').map(function () {
        return this.value;
    }).get();


//    var selected = [];
//    $('#day_of_week input:checked').each(function() {
//        selected.push($(this).attr('name'));
//    });

//    alert(selected.join("\n"));
//    var elements = document.getElementsByName("day_of_week");
//    for (var i = 0, l = elements.length; i < l; i++)
//    {
//        if (elements[i].checked)
//        {
//            //alert(elements[i].value);
//            return elements[i].value;
//        }
//    }
}


function runQuery() {
    $survey_type = surveyType();
    $requestData = {
        "requestFor": "heat-map",
        "map-filters": {
            "start_date": $("#start_date").val(),
            "end_date": $("#end_date").val(),
            "start_time": $("#start_time").val(),
            "end_time": $("#end_time").val(),
            "survey_type": $survey_type
        }
    };
    $requestObject.initialize($requestData, "fillBankOptions", null);
}

function fillBankOptions($data) {

    $("#bank_name").html($data);
}

function getProvinceList($dataFor) {
    $requestData = {
        "requestFor": "province-data",
        "map-filters": {
            "bank_name": $("#bank_name").val(),
            "requestType": $dataFor
        }
    };
    $requestObject.initialize($requestData, "fillProvinceOptions", null);
}

function fillProvinceOptions($data) {

    $("#region_name").html($data);
}

function getSubCategoryList($dataFor) {
    $('#sama_category_loader').show();

    $requestData = {
        "requestFor": "sub-category-data",
        "map-filters": {
            "survey_type": $dataFor
        }
    };
    $requestObject.initialize($requestData, "fillSubCategoryOptions", null);
}

function fillSubCategoryOptions($data) {

    $('#sama_category_loader').hide();
    $("#category_name").html($data);
    applyResults('.heat_map_filters');
}

function getMatricName(that) {
    $('#sama_matrice_loader').show();
    $('#incident_type option').remove(); //to remove existing option values
    $("#incident_type").append(new Option('--All--', ''));
    $dataFor = $("#category_name option:selected").text();
    $surveyId = $("input[name='survey_type']:checked").val();
    $('#matrice option').remove(); //to remove existing option values
    $("#matrice").append(new Option('--All Metrics--', '')); // to set a default option value
    $('#incident_type option').remove(); //to remove existing option values
    $("#incident_type").append(new Option(' --All-- ', '')); // to set a default option value
    /*   --  multiple select reset ---- */
    $(".ms-drop").html('');
    $('.ms-choice').find('span').remove();
    $(".ms-choice").prepend("<span class> --All--</span>");

    // console.log("hi tere");

    //alert($(that).attr('value'));
    $requestData = {
        "requestFor": "matric-data",
        "map-filters": {
            "sub_category": $dataFor,
            "survey_id" : $surveyId
        }
    };
    $requestObject.initialize($requestData, "fillMatricOptions", null);
}

function fillMatricOptions($data) {
    $('#sama_matrice_loader').hide();
    $("#matrice").html($data);
}

function getIncidentType(that) {
    $('#sama_incident_loader').show();
    $dataFor = $("#matrice option:selected").val();
    //alert($(that).attr('value'));
    $requestData = {
        "requestFor": "incident-data",
        "map-filters": {
            "metric_id": $dataFor
        }
    };
    $requestObject.initialize($requestData, "fillIncidentOptions", null);
}

function fillIncidentOptions($data) {
    $('#sama_incident_loader').hide();
    if ($data.trim()) {
        $("#incident_type").html($data);
        $(".ms-drop").html('');
        $('#incident_type').multipleSelect();
        $("li.ms-select-all").remove();
        $(".ms-choice").prepend("<span class>" + $("#incident_type option:first-child").val() + "</span>");

        //$(".ms-drop ul li:first-child").find('input').trigger('click');
        $(".ms-drop ul li:first-child").find('input').attr("checked", 'checked');
    } else {
        $(".ms-drop").html('');
        $('.ms-choice').find('span').remove();
        $(".ms-choice").prepend("<span class> --All--</span>");
    }
}


function applyResults($parentElement) {
    
    $('#sama_graph_loader').show();

    var filterData = getSamaFilter($parentElement);
    filterData['survey_type'] = $("input[name=survey_type]:checked").val();
    var dayOfWeek = [];
    $("input:checkbox[name=day_of_week]:checked").each(function(){
        dayOfWeek.push($(this).val());
    }) ; 
    
    filterData['day_of_week'] = dayOfWeek;
     $requestData = {
        "requestFor" : "heat-map-data",
        "filterData": filterData
    };
    
    $requestObject.initialize($requestData, "fillHeatMap", null);
}
var map;
var heatmap = null;

function fillHeatMap(result) {

    $('#sama_graph_loader').hide();
    $data = JSON.parse(result);
    $regionWiseDataHtml = "";

    if($data['regionWiseData']){
        $.each($data['regionWiseData'], function(index, value){
            if(baseBank == "samaatm_v1" || baseBank == "samaatm_v2"){
                $regionWiseDataHtml += '<tr><td>';
                $regionWiseDataHtml += '<a href="javascript:void(0);" onclick="drillRegionData('+"'"+ index +"'"+','+"'.heat_map_filters'"+')">';
                $regionWiseDataHtml += '&nbsp;';
                $regionWiseDataHtml += '<span class="label label-danger countBadge">'+value+'</span>&nbsp;'+index;
                $regionWiseDataHtml += '</a>';
                $regionWiseDataHtml += '</td></tr>';
            }else{
                $regionWiseDataHtml += '<li>';
                $regionWiseDataHtml += '<a href="#" onclick="drillRegionData('+"'"+ index +"'"+','+"'.heat_map_filters'"+')">';
                $regionWiseDataHtml += '&nbsp;';
                $regionWiseDataHtml += '<span class="label label-danger countBadge">'+value+'</span>'+index;
                $regionWiseDataHtml += '</a>';
                $regionWiseDataHtml += '</li>';
            }

        });

        $("#regionSection").html($regionWiseDataHtml);
    }
    if(result != null){
        $(".heat_data_count").html($data['dataRowset'].length);
        $mapCreatorObj = new createMap();
        var $center = new google.maps.LatLng(22.268763552489748, 47.87841796875);
        if($data['is_region_filter_active']){
            /** --focusing on specific region -- **/
            $center = new google.maps.LatLng($data['dataRowset'][0]['latitude'], $data['dataRowset'][0]['longitude']);
        }
        /** --- Creating map -- */
        $mapCreatorObj.createMap($center);

        $mapCreatorObj.spiderfy();
        /** --  iterating data and pushing each amrker in array --- **/
        $.each($data['dataRowset'], function(index, value){
            if(value['latitude'] != 0 && value['longitude'] !=0){
                $mapCreatorObj.heatmapData.push(new google.maps.LatLng(value['latitude'], value['longitude']));
            }
        });

        $mapCreatorObj.activateHeatMap();     /* ---  To create heat map --- */
        $mapCreatorObj.createMarkerCluster(); /* ---  To create  marker cluster -- **/


    }else{
        $(".heat_data_count").html('0');

        map = new google.maps.Map(document.getElementById('heatMap'), {
            center:  new google.maps.LatLng(22.268763552489748, 47.87841796875),
            zoom:  5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        createHeatMap(null);
    }

}

var forEachIn= function(object,action){
    for (var property in object) {
        if (object.hasOwnProperty(property))
            action(property, object[property]);
    }
};


var createMap = function(){
    this.map = null;
    this.heatmap = null;
    this.markers = [];
    this.clusterOptions = {styles: styles[0], maxZoom    : 14};
    this.heatmapData = [];
    this.center =  new google.maps.LatLng(22.268763552489748, 47.87841796875),
    this.oms = null;
    this.markerCluster = null;

    this.createMap = function(center){
        var options = {
            center: ($data['is_region_filter_active']) ? ((center) ? center : this.center) : this.center,
            zoom: ($data['is_region_filter_active']) ? 8 : 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            }
        };
         this.map = new google.maps.Map(document.getElementById("heatMap"), options);
         this.oms = new OverlappingMarkerSpiderfier(this.map);
         changeIsrailLayer(this.map.getZoom(), this.map);
    };

    this.createMapMarker = function($dataRow){
            var me = this;
            var latLng = new google.maps.LatLng($dataRow['latitude'], $dataRow['longitude']);
            var marker = new google.maps.Marker({
                position : latLng,
                animation: google.maps.Animation.DROP,
                map : me.map,
                icon : this.getMarkerIcon(($dataRow['bank_name']).toUpperCase().replace(" ", "_"))
            });

        return marker;


    };
    this.setInfoWindow = function(marker, data){
        oms = this.oms;
        var iw = new google.maps.InfoWindow();
        oms.addListener('click', function(marker, event) {
            iw.setContent(marker.desc);
            iw.open(map, marker);
        });

        oms.addListener('spiderfy', function(markers) {
            iw.close();
        });


    };
    this.activateHeatMap = function(){

        this.heatmap = new google.maps.visualization.HeatmapLayer({
            data: this.heatmapData,
            radius: 20
        });
        this.heatmap.setMap(this.map);
    };
    this.createMarkerCluster = function(){

         this.markerCluster = new MarkerClusterer(this.map, this.markers, this.clusterOptions);
    };

    this.getMarkerIcon =function(bankName){
        bankName = bankName.replace(" ", "_");
        if(markerIcons[0][bankName])
            return new google.maps.MarkerImage(markerIcons[0][bankName].image, null, null, null, new google.maps.Size(markerIcons[0][bankName].width, markerIcons[0][bankName].height));
        else
            return new google.maps.MarkerImage(markerIcons[0]['DEFAULT'].image, null, null, null, new google.maps.Size(markerIcons[0]['DEFAULT'].width, markerIcons[0]['DEFAULT'].height));
    };
    this.spiderfy = function(){
        this.oms = new OverlappingMarkerSpiderfier(this.map);
    }


};



function changeTicketStatusForSama(changeStatusTo, status_id, that,ticketNumber){
    //if( confirm("! This action will " + changeStatusTo + " the ticket. Please confirm to Proceed. ") ){
    console.log(ticketNumber);
    //var ticketNumber =   $("#ticket_number").val();
    var $ticketStatus = '';
    if(changeStatusTo) {
        if(changeStatusTo == 'Re-open'){
            $ticketStatus = "Re-opened";
        }else if(changeStatusTo == 'Close'){
            $ticketStatus = "Closed";
        }
        $('#cboxClose').trigger( "click" );
        swal({
            title: "! This action will <b>"+ changeStatusTo + "</b> the ticket <b>"+ ticketNumber +"</b> . Please confirm to proceed. ",
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: ' Yes ',
            cancelButtonText: ' No ',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            allowOutsideClick: false
        }).then(function (isConfirm) {
            if (isConfirm === true) {
                $(that).find("i").attr('class', 'fa fa-spinner fa-spin');
                $requestData = {
                    "requestFor": "change-sama-ticket-status",
                    "status_id": status_id,
                    'requested_status': changeStatusTo
                };
                //console.log($requestData);
                $requestObject.initialize($requestData, "ticketStatusChanged", null);
                /*swal(
                    'Successfully!',
                    'Ticket <b>'+ticketNumber+'</b> has been '+$ticketStatus,
                    'success'
                );*/
            } /*else if (isConfirm === false) {
                swal(
                    'Cancelled',
                    'There is no action performed as ticket is not Closed or Re-opened',
                    'error'
                );
            }*/
        })
    }
    //}
/*
    $("#action_confirm_modal .modal-body").html("<h4> <a style='text-decoration: none;'> <i class='fa fa-bullhorn red'></i>&nbsp; Do you really want to " + changeStatusTo + " this ticket? </a></h4>");
    $('#action_confirm_modal').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#action_confirmed', function() {
            $(that).find("i").attr('class', 'fa fa-spinner fa-spin');
            $requestData = {
                "requestFor"       : "change-sama-ticket-status",
                "status_id"        : status_id,
                'requested_status' : changeStatusTo
            }
            console.log($requestData);
            $requestObject.initialize($requestData,"ticketStatusChanged", null);
        });*/
}

function ticketStatusInfo(changeStatusTo, status_id, that,ticketNumber){
    if(changeStatusTo) {
        $('#cboxClose').trigger("click");
        swal({
            title: "! This ticket belongs to last month and you have <b> exceeded the SLA period</b>, you can't <b>" + changeStatusTo + "</b> the ticket <b>" + ticketNumber + "</b></br>",
            type: 'warning',
            allowOutsideClick: false
        });

    }
}

function changeTicketStatusForSamaVendor(changeStatusTo, status_id, that,ticketNumber){
    if(changeStatusTo) {
        $('#cboxClose').trigger("click");
        swal({
            title: "! This action will <b>" + changeStatusTo + "</b> the ticket <b>" + ticketNumber + "</b></br>"
                        + "<b>Please add your remarks to "+changeStatusTo+" the ticket</b>",
            input: 'textarea',
            inputPlaceholder: 'Enter Remark..',
            inputAutoTrim: true,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: ' Yes ',
            cancelButtonText: ' No ',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            showLoaderOnConfirm: true,
            inputValidator: function (reason) {

                return new Promise(function (resolve, reject) {
                    if (reason == '' && reason.trim() == '') {
                        reject('Please enter remarks !')
                    } else {
                        $requestData = {
                            "requestFor": "change-sama-ticket-status",
                            "status_id": status_id,
                            'requested_status': changeStatusTo,
                            'reason': reason
                        };
                        console.log($requestData, this);

                        $requestObject.initialize($requestData, "ticketStatusChanged", null);
                        resolve();
                    }

                });
            },
            allowOutsideClick: false
        });

        //if( confirm("! This action will " + changeStatusTo + " the ticket. Please confirm to Proceed. ") ){
        //console.log(ticketNumber);
        //var ticketNumber =   $("#ticket_number").val();
        //var $ticketStatus = '';
        //if(changeStatusTo) {
        //    if(changeStatusTo == 'Re-open'){
        //        $ticketStatus = "Re-opened";
        //    }else if(changeStatusTo == 'Close'){
        //        $ticketStatus = "Closed";
        //    }
        //    $('#cboxClose').trigger( "click" );
        //    swal({
        //        title: "! This action will <b>"+ changeStatusTo + "</b> the ticket <b>"+ ticketNumber +"</b> . Please confirm to proceed. ",
        //        text: "",
        //        type: 'warning',
        //        showCancelButton: true,
        //        confirmButtonColor: '#3085d6',
        //        cancelButtonColor: '#d33',
        //        confirmButtonText: ' Yes ',
        //        cancelButtonText: ' No ',
        //        confirmButtonClass: 'btn btn-success',
        //        cancelButtonClass: 'btn btn-danger',
        //        buttonsStyling: false
        //    }).then(function (isConfirm) {
        //        if (isConfirm === true) {
        //            $(that).find("i").attr('class', 'fa fa-spinner fa-spin');
        //            $requestData = {
        //                "requestFor": "change-sama-ticket-status",
        //                "status_id": status_id,
        //                'requested_status': changeStatusTo
        //            };
        //            //console.log($requestData);
        //            $requestObject.initialize($requestData, "ticketStatusChanged", null);
        //            /*swal(
        //             'Successfully!',
        //             'Ticket <b>'+ticketNumber+'</b> has been '+$ticketStatus,
        //             'success'
        //             );*/
        //        } /*else if (isConfirm === false) {
        //         swal(
        //         'Cancelled',
        //         'There is no action performed as ticket is not Closed or Re-opened',
        //         'error'
        //         );
        //         }*/
        //    })
        //}
    }
}


function changeTicketStatusForClosedReopen(changeStatusTo, status_id, that,ticketNumber){
    if(changeStatusTo) {
        $('#cboxClose').trigger("click");
        swal({
            title: "! This action will <b>" + changeStatusTo + "</b> the ticket <b>" + ticketNumber + "</b></br>"
            + "<b>Please add your remarks to "+changeStatusTo+" the ticket</b>",
            input: 'textarea',
            inputPlaceholder: 'Enter Remark..',
            inputAutoTrim: true,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: ' Yes ',
            cancelButtonText: ' No ',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            showLoaderOnConfirm: true,
            inputValidator: function (reason) {

                return new Promise(function (resolve, reject) {
                    // if (reason == '' && reason.trim() == '') {
                    //     reject('Please enter remarks !')
                    // } else {
                        $requestData = {
                            "requestFor": "change-sama-ticket-status",
                            "status_id": status_id,
                            'requested_status': changeStatusTo,
                            'reason': reason
                        };
                        console.log($requestData, this);

                        $requestObject.initialize($requestData, "ticketStatusChanged", null);
                        resolve();
                    // }

                });
            },
            allowOutsideClick: false
        });

        //if( confirm("! This action will " + changeStatusTo + " the ticket. Please confirm to Proceed. ") ){
        //console.log(ticketNumber);
        //var ticketNumber =   $("#ticket_number").val();
        //var $ticketStatus = '';
        //if(changeStatusTo) {
        //    if(changeStatusTo == 'Re-open'){
        //        $ticketStatus = "Re-opened";
        //    }else if(changeStatusTo == 'Close'){
        //        $ticketStatus = "Closed";
        //    }
        //    $('#cboxClose').trigger( "click" );
        //    swal({
        //        title: "! This action will <b>"+ changeStatusTo + "</b> the ticket <b>"+ ticketNumber +"</b> . Please confirm to proceed. ",
        //        text: "",
        //        type: 'warning',
        //        showCancelButton: true,
        //        confirmButtonColor: '#3085d6',
        //        cancelButtonColor: '#d33',
        //        confirmButtonText: ' Yes ',
        //        cancelButtonText: ' No ',
        //        confirmButtonClass: 'btn btn-success',
        //        cancelButtonClass: 'btn btn-danger',
        //        buttonsStyling: false
        //    }).then(function (isConfirm) {
        //        if (isConfirm === true) {
        //            $(that).find("i").attr('class', 'fa fa-spinner fa-spin');
        //            $requestData = {
        //                "requestFor": "change-sama-ticket-status",
        //                "status_id": status_id,
        //                'requested_status': changeStatusTo
        //            };
        //            //console.log($requestData);
        //            $requestObject.initialize($requestData, "ticketStatusChanged", null);
        //            /*swal(
        //             'Successfully!',
        //             'Ticket <b>'+ticketNumber+'</b> has been '+$ticketStatus,
        //             'success'
        //             );*/
        //        } /*else if (isConfirm === false) {
        //         swal(
        //         'Cancelled',
        //         'There is no action performed as ticket is not Closed or Re-opened',
        //         'error'
        //         );
        //         }*/
        //    })
        //}
    }
}


function ticketStatusChanged(result){
    result = JSON.parse(result);
    //alert(result.message);
    $("#action-btn-holder").html('');
    $('#cboxClose').trigger( "click" );
    manageNextprevButton('changeSamaTicketPage','gotoSamaTicketListPage');
    $filterData = getSamaFilter(".metric_detail_index_filters");
    $requestDataPagination = {
        'requestFor'  : 'ticket-list-pagination',
        'filterData' : $filterData,
        'pageVal'  : $("#gotoSamaTicketListPage option:selected").val(),

    };

    $requestObject.initialize($requestDataPagination,null, '#sama_ticket_pagination_div');
    $requestData = {
        'requestFor'  : 'ticket-list',
        'filterData' : $filterData,
        'question_id' : $("#question_id").val(),
        'orderField'  : $("#ticketOrderField").val(),
        'orderType'   : $("#ticketOrderType").val(),
        'pageVal'  : $("#gotoSamaTicketListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#sama_ticket_result');
    //$("#gotoSamaTicketListPage").trigger("click");
}


function drillRegionData(regionName, $parentElement) {

    var filterData = getSamaFilter($parentElement);

    filterData['survey_type'] = $("input[name=survey_type]:checked").val();
    filterData['region_name'] = regionName;
    var dayOfWeek = [];
    $("input:checkbox[name=day_of_week]:checked").each(function(){
        dayOfWeek.push($(this).val());
    }) ;

    filterData['day_of_week'] = dayOfWeek;

    filterData['request-type'] = "heat-map-drill";
    $requestData = {
        'requestFor'  : 'drill-region-data',
        'filterData' : filterData
    };
    $popupObject.setWidth("800px");
    $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);

}

var getSamaUserSurveyDetail = function ($parentFilterDiv, $requestFor) {
    $filterData = getSamaFilter($parentFilterDiv);
    if (validateSamaFilter($filterData)) {
        var requestData = {
            'requestFor': $requestFor,
            'filterData': $filterData
        };
        $("#mss_survey_detail_result").html($("#sama_ticket_loader").html());
        $requestObject.initialize(requestData, null, '#mss_survey_detail_result');
    }

};

function downloadSamaExcelReport(){
   
    var requestedData = JSON.parse($("#sama_RequestData_fordownload").val());
    requestedData['excel-download'] = true;
    $("#download-excel-button").attr('href', baseUrl+''+baseBank+'/samarequest.php?' + $.param(requestedData));
    var target=$("#download-excel-button");
    window.open(target.attr('href'));
}

var resetSamaUserSurveyDetail = function ($parentFilterDiv, $requestFor) {
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if ($(this).prop("tagName") == "SELECT") {
                if (userBank) {
                    if ($(this).attr('name') == "quarter_name")
                        $(this).attr('value', '');
                } else {
                    $(this).attr('value', '');
                }

            } else {
                $(this).attr('value', '');
            }
        });
    }
    getSamaUserSurveyDetail($parentFilterDiv, $requestFor);
};

var getSamaReviewSurvey = function($parentFilterDiv, $requestFor,$type){
    $('#survey_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=quality-review-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail + '&admin_id='+$type;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true&admin_id='+$type;
    $("#qaReviewExcelDownload a").attr('href', newSrc);
    $("#reviewExcelDownload a").attr('myval', newSrc);
    $("#samaQaReviewMailBtn").attr('myval', newSrcMail);

    $filterData = $.extend({}, $filterData, {admin_id:$type});

    $requestDataPagination = {
        'requestFor'  : 'quality-review-pagination',
        'filterData' : $filterData
    };
    $("#sama_review_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_review_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_review_survey_result');
};

var getSamaReviewSurveySearch = function($parentFilterDiv, $requestFor,$type){

    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=quality-review-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail + '&admin_id='+$type;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true&admin_id='+$type;
    $("#qaReviewExcelDownload a").attr('href', newSrc);
    $("#reviewExcelDownload a").attr('myval', newSrc);
    $("#samaQaReviewMailBtn").attr('myval', newSrcMail);

    $filterData = $.extend({}, $filterData, {admin_id:$type});

    $requestDataPagination = {
        'requestFor'  : 'quality-review-pagination',
        'filterData' : $filterData
    };
    $("#sama_review_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_review_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_review_survey_result');
};



var getSamaQualitySurvey = function($parentFilterDiv, $requestFor,$type){
    $('#survey_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=quality-survey-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#qaExcelDownload a").attr('href', newSrc);
    $("#qaExcelDownload a").attr('myval', newSrc);
    $("#samaQaMailBtn").attr('myval', newSrcMail);

    //console.log($filterData);
    $requestDataPagination = {
        'requestFor'  : 'quality-survey-pagination',
        'filterData' : $filterData
    };
    $("#sama_quality_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_qaulity_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_qaulity_survey_result');
};


var resetSamaQualityFilters = function ($parentFilterDiv, $requestFor) {
    jQuery('#myTab a:first').tab('show');
    $('#survey_search').val("");
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            //if($(this).attr('id')=='survey_type'){d
            //    //$(this).attr('value', '1');
            //    $(this).trigger('onchange');
            //}else{
                $(this).attr('value', '');
            //}
        });
    }
//    alert("here");
    getSamaQualitySurvey($parentFilterDiv, $requestFor);
};

var getSamaDBSurvey = function($parentFilterDiv, $requestFor,$type){
    $('#survey_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=db-survey-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#qaExcelDownload a").attr('href', newSrc);
    $("#qaExcelDownload a").attr('myval', newSrc);
    $("#samaQaMailBtn").attr('myval', newSrcMail);

    //console.log($filterData);
    $requestDataPagination = {
        'requestFor'  : 'db-survey-pagination',
        'filterData' : $filterData
    };
    $("#sama_quality_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_qaulity_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_qaulity_survey_result');
};


var resetSamaDBFilters = function ($parentFilterDiv, $requestFor) {
    jQuery('#myTab a:first').tab('show');
    $('#survey_search').val("");
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            //if($(this).attr('id')=='survey_type'){d
            //    //$(this).attr('value', '1');
            //    $(this).trigger('onchange');
            //}else{
            $(this).attr('value', '');
            //}
        });
    }
//    alert("here");
    getSamaQualitySurvey($parentFilterDiv, $requestFor);
};

var getSamaSurveyTarget = function($parentFilterDiv, $requestFor,$type){
    $('#survey_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=target-survey-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#qaDownload a").attr('href', newSrc);
    $("#qaDownload a").attr('myval', newSrc);
    $("#samaTargetMailBtn").attr('myval', newSrcMail);

    //console.log($filterData);
    $("#sama_target_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_target_survey_result');
};


var resetSamaSurveyTargetFilters = function ($parentFilterDiv, $requestFor) {
    jQuery('#myTab a:first').tab('show');
    $('#survey_search').val("");
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            //if($(this).attr('id')=='survey_type'){d
            //    //$(this).attr('value', '1');
            //    $(this).trigger('onchange');
            //}else{
            $(this).attr('value', '');
            //}
        });
    }
//    alert("here");
    getSamaQualitySurvey($parentFilterDiv, $requestFor);
};

var resetSamaReviewFilters = function ($parentFilterDiv, $requestFor,userId) {


    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            //if($(this).attr('id')=='survey_type'){d
            //    //$(this).attr('value', '1');
            //    $(this).trigger('onchange');
            //}else{
            $(this).attr('value', '');
            //}
        });
    }


//    alert("here");
    getSamaReviewSurvey($parentFilterDiv, $requestFor,userId);
};

var sortSamaQualitySurveyList = function($orderField, $orderType){
    $filterData = getSamaFilter(".quality_detail_index_filters");
    $("#sama_quality_survey_result_body").html("<tr><td colspan='12'>" + $("#sama_qaulity_loader").html() + "</td></tr>");
    $requestData = {
        'requestFor'  : 'quality-survey-list',
        'filterData' : $filterData,
        'orderField'  : $orderField,
        'orderType'   : $orderType,
        'pageNumber'  : $("#gotoSamaSurveyListPage option:selected").val(),
        'changeOrder' : true
    };
    $requestObject.initialize($requestData, null, '#sama_qaulity_survey_result');
};


var gotoSamaSurveyListPage = function($element,$type){

    $("#sama_quality_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader").html()+"</td></tr>");
    $filterData = getSamaFilter(".quality_detail_index_filters");

    manageNextprevButton('changeSamaQuaitySurveyPage','gotoSamaSurveyListPage');
    $requestData = {
        'requestFor'  : 'quality-survey-list',
        'filterData' : $filterData,
        'orderField'  : $("#ticketOrderField").val(),
        'orderType'   : $("#ticketOrderType").val(),
        'pageNumber'  : $("#gotoSamaSurveyListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#sama_qaulity_survey_result');
};

var gotoSamaDBSurveyListPage = function($element,$type){

    $("#sama_quality_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader").html()+"</td></tr>");
    $filterData = getSamaFilter(".quality_detail_index_filters");

    manageNextprevButton('changeSamaDBSurveyPage','gotoSamaDBSurveyListPage');
    $requestData = {
        'requestFor'  : 'db-survey-list',
        'filterData' : $filterData,
        'orderField'  : $("#ticketOrderField").val(),
        'orderType'   : $("#ticketOrderType").val(),
        'pageNumber'  : $("#gotoSamaDBSurveyListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#sama_qaulity_survey_result');
};

var sortSamaReviewSurveyList = function($orderField, $orderType){
    $filterData = getSamaFilter(".quality_detail_index_filters");
    $("#sama_review_survey_result_body").html("<tr><td colspan='12'>" + $("#sama_qaulity_loader").html() + "</td></tr>");
    $requestData = {
        'requestFor'  : 'quality-review-list',
        'filterData' : $filterData,
        'orderField'  : $orderField,
        'orderType'   : $orderType,
        'pageNumber'  : $("#gotoSamaReviewListPage option:selected").val(),
        'changeOrder' : true
    };
    $requestObject.initialize($requestData, null, '#sama_review_survey_result');
};

var gotoSamaReviewListPage = function($element,$type){

    $("#sama_review_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader").html()+"</td></tr>");
    $filterData = getSamaFilter(".quality_detail_index_filters");

    manageNextprevButton('changeSamaQuaityReviewPage','gotoSamaReviewListPage');
    $requestData = {
        'requestFor'  : 'quality-review-list',
        'filterData' : $filterData,
        'orderField'  : $("#ticketOrderField").val(),
        'orderType'   : $("#ticketOrderType").val(),
        'pageNumber'  : $("#gotoSamaReviewListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#sama_review_survey_result');
};


var $surveyPageChangeAction;
var $surveyPage;
var changeSamaQuaitySurveyPage = function($surveyPageChangeAction, $surveyPage){

    $selectedpageNumber = Number($('#gotoSamaSurveyListPage option:selected').val());
    if($surveyPageChangeAction == 'next'){
        $selectedpageNumber = $selectedpageNumber + 10;

        $("#gotoSamaSurveyListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaSurveyListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }
    if($surveyPageChangeAction == 'prev'){
        $selectedpageNumber = $selectedpageNumber - 10;
        $("#gotoSamaSurveyListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaSurveyListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }

    gotoSamaSurveyListPage('changeSamaPageNumber',$type);
};

var changeSamaDBSurveyPage = function($surveyPageChangeAction, $surveyPage){

    $selectedpageNumber = Number($('#gotoSamaDBSurveyListPage option:selected').val());
    if($surveyPageChangeAction == 'next'){
        $selectedpageNumber = $selectedpageNumber + 10;

        $("#gotoSamaDBSurveyListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaDBSurveyListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }
    if($surveyPageChangeAction == 'prev'){
        $selectedpageNumber = $selectedpageNumber - 10;
        $("#gotoSamaDBSurveyListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaDBSurveyListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }

    gotoSamaDBSurveyListPage('changeSamaPageNumber',$type);
};


var changeSamaQuaityReviewPage = function($surveyPageChangeAction, $surveyPage){

    $selectedpageNumber = Number($('#gotoSamaReviewListPage option:selected').val());
    if($surveyPageChangeAction == 'next'){
        $selectedpageNumber = $selectedpageNumber + 10;

        $("#gotoSamaReviewListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaReviewListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }
    if($surveyPageChangeAction == 'prev'){
        $selectedpageNumber = $selectedpageNumber - 10;
        $("#gotoSamaReviewListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaReviewListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }

    gotoSamaReviewListPage('changeSamaPageNumber',$type);
};

function editSamaSurveyDetail($resultId,$surveyId){
    $(".divHide").addClass('hide');
    $(".myLoader").show();

    //$("#edit_survey_result").html("<tr><td colspan='12'>" + $("#sama_qaulity_loader").html() + "</td></tr>");
    $requestData = {
        'requestFor'  : 'edit-sama-survey',
        'resultId'    : $resultId,
        'surveyType'  : $surveyId
    };
    $requestObject.initialize($requestData, 'handleEditSurveyRequest', null, '#edit_survey_result');
}

function editSamaDBSurveyDetail($resultId,$surveyId){
    $(".divHide").addClass('hide');
    $(".myLoader").show();

    //$("#edit_survey_result").html("<tr><td colspan='12'>" + $("#sama_qaulity_loader").html() + "</td></tr>");
    $requestData = {
        'requestFor'  : 'edit-sama-db-survey',
        'resultId'    : $resultId,
        'surveyType'  : $surveyId
    };
    $requestObject.initialize($requestData, 'handleEditSurveyRequest', null, '#edit_survey_result');
}


function handleEditSurveyRequest(response,$otherData){
    try {
        response= JSON.parse(response);
    }catch (e){
        //response= response;

    }

    if(response.error == true) {
        //$($otherData).removeAttr('disabled');
        //$($otherData).find('.fa-spin').hide();
        swal(
            'Access Denied!',
            response.message,
            'error'
        ).then(function (isConfirm) {
            //if (isConfirm === undefined || isConfirm) {
                window.forceReload = true;
                window.location.reload(true);
                //window.close();
            //}
        });
    }
    else {
        $($otherData).html(response);

    }

}


function rejectSamaSurvey($this,$userId,$resultId,$branchId){


    var s = $('#modal-content').clone();
    s.find('.chosen').addClass('swal');


//Reject Survey with reason
   swal({
       title: '<b>Please select the reason, why you want to reject? <b><br/>',
       html: s.html(),
       input: 'textarea',
       inputPlaceholder: 'Please write reason here.',
       inputAutoTrim: true,
       showCancelButton: true,
       confirmButtonText: 'Submit',
       showLoaderOnConfirm: true,
       inputValidator: function(reason) {
        var  val = $('.chosen.swal').val();

        var id= $('.chosen.swal').find(':selected').attr('data-id');

           return new Promise(function(resolve,reject) {

               if(val=='' || (id =='3' && reason.trim()== '')){
                   reject('Please select reason.')
               }else {
                   if(reason.trim()== '') {
                       reason = val;
                   }else {
                       reason = val + ',' + reason;
                   }
                   $requestData = {
                       'requestFor'  : 'reject-sama-survey',
                       'userId'      : $userId,
                       'resultId'    : $resultId,
                       'branchId'    : $branchId,
                       'reason'      : reason,
                       'reason_id'   : id
                   };
                   console.log($requestData,this);

                   $requestObject.initialize($requestData,"handleRejectSamaSurvey", null);
                   resolve();
               }

           });
       },
       allowOutsideClick: false
   }).then(function(reason) {
       console.log(reason);
       if (reason) {
           swal({
               type: 'success',
               title: 'Result ID: '+$resultId+ ' has beed rejected successfully.'
           });
       }
   });


//Reject Survey without reason

    // var rejectSwal = swal({
    //     title: 'Are you sure?',
    //     text: "You want to reject this survey!",
    //     type: 'warning',
    //     showCancelButton: true,
    //     confirmButtonColor: '#d33',
    //     confirmButtonText: 'Yes, reject it!',
    //     showLoaderOnConfirm: true,
    //     allowOutsideClick: false
    // }).then(function(isConfirm) {
    //     if (isConfirm) {
    //         $($this).attr('disabled','disabled');
    //         $($this).find('.fa-spin').show();
    //
    //             $requestData = {
    //                 'requestFor'  : 'reject-sama-survey',
    //                 'userId'      : $userId,
    //                 'resultId'    : $resultId,
    //                 'branchId'    : $branchId
    //             };
    //
    //             $requestObject.initialize($requestData,"handleRejectSamaSurvey", null,$this);
    //
    //
    //
    //     }
    // })
}
function handleRejectSamaSurvey(response,$otherData){
    response= JSON.parse(response);


    if(response.error == false){
        //$($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Rejected!',
            'You have successfully rejected the survey.',
            'success'
        ).then(function(isConfirm) {
            if (isConfirm === undefined || isConfirm) {
                //window.location.reload(true);
                window.forceReload = true;
                window.close();
            }
        });
    }else if(response.error == true){
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            response.message,
            'error'
        );

    }
    else {
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            'Something went wrong. Please try again later!',
            'error'
        );

    }

}

function approveSamaSurvey($this,$userId,$resultId,$branchId,$surveyId){

    swal({
        title: 'Are you sure?',
        text: "You want to approve this survey!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#18A689',
        confirmButtonText: 'Yes, approve it!',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(function(isConfirm) {
        if (isConfirm) {
            $($this).attr('disabled','disabled');
            $($this).find('.fa-spin').show();

                $requestData = {
                    'requestFor'  : 'approve-sama-survey',
                    'userId'      : $userId,
                    'resultId'    : $resultId,
                    'branchId'    : $branchId,
                    'surveyId'    : $surveyId
                };

                $requestObject.initialize($requestData,"handleApproveSamaSurvey", null,$this);
        }
    })
}
function handleApproveSamaSurvey(response,$otherData){
    response= JSON.parse(response);


    if(response.error == false){
        //$($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Approved!',
            'You have successfully approved the survey.',
            'success'
        ).then(function(isConfirm) {
            if (isConfirm === undefined || isConfirm) {
                //window.location.reload(true);
                window.forceReload = true;
                window.close();
            }
        });
    }else if(response.error == true){
        $('.btn-cancel').removeAttr('disabled');
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            response.message,
            'error'
        ).then(function (isConfirm) {
            window.forceReload = true;
            window.close();
        });

    }
    else {
        $('.btn-cancel').removeAttr('disabled');
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            'Something went wrong. Please try again later!',
            'error'
        ).then(function (isConfirm) {
            window.forceReload = true;
            window.close();
        });

    }

}

function handleScoreCreditSamaSurvey(response,$otherData){
    response= JSON.parse(response);


    if(response.error == false){
        //$($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Submitted!',
            'You have successfully credited score to the survey.',
            'success'
        ).then(function(isConfirm) {
            if (isConfirm === undefined || isConfirm) {
                //window.location.reload(true);
                window.forceReload = true;
                window.close();
            }
        });
    }else if(response.error == true){
        $('.btn-cancel').removeAttr('disabled');
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            response.message,
            'error'
        ).then(function (isConfirm) {
            window.forceReload = true;
            window.close();
        });

    }
    else {
        $('.btn-cancel').removeAttr('disabled');
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            'Something went wrong. Please try again later!',
            'error'
        ).then(function (isConfirm) {
            window.forceReload = true;
            window.close();
        });

    }

}

function handleDeleteSamaTargets(response,$otherData){
    response= JSON.parse(response);


    if(response.error == false){
        //$($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Submitted!',
            'You have successfully deleted the survey targets.',
            'success'
        ).then(function(isConfirm) {
            if (isConfirm === undefined || isConfirm) {
                //window.location.reload(true);
                window.forceReload = true;
                // window.location.reload(true);
                $('#quality_detail_go').trigger("click");
            }
        });
    }else if(response.error == true){
        $('.btn-cancel').removeAttr('disabled');
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            response.message,
            'error'
        ).then(function (isConfirm) {
            window.forceReload = true;
        });

    }
    else {
        $('.btn-cancel').removeAttr('disabled');
        $($otherData).removeAttr('disabled');
        $($otherData).find('.fa-spin').hide();
        swal(
            'Failed!',
            'Something went wrong. Please try again later!',
            'error'
        ).then(function (isConfirm) {
            window.forceReload = true;
            window.close();
        });

    }

}

//function OnBlurInput(val, ans_code) {
//
//    console.log(val.value);
//    console.log(ans_code);
//
//    var inp_val = val.value;
//    //$("#"+ans_code).value(inp_val);
//    $("#"+ans_code).val(inp_val+'#['+ans_code+']');
//
//}


// $elementList = ['select', 'input', 'textarea'];
function saveAndApproveSamaSurvey($this,$userId,$resultId,$branchId,$surveyId){


    //console.log($surveyData);
    swal({
        title: 'Are you sure?',
        text: "You want to approve this survey!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#18A689',
        confirmButtonText: 'Yes, approve it!',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(function(isConfirm) {
        if (isConfirm) {
            //console.log(clock);
            $('.otherfree').each(function (key,val) {
                var $val = $(this).val();
                //console.log(val,$(this).data(),$val);
                var $answer = $val.replace(/["']/g, "");
                $answer = $answer.replace(/[,.]/g , '');
                $answer = $answer.replace(/[#]/g , '');
                //console.log($answer);
                $(this).val($answer +"#[" +$(this).data('otherCode')+"]");
            });
            $surveyData =  $("#survey_edit_form").serializeArray();

            clock.stop();
            $($this).attr('disabled','disabled');
            $('.btn-cancel').attr('disabled','disabled');
            $($this).find('.fa-spin').show();
            var $clockDetails = {};
            $clockDetails['hours'] = clock.time.getHours();
            $clockDetails['minutes'] = clock.time.getMinutes();
            $clockDetails['seconds'] = clock.time.getSeconds(true);
            $clockDetails['counter'] = clock.time.getMinuteCounter();

            $requestData = {
                'requestFor'  : 'save-sama-survey',
                'userId'      : $userId,
                'resultId'    : $resultId,
                'branchId'    : $branchId,
                'surveyId'    : $surveyId,
                'clockDetails': $clockDetails,
                'surveyData'  : $surveyData
            };
            //console.log($requestData);

            $requestObject.initialize($requestData,"handleApproveSamaSurvey", null,$this);
        }
    })


}

function creditScoreSamaSurvey($this,$userId,$resultId,$branchId,$surveyId,$bankName){

// for()
    var score1 = $('#score137').val();
    // alert(score1);
    //console.log($surveyData);
    swal({
        title: 'Are you sure?',
        text: "You want to credit score to this survey!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#18A689',
        confirmButtonText: 'Yes!',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(function(isConfirm) {
        if (isConfirm) {
            //console.log(clock);
            $('.otherfree').each(function (key,val) {
                var $val = $(this).val();
                //console.log(val,$(this).data(),$val);
                var $answer = $val.replace(/["']/g, "");
                $answer = $answer.replace(/[,.]/g , '');
                $answer = $answer.replace(/[#]/g , '');
                //console.log($answer);
                $(this).val($answer +"#[" +$(this).data('otherCode')+"]");
            });
            $surveyData =  $("#survey_edit_form").serializeArray();

            // clock.stop();
            $($this).attr('disabled','disabled');
            $('.btn-cancel').attr('disabled','disabled');
            $($this).find('.fa-spin').show();
            /*var $clockDetails = {};
            $clockDetails['hours'] = clock.time.getHours();
            $clockDetails['minutes'] = clock.time.getMinutes();
            $clockDetails['seconds'] = clock.time.getSeconds(true);
            $clockDetails['counter'] = clock.time.getMinuteCounter();*/

            $requestData = {
                'requestFor'  : 'credit-sama-survey',
                'userId'      : $userId,
                'resultId'    : $resultId,
                'branchId'    : $branchId,
                'surveyId'    : $surveyId,
                'bankName'    : $bankName,
                'surveyData'  : $surveyData
            };
            console.log($requestData);

            $requestObject.initialize($requestData,"handleScoreCreditSamaSurvey", null,$this);
        }
    })


}

function cancelSamaEditSurvey(){
    window.forceReload = true;

    $('.clock').trigger('timer:expire',[{alert:true, message: "Are you sure? Your unsaved data will be lost."}] );

    //window.location.reload();
}

function cancelSamaScoreCredit(){
    swal({
        title: 'Are you sure?',
        text: "You want to exit from this survey!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#18A689',
        confirmButtonText: 'Yes!',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(function(isConfirm) {
        if (isConfirm) {
            window.forceReload = true;
            window.location.reload(true);
        }
    })
}

function samaUnlockSurvey($this,$branchId){
    $($this).attr('disabled','disabled');
    $($this).find('.fa-spin').show();

    $requestObject.initialize({
        'requestFor'  : 'editable-sama-survey',
        'branchId'    : $branchId
    },'handleUnlock', null,$this);


}
function handleUnlock($result){
    window.forceReload = true;
    window.location.reload(true);
}

function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;

    if(m == 0){
        return (s < 10 ? '0'+s : s)+" sec "; //zero padding on minutes and seconds
    }
    return (m < 10 ? '0'+m : m)+" min : "+(s < 10 ? '0'+s : s)+" sec "; //zero padding on minutes and seconds
    //return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
}

// Alert Plugin Script

(function ($) {
    $.alert_ext = {

        defaults: {
            autoClose: true,
            closeTime: 5000,
            withTime: false,
            type: 'danger',
            position: ['center', [-0.42, 0]],
            title: false,
            close: '',
            speed: 'normal',
            isOnly: true,
            minTop: 10,
            onShow: function () {
            },
            onClose: function () {
            }
        },


        tmpl: '<div class="alert alert-dismissable ${State}"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4 style="white-space: nowrap;">${Title}</h4><p>${Content}</p></div>',


        init: function (msg, options) {
            this.options = $.extend({}, this.defaults, options);

            this.create(msg);
            this.set_css();

            this.bind_event();

            return this.alertDiv;
        },

        template: function (tmpl, data) {
            $.each(data, function (k, v) {
                tmpl = tmpl.replace('${' + k + '}', v);
            });
            return $(tmpl);
        },


        create: function (msg) {
            this.alertDiv = this.template(this.tmpl, {
                State: 'alert-' + this.options.type,
                Title: this.options.title,
                Content: msg
            }).hide();
            if (!this.options.title) {
                $('h4', this.alertDiv).remove();
                $('p', this.alertDiv).css('margin-right', '15px');
            }
            if (this.options.isOnly) {
                $('body > .alert').remove();
            }
            this.alertDiv.appendTo($('body'));
        },


        set_css: function () {
            var alertDiv = this.alertDiv;


            alertDiv.css({
                'position': 'fixed',
                'z-index': 10001 + $(".alert").length
            });


            var ie6 = 0;
            if ($.browser && $.browser.msie && $.browser.version == '6.0') {
                alertDiv.css('position', 'absolute');
                ie6 = $(window).scrollTop();
            }


            var position = this.options.position,
                pos_str = position[0].split('-'),
                pos = [0, 0];
            if (position.length > 1) {
                pos = position[1];
            }


            if (pos[0] > -1 && pos[0] < 1) {
                pos[0] = pos[0] * $(window).height();
            }
            if (pos[1] > -1 && pos[1] < 1) {
                pos[1] = pos[1] * $(window).width();
            }



            for (var i in pos_str) {
                if ($.type(pos_str[i]) !== 'string') {
                    continue;
                }
                var str = pos_str[i].toLowerCase();

                if ($.inArray(str, ['left', 'right']) > -1) {
                    alertDiv.css(str, pos[1]);
                } else if ($.inArray(str, ['top', 'bottom']) > -1) {
                    alertDiv.css(str, pos[0] + ie6);
                } else {
                    alertDiv.css({
                        'top': ($(window).height() - alertDiv.outerHeight()) / 2 + pos[0] + ie6,
                        'left': ($(window).width() - alertDiv.outerWidth()) / 2 + pos[1]
                    });
                }
            }

            if (parseInt(alertDiv.css('top')) < this.options.minTop) {
                alertDiv.css('top', this.options.minTop);
            }
        },


        bind_event: function () {
            this.bind_show();
            this.bind_close();

            if ($.browser && $.browser.msie && $.browser.version == '6.0') {
                this.bind_scroll();
            }
        },


        bind_show: function () {
            var ops = this.options;
            this.alertDiv.fadeIn(ops.speed, function () {
                ops.onShow($(this));
            });
        },


        bind_close: function () {
            var alertDiv = this.alertDiv,
                ops = this.options,
                closeBtn = $('.close', alertDiv).add($(this.options.close, alertDiv));

            closeBtn.bind('click', function (e) {
                alertDiv.fadeOut(ops.speed, function () {
                    $(this).remove();
                    ops.onClose($(this));
                });
                e.stopPropagation();
            });


            if (this.options.autoClose) {
                var time = parseInt(this.options.closeTime / 1000);
                if (this.options.withTime) {
                    //$('p', alertDiv).append('<span>...<em>' + time + '</em></span>');
                    //if(time >= 10){
                        $('p', alertDiv).append('<strong> Expires in: <em data-time='+time+'>' + secondsTimeSpanToHMS(time) + '</em></strong>');
                    //}else{
                    //    $('p', alertDiv).append('<span> Expires in: <em>' + time + '</em></span>');
                    //}
                }
                var timer = setInterval(function () {
                    $('em', alertDiv).data('time',--time).text(secondsTimeSpanToHMS(time));
                    if (!time) {
                        clearInterval(timer);
                        closeBtn.trigger('click');
                        $('.clock').trigger('timer:expire');
                    }
                }, 1000);
            }
        },


        bind_scroll: function () {
            var alertDiv = this.alertDiv,
                top = alertDiv.offset().top - $(window).scrollTop();
            $(window).scroll(function () {
                alertDiv.css("top", top + $(window).scrollTop());
            })
        },


        check_mobile: function () {
            var userAgent = navigator.userAgent;
            var keywords = ['Android', 'iPhone', 'iPod', 'iPad', 'Windows Phone', 'MQQBrowser'];
            for (var i in keywords) {
                if (userAgent.indexOf(keywords[i]) > -1) {
                    return keywords[i];
                }
            }
            return false;
        }
    };

    $.alert = function (msg, arg) {
        if ($.alert_ext.check_mobile()) {
            alert(msg);
            return;
        }
        if (!$.trim(msg).length) {
            return false;
        }
        if ($.type(arg) === "string") {
            arg = {
                title: arg
            }
        }
        if (arg && arg.type == 'error') {
            arg.type = 'danger';
        }
        return $.alert_ext.init(msg, arg);
    }
})(jQuery);

var UserPanelRequestCreator = function(){
    var data;
    return {
        postUserData : function($requestData){
            var that = this;
            $.ajax({
                type: 'POST',
                url: baseUrl+''+baseBank+'/request.php',
                data: $requestData,
                success: function(response){
                    $userDeleteResposnse = $.parseJSON(response);
                    if($userDeleteResposnse){
                        performPostAction($userDeleteResposnse);
                    }
                }
            });
        }
    };
};

function performPostAction($responseData){

    $userId = $responseData['userId'];

    $responseText = $responseData['response'];
    if($responseText=='Deletion Successfull'){
        $('#image_'+$userId).css('display','none');
        $('#userData_'+$userId).hide("slow");
    }
}


var userAjaxRequest = UserPanelRequestCreator();
var isRequestedByClient = true;
//$(document).ready(function(){
//    $('.iphone-toggle').iphoneStyle({
//        onChange : function(){
//            if(isRequestedByClient){
//                $userId  = $(this.elem).data('id');
//                $checkedFlag = $(this.elem).is(':checked');
//                $postData = {
//                    'requestFor'    :  'update-user-availibily',
//                    'requestParam'  :   {
//                        'userId'      :  $(this.elem).data('id'),
//                        'requestType' :  "user-updation"
//                    }
//                }
//
//                $confirm = confirm("Are you sure?");
//                //console.log($confirm);
//                if ($confirm === true){
//                    $userActivationResposnse =  userAjaxRequest.postUserData($postData);
//                }else{
//                    isRequestedByClient = false;
//                    if($checkedFlag){
//                        $(this.elem).removeAttr('checked');
//                    }else{
//                        $(this.elem).attr('checked','checked');
//                    }
//                }
//            }
//            isRequestedByClient = true;
//            //console.log("here");
//        }
//    });
//    initializeListFilter("#searchField");
//
//});
//
$formData = {};
$(document).ready(function(){

});

function deleteUser($userId){

    var rejectSwal = swal({
        title: 'Are you sure?',
        text: "You want to delete this user!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(function(isConfirm) {
        if (isConfirm) {
            $('#image_'+$userId).css('display','inline');
            $postData = {
                'requestFor'    :  'update-user-availibily',
                'requestParam'  :   {
                    'userId'      :  $userId,
                    'requestType' :  "user-deletion"
                }
            };
            userAjaxRequest.postUserData($postData);
            location.reload();



        }
    })

   
}

function departmentVisibility($currentElement){
    $(".department-box").each(function(){
        if( $($currentElement).val() ){
            if( $(this).hasClass('dept-'+$($currentElement).val()) ){
                $(this).show();
            }else{
                $(this).hide();
            }
        }else{
            $(this).show();
        }
    })
}

function userRolesSelect($currentElement)
{
    $("#user-region").prop('selectedIndex',0);
    $("#user-department").prop('selectedIndex',0);
    if($($currentElement).val()=='Department Admin'){
        $("#department-name").html("Department Name");
        $("#user-department").css("display","inline");
        $("#user-region").css("display","none");
    } else if($($currentElement).val()=='Guest'){
        // $("#user-department").prop("disabled", true);
        $("#department-name").html("Department Name");
        $("#user-department").css("display","none");
        $("#user-region").css("display","none");
    } else if($($currentElement).val()=='Super User'){
        $("#department-name").html("Department Name");
        $("#user-department").css("display","none");
        $("#user-region").css("display","none");
    } else if($($currentElement).val()=='Region User'){
        $("#department-name").html("Regional Office");
        $("#user-region").css("display","inline");
        $("#user-department").css("display","none");
    }else {
        $("#department-name").html("Department Name");
        $("#user-department").css("display","inline");
        $("#user-region").css("display","none");
    }

    if($($currentElement).attr('value') =='Sama'){
        $("#survey_limit").hide();
    }else{
        $("#survey_limit").show();
    }
}

function userTypeVisibility($currentElement)
{
    $(".department-box").each(function(){

        if( $($currentElement).val() =='Super Admin') {

            $(".super_admin").show();
            $(".admin").hide();
        } else if( $($currentElement).val() =='Admin'){

            $(".super_admin").hide();
            $(".admin").show();
        } else {

            $(".super_admin").show();
            $(".admin").show();
        }
    })
}


function manageUser($userData)
{

    $('#userupdationMessage').html('');
    $('#usercreationMessage').html('');

    $('#userupdationMessage').html('<img src="'+baseUrl+'public/img/ajax-loaders/ajax-loader-1.gif" title="Loading" style="/* margin-top:-4px; *//* display:none; *//* margin-left: -18px; */">');
    $('#usercreationMessage').html('<img src="'+baseUrl+'public/img/ajax-loaders/ajax-loader-1.gif" title="Loading" style="/* margin-top:-4px; *//* display:none; *//* margin-left: -18px; */">');

    $postData = {

        'requestFor'    :  'manage-user-roles',
        'requestParam'  :   {
            'departMentId'  : $userData['user-department'],
            'userEmail'     : $userData['user-email'],
            'fullName'      : $userData['full-name'],
            'userName'      : $userData['user-name'],
            'userPassword'  : $userData['user-password'],
            'sendMailStatus': $userData['send-email'],
            'currentStatus' : $userData['current-status'],
            'userOperation' : $userData['user-request'],
            'userId'        : $userData['user-id'],
            'userRole'      : $userData['user-roles'],
            'userRegionId'  : $userData['user-region'],
            'userLimit'     : $userData['user-limit'],
            'requestType'   : "user-profile-action"

        }
    };

    $.ajax({
        type: 'POST',
        url: baseUrl+''+baseBank+'/samarequest.php',
        data: $postData,
        success: function(response){
            $('#userupdationMessage').css('display','none');
            $('#usercreationMessage').css('display','none');
            $userProfileResponse = JSON.parse(response);
            if($userProfileResponse){
                performPostProfileAction($userProfileResponse);
                //location.replace(baseUrl+''+baseBank+'/sama-user.php');
               // location.reload();
            }
        }
    });
}

function performPostProfileAction($userProfileResponse)
{
    $operationResponse = $userProfileResponse.operation;
    $status = $userProfileResponse.status;
    console.log($operationResponse+"  "+$status);
    if($operationResponse=='UserUpdation' && $status=='User updated'){

        swal(
            'Congrats !',
            'User Updated Successfully',
            'success'
        ).then(function(isConfirm) {
            if (isConfirm === undefined || isConfirm) {
                // window.location.replace(baseUrl+''+baseBank+'/sama-user.php');
                window.location.href= (baseUrl+''+baseBank+'/sama-user.php');
            }
        });
        // $('#userupdationMessage').css('display','inline');
        // $('#userupdationMessage').html('<span class="label label-success" style="font-size:14px;">User Updated Successfully!</span>');
        //
    }

    if($operationResponse=='UserCreation' && $status=='Created'){

        swal(
            'Congrats !',
            'User Created Successfully',
            'success'
        ).then(function(isConfirm) {
            if (isConfirm === undefined || isConfirm) {
                // window.location.replace(baseUrl+''+baseBank+'/sama-user.php');
                window.location.href= (baseUrl+''+baseBank+'/sama-user.php');
            }
        });
    //     $('#usercreationMessage').css('display','inline');
    //     $('#usercreationMessage').html('<span class="label label-success" style="font-size:14px;">User Created Successfully!</span>');
    //
        }

    if($operationResponse=='UserCreation' && $status=='Already Exist'){
        swal(
            '',
            'User Already Exist!',
            'error'
        );

        // $('#usercreationMessage').css('display','inline');
        // $('#usercreationMessage').html('<span class="label label-important" style="font-size:14px;">User Already Exist!</span>');

    }

}


function validateFormData($formData,$type)
{
    $departMentVal = $formData['user-department'];
    $emailAddress  = $formData['user-email'];
    $userNameVal   = $formData['user-name'];
    $fullNameVal   = $formData['full-name'];
    $userPassword  = $formData['user-password'];
    $userRole      = $formData['user-roles'];

    $isValidationError = 0;

    if($type=='profile'){
        if($userRole == 'Department Admin'){
            if($departMentVal==''){
                $('#deprtment_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#deprtment_error').css('display','none');
            }
        } else{
            $('#deprtment_error').css('display','none');
        }
    }

    if($userRole==''){
        $('#userrole_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#userrole_error').css('display','none');
    }

    if($emailAddress==''){
        $('#email_error').css('display','inline');
        $isValidationError = 1;
    } else{
        var emailRegularExpression = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (!$emailAddress.match(emailRegularExpression)) {
            $('#email_error').html('Please Enter Valid Email Address!');
            $('#email_error').css('display','inline');
            $isValidationError = 1;
        } else{
            $('#email_error').css('display','none');
        }
    }

    if($userNameVal==''){
        $('#username_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#username_error').css('display','none');

    }

    if($fullNameVal==''){
        $('#fullname_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#fullname_error').css('display','none');

    }

    if($userPassword==''){
        $('#userpassword_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#userpassword_error').css('display','none');
    }
    //console.log($isValidationError);
    return $isValidationError == 0;
}
function validateEditProfileFormData($formData)
{
    $userFullName = $formData['user-full-name'];
    $emailAddress  = $formData['user-email'];
    $userMobile   = $formData['user-mobile'];
    $userSmsNubmer  = $formData['user-sms-number'];
    $userCostCenter      = $formData['user-cost-center'];

    $isValidationError = 0;



    if($userFullName==''){
        $('#user_full_name_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#user_full_name_error').css('display','none');
    }
    if($emailAddress==''){
        $('#user_email_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#user_email_error').css('display','none');
    }
    if($userMobile==''){
        $('#user_mobile_error').css('display','inline');
        $isValidationError = 1;
    } else {
        var mobileRegularExpression = new RegExp(/^[0-9]{8,10}$/);
        if (!$userMobile.match(mobileRegularExpression)) {
            $('#user_mobile_error').html('Please enter upto 10 digit valid mobile number!');
            $('#user_mobile_error').css('display','inline');
            $isValidationError = 1;
        } else{
            $('#user_mobile_error').css('display','none');
        }
    }
    if($userSmsNubmer==''){
        $('#sms_number_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#sms_number_error').css('display','none');
    }
    if($userCostCenter==''){
        $('#user_cost_center_error').css('display','inline');
        $isValidationError = 1;
    } else {
        var numberRegularExpression = new RegExp(/^\d+$/);
        if (!$userCostCenter.match(numberRegularExpression)) {
            $('#user_cost_center_error').html('Please enter valid cost center! Only number please.');
            $('#user_cost_center_error').css('display','inline');
            $isValidationError = 1;
        } else{
            $('#user_cost_center_error').css('display','none');
        }
    }


    if($emailAddress==''){
        $('#user_email_error').css('display','inline');
        $isValidationError = 1;
    } else{
        var emailRegularExpression = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (!$emailAddress.match(emailRegularExpression)) {
            $('#user_email_error').html('Please enter valid email address!');
            $('#user_email_error').css('display','inline');
            $isValidationError = 1;
        } else{
            $('#user_email_error').css('display','none');
        }
    }

    //console.log($isValidationError);
    return $isValidationError == 0;
}

function validateCreateFormData($formData,$type)
{

    $departMentVal = $formData['user-department'];
    $emailAddress  = $formData['user-email'];
    $userNameVal   = $formData['user-name'];
    $fullNameVal   = $formData['full-name'];
    $userPassword  = $formData['user-password'];
    $userRole      = $formData['user-roles'];
    $userRegion    = $formData['user-region'];
    $userLimit    = $formData['user-limit'];

    $isValidationError = 0;

    if($type=='create'){
        if($userRole == 'Department Admin'){
            if($departMentVal==''){
                $('#deprtment_error').html('Please select department');
                $('#deprtment_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#deprtment_error').css('display','none');
            }
        } else if($userRole == 'Region User'){
            if($userRegion == ''){
                $('#deprtment_error').html('Please select regional office');
                $('#deprtment_error').css('display','inline');
                $isValidationError = 1;
            } else {
                $('#deprtment_error').css('display','none');
            }
        }else{
            $('#deprtment_error').css('display','none');
        }
    }

    if($userRole==''){
        $('#userrole_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#userrole_error').css('display','none');
    }

    if($emailAddress==''){
        $('#email_error').css('display','inline');
        $isValidationError = 1;
    } else{
        var emailRegularExpression = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (!$emailAddress.match(emailRegularExpression)) {
            $('#email_error').html('Please Enter Valid Email Address!');
            $('#email_error').css('display','inline');
            $isValidationError = 1;
        } else{
            $('#email_error').css('display','none');
        }
    }

    if($userNameVal==''){
        $('#username_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#username_error').css('display','none');

    }

    if($fullNameVal==''){
        $('#fullname_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#fullname_error').css('display','none');

    }

    if($userPassword==''){
        $('#userpassword_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#userpassword_error').css('display','none');
    }

    if($userLimit=='' && $userRole !== 'Sama'){
        $('#userlimit_error').css('display','inline');
        $isValidationError = 1;
    } else {
        $('#userlimit_error').css('display','none');
    }

    return $isValidationError == 0;
}


//function initializeListFilter($parentInputBox){
//
//    $($parentInputBox).val("");
//    $($parentInputBox).keyup(function () {
//
//        //first we create a variable for the value from the search-box
//        var searchTerm = $($parentInputBox).val();
//        console.log(searchTerm);
//        //then a variable for the list-items (to keep things clean)
//        var listItem = $('.result-item').children('span');
//
//        //extends the default :contains functionality to be case insensitive
//        //if you want case sensitive search, just remove this next chunk
//        $.extend($.expr[':'], {
//            'containsi': function(elem, i, match, array)
//            {
//                return (elem.textContent || elem.innerText || '').toLowerCase()
//                        .indexOf((match[3] || "").toLowerCase()) >= 0;
//            }
//        });//end of case insensitive chunk
//
//
//        //this part is optional
//        //here we are replacing the spaces with another :contains
//        //what this does is to make the search less exact by searching all words and not full strings
//        var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
//
//        if(searchSplit){
//            //here is the meat. We are searching the list based on the search terms
//            $(".result-item .item").not(":containsi('" + searchSplit + "')").each(function(e)   {
//                $(this).removeClass('highlight');
//                //add a "hidden" class that will remove the item from the list
//                $(this).parent().parent().addClass('hidden');
//            });
//
//            //this does the opposite -- brings items back into view
//            $(".result-item .item:containsi('" + searchSplit + "')").each(function(e) {
//                //remove the hidden class (reintroduce the item to the list)
//                $(this).addClass('highlight');
//                $(this).parent().parent().removeClass('hidden');
//            });
//        }else{
//            $(".result-item .item").each(function(e) {
//                //remove the hidden class (reintroduce the item to the list)
//                $(this).removeClass('highlight');
//                $(this).parent().parent().removeClass('hidden');
//            });
//        }
//    });
//}

function changeUserStatus(element,admin_id,status){
    if(status == 1){
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=0;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=1;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }else{
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=1;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=0;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }



    var $requestData = {
        'requestFor': 'update-user-status',
        'admin_id': admin_id,
        'status':status
    };
    $requestObject.initialize($requestData, null,null);

    var id='#user_'+admin_id+' '+'button';
// console.log(id);

    /* reverse locking status */
    $(id).eq(1).toggleClass('locked_inactive lock' +
        'ed_active btn-default btn-info');
    $(id).eq(0).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');


}


var optionStatusCreater = function (that) {
    if ($(that).attr('name') == 'report_status') {
        // console.log(that.value);
        if (that.value == 'byTicketStatus') {
            $('.report_ticket_status').show();
        } else {
            $('.report_ticket_status').hide();
        }
    }
};

var getSamaQualitySurveySearch = function($parentFilterDiv, $requestFor,$type){
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=quality-survey-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#qaExcelDownload a").attr('href', newSrc);
    $("#qaExcelDownload a").attr('myval', newSrc);
    $("#samaQaMailBtn").attr('myval', newSrcMail);

    //console.log($filterData);
    $requestDataPagination = {
        'requestFor'  : 'quality-survey-pagination',
        'filterData' : $filterData
    };
    $("#sama_quality_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_qaulity_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_qaulity_survey_result');
};

var getSamaDBSurveySearch = function($parentFilterDiv, $requestFor,$type){
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=db-survey-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#qaExcelDownload a").attr('href', newSrc);
    $("#qaExcelDownload a").attr('myval', newSrc);
    $("#samaQaMailBtn").attr('myval', newSrcMail);

    //console.log($filterData);
    $requestDataPagination = {
        'requestFor'  : 'db-survey-pagination',
        'filterData' : $filterData
    };
    $("#sama_quality_survey_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_qaulity_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_qaulity_survey_result');
};

var getSamaTicketSearch = function($parentFilterDiv, $requestFor,$type){

    $filterData = getSamaFilter($parentFilterDiv);
    $requestDataPagination = {
        'requestFor'  : 'ticket-list-pagination',
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),

    };
    $("#sama_ticket_result_body").html("<tr><td colspan='6'>"+$("#sama_ticket_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_ticket_pagination_div');



    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),
    };
    $requestObject.initialize($requestData, null, '#sama_ticket_result');
};

var getMadaAppTicketSearch = function($parentFilterDiv, $requestFor,$type){

    $filterData = getSamaFilter($parentFilterDiv);
    $requestDataPagination = {
        'requestFor'  : 'mada-ticket-list-pagination',
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),

    };
    $("#mada_ticket_result_body").html("<tr><td colspan='13'>"+$("#sama_ticket_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#mada_ticket_pagination_div');



    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
        //'pageNumber'  : $("#gotoSamaTicketListPage option:selected").val(),
    };
    $requestObject.initialize($requestData, null, '#mada_ticket_result');
};


function samaActiveActionButtons(){

    $ticketList = getSamaSelectedTickets('current-status');
    actionButtonVisibilityControl($ticketList);

}

function samaActiveQVPActionButtons(){

    $surveyList = getSamaSelectedSurveys('current-status');
    console.log($surveyList);
    actionButtonSurveyVisibilityControl($surveyList);

}

function getSamaSelectedTickets($attribute){
    $ticketList = [];
    $("#sama_ticket_result_body").find("input.sama_ticket_panel_checkbox").each(function(){

        if($(this).is(":checked")){
            $ticketList.push($(this).data($attribute));
        }
    });
     return $ticketList;
}

function getSamaSelectedSurveys($attribute){
    $surveyList = [];
    $("#sama_quality_survey_result_body").find("input.sama_survey_checkbox").each(function(){

        if($(this).is(":checked")){
            $surveyList.push($(this).data($attribute));
        }
    });
    return $surveyList;
}

function getSamaSelectedStatusId($attribute){
    $statusList = [];
    $("#sama_ticket_result_body").find("input.sama_ticket_panel_checkbox").each(function(){

        if($(this).is(":checked")){
            $statusList.push($(this).data($attribute));
        }
    });
    return $statusList;
}

function getSamaSelectedSurveyId($attribute){
    $statusList = [];
    $("#sama_quality_survey_result_body").find("input.sama_survey_checkbox").each(function(){

        if($(this).is(":checked")){
            $statusList.push($(this).data($attribute));
        }
    });
    return $statusList;
}

function actionButtonVisibilityControl($ticketStatusList){
    $buttonList =   getActionButtons();
    $buttonVisibilityFlag = {};
    for(var buttonListIndex in $buttonList){
        $buttonVisibilityFlag[buttonListIndex] = true;
    }

    activateButton(['closeButton','resolveButton','transferButton','approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'');
    if(!($ticketStatusList.length === 0)){
        for(var ticketListIndex in $ticketStatusList){
            switch($ticketList[ticketListIndex].toUpperCase()){
                case "PENDING"          : $buttonVisibilityFlag = activateButton(['closeButton','resolveButton','transferButton'],$buttonVisibilityFlag,'activate');
                    $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                    break;

                case "RESOLVED"          : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
                    $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
                    break;
                default : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
                    $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
                    break;
                    

              }
        }
    }
}

function actionButtonSurveyVisibilityControl($ticketStatusList){
    $buttonList =   getActionButtons();
    $buttonVisibilityFlag = {};
    for(var buttonListIndex in $buttonList){
        $buttonVisibilityFlag[buttonListIndex] = true;
    }
    activateButton(['closeButton','resolveButton','transferButton','approvalButton','rejectButton','reopenButton','reserveButton'],$buttonVisibilityFlag,'');
    if(!($ticketStatusList.length === 0)){
        for(var ticketListIndex in $ticketStatusList){
            switch($surveyList[ticketListIndex].toUpperCase()){
                case "PENDING"          : $buttonVisibilityFlag = activateButton(['closeButton','resolveButton','transferButton','reserveButton'],$buttonVisibilityFlag,'activate');
                    $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                    break;

                case "RESOLVED"          : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
                    $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
                    break;
                default : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
                    $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
                    break;


            }
        }
    }
}

function activateButton($buttonList,$buttonVisibilityFlag,$action){
    $availableButtonList =   getActionButtons();
    $classToAdd = 'btn btn-disabled';
    if($action == 'activate'){

        $classToAdd = 'btn btn-success';
    }

    for($buttonListIndex in $buttonList){

        if($buttonVisibilityFlag[ $buttonList[$buttonListIndex] ]){
            if($availableButtonList[ $buttonList[$buttonListIndex] ]){

                $($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class',$classToAdd);

                $("#statusButtonSpan").find('a').each(function(){
                    if($(this).attr('status')=='Close'){
                        $(this).attr('class', 'btn btn-success')
                    }
                    else if($(this).attr('status')=='Reserved'){
                        $(this).attr('class', 'btn btn-success')
                    }
                });



                /* Change the button status */
                if($buttonList[$buttonListIndex] == 'transferButton'){

                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).unbind('click');
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class',$($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class')+" btn-dashboard");
                    activateTransferButton();
                }
                if($action == 'activate'){
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).show();
                }else{
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).hide();
                }
            }
        }

        if($action == 'deactivate'){
            $buttonVisibilityFlag[ $buttonList[$buttonListIndex] ] = false;
        }
    }
    return $buttonVisibilityFlag;
}


function changeSamaStatusTo($element){

    var $isCloseAllowed = 0;
    var isClosedTicketExist= 0;
    if($($element).attr('status')=='Close'){
        if($($element).attr('status') == 'undo'){
            $ticketList =   new Array($($element).attr('ticket-number'));
        }else{
            $ticketList =   getSamaSelectedTickets('ticket-number');
        }
        changeTicketStatus($ticketList,null,$($element).attr('status'));

        // swal({
        //     title: 'Are you sure?',
        //     text: "You want to Close the selected ticket!",
        //     type: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: ' Yes ',
        //     cancelButtonText: ' No ',
        //     confirmButtonClass: 'btn btn-success',
        //     cancelButtonClass: 'btn btn-danger'
        // }).then(function(isConfirm) {
        //
        //     if (isConfirm) {
        //         if($($element).attr('status') == 'undo'){
        //             $ticketList =   new Array($($element).attr('ticket-number'));
        //         }else{
        //             $ticketList =   getSamaSelectedTickets('ticket-number');
        //         }
        //         changeTicketStatus($ticketList,null,$($element).attr('status'));
        //     }
        // });


        // if(confirm("Are you sure you want to close this ticket?")){
        //     $isCloseAllowed = 1;
        //     isClosedTicketExist = 1;
        // }
    } else {

        switch( ($($element).attr('status')).toUpperCase() ){
            case "TRANSFER" : case "RESOLVE" : case "UNDO" : case "APPROVE" : case "REJECT" :case "PENDING" : $isCloseAllowed = 1; break;
            default: $isCloseAllowed = 0;
        }
    }

    // console.log($isCloseAllowed);
    if( $($element).hasClass('disabled') ){

    }

    // if($isCloseAllowed){
    //
    //     // $("#sama_ticket_loader1").show();
    //     // $("#sama_ticket_result").addClass('hide');
    //     if($($element).attr('status') == 'undo'){
    //         $ticketList =   new Array($($element).attr('ticket-number'));
    //     }else{
    //         $ticketList =   getSamaSelectedTickets('ticket-number');
    //     }
    //     changeTicketStatus($ticketList,null,$($element).attr('status'));
    // }

}

function changeSurveyStatusTo($element){

    var $isCloseAllowed = 0;
    var isClosedTicketExist= 0;
    if($($element).attr('status')=='Reserved'){
        if($($element).attr('status') == 'undo'){
            $surveyList =   new Array($($element).attr('status-id'));
        }else{
            $surveyList =   getSamaSelectedSurveys('status-id');
        }
        changeSurveyStatus($surveyList,null,$($element).attr('status'));

        // swal({
        //     title: 'Are you sure?',
        //     text: "You want to Close the selected ticket!",
        //     type: 'warning',
        //     showCancelButton: true,
        //     confirmButtonColor: '#3085d6',
        //     cancelButtonColor: '#d33',
        //     confirmButtonText: ' Yes ',
        //     cancelButtonText: ' No ',
        //     confirmButtonClass: 'btn btn-success',
        //     cancelButtonClass: 'btn btn-danger'
        // }).then(function(isConfirm) {
        //
        //     if (isConfirm) {
        //         if($($element).attr('status') == 'undo'){
        //             $ticketList =   new Array($($element).attr('ticket-number'));
        //         }else{
        //             $ticketList =   getSamaSelectedTickets('ticket-number');
        //         }
        //         changeTicketStatus($ticketList,null,$($element).attr('status'));
        //     }
        // });


        // if(confirm("Are you sure you want to close this ticket?")){
        //     $isCloseAllowed = 1;
        //     isClosedTicketExist = 1;
        // }
    } else {

        switch( ($($element).attr('status')).toUpperCase() ){
            case "TRANSFER" : case "RESOLVE" : case "UNDO" : case "APPROVE" : case "REJECT" :case "PENDING" : $isCloseAllowed = 1; break;
            default: $isCloseAllowed = 0;
        }
    }

    // console.log($isCloseAllowed);
    if( $($element).hasClass('disabled') ){

    }

    // if($isCloseAllowed){
    //
    //     // $("#sama_ticket_loader1").show();
    //     // $("#sama_ticket_result").addClass('hide');
    //     if($($element).attr('status') == 'undo'){
    //         $ticketList =   new Array($($element).attr('ticket-number'));
    //     }else{
    //         $ticketList =   getSamaSelectedTickets('ticket-number');
    //     }
    //     changeTicketStatus($ticketList,null,$($element).attr('status'));
    // }

}


function changeTicketStatus($ticketNumber,$imageId,$requestType){

    $('#sama_ticket_result').hide();
    $('#statusButtonSpan').hide();
    $('#sama_ticket_pagination_div').hide();
    $('#sama_ticket_loader').show();
 var  statusId = getSamaSelectedStatusId('status-id');
    for (i = 0; i < statusId.length; i++) {
        $requestData = {
            "requestFor": "change-sama-ticket-status",
            "status_id": statusId[i],
            'requested_status': $requestType
        };
        // console.log($requestData);
        $requestObject.initialize($requestData, null, null);

    }

    setTimeout(function(){
        location.reload();
    },30000);



    // if($requestData){
    //     $requestData['database_name']=$('#database_name').val();
    //     createTicketStatusRequest($requ
    // estData);
    // }
}

function changeSurveyStatus($ticketNumber,$imageId,$requestType){

    $('#sama_qaulity_survey_result').hide();
    $('#myTab').hide();
    $('#statusButtonSpan').hide();
    $('#sama_qaulity_pagination_div').hide();
    $('#sama_qaulity_loader').show();
    var  statusId = getSamaSelectedSurveyId('status-id');
    for (i = 0; i < statusId.length; i++) {
        $requestData = {
            "requestFor": "change-sama-survey-status",
            "status_id": statusId[i],
            'requested_status': $requestType
        };
        // console.log($requestData);
        $requestObject.initialize($requestData, null, null);

    }

    setTimeout(function(){
        location.reload();
    },30000);



    // if($requestData){
    //     $requestData['database_name']=$('#database_name').val();
    //     createTicketStatusRequest($requ
    // estData);
    // }
}


var getSamaUserReviewSearchDetail = function ($parentFilterDiv, $requestFor) {
    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if($(this).attr('id')=='usertype-list'){
                $(this).attr('value', 'ALL');
                }else if($(this).attr('name') == 'start_date' || $(this).attr('name')  == 'end_date' || $(this).attr('name')  == 'user_name'){
                $(this).attr('value', '');
            }
        });
    }

    $filterData = getSamaFilter($parentFilterDiv);

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=sama-user-review-detail&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-qa-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    $("#reviewExcelDownload a").attr('href', newSrc);
    $("#reviewExcelDownload a").attr('myval', newSrc);
    $("#samaReviewMailBtn").attr('myval', newSrcMail);


    // if (validateSamaFilter($filterData)) {
    var requestData = {
        'requestFor': $requestFor,
        'filterData': $filterData
    };
    $("#sama_review_panel").html($("#sama_ticket_loader").html());
    $requestObject.initialize(requestData, null, '#sama_review_panel');
    // }

};


function changeTicketStatusForSamaUser(changeStatusTo, status_id, that,ticketNumber) {
    //if( confirm("! This action will " + changeStatusTo + " the ticket. Please confirm to Proceed. ") ){
    //console.log(ticketNumber);
    //var ticketNumber =   $("#ticket_number").val();
    var $ticketStatus = '';
    if (changeStatusTo) {
        if (changeStatusTo == 'Re-open') {
            $ticketStatus = "Re-opened";
        } else if (changeStatusTo == 'Close') {
            $ticketStatus = "Closed";
        }
        $('#cboxClose').trigger( "click" );
        $requestData = {
            "requestFor": "change-sama-ticket-status",
            "status_id": status_id,
            'requested_status': changeStatusTo
        };
        // console.log($requestData);
        $requestObject.initialize($requestData, "ticketStatusChanged", null);
    }
}


var getSamaQualityList = function($parentFilterDiv, $requestFor,$isExcel){
    $('#quality_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=quality-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=quality-list&isExcel=true";

    newSrc = url + $filerDetail;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    if($isExcel == 'isExcel'){
        window.location.assign(newSrc);
    }

    $("#excelDownload").attr('href', newSrc);
    $("#reviewExcelDownload a").attr('myval', newSrc);
    $("#samaQaReviewMailBtn").attr('myval', newSrcMail);

    $requestDataPagination = {
        'requestFor'  : 'quality-list-pagination',
        'filterData' : $filterData
    };
    $("#sama_quality_list_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#quality_list_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#quality_list_result');
};

var resetQualityListFilters = function ($parentFilterDiv, $requestFor,userId) {


    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            //if($(this).attr('id')=='survey_type'){d
            //    //$(this).attr('value', '1');
            //    $(this).trigger('onchange');
            //}else{
            $(this).attr('value', '');
            //}
        });
    }


//    alert("here");
    getSamaQualityList($parentFilterDiv, $requestFor);
};

var gotoSamaQualityListPage = function($element,$type){

    $("#sama_quality_list_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader").html()+"</td></tr>");
    $filterData = getSamaFilter(".quality_detail_index_filters");

    manageNextprevButton('changeSamaQualityListPage','gotoSamaQualityListPage');
    $requestData = {
        'requestFor'  : 'quality-list',
        'filterData' : $filterData,
        // 'orderField'  : $("#ticketOrderField").val(),
        // 'orderType'   : $("#ticketOrderType").val(),
        'pageNumber'  : $("#gotoSamaQualityListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#quality_list_result');
};


var changeSamaQualityListPage = function($surveyPageChangeAction, $surveyPage){

    $selectedpageNumber = Number($('#gotoSamaQualityListPage option:selected').val());
    if($surveyPageChangeAction == 'next'){
        $selectedpageNumber = $selectedpageNumber + 10;

        $("#gotoSamaQualityListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaQualityListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }
    if($surveyPageChangeAction == 'prev'){
        $selectedpageNumber = $selectedpageNumber - 10;
        $("#gotoSamaQualityListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaQualityListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }

    gotoSamaQualityListPage('changeSamaPageNumber',$type);
};

var getSamaQualityListSearch = function($parentFilterDiv, $requestFor,$type){

    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=quality-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=quality-list&isExcel=true";

    newSrc = url + $filerDetail + '&admin_id='+$type;
    newSrcMail = urlMail + $filerDetail+'&isEmail=true&admin_id='+$type;
    $("#qaReviewExcelDownload a").attr('href', newSrc);
    $("#reviewExcelDownload a").attr('myval', newSrc);
    $("#samaQaReviewMailBtn").attr('myval', newSrcMail);



    $requestDataPagination = {
        'requestFor'  : 'quality-list-pagination',
        'filterData' : $filterData
    };
    $("#sama_quality_list_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#quality_list_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#quality_list_result');
};


var getSamaChangeSurvey = function($parentFilterDiv, $requestFor,$isExcel){
    $('#quality_search').val("");
    $filterData = getSamaFilter($parentFilterDiv);
    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/samarequest.php?requestFor=sama-changes-list&isExcel=true";
    var urlMail = baseUrl + baseBank + "/samarequest.php?requestFor=sama-change-list-mail&isExcel=true";

    newSrc = url + $filerDetail;
    // console.log(newSrc);
    newSrcMail = urlMail + $filerDetail+'&isEmail=true';
    if($isExcel == 'isExcel'){
        window.location.assign(newSrc);
    }

    $("#excelDownload").attr('href', newSrc);
    $("#reviewExcelDownload a").attr('myval', newSrc);
    $("#samaQaChangeMailBtn").attr('myval', newSrcMail);

    $requestDataPagination = {
        'requestFor'  : 'sama-changes-list-pagination',
        'filterData' : $filterData
    };
    $("#sama_change_list_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader1").html()+"</td></tr>");
    $requestObject.initialize($requestDataPagination,null, '#sama_change_list_pagination_div');

    $requestData = {
        'requestFor'  : $requestFor,
        'filterData' : $filterData
    };
    $requestObject.initialize($requestData, null, '#sama_change_list_result');
};

var resetSamaChangeSurveyFilters = function ($parentFilterDiv, $requestFor,userId) {


    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if($(this).attr('id')=='survey_type'){
               $(this).attr('value', '2');
                changeMetrics(this);
            }else{
            $(this).attr('value', '');
            }
        });
    }


//    alert("here");
    getSamaChangeSurvey($parentFilterDiv, $requestFor);
};

var gotoSamaChangeListPage = function($element,$type){

    $("#sama_change_list_result_body").html("<tr><td colspan='12'>"+$("#sama_qaulity_loader").html()+"</td></tr>");
    $filterData = getSamaFilter(".survey_change_index_filters");

    manageNextprevButton('changeSamaChangeListPage','gotoSamaChangeListPage');
    $requestData = {
        'requestFor'  : 'sama-changes-list',
        'filterData' : $filterData,
        // 'orderField'  : $("#ticketOrderField").val(),
        // 'orderType'   : $("#ticketOrderType").val(),
        'pageNumber'  : $("#gotoSamaChangeListPage option:selected").val()
    };
    $requestObject.initialize($requestData, null, '#sama_change_list_result');
};


var changeSamaChangeListPage = function($surveyPageChangeAction, $surveyPage){

    $selectedpageNumber = Number($('#gotoSamaChangeListPage option:selected').val());
    if($surveyPageChangeAction == 'next'){
        $selectedpageNumber = $selectedpageNumber + 10;

        $("#gotoSamaChangeListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaChangeListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }
    if($surveyPageChangeAction == 'prev'){
        $selectedpageNumber = $selectedpageNumber - 10;
        $("#gotoSamaChangeListPage").find('option:selected').removeAttr("selected");
        $('#gotoSamaChangeListPage option[value="'+$selectedpageNumber+'"]').attr("selected", "selected");
    }

    gotoSamaChangeListPage('changeSamaChangeListPage',$type);
};

var changeMetrics =function changeMetrics(element){

    $("#metrics").html("<option value>-- Select Metrics --</option>");
    //$requestObject.initialize($requestData, null,'#bank_name');

    $requestData = {
        'requestFor' : 'optionCreator',
        'val'        : element.value,
        'name'       : 'metrics'
    };
    $requestObject.initialize($requestData, null,'#metrics');
};


function updatePosValidateStatus(element,admin_id,status,survey_type){
    if(status == 1){
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=0;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=1;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }else{
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=1;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=0;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }



    var $requestData = {
        'requestFor': 'user-validate-survey-status',
        'admin_id': admin_id,
        'validate_survey_status': status,
        'survey_type' : survey_type
    };
    //console.log($requestData);
    $requestObject.initialize($requestData, null,null);

    var id='#user_validate_'+survey_type+'_'+admin_id+' '+'button';
// console.log(id);

    /* reverse locking status */
    $(id).eq(1).toggleClass('locked_inactive lock' +
        'ed_active btn-default btn-info');
    $(id).eq(0).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');

}

function updateMssSurveyValidateStatus(element,admin_id,status,survey_type){
    if(status == 1){
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=0;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=1;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }else{
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=1;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=0;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }



    var $requestData = {
        'requestFor': 'user-validate-survey-status',
        'admin_id': admin_id,
        'validate_survey_status': status,
        'survey_type' : survey_type
    };
    //console.log($requestData);
    $requestObject.initialize($requestData, null,null);

    var id='#user_validate_'+survey_type+'_'+admin_id+' '+'button';
 //console.log(id);

    /* reverse locking status */
    $(id).eq(1).toggleClass('locked_inactive lock' +
        'ed_active btn-default btn-info');
    $(id).eq(0).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');

}

function updateAtmSurveyValidateStatus(element,admin_id,status,survey_type){
    if(status == 1){
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=0;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=1;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }else{
        if($(element).children().hasClass('locked_active') || $(element).children().hasClass('unlocked_inactive')){
            status=1;
            /* code to do when unlocking */
            $('#switch_status').html('Switched on.');
        }else{
            status=0;
            /* code to do when locking */
            $('#switch_status').html('Switched off.');
        }
    }



    var $requestData = {
        'requestFor': 'user-validate-survey-status',
        'admin_id': admin_id,
        'validate_survey_status': status,
        'survey_type' : survey_type
    };
    //console.log($requestData);
    $requestObject.initialize($requestData, null,null);

    var id='#user_validate_'+survey_type+'_'+admin_id+' '+'button';
 //console.log(id);

    /* reverse locking status */
    $(id).eq(1).toggleClass('locked_inactive lock' +
        'ed_active btn-default btn-info');
    $(id).eq(0).toggleClass('unlocked_inactive unlocked_active btn-info btn-default');

}

var getSamaVendorInsightDetails = function($parentFilterDiv, $requestFor,$type){
    $filterData = getSamaFilter($parentFilterDiv);
    $requestData = {
        'requestFor'  : 'vendor-insight-detail',
        'filterData' : $filterData
    };
    $("#vendor_insight_result").html("<div class='col-lg-12'>"+$("#sama_ticket_loader1").html()+"</div>");
    $requestObject.initialize($requestData, null, '#vendor_insight_result');
};



var getSamaVendorTicketDetailsbyBank = function($parentFilterDiv, $requestFor,$type){
    $filterData = getSamaFilter($parentFilterDiv);
    $requestData = {
        'requestFor'  : 'vendor-ticket-detail-by-bank',
        'filterData' : $filterData
    };
    $("#vendor_ticket_result").html("<div class='col-lg-12'>"+$("#sama_ticket_loader1").html()+"</div>");
    $requestObject.initialize($requestData, null, '#vendor_ticket_result');

};

var TicketDateChange = function (that) {

    if ($(that).attr('name') == 'date_filter') {
        // console.log(that.value);
        if (that.value == 'updated_date') {
            $('#sama_start_date').val('');
            $('#sama_end_date').val('');
            $('#sama_start_date').hide();
            $('#sama_end_date').hide();
            $('#status_start_date').show();
            $('#status_end_date').show();
        }else{
            $('#status_start_date').val('');
            $('#status_end_date').val('');
            $('#sama_start_date').show();
            $('#sama_end_date').show();
            $('#status_start_date').hide();
            $('#status_end_date').hide();
        }
    }
};



var resetSamaVendorTicketDetails = function ($parentFilterDiv, $requestFor) {

    for (i in $elementList) {
        $($parentFilterDiv).find($elementList[i]).each(function () {
            if ($(this).attr('id') == 'survey_type') {
                //$(this).attr('value', '1');
                $(this).attr('value', $(this).val());
                $(this).trigger('onchange');
            } else if ($(this).attr('id') == 'bank_name') {
                $("#bank_name").attr('value','');
            } else {
                $(this).attr('value', '');
            }
        });
    }
}

function downloadExcelReport($parentFilterDiv, $requestFor, $vendor, $surveyId){
    var requestedData = JSON.parse($("#sama_RequestData_fordownload").val());
    // throw requestedData;
    requestedData.graphData = '';
    requestedData.requestFor = $requestFor;
    requestedData['vendor-name'] = $vendor;
    requestedData['survey-type'] = $surveyId;
    // throw requestedData;
    requestedData['excel-download'] = true;
    $("#download-excel-button").attr('href', baseUrl+''+baseBank+'/samarequest.php?' + $.param(requestedData));
    var target=$("#download-excel-button");
    window.open(target.attr('href'));
}