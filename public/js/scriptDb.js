function getBranchData($filterCondition){
    if($filterCondition.type == 'Bank'){
        return branchData().filter([
                $filterCondition
            ]).get();
    }else{
        return atmData().filter([
                $filterCondition
            ]).get();
    }
}

function getHeatMapData($filterCondition){
    if($filterCondition.type == 'Bank'){
        return heatMapBranch().filter([
            $filterCondition
        ]).get();
    }else{
        return heatMapAtm().filter([
            $filterCondition
        ]).get();
    }
    
}