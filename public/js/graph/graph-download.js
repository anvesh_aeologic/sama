
var currentGraphToEmail = null;
function downloadDashboardGraph(type, imageSource, imageName, folderName) 
{
    var requestTypeInput = document.createElement("input");
    requestTypeInput.setAttribute("name", 'requestFor');
    requestTypeInput.setAttribute("value", 'graph-download');
    requestTypeInput.setAttribute("type", "hidden");
    
    var typeInput = document.createElement("input");
    typeInput.setAttribute("name", 'type');
    typeInput.setAttribute("value", type);
    typeInput.setAttribute("type", "hidden");

    var dataInput = document.createElement("input");
    dataInput.setAttribute("name", 'img_source');
    dataInput.setAttribute("value", $(imageSource).attr('src'));
    dataInput.setAttribute("type", "hidden");

    var nameInput = document.createElement("input");
    nameInput.setAttribute("name", 'img_name');
    nameInput.setAttribute("value", imageName);

    if (folderName === undefined || !folderName) {
        folderName = '4c-dashboard';
    }

    var folderNameInput = document.createElement("input");
    folderNameInput.setAttribute("name", 'folder_name');
    folderNameInput.setAttribute("value", folderName);

    var myForm = document.createElement("form");
    myForm.method = 'post';
    myForm.action = baseUrl + ''+baseBank+'/request.php';
    myForm.appendChild(requestTypeInput);
    myForm.appendChild(typeInput);
    myForm.appendChild(dataInput);
    myForm.appendChild(nameInput);
    myForm.appendChild(folderNameInput);

    document.body.appendChild(myForm);
    myForm.submit();
    document.body.removeChild(myForm);
}

function emailDashboardGraph() 
{
    var graphMessage = currentGraphToEmail.attr('graph-message');
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }
    
    $requestData = {
                "requestFor" : 'graph-email',
                "graph-data" : {
                        "title"           : currentGraphToEmail.attr('graph-title'),
                        "message"         : graphMessage,
                        "img_source"      : $("#" + currentGraphToEmail.attr('graph-link')).attr('src'),
                        "img_name"        : currentGraphToEmail.attr('graph-image'),
                        "user_email"      : $("#graphEmailAddress").attr('value'),
                        "user_attachment" : $('#forwardinfoAttachment').attr('value'),
                        "cc_email"        : $('#forwardInfoCcEmail').attr('value'),
                        "score"           : currentGraphToEmail.attr('graph-score')  
                }
    };
    $requestObject.initialize($requestData,"graphDownloadConfirmation",null);
   
}

function emailMapData()
{
    var graphContent = document.getElementById("emailDiv").getAttribute("content");
    $requestData = {
        "requestFor" : 'map-email',
        "graph-data" : {
            "title"           : 'Al-Rajhi ATM Detail',
            "message"         : $("#graphMessage").attr('value'),
            "content"         : graphContent,
            "user_email"      : $("#graphEmailAddress").attr('value'),
            "cc_email"        : $('#forwardInfoCcEmail').attr('value')
        }
    };
    $requestObject.initialize($requestData,"graphDownloadConfirmation",null);

}

function emailDashboardQuestion()
{
    var graphMessage = currentGraphToEmail.attr('graph-message');
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }
    
    var sortOn = 'cdate_no';
    var orderBy = 'DESC';
    var sortingFlag = false;
    $("#ticketTabHeader").find('th').each(function(){
        if((!sortingFlag)&&($(this).attr('for')!=undefined)){
            if($(this).find('span img').attr('for')!='nutral'){
                sortOn  =  $(this).attr('for');  
                orderBy =  $(this).find('span img').attr('for');
                sortingFlag = true; 
               
            }
        }
    });
    
    var postedValue = {
        'question_id'        :   currentGraphToEmail.attr('question_id'),
        'department_name'    :   currentGraphToEmail.attr('department_name'),
        'department_id'      :   currentGraphToEmail.attr('department_id'),
        'branchProfileFlag'  :   currentGraphToEmail.attr('branchProfileFlag'),
        'branchCode'         :   currentGraphToEmail.attr('branchCode'),
        'branchType'         :   currentGraphToEmail.attr('branchType'),
        'gentsArea'          :   currentGraphToEmail.attr('gentsArea'),
        'ladiesArea'         :   currentGraphToEmail.attr('ladiesArea'),
        'zoneId'             :   currentGraphToEmail.attr('zoneId'),
        'areaId'             :   currentGraphToEmail.attr('areaId'),
        'sortBy'             :   currentGraphToEmail.attr('sortBy'),
        'orderBy'            :   currentGraphToEmail.attr('orderBy'),
        'istranstered'       :   currentGraphToEmail.attr('isdepartment-transfered'),
    }
    
   
    $requestData = {
        "requestFor" : 'metrices-ticket-list-email',
        "question-metrices-data" : {
            "type"               : 'question',
            "title"              : currentGraphToEmail.attr('graph-title'),
            "message"            : graphMessage,
            "email"              : $("#graphEmailAddress").attr('value'),
            'postedValue'        : postedValue,
            'requestForAreaWise' : currentGraphToEmail.attr('requestForAreaWise'),
            "user_attachment"    : $('#forwardinfoAttachment').attr('value'),
            "cc_email"           : $('#forwardInfoCcEmail').attr('value'),
            "image-type"         : currentGraphToEmail.attr('image-type')
        }
    }
    $requestObject.initialize($requestData,"questionMailConfirmation",null);
}

function emailDashboardVendor()
{
    var graphMessage = currentGraphToEmail.attr('graph-message');
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }

    var sortOn = 'cdate_no';
    var orderBy = 'DESC';
    var sortingFlag = false;
    $("#ticketTabHeader").find('th').each(function(){
        if((!sortingFlag)&&($(this).attr('for')!=undefined)){
            if($(this).find('span img').attr('for')!='nutral'){
                sortOn  =  $(this).attr('for');
                orderBy =  $(this).find('span img').attr('for');
                sortingFlag = true;

            }
        }
    });

    var postedValue = {
        'vendor_name'        :   currentGraphToEmail.attr('vendor_name'),
        'vendor_id'          :   currentGraphToEmail.attr('vendor_id'),
        'vendor_title'       :   currentGraphToEmail.attr('vendor_title'),
        'sortBy'             :   currentGraphToEmail.attr('sortBy'),
        'orderBy'            :   currentGraphToEmail.attr('orderBy'),
        'isVendorTickets'    :   true
    };


    $requestData = {
        "requestFor" : 'metrices-ticket-list-email',
        "question-metrices-data" : {
            "type"               : 'question',
            "title"              : currentGraphToEmail.attr('graph-title'),
            "message"            : graphMessage,
            "email"              : $("#graphEmailAddress").attr('value'),
            'postedValue'        : postedValue,
            'requestForAreaWise' : currentGraphToEmail.attr('requestForAreaWise'),
            "user_attachment"    : $('#forwardinfoAttachment').attr('value'),
            "cc_email"           : $('#forwardInfoCcEmail').attr('value')
        }
    };
    $requestObject.initialize($requestData,"questionMailConfirmation",null);
}


function emailIndividualBranchReport()
{  
    var graphMessage = currentGraphToEmail.attr('graph-message');
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }
    
    var branchCode    =   currentGraphToEmail.attr('branch-code');
    var branchType    =   currentGraphToEmail.attr('branch-type');
    var branchReport  =   currentGraphToEmail.attr('for-branch-report');
    var email         =   $("#graphEmailAddress").attr('value');
    var bank          =   baseBank;

    $requestData = {
        "requestFor"  : "send-branch-pdf-mail",
         "action"             : 'email',
         "bank"               :  bank,
         "type"               :  branchType,
         "code"               :  branchCode,
         "email"              :  email,
         "message"            :  graphMessage
    }
    $requestObject.initialize($requestData,"emailIndividualBranchReportCallBack",null); 
 }
 
 

function emailEVPReport()
{
  var graphMessage = currentGraphToEmail.attr('graph-message');
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }
  var email         =   $("#graphEmailAddress").attr('value');
 
  $requestData = {
        "requestFor"    : "email-evp-report",
        "type"          : 'evp-report',
        "reportName"    : 'EVP Report',
        "title"         : 'Please find the attachemetn for EVP Report.',
        "message"       : graphMessage,
        "email"         : $("#graphEmailAddress").attr('value')
    }
    $requestObject.initialize($requestData,"evpReportCallBack",null); 
       
}

function ughiStockTicketListMail()
{
  var graphMessage = currentGraphToEmail.attr('graph-message');
  var link = currentGraphToEmail.attr('myval');
    //console.log(link);
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }
    var email         =   $("#graphEmailAddress").attr('value');

  $requestData = {
        "requestFor"        : "stock-ticket-email-report",
        "type"              : 'stock-report',
        "reportName"        : 'Stock Ticket Report',
        "excelLink"         : link,
        "title"             : 'Please find the attachment for Stock Ticket Report.',
        "message"           : graphMessage,
        "email"             : $("#graphEmailAddress").attr('value'),
        "emailTo"           : $("#forwardInfoCcEmail").attr('value')
    }
    $requestObject.initialize($requestData,"evpReportCallBack",null);

}

function ughiNotListedTicketListMail()
{
    var graphMessage = currentGraphToEmail.attr('graph-message');
    var link = currentGraphToEmail.attr('myval');
    //console.log(link);
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }

    var email         =   $("#graphEmailAddress").attr('value');

    $requestData = {
        "requestFor"        : "notlisted-ticket-email-report",
        "type"              : 'notlisted-report',
        "reportName"        : 'Not Listed Ticket Report',
        "excelLink"         : link,
        "title"             : 'Please find the attachment for Not Listed Ticket Report.',
        "message"           : graphMessage,
        "email"             : $("#graphEmailAddress").attr('value'),
        "emailTo"           : $("#forwardInfoCcEmail").attr('value')
    }
    $requestObject.initialize($requestData,"evpReportCallBack",null);

}

function ughiShelveTicketListMail()
{
    var graphMessage = currentGraphToEmail.attr('graph-message');
    var link = currentGraphToEmail.attr('myval');
    //console.log(link);
    if ($("#graphMessage").attr('value')) {
        graphMessage = $("#graphMessage").attr('value');
    }

    var email         =   $("#graphEmailAddress").attr('value');

    $requestData = {
        "requestFor"        : "shelve-ticket-email-report",
        "type"              : 'shelve-report',
        "reportName"        : 'Shelve Ticket Report',
        "excelLink"         : link,
        "title"             : 'Please find the attachment for Shelve Ticket Report.',
        "message"           : graphMessage,
        "email"             : $("#graphEmailAddress").attr('value'),
        "emailTo"           : $("#forwardInfoCcEmail").attr('value')
    }
    $requestObject.initialize($requestData,"evpReportCallBack",null);

}


function evpReportCallBack()
{
    $("#emailSenderLoader").hide();
    $("#emailSuccessMsg").show();
    $("#emailSendSuccess").show();
    $("#graphEmailAddress").val('');
    $("#graphMessage").val('')
}
 
 function emailIndividualBranchReportCallBack()
 { 
    $("#emailSenderLoader").hide();
    $("#emailSuccessMsg").show();
    $("#emailSendSuccess").show();
    $("#graphEmailAddress").val('');
    $("#graphMessage").val('');
 setTimeout(function () { $("#closeEmailPopup").trigger("click");}, 3000);

 }

function emailPdfIndividualBranch(code,type,bank)
{
   $.post(
        baseUrl + 'pdf-creator/download-profile-pdf.php',
        {
            "action"    : 'email',
            "bank"      :  bank,
            "type"      :  type,
            "code"      :  code,
            "email"     :  $("#graphEmailAddress").val(),
            "message"   :  $("#graphMessage").val()
        },function(){
            $("#graphEmailLoader").hide();
            $("#graphEmailSuccess").show();
            $("#graphEmailSuccessMsg").show();
        });
}



function questionMailConfirmation($result)
{
    $("#emailSenderLoader").hide();
    $("#emailSuccessMsg").show();
    $("#emailSendSuccess").show();
    $("#graphEmailAddress").val('');
    $("#graphMessage").val('');
    $("#forwardInfoCcEmail").val('');
    $('#userAttachment').text('');     
    $('#attachment_name').html('');
    $('#size_error').val('');
    
}


function graphDownloadConfirmation($result)
{
    $("#emailSenderLoader").hide();
    $("#emailSuccessMsg").show();
    $("#emailSendSuccess").show();
    $("#graphEmailAddress").val('');
    $("#graphMessage").val('');
    $("#forwardInfoCcEmail").attr('value','');
    $('#userAttachment').text('');    
    $('#attachment_name').html('');
    $('#size_error').val('');
     setTimeout(function () { $("#closeEmailPopup").trigger("click");}, 3000);
}

function validateEmailAddress(emailContainerId,emailPostFunction)
{  
        var ccEmailContainer = 'forwardInfoCcEmail';
        var validEmail = false;
        if(checkEmail(emailContainerId))
        {   
            if($('#forwardInfoCcEmail').val() != ''){
                var ccEmailArray = [];
                ccEmailArray = $('#forwardInfoCcEmail').val().split(',');
                for(var i = 0; i<ccEmailArray.length;i++){
                    if(checkCcEmail(ccEmailArray[i]))
                    {   
                        var validEmail = true;
                    }  
                }
                
                if(validEmail){
                    $("#emailSuccessMsg").hide();
                    $('#emailSenderLoader').show();
                    if(emailPostFunction)
                        window[emailPostFunction]();
                }
                     
            } else{
                $("#emailSuccessMsg").hide();
                $('#emailSenderLoader').show();
                if(emailPostFunction)
                window[emailPostFunction]();
             }               
        }
}

function validateEmailAddressMap(emailContainerId,emailPostFunction)
{
    var ccEmailContainer = 'forwardInfoCcEmail';
    var validEmail = false;
    if(checkEmail(emailContainerId))
    {
        if($('#forwardInfoCcEmail').val() != ''){
            var ccEmailArray = [];
            ccEmailArray = $('#forwardInfoCcEmail').val().split(',');
            for(var i = 0; i<ccEmailArray.length;i++){
                if(checkCcEmail(ccEmailArray[i]))
                {
                    var validEmail = true;
                }
            }

            if(validEmail){
                $("#emailSuccessMsg").hide();
                $('#emailSenderLoader').show();
                if(emailPostFunction)
                    window[emailPostFunction]();
            }

        } else{
            $("#emailSuccessMsg").hide();
            $('#emailSenderLoader').show();
            if(emailPostFunction)
                window[emailPostFunction]();
        }
    }
}


function checkEmail(emailContainerId) 
{
    var email = document.getElementById(emailContainerId);
    var filter = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/;

    if (!filter.test(email.value)) {
        alert('Please provide a valid email address');
        email.focus;
        return false;
    }
    return true;
}
function checkCcEmail(email) 
{
    var filter = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/;

    if (!filter.test(email)) {
        alert('Please provide a valid email address');
        email.focus;
        return false;
    }
    return true;
}


function sendBranchReportMail() {
   
    $('#emailSenderLoader').show();
    $('#downloadParaForBranchReport').remove();
    
    email_id      = $('#graphEmailAddress').val();
    email_message = $('#graphMessage').val();
    
    formContentHtml  = '<input type="hidden" name="sendMail" value="true" id="mailParaForBranchReport">';
    formContentHtml += '<input type="hidden" name="email_id" value="'+email_id+'" id="branchEmailIdSend">';
    formContentHtml += '<input type="hidden" name="message" value="'+email_message+'" id="branchMessageSend">';
    
    if(baseBank == 'mobily'){
        $('#mobilyOutletProfileReportDiv #downloadBranchBasketReportForm').append(formContentHtml);
        $data = $('#mobilyOutletProfileReportDiv form#downloadBranchBasketReportForm').serialize();
    }else if(baseBank == "riyad" || baseBank == "aljazeera" || baseBank == "rbatm"|| baseBank == "rbatm_v3" || baseBank == "ncbatm"|| baseBank == "ncbatm_v3"){
         $('.branchProfileReportDiv #downloadBranchBasketReportForm').append(formContentHtml);
         $data = $('.branchProfileReportDiv form#downloadBranchBasketReportForm').serialize();
    }else{
        $('#downloadBranchBasketReportForm').append(formContentHtml);
        $data = $('form#downloadBranchBasketReportForm').serialize();
    }
    //console.log($data);
    url = bankAdminBaseUrl+'/Bank-Admin/Pdf/create-branch-pdf.php';

           
    $.post( url, $data, function(data) {

           // $("#closeEmailPopup").trigger("click");
        if(data) {
            $('#emailSenderLoader').hide();
            $('#emailSendSuccess').show();
            $('#emailSuccessMsg').show();
            $("#graphEmailAddress").val("");
            $("#graphMessage").val("");
           
        }
        
    });
    
    $('#mailParaForBranchReport').remove();
    $('#branchEmailIdSend').remove();
    $('#branchMessageSend').remove();
    formContentHtml = '<input type="hidden" name="download" value="true" id="downloadParaForBranchReport">';
    $('#downloadBranchBasketReportForm').append(formContentHtml);
    //$('#downloadBranchBasketReportForm').submit();
}


function ticketListMail(){
   findTicketFilter($("#ticket_selected_for").html());
   
   $requestData = {
        "requestFor"     :   "ticket-list-email",
        "email"          :   $("#graphEmailAddress").val(),
        "subject"        :   $("#graphMessage").val(),
        "ticket-filters" :   ticketFilterData.get()
        
   }
   $requestObject.initialize($requestData,"resetEmailBox",null);
}

function resetEmailBox($result){
    $('#emailSenderLoader').hide();
    $("#graphEmailAddress").val("");
    $("#graphMessage").val("");
    $("#emailSuccessMsg").show();
    
   
    
    $("#forwardInfoCcEmail").val('');
    $('#userAttachment').text('');  
    $('#attachment_name').html('');
    $('#size_error').val('');
    $('#userAttachment').html('');
      setTimeout(function () { $("#closeEmailPopup").trigger("click");}, 3000);
}


function emailAssetTickets(){
    $requestData = {
        "requestFor"        :    "asset-alert-notification",
        "email"             :    $("#graphEmailAddress").val(),
        "subject"           :    $("#graphMessage").val(),
        "ticket-number"     :    $("#assestNotificationEmail").attr('ticket-number'),
        "gap"               :    Number($("#assestNotificationEmail").attr("gap")),
        "user_attachment"   : $('#forwardinfoAttachment').attr('value'),
        "cc_email"          : $('#forwardInfoCcEmail').attr('value') 
    }
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function mailTicketList(){
    $requestData = {
        "requestFor"         :    "mail-ticket-list",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    : $('#forwardinfoAttachment').attr('value'),
        "cc_email"           : $('#forwardInfoCcEmail').attr('value')
    }
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function mailZonewiseScoreList(){
    $requestData = {
        "requestFor"         :    "mail-zone-and-mcp-report",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "reportFor"          :    "zone"
        
    }
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function mailMcpList(){
    $requestData = {
        "requestFor"         :    "mail-zone-and-mcp-report",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "reportFor"          :    "mcp",
        "shopType"           :    $("#mcpReportShopTypeSelect").val()
    }
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function mailSamaDrillSurveyList(){

    var requestedData = JSON.parse($("#sama_RequestData_fordownload").val());
    //requestedData['mail-content'] = true;
    //console.log(requestedData);
    /*console.log(requestedData.requestFor);
     console.log(requestedData.graphData);*/
    $requestData = {
        'requestFor'   : requestedData.requestFor,
        'filterData'   : requestedData.filterData,
        'graphData'    : requestedData.graphData,
        'toMail'       : true
    }
    //console.log($requestData);
    $requestObject.initialize($requestData,null,"#samaMailContent");

    $requestData = {
        "requestFor"         :    "mail-sama-drill-survey-list-report",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "request-type"       :    $("#emailSamaGraphDrillList").attr("request-type"),
        "toMail"       :    true
    }
    //console.log($requestData);
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function mailSamaTicketDetail(){
    $requestData = {
        "requestFor"         :    "mail-sama-ticket-detail-report",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "ticket_number"      :    $("#samaTicketInfomailButton").attr("ticket_number")
    }
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function mailAssetDrillData(){
    $requestData = {
        "requestFor"         :    "mail-asset-drill-data",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "asset-category"     :    $("#arbAssetEmailBtn").attr("asset-category"),
        "asset-status"       :    $("#arbAssetEmailBtn").attr("asset-status")
    }
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function emailQADashboardDetail() {

    $filterData = getSamaFilter('.quality_detail_index_filters');

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/content.php?type=quality-survey-list&isExcel=true";

    $newSrc = url + $filerDetail;

    $excelLink = encodeURI($newSrc);
    url = encodeURIComponent($excelLink);
    $requestData = {
        "qaEmail"            :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "comment"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "linkForExcel"       :    url
    }

    $link = $("#samaQaMailBtn").attr('myval');
    $link+='&qaEmail='+$requestData['qaEmail']+'&subject='+$requestData['subject']+'&cc_email='+$requestData['cc_email']+'&comment='+$requestData['comment']
        +'&linkForExcel='+$requestData['linkForExcel'];

    if($requestData['user_attachment'])
        $link+='&user_attachment='+$requestData['user_attachment'];

    $.ajax({
       url:$link,
       type:'get'
    });
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function emailSurveyTargetDetail() {

    $filterData = getSamaFilter('.quality_detail_index_filters');

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/content.php?type=target-survey-list&isExcel=true";

    $newSrc = url + $filerDetail;

    $excelLink = encodeURI($newSrc);
    url = encodeURIComponent($excelLink);
    $requestData = {
        "qaEmail"            :    $("#graphEmailAddress").val(),
        "subject"            :    'SAMA Survey Target Report',
        "comment"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "linkForExcel"       :    url
    }

    $link = baseUrl + baseBank +"/samarequest.php?requestFor=sama-target-list-mail&isExcel=true"+$filerDetail+"&isEmail=true";
    $link+='&qaEmail='+$requestData['qaEmail']+'&subject='+$requestData['subject']+'&cc_email='+$requestData['cc_email']+'&comment='+$requestData['comment']
        +'&linkForExcel='+$requestData['linkForExcel'];

    if($requestData['user_attachment'])
        $link+='&user_attachment='+$requestData['user_attachment'];

    $.ajax({
        url:$link,
        type:'get'
    });
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function emailQAReviewDetail() {

    $filterData = getSamaFilter('.sama_review_detail_index_filters');

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/content.php?type=sama-user-review-detail&isExcel=true";

    $newSrc = url + $filerDetail;

    $excelLink = encodeURI($newSrc);
    url = encodeURIComponent($excelLink);
    $requestData = {
        "qaEmail"            :    $("#graphEmailAddress").val(),
        "subject"            :   'SAMA QA Reviewed Survey Detail',
        "comment"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "linkForExcel"       :    url
    }

    $link = $("#samaReviewMailBtn").attr('myval');
    $link+='&qaEmail='+$requestData['qaEmail']+'&subject='+$requestData['subject']+'&cc_email='+$requestData['cc_email']+'&comment='+$requestData['comment']
        +'&linkForExcel='+$requestData['linkForExcel'];

    if($requestData['user_attachment'])
        $link+='&user_attachment='+$requestData['user_attachment'];

    $.ajax({
        url:$link,
        type:'get'
    });
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function emailQAReviewDetailResult() {

    $filterData = getSamaFilter('.quality_detail_index_filters');

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }
    var admin_id =$("#samaQaReviewMailBtn").attr("val");
    var url = baseUrl + baseBank + "/content.php?type=quality-review-list&isExcel=true&admin_id="+admin_id;

    $newSrc = url + $filerDetail;

    $excelLink = encodeURI($newSrc);
    url = encodeURIComponent($excelLink);
    $requestData = {
        "qaEmail"            :    $("#graphEmailAddress").val(),
        "subject"            :    'SAMA QA Reviewed Survey Detail',
        "comment"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "linkForExcel"       :    url
    }

    $link = $("#samaQaReviewMailBtn").attr('myval');
    $link+='&qaEmail='+$requestData['qaEmail']+'&subject='+$requestData['subject']+'&cc_email='+$requestData['cc_email']+'&comment='+$requestData['comment']
        +'&linkForExcel='+$requestData['linkForExcel'];

    if($requestData['user_attachment'])
        $link+='&user_attachment='+$requestData['user_attachment'];

    $.ajax({
        url:$link,
        type:'get'
    });
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function emailLoginDetailResult() {

    $filterData = getSamaFilter('.sama_login_history_detail_index_filters');

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank + "/content.php?type=sama-user-login-history&isExcel=true";

    $newSrc = url + $filerDetail;

    $excelLink = encodeURI($newSrc);
    url = encodeURIComponent($excelLink);
    $requestData = {
        "qaEmail"            :    $("#graphEmailAddress").val(),
        "subject"            :   'SAMA QA Login History',
        "comment"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "linkForExcel"       :    url
    }

    $link = $("#qaLoginHistoryEmail").attr('myval');
    $link+='&qaEmail='+$requestData['qaEmail']+'&subject='+$requestData['subject']+'&cc_email='+$requestData['cc_email']+'&comment='+$requestData['comment']
        +'&linkForExcel='+$requestData['linkForExcel'];

    if($requestData['user_attachment'])
        $link+='&user_attachment='+$requestData['user_attachment'];

    $.ajax({
        url:$link,
        type:'get'
    });
    $requestObject.initialize($requestData,"resetEmailBox",null);
}


function samaQaProfileDetailReport(){

    $requestData = {

        "qaProfileEmail"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
    }
    $link = $("#emailQaProfileReportEmail").attr('myval');
    $link+='&qaProfileEmail='+$requestData['qaProfileEmail']+'&subject='+$requestData['subject']+'&cc_email='+$requestData['cc_email'];
    if($requestData['user_attachment'])
        $link+='&user_attachment='+$requestData['user_attachment'];

    $.ajax({
        url:$link,
        type:'get'
    });

    $requestObject.initialize($requestData,"resetEmailBox",null);
 }

function mailSamaRejectedSurveyDrillSurveyList(){

    var requestedData = JSON.parse($("#sama_RequestData_fordownload").val());
    //requestedData['mail-content'] = true;
    //console.log(requestedData);
    /*console.log(requestedData.requestFor);
     console.log(requestedData.graphData);*/
    $requestData = {
        'requestFor'   : requestedData.requestFor,
        'filterData'   : requestedData.filterData,
        'graphData'    : requestedData.graphData,
        'toMail'       : true
    }
    //console.log($requestData);
    $requestObject.initialize($requestData,null,"#samaMailContent");

    $requestData = {
        "requestFor"         :    "mail-sama-rejected-drill-survey-list-report",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "request-type"       :    $("#emailSamaGraphDrillList").attr("request-type"),
        "toMail"             :    true

     }

    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function emailQAChangeDetailResult() {

    $filterData = getSamaFilter('.survey_change_index_filters');

    $filerDetail = '';

    for(i in $filterData){
        $filerDetail = $filerDetail + "&"+i+"="+$filterData[i];
    }

    var url = baseUrl + baseBank +  "/content.php?type=sama-changes-list&isExcel=true";

    $newSrc = url + $filerDetail;

    $excelLink = encodeURI($newSrc);
    url = encodeURIComponent($excelLink);
    $requestData = {
        "qaEmail"            :    $("#graphEmailAddress").val(),
        "subject"            :    'SAMA QA Validated(Edited) Survey Detail',
        "comment"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        "linkForExcel"       :    url
    }

    $link = $("#samaQaChangeMailBtn").attr('myval');
    $link+='&qaEmail='+$requestData['qaEmail']+'&subject='+$requestData['subject']+'&cc_email='+$requestData['cc_email']+'&comment='+$requestData['comment']
        +'&linkForExcel='+$requestData['linkForExcel'];

    if($requestData['user_attachment'])
        $link+='&user_attachment='+$requestData['user_attachment'];

    $.ajax({
        url:$link,
        type:'get'
    });
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

function mailSamaVendorDetailList(){
    var requestedData = JSON.parse($("#sama_RequestData_fordownload").val());
    $requestData = {
        'requestFor'   : requestedData.requestFor,
        'filterData'   : requestedData.filterData,
        'vendor-name' : requestedData['vendor-name'],
        'survey-type' : requestedData['survey-type'],
        'toMail'       : true
    }
   // console.log($requestData);
    $requestObject.initialize($requestData,null,null);

    $requestData = {
        "requestFor"         :    "mail-sama-vendor-ticket-detail-report",
        "email"              :    $("#graphEmailAddress").val(),
        "subject"            :    $("#graphMessage").val(),
        "user_attachment"    :    $('#forwardinfoAttachment').attr('value'),
        "cc_email"           :    $('#forwardInfoCcEmail').attr('value'),
        'vendor-name'        :    requestedData['vendor-name'],
        'filterData'         :    requestedData.filterData,
        'mailType'           :    requestedData.requestFor,
        "toMail"             :    true
    }
    $requestObject.initialize($requestData,"resetEmailBox",null);
}

