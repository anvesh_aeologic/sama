var bankCode,bankSurveyDate,bankLatitude,bankLongitude;
$(document).ready(function(){
       var where = "'Name'+=+'Riyad Bank'+AND+'BranchCode'+=+'"+branchCode+"'";
       var url = "https://www.google.com/fusiontables/api/query?sql=SELECT+BranchName,Location,Province,City+FROM+4033903+";
       if (where != null && where != undefined && where != "") 
            url += "WHERE+" + where+"+";
       url += "&jsonCallback=?";
       $foundFlag = false;
       $.getJSON(url,function(data){
            var fusionRows = data.table.rows;
            for(i in fusionRows){
                if(fusionRows[i][0]){
                    $address = '';
                    if(fusionRows[i][0]){
                        $address += fusionRows[i][0];
                    }
                    if(fusionRows[i][2]){
                        if($address){
                            $address += ", ";
                        }
                        $address += fusionRows[i][2];
                    }
                    if(fusionRows[i][3]){
                        if($address){
                            $address += ", ";
                        }
                        $address += fusionRows[i][2];
                    }
                    $("#branchName").html($address);
                    var latLng =(fusionRows[i][1]).split(',');
                    bankLatitude = latLng[0];
                    bankLongitude = latLng[1]; 
                    $foundFlag = true;
                    createMarker(new google.maps.LatLng(bankLatitude,bankLongitude));
                }
            }
            if($foundFlag){
                getHealthReport();
            }else{
                alert('Nothing found');
            }
       });
        function getHealthReport(){
            $.post('soapRequest.php',{'type':'data','branchCode':branchCode},function(result){
                result = JSON.parse(result);
                $("#healthReport").html(result.content);
                if(result.code){
                    bankCode = result.code;
                    bankSurveyDate = result.date;
                  //  if(result.latitude && result.longitude){
//                        marker.setPosition(new google.maps.LatLng(result.latitude,result.longitude));
//                    } else {
//                        marker.setPosition(new google.maps.LatLng(bankLatitude,bankLongitude));
//                    }
                    getGalleryImages();
                } else {
                     $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');
                    
                }
            })
        }
        function getGalleryImages(){
            $.post('soapRequest.php',{'type':'images','bankCode':bankCode,'bankSurveyDate':bankSurveyDate},function(result){
                    $("#galleryImages").html(result);
                    $('[rel="popupform"],[data-rel="popupform"]').popupform();
                    //gallery controlls container animation
                	if($('ul.gallery').attr('tooltip')=='true'){
                		$('ul.gallery li').hover(function(){
                		   	$('img',this).fadeToggle(1000);
                			$(this).find('.gallery-controls').remove();
                			$(this).append('<div class="well gallery-controls">'+
                								'<p><a href="#" class="gallery-edit btn"><i class="icon-edit"></i></a> <a href="#" class="gallery-delete btn"><i class="icon-eye-open"></i></a></p>'+
                							'</div>');
                			$(this).find('.gallery-controls').stop().animate({'margin-top':'-1'},400,'easeInQuint');
                		},function(){
                			$('img',this).fadeToggle(1000);
                			$(this).find('.gallery-controls').stop().animate({'margin-top':'-30'},200,'easeInQuint',function(){
                					$(this).remove();
                			});
                		});
                	}
                
                	//gallery delete
                	$('.thumbnails').on('click','.gallery-delete',function(e){
                		e.preventDefault();
                    });
                	//gallery edit
                	$('.thumbnails').on('click','.gallery-edit',function(e){
                		e.preventDefault();
                    });
                    
                    if(isTouchDevice){
                	   $('.openSample a').colorbox({inline:true,rel:'openSample a', transition:"elastic", maxWidth:"95%", maxHeight:"95%"},function(){var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');});
                    }else{
                       $('.thumbnails a').colorbox({inline:true,rel:'thumbnails a', transition:"elastic", maxWidth:"95%", maxHeight:"95%"},function(){var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');});
                    }
            });
        }
       
});