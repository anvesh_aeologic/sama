
function createMarkers(results){
  
  if(firstMapRequestFlag){
     firstMapRequestFlag = false;
     results     = results;
  } else{
     results     = JSON.parse(results);
  }
  
     if(results.resultType == 'atm')
        mapObject   =  mapObjectAtm;
     else
        mapObject   = mapObjectBranch;
        mapObject.clearMap();
    
     var mapdata     = results.mapdata;
    
    
     for(var counter in mapdata){    
        mapObject.createMarker(new google.maps.LatLng(mapdata[counter].latitude, mapdata[counter].longitude),results.resultType,mapdata[counter]);
     }
     
     if(baseBank != "ncbrebranding")
        mapObject.createCluster();
     mapObject.createList();
     
    if(results.resultType) {
        $("#loading_screen_"+(results.resultType).toLowerCase()).hide();
    }     
    $("#loading_screen_branch_smallshop").hide();
 }
 
 
 function createBranchReport() {
    mapObject.createBranchReportList();
 }
 
 function createHeatMap(results){
    results     = JSON.parse(results);
    if(results.resultType == 'atm')
        mapObject   =  mapObjectAtm;
    else
        mapObject   = mapObjectBranch;
   
    var heatmapdata = results.heatmapdata;
    if(!mapObject.heatmap){
        initializeHeatMap(heatmapdata,results.resultType);
        mapObject.heatmap.setMap(mapObject.map);
        $("#loading_screen_"+results.resultType).hide();
    }
 }
 
 function getDateTimeStamp(enddate){
    var split = enddate.split('-');
   
    // Month is zero-indexed so subtract one from the month inside the constructor
    var date = new Date(split[2], parseInt(split[1] - 1), split[0]); //Y M D
    return Date.parse(date);
 }
 
function sortBranchListByLastVisitDate($resultArray){
     $resultArray['mapdata'].sort(function(a,b) {
        $pre  = a.lastVisitDate;         
        $next = b.lastVisitDate;
        if(typeof($pre) == 'undefined' || $pre == '0' || $pre == '' || $pre == 'NA' || getDateTimeStamp($pre)  == 'NaN' ||  $pre  == 'N/A'){
            $pre = "1970-01-01";
        }
        if(typeof($next) == 'undefined' || $next == '0' || $next == '' || $next == 'NA'  || getDateTimeStamp($next) == 'NaN'  || $next == 'N/A'){
            $next = "1970-01-01";
        }
        
        return getDateTimeStamp($next) - getDateTimeStamp($pre); 
    });
    
    
    return $resultArray;
}

function sortBranchListByLastVisitDateAlrajhi($resultArray){
    $resultArray['mapdata'].sort(function(a,b) {
        $pre  = a.lastVisitDate;
        $next = b.lastVisitDate;
        $preL  = a.lastVisitDateL;
        $nextL = b.lastVisitDateL;
        if(typeof($pre) == 'undefined' || $pre == '0' || $pre == '' || $pre == 'NA' || getDateTimeStamp($pre)  == 'NaN' ||  $pre  == 'N/A'){
            $pre = "1970-01-01";
        }
        if(typeof($next) == 'undefined' || $next == '0' || $next == '' || $next == 'NA'  || getDateTimeStamp($next) == 'NaN'  || $next == 'N/A'){
            $next = "1970-01-01";
        }

        if(typeof($preL) == 'undefined' || $preL == '0' || $preL == '' || $preL == 'NA' || getDateTimeStamp($preL)  == 'NaN' ||  $preL  == 'N/A'){
            $preL = "1970-01-01";
        }
        if(typeof($nextL) == 'undefined' || $nextL == '0' || $nextL == '' || $nextL == 'NA'  || getDateTimeStamp($nextL) == 'NaN'  || $nextL == 'N/A'){
            $nextL = "1970-01-01";
        }

        if($pre == "1970-01-01" && $preL == "1970-01-01") {
            return 1;
        }
        if($pre == "1970-01-01" && $nextL == "1970-01-01") {
            return 0;
        }

        return getDateTimeStamp($next) - getDateTimeStamp($pre);
    });


    return $resultArray;
}