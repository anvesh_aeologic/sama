/**
 * ticket info for each metrice graph on click
 */
$elementList = ["input", "select", "textarea"];

var filterDataPockets = function ($parentFilterId) {
    var filterData = {};
    for (var i in $elementList) {
        $($parentFilterId).find($elementList[i]).each(function ($index, $value) {
            if ($(this).val()) {
                filterData[$(this).attr('name')] = $(this).val();
            }
        });
    }
    return filterData;
}


var resetAlrajhiAssetFilters = function($parentFilterId, $goButtonId){
    for(var i in $elementList){
        $($parentFilterId).find($elementList[i]).each(function($index, $value){
            if($(this).attr('type') != "hidden" && $(this).attr('name') != "branch_survey_type")
                $(this).attr('value', '');
        });
    }
    $($goButtonId).trigger("click");
}

var refreshData = function ($parentFilterId, $containerId, $requestFor) {
    
    var filterData = filterDataPockets($parentFilterId);
    $requestData = {
        'requestFor': $requestFor,
        'filter-data': filterData,
        'request-type': $requestFor
    }

    if($requestFor == "good-and-damaged-asset-count"){
        if($containerId)
            $($containerId).html("<tr><td colspan='5' style='text-align:center;'>"+$("#dashbordLoader").html()+"</td></tr>");
        $requestObject.initialize($requestData, null, $containerId);
    }else if($requestFor == "asset-gallery"){
        if($containerId)
            $($containerId).html($("#dashbordLoader").html());
         $requestObject.initialize($requestData, null, $containerId);
    }else{
        if($containerId)
            $($containerId).html($("#dashbordLoader").html());
        $requestObject.initialize($requestData, 'assetCallBackFunction', null);
    }

}

function assetCallBackFunction($resultData) {
    $resultData = JSON.parse($resultData);
    //console.log($resultData);
    if($resultData.data.length){
        if($resultData['callbackFunction'])
            window[$resultData['callbackFunction']]($resultData);
    }else{
       $("#"+$resultData.containerId).html("<div align='center' style='font-size:20px;font-weight:bold;color:#6383C4;height:133px;top:100px;position:relative;'> No data under selected filters </div>") ;
    }
    
}

/*
 * 
 */

var assetPieChart = function ($result) {
  
 assetPierChartObj = new Highcharts.Chart({
        chart: {
            renderTo:$result.containerId,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: $result.title
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{name: $result.seriesName, data: $result.data,
                point: {
                    events: {
                        click: function () {
                            $requestData = {
                                'requestFor': 'arb-asset-graph-drill-data',
                                'graph-data' : {
                                    'condition' : $result.condition,
                                    'selected-category' : (this.name) ? this.name : '',
                                    'region' : ($result.region) ? $result.region : "",
                                    'request-type' : $result.requestType
                                }
                            }
                            window[$result.drillFunction]($requestData);
                        }
                    }
                }
            }]
    });
    var chart_svg = assetPierChartObj.getSVG();
    var chart_canvas = document.getElementById($result.baseId+'_pie_chart_canvas'); 
    canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
    var chart_img = chart_canvas.toDataURL('image/png');
    $('#'+$result.baseId+'_pie_chart_div').html('<img id="'+$result.baseId+'_pie_chart_image" src="' + chart_img + '" />');
}
var currentPosition = 0;
var chartClickEvent = function($requestData){
    $popupObject.setWidth("1000px");
    $popupObject.setHeight("650px");
    $popupObject.open('#drillPopUp', 'fillArbPopup', $requestData);
    $popupObject.setWidth("800px");
    //currentPosition = $(window).scrollTop();
    //console.log(currentPosition);
    //$("html, body").animate({ scrollTop: 0 }, "slow");

}
function fillArbPopup($requestParam){
      $requestObject.initialize($requestParam,null, '#drillPopUp');
 }
/*
 * 
 */

var assetBarChart = function ($result) {
  assetBarChartObj = new Highcharts.Chart({
        chart: {
            renderTo : $result.containerId,
            type: 'column'
        },
        title: {
            text: $result.title
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: $result.categories,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Asset percentage'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b>({point.percentage:.0f}%)</td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                stacking: 'percent'
            },
            series :{
                point :{
                    events : {
                        click: function () {
                            $requestData = {
                                'requestFor': 'arb-asset-graph-drill-data',
                                'graph-data' : {
                                    'condition'         : $result.condition,
                                    'branchSurveyCondition' : (this.series.name == 'Damaged Furniture') ? (($result.branchSurveyCondition) ? $result.branchSurveyCondition : "") : "",
                                    'assetBranchCondition' :  (this.series.name == 'Good Furniture') ? (($result.assetBranchCondition) ? $result.assetBranchCondition : "") :"",
                                    'selected-category' : (this.category) ? this.category : '',
                                    'selected-category-type' : (this.series.name) ? this.series.name : '',
                                    'region' : ($result.region) ? $result.region : "",
                                    'request-type'      : $result.requestType
                                }
                            }
                            window[$result.drillFunction]($requestData);
                        }
                    }
                }
            }
        },
        series: $result.data
    });
    var chart_svg = assetBarChartObj.getSVG();
    var chart_canvas = document.getElementById($result.baseId+'_bar_chart_canvas'); 
    canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
    var chart_img = chart_canvas.toDataURL('image/png');
    $('#'+$result.baseId+'_bar_chart_div').html('<img id="'+$result.baseId+'_bar_chart_image" src="' + chart_img + '" />');
}

    
/**
 * @description To get records form Next and Previous Button pagination
 * @param button type (next, previous)) , request type
 * @return filtered html content
 */    
    
    function switchPageNumber(buttonType, requestType){
            if(buttonType == 'next'){
                 pageNumber = $("#getPageNumber").val();
                 pageNumber++;
               $("#switchPageNumberPrevButton").show(); 
            }else if(buttonType == 'prev'){
                 pageNumber = $("#getPageNumber").val();
                 pageNumber--;
            }else{
               pageNumber = $("#getPageNumber").val();
            } 
            $("#getPageNumber").val(pageNumber);
            
            if(pageNumber == 1){
               $("#switchPageNumberPrevButton").hide();
               $("#switchPageNumberNextButton").show();  
            }else if(pageNumber == $('#getPageNumber option:last-child').val()){
               $("#switchPageNumberNextButton").hide();
               $("#switchPageNumberPrevButton").show();
            }else{
               $("#switchPageNumberNextButton").show();
               $("#switchPageNumberPrevButton").show();
            }
       

           var $requestData = {
               'requestFor'  : 'arb-drill-data-pages',
               'page-number' : pageNumber,
               'request-type': requestType,
               'drill-condition': ($("#arb_asset_drill_result_container").attr('drill-condition')) ? $("#arb_asset_drill_result_container").attr('drill-condition') : '',
               'drill-callback' : ($("#arb_asset_drill_result_container").attr("drill-callback")) ? $("#arb_asset_drill_result_container").attr("drill-callback") : "",
               'region' : ($("#arb_asset_drill_result_container").attr("region")) ? $("#arb_asset_drill_result_container").attr("region") : "",
               'selected-category' : ($("#arb_asset_drill_result_container").attr("selected-category")) ? $("#arb_asset_drill_result_container").attr("selected-category") : "",
               'branchSurveyCondition' : ($("#arb_asset_drill_result_container").attr("branchSurveyCondition")) ? $("#arb_asset_drill_result_container").attr("branchSurveyCondition") : ""
           }
           $("#arb_asset_drill_result_container").html("<tr><td colspan='5' style='text-align:center;'>"+$("#dashbordLoader").html()+"</td></tr>");
           $requestObject.initialize($requestData, null, "#arb_asset_drill_result_container");
          
    }
