var testData={
        location:null,
        weight:null
};
$zoomChanged = false;
var heatmapData = [];
var heatmap;
var mapType = 'normal';
function getPopulationData(){
    $.post("model/heatmap.php",{'population':true},function(data){
        data = JSON.parse(data);
        var rows = data.result;
        var location;
        var counter = 0;
        for(var i in rows){
            if(rows[i]['location']){
                location = (rows[i]['location']).split(",");
                if((location.length==2)&&(Number(rows[i]['population']))){
                    counter++;
                    testData.location = new google.maps.LatLng(location[0],location[1]);
                    testData.weight = Number(rows[i]['population']);
                    heatmapData.push(testData);
                    testData={
                        location:null,
                        weight:null
                    };
                }
            }
        }
        heatmap = new google.maps.visualization.HeatmapLayer({
            data:heatmapData, 
            radius:85
        });
        
        google.maps.event.addListener(map, 'zoom_changed', function () {
            $zoomChanged = true;
        });
        google.maps.event.addListener(map, 'idle', function () {
            if($zoomChanged){
                heatmap.setOptions({radius:getNewRadius(this.getZoom())});
                $zoomChanged = false;
            }
        });
       
        
    });
    
	mapType = 'normal';   
} 

function getNewRadius(zoomLevel) {
            if(zoomLevel == 6){
                return 85;
            }
            if(zoomLevel>6)
                return 25*zoomLevel;
            else
                return 65;
} 

function insertHeatMapController(){
    return;
}

