$ticketFilters = null;
$(document).ready(function(){
    $("#admin").trigger('click');
    
    $("#reportTab a").click(function(e){
            e.preventDefault();
            $(this).tab('show');
    }); 

   
    $('.img_popup').click(function(){
        $popupObject.setWidth("750px");
        $popupObject.initialize(this,'#ticketDetailPopup','getTicketPopupDetail');
    });
  
    $('.slider-image').click(function(){
        $popupObject.setWidth("750px");
        $popupObject.initialize(this,'#ticketDetailPopup','getTicketPopupDetail');
    })
  
    $('#healthReportContainer .dept_popup').click(function(){
       //$popupObject.setWidth("650px");
        $popupObject.setWidth("750px");
        $popupObject.initialize(this,'#questionDetailPopup','getQuestionTickets');
    });
    
    /* $('#healthReportContainer .dept_popup').click(function(){
        $popupObject.setWidth("1000px");
        $popupObject.initialize(this,'#questionDetailPopup','getQuestionTickets');
        $popupObject.setWidth("800px");
    }); */
    
    $('#healthReport_atm .metrics_number').click(function(){ 
        $popupObject.setWidth("750px");
        $popupObject.initialize(this,'#atmQuestionDetailPopup','getATMQuestionTickets');
       // $popupObject.setWidth("800px");
    });
    
    /* MCP Popup For EVP Report*/
    $('#mcpMetricesDiv .metrics_number').click(function(){ 
        $popupObject.setWidth("750px");
        $popupObject.initialize(this,'#mcpQuestionDetailPopup','getMCPQuestionTickets');
        //$popupObject.setWidth("800px"); 
    });
    
    /*MCP POpup For Area Wise Report */
    $('#area_filter_result #mcp_content_div .areaWise-question-popup').click(function(){ 
        alert('clicked');
        /*$popupObject.setWidth("1000px");
          $popupObject.initialize(this,'#mcpQuestionDetailPopup','getMCPQuestionTickets');
          $popupObject.setWidth("800px");  */
    });
    
    
    initializeTicketPopup();
    
    $("#indexscoreKnobMe").knob({
        'min':0,
        'max':100
    });
    
    $(".dial").knob({
        'min':0
        ,'max':100
    }); 
});

String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};

String.prototype.ltrim=function(){return this.replace(/^\s+/,'');};

String.prototype.rtrim=function(){return this.replace(/\s+$/,'');};

String.prototype.fulltrim=function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};

function initializeTicketPopup(){
    $('.ticketViewButton').click(function(){
         $popupObject.setWidth("750px");
         $popupObject.initialize(this,'#ticketDetailPopup','getTicketPopupDetail');
         
    });

      $('.ATMticketViewButton').click(function(){
         $popupObject.setWidth("750px");
         $popupObject.initialize(this,'#ticketDetailPopupATM','getTicketPopupDetailATM');
         
    });
}

function getQuestionTickets($element){
    
    $postData = {
        'requestFor'    :  'question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup" 
        }   
    }
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}

function popupCallBackCustom(){
     $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({
              "placement":"bottom",
              delay: {
                 show: 400, 
                 hide: 200
              }
    }); 
}

function getQuestionTicketsContent($requestParam){
    $ticketFilters  =   $requestParam;
    $postData = {
        'requestFor'    :  'question-ticket',
        'requestParam'  :   $requestParam
        
    }
    $requestObject.initialize($postData,'popupCallBackCustom','#questionDetailPopup');
}


function getATMQuestionTickets($element){
    $postData = {
        'requestFor'    :  'atm-question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup" 
        }   
    }
    $requestObject.initialize($postData,'popupCallBackCustom','#atmQuestionDetailPopup');
}

function getMCPQuestionTickets($element){
    $postData = {
        'requestFor'    :  'mcp-question-ticket',
        'requestParam'  :   {
                'questionId'    :  $($element).attr('question-id'),
                'requestType'   :  "question-popup" 
        }   
    }
    $requestObject.initialize($postData,'popupCallBackCustom','#mcpQuestionDetailPopup');
}


function gotoFilterPageNumber(element){
    ticketQuestionFilterData.destroy();
    getTicketQuestionFilter();
    getCurrentPageNumber();
    ticketQuestionFilterData.set('pageNumber',Number($(element).val()));
    ticketQuestionFilterData.set('departmentName',$ticketFilters.departmentName);
    ticketQuestionFilterData.set('ticketColor',$ticketFilters.ticketColor);
    ticketQuestionFilterData.set('ticketStatus',$ticketFilters.ticketStatus);
    ticketQuestionFilterData.set('requestType',$ticketFilters.requestType);
    ticketQuestionFilterData.set('branchCode',$branchCode);
    ticketQuestionFilterData.set('branchType',$branchType);
  
    $requestData = {
        "requestFor"     :  "ticket-question-pagination",
        "ticket-filters" :  ticketQuestionFilterData.get()   
      }
    $requestObject.initialize($requestData,"fillTicketQuestionFilterResult",null);
}

function setQuestionPopup($deptName,$ticketColor,$ticketStatus,$requestType){
    $requestParam = {
        'departmentName'    :  $deptName,
        'ticketColor'       :  $ticketColor,
        'ticketStatus'      :  $ticketStatus,
        'requestType'       :  $requestType,
        'branchCode'        :   $branchCode,
        'branchType'        :   $branchType
    };
    $popupObject.setWidth("750px");
    $popupObject.open('#questionDetailPopup','getQuestionTicketsContent',$requestParam);
   // $popupObject.setWidth("800px");
}

function getTicketPopupDetail($element){
    $ticketPanelPageNumber  =   0;
    $ticketPanelPageValue   =   0;
    if($("#gotoPageNumber")){
        $ticketPanelPageNumber =  $("#gotoPageNumber option:selected").text();
        $ticketPanelPageValue  =  $("#gotoPageNumber").val();
    }
    $("#ticketDetailPopup").html($("#popupLoader").html());
    $postData = {
        'requestFor'    :   'ticket-detail',
        'ticketNumber'  :   $($element).attr('ticket-number'),
        'process'       :   "first"
    }
    $requestObject.initialize($postData,"initializePopupTab",'#ticketDetailPopup');
}

function getTicketPopupDetailATM($element){
    $ticketPanelPageNumber  =   0;
    $ticketPanelPageValue   =   0;
    if($("#gotoPageNumber")){
        $ticketPanelPageNumber =  $("#gotoPageNumber option:selected").text();
        $ticketPanelPageValue  =  $("#gotoPageNumber").val();
    }
    $("#ticketDetailPopupATM").html($("#popupLoader").html());
    $postData = {
        'requestFor'    :   'ticket-detail-atm',
        'ticketNumber'  :   $($element).attr('ticket-number'),
        'process'       :   "first"
    }
    $requestObject.initialize($postData,"initializePopupTab",'#ticketDetailPopupATM');
}


function getTicketPopupContent($ticketNumber){
     $("#ticketDetailPopup").html($("#popupLoader").html());
    $postData = {
        'requestFor'    :  'ticket-detail',
        'ticketNumber'  :  $ticketNumber
    }
    $requestObject.initialize($postData,"clearAndFillPopup",null);
}

function clearAndFillPopup($content){
    $("#ticketDetailPopup").html($content);
    initializePopupTab();
}

function tabSwitchCreator($tabElement){
    $($tabElement).click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });
}

function initializePopupTab() {
    $("#popupActionTab li a").click(function (e) {
        $("#ticketDetailPopupATM").html("");
        e.preventDefault();
        $(this).tab('show');
        if($(this).attr('href') == '#ticket_comment_containor'){
            initializeCommentScrollbar();
        }
    });

     $("#popupActionTabATM li a").click(function (e) {
        $("#ticketDetailPopup").html("");
        e.preventDefault();
        $(this).tab('show');
        if($(this).attr('href') == '#ticket_comment_containor'){
            initializeCommentScrollbar();
        }
    });
    
}

function toTitleCase(str) { 
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function branchProfileDisplayName($branchName, $region, $city){
    var $branchName = $branchName.toLowerCase();
    var $region = $region.toLowerCase();
    var $city = $city.toLowerCase();

    var $branchProfileDisplayName = $branchName;

    if (!($branchProfileDisplayName.indexOf($region) >= 0)) {
        $branchProfileDisplayName += ', ' + $region;
    }

    if (!($branchProfileDisplayName.indexOf($city) >= 0)) {
        $branchProfileDisplayName += ', ' + $city;
    }

    return toTitleCase($branchProfileDisplayName);
}


function branchTypeToDisplay(branchType){
    if(branchType === 'gents'){
        return 'Men';
    } else if(branchType === 'gents and ladies'){
        return 'Men and Ladies';
    }
    return branchType;
}

function sendToUrl($link){
    alert($link);
}

/**
 * function::getAreaByZone()
 * 
 * @param string thisObj
 * @return lsit of area wise based on zone id
 */
function getAreaByZone(thisObj){
     $zone_id = $(thisObj).attr('value');
     if($zone_id == '') {
            $('#area_name_select').html('<option value="">-- Area Name --</option>');
        } else {
           $('#area_name_select').html('<option value="">--Loading_Data--</option>'); 
           $('#area_select_loader').show();
           $requestParam = {
                'zone_id': $zone_id
                
           };
           $postData = {
                'requestFor'    :  'get-area-by-zone',
                'requestParam'  :   $requestParam
           }
          $requestObject.initialize($postData,null,'#area_name_select');
          //$('#area_select_loader').show();
          setTimeout(function(){$('#area_select_loader').hide();},1000);
     }
}

 function getFilteredAreaResult(thisObj) {
     
     
        $area_id   = $('#area_name_select').val();
        $area_name = $("#area_name_select :selected").text();
        $zone_id   = $('#zone_name_select').val();
        $zone_name = $("#zone_name_select :selected").text();
        $page      = $(thisObj).attr('value');
        $orderBy   = $(thisObj).attr('orderBy');
        $order     = $(thisObj).attr('order');
        
        $('#area_wise_result_table_body').html('<tr><td colspan="5" style="padding: 20px 0px; text-align: center">'+
            '<b>Loading Data</b> <img title="Loading Data" src="../public/img/ajax-loaders/bsfmap-loader1.gif">'+
            '</td></tr>'
        );
        
        $('#mcp_content_div').html('<div style="padding: 20px 0px; text-align: center">'+
            '<b>Loading Data</b> <img title="Loading Data" src="../public/img/ajax-loaders/bsfmap-loader1.gif">'+
            '</div>'
        );
        
        if($zone_id == '') {
            $('#area_name_mail_report_button').attr('area-name', '');   
            $('#area_name_mail_report_button').attr('zone-name', '');   
            $('#area_name_mail_report_button').attr('area-id', '');   
            $('#area_name_mail_report_button').attr('zone-id', '');   
            $('#area_name_mail_report_button').attr('graph-title', '');  
        } else {
            if($area_id == '') {
                $('#area_name_mail_report_button').attr('area-name', '');   
                $('#area_name_mail_report_button').attr('area-id', '');   
                $('#area_name_mail_report_button').attr('graph-title', 'Branch Score for '+$zone_name+ ' zone');  
                
            } else {
                $('#area_name_mail_report_button').attr('area-name', $area_name);   
                $('#area_name_mail_report_button').attr('area-id', $area_id);
                $('#area_name_mail_report_button').attr('graph-title', 'Branch Score for '+$area_name+ ' area');  
            }
            
            $('#area_name_mail_report_button').attr('zone-name', $zone_name);   
            $('#area_name_mail_report_button').attr('zone-id', $zone_id);   
           
            $('#area_name_pdf_report_button').attr('href', '../Pdf/create-area-pdf.php?bank_name=riyad&area_name='+$area_name+'&zone_name='+$zone_name+'&area_id='+$area_id+'&zone_id='+$zone_id);  
        }
        
         $requestParam = {
                'area_id'  : $area_id,
                'page'     : $page,
                'orderBy'  : $orderBy,
                'order'    : $order,
                'zone_id'  : $zone_id,
                'area_name': $area_name,
                'zone_name': $zone_name
           };
        
         $postData = {
                'requestFor'    :  'get-area-wise-result',
                'requestParam'  :   $requestParam
          }
         $requestObject.initialize($postData,null,'#area_filter_result');
  }
 
 function getFilteredAreaResultBranches(){
     
     
        $area_id   = $('#area_name_select_branches').val();
        $area_name = $("#area_name_select_branches :selected").text();
        $city =$('#mobilyBranchCity').val();
        $outletType =$('#outletTypeSelector').val();
        $toDate =$('#mcpFrom').val();
        $fromDate =$('#mcpTo').val();
        
        
         $requestParam = {
                'area_id'  : $area_id,
                'area_name': $area_name,
                'city'     :$city,
                'outletType':$outletType,
                'toDate':$toDate,
                'fromDate':$fromDate
             
           };
        
         $postData = {
                'requestFor'    :  'get-area-wise-result-branches',
                'requestParam'  :   $requestParam
          };
         $requestObject.initialize($postData,null,'#branch_filter_result');
  }

 

 function getAreaByZone123(thisObj){
     $zone_id = $(thisObj).attr('value');
     if($zone_id == '') {
            $('#area_name_select').html('<option value="">--Area Name--</option>');
        } else {
           $('#area_name_select').html('<option value="">-- Loading_Data --</option>'); 
           $('#area_select_loader').show();
           $requestParam = {
                'zone_id': $zone_id
                
           };
           $postData = {
                'requestFor'    :  'get-area-by-zone',
                'requestParam'  :   $requestParam
           }
          $requestObject.initialize($postData,null,'#area_name_select');
          //$('#area_select_loader').show();
          setTimeout(function(){$('#area_select_loader').hide();},1000);
     }
}

 function getAreaWiseScore($month,$month_name,$bankName){
         //  $('#area_select_loader').show();
           $requestParam = {
                'month': $month
           };
           $postData = {
                'requestFor'    :  'get-area-wise-score',
                'requestParam'  :   $requestParam
           }
           
           $requestObject.initialize($postData,"areaWiseScoreGraph",null);
 }
 

function areaWiseScoreGraph($content){
  $content = $.parseJSON($content);
    if($content.data_found) {
        popupCallBackCustom();
        $('#area_score_label').html($content.areaScoreLabel);
        $('#area_wise_score_excel_link').html($content.excel_download);
        makeAreaGraph($content.areaScoreRowset,$month_name);
    }  else {
        $('#area_graph').html('<div align="center" style="margin-left:20px;">'+
            '<div align="center" style="font-weight: bold;padding-bottom:10px;padding-top: 130px;font-size: 36px;color:red;">Nothing Found</div>'+
            '</div>'
        );
    }  
 }
 
 function reloadPopUpATM(){
    //alert('clicked');
 }
 
 /* function downloadProfilePdf(code,type,bank,action) {
  // alert('clicked');
   $.post('../helper/hasSurvey.php',{
        'code'  :   code,
        'type'  :   type,
        'bank'  :   bank
    },function(result){
        result = JSON.parse(result);
        if(!result.success){
            alert("No survey found for Branch - "+code+","+type);
        }else{
            if(action == 'download')
                downloadPdf(code,type,bank);
            else
                emailPdf(code,type,bank);
        }
    }) 
} */

 
function downloadProfilePdf(code,type,bank,action) {
     $requestParam = {
        'code'  :   code,
        'type'  :   type,
        'bank'  :   bank,
        'action':   action
     };
     $postData = {
            'requestFor'    :  'get-indivdual-branch-pdf-report',
            'requestParam'  :   $requestParam
     }
     $requestObject.initialize($postData,"mailOrDownload",null);
 }
 

function mailOrDownload($content){
  $content = $.parseJSON($content);
  if(!$content['result']){
            alert("No survey found for Branch - "+$content['code']+","+$content['type']);
  } else {  
    if($content['action'] =='download'){
        downloadPdf($content['code'],$content['type'],$content['bank'],$content['action']);
    } else {
        emailPdf($content['code'],$content['type'],$content['bank'],$content['bank'],$content['action']); 
    }
   }
 }
 
 function downloadPdf(code,type,bank,action){
    /*$requestParam = {
        'code'  : code,
        'type'  : type,
        'bank'  : bank,
        'action': action
    } 
    $postData = {
        'requestFor'   :'download-branch-individual-pdf',
        'requestParam' : $requestParam
    } */
  //  return false; 
 
   //var upDatedbaseUrl = location.protocol + '//' + location.host;
   var upDatedbaseUrl = bankAdminBaseUrl;
   
   var actionInput = document.createElement("input");
    actionInput.setAttribute("name", 'action');
    actionInput.setAttribute("value", 'download');
    actionInput.setAttribute("type", "hidden");
    
    var codeInput = document.createElement("input");
    codeInput.setAttribute("name", 'code');
    codeInput.setAttribute("value", code);
    codeInput.setAttribute("type", "hidden");
    
    var typeInput = document.createElement("input");
    typeInput.setAttribute("name", 'type');
    typeInput.setAttribute("value", type);
    typeInput.setAttribute("type", "hidden");
    
    var bankInput = document.createElement("input");
    bankInput.setAttribute("name", 'bank');
    bankInput.setAttribute("value", bank);
    bankInput.setAttribute("type", "hidden");


    var myForm = document.createElement("form");
    myForm.method = 'post';
    myForm.action = upDatedbaseUrl + '/Bank-Admin/pdf-creator/download-profile-pdf.php';
    myForm.appendChild(actionInput);
    myForm.appendChild(codeInput);
    myForm.appendChild(typeInput);
    myForm.appendChild(bankInput);
    
    document.body.appendChild(myForm);
    myForm.submit();
    document.body.removeChild(myForm);
 
 }
 
 function emailPdf(code,type,bank,action){
    $requestParam = {
        'code'   :code,
        'type'   :type,
        'bank'   :bank,
        'action' :action
    }
    $postData = {
        'requestFor'  :'email-pdf-report',
        'requestParam': $requestParam
    }
    return false;
 }






