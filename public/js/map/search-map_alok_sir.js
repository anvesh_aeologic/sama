var map;
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
var testData  = {
                location:null,
                weight:null
              };

function MapCreator(options) {
    this.cluster = null;
    this.markerArray = new Array();
    this.map    =   null;
    this.centerPoint    =   null;
    this.clusterOptions = {  styles: styles[0] };
    this.mapType = options.type;
    this.mapContainer   =  options.mapContainer;  
    this.markerInfo = new Array();
    this.heatmap = null;
    this.infowindowObj =  new InfoBubble({maxWidth: 500,borderColor: '#CCCCCC'});
    this.createMap = function(){
        var mapOptions = { 
                center: new google.maps.LatLng(22.268763552489748,47.87841796875),
                zoom: 5, 
                mapTypeId: google.maps.MapTypeId.ROADMAP 
        };
        
        this.map = new google.maps.Map(document.getElementById(this.mapContainer), mapOptions);
        changeIsrailLayer(this.map.getZoom(),this.map);
        
    }
    
    this.createMarker = function(latLng,markerType,otherData){
        var me = this;
        var marker = new google.maps.Marker({ 
                position    :   latLng, 
                icon        :   this.createIcon(markerType),
                map         :   this.map
        }); 
        
        me.markerArray.push(marker);
        me.markerInfo.push(new Array(
                    otherData,
                    me.markerArray.length-1
                )
        );
        
        google.maps.event.addListener(marker, 'click', function (event) {
            me.openInfowindow(this,otherData);
        });
    }
    
    this.openInfowindow = function(marker,data){
        this.infowindowObj.setContent(this.createContent(data));
        this.infowindowObj.open(this.map,marker);
    }
    
    this.createContent = function(data){
         var genderIndicator;
         var gentsExist = false;
         var finalBranchName;
        if(data.type == 'ATM'){
           html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                        "<div style='height:0px;width:220px;'></div>" + 
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
           return html; 
         } else {
           html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                        "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" + 
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Branch Type :</span>&nbsp;<span class='title'>" + this.getBranchType(data) + "</span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
           return html;    
         }    
        
    }
    
    this.getBranchType = function(data){
       var html = '';
       if(data.type == 'ATM'){
            html += "<div style='height:40px;width:100%'>";
            html += "<div class='title' align='center'>ATM Code&nbsp;:&nbsp;" +data.branchCode+ "&nbsp;</div>";        
            return html;
       } else {
        switch(data.branchType.toUpperCase()){
             case 'LADIES'  : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title="Ladies Branch">Ladies</span>&nbsp;<img title="Ladies Branch" src="'+baseUrl+'public/img/ladies.jpg"></a>'; break;
             case 'GENTS'   : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="Men Branch">Gents</span>&nbsp;<img title="Men Branch" src="'+baseUrl+'public/img/men.jpg"></a>'; break;
             default        : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="Men Branch">Gents</span>&nbsp;<img title="Men Branch" src="'+baseUrl+'public/img/men.jpg"></a>';
                              html += '&nbsp;&nbsp;<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title="Ladies Branch">Ladies</span>&nbsp;<img title="Ladies Branch" src="'+baseUrl+'public/img/ladies.jpg"></a>';  
          } 
         return html;  
       } 
    }
    
    this.validateImage = function($img){
        if($original_bank_name == 'Demo Bank') {
            var branchImg = baseUrl + 'public/img/x_image.png';
            
            return branchImg;
        }
        var branchImg = baseUrl + 'public/img/preview.png';
        if ($img !== '' || $img != undefined)
            branchImg = $img;
        return branchImg;
    }
    
    this.clearMarker = function(){
        if(this.markerArray){
            for(var markerArrayIndex in this.markerArray){
                this.markerArray[markerArrayIndex].setMap(null);
            }
        }
        this.markerArray = new Array();
    }
    
    this.createIcon = function(type){
        var imagePosition = (type == 'atm') ? 1 : 0;
        return new google.maps.MarkerImage(markerIcons[imagePosition].image, null, null, null, new google.maps.Size(markerIcons[imagePosition].width, markerIcons[imagePosition].height));
    }
    
    this.createCluster = function(){
        if (this.cluster == undefined) { 
            this.cluster = new MarkerClusterer(this.map, this.markerArray,this.clusterOptions);
        } else { 
            this.cluster.addMarkers(this.markerArray, false); 
        }
    }
    
    this.clearCluster = function(){
        if((this.cluster != undefined)&&(this.cluster)) {
            this.cluster.clearMarkers();
            this.cluster = null;
        }
    }
    
    this.getSpace = function($data){
        if(Number($data) <= 9){
            return "&nbsp;&nbsp;&nbsp;"
        }
        if(Number($data) <= 99){
            return "&nbsp;&nbsp;"
        }
        return "&nbsp;"
       
    }
    
    this.createList = function(type){
        $('#scrollbar_'+this.mapType+' .scrollbar').hide();
        $content = "<ul class='dashboard-sublist'>";
        $contentATM = "<ul class='dashboard-sublist'>";
        var markerInfo = this.markerInfo;
        var liCount = 0;
        var liCountATM = 0;
          for(var markerInfoIndex in markerInfo){
            liCount++;
            if(!markerInfo[markerInfoIndex][0]['branchName']){
                markerInfo[markerInfoIndex][0]['branchName'] = ""; 
            }
           
           if(markerInfo[markerInfoIndex][0]['type'] == 'ATM'){
                liCountATM++;
                $contentATM += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ATM" target="_blank"><span style=" color:#4183C4;width:73%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                 '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+    
                               '</li>';
           } else {
           
            switch((markerInfo[markerInfoIndex][0]['branchType']).toUpperCase()){
                    case 'LADIES':
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:'+$listWidth+';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                       '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="'+baseUrl+'public/img/ladies.jpg"  title="Ladies Branch"/></a>'+    
                                    '</li>';
                                     break;
                     case 'GENTS':
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:'+$listWidth+';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                      '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><img src="'+baseUrl+'public/img/men.jpg"   title="Men Branch"/></a>'+    
                                    '</li>';
                                      break;
                     case 'GENTS AND LADIES':
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:'+$listWidth+';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                      '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><img src="'+baseUrl+'public/img/men.jpg"   title="Men Branch"/></a>&nbsp;<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="'+baseUrl+'public/img/ladies.jpg"  title="Ladies Branch"/></a>'+    
                                    '</li>';
                                      break;
                     default:
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                      '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+    
                                    '</li>';
                                      break;
                } 
             }
            
          } 
          
          if(!liCount){
            $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
             if(this.mapType=='atm'){
                 $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
             }
          } 

          $content += "</ul>"; 
          $contentATM += "</ul>"; 
          $("#result_counter_"+this.mapType).html((this.markerInfo).length);
          $("#overview_"+this.mapType).html($content);
          
          if(this.mapType=='atm'){
            $("#overview_"+this.mapType).html($contentATM);
          }
          
          initializeTinyscrollbar(this.mapType);
         if(this.mapType == 'atm'){
             if(liCountATM>18){
               $('#scrollbar_atm .scrollbar').show();
             }

         } else {
             if(liCount>18){
               $('#scrollbar_'+this.mapType+' .scrollbar').show();
             }

         }
   }

   function calculateChar(mainStr){
       if((String(mainStr).split("")).length==1){
         return '&nbsp;&nbsp;&nbsp;&nbsp;';
        }else{
       if((String(mainStr).split("")).length==2)
         return '&nbsp;&nbsp;';
       else
         return '&nbsp;';
      } 
   }
    
   var firstTime = 0; 
   var flagAgent = false; 
   this.createBranchReportList = function(type){
        if(firstTime == 0){
        var markerInfo = this.markerInfo;
        if(markerInfo){
        var addressBranch;   
        $reportContent = '<table id="branch-list-box-table" style="width:100%;" class="table table-bordered"><tr>';  
        $rowCounter = 1;  
        var markerInfo = this.markerInfo;
          for(var markerInfoIndex in markerInfo){
             //alert(markerInfo.toSource());
            if(markerInfo[markerInfoIndex][0]['name'] == 'Al-Rajhi Bank'){
              markerInfo[markerInfoIndex][0]['name'] = 'arb';
            }
            
            if(markerInfo[markerInfoIndex][0]['name'] == 'Riyad Bank'){
              markerInfo[markerInfoIndex][0]['name'] = 'riyad';
            }
            
            
            if(!markerInfo[markerInfoIndex][0]['branchName']){
                markerInfo[markerInfoIndex][0]['branchName'] = ""; 
            }
            
             if($rowCounter>4){
                $rowCounter = 1;
                $reportContent += '</tr><tr>';
             }
            switch(markerInfo[markerInfoIndex][0]['branchType']){
                    case 'Ladies':
                            $rowCounter++;
                            flagAgent = true;
                           
                           /* $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                              '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.jpg"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\'riyad\')"></span></td>';
                              break; */
                            
                            $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                            '</td><td><i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode']+ '&type=ladies" target="_blank">'+markerInfo[markerInfoIndex][0]['branchCode']+'</a></i>'+markerInfo[markerInfoIndex][0]['branchCode']+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.jpg"  title="Ladies Branch"/></a><span class="icon32 icon-blue  icon-pdf" title="Download Profile pdf"  style="cursor:pointer;background-image: url(../public/img/opa-icons-blue32.png);" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" related-box="myModal" class="btn-setting icon32 icon-blue icon-envelope-closed" title="Email" branch-code="'+markerInfo[markerInfoIndex][0]['branchCode']+'" branch-type="Ladies" for-branch-report="true" style="cursor:pointer"></span></td>';
                            break;
                        
                    case 'Gents':
                            $rowCounter++; 
                            flagAgent = true;
                            /* $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                              '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.jpg"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\'riyad\')"></span></td>';
                              break;*/
                            $branchCode = markerInfo[markerInfoIndex][0]['branchCode'];
                            $reportContent += '<td class="branchCode-'+$branchCode+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                            '</td><td><i><a style="text-decoration: underline;" href="index.php?code=' + $branchCode + '&type=Men" target="_blank">'+$branchCode+'</a></i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=Men" target="_blank"><img src="../public/img/men.jpg"  title="Men Branch"/></a><span class="icon32 icon-blue  icon-pdf" title="Download Profile pdf"  style="cursor:pointer;background-image: url(../public/img/opa-icons-blue32.png);" onclick="downloadProfilePdf(\''+$branchCode+'\',\'Men\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" related-box="myModal" class="btn-setting icon32 icon-blue icon-envelope-closed" title="Email" branch-code="'+$branchCode+'" branch-type="Men" for-branch-report="true" style="cursor:pointer"></span></td>';
                            break;
                        
                        
                    case 'Gents and Ladies':
                            $rowCounter++;
                            flagAgent = true;
                            /*$reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                             '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.jpg"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\'riyad\')"></span></td>';
                             */
                            $branchCode = markerInfo[markerInfoIndex][0]['branchCode'];
                            $reportContent += '<td class="branchCode-'+$branchCode+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                            '</td><td><i><a style="text-decoration: underline;" href="index.php?code=' +$branchCode+ '&type=ladies" target="_blank">'+$branchCode+'</a></i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=ladies" target="_blank"><img src="../public/img/ladies.jpg"  title="Ladies Branch"/></a><span class="icon32 icon-blue  icon-pdf" title="Download Profile pdf"  style="cursor:pointer;background-image: url(../public/img/opa-icons-blue32.png);" onclick="downloadProfilePdf(\''+$branchCode+'\',\'Ladies\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" branch-code="'+$branchCode+'" branch-type="Ladies" related-box="myModal" class="btn-setting icon32 icon-blue icon-envelope-closed" title="Email" for-branch-report="true" style="cursor:pointer"></span><br/><i><a style="text-decoration: underline;" href="index.php?code=' +$branchCode+ '&type=Men" target="_blank">'+$branchCode+'</a></i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=Men" target="_blank"><img src="../public/img/men.jpg"  title="Men Branch"/></a><span class="icon32 icon-blue  icon-pdf" title="Download Profile pdf"  style="cursor:pointer;background-image: url(../public/img/opa-icons-blue32.png);" onclick="downloadProfilePdf(\''+$branchCode+'\',\'Men\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" related-box="myModal" class="btn-setting icon32 icon-blue icon-envelope-closed" branch-code="'+$branchCode+'" branch-type="Men" title="Email" for-branch-report="true" style="cursor:pointer"></span></td>';
                            
                            if($rowCounter>4){
                               $rowCounter = 1;
                               $reportContent += '</tr><tr>';
                             }
                            
                            /*$rowCounter++;
                             $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                             '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><img src="../public/img/men.jpg"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Men\',\'riyad\')"></span></td>';
                             break;*/
                    default:
                       break;
              } 
         } 
        $reportContent += "</tr></table>"; 
        if(flagAgent){
          $("#branch-list-box").html($reportContent);
        }
        firstTime = 1;
        flagAgent = false;
       } 
     }
   } 
    
    this.clearMap = function(){
        this.clearCluster();
        this.clearMarker();
        this.markerInfo = new Array();
        if($("#toggle_map_"+this.mapType).is(":checked")){
            $("#toggle_map_"+this.mapType).trigger("click");
            $("#toggle_map_"+this.mapType).removeAttr('checked');
            initializeHeatmapSelector(this.mapType);
        }
        
    }
    
    this.createHeatmap = function(heatmapData){
        this.heatmap = new google.maps.visualization.HeatmapLayer({
            data:heatmapData, 
            radius:85
        });
    }
    
    this.addListeners = function(){
        var me = this;
         google.maps.event.addListener(this.map, 'tilesloaded', function (event) { 
              if(first == ""){
                changeIsrailLayer(this.getZoom());
                first = 1;
              }
              if(!previousMapType){
                previousMapType = this.getMapTypeId();
              }else{
                  if(this.getMapTypeId() != previousMapType){
                        changeIsrailLayer(this.getZoom());
                        previousMapType = this.getMapTypeId();
                  }
              }
          });
          google.maps.event.addListener(this.map, 'zoom_changed', function (event) {
                $zoomChanged = true;
                changeIsrailLayer(this.getZoom());
                if(this.getZoom() < 5){
                    if(first == 1)
                        this.setZoom(5);
                    first = 2;
                };
          });
          google.maps.event.addListener(this.map, 'idle', function () {
            if($zoomChanged){
                if(me.heatmap)
                    me.heatmap.setOptions({radius:getNewRadius(this.getZoom())});
                $zoomChanged = false;
            }
          });
    }
    this.resizeMap = function() {
        google.maps.event.trigger(this.map, 'resize');
        this.map.setCenter(new google.maps.LatLng(22.268763552489748,47.87841796875));
    }
}

function callMe(results){
    myresults  = JSON.parse(results);
    $("#mapjscontent").html('<script type="text/javascript">'+myresults.mapfunction+'</script>');
    createMarkers(results);
}

function createMarkers(results){
    //results     = JSON.parse(results);
    if(results.resultType == 'atm')
        mapObject   =  mapObjectAtm;
    else
        mapObject   = mapObjectBranch;
        mapObject.clearMap();
    
    var mapdata     = results.mapdata;
    var heatmapdata = results.heatmapdata;
    
    for(var resultIndex in mapdata){
        mapObject.createMarker(new google.maps.LatLng(mapdata[resultIndex].latitude, mapdata[resultIndex].longitude),results.resultType,mapdata[resultIndex]);
    }
    
    if(!mapObject.heatmap)
        initializeHeatMap(heatmapdata,results.resultType);
        
    mapObject.createCluster();
    mapObject.createList();
    mapObject.createBranchReportList();
    $("#loading_screen_"+(results.resultType).toLowerCase()).hide();
 }

function createMapRequest($type,$requestType){   
    $filterRequestType = ($requestType == 'atm')?'ATM':'Bank';
    $branchFilter = {
            type :  $filterRequestType 
    };
    $heatMapFilter = {
        type :  $filterRequestType 
    };
    
    if($type == 'code'){
       $digitRegx = '\[0-9]';
       if($("#search_code_"+$requestType).val().match($digitRegx)) {
           $("#overview_"+$requestType).html($("#branch_loader").html());
           $branchFilter.branchCode = $("#search_code_"+$requestType).val();
           $heatMapFilter.branchCode = $("#search_code_"+$requestType).val();
        }else{
             alert("Please provide a valid Branch Code");
             $("#loading_screen_"+$requestType).hide();
             return;
        }
    }else{
        $("#overview_"+$requestType).html($("#branch_loader").html());
       /* $requestData = {
        "requestFor"    :   "map-data",
        "map-filters"       :   {
                                "province"          :   $("#province_"+$requestType).val(),
                                "city"              :   $("#city_"+$requestType).val(),
                                "branchType"        :   titleBox.getValue(),
                                "requestType"       :   $requestType 
                            }
        }*/
        
        if($("#province_"+$requestType).val()){
            $branchFilter.province = $("#province_"+$requestType).val();
            $heatMapFilter.province = $("#province_"+$requestType).val();
        }
        
        if($("#city_"+$requestType).val()){
            $branchFilter.city = $("#city_"+$requestType).val();
            $heatMapFilter.city = $("#city_"+$requestType).val();
        }
        
        if(titleBox.getValue().length){
            $branchType  = titleBox.getValue();
            $customBranchType = [];
            for(var i in $branchType){
                if($branchType[i] == 'Ladies'){ 
                    $customBranchType.push($branchType[i]);
                }
                    
                if($branchType[i] == 'Men'){
                    $customBranchType.push('Gents');
                }
                
                if($branchType[i] == 'Private Banking'){
                    $branchFilter.isPrivateBanking = 1;
                }
                
                if($branchType[i] == 'Golder Service'){
                    $branchFilter.goldenService = 1;
                }
            }
            if($customBranchType.length){
                $customBranchType.push('Gents and Ladies');
            }
            
            $branchFilter.branchType = $customBranchType;
            
        }
    }
    $resultData = {
            heatmapdata :getHeatMapData($heatMapFilter),
            resultType  : $requestType,
            mapdata :   getBranchData($branchFilter)
    }
    
    console.log($resultData);
    createMarkers($resultData);
    //$requestObject.initialize($requestData,"callMe",null);
}

function fillCityOption($data){
   mapObject   =  getActiveMapTab();
   $("#city_"+mapObject.mapType).html($data);
}

function getCityList($dataFor){
    $requestData = {
        "requestFor"    :   "city-data",
        "map-filters"       :   {
                                    "region"        :   $("#province_"+$dataFor).val(),
                                    "requestType"   :   $dataFor
                                }
    }
    $requestObject.initialize($requestData,"fillCityOption",null);
    
}

function resetMapFilter($type){
    $("#province_"+$type).attr('value',"");
    $("#city_"+$type).html("<option value=''>-- Select City --</option>");
    $("#search_code_"+$type).attr('value','');
    titleBox.uncheckAll();
    if($type == 'atm'){
     $("#search_tab_atm").find('li.active').each(function(){
        if($(this).find('a').attr('href') == '#byCodeDivAtm'){
            $("#atmCodeSearchButton").trigger('click');
        }else{
            $("#atmCodeRegionButton").trigger('click');
        }
      })  
    
    } else {
          $("#search_tab_branch").find('li.active').each(function(){
        if($(this).find('a').attr('href') == '#byCodeDiv'){
            $("#branchCodeSearchButton").trigger('click');
        }else{
            $("#branchCodeRegionButton").trigger('click');
        }
      }) 
        
    }
    
    
}

function filterData($type,$filterFor){
    $("#loading_screen_"+$filterFor).show();
    if($type == 'code'){
        createMapRequest('code',$filterFor);
    }else{
        createMapRequest('other',$filterFor);
    }
}

function initializeHeatMap($result,$resultType){
    
    if($resultType == 'atm')
        mapObject   =  mapObjectAtm;
    else
        mapObject   = mapObjectBranch;
    
    var heatmapData = new Array();
   
    for(var i in $result){
        
        if(Number($result[i]['population'])){
            testData.location = new google.maps.LatLng($result[i]['latitude'],$result[i]['longitude']);
            testData.weight = Number($result[i]['population']);
            heatmapData.push(testData);
            testData={
                location:null,
                weight:null
            };
        }
    }
   
    mapObject.createHeatmap(heatmapData);
}

function switchMap(mapType){
    mapObject   =  getActiveMapTab();
    
    if(mapType == 'heat'){
        if(mapObject.heatmap)
		  mapObject.heatmap.setMap(mapObject.map);
          
        if(mapObject.cluster)
            mapObject.cluster.setMarkerMap(null);
        return "normal"
	}else{
	    if(mapObject.heatmap)
		  mapObject.heatmap.setMap(null);
          
        if(mapObject.cluster)
          mapObject.cluster.setMarkerMap(true);
        return "heat"
	}
}


function initializeHeatmapSelector($checkBoxType){
    $(document).ready(function(){
        $('#toggle_map_'+$checkBoxType+':checkbox').iphoneStyle({
            uncheckedLabel: 'Off',
            checkedLabel:   'On',
            onChange: function(){
              $("#loading_screen_"+$checkBoxType).show();
              if($checkBoxType == 'atm')
                mapType_atm = switchMap(mapType_atm);
              else
                mapType_branch = switchMap(mapType_branch);
              $("#loading_screen_"+$checkBoxType).hide();
            }
        });
    })
}

function initializeTinyscrollbar($type){
    $(document).ready(function(){
        $('#scrollbar_'+$type).tinyscrollbar({
                    sizethumb: 40
        });
    });
}

function getActiveMapTab(){
 $activeTab =   "";
 $("#myTab").find("li.active").each(function(){
        $activeTab = $(this).find("a").attr('href');
 }) 
 
 switch($activeTab){
    case "#Dashboard"    :   return mapObjectBranch;
    case "#AtmContainer" :   return mapObjectAtm;
    default              :   return mapObjectBranch;
 }  
}

function resizeActiveMap($resultType){
    if($resultType == 'atm')
        if(mapObjectAtm)
            mapObjectAtm.resizeMap();
    else
        if(mapObjectBranch)
             mapObjectBranch.resizeMap();
    
}

var mapObjectBranch = new MapCreator({
                    mapContainer    :   "map_section_branch",
                    type            :   "branch"
                });
var mapObjectAtm = new MapCreator({
                    mapContainer    :   "map_section_atm",
                    type            :   "atm"
                });
var mapObject = null;
function createMap(){
    mapObjectBranch.createMap();
    if($("#loading_screen_atm").length){
        mapObjectAtm.createMap();
    }
    
    loadMapJsData('branch');
    //createMapRequest('other','branch');
    
    //if($("#loading_screen_atm").length){
      // createMapRequest('other','atm');
    //}
}

function loadMapJsData($type){
    $requestData = {
    "requestFor"    :   "write-map-data",
    "request-param" :   {
                            "for"          :   $type
                          
                        }
    }
    $requestObject.initialize($requestData,"requestFilterMap",null);
}

function requestFilterMap($result){
    $flag = false;
    $result = JSON.parse($result);
    $("#"+$result.type+"JavascriptData").html($result.scriptData);
    
    if($result.type == 'atm'){
        if($("#loading_screen_atm").length){
            $flag = true;
        }
    }else{
        $flag = true;
    }
    //uppendScript($result,$flag);
    if($flag){
      createMapRequest('other',$result.type);
    }
}

function uppendScript(file,runFlag){
    var oScript = document.createElement("script");
	oScript.setAttribute("src", file.scriptfile);
	oScript.setAttribute("type", "text/javascript");
	document.getElementsByTagName("head")[0].appendChild(oScript);
    oScript.onload = function(runFlag){
        if(runFlag){
            createMapRequest('other',file.type);
        }
    }
}

$("#overview_branch").html($("#branch_loader").html());

 if($("#overview_atm").length){
    $("#overview_atm").html($("#atm_loader").html());
} 

 createMap();                                //Execution starts Here
 initializeHeatmapSelector('branch');
 initializeHeatmapSelector('atm');


