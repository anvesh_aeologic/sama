
function ticketQuestionFilter(){
    this.data = {   
        "pageNumber"    :   0,
        "orderType"     :   "DESC",
        "orderField"    :   "ticketNumber",
        "changeOrder"   :   true,
        "ticketType"    :   false
    };
    
    this.set = function($filterIndex,$filterValue) {
        switch($filterIndex){
            case "orderField"           :   this.data.orderField         = $filterValue;break;
            case "orderType"            :   this.data.orderType          = $filterValue;break;
            case "pageNumber"           :   this.data.pageNumber         = $filterValue;break;
            case "changeOrder"          :   this.data.changeOrder        = $filterValue;break;
            case "questionId"           :   this.data.questionId         = $filterValue;break;
            case 'departmentName'       :   this.data.departmentName     = $filterValue;break;
            case 'ticketColor'          :   this.data.ticketColor        = $filterValue;break;
            case 'ticketStatus'         :   this.data.ticketStatus       = $filterValue;break;
            case 'requestType'          :   this.data.requestType        = $filterValue;break;
            case 'branchCode'           :   this.data.branchCode         = $filterValue;break;
            case 'branchType'           :   this.data.branchType         = $filterValue;break;
            case 'functionName'         :   this.data.functionName       = $filterValue;break;
            case 'ticketType'           :   this.data.ticketType         = $filterValue;break;
            case 'ticketAge'            :   this.data.ticketAge          = $filterValue;break;
            case 'regionName'           :   this.data.regionName         = $filterValue;break;
        }
    }

    this.get = function(){
        return this.data;
    }

    this.destroy = function(){
         this.data.orderField     =    'ticketNumber';
         this.data.orderType      =    'DESC';
         this.data.pageNumber     =     0;
         this.data.changeOrder    =     false;
         this.data.questionId     =     '';
         this.data.departmentName =     '';
         this.data.ticketColor    =     '';
         this.data.ticketStatus   =     '';
         this.data.requestType    =     '';
         this.data.ticketType     =     '';
         this.data.ticketAge      =     '';
         this.data.regionName     =     '';
    }
    
    this.getOrderType = function(){
        $orderType = {
            "orderType"     :   "DESC",
            "orderField"    :   "ticketNumber",
        }

        $("#ticket-question-sorting").find('th').each(function(){
            if($(this).data('sorttype')){
                $orderType["orderField"] = $(this).data('sortfield');
                $orderType["orderType"] = $(this).data('sorttype');
                
            }
        });
        
        ticketQuestionFilterData.set('orderField',$orderType.orderField);
        ticketQuestionFilterData.set('orderType',$orderType.orderType);
        return $orderType;
    }
}

function ticketATMQuestionFilter(){
    this.data = {
        "pageNumber"         :   0,
        "orderType"          :   "DESC",
        "orderField"         :   "ticketNumber",
        "changeOrder"        :   true
    };
    
    this.set = function($filterIndex,$filterValue) {
        switch($filterIndex){
            case "orderField"                 :   this.data.orderField               = $filterValue;break;
            case "orderType"                  :   this.data.orderType                = $filterValue;break;
            case "pageNumber"                 :   this.data.pageNumber               = $filterValue;break;
            case "changeOrder"                :   this.data.changeOrder              = $filterValue;break;
            case "questionId"                 :   this.data.questionId               = $filterValue;break;
            case 'departmentName'             :   this.data.departmentName           = $filterValue;break;
            case 'ticketColor'                :   this.data.ticketColor              = $filterValue;break;
            case 'ticketStatus'               :   this.data.ticketStatus             = $filterValue;break;
            case 'requestType'                :   this.data.requestType              = $filterValue;break;
            case 'branchCode'                 :   this.data.branchCode               = $filterValue;break;
            case 'branchType'                 :   this.data.branchType               = $filterValue;break;
            case 'functionName'               :   this.data.functionName             = $filterValue;break;
            case 'ticketAge'                  :   this.data.ticketAge                = $filterValue;break;
        }
    }

    this.get = function(){
        return this.data;
    }

    this.destroy = function(){
         this.data.orderField     =    'ticketNumber';
         this.data.orderType      =    'DESC';
         this.data.pageNumber     =     0;
         this.data.changeOrder    =     false;
         this.data.questionId     =     '';
         this.data.departmentName =    '';
         this.data.ticketColor    =    '';
         this.data.ticketStatus   =    '';
         this.data.requestType    =    '';
         this.data.ticketAge      =    '';
    }
}



var ticketQuestionFilterData = new ticketQuestionFilter();

function getTicketQuestionFilter($filterType){
    $("#ticket-question-sorting").find('span img').each(function(){
        if($(this).attr('orderType')){
            ticketQuestionFilterData.set('orderField',$(this).attr('orderField'));
            ticketQuestionFilterData.set('orderType',$(this).attr('orderType'));
        }

    });
    
}

var ticketATMQuestionFilterData = new ticketATMQuestionFilter();

function getTicketQuestionFilter($filterType){
    $("#ticket-question-sorting").find('span img').each(function(){
        if($(this).attr('orderType')){
            ticketATMQuestionFilterData.set('orderField',$(this).attr('orderField'));
            ticketATMQuestionFilterData.set('orderType',$(this).attr('orderType'));
        }
    });
    
}


function getCurrentPageNumber(){
    if($("#gotoQuestionPageNumber").length){
       
        ticketQuestionFilterData.set('pageNumber',$("#gotoQuestionPageNumber").val());
    }
    if($("#gotoFilterPageNumber").length){
        ticketQuestionFilterData.set('pageNumber',$("#gotoFilterPageNumber").val());
    }
}


function getATMCurrentPageNumber(){
  
    if($("#gotoQuestionPageNumber")){
        ticketATMQuestionFilterData.set('pageNumber',$("#gotoQuestionPageNumber").val());
    }
    
    if($("#gotoATMFilterPageNumber")){
        ticketATMQuestionFilterData.set('pageNumber',$("#gotoATMFilterPageNumber").val());
    }
}

function fillTicketQuestionPaginationResult(result){
    $("#ticketQuestionPaginationDiv").html(result);
    $("#ticketQuestionFileterloader").hide();
}

 function fillTicketQuestionFilterResult(result){
   
   if(result){
        $("#ticketQuestionList").html('');
        $("#ticketQuestionList").html(result);
   }else{
       return {
            call : function(result){
                $("#ticketQuestionList").html('');
                $("#ticketQuestionList").html(result);
            }
       };
   
   }
}
_global.fillTicketQuestionFilterResult = new fillTicketQuestionFilterResult(''); 


 function fillATMTicketQuestionFilterResult(result){
   if(result){
         $("#atmTicketImageLoader").hide();
         $("#atmticketQuestionList").html('');
         $("#atmticketQuestionList").html(result);
   }else{
       return {
            call : function(result){
                $("#atmticketQuestionList").html('');
                $("#atmticketQuestionList").html(result);
            }
       };
   
   }
}
_global.fillATMTicketQuestionFilterResult = new fillATMTicketQuestionFilterResult(''); 





function sortByTicketQuestion(sortBy,sortOrder,imageType){
    
    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('orderField',sortBy);
    ticketQuestionFilterData.set('orderType',sortOrder);
    $("#"+sortBy+"_sorting_header").find(".sort-loader").css('visibility','visible');
    
    ticketQuestionFilterData.set('changeOrder',false);
    ticketQuestionFilterData.set('functionName','sortByTicketQuestion');
    
    if($("#ticketQuestionList").attr('ticket-current-status')){
        $dataToSend = ticketQuestionFilterData.get()
        $dataToSend['ticket-current-status']  = $("#ticketQuestionList").attr('ticket-current-status');
        $dataToSend['requestType']            = $("#ticketQuestionList").attr('ticket-current-status')+' Tickets';
        $dataToSend['ticket-time']            = $("#ticketQuestionList").attr('ticket-time');
        $dataToSend['department-id']          = $("#ticketQuestionList").attr('department-id');
        $dataToSend["isTicketQuestionFilter"] = true; 
    }else{
        $dataToSend=[];
        $dataToSend['ticket-current-status'] = 0;
        $dataToSend['requestType'] = 0;
        $dataToSend['ticket-time'] = 0;
        $dataToSend['department-id'] = 0;
        $dataToSend['isTicketQuestionFilter'] = 0;
        
        ticketQuestionFilterData.set('questionId',$("#ticketQuestionList").attr('question-id'));
        ticketQuestionFilterData.set('ticketType',$("#ticketQuestionList").attr('ticket-type'));
        ticketQuestionFilterData.set('ticketStatus','pending');
        ticketQuestionFilterData.set('ticketAge',$("#ticketQuestionList").attr('ticket-age'));
        getCurrentPageNumber();

        $dataToSend = ticketQuestionFilterData.get()
        $dataToSend["isTicketQuestionFilter"] = true; 
    }
    
    $("#ticketFileterloader").show();
    $requestData = {
        "requestFor"     :   "ticket-question-result",
        "image-type"     :    imageType,
        "ticket-question-filters" : $dataToSend
    }    
    $requestObject.initialize($requestData,"fillTicketQuestionFilterResult",null);
}

function sortByTransferedQuestion(sortBy,sortOrder){
    $pageNumber = 0;
    if( Number($('#gotoTransferedQuestionPageNumberUpdated option:selected').val()) ){
        $pageNumber = Number($('#gotoTransferedQuestionPageNumberUpdated option:selected').val());
    }
    $("#ticketFileterloader").show();
    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('orderField',sortBy);
    ticketQuestionFilterData.set('pageNumber', $pageNumber);    
    ticketQuestionFilterData.set('orderType',sortOrder);
    ticketQuestionFilterData.set('questionId',$("#ticketQuestionList").attr('question-id'));
    ticketQuestionFilterData.set('departmentName',$("#ticketQuestionList").attr('department-name'));
    ticketQuestionFilterData.set('changeOrder',false);
   // ticketQuestionFilterData.set('ticketStatus','pending');
    ticketQuestionFilterData.set('functionName','sortByTicketQuestion');
    
    getCurrentPageNumber(); 
    $requestData = {
        "requestFor"     :   "transfered-question-result",
        "ticket-question-filters" : ticketQuestionFilterData.get()
    }
    $requestObject.initialize($requestData,"fillTicketQuestionFilterResult",null);
    
}


function sortByATMTicketQuestion(sortBy,sortOrder){
   
    $("#atmTicketImageLoader").show();
    ticketATMQuestionFilterData.destroy();
    ticketATMQuestionFilterData.set('orderField',sortBy);
    ticketATMQuestionFilterData.set('orderType',sortOrder);
    ticketATMQuestionFilterData.set('questionId',$("#atmticketQuestionList").attr('question-id'));
    ticketATMQuestionFilterData.set('changeOrder',false);
    
    ticketATMQuestionFilterData.set('ticketStatus','pending');
    ticketATMQuestionFilterData.set('functionName','sortByATMTicketQuestion');
    
    getATMCurrentPageNumber(); 
    
    $requestData = {
        "requestFor"     :   "atm-ticket-question-result",
        "ticket-question-filters" : ticketATMQuestionFilterData.get()
    }
    
    $requestObject.initialize($requestData,"fillATMTicketQuestionFilterResult",null);
}


function sortByTicketColor(sortBy,sortOrder){
    
    $("#ticketFileterloader").show();
    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('orderField',sortBy);
    ticketQuestionFilterData.set('orderType',sortOrder);
    ticketQuestionFilterData.set('departmentName',$("#popupDepartmentName").val());
    ticketQuestionFilterData.set('ticketColor',$("#popupTicketColor").val());
    ticketQuestionFilterData.set('regionName',$("#popupRegionName").val());
    ticketQuestionFilterData.set('changeOrder',false);
    ticketQuestionFilterData.set('functionName','sortByTicketColor');
    
    getCurrentPageNumber(); 
    $requestData = {
        "requestFor"     :   "ticket-question-result",
        "ticket-question-filters" :   ticketQuestionFilterData.get()
    }
    $requestObject.initialize($requestData,"fillTicketQuestionFilterResult",null);
}

function sortByATMTicketColor(sortBy,sortOrder)
{
     $("#atmTicketImageLoader").show();
     ticketATMQuestionFilterData.destroy();
     ticketATMQuestionFilterData.set('orderField',sortBy);
     ticketATMQuestionFilterData.set('orderType',sortOrder);
     ticketATMQuestionFilterData.set('departmentName',$("#popupDepartmentName").val());
     ticketATMQuestionFilterData.set('ticketColor',$("#popupTicketColor").val());
     ticketATMQuestionFilterData.set('changeOrder',false);
     ticketATMQuestionFilterData.set('functionName','sortByATMTicketColor');
    
     getATMCurrentPageNumber(); 
     $requestData = {
        "requestFor"     :    "atm-ticket-question-result",
        "ticket-question-filters" :   ticketATMQuestionFilterData.get()
     }
     $requestObject.initialize($requestData,"fillATMTicketQuestionFilterResult",null);
}


function sortByATMTicketStatus(sortBy,sortOrder)
{    
  
    $("#atmTicketImageLoader").show();
    $departmentClosedOpen = $("#popupDepartmentName").val();
    $ticketStatus = $("#popupTicketStatus").val();
    $questionId   = $("#ticketQuestionList").attr('question-id');
    ticketATMQuestionFilterData.destroy();
    ticketATMQuestionFilterData.set('orderField',sortBy);
    ticketATMQuestionFilterData.set('orderType',sortOrder);
    ticketATMQuestionFilterData.set('departmentName',$departmentClosedOpen);
    ticketATMQuestionFilterData.set('ticketStatus',$ticketStatus);
    ticketATMQuestionFilterData.set('questionID',$questionId);
    ticketATMQuestionFilterData.set('changeOrder',false);
    ticketATMQuestionFilterData.set('functionName','sortByATMTicketStatus');
    
    getATMCurrentPageNumber(); 
    $requestData = {
        "requestFor"     :     "atm-ticket-question-result",
        "ticket-question-filters" :   ticketATMQuestionFilterData.get()
    }
   
   $requestObject.initialize($requestData,"fillATMTicketQuestionFilterResult",null);
}



function sortByTicketStatus(sortBy,sortOrder)
{
    $departmentClosedOpen = $("#popupDepartmentName").val();
    $ticketStatus = $("#popupTicketStatus").val();
    $questionId  = $("#ticketQuestionList").attr('question-id');
    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('orderField',sortBy);
    ticketQuestionFilterData.set('orderType',sortOrder);
    ticketQuestionFilterData.set('departmentName',$departmentClosedOpen);
    ticketQuestionFilterData.set('ticketStatus',$ticketStatus);
     ticketQuestionFilterData.set('questionID',$questionId);
    ticketQuestionFilterData.set('changeOrder',false);
    ticketQuestionFilterData.set('functionName','sortByTicketStatus');
    
    getCurrentPageNumber(); 
    $requestData = {
        "requestFor"     :   "ticket-question-result",
        "ticket-question-filters" :   ticketQuestionFilterData.get()
    }
   
   $requestObject.initialize($requestData,"fillTicketQuestionFilterResult",null);
}


function changeQuestionPageNumber($type,$totalPage,$imagetype)
{
    $("#ticketFileterloader").show();
     $doPagination = false;
     $currentPage = $('#gotoQuestionPageNumber option:selected').text();
     $currentPage = $currentPage-1;
    if($type == 'next'){
        $currentPage++;
        if($('#gotoQuestionPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeQuestionPageNumberNextButton").hide();
            $("#changeQuestionPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }else{
            $("#changeQuestionPageNumberNextButton").show();
            $("#changeQuestionPageNumberPrevButton").show();
            $doPagination = true;
        }
    }else{
        $currentPage--;
        if($currentPage <= 0){
            $("#changeQuestionPageNumberPrevButton").hide();
            $("#changeQuestionPageNumberNextButton").show();
            $currentPage = 0;
            $doPagination = true;
        }else{
            $("#changeQuestionPageNumberNextButton").show();
            $("#changeQuestionPageNumberPrevButton").show();
            $doPagination = true;
        }
        
    }
    
    
    if($doPagination){
        $('#gotoQuestionPageNumber option:eq('+$currentPage+')').attr('selected', 'selected');
        $('#gotoQuestionPageNumber').trigger('change',$imagetype);
        if($('#gotoQuestionPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeQuestionPageNumberNextButton").hide();
            $("#changeQuestionPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }
    }
}

function changeVendorPageNumber($type,$totalPage)
{
    $("#ticketFileterloader").show();
     $doPagination = false;
     $currentPage = $('#gotoVendorPageNumber option:selected').text();
     $currentPage = $currentPage-1;
    if($type == 'next'){
        $currentPage++;
        if($('#gotoVendorPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeVendorPageNumberNextButton").hide();
            $("#changeVendorPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }else{
            $("#changeVendorPageNumberNextButton").show();
            $("#changeVendorPageNumberPrevButton").show();
            $doPagination = true;
        }
    }else{
        $currentPage--;
        if($currentPage <= 0){
            $("#changeVendorPageNumberPrevButton").hide();
            $("#changeVendorPageNumberNextButton").show();
            $currentPage = 0;
            $doPagination = true;
        }else{
            $("#changeVendorPageNumberNextButton").show();
            $("#changeVendorPageNumberPrevButton").show();
            $doPagination = true;
        }

    }


    if($doPagination){
        $('#gotoVendorPageNumber option:eq('+$currentPage+')').attr('selected', 'selected');
        $('#gotoVendorPageNumber').trigger('change');
        if($('#gotoVendorPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeVendorPageNumberNextButton").hide();
            $("#changeVendorPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }
    }
}


$("#gotoQuestionPageNumber").live('change',function(){
        if($('#gotoQuestionPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeQuestionPageNumberNextButton").hide();
            $("#changeQuestionPageNumberPrevButton").show();
            $doPagination = true;
        }else if($('#gotoQuestionPageNumber option:selected').text()=='1'){
            $("#changeQuestionPageNumberNextButton").show();
            $("#changeQuestionPageNumberPrevButton").hide();
        }else{
            $("#changeQuestionPageNumberNextButton").show();
            $("#changeQuestionPageNumberPrevButton").show();
            $doPagination = true;
        }
 });
 
 
function changefilterPageNumber($type,$totalPage){
    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    $doPagination = false;
    $currentPage = $('#gotoFilterPageNumber option:selected').text();
    $currentPage = $currentPage-1;
    if($type == 'next'){
        $currentPage++;
        if($('#gotoFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changefilterPageNumberNextButton").hide();
            $("#changefilterPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }else{
            $("#changefilterPageNumberNextButton").show();
            $("#changefilterPageNumberPrevButton").show();
            $doPagination = true;
        }
    }else{
        $currentPage--;
        if($currentPage <= 0){
            $("#changefilterPageNumberPrevButton").hide();
            $("#changefilterPageNumberNextButton").show();
            $currentPage = 0;
            $doPagination = true;
        }else{
            $("#changefilterPageNumberNextButton").show();
            $("#changefilterPageNumberPrevButton").show();
            $doPagination = true;
        }
        
    }
    
    
    if($doPagination){
        $('#gotoFilterPageNumber option:eq('+$currentPage+')').attr('selected', 'selected'); 
        $('#gotoFilterPageNumber').trigger('change');
        if($('#gotoFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changefilterPageNumberNextButton").hide();
            $("#changefilterPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }
    }
    
}

function changeTransferedFilterPageNumber($type,$totalPage){
    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    $doPagination = false;
    $currentPage = $('#gotoTransferedQuestionPageNumberUpdated option:selected').text();
    $currentPage = $currentPage-1;
    if($type == 'next'){
        $currentPage++;
        $('#gotoTransferedQuestionPageNumberUpdated option:eq('+$currentPage+')').attr('selected', 'selected');
        transferredTicketsNextPrevButtonManage();
        $doPagination = true;
    } else {
        $currentPage--;
        $('#gotoTransferedQuestionPageNumberUpdated option:eq('+$currentPage+')').attr('selected', 'selected');
        transferredTicketsNextPrevButtonManage();
        $doPagination = true;
    }
    
    if($doPagination){
        //$('#gotoTransferedQuestionPageNumberUpdated option:eq('+$currentPage+')').attr('selected', 'selected'); 
        $('#gotoTransferedQuestionPageNumberUpdated').trigger('change');
       /* if($('#gotoTransferedQuestionPageNumberUpdated option:selected').attr('pageClass') == 'last'){
            $("#changefilterPageNumberNextButton").hide();
            $("#changefilterPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }*/
    }
}

function transferredTicketsNextPrevButtonManage(){
     if($('#gotoTransferedQuestionPageNumberUpdated option:selected').attr('pageClass') == 'last'){
            $("#changeTransferedFilterPageNumberNextButton").hide();
            $("#changeTransferedFilterPageNumberPrevButton").show();
    }else if($('#gotoTransferedQuestionPageNumberUpdated option:selected').val() == 0){
        $("#changeTransferedFilterPageNumberNextButton").show();
        $("#changeTransferedFilterPageNumberPrevButton").hide();
    }else{
        $("#changeTransferedFilterPageNumberNextButton").show();
        $("#changeTransferedFilterPageNumberPrevButton").show();
    }
}
function changeATMfilterPageNumber($type,$totalPage)
{
    $("#atmTicketQuestionList").html($("#popupLoaderOther").html()); 
    $doPagination = false;
    $currentPage = $('#gotoATMFilterPageNumber option:selected').text();
    $currentPage = $currentPage-1;
    alert($currentPage);
    if($type == 'next'){
        $currentPage++;
        if($('#gotoATMFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changefilterPageNumberNextButton").hide();
            $("#changefilterPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }else{
            $("#changefilterPageNumberNextButton").show();
            $("#changefilterPageNumberPrevButton").show();
            $doPagination = true;
        }
    }else{
        $currentPage--;
        if($currentPage <= 0){
            $("#changefilterPageNumberPrevButton").hide();
            $("#changefilterPageNumberNextButton").show();
            $currentPage = 0;
            $doPagination = true;
        }else{
            $("#changefilterPageNumberNextButton").show();
            $("#changefilterPageNumberPrevButton").show();
            $doPagination = true;
        }
        
    }
    
    
    if($doPagination)
    {
        $('#gotoATMFilterPageNumber option:eq('+$currentPage+')').attr('selected', 'selected'); 
        $('#gotoATMFilterPageNumber').trigger('change');
        if($('#gotoATMFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changefilterPageNumberNextButton").hide();
            $("#changefilterPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }
    }
    
}





$("#gotoFilterPageNumber").live('change',function(){
        if($('#gotoFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changefilterPageNumberNextButton").hide();
            $("#changefilterPageNumberPrevButton").show();
            $doPagination = true;
        } else if($('#gotoFilterPageNumber option:selected').val() == 0){
            $("#changefilterPageNumberNextButton").show();
            $("#changefilterPageNumberPrevButton").hide();
        } else{
            $("#changefilterPageNumberNextButton").show();
            $("#changefilterPageNumberPrevButton").show();
        }
 });


$("#gotoATMFilterPageNumber").live('change',function(){
        
        if($('#gotoATMFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changefilterPageNumberNextButton").hide();
            $("#changefilterPageNumberPrevButton").show();
            $doPagination = true;
        } else if($('#gotoQuestionPageNumber option:selected').text()=='1'){
            $("#changefilterPageNumberNextButton").show();
            $("#changefilterPageNumberPrevButton").hide();
        }
 }); 
 
    
    
function gotoQuestionPageNumber(element,type,imageType){

    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('pageNumber',Number($(element).val()));
    ticketQuestionFilterData.set('changeOrder',false);
    ticketQuestionFilterData.set('questionId',$("#ticketQuestionList").attr('question-id'));   
    ticketQuestionFilterData.set('ticketType',$("#ticketQuestionList").attr('ticket-type'));  
    requestParam = {};
    
    if($("#myTab li.active").text().trim() == 'Reports'){
        if($("#hasMCP").val() == 'true'){
           $outeltType = ($("#mcpReportOutletTypeSelect").val()) ? $("#mcpReportOutletTypeSelect").val() : "";
             $fromDate = ($("#mcpFrom").val()) ? $("#mcpFrom").val() : "";
             $toDate = ($("#mcpTo").val()) ? $("#mcpTo").val() : "";

           requestParam = {
               'area-id'      : $("#area_name_select_mcp  option:selected").val(),
               'area-name'    : $("#area_name_select_mcp  option:selected").text(),
               'zone-id'      : $("#zone_name_select_mcp  option:selected").val(),
               'zone-name'    : $("#zone_name_select_mcp  option:selected").text(),
               'requestType'  : "question-popup",
               'outlet-type'  : $outeltType,
               'fromDate'     : $fromDate,
               'toDate'       : $toDate
           };
           $("#questionDetailPopup").html('');
        }
    }else{
        $("#mcpQuestionDetailPopupAreaWise").html('');
    }
    $orderFields = ticketQuestionFilterData.getOrderType();
    
    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    ticketQuestionFilterData.set('orderField','creationDate');
    $requestData = {
        "requestFor"     :   "ticket-question-pagination",
        "ticket-question-filters" :   ticketQuestionFilterData.get(),
        "requestParam"             :  requestParam,
        "imageType"              : imageType
     };
     
     $requestObject.initialize1($requestData,_global.fillTicketQuestionFilterResult,'');
}



function gotoVendorPageNumber(element){

    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('pageNumber',Number($(element).val()));
    ticketQuestionFilterData.set('changeOrder',false);
    ticketQuestionFilterData.set('vendorId',$("#ticketQuestionList").attr('vendor-id'));
    ticketQuestionFilterData.set('vendorName',$("#ticketQuestionList").attr('vendor-name'));
    ticketQuestionFilterData.set('vendorTitle',$("#ticketQuestionList").attr('vendor-title'));
    requestParam = {};
    requestParam = {
                    'vendorId'    : $("#ticketQuestionList").attr('vendor-id'),
                    'vendorName'  : $("#ticketQuestionList").attr('vendor-name'),
                    'vendorTitle' : $("#ticketQuestionList").attr('vendor-title')
                    };
    $orderFields = ticketQuestionFilterData.getOrderType();

    $("#ticketQuestionList").html($("#popupLoaderOther").html());
    ticketQuestionFilterData.set('orderField','creationDate');
    $requestData = {
        "requestFor"     :   "ticket-vendor-pagination",
        "ticket-question-filters" :   ticketQuestionFilterData.get(),
        "requestParam"             :  requestParam

     };

     $requestObject.initialize1($requestData,_global.fillTicketQuestionFilterResult,'');
}


function gotoTransferedQuestionPageNumber(element){
    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('pageNumber',Number($(element).val()));
    ticketQuestionFilterData.set('changeOrder',false);
    ticketQuestionFilterData.set('questionId',$("#ticketQuestionList").attr('question-id'));   
    
    $orderFields = ticketQuestionFilterData.getOrderType();
    
    $requestData = {
        "requestFor"     :   "transfer-ticket-pagination",
        "ticket-question-filters" :   ticketQuestionFilterData.get()
     };
     
    $requestObject.initialize1($requestData,_global.fillTicketQuestionFilterResult,'');
}


function gotoTransferedQuestionPageNumberUpdated(element){
    transferredTicketsNextPrevButtonManage();
    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    ticketQuestionFilterData.destroy();
    ticketQuestionFilterData.set('pageNumber',Number($(element).val()));
    ticketQuestionFilterData.set('changeOrder',false);
    ticketQuestionFilterData.set('questionId',$("#ticketQuestionList").attr('question-id')); 
    ticketQuestionFilterData.set('departmentName',$("#ticketQuestionList").attr('department-name'));    
    
    
    $orderFields = ticketQuestionFilterData.getOrderType();
    
    $requestData = {
        "requestFor"     :   "transfered-ticket-pagination",
        "ticket-question-filters" : ticketQuestionFilterData.get()
     };
     
    $requestObject.initialize1($requestData,_global.fillTicketQuestionFilterResult,'');
}

function changeTicketAnalysisfilterPageNumber($type,$totalPage){
    $("#ticketQuestionList").html($("#popupLoaderOther").html()); 
    $doPagination = false;
    $currentPage = $('#gotoTicketAnalysisFilterPageNumber option:selected').text();
    $currentPage = $currentPage-1;
    if($type == 'next'){
        $currentPage++;
        if($('#gotoTicketAnalysisFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeTicketAnalysisfilterPageNumberNextButton").hide();
            $("#changeTicketAnalysisfilterPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }else{
            $("#changeTicketAnalysisfilterPageNumberNextButton").show();
            $("#changeTicketAnalysisfilterPageNumberPrevButton").show();
            $doPagination = true;
        }
    }else{
        $currentPage--;
        if($currentPage <= 0){
            $("#changeTicketAnalysisfilterPageNumberPrevButton").hide();
            $("#changeTicketAnalysisfilterPageNumberNextButton").show();
            $currentPage = 0;
            $doPagination = true;
        }else{
            $("#changeTicketAnalysisfilterPageNumberNextButton").show();
            $("#changeTicketAnalysisfilterPageNumberPrevButton").show();
            $doPagination = true;
        }
        
    }
    
    
    if($doPagination){
        $('#gotoTicketAnalysisFilterPageNumber option:eq('+$currentPage+')').attr('selected', 'selected'); 
        $('#gotoTicketAnalysisFilterPageNumber').trigger('change');
        if($('#gotoTicketAnalysisFilterPageNumber option:selected').attr('pageClass') == 'last'){
            $("#changeTicketAnalysisfilterPageNumberNextButton").hide();
            $("#changeTicketAnalysisfilterPageNumberPrevButton").show();
            $currentPage = Number($totalPage)-1;
            $doPagination = true;
        }
    }
    
}