/**
 * ticket info for each metrice graph on click
 */
$elementList = ['select', 'input', 'textarea'];
var filterParentDivClass;
var resetGraphSamaFilter = function($parentFilterDiv, $requestFor){
   for(i in $elementList){
        $($parentFilterDiv).find($elementList[i]).each(function(){
            if( $(this).prop("tagName") == "SELECT" ){
                if(userBank){
                    console.log("In bank name")
                }else{
                    $(this).attr('value', ''); 
                }
                
            }else{
                console.log("in zero value ");
               $(this).attr('value', ''); 
            }
             
        });
   }
   refreshGraph($parentFilterDiv, $requestFor);
}

var getSamaGraphFilterData = function($parentFilterDiv){
    var filterData = {};
    for(i in $elementList){
        $($parentFilterDiv).find($elementList[i]).each(function(){
           if($(this).val()) {
               console.log($(this).val());
               $val = $(this).val()
               if($val == 'NA') {
                   $val = null;
               }
               filterData[$(this).attr('name')] = $val;
           }
        });
    }
    return filterData;
}

var refreshGraph = function($parentFilterDiv, $requestFor){
    var filterData = getSamaGraphFilterData($parentFilterDiv);
    console.log(filterData);
    filterQueryString($parentFilterDiv, false); // SETTING FILTERS IN QUERY STRING
    var drillFor = ($("#sama_drill_for").val()) ? $("#sama_drill_for").val() : "";
    if(!validateSamaFilter(filterData)){
    
   }else{
        $requestData = {
            'requestFor'  : $requestFor,
            'filterData' : filterData,
            'drill-for'   : drillFor
        }
       $(".sama_graph_loader").show();
       $(".calculating_knob_score").text("drilling ..");
       $requestObject.initialize($requestData,'renderGraph', null); 
   }
}

function renderGraph($resultData){
    $resultData = JSON.parse($resultData);
    $.each($resultData, function($index, $value){
        $(".sama_graph_loader").hide();
        $("#"+$value['render_to']).html($(".sama_each_graph_loader").html());
        if($value['graph_function']){
            window[$value['graph_function']]($value);
        }
            
        
    });
   
}
/**
 * SAMA :: barchart function
 * @parameter Array of graph data and configuration (such as containerID, text, subtext, data)
 */
var barChart;
var drawBarChart = function($graphData){
    $('#'+$graphData['base_id']+'_bar_chart_div').html(); // reseting SVG graph to blank for download option
    var $dataArray  = [];
    var $categories = [];
    var averageValue = 0;
    var $questionText;
    if($graphData['render_to'] == 'pos_average_transaction_time_bar_chart_container'){ 
        $questionText = '<span style="cursor:pointer;">'+$graphData['question_text']+'<br/></span><span style="color:forestgreen;">'+'Average Transaction Speed :'+$graphData['average']+'</span>';
    }else{
        $questionText = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
    }
    
    $sectorGraphHtml = "";
    if($graphData['render_to'] == "segmentation_compliance_by_sector_bar_chart_container"){
       $sectorGraphHtml = " <table style='width:100%;'><tr>";
    }
    
    
    if($graphData['data']){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
        var counter    = 0;
        var maxRange   = 100;
        var totalValue = 0;
        var dataSum    = 0;
       if($graphData['request_for'] !== 'overall-quality-index-graph-details' || $graphData['request_for'] !== 'pos-overall-quality-index-graph-details'
               || $graphData['render_to'] !== 'atm_bank_name_comparision_bar_chart_container' || $graphData['render_to'] !== 'merchant_pos_terminal_service_comparision_bar_chart_container'){
            $.each($graphData['data'], function($index, $value){
                dataSum = (dataSum + parseInt($value['count']));
            });
       }
        $.each($graphData['data'], function($index, $value){ 
            $categories.push($index);
            totalValue = (totalValue + parseInt($value['count']));
            if($graphData['request_for'] == 'overall-quality-index-graph-details' || $graphData['request_for'] == 'pos-overall-quality-index-graph-details' || $graphData['render_to'] == 'vendor_ticket_detail_bar_chart_container'){
                $data = {
                         y: (Math.round(($value['count']))),
                        'color' : $value['color'],
                        'survey_count' :$value['total_survey']
                }
            }else if($graphData['render_to'] == 'atm_bank_name_comparision_bar_chart_container' || $graphData['render_to'] == 'merchant_pos_terminal_service_comparision_bar_chart_container'){
                $data = {
                         y: (parseInt($value['count'])),
                        'color' : $value['color'],
                        'survey_count' :$value['count']
                }
            }else if($graphData['render_to'] == 'pos_average_transaction_time_bar_chart_container'){
                $data = {
                         //y: (((parseInt($value['count'])/dataSum) * 100)),
                        y: $value['survey_percent'],
                        'color' : $value['color'],
                        'survey_count' :$value['count']
                }
            }else{
                $data = {
                    y              : (Math.round((parseInt($value['count'])/dataSum) * 100)),
                    'color'        : $value['color'],
                    'survey_count' : $value['count']
                }
            }

            if($graphData['request_for'] == 'atm-details'){
                //console.log($value['quality_index_score']);

                $data = {
                    y: $value['quality_index_score'],
                    'color' : $value['color'],
                    'survey_count' :$value['count'],
                    'survey_percent' : (Math.round((parseInt($value['count'])/dataSum) * 100)),
                }
            }


            $dataArray.push($data);
            //console.log($dataArray);
           if($graphData['render_to'] == "segmentation_compliance_by_sector_bar_chart_container"){  
                if( (counter) % 4 == 0){
                    $sectorGraphHtml += "</tr><tr><td colspan='4'>&nbsp;</td></tr><tr>";
                }
                $sectorGraphHtml += "<td><div style='width: 20px;height: 20px;background: "+$value['color']+" ;-moz-border-radius: 50px;-webkit-border-radius: 50px;border-radius: 50px;position: absolute;margin-top: -2px;'></div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+(counter + 1)+ ". "+$index+"</td>";
           }
           counter++;
        }); 
        

         if($graphData['request_for'] == 'overall-quality-index-graph-details' || $graphData['request_for'] == 'pos-overall-quality-index-graph-details') {
             averageValue = Number((totalValue/ $dataArray.length).toFixed(2));
         }else {
             averageValue = Number((100 / $dataArray.length).toFixed(2));
         }

         
        var minimumMarkOnGraphConfig = "";   // FOR INDEX SCORE GRAPHS. ------
        if($graphData['request_for'] == 'overall-quality-index-graph-details' ||  $graphData['request_for'] == 'pos-overall-quality-index-graph-details'){ 
            minimumMarkOnGraphConfig = [{ value : averageValue,color : 'red',dashStyle : 'shortdash', width : 2,zIndex: 99999,label : {text : averageValue , align: 'right', color: 'red'}}]; 
        }

        
        if($graphData['render_to'] == "segmentation_compliance_by_sector_bar_chart_container"){
           $sectorGraphHtml += "</tr></table>";
           $("#segmentation_compliance_by_sector_legend_holder").html($sectorGraphHtml);
        }
        var each = Highcharts.each;
        Highcharts.wrap(Highcharts.seriesTypes.column.prototype, 'drawPoints', function(proceed) {
           var series = this;
           if(series.data.length > 0 ){
               var width = series.barW > series.options.maxPointWidth ? series.options.maxPointWidth : series.barW;
               each(this.data, function(point) {
                   point.shapeArgs.x += (point.shapeArgs.width - width) / 2;
                   point.shapeArgs.width = width;
               });
           }
           proceed.call(this);
        })
//        console.log($data);
        barChart = new Highcharts.Chart({
            chart: {
                renderTo: $graphData['render_to'],
                type: 'column',
                marginTop: 80
            },
            
            title: {
                text: graphHeading,
                useHTML : true
            },
            
            subtitle: {
                text: '<br/><span style="font-size: 15px">'+($graphData['sub_text'] ? $graphData['sub_text']: "") +($graphData['average'] ? $graphData['average'] : "")+'</span>'
            },
            
            xAxis: {
               categories: $categories,
                           labels: {
                rotation: parseInt($graphData['label_rotation'])
            }
            },

            yAxis: {
                min: 0,
                max: maxRange,
                allowDecimals: false,
                title: {
                    text: ''
                },
                tickInterval: 20,
                labels: {
                    formatter: function() {
                        var dataVal = this.value; 
                        return (($graphData['request_for'] == 'overall-quality-index-graph-details'|| $graphData['request_for'] == 'pos-overall-quality-index-graph-details') ? (dataVal) : (dataVal + '%'));
                    }
                },
                 plotLines : minimumMarkOnGraphConfig
            },
            tooltip: {
                pointFormat: ($graphData['is_percentage']) ? (($graphData['request_for'] == 'atm-details')
                    ? '<span>Number of Survey</span>: <b>{point.survey_count}</b><br><span>Survey Percent</span>: <b>{point.survey_percent} %</b>' : '<span>{series.name}</span>: <b>{point.survey_count}</b>' ) : '<span>{series.name}</span>: <b>{point.y}</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: false 
                },
                series : {
                    point : {
                        events : {
                            click : function(){
                                var selectCategory = this.category;
                                $dataToReturn = {
                                    'render_to'     : $graphData['render_to'],
                                    'category'      : selectCategory,
                                    'question_id'   : $graphData['question_id'],
                                    'question_text' : $graphData['question_text'] ,
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    borderWidth:0,
                    allowDecimals: false,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            if(parseInt(this.y) > 95){
                                return '<span style="color:white;">'+(($graphData['request_for'] == 'overall-quality-index-graph-details' || $graphData['request_for'] == 'pos-overall-quality-index-graph-details')
                                        ? (Highcharts.numberFormat(this.y,0)) : (($graphData['request_for'] == 'atm-details'    ) ? (Highcharts.numberFormat(this.y,2)) : (Highcharts.numberFormat(this.y,0) + '%')))+'</span>';
                            } else {
                                return (($graphData['request_for'] == 'overall-quality-index-graph-details' || $graphData['request_for'] == 'pos-overall-quality-index-graph-details') ? (Highcharts.numberFormat(this.y,0)) : (
                                ($graphData['render_to'] == 'pos_average_transaction_time_bar_chart_container') ? (Highcharts.numberFormat(this.y,2) + '%') :
                                    (($graphData['request_for'] == 'atm-details') ? (Highcharts.numberFormat(this.y,2)) : (Highcharts.numberFormat(this.y,0) + '%'))));
                            }
                            
                        }
                    }
                }
            },
            series: [{ 
                name: $graphData['legend_text'],
                color: $graphData['legend_color'].split(",")[0],
                maxPointWidth: 50,
                marker: {
                    symbol: 'square'
                    
                },
                data: $dataArray
            }]
        
        }, function(){
            $(".nav-tabs").on("click", function(){
                barChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
            if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;
                
                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                   $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove(); 
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');
                
                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });
        
        var chart_svg = barChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_bar_chart_canvas'); 
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_bar_chart_div').html('<img id="'+$graphData['base_id']+'_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }  
}

/**
 * drawPieChart :: generic function to draw pie chart
 * @parameter array of all required details fro pie chart
 * @return render pie chart to container
 */
 var pieChart;
 var drawPieChart = function($graphData){
    $('#'+$graphData['base_id']+'_bar_chart_div').html(); // reseting SVG graph to blank for download option
    
    var $dataArray  = [];
//    console.log($graphData['question_text']);
//    console.log($graphData['data']);

     $transactionSpeedGraphHtml = "";
     if($graphData['render_to'] == "pos_first_transaction_duration_pie_chart_container" || $graphData['render_to'] == "pos_second_transaction_duration_pie_chart_container" ||
         $graphData['render_to'] == "pos_third_transaction_duration_pie_chart_container"){
         $transactionSpeedGraphHtml = " <table style='width:100%;' class='table table-striped'><thead><tr><th style='width: 205px;'>Bank Name</th><th>Average Transaction Speed (Sec.)</th></tr></thead><tbody>";
     }
    if($graphData['data']){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
        var counter = 0;
        var color = '';
        var selectedCategoryIndex = 0;
        var sliced = true;
        $.each($graphData['data'], function($index, $value){
            $data = {
                'name'   : $index,
                 y       : parseInt($value['count']),
                 sliced  :  sliced,
                'color'  : $value['color']
            }
            $dataArray.push($data);



        });
        if($graphData['render_to'] == "pos_first_transaction_duration_pie_chart_container" || $graphData['render_to'] == "pos_second_transaction_duration_pie_chart_container" ||
            $graphData['render_to'] == "pos_third_transaction_duration_pie_chart_container") {
            $.each($graphData['transaction_speed'], function ($index, $value) {

                $transactionSpeedGraphHtml += "<tr><td>" + $value['bank_name'] + "</td><td style='text-align: center;'>" + $value['average_speed'] + "</td></tr>";

            });
        }

       // console.log($transactionSpeedGraphHtml);
        if($graphData['render_to'] == "pos_first_transaction_duration_pie_chart_container"){
            $transactionSpeedGraphHtml += "</tbody></table>";
            $("#pos_first_transaction_duration_pie_chart_holder").html($transactionSpeedGraphHtml);
        }

        if($graphData['render_to'] == "pos_second_transaction_duration_pie_chart_container"){
            $transactionSpeedGraphHtml += "</tbody></table>";
            $("#pos_second_transaction_duration_pie_chart_holder").html($transactionSpeedGraphHtml);
        }

        if($graphData['render_to'] == "pos_third_transaction_duration_pie_chart_container"){
            $transactionSpeedGraphHtml += "</tbody></table>";
            $("#pos_third_transaction_duration_pie_chart_holder").html($transactionSpeedGraphHtml);
        }

        pieChart = new Highcharts.Chart({
            chart: {
                renderTo: $graphData['render_to'],
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: graphHeading,
                useHTML : true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            credits : {
                enabled :  false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                    enabled: true,
                    format: '{point.percentage:.0f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                    showInLegend: true
                },
                series : {
                    point : {
                        events : {
                            click : function(){
                                console.log($graphData);
                                if($graphData['drill_function'] == "drillPieFurther"){
                                    $globalQueryString = filterQueryString($graphData['filter_parent_div_class'], true);
                                    if($globalQueryString)
                                        $globalQueryString = "&"+$globalQueryString;
                                    //console.log($globalQueryString);
                                    $graphToBeDrillFor = ['COUNTER-TOP', 'PORTABLE', 'MOUNTED', 'ON THE SURFACE', 'PIN PAD NOT ATTACHED', 'PIN PAD ATTACHED'];
                                    if (($.inArray(this.name, $graphToBeDrillFor) !== -1) || $graphData['render_to'] == "pin_pad_attached_pie_chart_container"){
                                        window.open(baseUrl+baseBank+"/posdashboard/accessLocationDrillIndex.php?drill-for="+encodeURIComponent(this.name)+"&bread_cum="+encodeURIComponent($(".sama_breadcum").html())+$globalQueryString);
                                    }
                            }else{
                                    answerText = this.name;
                                    if($graphData['render_to'] == "vendor_brand_detail_pie_chart_container") {
                                        $dataToReturn = {
                                            'render_to': $graphData['render_to'],
                                            'question_id': $graphData['question_id'],
                                            'survey_type': $graphData['data'][answerText]['survey_type'],
                                            'answer_text': answerText,
                                            'question_text': $graphData['question_text'],
                                            'request_for': $graphData['request_for']
                                        }
                                    } else {
                                        $dataToReturn = {
                                            'render_to': $graphData['render_to'],
                                            'question_id': $graphData['question_id'],
                                            'answer_text': answerText,
                                            'question_text': $graphData['question_text'],
                                            'request_for': $graphData['request_for']
                                        }
                                    }
                                    console.log($dataToReturn);
                                    window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                                }
                            }
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: $graphData['legend_text'],
                data: $dataArray
            }]
        }, function(){
            $(".nav-tabs").on("click", function(){
                pieChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
            if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;
                
                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                   $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove(); 
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');
                
                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });
      
        
        var chart_svg = pieChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_pie_chart_canvas'); 
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_pie_chart_div').html('<img id="'+$graphData['base_id']+'_pie_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }
    
 }
 
 /**
  * @description :: Draws line chart 
  * @oparam graph data array
  */
 var lineChart;
 var drawLineChart = function($graphData){ 
    var chart;
    var categories = [];
    var categoryIndex = [];
    var $dataArray = [];
    var $colorArray = ($graphData['legend_color']).split(',');
    if($graphData['data'] != ''){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
//        console.log($graphData['categories']);
        $.each($graphData['categories'], function($index, $value){
            categories.push($value); 
        });
        
        $.each($graphData['data'], function($index, $value){
            $data = {
                'name' : $index,
                'data' : $value['data'],
                'total_count' :  $value['count']
            }
           $dataArray.push($data);
        });
        
        lineChart = new Highcharts.Chart({
             chart: {
                renderTo: $graphData['render_to'],
                type: 'line'
            },
            colors: $colorArray,
            title: {
                text: graphHeading,
                useHTML :  true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            tooltip: {
                    pointFormat: ((($graphData['request_for'] === 'satisfaction-level-graph-details' && $graphData['render_to'] !== 'satisfaction_level_trend_line_chart_container')
                            || $graphData['render_to'] === 'transaction_receipt_line_chart_container') 
                            ? ('{series.name}: <b>{point.y}</b>') :('{series.name}: <b>{point.y}%</b>'))
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: ''
                },
                min: 0,
                max: 100,
                allowDecimals: false,
                tickInterval: 20,
                labels: {
                    formatter: function () {
                        return ((($graphData['request_for'] === 'satisfaction-level-graph-details' && $graphData['render_to'] !== 'satisfaction_level_trend_line_chart_container')
                                || $graphData['render_to'] === 'transaction_receipt_line_chart_container') 
                                ? (this.value) : (this.value + '%'));
                    }
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: true
                },
                 series : {
                    point : {
                        events : {
                            click : function(){
                                month         = this.category;
                                answerText    = this.series.name;
                                $dataToReturn = {
                                    'render_to'     : $graphData['render_to'],
                                    'question_id'   : $graphData['question_id'],
                                    'month'         : month,
                                    'answer_text'   : answerText,
                                    'question_text' : $graphData['question_text'],
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    borderWidth:0,
                    allowDecimals: false,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            return ((($graphData['request_for'] === 'satisfaction-level-graph-details' && $graphData['render_to'] !== 'satisfaction_level_trend_line_chart_container')
                                    || $graphData['render_to'] === 'transaction_receipt_line_chart_container'
                                    ) ? (Highcharts.numberFormat(this.y,0)) : (Highcharts.numberFormat(this.y,0) + '%'));
                        }
                    }
                }
            },
            series: $dataArray
        }, function(){
           $(".nav-tabs").on("click", function(){
                lineChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            }); 
           if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;
                
                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                   $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove(); 
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');
                
                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });
        
        //chart.setSize ($graphData['graph_resize_width'], $graphData['graph_resize_height']); 
        var chart_svg = lineChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_line_chart_canvas'); 
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_line_chart_div').append('<img id="'+$graphData['base_id']+'_line_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }
    
 }
 
 function validateSamaFilter($data){
    if($data.start_date == undefined && $data.end_date){
      $('#samaMessageContainerModal .modal-body').html('<div style="height:30px; padding:15px;font-size:20px;" class="alert alert-info">Please Select the Valid Range!</div>');
      $('#samaMessageContainerModal').modal('show'); 
      return false;  
    }else{
      return true;  
    }
 }
 
var drillBarGraph =  function($parentFilterDiv, $graphData){
    var $filterData = getSamaGraphFilterData($parentFilterDiv);
    $requestData = {
     'requestFor'  : 'drill-bar-graph',
     'filterData' : $filterData,
     'graphData'  : $graphData
    }
    $popupObject.setWidth("800px");
    $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);
}

var drillPieChart =  function($parentFilterDiv, $graphData){
    var $filterData = getSamaGraphFilterData($parentFilterDiv);
    $requestData = {
    'requestFor'  : 'drill-pie-chart',
    'filterData' : $filterData,
    'graphData'  : $graphData
    }
    $popupObject.setWidth("800px");
    $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);
}

var drillPieFurther =  function($parentFilterDiv, $graphData){
    var $filterData = getSamaGraphFilterData($parentFilterDiv);
    $requestData = {
    'requestFor'  : 'drill-pie-chart',
    'filterData' : $filterData,
    'graphData'  : $graphData
    }
    $popupObject.setWidth("800px");
    $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);
}

var drillLineChart =  function($parentFilterDiv, $graphData){
    var $filterData = getSamaGraphFilterData($parentFilterDiv);
    $requestData = {
    'requestFor'  : 'drill-line-chart',
    'filterData' : $filterData,
    'graphData'  : $graphData
    }
    $popupObject.setWidth("800px");
    $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);
}

var drillDoubleBarGraph =  function($parentFilterDiv, $graphData){
    var $filterData = getSamaGraphFilterData($parentFilterDiv);
    $requestData = {
    'requestFor'  : 'drill-double-bar-chart',
    'filterData' : $filterData,
    'graphData'  : $graphData
    }
    $popupObject.setWidth("800px");
    $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);
}

var getGraphDrillData = function($requestData){
    $requestObject.initialize($requestData, null,'#ticketDetailPopup');
}

/**
 * @description :: draws bar chart with double bars.
 * 
 */
var doubleBarChart;
var drawDoubleBarChart = function($graphData){
    var chart;
    var categories = [];
    var $dataArray = [];
    var colorIndex = 0;
    var dataSum = 0;
    var $colorArray = ($graphData['legend_color']).split(',');
    var categoriesArray = ($graphData['categories']).split(',');
    if($graphData['data'] != ''){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
        
        $.each(categoriesArray, function($index, $value){
            categories.push($value);
        });
        
        $.each($graphData['data'], function($index, $value){

            data = {
                'name' : $index,
                'data' : $value['count'],
                'color': $value['color']
            }
            $dataArray.push(data);
            
        });

        doubleBarChart = new Highcharts.Chart({
           chart: {
                renderTo : $graphData['render_to'],
                type: 'column'
            },
            title: {
                text: graphHeading,
                useHTML : true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            xAxis: {
                categories: categories,
                labels: {
                rotation: parseInt($graphData['label_rotation'])
                },
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 100,
                allowDecimals: false,
                tickInterval: 20,
                labels: {
                    formatter: function() {
                        return this.value + '%'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} </b>({point.percentage:.0f}%)</td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },credits : {
                enabled :  false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    stacking: 'percent'
                },
                series : {
                    pointWidth: 15,
                    point : {
                        events : {
                            click : function(){
//                                console.log(this.series.name);
                                var selectCategory = this.category;
                                $dataToReturn = {
                                    'render_to'    : $graphData['render_to'],
                                    'category'     : selectCategory,
                                    'question_id'  : $graphData['question_id'],
                                    'category_type': this.series.name,
                                    'question_text' : $graphData['question_text'],
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    allowDecimals: false,
                    borderWidth:0,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                           // return Highcharts.numberFormat(this.y,0) + '%';
                        }
                    }
                }
            },
            series: $dataArray
        }, function(){
             $(".nav-tabs").on("click", function(){
                doubleBarChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
            if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;
                
                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                   $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove(); 
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');
                
                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });

        //chart.setSize ($graphData['graph_resize_width'], $graphData['graph_resize_height']); 
        var chart_svg = doubleBarChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_bar_chart_canvas'); 
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_bar_chart_div').append('<img id="'+$graphData['base_id']+'_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }

 }

var drawKnob = function($graphData){
    var IdArray = ['atm_index_knob', 'pos_index_knob', 'merchant_index_knob']
    if($graphData['data']){
        if($graphData['render_to'] == 'pos_index_knob'){
                $(".pos_total_survey_counter").text($graphData['data']['total_survey']);
                $(".pos_month_survey_counter").text($graphData['data']['month_survey']);

            }
            if($graphData['render_to'] == 'atm_index_knob'){
                $(".atm_total_survey_counter").text($graphData['data']['total_survey']);
                $(".atm_month_survey_counter").text($graphData['data']['month_survey']);

            }
            if($graphData['render_to'] == 'merchant_index_knob'){
                $(".merchant_total_survey_counter").text($graphData['data']['total_survey']);
                $(".merchant_month_survey_counter").text($graphData['data']['month_survey']);

            }

        $(".month_survey_counter").text($graphData['data']['total_month_survey']);
        if($.inArray($graphData['render_to'], IdArray) !== -1){
            $("#"+$graphData['render_to']+"_score").attr("data-tooltip", "Total Survey : "+$graphData['data']['total_survey']+"");
            $("#"+$graphData['render_to']+"_score").text($graphData['data']['score']);
            $("#"+$graphData['render_to']+"_div").html($graphData['data']['html']);
            if($graphData['data']['score'] < 80){
                $("#"+$graphData['render_to']+"_status").html('<i class="fa fa-arrow-down fa-2x text-danger" style="color:#DE5F5F; font-size: 18px; -webkit-transform: rotate(300deg); transform: rotate(300deg);"></i>');
            }else {
                $("#"+$graphData['render_to']+"_status").html('<i class="fa fa-arrow-down fa-2x text-danger" style="color:#9FD687; font-size: 18px; -webkit-transform: rotate(230deg); transform: rotate(230deg);"></i>');
            }
             
        }else{
            $("#"+$graphData['render_to']+"_score").text($graphData['data']['score']+"");
            $("#"+$graphData['render_to']+"_div").html($graphData['data']['html']);
            if($graphData['data']['score'] < '<?php echo $this->getScoreColorCheck();?>'){
                $("#"+$graphData['render_to']+"_status").html('<i class="fa fa-arrow-down fa-2x text-danger" style="color:#DE5F5F; font-size: 18px; -webkit-transform: rotate(300deg); transform: rotate(300deg);"></i>');
            }else {
                $("#"+$graphData['render_to']+"_status").html('<i class="fa fa-arrow-down fa-2x text-danger" style="color:#9FD687; font-size: 18px; -webkit-transform: rotate(190deg); transform: rotate(190deg);"></i>');
            }
        }
//       $("#"+$graphData['render_to']+"_score").text($graphData['data']['score']);
//       $("#"+$graphData['render_to']+"_div").html($graphData['data']['html']);
    }
}

var surveyCounterHtml = function($graphData){
    var IdArray = ['atm_survey_count', 'pos_survey_count', 'merchant_survey_count']
    if( $($graphData['filter_parent_div_class']).find(".ticket_countr_holder")){
        $($graphData['filter_parent_div_class']).find(".ticket_countr_holder").remove();
    }

    if($graphData['filter_parent_div_class'] == '.overall_quality_index_filters'){
        $(".total_survey_counter").text($graphData['survey_count']);
    }else{
        $(".total_survey_counter").html("<p class='ticket_countr_holder' style=float:right;font-size:16px;><b>Number of Surveys Completed : "+$graphData['survey_count']+"</p>");
    }

}


function toggleChart(renderDiv){
   
//    var chart = $('#'+renderDiv).highcharts();
//    var series = chart.series[0];
//    if (series.visible) {
//        series.show();
//        series.update({
//            dataLabels: {
//                enabled: false
//            }
//        });
//    } else {
//        series.show();
//        $('#pos_checkout_lane_type_pie_chart_link').trigger('click');
//        //var counterTopPopup = window.open($(this).prop(baseUrl+baseBank+"/posdashboard/accessLocationDrillIndex.php?drill-for=COUNTERTOP"), '', 'height=500,width=800,margin=100');
////         if (window.focus) 
////         {
////           counterTopPopup.focus();
////         }
////         return false;
////        window.open(baseUrl+baseBank+"/posdashboard/accessLocationDrillIndex.php?drill-for=COUNTERTOP");
//        series.update({
//            dataLabels: {
//                enabled: true
//            }
//        });
//    }
        $requestData = {
            'requestFor' : 'pos-lane-checkout-graph',
            'filterData' : getSamaGraphFilterData(".access_location_index_filters")
        }
        $(pos_checkout_lane_type_pie_chart_container_check).attr('checked', false);
        $popupObject.setWidth("800px");
        $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);
}
var unstackedDoubleBarChart;
var drawUnstackedDoubleBarChart = function($graphData){
    var chart;
    var categories = [];
    var $dataArray = [];
    var colorIndex = 0;
    var dataSum = 0;
    var $colorArray = ($graphData['legend_color']).split(',');
    var categoriesArray = ($graphData['categories']).split('+');
    if($graphData['data'] != ''){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
        
        $.each(categoriesArray, function($index, $value){
            categories.push($value);
        });
        
        $.each($graphData['data'], function($index, $value){
            
            data = {
                'name' : $index,
                'data' : $value['count'],
                'color': $value['color']
            }
            $dataArray.push(data);
            
        });
        unstackedDoubleBarChart = new Highcharts.Chart({
           chart: {
                renderTo : $graphData['render_to'],
                type: 'column'
            },
            title: {
                text: graphHeading,
                useHTML : true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            xAxis: {
                categories: categories,
                labels: {
                rotation: parseInt($graphData['label_rotation'])
                },
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 100,
                allowDecimals: true,
                tickInterval: 20,
                labels: {
                    formatter: function() {
                        return this.value + '%'
                    }
                }
            },
            
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}% </b></td></tr>',
                footerFormat: '</table>',
                
                useHTML: true
            },credits : {
                enabled :  false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series : {
                    point : {
                        events : {
                            click : function(){
//                                console.log(this.series.name);
                                var selectCategory = this.category;
                                $dataToReturn = {
                                    'render_to'    : $graphData['render_to'],
                                    'category'     : selectCategory,
                                    'question_id'  : $graphData['question_id'],
                                    'category_type': this.series.name,
                                    'question_text' : $graphData['question_text'],
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    allowDecimals: false,
                    borderWidth:0,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                           // return Highcharts.numberFormat(this.y,0) + '%';
                        }
                    }
                }
            },
            series: $dataArray
        }, function(){
             $(".nav-tabs").on("click", function(){
                unstackedDoubleBarChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
            if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;
                
                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                   $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove(); 
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');
                
                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });

        //chart.setSize ($graphData['graph_resize_width'], $graphData['graph_resize_height']); 
        var chart_svg = unstackedDoubleBarChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_bar_chart_canvas'); 
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_bar_chart_div').append('<img id="'+$graphData['base_id']+'_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }
}


var stackedBarChart;
var drawStackedBarChart = function($graphData){
    var chart;
    var categories = [];
    var $dataArray = [];
    var colorIndex = 0;
    var dataSum = 0;
    var $colorArray = ($graphData['legend_color']).split(',');
    var categoriesArray = ($graphData['categories']).split(',');
    if($graphData['data'] != ''){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';

        $.each(categoriesArray, function($index, $value){
            categories.push($value);
        });

        $.each($graphData['data'], function($index, $value){
            data = {
                'name' : $index,
                'data' : $value['count'],
                'color': $value['color'],
                'composition': {
                    'avg_speed': $value['avg_speed'] ? $value['avg_speed'] : ''
                }
            }
            $dataArray.push(data);

        });
        //console.log($dataArray);
        stackedBarChart = new Highcharts.Chart({
            chart: {
                renderTo : $graphData['render_to'],
                type: 'column'
            },
            title: {
                text: graphHeading,
                useHTML : true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            xAxis: {
                categories: categories,
                labels: {
                    rotation: parseInt($graphData['label_rotation'])
                },
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 100,
                allowDecimals: false,
                tickInterval: 20,
                labels: {
                    formatter: function() {
                        return this.value + '%'
                    }
                }
            },
            series: $dataArray,
            tooltip: {
                shared: false,
                formatter: function() {
                    var serie = this.series;
                    var percentValue = Number(this.point.percentage.toFixed(2));
                    //console.log(percentValue);
                    //NOTE: may cause efficiency issue when we got lots of points, data in series
                    //should be change from [x, y] to {"x": x, "y": y, "index": index}
                    var index = this.series.data.indexOf(this.point);
                    var s = '<b>' + this.x + '</b><br>';
                    s += '<span style="color:' + serie.color + '">' + serie.options.name + '</span>: <b>' + this.y +'</b>'+ ' (' + percentValue + ')'+ '%' +'<br/>';
                    $.each(serie.options.composition, function(name, value) {
                        s += '<span style="color:' + serie.color + '">' +'Average Transaction Speed (Sec.)'+ '</span>:'  + value[index] + '<br>';
                    });
                    return s;
                }

            },credits : {
                enabled :  false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    stacking: 'percent'
                },
                series : {
                    pointWidth: 15,
                    point : {
                        events : {
                            click : function(){
                                var selectCategory = this.category;
                                $dataToReturn = {
                                    'render_to'    : $graphData['render_to'],
                                    'category'     : selectCategory,
                                    'question_id'  : $graphData['question_id'],
                                    'category_type': this.series.name,
                                    'question_text' : $graphData['question_text'],
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    allowDecimals: false,
                    borderWidth:0,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            // return Highcharts.numberFormat(this.y,0) + '%';
                        }
                    }
                }
            },
            series: $dataArray
        }, function(){
            $(".nav-tabs").on("click", function(){
                stackedBarChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
            if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;

                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                    $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove();
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');

                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });

        //chart.setSize ($graphData['graph_resize_width'], $graphData['graph_resize_height']);
        var chart_svg = stackedBarChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_bar_chart_canvas');
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_bar_chart_div').append('<img id="'+$graphData['base_id']+'_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }



}

var getRejectedSurvey = function($parentFilterDiv, $requestFor){
    var filterData = getSamaGraphFilterData($parentFilterDiv);

    filterQueryString($parentFilterDiv, false); // SETTING FILTERS IN QUERY STRING
    var drillFor = ($("#sama_drill_for").val()) ? $("#sama_drill_for").val() : "";
    if(!validateSamaFilter(filterData)){

    }else{
        $requestData = {
            'requestFor'  : $requestFor,
            'filterData' : filterData,
            'drill-for'   : drillFor
        }
        $(".sama_graph_loader").show();
        $(".calculating_knob_score").text("drilling ..");
        $requestObject.initialize($requestData,'renderRejectedGraph', null);
    }
}

var resetRejectedSurveyFilters = function($parentFilterDiv, $requestFor){
    for(i in $elementList){
        $($parentFilterDiv).find($elementList[i]).each(function(){
            if( $(this).prop("tagName") == "SELECT" ){
                if(userBank){
                    console.log("In bank name")
                }else{
                    $(this).attr('value', '');
                }

            }else{
                console.log("in zero value ");
                $(this).attr('value', '');
            }

        });
    }
    getRejectedSurvey($parentFilterDiv, $requestFor);
}

function renderRejectedGraph($graphData){

    $graphData = JSON.parse($graphData);

// console.log($graphData);
// console.log(($graphData['user_data']).split(','));
    var chart;
    var categories = [];
    var $dataArray = [];
    var colorIndex = 0;
    var dataSum = 0;
    // var $colorArray = ($graphData['legend_color']).split(',');
    $graphData['base_id'] ='total_survey_count';
    $graphData['render_to']='total_survey_count_bar_chart_container';
    $graphData['question_text'] ='Details of Rejected Survey';
    $graphData['request_for'] ='rejected-survey-graph';
    $graphData['render_to'] ='total_survey_count_bar_chart_container';
    var categoriesArray = ($graphData['categories']).split(',');
    if($graphData['data'] != ''){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';

        $.each(categoriesArray, function($index, $value){
           categories.push($value);
        });


        $.each($graphData['data'], function($index, $value){
            data = {
                 'name' : $index,
                'data' : $value['count'],
                'color': $value['color'],
                'user_id':$value['user_id'],
                'id'    :$value['reason_id']

            }
            $dataArray.push(data);

        });
        // console.log($dataArray);
        stackedBarChart = new Highcharts.Chart({
            chart: {
                renderTo : $graphData['render_to'],
                type: 'column'
            },
            title: {
                text: graphHeading,
                useHTML : true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            xAxis: {
                categories: categories,
                labels: {
                    rotation: parseInt($graphData['label_rotation']),
                    rotation: -45
                },
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 100,
                allowDecimals: false,
                tickInterval: 20,
                labels: {
                    formatter: function() {
                       return this.value + '%'
                    }
                }
            },
            series: $dataArray,
            tooltip: {
                shared: false,
                formatter: function() {
                    var serie = this.series;
                    var percentValue = Number(this.point.percentage.toFixed(2));
                    //console.log(percentValue);
                    //NOTE: may cause efficiency issue when we got lots of points, data in series
                    //should be change from [x, y] to {"x": x, "y": y, "index": index}
                    var index = this.series.data.indexOf(this.point);
                    var s = '<b>' + this.x + '</b><br>';
                    s += '<span style="color:' + serie.color + '">' + serie.options.name + '</span>: <b>' + this.y +'</b>'+ ' (' + percentValue + ')'+ '%' +'<br/>';
                   return s;
                }

            },credits : {
                enabled :  false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    stacking: 'percent'
                },
                series : {
                    pointWidth: 15,
                    point : {
                        events : {
                            click : function(){
                                var selectCategory = this.category;

                                $dataToReturn = {
                                    'render_to'    : $graphData['render_to'],
                                    'category'     : selectCategory,
                                    'user_id'      : this.subCategories,
                                    'category_type': this.series.name,
                                    'reason_id'    : this.series.userOptions.id,
                                    'question_text' : $graphData['question_text'],
                                    'request_for'   : $graphData['request_for']
                                }
                                window["drillRejectGraph"]('.rejected_detail_index_filters', $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    allowDecimals: false,
                    borderWidth:0,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            // return Highcharts.numberFormat(this.y);
                        },
                        rotation: -90,
                        allowDecimals: false,
                    }
                }
            },
            series: $dataArray
        }, function(){
            $(".nav-tabs").on("click", function(){
                stackedBarChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });

        });

        //chart.setSize ($graphData['graph_resize_width'], $graphData['graph_resize_height']);
        var chart_svg =stackedBarChart.getSVG();
        // console.log(chart_svg);
        var chart_canvas = document.getElementById('total_rejected_survey_count_bar_chart_canvas');
        canvg(chart_canvas, chart_svg, {scaleWidth: 600, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
       
        $('#total_rejected_survey_count_bar_chart_div').append('<img id="total_survey_count_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }

}

var drillRejectGraph =  function($parentFilterDiv, $graphData){

    var $filterData = getSamaGraphFilterData($parentFilterDiv);
    $requestData = {
        'requestFor'  : 'drill-reject-graph',
        'filterData' : $filterData,
        'graphData'  : $graphData
    }
    $popupObject.setWidth("800px");
    $popupObject.open('#ticketDetailPopup', 'getGraphDrillData', $requestData);
}

var unstackedDoubleBarChart;
var drawUnstackedDoubleBarChartR = function($graphData){
    var chart;
    var categories = [];
    var $dataArray = [];
    var colorIndex = 0;
    var dataSum = 0;
    var $colorArray = ($graphData['legend_color']).split(',');
    var categoriesArray = ($graphData['categories']).split('+');
    if($graphData['data'] != ''){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';

        $.each(categoriesArray, function($index, $value){
            categories.push($value);
        });

        $.each($graphData['data'], function($index, $value){
            data = {
                'name' : $index,
                'data' : $value['count'],
                'color': $value['color']
            }
            $dataArray.push(data);

        });
        unstackedDoubleBarChart = new Highcharts.Chart({
            chart: {
                renderTo : $graphData['render_to'],
                type: 'column'
            },
            title: {
                text: graphHeading,
                useHTML : true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            xAxis: {
                categories: categories,
                labels: {
                    rotation: parseInt($graphData['label_rotation'])
                },
                crosshair: true
            },
            yAxis: {
                min: 0,
                labels: {
                    overflow: 'justify'
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',
                footerFormat: '</table>',

                useHTML: true
            },credits : {
                enabled :  false
            },
            plotOptions: {
                bar: {
                    pointPadding: 0.2,
                    borderWidth:10
                },
                series : {
                    point : {
                        events : {
                            click : function(){
//                                console.log(this.series.name);
                                var selectCategory = this.category;
                                $dataToReturn = {
                                    'render_to'    : $graphData['render_to'],
                                    'category'     : selectCategory,
                                    'question_id'  : $graphData['question_id'],
                                    'category_type': this.series.name,
                                    'question_text' : $graphData['question_text'],
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    allowDecimals: false,
                    borderWidth:0,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            // return Highcharts.numberFormat(this.y,0) + '%';
                        }
                    }
                }
            },
            series: $dataArray
        }, function(){
            $(".nav-tabs").on("click", function(){
                unstackedDoubleBarChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
            if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;

                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                    $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove();
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');

                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });

        //chart.setSize ($graphData['graph_resize_width'], $graphData['graph_resize_height']);
        var chart_svg = unstackedDoubleBarChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_bar_chart_canvas');
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_bar_chart_div').append('<img id="'+$graphData['base_id']+'_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }
}

function getVendorDetails($parentFilterDiv, $requestFor){
 
    var filterData = getSamaGraphFilterData($parentFilterDiv);

    filterQueryString($parentFilterDiv, false); // SETTING FILTERS IN QUERY STRING
    var drillFor = ($("#sama_drill_for").val()) ? $("#sama_drill_for").val() : "";
    if(!validateSamaFilter(filterData)){

    }else{
        $requestData = {
            'requestFor'  : $requestFor,
            'filterData'  : filterData,
            'drill-for'   : drillFor
        }
        $(".sama_graph_loader").show();
        $(".calculating_knob_score").text("drilling ..");
        $requestObject.initialize($requestData,'renderGraph', null);

        $requestData = {
            'requestFor' : 'vendor-details',
            'filterData' : filterData
        }

        $requestObject.initialize($requestData, null, '#vendor_detail_div');

    }
}

function getVendorProfileDetails($parentFilterDiv, $requestFor, $vendor, $surveyId){

    var filterData = getSamaGraphFilterData($parentFilterDiv);

    filterQueryString($parentFilterDiv, false); // SETTING FILTERS IN QUERY STRING
    var drillFor = ($("#sama_drill_for").val()) ? $("#sama_drill_for").val() : "";
    console.log($vendor);
    if(!validateSamaFilter(filterData)){

    }else{
        $requestData = {
            'requestFor'  : $requestFor,
            'filterData'  : filterData,
            'drill-for'   : drillFor,
            'vendor-name' : $vendor,
            'survey-type' : $surveyId
        }
        $(".sama_graph_loader").show();
        $(".calculating_knob_score").text("drilling ..");
        $requestObject.initialize($requestData,'renderGraph', null);

        $requestData = {
            'requestFor' : 'vendor-profile-incident-details',
            'filterData' : filterData,
            'vendor-name' : $vendor,
            'survey-type' : $surveyId
        }
        $requestObject.initialize($requestData, null, '#vendorTicketDetail');
    }
}

function getMailDetails($parentFilterDiv, $requestFor, $vendor, $survey){
    var filterData = getSamaGraphFilterData($parentFilterDiv);
        $requestData = {
            'requestFor' : $requestFor,
            'filterData' : filterData,
            'vendor-name' : $vendor,
            'survey-type' : $survey,
        }
        $requestObject.initialize($requestData, null, '#sama_RequestData_fordownload');
}

var barChartC;
var drawBarChartC = function($graphData){
    $('#'+$graphData['base_id']+'_bar_chart_div').html(); // reseting SVG graph to blank for download option
    var $dataArray  = [];
    var $categories = [];
    var averageValue = 0;
    var $questionText;
    $questionText = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
    $sectorGraphHtml = "";

    if($graphData['data']){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
        var counter    = 0;
        var maxRange   = 100;
        var totalValue = 0;
        var dataSum    = 0;
        var tickInterval = 20;

        $.each($graphData['data'], function($index, $value){
            $categories.push($index);
            totalValue = (totalValue + parseInt($value['count']));
            // if($graphData['render_to'] == "sama_user_login_comparision_bar_chart_container") {
            //     console.log($value['count'])
            //     $data = {
            //         y              : (Math.round((parseInt($value['count'])))),
            //         'color'        : $value['color'],
            //         'survey_count' : $value['count']
            //     }
            //     console.log($data);
            // }else{
                $data = {
                    y              : (Math.round((parseInt($value['count'])))),
                    'name'         : $index,
                    'color'        : $value['color'],
                    'survey_count' : $value['count']
                }
            // }

            if($graphData['request_for'] == 'atm-details'){
                //console.log($value['quality_index_score']);

                $data = {
                    y: $value['quality_index_score'],
                    'color' : $value['color'],
                    'survey_count' :$value['count'],
                    'survey_percent' : (Math.round((parseInt($value['count'])/dataSum) * 100)),
                }
            }

            $dataArray.push($data);
            counter++;
        });

// console.log($dataArray)
        var minimumMarkOnGraphConfig = "";   // FOR INDEX SCORE GRAPHS. ------
        if($graphData['request_for'] == 'overall-quality-index-graph-details' ||  $graphData['request_for'] == 'pos-overall-quality-index-graph-details'){
            minimumMarkOnGraphConfig = [{ value : averageValue,color : 'red',dashStyle : 'shortdash', width : 2,zIndex: 99999,label : {text : averageValue , align: 'right', color: 'red'}}];
        }

        var each = Highcharts.each;
        Highcharts.wrap(Highcharts.seriesTypes.column.prototype, 'drawPoints', function(proceed) {
            var series = this;
            if(series.data.length > 0 ){
                var width = series.barW > series.options.maxPointWidth ? series.options.maxPointWidth : series.barW;
                each(this.data, function(point) {
                    point.shapeArgs.x += (point.shapeArgs.width - width) / 2;
                    point.shapeArgs.width = width;
                });
            }
            proceed.call(this);
        })
//        console.log($data);
        barChartC = new Highcharts.Chart({
            chart: {
                renderTo: $graphData['render_to'],
                type: 'column',
                marginTop: 80
            },

            title: {
                text: graphHeading,
                useHTML : true
            },

            subtitle: {
                text: '<br/><span style="font-size: 15px">'+($graphData['sub_text'] ? $graphData['sub_text']: "") +($graphData['average'] ? $graphData['average'] : "")+'</span>'
            },

            xAxis: {
                categories: $categories,
                labels: {
                    rotation: parseInt($graphData['label_rotation'])
                }
            },

            yAxis: {
                allowDecimals: false,
                title: {
                    text: ''
                },
                tickPositioner: function () {
                    var positions = [],
                        tick = Math.floor(this.dataMin),
                        increment = Math.ceil((this.dataMax - this.dataMin) / 6);

                    if (this.dataMax !== null && this.dataMin !== null) {
                        for (tick; tick - increment <= this.dataMax; tick += increment) {
                            positions.push(tick);
                        }
                    }
                },
                labels: {
                    formatter: function() {
                        var dataVal = this.value;
                        return dataVal+' Hrs';
                    }
                },
                plotLines : minimumMarkOnGraphConfig
            },
            tooltip: {
                pointFormat: (($graphData['request_for'] == 'sama-login-index-graph-detail') ?
                    '<span>{series.name}</span>: <b>{point.y} Hrs</b>' :
                    ($graphData['is_percentage']) ? (($graphData['request_for'] == 'atm-details')
                    ? '<span>Number of Survey</span>: <b>{point.survey_count}</b><br><span>Survey Percent</span>: <b>{point.survey_percent} %</b>' : '<span>{series.name}</span>: <b>{point.survey_count}</b>' ) : '<span>{series.name}</span>: <b>{point.y}</b>')
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: false
                },
                series : {
                    point : {
                        events : {
                            click : function(){
                                var selectCategory = this.category;
                                $dataToReturn = {
                                    'render_to'     : $graphData['render_to'],
                                    'category'      : selectCategory,
                                    'question_id'   : $graphData['question_id'],
                                    'question_text' : $graphData['question_text'] ,
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    borderWidth:0,
                    allowDecimals: false,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                                return Highcharts.numberFormat(this.y,0)+' Hrs';
                        }
                    }
                }
            },
            series: [{
                name: $graphData['legend_text'],
                color: $graphData['legend_color'].split(",")[0],
                maxPointWidth: 50,
                marker: {
                    symbol: 'square'

                },
                data: $dataArray
            }]

        }, function(){
            $(".nav-tabs").on("click", function(){
                barChartC.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
        });

        var chart_svg = barChartC.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_bar_chart_canvas');
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_bar_chart_div').html('<img id="'+$graphData['base_id']+'_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }
}

var lineChartC;
var drawLineChartC = function($graphData){
    // console.log($graphData);
    var chart;
    var categories = [];
    var categoryIndex = [];
    var $dataArray = [];
    var $colorArray = ($graphData['legend_color']).split(',');
    if($graphData['data'] != ''){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
//        console.log($graphData['categories']);
        $.each($graphData['categories'], function($index, $value){
            categories.push($value);
        });

        $.each($graphData['data'], function($index, $value){
            $data = {
                'name' : $index,
                'data' : $value['data'],
                'total_count' :  $value['count']
            }
            $dataArray.push($data);
        });
// console.log($dataArray);
        lineChartC = new Highcharts.Chart({
            chart: {
                renderTo: $graphData['render_to'],
                type: 'line'
            },
            colors: $colorArray,
            title: {
                text: graphHeading,
                useHTML :  true
            },
            subtitle: {
                text: $graphData['sub_text']
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y} Hrs</b>'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: ''
                },
                allowDecimals: false,
                tickPositioner: function () {
                    var positions = [],
                        tick = Math.floor(this.dataMin),
                        increment = Math.ceil((this.dataMax - this.dataMin) / 6);

                    if (this.dataMax !== null && this.dataMin !== null) {
                        for (tick; tick - increment <= this.dataMax; tick += increment) {
                            positions.push(tick);
                        }
                    }
                },
                labels: {
                    formatter: function () {
                        return this.value+' Hrs';
                    }
                },
                min:0
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: true
                },
                series : {
                    point : {
                        events : {
                            click : function(){
                                month         = this.category;
                                answerText    = this.series.name;
                                $dataToReturn = {
                                    'render_to'     : $graphData['render_to'],
                                    'question_id'   : $graphData['question_id'],
                                    'month'         : month,
                                    'answer_text'   : answerText,
                                    'question_text' : $graphData['question_text'],
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    borderWidth:0,
                    allowDecimals: false,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            return ((($graphData['request_for'] === 'satisfaction-level-graph-details' && $graphData['render_to'] !== 'satisfaction_level_trend_line_chart_container')
                                || $graphData['render_to'] === 'transaction_receipt_line_chart_container'
                            ) ? (Highcharts.numberFormat(this.y,0)) : (Highcharts.numberFormat(this.y,0) ));
                        }
                    }
                }
            },
            series: $dataArray
        }, function(){
            $(".nav-tabs").on("click", function(){
                lineChartC.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
            if(parseInt($graphData['has_ticket'])){
                var queryString = filterQueryString($graphData['filter_parent_div_class'], true);
                if(queryString)
                    queryString = "&"+queryString;
                $url = baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($("#"+$graphData['render_to']+" .highcharts-title").text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text())+queryString;

                if($("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link")){
                    $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon .ticket_link").remove();
                }
                $("#"+$graphData['render_to']).parent(".box-content").parent('.box').find(".box-header .box-icon").prepend('<a target="_blank" href="'+$url+'" class="btn btn-round btn-info ticket_link" title="view tickets"><i class="fa fa-list"></i></a>');

                $("#"+$graphData['render_to']+" .highcharts-title").on("click", function(){
                    window.open(baseUrl+baseBank+"/SamaHelper/ticketPanelSama.php?question_name="+encodeURIComponent($(this).text())+"&question_id="+$graphData['question_id']+"&header_text="+encodeURIComponent($(".sama_breadcum").text()), "_blank");
                });
            }
        });

        //chart.setSize ($graphData['graph_resize_width'], $graphData['graph_resize_height']);
        var chart_svg = lineChartC.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_line_chart_canvas');
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_line_chart_div').append('<img id="'+$graphData['base_id']+'_line_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }
}

var vendorBarChart;
var drawVendorBarChart = function($graphData){
    $('#'+$graphData['base_id']+'_bar_chart_div').html(); // reseting SVG graph to blank for download option
    var $dataArray  = [];
    var $categories = [];
    var averageValue = 0;
    var $questionText;
    if($graphData['render_to'] == 'pos_average_transaction_time_bar_chart_container'){
        $questionText = '<span style="cursor:pointer;">'+$graphData['question_text']+'<br/></span><span style="color:forestgreen;">'+'Average Transaction Speed :'+$graphData['average']+'</span>';
    }else{
        $questionText = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
    }

    $sectorGraphHtml = "";
    if($graphData['render_to'] == "segmentation_compliance_by_sector_bar_chart_container"){
        $sectorGraphHtml = " <table style='width:100%;'><tr>";
    }


    if($graphData['data']){
        var graphHeading = '<span style="cursor:pointer;">'+$graphData['question_text']+'</span>';
        var counter    = 0;
        var maxRange   = 100;
        var totalValue = 0;
        var dataSum    = 0;
        if($graphData['request_for'] !== 'overall-quality-index-graph-details'){
            $.each($graphData['data'], function($index, $value){
                dataSum = (dataSum + parseInt($value['count']));
            });
        }
        $.each($graphData['data'], function($index, $value){
            $categories.push($index);
            totalValue = (totalValue + parseInt($value['count']));
           if($graphData['render_to'] == 'vendor_performance_bar_chart_container'){
                $data = {
                    //y: (((parseInt($value['count'])/dataSum) * 100)),
                    y: $value['count'],
                    'color' : $value['color'],
                    'survey_count' :$value['total_survey']
                }
            }else{
                $data = {
                    y              : (Math.round((parseInt($value['count'])/dataSum) * 100)),
                    'color'        : $value['color'],
                    'survey_count' : $value['count']
                }
            }

            $dataArray.push($data);
            counter++;
        });


        if($graphData['request_for'] == 'overall-quality-index-graph-details' || $graphData['request_for'] == 'pos-overall-quality-index-graph-details') {
            averageValue = Number((totalValue/ $dataArray.length).toFixed(2));
        }else {
            averageValue = Number((100 / $dataArray.length).toFixed(2));
        }


        var minimumMarkOnGraphConfig = "";   // FOR INDEX SCORE GRAPHS. ------
        // if($graphData['request_for'] == 'overall-quality-index-graph-details' ||  $graphData['request_for'] == 'pos-overall-quality-index-graph-details'){
        //     minimumMarkOnGraphConfig = [{ value : averageValue,color : 'red',dashStyle : 'shortdash', width : 2,zIndex: 99999,label : {text : averageValue , align: 'right', color: 'red'}}];
        // }


        // if($graphData['render_to'] == "segmentation_compliance_by_sector_bar_chart_container"){
        //     $sectorGraphHtml += "</tr></table>";
        //     $("#segmentation_compliance_by_sector_legend_holder").html($sectorGraphHtml);
        // }
        var each = Highcharts.each;
        Highcharts.wrap(Highcharts.seriesTypes.column.prototype, 'drawPoints', function(proceed) {
            var series = this;
            if(series.data.length > 0 ){
                var width = series.barW > series.options.maxPointWidth ? series.options.maxPointWidth : series.barW;
                each(this.data, function(point) {
                    point.shapeArgs.x += (point.shapeArgs.width - width) / 2;
                    point.shapeArgs.width = width;
                });
            }
            proceed.call(this);
        })
        vendorBarChart = new Highcharts.Chart({
            chart: {
                renderTo: $graphData['render_to'],
                type: 'column',
                marginTop: 80
            },

            title: {
                text: graphHeading,
                useHTML : true
            },

            subtitle: {
                text: '<br/><span style="font-size: 15px">'+($graphData['sub_text'] ? $graphData['sub_text']: "") +($graphData['average'] ? $graphData['average'] : "")+'</span>'
            },

            xAxis: {
                categories: $categories,
                labels: {
                    rotation: parseInt($graphData['label_rotation'])
                }
            },

            yAxis: {
                min: 0,
                max: maxRange,
                allowDecimals: false,
                title: {
                    text: ''
                },
                tickInterval: 20,
                labels: {
                    formatter: function() {
                        var dataVal = this.value;
                        return (($graphData['request_for'] == 'overall-quality-index-graph-details'|| $graphData['request_for'] == 'pos-overall-quality-index-graph-details') ? (dataVal) : (dataVal + '%'));
                    }
                },
                plotLines : minimumMarkOnGraphConfig
            },
            tooltip: {
                pointFormat: ($graphData['is_percentage']) ? (($graphData['request_for'] == 'vendor-quality-index-graph-details')
                    ? '<span>Number of Survey</span>: <b>{point.survey_count}</b>' : '<span>{series.name}</span>: <b>{point.survey_count}</b>' ) : '<span>{series.name}</span>: <b>{point.y}</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: false
                },
                series : {
                    point : {
                        events : {
                            click : function(){
                                var selectCategory = this.category;
                                $dataToReturn = {
                                    'render_to'     : $graphData['render_to'],
                                    'category'      : selectCategory,
                                    'question_id'   : $graphData['question_id'],
                                    'question_text' : $graphData['question_text'] ,
                                    'request_for'   : $graphData['request_for']
                                }
                                window[$graphData['drill_function']]($graphData['filter_parent_div_class'], $dataToReturn);
                            }
                        }
                    },
                    shadow:false,
                    borderWidth:0,
                    allowDecimals: false,
                    dataLabels:{
                        enabled:true,
                        formatter:function() {
                            if(parseInt(this.y) > 95){
                                return '<span style="color:white;">'+(($graphData['request_for'] == 'overall-quality-index-graph-details' || $graphData['request_for'] == 'pos-overall-quality-index-graph-details')
                                        ? (Highcharts.numberFormat(this.y,0)) : (($graphData['request_for'] == 'atm-details'    ) ? (Highcharts.numberFormat(this.y,2)) : (Highcharts.numberFormat(this.y,0) + '%')))+'</span>';
                            } else {
                                return (($graphData['request_for'] == 'overall-quality-index-graph-details' || $graphData['request_for'] == 'pos-overall-quality-index-graph-details') ? (Highcharts.numberFormat(this.y,0)) : (
                                    ($graphData['render_to'] == 'pos_average_transaction_time_bar_chart_container' || $graphData['render_to']=='vendor_performance_bar_chart_container' ) ? (Highcharts.numberFormat(this.y,2) + '%') :
                                        (($graphData['request_for'] == 'atm-details') ? (Highcharts.numberFormat(this.y,2)) : (Highcharts.numberFormat(this.y,0) + '%'))));
                            }

                        }
                    }
                }
            },
            series: [{
                name: $graphData['legend_text'],
                color: $graphData['legend_color'].split(",")[0],
                maxPointWidth: 50,
                marker: {
                    symbol: 'square'

                },
                data: $dataArray
            }]

        }, function(){
            $(".nav-tabs").on("click", function(){
                vendorBarChart.setSize($("#"+$graphData['render_to']).width(), $graphData['graph_resize_height']);
            });
        });

        var chart_svg = vendorBarChart.getSVG();
        var chart_canvas = document.getElementById($graphData['base_id']+'_bar_chart_canvas');
        canvg(chart_canvas, chart_svg, {scaleWidth: 512, scaleHeight: 400});
        var chart_img = chart_canvas.toDataURL('image/png');
        $('#'+$graphData['base_id']+'_bar_chart_div').html('<img id="'+$graphData['base_id']+'_bar_chart_image" src="' + chart_img + '" />');
    }else{
        $("#"+$graphData['render_to']).html('<div align="center" style="font-size:15px;font-weight:bold;color:#6383C4;min-height: 250px;margin-top: 150px;">No data found within selected filters</div> ');
    }
}
