var bankCode,bankSurveyDate,bankLatitude,bankLongitude,$urlToSendReq;
$(document).ready(function(){

    $(".scrollbar").height(360);
    $(".viewport").height(360);
    $(".scrollbar").show();
    $('#scrollbar1').tinyscrollbar({
        sizethumb: 40
    });  
    $urlToSendReq = '4c360db.php'
    var where = "'Name'+=+'Riyad Bank'+AND+'BranchCode'+=+'"+branchCode+"'+AND+'Type'+=+'"+type+"'";
    var url = "https://www.google.com/fusiontables/api/query?sql=SELECT+BranchName,Location,Province,City,Image,BranchType,IsAffluent,IsPrivateBanking,IsTahweel+FROM+"+fusion_key+"+";
    if (where != null && where != undefined && where != "") 
        url += "WHERE+" + where+"+";
    url += "&jsonCallback=?";
    $foundFlag = false;
    
/*
* Check if this branch exists in Gis data
* Returns Branch Data if found in Gis
*/
    $.getJSON(url,function(data){   
        var fusionRows = data.table.rows; 
        $foundType = branchType;
        $foundImage = false;
        for(i in fusionRows){
            if(fusionRows[i][0]){
                $address = '';
                if(fusionRows[i][0]){
                    $address += fusionRows[i][0];
                }
                if(fusionRows[i][2]){
                    if($address){
                        $address += ", ";
                    }
                    $address += fusionRows[i][2];
                }
                if(fusionRows[i][3]){
                    if($address){
                        $address += ", ";
                    }
                    $address += fusionRows[i][2];
                }
                $("#branchName").html(branchProfileDisplayName(fusionRows[i][0], fusionRows[i][2], fusionRows[i][3]));
                var latLng =(fusionRows[i][1]).split(',');
                bankLatitude = latLng[0];
                bankLongitude = latLng[1]; 
                $branchType = [];

                if(fusionRows[i][5]){
                    if(fusionRows[i][5].toUpperCase() == 'GENTS')
                        $branchType.push('Men');
                    else if(fusionRows[i][5].toUpperCase() == 'GENTS AND LADIES')                  
                        $branchType.push('Men and Ladies');                  
                }
                if(fusionRows[i][6] == 'Y')
                    $branchType.push('Affluent');
                if(fusionRows[i][7] == 'Y')
                    $branchType.push('Private Banking');
                if(fusionRows[i][8] == 'Y')
                    $branchType.push('Tahweel');
                
                //Creates Marker on my map
                createMarker(new google.maps.LatLng(bankLatitude,bankLongitude),fusionRows[i][4],$branchType);
                $foundFlag = true;
                
                //ATM Image from Gis Data
                $foundImage = fusionRows[i][4];
            }
        }
        getBranchData($foundFlag);
      
    });          
});


/*
 * Gets The Branch data for the given code and type *  
 * @param $foundFlag type Boolean;
 * return : Health Metrics for Riyad ATM
 */
function getBranchData($foundFlag)
{   
    console.log($foundFlag);
    $.post($urlToSendReq,{
        'action'    :'branchInfo',
        'branchCode':branchCode,
        'branchType':branchType,
        'type'      :type
    },function(result){
        result = JSON.parse(result);
        if(result.found){
            if($foundFlag){
                bankLatitude = result.latitude;
                bankLongitude = result.longitude;
                $foundFlag = true;
            } else {
                createMarker(new google.maps.LatLng(24.570855,46.911621),'',branchType);
            }
            
            getHealthReport();
        }else{
            createMarker(new google.maps.LatLng(24.570855,46.911621),'',branchType);
            $("#scorecard_div").html('<div align="center" style="font-size:30px;font-weight:bold;color:red;height:133px;margin-top:100px;">nothing found!</div>');
            $.post('nothingFound_ATM.php',{
                'type':'data'
            },function(result){
                $("#healthReport").html(result);
                gaugeValue =  0;
                drawGauge();
                $("#galleryImages").html("<div align='center'><span style='width:70%;text-align:left' class='green'><b>Nothing found</b></span></div>");
            });
        }
    });
}
    

/*
 * Generates Health report   
 * Uses Global variable for this branch
 */
function getHealthReport()
{
    $.post($urlToSendReq,{
        'action'      :'data',
        'branchCode':branchCode,
        'branchType':branchType, 
        'survey_id' :survey_id,
        'type'      :type         
    },function(result){
        result = JSON.parse(result);
        $("#healthReport_metrics").html(result.content);
        $("#scorecard_div").html(result.scorecard_div);
        
        gaugeValue =  Number(result.pending);  
        drawGauge();
        initializePopup();
        if(result.hasImage){
            getGalleryImages();
        }else{
            $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');
        }

        $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');                    
        
    })
}
function getGalleryImages(){
    $.post($urlToSendReq, {
        'action'        :'images',
        'branchCode'    :branchCode,
        'branchType'    :branchType,
        'branch_id'     :branch_id,
        'survey_id'     :survey_id,
        'type'          :type     
    },function(result){
        $("#galleryImages").html(result);
        //$('[rel="popupform"],[data-rel="popupform"]').popupform();
        //gallery controlls container animation
                	
        if(isTouchDevice){
            $('.openSample a').colorbox({
                inline:true,
                rel:'openSample a', 
                transition:"elastic", 
                maxWidth:"95%", 
                maxHeight:"95%"
            },function(){//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');
                });
        }else{
            $('.thumbnails div.img_popup').colorbox({
                inline:true,
                rel:'thumbnails div.img_popup', 
                transition:"elastic",
                title:false, 
                maxWidth:"100%", 
                maxHeight:"100%"
            },function(){//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');     
                initializeScroll();
            });
        }
    });
}
    
$(document).ready(function(){   
    $('.thumbnails div.img_popup').live('click',function(){           
        loadImageComments(this);
    }); 
    $('#healthReport_metrics .img_popup').live('click',function(){ 
        $('#galleryTab').trigger('click');
        var image_id = $(this).attr('image_id');
        if($('.thumbnails div.img_popup').each(function(){
            if($(this).attr('image_id') == image_id){
                $(this).trigger('click');
            }
        }));
    }); 
});

function loadImageComments(thisone){
    var image_id = $(thisone).attr('image_id');
    var ticket_number = $(thisone).attr('ticket');
    var img_path = $(thisone).find('img').attr('src');
    loadComments(image_id, ticket_number, img_path);
}

function initializePopup(){
    $('.dashboard-list a.img_popup').colorbox({
        inline:true,
        rel:'dashboard-list a.img_popup', 
        transition:"elastic",
        title:false, 
        maxWidth:"100%", 
        maxHeight:"100%"
    },function(){//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');     
        initializeScroll();
    });
}