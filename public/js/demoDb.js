var bankCode, bankSurveyDate, bankLatitude, bankLongitude;
$(document).ready(function() {
    createMyMap();
    function createMyMap() {
        var fusionRows = new Array();
        fusionRows[0] = new Array();
        fusionRows[0][0] = "MAKKAH MAIN";
        fusionRows[0][1] = "21.4299557,39.8289439";
        fusionRows[0][2] = "Makkah";
        fusionRows[0][3] = "Mecca";
        fusionRows[0][4] = "../img/uploads/x_image.png";
        fusionRows[0][5] = "Gents";
        fusionRows[0][6] = "";
        fusionRows[0][7] = "";
        fusionRows[0][8] = "";
        $foundType = branchType;
        $foundImage = false;
        for (i in fusionRows) {
            if (fusionRows[i][0]) {
                $address = '';
                if (fusionRows[i][0]) {
                    $address += fusionRows[i][0];
                }
                if (fusionRows[i][2]) {
                    if ($address) {
                        $address += ", ";
                    }
                    $address += fusionRows[i][2];
                }
                if (fusionRows[i][3]) {
                    if ($address) {
                        $address += ", ";
                    }
                    $address += fusionRows[i][2];
                }
                $("#branchName").html($address);
                var latLng = (fusionRows[i][1]).split(',');
                bankLatitude = latLng[0];
                bankLongitude = latLng[1];
                $branchType = [];

                if (fusionRows[i][5]) {
                    if (fusionRows[i][5].toUpperCase() === 'GENTS')
                        $branchType.push('Men');
                    else if (fusionRows[i][5].toUpperCase() === 'GENTS AND LADIES')
                        $branchType.push('Men and Ladies');
                }
                if (fusionRows[i][6] === 'Y')
                    $branchType.push('Affluent');
                if (fusionRows[i][7] === 'Y')
                    $branchType.push('Private Banking');
                if (fusionRows[i][8] === 'Y')
                    $branchType.push('Tahweel');
                createMarker(new google.maps.LatLng(bankLatitude, bankLongitude), fusionRows[i][4], $branchType);
                $foundFlag = true;
                $foundImage = fusionRows[i][4];
                $foundType = $branchType;
            }
        }
        checkBankData($foundImage, $foundType, $foundFlag);
    }


    function checkBankData(image, currentBrType, foundFlag) {
        $.post('databaseRequest.php', {
            'type': 'branchInfo',
            'branchCode': branchCode,
            'branchType': branchType
        }, function(result) {
            result = JSON.parse(result);
            if (result.found) {
                if ($foundFlag) {
                    // $("#branchName").html(result.branch_name);
                    bankLatitude = result.latitude;
                    bankLongitude = result.longitude;
                    $foundFlag = true;
                    //createMarker(new google.maps.LatLng(bankLatitude,bankLongitude),image,currentBrType);
                } else {
                    createMarker(new google.maps.LatLng(24.570855, 46.911621), image, currentBrType);
                }
                getHealthReport();
            } else {
                createMarker(new google.maps.LatLng(24.570855, 46.911621), image, currentBrType);
                $("#scorecard_div").html('<div align="center" style="font-size:30px;font-weight:bold;color:red;height:133px;margin-top:100px;">nothing found!</div>');
                $.post('nothingFound.php', {
                    'type': 'data'
                }, function(result) {
                    $("#healthReport").html(result);
                    gaugeValue = 0;
                    drawGauge();
                    $("#galleryImages").html("<div align='center'><span style='width:70%;text-align:left' class='green'><b>Nothing found</b></span></div>");
                });
            }
        });
    }

    function addTour() {
        if ($('.tourindex').length && typeof(tour) == 'undefined') {
            var tour = new Tour();
            tour.addStep({
                element: "#tour-healtReportToolTip",
                placement: "top",
                title: "<h3 style='color:#FFFFFF;'>Health Report</h3>",
                content: "Red arrows indicate there is a problem. By click on it you can see the problem, share it and collaborate in real time with all parties concerned to make sure this is fixed the soonest possible."
            });
            tour.addStep({
                element: "#tour-map",
                placement: "top",
                title: "<h3 style='color:#FFFFFF;'>My Map</h3>",
                content: "Shows a particular branch on map with branch type"
            });



            tour.addStep({
                element: "#tour-scorecard",
                placement: "top",
                title: "<h3 style='color:#FFFFFF;'>Score Card</h3>",
                content: "Based on the Metrics agreed and the weight allocated for each, the systemwill generate a score for each and every branch."
            });

            tour.addStep({
                element: "#tour-deptproblem-index",
                placement: "top",
                title: "<h3 style='color:#FFFFFF;'>Problems by Department</h3>",
                content: "Track progress and identify bottlenecks. See how many problems have been resolved and how many more are pending, by department, lightning-fast insights without the complexity to effectively manage associated risks."
            });

            tour.addStep({
                element: "#tour-gaugemap",
                placement: "top",
                title: "<h3 style='color:#FFFFFF;'>Pending issues</h3>",
                content: "Shows no of pending issues through gauge meter."
            });


            tour.addStep({
                element: "#tour-mothlyscore",
                placement: "top",
                title: "<h3 style='color:#FFFFFF;'>Monthly Score</h3>",
                content: "Shows monthly score of each branch through bar chart."
            });

            tour.restart();
        }
    }


    function getHealthReport() {
        $.post('databaseRequest.php', {
            'type': 'data',
            'branchCode': branchCode,
            'branchType': branchType

        }, function(result) {
            result = JSON.parse(result);
            $("#healthReport").html(result.content);
            $("#scorecard_div").html(result.scorecard_div);
            addTour();
            if (result.code) {
                bankCode = result.code;
                bankSurveyDate = result.date;
                gaugeValue = result.pending;

                drawGauge();
                initializePopup();
                if (result.hasImage) {
                    getGalleryImages();
                } else {
                    $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');
                }
            } else {
                $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');

            }

            $('[data-rel="tooltip-modify"]').tooltip({
                "placement": "bottom",
                "template": '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner-modify"></div></div>',
                delay: {
                    show: 400,
                    hide: 200
                },
                modified: 1
            });
        })
    }
    
    function getGalleryImages() {
        $.post('databaseRequest.php', {
            'type': 'images',
            'bankCode': bankCode,
            'branchType': branchType,
            'bankSurveyDate': bankSurveyDate
        }, function(result) {
            $("#galleryImages").html(result);
            //$('[rel="popupform"],[data-rel="popupform"]').popupform();
            //gallery controlls container animation

            if ($('ul.gallery').attr('tooltip') == 'true') {
                $('ul.gallery li').hover(function() {
                    $('img', this).fadeToggle(1000);
                    $(this).find('.gallery-controls').remove();
                    $(this).append('<div class="well gallery-controls">' +
                            '<p><a href="#" class="gallery-edit btn"><i class="icon-edit"></i></a> <a href="#" class="gallery-delete btn"><i class="icon-eye-open"></i></a></p>' +
                            '</div>');
                    $(this).find('.gallery-controls').stop().animate({
                        'margin-top': '-1'
                    }, 400, 'easeInQuint');
                }, function() {
                    $('img', this).fadeToggle(1000);
                    $(this).find('.gallery-controls').stop().animate({
                        'margin-top': '-30'
                    }, 200, 'easeInQuint', function() {
                        $(this).remove();
                    });
                });
            }

            //gallery delete
            $('.thumbnails').on('click', '.gallery-delete', function(e) {
                e.preventDefault();
            });
            //gallery edit
            $('.thumbnails').on('click', '.gallery-edit', function(e) {
                e.preventDefault();
            });

            if (isTouchDevice) {
                $('.openSample a').colorbox({
                    inline: true,
                    rel: 'openSample a',
                    transition: "elastic",
                    maxWidth: "95%",
                    maxHeight: "95%"
                }, function() {//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');
                });
            } else {
                $('.thumbnails div.img_popup').colorbox({
                    inline: true,
                    rel: 'thumbnails div.img_popup',
                    transition: "elastic",
                    title: false,
                    maxWidth: "100%",
                    maxHeight: "100%"
                }, function() {//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');     
                    initializeScroll();
                });
            }
        });
    }

});
var prev_popup = null;

$(document).ready(function() {
    $('.thumbnails div.img_popup').live('click', function() {
        loadImageComments(this);
    });
    $('.dashboard-list a.img_popup').live('click', function() {
        var href = $(this).attr('href');
        var has_photoproof = $(href).find('.issue_img').attr('photoproof');

        if (has_photoproof != 0) {
            $('#galleryTab').trigger('click');
            loadImageComments(this);
        } else {
            $(href).find('.issue_img').attr('src', '../img/img_issue_dummy.png');
            loadImageComments(this);
        }
    });
});

function loadImageComments(thisone) {
    if (prev_popup) {
        prev_popup.html('');
    }
    prev_popup = $('#comments_' + $(thisone).attr('data-content'));

    var comments = loadComments($(thisone).attr('data-content'));
    //console.log('#comments_'+$(thisone).attr('data-content'));
    $('#comments_' + $(thisone).attr('data-content')).html(comments);
}

function initializePopup() {
    $('.dashboard-list a.img_popup').colorbox({
        inline: true,
        rel: 'dashboard-list a.img_popup',
        transition: "elastic",
        title: false,
        maxWidth: "100%",
        maxHeight: "100%"
    }, function() {
        initializeScroll();
    });
}