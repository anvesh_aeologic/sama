var bankCode, bankSurveyDate, bankLatitude, bankLongitude;

$(document).ready(function() {
    var where = " Where Name = 'Al-Rajhi Bank' AND BranchCode='" + branchCode + "' AND Type='Bank' ";

    fusionTable('SELECT BranchName,Location,Province,City,Image,BranchType,IsAffluent,IsPrivateBanking,IsTahweel FROM ', where, ' ORDER BY BranchCode ASC', 'executeFusionDataOnProfile');
});
function executeFusionDataOnProfile(data) {

    $foundFlag = false;    
    var fusionRows = [];
    if(data.rows)
        fusionRows = data.rows;
    
    var markerImage = 'img/Icon_1.png';
    $foundType = branchType;
    $foundImage = false;
    for (i in fusionRows) {
        if (fusionRows[i][0]) {
            $address = '';
            $("#branchName").html(branchProfileDisplayName(fusionRows[i][0], fusionRows[i][2], fusionRows[i][3]));
            var latLng = (fusionRows[i][1]).split(',');
            bankLatitude = latLng[0];
            bankLongitude = latLng[1];
            $branchType = [];

            if (fusionRows[i][5]) {
                if (fusionRows[i][5].toUpperCase() === 'GENTS')
                    $branchType.push('Men');
                else if (fusionRows[i][5].toUpperCase() === 'LADIES')
                    $branchType.push('Ladies');
                else if (fusionRows[i][5].toUpperCase() === 'GENTS AND LADIES')
                    $branchType.push('Men and Ladies');
            }
            if (fusionRows[i][6] === 'Y')
                $branchType.push('Affluent');
            if (fusionRows[i][7] === 'Y')
                $branchType.push('Private Banking');
            if (fusionRows[i][8] === 'Y') {
                $branchType.push('Tahweel');
                markerImage = 'img/Alrajhi-Tahweel.png';
            }
            createMarker(new google.maps.LatLng(bankLatitude, bankLongitude), fusionRows[i][4], $branchType, markerImage);
            $foundFlag = true;
            $foundImage = fusionRows[i][4];
            $foundType = $branchType;
        }
    }
    checkBankData($foundImage, $foundType, $foundFlag, markerImage);
}


    function checkBankData(image, currentBrType, foundFlag, markerImage) {
        $.post('databaseRequest.php', {
            'type': 'branchInfo',
            'branchCode': branchCode,
            'branchType': branchType
        }, function(result) {
            result = JSON.parse(result);
            if (result.found) {
                if ($foundFlag) {
                    // $("#branchName").html(result.branch_name);
                    bankLatitude = result.latitude;
                    bankLongitude = result.longitude;
                    $foundFlag = true;
                    //createMarker(new google.maps.LatLng(bankLatitude,bankLongitude),image,currentBrType);
                } else {
                    createMarker(new google.maps.LatLng(24.570855, 46.911621), image, currentBrType, markerImage);
                }
                getHealthReport();
            } else {
                createMarker(new google.maps.LatLng(24.570855, 46.911621), image, currentBrType, markerImage);
                $("#scorecard_div").html('<div align="center" style="font-size:30px;font-weight:bold;color:red;height:133px;margin-top:100px;">nothing found!</div>');
                $.post('nothingFound.php', {
                    'type': 'data'
                }, function(result) {
                    $("#healthReport").html(result);
                    gaugeValue = 0;
                    drawGauge();
                    $("#galleryImages").html("<div align='center'><span style='width:70%;text-align:left' class='green'><b>Nothing found</b></span></div>");
                });
            }
        });
    }

    function getHealthReport() {
        $.post('databaseRequest.php', {
            'type': 'data',
            'branchCode': branchCode,
            'branchType': branchType

        }, function(result) {
            result = JSON.parse(result);
            $("#healthReport").html(result.content);
            $("#scorecard_div").html(result.scorecard_div);
            if (result.code) {
                bankCode = result.code;
                bankSurveyDate = result.date;
                gaugeValue = result.pending;

                drawGauge();
                initializePopup();
                if (result.hasImage) {
                    getGalleryImages();
                } else {
                    $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');
                }
            } else {
                $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');

            }
        })
    }
    function getGalleryImages() {
        $.post('databaseRequest.php', {
            'type': 'images',
            'bankCode': bankCode,
            'bankSurveyDate': bankSurveyDate,
            'branchType': branchType
        }, function(result) {
            $("#galleryImages").html(result);
            //$('[rel="popupform"],[data-rel="popupform"]').popupform();
            //gallery controlls container animation

            if ($('ul.gallery').attr('tooltip') == 'true') {
                $('ul.gallery li').hover(function() {
                    $('img', this).fadeToggle(1000);
                    $(this).find('.gallery-controls').remove();
                    $(this).append('<div class="well gallery-controls">' +
                            '<p><a href="#" class="gallery-edit btn"><i class="icon-edit"></i></a> <a href="#" class="gallery-delete btn"><i class="icon-eye-open"></i></a></p>' +
                            '</div>');
                    $(this).find('.gallery-controls').stop().animate({
                        'margin-top': '-1'
                    }, 400, 'easeInQuint');
                }, function() {
                    $('img', this).fadeToggle(1000);
                    $(this).find('.gallery-controls').stop().animate({
                        'margin-top': '-30'
                    }, 200, 'easeInQuint', function() {
                        $(this).remove();
                    });
                });
            }

            //gallery delete
            $('.thumbnails').on('click', '.gallery-delete', function(e) {
                e.preventDefault();
            });
            //gallery edit
            $('.thumbnails').on('click', '.gallery-edit', function(e) {
                e.preventDefault();
            });

            if (isTouchDevice) {
                $('.openSample a').colorbox({
                    inline: true,
                    rel: 'openSample a',
                    transition: "elastic",
                    maxWidth: "95%",
                    maxHeight: "95%"
                }, function() {//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');
                });
            } else {
                $('.thumbnails div.img_popup').colorbox({
                    inline: true,
                    rel: 'thumbnails div.img_popup',
                    transition: "elastic",
                    title: false,
                    maxWidth: "100%",
                    maxHeight: "100%"
                }, function() {//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');     
                    initializeScroll();
                });
            }
        });
    }

var prev_popup = null;

$(document).ready(function() {
//    $('.thumbnails div.img_popup').live('click', function() {
//        loadImageComments(this);
//    });
    $('.dashboard-list a.img_popup').live('click', function() {
        var href = $(this).attr('href');
        var has_photoproof = $(href).find('.issue_img').attr('photoproof');

        if (has_photoproof != 0) {
            $('#galleryTab').trigger('click');
            loadImageComments(this);
        } else {
            $(href).find('.issue_img').attr('src', '../img/AlRajhiProfileImage.png');
            loadImageComments(this);
        }
    });
});
function loadImageComments(thisone) {
    alert(thisone);
    if (prev_popup) {
        prev_popup.html('');
    }
    prev_popup = $('#comments_' + $(thisone).attr('data-content'));
alert(prev_popup);
    var comments = loadComments($(thisone).attr('data-content'));
    alert(comments);
    //console.log('#comments_'+$(thisone).attr('data-content'));
    $('#comments_' + $(thisone).attr('data-content')).html(comments);
}

function initializePopup() {
    $('.dashboard-list a.img_popup').colorbox({
        inline: true,
        rel: 'dashboard-list a.img_popup',
        transition: "elastic",
        title: false,
        maxWidth: "100%",
        maxHeight: "100%"
    }, function() {
        initializeScroll();
    });
}