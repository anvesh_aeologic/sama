function MapCreator(options) {
    this.markerArray = [];
    this.map = null;
    this.clusterOptions = {styles: styles[0]};
    this.heatmap = null;
    this.heatmapData = [];
    this.mapContainer = options.mapContainer;
    this.autoCompleteInput = options.autoCompleteInput;
    this.geoCoder = new google.maps.Geocoder();
    this.cityCircle = null;
    this.oms = null;
    /*this.infowindow = new google.maps.InfoWindow({
     content: ""
     });*/
    this.infowindow = null;
    this.centerMarker = null;

    this.draggedPosition = null;
    this.selectedSearch = "radius";

    this.createMap = function () {
        var me = this;
        var mapOptions = {
            center: new google.maps.LatLng(22.268763552489748, 47.87841796875),
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            }
        };
        var filterHtml = document.getElementById('bank_filters');

        this.map = new google.maps.Map(document.getElementById(this.mapContainer), mapOptions);
        mapchange = this.map;
        changeIsrailLayer(this.map.getZoom(), this.map);
        this.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(filterHtml);

        this.oms = new OverlappingMarkerSpiderfier(this.map); //Spiderfied it here

        this.infowindow = new google.maps.InfoWindow();
        me.oms.addListener('click', function(marker, event) {
            me.infowindow.setContent(marker.infoContent);
            me.infowindow.open(map, marker);
        });

    };

    this.autoComplete = function(){
        map = this.map;
        var autoCompleterOptions = {
            types: ['(regions)'],
            componentRestrictions: {country: 'sa'}
        };
        var autocomplete = new google.maps.places.Autocomplete(this.autoCompleteInput, autoCompleterOptions);
        autocomplete.bindTo('bounds', this.map);
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: this.map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {

        });
    };

    this.geocodeAddress = function(address, callbackFunction){
        var me = this;
        map = this.map;
        geococder = this.geoCoder;
        markerArray = this.markerArray;

        var data = (this.draggedPosition) ? {'location' : this.draggedPosition} : {'address': address+"saudi arabia"};

        geococder.geocode(data, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                $("#map-data-go").find("i").attr('class', 'fa fa-search');
                console.log(results[0].formatted_address);
                var editedAddress = "";
                if(me.draggedPosition){
                    var draggedAddress = "";
                    $.each((results[0].formatted_address).split(","), function(index, value){
                        console.log(value);
                        if(value.indexOf('Unnamed') < 0){

                            if(draggedAddress)
                                draggedAddress += ",";
                            draggedAddress += value;
                        }
                    });
                    if(draggedAddress)
                        address = draggedAddress;

                    console.log(address);
                    editedAddress = "<i class='fa fa-edit fa-1x red blink_me'></i>";
                    $("#address-auto-complete").val(address);
                }

                map.setCenter(results[0].geometry.location);
                var marker = null;

                if( me.centerMarker)
                    me.centerMarker.setMap(null);
                var pinIcon = new google.maps.MarkerImage(
                    baseUrl + "public/img/Azure.png",
                    null, /* size is determined at runtime */
                    null, /* origin is 0,0 */
                    null, /* anchor is bottom center of the scaled image */
                    new google.maps.Size(33, 45)
                );
                me.centerMarker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    animation: google.maps.Animation.DROP,
                    icon :pinIcon,
                    draggable : true
                });


                google.maps.event.addListener(me.centerMarker, 'dragstart', function(evt){
                    me.draggedPosition = null;
                });

                google.maps.event.addListener(me.centerMarker, 'dragend', function(evt){
                    me.draggedPosition = this.getPosition();
                    $("#map-data-go").trigger('click');
                });

                var content = " ";
                content += "<html><div>";
                content += "<h3 style='color:#4183C4;'>Center of search </h3>";
                content += "<h4 style='color:#4183C4;'>"+(address)+" &nbsp;"+editedAddress+"</h4>";
                content += "</div></html>";

                me.infowindow.setContent(content);
                me.centerMarker.infoContent = content;

                google.maps.event.addListener(me.centerMarker, 'click', function() {
                    me.infowindow.setContent(content);
                    me.infowindow.open(me.map, this);
                });

                me.infowindow.open(me.map, me.centerMarker);
                me.oms.addMarker(me.centerMarker);
                markerArray.push(me.centerMarker);

                if(typeof callbackFunction != 'undefined'){
                    callbackFunction(results[0].geometry.location);
                }

            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    };

    this.formatAddress = function(){

    };

    this.createCircle = function(center, range, callbackFunction){

        this.cityCircle = new google.maps.Circle({
            strokeColor: '#B4D9EF',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#B4D9EF',
            fillOpacity: 0.35,
            map: map,
            center: center,
            radius: (range * 1000)
        });

        map.fitBounds(this.cityCircle.getBounds());

        if( this.centerMarker) {
            this.centerMarker.bindTo("position", this.cityCircle, "center");
        }

        if(typeof callbackFunction != 'undefined'){
            callbackFunction();
        }
    };

    this.clearCircle = function(){
        if(this.cityCircle){
            this.cityCircle.setMap(null);
        }
    };

    this.createMapMarker = function($dataRow){
        var me = this;
        var latLng = new google.maps.LatLng($dataRow['latitude'], $dataRow['longitude']);
        var marker = new google.maps.Marker({
            position : latLng,
            animation: google.maps.Animation.DROP,
            map : me.map,
            icon : this.getMarkerIcon($dataRow)
        });

        marker.infoContent = this.infoWindowContent($dataRow); /** -- Setting info window content ---  **/
        me.oms.addMarker(marker);

        this.markerArray.push(marker);

        return marker;


    };

    this.getMarkerIcon =function($dataRow){
        var locationCategory = ($dataRow['type']) ? ( ($dataRow['type']).toUpperCase().replace(/ /g,'_')) : "DEFAULT";
        if($dataRow['type'] == 'Bank' || $dataRow['type'] == "Outlet"){
            locationCategory =  ($dataRow['name']) ? ( ($dataRow['name']).toUpperCase().replace(/ /g,'_') ) : "DEFAULT";
            locationCategory =  locationCategory.replace("-", "");
        }

        if(locatorMarkerIcons[0][locationCategory])
            return new google.maps.MarkerImage(locatorMarkerIcons[0][locationCategory].image, null, null, null, new google.maps.Size(locatorMarkerIcons[0][locationCategory].width, locatorMarkerIcons[0][locationCategory].height));
        else
            return new google.maps.MarkerImage(locatorMarkerIcons[0]['DEFAULT'].image, null, null, null, new google.maps.Size(locatorMarkerIcons[0]['DEFAULT'].width, locatorMarkerIcons[0]['DEFAULT'].height));
    };

    this.infoWindowContent = function(data){
        var otherDataSet = "";

        for(var index in data){
            otherDataSet += "data-" + index + "='"+data[index]+"' ";
        }

        var bankName   = (data['name']) ? data['name'] : 'NA';
        var imagePath  = (data['image']) ? data['image'] : baseUrl+"/public/img/preview.png";
        var branchName = (data['branch_name']) ? data['branch_name'] : 'NA';
        var distance   = (data['distance']) ? Number(data['distance']).toFixed(2)+" KMs" : "";
        var routeBtn   = (data['distance']) ? '<a class="modal-anchor" data-toggle="modal" style="text-decoration:none;cursor: pointer;" data-target=".route-map-modal" '+otherDataSet+' onclick="createRouteMap(this)" ><i class="fa fa-map-marker red">&nbsp;</i>View Route</a>' : "";
        var content    = " ";

        content += "<html> <div style='width: 350px;'>";
        content +=      "<table style='width: 100%' cellspacing='5' cellpadding='5'>";
        content +=          "<tr>";
        content +=              "<td style='width:40%'>";
        content +=                  "<img src='"+imagePath+"' style='width:100%;height: 130px;'>";
        content +=               "</td>";
        content +=              "<td valign='top'>";
        content +=                  "<h4 style='color:#4183C4;'>"+bankName+"</h4>";
        content +=                  "<h5 style='color:#4183C4;'>"+branchName+"</h5>";
        content +=                  "<h5 style='color:#4183C4;'>"+distance+" </h5>";
        content +=                  "<h5 style='color:#4183C4;'>"+routeBtn+" </h5>";
        content +=              "</td>";
        content +=          "</tr>";
        content +=       "</table> ";
        content += "</div></html>";
        return content;
    };

    this.removeMarkers = function(){

        if(this.markerArray.length){
            for (var i = 0; i < this.markerArray.length; i++) {
                this.markerArray[i].setMap(null);
            }
            this.markerArray = [];
        }
    };

    this.activateHeatMap = function($data){
        me = this;
        $.each($data, function(index, value){
            //console.log(me.getHeatMapWeight(value['population']));
            var point = {
                location : new google.maps.LatLng(value['latitude'], value['longitude']),
                weight : (Number(value['population'])) ? me.getHeatMapWeight(value['population']) : 100
            };
            me.heatmapData.push(point);

        });

        this.heatmap = new google.maps.visualization.HeatmapLayer({
            data: me.heatmapData,
            "radius":35
        });
        this.heatmap.setMap(this.map);
    };

    this.getHeatMapWeight = function(population){
        var populationDigitCount = Number((population).trim().length);
        var stanadardPopulation = 1;
        for(var i = 1; i < populationDigitCount; i++){
            stanadardPopulation = (stanadardPopulation * 10);
        }
        return Number((Number(population) / stanadardPopulation).toFixed(2));
    };

    this.clearHeatMap = function(){
        this.heatmap.setMap(null);
    }


}
var mapObject;
$(document).ready(function(){
    var options = {
        mapContainer : "map-container",
        autoCompleteInput: document.getElementById("address-auto-complete")
    };
    mapObject = new MapCreator(options);
    mapObject.createMap();

    mapObject.autoComplete();
    $requestObject.initialize({'requestFor' : 'population-data'}, "fillHeatMap", null);


    $('.modal-anchor').on('show.bs.modal', function (e) {
        google.maps.event.trigger(MapInstance,'resize')
    });
});

var fillHeatMap = function(result){
    result = JSON.parse(result);
    if(result){
        mapObject.activateHeatMap(result['populationData']);
    }

};

function fillCityOptions($data) {
    $("#city_loader").hide();
    $("#city_branch").html($data);
}

function getCityList($dataFor) {
    $("#city_loader").show();
    $requestData = {
        "requestFor": "city-data",
        "map-filters": {
            "region": $("#province_" + $dataFor).val(),
            "requestType": $dataFor
        }
    };
    $requestObject.initialize($requestData, "fillCityOptions", null);

}
var filterElements = ["select", "input"];
/**
 * Reads all filters
 * @param $parentFilterElement
 * @returns {{data array of filters}}
 */
var getFilters = function($parentFilterElement){
    var filters = {};
    var pointOfInterest = [];
    var bankName = [];
    for(var i in filterElements ){
        $($parentFilterElement).find(filterElements[i]).each(function(){
            filters[$(this).attr('name')] = $(this).attr('value');

        });

    }

    for(var i in filterElements ){
        $("#bank_filters").find(filterElements[i]).each(function(){

            if($(this).attr('checked') == "checked" && $(this).attr('value')){
                if($(this).attr('class') == "pointOfInterestInput"){
                    pointOfInterest.push($(this).attr('value'));
                }else if($(this).attr('class') == "bankInput" ){
                    bankName.push($(this).attr('value'));
                }
            }


        });

    }

    filters['point_of_interest'] = pointOfInterest;
    filters['bank_name'] = bankName;

    return filters;
};
/**
 *  Resets all filters
 * @param $parentFilterElement
 */
var resetMapData = function($parentFilterElement, $requestFor){
    for(var i in filterElements ){
        $($parentFilterElement).find(filterElements[i]).each(function(){
            if(filterElements[i] == "select"){
                //$(this).attr('value', 0);
                $(this).prop('selectedIndex', 0);
                //$(this).attr('value', '0');
                $(this).trigger('onchange');
            }
            if(filterElements[i] == "input"){
                if($(this).attr('name') != "range")
                    $(this).attr('value', '');
            }

        });
    }
    /** -- removing previous data from map --**/
    $("#record_count").html(0);
    //toggleAllCheckBox(null, "#bank_filters", false);
    mapObject.removeMarkers();
    mapObject.clearCircle();
    mapObject.map.setCenter(new google.maps.LatLng(22.268763552489748, 47.87841796875));
    mapObject.map.setZoom(5);
    /**           ------             **/
};

/**
 * Filters validator
 * @param filters
 * @returns {boolean}
 */
var validateFilters = function(filters){
    var error = false;
    //** -- data validation for is search is Region wise -- **/
    if(mapObject.selectedSearch == 'region'){
        if((filters.province).length == 0){
            alert("Please enter Province");
            error = true;
        }/*else if((filters.city).length == 0){
         alert("Please enter City");
         error = true;
         }*/else if( (filters.point_of_interest).length == 0 && (filters.bank_name).length == 0){
            alert("Please select bank/point of interest");
            error = true;
        }

    }
    //** -- data validation for is search is radius wise -- **/
    if(mapObject.selectedSearch == "radius"){
        if((filters.address).length == 0){
            alert("Please enter Address");
            error = true;
        }else if( (filters.point_of_interest).length == 0 && (filters.bank_name).length == 0){
            alert("Please select bank/point of interest");
            error = true;
        }
    }


    return error;
};

var showRangeSlider = function(slidervalue){
    if(slidervalue){
        $("#range-slider-div").show();
    }else{
        $("#range-slider-div").hide();
    }
};

/**
 * gets map data rowset, as per filters
 * @param $parentFilterElement
 * @param $requestFor
 */

var getMapData = function($parentFilterElement, $requestFor){

    var filters = getFilters($parentFilterElement);
    /** -- removing previous data from map --**/
    mapObject.removeMarkers();
    mapObject.clearCircle();
    $("#record_count").html(0);
    /**           ------             **/

    if(!validateFilters(filters)){
        //var address = filters['address']+","+filters['city']+","+filters['province'];

        var address = filters['address'];
        if(mapObject.selectedSearch == "radius"){
            mapObject.geocodeAddress(address, function(position){  /**  To get Geocode from address **/
            var location = position.lat() +","+position.lng(); /** Address Geocode --- **/
            mapObject.draggedPosition = null;
                $("#"+$requestFor).find("i").attr('class', 'fa fa-spinner fa-spin');
                mapObject.createCircle(position, filters['range'], function(){
                    filters['location'] = location;
                    filters['search-type'] = 'radius';
                    var requestData = {
                        'requestFor'  : 'locator-map-data',
                        'filter-data' : filters
                    };
                    mapDataRequest(requestData);
                });
            });
        }else{
            $("#"+$requestFor).find("i").attr('class', 'fa fa-spinner fa-spin');
            filters['search-type'] = 'region';
            var requestData = {
                'requestFor'  : 'locator-map-data',
                'filter-data' : filters
            };
            mapDataRequest(requestData);
        }

    }
};

var mapDataRequest = function(requestData){
    //if($requestFor == "map-data-go" || $requestFor == "map-data-reset")
    $requestObject.initialize(requestData, 'mapRequestCallBack', null);
};


var mapRequestCallBack = function(result){
    $("#map-data-reset").find("i").attr('class', 'fa fa-refresh');
    $("#map-data-go").find("i").attr('class', 'fa fa-search');
    if($("#bank_filters").attr('class') != "animated")
        $("#bank_filters").attr('class', 'animated');
    $(".animateFilterDiv").trigger("click");
    if(result != null){
        $data= JSON.parse(result);
        $("#record_count").html(($data['dataRowset']).length);
        if(mapObject.selectedSearch == "region"){
            mapObject.map.setCenter(new google.maps.LatLng($data['dataRowset'][0]['latitude'], $data['dataRowset'][0]['longitude']));
            mapObject.map.setZoom(7);
        }
        $.each($data['dataRowset'], function(index, value){
            var marker = mapObject.createMapMarker(value);

        })
    }
};

var toggleFilters = function(that){

    /** -- reseting filters  ---  **/

    resetMapData('#map-filters-div', 'map-data-go');
    /** removing active class from li  -- **/
    $(that).parent().find("li").each(function(){
        $(this).removeAttr('class');
    });

    $(that).addClass('active');  /** setting active to the selected ;i -- **/

    $("#selected_filters_type").html($(that).find('a').html());

    /** -- if radius search is selected  --  **/
    if($(that).find('i').text().trim() == "Radius search"){
        $("#region_search").hide();
        $("#radius_search").show();
        mapObject.selectedSearch = "radius";
    }
    /** -- if region wise search is selected  --  **/
    if($(that).find('i').text().trim() == "Region search"){
        $("#radius_search").hide();
        $("#region_search").show();
        mapObject.selectedSearch = "region";
    }

};
var routeMap = null;
var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
var directionsService = new google.maps.DirectionsService();
var icons;
var routeMarkersArray = [];
var routeInfoWindow = new google.maps.InfoWindow();
var createRouteMap = function(currentElement){
    var data = $(currentElement).data();


    if(!routeMap){
        routeMap = new google.maps.Map(document.getElementById('route-map-container'), {
            zoom: 10,
            center: {lat: mapObject.centerMarker.getPosition().lat() , lng: mapObject.centerMarker.getPosition().lng()}
        });
        directionsDisplay.setMap(routeMap);
    }

    /** Search center changes, we need to change the center of map -- **/
    if(routeMap.getCenter().lat() != mapObject.centerMarker.getPosition().lat()){
        routeMap.setCenter(new google.maps.LatLng(mapObject.centerMarker.getPosition().lat(),mapObject.centerMarker.getPosition().lng()));
    }

    if(routeMarkersArray.length){

        for (var i = 0; i < routeMarkersArray.length; i++) {
            routeMarkersArray[i].setMap(null);
        }
        routeMarkersArray = [];

        routeInfoWindow.setMap(null);
    }

    icons = {
        start: new google.maps.MarkerImage(
            // URL
            baseUrl + "public/img/Azure.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(33, 45)
        ),
        end: mapObject.getMarkerIcon(data)
    };

    // google.maps.event.addListener(routeMap, 'tilesloaded', function(evt) {
    calculateAndDisplayRoute(data);
    // });

};

function calculateAndDisplayRoute(data) {
    var bounds = new google.maps.LatLngBounds();

    directionsService.route({
        origin:  new google.maps.LatLng(mapObject.centerMarker.getPosition().lat(),mapObject.centerMarker.getPosition().lng()),
        destination:  new google.maps.LatLng(data['latitude'], data['longitude']),
        travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            console.log(response);

            directionsDisplay.setOptions({ preserveViewport: true });
            directionsDisplay.setDirections(response);

            var leg = response.routes[ 0 ].legs[ 0 ];
            makeMarker( leg.start_location, icons.start, "Journey starts here" );
            makeMarker( leg.end_location, icons.end, 'Destination' );

            var routePoints =  response.routes[ 0 ].overview_path;
            for(var routePointIndex in routePoints){
                // bounds.extend(routePoints[routePointIndex]);
            }
            var step = 1;
            var viaInfo =(response.routes[ 0 ].summary) ? response.routes[ 0 ].summary : '(<i class="fa fa-warning red"></i> &nbsp;Info not available)';
            var routeInfo = "<p>From center of search &nbsp;<i class='fa fa-exchange'></i>&nbsp; "+data['name']+" ( <b>"+leg.distance.text+"</b> )</p>";
            routeInfo += "<i class='fa fa-car'></i>&nbsp;At a drive of <strong>"+leg.duration.text+" </strong> via <strong>"+ viaInfo +"</strong>";
            $("#route-message").html(routeInfo);
            routeMap.setCenter(response.routes[0].legs[0].steps[step].end_location);
            /*
             var routeInfo = "<div style='width: 60px;text-align: right'><strong>" + leg.distance.text +"</strong></div>";
             routeInfoWindow.setContent(routeInfo);
             routeInfoWindow.setPosition(response.routes[0].legs[0].steps[step].end_location);
             routeInfoWindow.open(routeMap);
             */

            // routeMap.fitBounds(bounds);

        } else {
            $("#route-message").html('<i class="fa fa-warning red"></i> &nbsp;No Routes found');
            window.alert('Directions request failed due to ' + status);

        }
    });
}
function makeMarker( position, icon, title ) {
    var marker = new google.maps.Marker({
        position: position,
        map: routeMap,
        icon: icon,
        title: title
    });
    routeMarkersArray.push(marker);
}
/*
 $(".close-route-map").on("click", function(){
 $("#route-map-holder").html('<div id="route-map-container" style="height: 400px;width: 100%;" ></div>');
 });*/






