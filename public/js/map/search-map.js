
var map;
var check = 1;
var mobilyResetCheck = 1;
var branchListObj = {};
var branchListObjForHRDF = {};
String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
var testData = {
    location: null,
    weight: null
};


function sorListAndFill($type) {

    $("#overview_branch").html("");
    $("#overview_branch").html("<div align='center'>Updating.....</div>");
    var myObj = branchListObj;
    $hasContentFill = false;
    $mainContent = '';
    $content = "";
    $previousScore = 0;


    for (var branchScore in myObj) {

        $content = "";

        for (var branchCode in myObj[branchScore]) {
            $content += myObj[branchScore][branchCode];
        }

        if (branchScore == "NaN") {
            branchScore = 0;
        }
        // console.log(branchScore);
        // console.log($content);
        if (!$hasContentFill) {
            if ($type == 'asc') {
                if (Number($previousScore) <= Number(branchScore)) {
                    $mainContent += $content;
                } else {
                    $mainContent = $content + $mainContent;
                }
            } else {

                if (Number($previousScore) >= Number(branchScore)) {
                    $mainContent += $content;
                } else {
                    $mainContent = $content + $mainContent;
                }
            }
            $previousScore = branchScore;
        } else {
            $previousScore = branchScore;
            $mainContent += $content;
            $hasContentFill = true;
        }

    }

    $("#overview_branch").html('<ul class="dashboard-sublist"><li style="height:36px;"><a style="margin-left:310px;" target="_blank" href="#"><img title="Men Branch" src="https://4c360.com/4c-dashboard/public/img/men.png"></a>&nbsp;<a target="_blank" style="margin-left: 18px;" href="#"><img title="Ladies Branch" src="https://4c360.com/4c-dashboard/public/img/ladies.png"></a></li>' + $mainContent + '</ul>');
}


function sorListAndFillForHRDF($type) {

    $("#overview_branch").html("");
    $("#overview_branch").html("<div align='center'>Updating.....</div>");


    var myObj = branchListObjForHRDF;
    $hasContentFill = false;
    $mainContent = '';
    $content = "";
    $previousScore = 0;
    for (var branchScore in myObj) {

        $content = "";

        for (var branchCode in myObj[branchScore]) {
            $content += myObj[branchScore][branchCode];
        }

        if (branchScore == "NaN") {
            branchScore = 0;
        }
        // console.log(branchScore);
        if (!$hasContentFill) {
            if ($type == 'asc') {
                if (Number($previousScore) <= Number(branchScore)) {
                    $mainContent += $content;
                } else {
                    $mainContent = $content + $mainContent;
                }
            } else {

                if (Number($previousScore) >= Number(branchScore)) {
                    $mainContent += $content;
                } else {
                    $mainContent = $content + $mainContent;
                }
            }
            $previousScore = branchScore;
        } else {
            $previousScore = branchScore;
            $mainContent += $content;
            $hasContentFill = true;
        }

    }

    $("#overview_branch").html('<ul class="dashboard-sublist">' + $mainContent + '</ul>');
}




/*
 function sortNCBReBrandingListAndFill($type){
 $("#overview_branch").html("<div align='center'>Updating.....</div>");

 var myObj = branchListObj;
 $hasContentFill = false;
 $mainContent = '';
 $content = "";
 $previousScore = 0;
 for(var branchScore in myObj){
 // console.log(myObj[branchScore]);
 $content = "";
 for(var branchCode in myObj[branchScore]){
 $content += myObj[branchScore][branchCode];
 }

 if(branchScore == "NaN"){
 branchScore = 0;
 }
 // console.log(branchScore);
 // console.log($content);
 if( !$hasContentFill ){
 if($type == 'asc'){
 if(Number($previousScore) <= Number(branchScore)){
 $mainContent += $content;
 }else{
 $mainContent = $content+$mainContent;
 }
 }else{

 if(Number($previousScore) >= Number(branchScore)){
 $mainContent += $content;
 }else{
 $mainContent = $content+$mainContent;
 }
 }
 $previousScore = branchScore;
 }else{
 $previousScore = branchScore;
 $mainContent += $content;
 $hasContentFill = true;
 }

 }
 $("#overview_branch").html('<ul class="dashboard-sublist"><li style="height:36px;"><a style="margin-left:310px;" target="_blank" href="#"><img title="Men Branch" src="https://4c360.com/4c-dashboard/public/img/men.png"></a>&nbsp;<a target="_blank" style="margin-left: 18px;" href="#"><img title="Ladies Branch" src="https://4c360.com/4c-dashboard/public/img/ladies.png"></a></li>'+$mainContent+'</ul>');
 } */


function MapCreator(options) {
    this.cluster = null;
    this.markerArray = [];
    this.map = null;
    this.centerPoint = null;
    this.clusterOptions = {styles: styles[0]};
    this.mapType = options.type;
    this.mapContainer = options.mapContainer;
    this.markerInfo = [];
    this.heatmap = null;

    if (bankFolder != 'rbcapital') {
        this.infowindowObj = new InfoBubble({maxWidth: 400, borderColor: '#CCCCCC'});
    } else {
        this.infowindowObj = null;
    }

    this.createMap = function () {
        var mapOptions = {
            center: new google.maps.LatLng(22.268763552489748, 47.87841796875),
            zoom: 5,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(document.getElementById(this.mapContainer), mapOptions);
        mapchange = this.map;
        changeIsrailLayer(this.map.getZoom(), this.map);

    };

    this.createMarker = function (latLng, markerType, otherData) {

        var me = this;
        $displayMarkerOnMap = null;
        if (baseBank == "ncbrebranding")
            $displayMarkerOnMap = me.map;

        var marker = new google.maps.Marker({
            position: latLng,
            icon: this.createIcon(markerType, otherData),
            flat: true,
            map: $displayMarkerOnMap
        });
        if (baseBank == 'hrdf_v2' && otherData.branchType !== null && (otherData.branchType).toUpperCase() == group_name.toUpperCase()) {
            me.markerArray.push(marker);
        } else {
            if (baseBank != 'hrdf_v2'&&  latLng.lat()!=0 && latLng.lng()!=0 ) {
                me.markerArray.push(marker);
            }
        }
        me.markerInfo.push([otherData,
                me.markerArray.length - 1]
                );

        google.maps.event.addListener(marker, 'click', function (event) {
            if (typeof otherData.shop_id == 'undefined')
                me.openInfowindow(this, otherData);
            else
                me.tabInfoWindow(this, otherData);
        });
    };

    this.tabInfoWindow = function (marker, data) {
        /* this.infowindowObj = new InfoBubble({maxWidth: 400,borderColor: '#CCCCCC'});

         this.infowindowObj.addTab('<b style="color:#4183C4;"> Info </b>',"<div align='center' ><ul class='dashboard-list'><li class=''><ul class='dashboard-sublist' style ='text-align:left;'><li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Code</span></a><b style='color:#3E576F;font-size:14px;'>ATF002C</b></li><li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Region</span></a><b style='color:#3E576F;font-size:14px;'>Central </b></li><li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>City</span></a><b style='color:#3E576F;font-size:14px;'>Buridah</b></li><li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Partner</span></a><b style='color:#3E576F;font-size:14px;'>ATC</b></li><li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Type</span></a><b style='color:#3E576F;font-size:14px;'>MFBO</b></li><li class='last'><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Operation Date</span></a><b style='color:#3E576F;font-size:14px;'>1/5/2008</b></li></ul></li></ul><div>");
         this.infowindowObj.addTab('<b style="color:#4183C4;"> Contact </b>',"<div align='center'><ul class='dashboard-list'><li class=''><ul class='dashboard-sublist' style ='margin-left:-51px;'><li><a href='#'><span style='width:22%;font-size:14px;text-align:left;color:#808080;'>Email</span></a><b style='color:#3E576F;font-size:14px;margin-left:40px;'>ai.alghamdi@mobily.com.sa</b></li><li class='last'><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;margin-left:-55px'>Mobile</span></a><b style='color:#3E576F;font-size:14px;'>0500130358</b></li></ul></li></ul><div>");
         this.infowindowObj.addTab('<b style="color:#4183C4;"> Photo </b>',"<div align='center'><div style='padding-left:7px;'><img src='https://4c360.com/4c-dashboard/public/js/map/entrance2.jpg' style='height: 204px;width: 377px;'></div><div>");
         this.infowindowObj.open(this.map,marker); */
        this.infowindowObj = new InfoBubble({maxWidth: 400, borderColor: '#CCCCCC'});
        if (this.infowindowObj) {
            this.infowindowObj.removeTab(0);
            this.infowindowObj.removeTab(1);
            this.infowindowObj.setMap(null);
        }

        this.infowindowObj = new InfoBubble({maxWidth: 300, maxHeight: 225, borderColor: '#CCCCCC'});
        this.infowindowObj.open(this.map, marker);

        this.infowindowObj.addTab('<b> Outlet Detail</b>', this.createContentMobily(data));

        this.infowindowObj.addTab('<b> Image </b>', this.mobilyImageInfo(data));

    };



    if (bankFolder != 'rbcapital' && bankFolder != 'tawuniya' && bankFolder != 'mobily' && bankFolder != 'hrdf'&& bankFolder != 'arbbranches_v2' && bankFolder != 'arbasset') {
        this.openInfowindow = function (marker, data) {

            this.openInfowindow = function (marker, data) {
                if (bankFolder == 'tawuniya') {
                    //this.infowindowObj.setContent(this.createContentTawuniya(data));
                } else {
                    this.infowindowObj.setContent(this.createContent(data));
                }

                this.infowindowObj.open(this.map, marker);
            }

        }

    } else if (bankFolder == 'tawuniya') {

        this.openInfowindow = function (marker, data) {

            if (this.infowindowObj) {
                this.infowindowObj.removeTab(0);
                this.infowindowObj.removeTab(1);
                this.infowindowObj.setMap(null);
            }

            this.infowindowObj = new InfoBubble({maxWidth: 400, borderColor: '#CCCCCC'});
            this.infowindowObj.open(this.map, marker);

            this.infowindowObj.addTab('<b> Profile </b>', this.createContentTawuniya(data));

            this.infowindowObj.addTab('<b> Gallery </b>', tawuniyaImageInfo(data));

        }
    } else if (bankFolder == 'hrdf') {

        this.openInfowindow = function (marker, data) {

            if (this.infowindowObj) {
                this.infowindowObj.removeTab(0);
                this.infowindowObj.removeTab(1);
                this.infowindowObj.setMap(null);
            }

            this.infowindowObj = new InfoBubble({maxWidth: 400, borderColor: '#CCCCCC'});
            this.infowindowObj.open(this.map, marker);

            this.infowindowObj.addTab('<b> Branch Info </b>', this.createContentHrdf(data));

            this.infowindowObj.addTab('<b> Branch Image </b>', hrdfImageInfo(data));

        }
    }else if (bankFolder == 'arbbranches_v2' || bankFolder == 'arbasset') {

        this.openInfowindow = function (marker, data) {

            if (this.infowindowObj) {
                this.infowindowObj.removeTab(0);
                this.infowindowObj.removeTab(1);
                this.infowindowObj.setMap(null);
            }

            this.infowindowObj = new InfoBubble({maxWidth: 300, borderColor: '#CCCCCC'});
            this.infowindowObj.open(this.map, marker);

            this.infowindowObj.addTab('<b> Branch Image </b>', arbImageInfo(data));
            this.infowindowObj.addTab('<b> Branch Info </b>', this.createContentArb(data));

        }
    } else if (bankFolder == 'mobily') {

        this.openInfowindow = function (marker, data) {

            if (this.infowindowObj) {
                this.infowindowObj.removeTab(0);
                this.infowindowObj.removeTab(1);
                this.infowindowObj.setMap(null);
            }

            this.infowindowObj = new InfoBubble({maxWidth: 300, maxHeight: 270, borderColor: '#CCCCCC'});
            this.infowindowObj.open(this.map, marker);

            this.infowindowObj.addTab('<b> Outlet Detail</b>', this.createContentMobily(data));

            this.infowindowObj.addTab('<b> Image </b>', this.mobilyImageInfo(data));

        }
    } else {
        this.openInfowindow = function (marker, data) {
            if (this.infowindowObj) {
                this.infowindowObj.removeTab(0);
                this.infowindowObj.removeTab(1);
                this.infowindowObj.setMap(null);
            }

            this.infowindowObj = new InfoBubble({maxWidth: 400, borderColor: '#CCCCCC', disableAutoPan: false});
            this.infowindowObj.open(this.map, marker);
            this.infowindowObj.addTab('<b style="color:#4183C4;">Photo</b>', this.createContentRC(data));
            this.infowindowObj.addTab('<b style="color:#4183C4;">Info</b>', tabInfo1(data));
            this.infowindowObj.addTab('<b style="color:#4183C4;">Financials</b>', tabInfo2(data));

        }


    }

    this.createContent = function (data) {

        var genderIndicator;
        var gentsExist = false;
        var finalBranchName;

        if (data.type == 'ATM') {
            html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                    "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                    "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" +
                    "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span></div>" +
                    "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
            return html;
        } else {
            if (branchTypeArabicDashboard) {
                html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                        "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                        "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>";
                if (data.branchType.toUpperCase() == 'GENTS') {
                    html += "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:115px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: " + branchTypeArabicDashboard + "</span></div>";
                } else {
                    html += "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:48px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: " + branchTypeArabicDashboard + "</span></div>";
                }
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
            } else {

                if (bankFolder == 'mobily') {
                    html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                            "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                            "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" +
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Outlet Type:</span>&nbsp;<span class='title'>" + this.getOutletype(data) + "</span></div>" +
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
                } else if (bankFolder == 'ughi') {
                    html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                            "<div align='center' class='title'><b>" + data.branchCode_orignal + "</b></div>" +
                            "<div style='height:210px;width:320px;'><div>" + this.createContentUghi(data) + "</div></div>" +
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
                } else if (bankFolder == 'hrdfold') {
                    html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                            "<div align='center' class='title'><b>" + data.branchName + ")" + data.branchCode + ")</b></div>" +
                            "<div style='height:175px;width:320px;'><div>" + this.createContentHrdf(data) + "</div></div>" +
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
                } else if (bankFolder == "ncbrebranding") {
                    var branchCode = data.branchCode;
                    if (branchCode == 440123)
                        branchCode = '440-Old';
                    html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                            "<div align='center' class='title'><b>" + data.branchName + "(" + branchCode + ")</b></div>" +
                            "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" +
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Branch Type:</span>&nbsp;<span class='title'>" + this.getBranchType(data) + "</span></div>" +
                            "<div><span class='nameTitle' style='font-size:13px;'>Last Visit Date :</span>&nbsp;<span class='' style='font-size:12px;'>" + ((data.lastVisitDate) ? data.lastVisitDate : 'NA') + "</span></div>" +
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
                } else if (bankFolder == "medgulf") {

                    html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                            "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                            "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" +
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Branch Type:</span>&nbsp;<span class='title'>" + this.getBranchType(data) + "</span></div>" +
                            "<div><span class='nameTitle' style='font-size:13px;'>Last Visit Date :</span>&nbsp;<span class='' style='font-size:12px;'>" + ((data.lastVisitDate) ? data.lastVisitDate : 'NA') + "</span></div>" +
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
                } else {
                    html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                            "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                            "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" +
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Branch Type:</span>&nbsp;<span class='title'>" + this.getBranchType(data) + "</span></div>" +
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
                }

            }

            return html;
        }

    };

    this.createContentRC = function (data) {
        var genderIndicator;
        var gentsExist = false;
        var finalBranchName;

        if (data.type == 'ATM') {
            html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                    "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                    "<div style='height:0px;width:220px;'></div>" +
                    "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span></div>" +
                    "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
            return html;
        } else {
            if (branchTypeArabicDashboard) {
                html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                        "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                        "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>";
                if (data.branchType.toUpperCase() == 'GENTS') {
                    html += "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:115px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: " + branchTypeArabicDashboard + "</span></div>";
                } else {
                    html += "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:48px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: " + branchTypeArabicDashboard + "</span></div>";
                }
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
            } else {
                html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                        "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                        "<div style='height: 207px;width: 220px;'><div style='padding-left:7px;'><img src='" + this.validateImage(data.image) + "' style='height: 204px;width: 377px;'/></div></div>" +
                        "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Branch Type:</span>&nbsp;<span class='title'>" + this.getBranchType(data) + "</span></div>" +
                        "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
            }

            return html;
        }
    };

    this.createContentTawuniya = function (data) {

        var genderIndicator;
        var gentsExist = false;
        var finalBranchName;

        var htmlTawuniya = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>";
        htmlTawuniya += "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>";

        htmlTawuniya += '<div style="top: -7px;position:relative;margin-left: 3px;" class="box span12">';
        htmlTawuniya += '<ul class="dashboard-list" style="height:256px;"><li class=""><ul class="dashboard-sublist">';

        if (typeof (data.agent_name) === undefined)
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Agent Name</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Agent Name</span></a><b style="color:#3E576F;font-size:14px;">' + (data.agent_name ? data.agent_name : '-') + '</b></li>';

        if (typeof (data.office) == 'undefined')
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Office</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Office</span></a><b style="color:#3E576F;font-size:14px;">' + (data.office ? data.office : '-') + '</b></li>';
        htmlTawuniya += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Branch Code</span></a><b style="color: #3E576F;font-size:14px;">' + (data.branchCode ? data.branchCode : '-') + '</b></li>';
        htmlTawuniya += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Zone Name</span></a><b style="color: #3E576F;font-size:14px;">' + (data.area_name ? data.area_name : '-') + '</b></li>';
        htmlTawuniya += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b style="color: #3E576F;font-size:14px;">' + (data.address ? data.address : '-') + '</b></li>';
        if (typeof (data.po_box) == 'undefined')
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">PoBox</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">PoBox</span></a><b style="color:#3E576F;font-size:14px;">' + (data.po_box ? data.po_box : '-') + '</b></li>';
        if (typeof (data.geo_code) == 'undefined')
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Geo Code</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Geo Code</span></a><b style="color:#3E576F;font-size:14px;">' + (data.geo_code ? data.geo_code : '-') + '</b></li>';
        if (typeof (data.branch_manager) == 'undefined')
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Branch Manager</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Branch Manager</span></a><b style="color:#3E576F;font-size:14px;">' + (data.branch_manager ? data.branch_manager : '-') + '</b></li>';
        if (typeof (data.mobile_number) == 'undefined')
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Mobile number</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Mobile number</span></a><b style="color:#3E576F;font-size:14px;">' + (data.mobile_number ? data.mobile_number : '-') + '</b></li>';
        if (typeof (data.working_hours_satTowed) == 'undefined')
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Working Hours</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlTawuniya += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Working Hours</span></a><b style="color:#3E576F;font-size:14px;">' + (data.working_hours_satTowed ? data.working_hours_satTowed : '-') + '</b></li>';

        htmlTawuniya += '</ul>';
        htmlTawuniya += '</li>';
        htmlTawuniya += '</ul>';
        htmlTawuniya += '</div>';
        htmlTawuniya += "<div style='height:165px;width:361px;'><div>";

        htmlTawuniya += "</div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>" +
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        return htmlTawuniya;

    };


    this.createContentHrdf = function (data) {

        var htmlHrdf = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>";
        htmlHrdf += "<div align='center' class='title'><b>" + data.branchCode + " (" + data.branchName + ")</b></div>";

        htmlHrdf += '<div style="top: -7px;position:relative;margin-left: 3px;" class="box span12">';
        htmlHrdf += '<ul class="dashboard-list" style="height:256px;"><li class=""><ul class="dashboard-sublist">';

        htmlHrdf += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Name</span></a><b style="color:#3E576F;font-size:14px;">' + (data.branchName ? data.branchName : '-') + '</b></li>';
        htmlHrdf += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Code</span></a><b style="color:#3E576F;font-size:14px;">' + (data.branchCode ? data.branchCode : '-') + '</b></li>';
        htmlHrdf += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Province</span></a><b style="color: #3E576F;font-size:14px;">' + (data.province ? data.province : '-') + '</b></li>';
        htmlHrdf += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">City</span></a><b style="color: #3E576F;font-size:14px;">' + (data.city ? data.city : '-') + '</b></li>';
        //htmlHrdf +='<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b title="'+ (data.address ? data.address : '-') +'" style="cursor: pointer; color: #3E576F;font-size:14px;">'+ (data.address ? data.address : '-') +'</b></li>';
        var address = data.address;

        if (address.length > 15) {
            address = address.substr(0, 15);
            address = address + '...';
        } else {
            address = address;
        }

        htmlHrdf += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b title="' + (data.address ? data.address : '-') + '" style="cursor: pointer; color: #3E576F;font-size:14px;">' + address + '</b></li>';
        htmlHrdf += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Manager Name</span></a><b style="color:#3E576F;font-size:14px;">' + (data.manager_name ? data.manager_name : '-') + '</b></li>';
        htmlHrdf += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Manager Email</span></a><b style="color:#3E576F;font-size:14px;">' + (data.manager_email ? data.manager_email : '-') + '</b></li>';
        htmlHrdf += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Tel</span></a><b style="color:#3E576F;font-size:14px;">' + (data.outlet_telephone ? data.outlet_telephone : '-') + '</b></li>';

        htmlHrdf += '</ul>';
        htmlHrdf += '</li>';
        htmlHrdf += '</ul>';
        htmlHrdf += '</div>';
        htmlHrdf += "<div style='height:165px;width:361px;'><div>";

        htmlHrdf += "</div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>" +
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        return htmlHrdf;

    };

    this.createContentArb = function (data) {
        var branch_name = data.branchName;
        var branch = '';
        if(branch_name) {
            var branch = ' ( ' + branch_name + ' )';
        } else {
            var branch = '';
        }
        if (data.branchName.length > 22) {
            branchName = data.branchName.substr(0, 22);
            branchName = branchName + '...';
        } else {
            branchName = branch_name;
        }
        var htmlArb = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>";
        htmlArb += "<div align='center' class='title'><b>" + data.branchCode + branch +"</b></div>";

        htmlArb += '<div style="top: -7px;position:relative;margin-left: 3px;" class="box span12">';
        htmlArb += '<ul class="dashboard-list" style="height:256px;"><li class=""><ul class="dashboard-sublist">';

        htmlArb += '<li><a href="#"><span style="width:32%;font-size:14px;text-align:left;color:#808080;">Name</span></a><b title="'+ data.branchName +'" style="color:#3E576F;font-size:14px;">' + (branchName ? branchName : '-') + '</b></li>';
        htmlArb += '<li><a href="#"><span style="width:32%;font-size:14px;text-align:left;color:#808080;">Code</span></a><b style="color:#3E576F;font-size:14px;">' + (data.branchCode ? data.branchCode : '-') + '</b></li>';
        htmlArb += '<li><a href="#"><span  style="width:32%;font-size:14px;text-align:left;color:#808080;">Province</span></a><b style="color: #3E576F;font-size:14px;">' + (data.province ? data.province : '-') + '</b></li>';
        htmlArb += '<li><a href="#"><span  style="width:32%;font-size:14px;text-align:left;color:#808080;">City</span></a><b style="color: #3E576F;font-size:14px;">' + (data.city ? data.city : '-') + '</b></li>';
        if (bankFolder != 'arbbranches_v2') {
        htmlArb +='<li><a href="#"><span  style="width:32%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b title="'+ (data.address ? data.address : '-') +'" style="cursor: pointer; color: #3E576F;font-size:14px;">'+ (data.address ? data.address : '-') +'</b></li>';}
        htmlArb +='<li><a href="#"><span  style="width:32%;font-size:14px;text-align:left;color:#808080;">City Category</span></a><b title="'+ (data.city_category ? data.city_category : '-') +'" style="cursor: pointer; color: #3E576F;font-size:14px;">'+ (data.city_category ? data.city_category : '-') +'</b></li>';
        htmlArb +='<li><a href="#"><span  style="width:32%;font-size:14px;text-align:left;color:#808080;">Branch Type</span></a><b title="'+ (data.branchType ? data.branchType : '-') +'" style="cursor: pointer; color: #3E576F;font-size:14px;">'+ (data.branchType ? data.branchType : '-') +'</b></li>';
        if(bankFolder == "arbasset"){
            htmlArb +='<li><a href="#"><span  style="width:32%;font-size:14px;text-align:left;color:#808080;">Lat inventory date</span></a><b title="'+ (data.lastVisitDate ? data.lastVisitDate : '-') +'" style="cursor: pointer; color: #3E576F;font-size:14px;">'+ (data.lastVisitDate ? data.lastVisitDate : 'NA') +'</b></li>';
        }
        htmlArb += '</ul>';
        htmlArb += '</li>';
        htmlArb += '</ul>';
        htmlArb += '</div>';
        htmlArb += "<div style='height:165px;width:361px;'><div>";

        htmlArb += "</div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>" +
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        return htmlArb;

    };

    function hrdfImageInfo(data) {
        var html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                "<div align='center' class='title'><b>Branch Code&nbsp;:&nbsp;" + data.branchCode + "</b></div>" +
                "<div style='height:214px;width: 100px;'><div style='padding-left:7px;'><img src=" +
                ((data.image && (data.image != 'null')) ? data.image : 'https://4c360.com/4c-dashboard//public/img/logo/hrdf_branch.png') + " style='height: 265px;width: 377px;'/></div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div></div>" +
                "<div></div></div>";
        return html;

    }
    function arbImageInfo(data) {
        var html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                "<div align='center' class='title'><b>Branch Code&nbsp;:&nbsp;" + data.branchCode + "</b></div>" +
                "<div style='height:265px;width: 300px;'><div style='padding-left:7px;'><img src=" +
                ((data.image && (data.image != 'null')) ? data.image : 'https://4c360.com/4c-dashboard/public/img/AlRajhiProfileImage.png') + " style='height: 260px;width: 275px;'/></div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div></div>" +
                "<div></div></div>";
        return html;

    }

    this.mobilyImageInfo = function (data) {


        var html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>" +
                "<div style='height:150px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:140px;width:291px;'/></div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Outlet Type:</span>&nbsp;<span class='title'>" + this.getOutletype(data) + "</span></div>" +
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        return html;

    };

    this.createContentMobily = function (data) {

        var genderIndicator;
        var gentsExist = false;
        var finalBranchName;

        var htmlMobily = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>";
        htmlMobily += "<div align='center' class='title'><b>" + data.branchName + "(" + data.branchCode + ")</b></div>";

        htmlMobily += '<div style="top: -7px;position:relative;margin-left: 3px;height:158px" class="box span12">';
        htmlMobily += '<ul class="dashboard-list" style="height:250px;"><li class=""><ul class="dashboard-sublist">';

        if (typeof (data.branchCode) === undefined)
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Code</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Code</span></a><b style="color:#3E576F;font-size:14px;">' + (data.branchCode ? data.branchCode : '-') + '</b></li>';

        if (typeof (data.outlet_type) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Type</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Type</span></a><b style="color:#3E576F;font-size:14px;">' + (data.outlet_type ? data.outlet_type : '-') + '</b></li>';
        if (typeof (data.address) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b style="color:#3E576F;font-size:14px;">' + (data.address ? data.address : '-') + '</b></li>';
        if (typeof (data.city) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">City</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">City</span></a><b style="color:#3E576F;font-size:14px;">' + (data.city ? data.city : '-') + '</b></li>';
        if (typeof (data.repetedLandlineNumbers) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Contact Number</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Contact Number</span></a><b style="color:#3E576F;font-size:14px;">' + (data.repetedLandlineNumbers ? data.repetedLandlineNumbers : '-') + '</b></li>';

        htmlMobily += '</ul>';
        htmlMobily += '</li>';
        htmlMobily += '</ul>';
        htmlMobily += '</div>';
        htmlMobily += "<div style='height:165px;width:361px;'><div>";

        htmlMobily += "</div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>" +
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        return htmlMobily;

    };


    this.createContentUghi = function (data) {
        console.log(data);
        var genderIndicator;
        var gentsExist = false;
        var finalBranchName;


        var htmlMobily = '<div style="top: -7px;position:relative;margin-left: 3px;height:210px" class="box span12">';
        htmlMobily += '<ul class="dashboard-list" style="height:200px;"><li class=""><ul class="dashboard-sublist">';

        if (typeof (data.branchCode_orignal) === undefined)
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Code</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Code</span></a><b style="color:#3E576F;font-size:14px;">' + (data.branchCode_orignal ? data.branchCode_orignal : 'N/A') + '</b></li>';

        if (typeof (data.branchName) === undefined)
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Name</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else {
            var branch_name = data.branchName;
            var branch_names = branch_name.length;
            if (branch_names > 17) {
                var branch_name_ughi = branch_name.substr(0, 19) + '...';
            } else {
                branch_name_ughi = branch_name;
            }
            htmlMobily += '<li><a href="#" title="' + branch_name + '"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Outlet Name</span><b style="color:#3E576F;font-size:14px;">' + (branch_name_ughi ? ( (branch_name_ughi != '(NULL)') ? toTitleCase(branch_name_ughi) : 'N/A' ) : 'N/A') + '</b></a></li>';
        }
        if (typeof (data.outlet_type) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Branch Type</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Branch Type</span></a><b style="color:#3E576F;font-size:14px;">' + (data.outlet_type ? ( (data.outlet_type != '(NULL)') ? data.outlet_type : 'N/A' ) : 'N/A') + '</b></li>';

        //if (typeof (data.region_name) == 'undefined')
        //    htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Region</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        //else
        //    htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Region</span></a><b style="color:#3E576F;font-size:14px;">' + (data.region_name ? ( (data.region_name != '(NULL)') ? toTitleCase(data.region_name) : 'N/A') : 'N/A') + '</b></li>';

        if (typeof (data.Merchandiser_code) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Merchandiser Code</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Merchandiser Code</span></a><b style="color:#3E576F;font-size:14px;">' + (data.Merchandiser_code ? ((data.Merchandiser_code != '(NULL)') ? data.Merchandiser_code : 'N/A' ) : 'N/A') + '</b></li>';

        if (typeof (data.merchandiser_name) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Merchandiser Name</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else {
            var merchendise_name = data.merchandiser_name;
            var merchent_name = merchendise_name.length;
            if (merchent_name > 17) {
                var merchantName = merchendise_name.substr(0, 19) + '...';
            } else {
                merchantName = merchendise_name;
            }
            htmlMobily += '<li><a href="#" title="' + merchantName + '"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Merchandiser Name</span><b style="color:#3E576F;font-size:14px;">' + (merchantName ? ( (merchantName != '(NULL)') ? toTitleCase(merchantName) : 'N/A') : 'N/A') + '</b></a></li>';
        }

        if (typeof (data.address) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else {
            var data_address = data.address;
            var address = data_address.length;
            if (address > 17) {
                var merchantAddress = data_address.substr(0, 19) + '...';
            } else {
                merchantAddress = data_address;
            }
            htmlMobily += '<li><a href="#" title="' + merchantAddress + '"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span><b style="color:#3E576F;font-size:14px;">' + (merchantAddress ? ( (merchantAddress != '(NULL)') ? toTitleCase(merchantAddress) : 'N/A') : 'N/A') + '</b></a></li>';
        }
        if (typeof (data.lastUghiSurveyDate) == 'undefined' || typeof (data.lastUghiSurveyTime) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Last Visit Date</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Last Visit Date</span></a><b style="color:#3E576F;font-size:14px;">' + (data.lastVisitDate ? ((data.lastVisitDate != '(NULL)') ? data.lastVisitDate : 'N/A') : 'N/A') + '</b></li>';



        htmlMobily += '</ul>';
        htmlMobily += '</li>';
        htmlMobily += '</ul>';
        htmlMobily += '</div>';
        htmlMobily += "<div style='height:165px;width:361px;'><div>";

        htmlMobily += "</div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>" +
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        return htmlMobily;

    };


    this.createContentHrdfOld = function (data) {

        var genderIndicator;
        var gentsExist = false;
        var finalBranchName;


        var htmlMobily = '<div style="top: -7px;position:relative;margin-left: 3px;height:165px" class="box span12">';
        htmlMobily += '<ul class="dashboard-list" style="height:200px;"><li class=""><ul class="dashboard-sublist">';

        if (typeof (data.branchCode) === undefined)
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Branch Code</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Branch Code</span></a><b style="color:#3E576F;font-size:14px;">' + (data.branchCode ? data.branchCode : 'N/A') + '</b></li>';

        if (typeof (data.address) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Address</span></a><b style="color:#3E576F;font-size:14px;">' + (data.address ? data.address : 'N/A') + '</b></li>';

        if (typeof (data.city_category) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Category</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Category</span></a><b style="color:#3E576F;font-size:14px;">' + (data.city_category ? data.city_category : 'N/A') + '</b></li>';

        if (typeof (data.city) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">City</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">City</span></a><b style="color:#3E576F;font-size:14px;">' + (data.city ? data.city : 'N/A') + '</b></li>';

        if (typeof (data.province) == 'undefined')
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Province</span></a><b style="color:#3E576F;font-size:14px;"></b></li>';
        else
            htmlMobily += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Province</span></a><b style="color:#3E576F;font-size:14px;">' + (data.province ? data.province : 'N/A') + '</b></li>';


        htmlMobily += '</ul>';
        htmlMobily += '</li>';
        htmlMobily += '</ul>';
        htmlMobily += '</div>';
        htmlMobily += "<div style='height:165px;width:361px;'><div>";

        htmlMobily += "</div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>" +
                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
        return htmlMobily;

    };


    function getObjectLength(o) {
        var length = 0;

        for (var i in o) {
            if (Object.prototype.hasOwnProperty.call(o, i)) {
                length++;
            }
        }
        return length;
    }


    function tawuniyaImageInfo(data) {
        var html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                "<div align='center' class='title'><b>Branch Code&nbsp;:&nbsp;" + data.branchCode + "</b></div>" +
                "<div style='height:214px;width: 100px;'><div style='padding-left:7px;'><img src=" +
                ((data.image && (data.image != 'null')) ? data.image : 'https://4c360.com/4c-dashboard/public/img/NoImage.jpg') + " style='height: 265px;width: 377px;'/></div></div>" +
                "<div style='width:240px;line-height:10px;margin-top:10px;'><div></div>" +
                "<div></div></div>";
        return html;

    }

    function tabInfo1(data) {
        if (data.branch_manager == '' || data.branch_manager == 'undefined' || data.branch_manager == 'null') {
            $managerName = 'N/A';
        } else {
            $managerName = data.branch_manager;
        }

        var html1 = '';
        html1 = '<div style="top:5px;position:relative;" class="box span12">';
        html1 += '<ul class="dashboard-list" style="height:256px;"><li class=""><ul class="dashboard-sublist">';
        html1 += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Name</span></a><b style="color:#3E576F;font-size:14px;">' + data.branchName + '</b></li>';
        html1 += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Code</span></a><b style="color:#3E576F;font-size:14px;">' + data.branchCode + '</b></li>';
        html1 += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Manager Name</span></a><b style="color: #3E576F;font-size:14px;">' + $managerName + '</b></li>';
        html1 += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Region</span></a><b style="color: #3E576F;font-size:14px;">' + data.province + '</b></li>';
        html1 += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Center City</span></a><b style="color: #3E576F;font-size:14px;">' + data.city + '</b></li>';

        var servicesAvailiable = data.services_availiable.split(',');
        var servicesArrayLenght = getObjectLength(servicesAvailiable);
        if (servicesArrayLenght > 1) {
            html1 += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Other Services</span></a>' + getOtherServiceContent(servicesAvailiable) + '</li>';

        } else {
            html1 += '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Other Services</span></a><b style="color:#3E576F;font-size:14px;"><b style="color:#3884B6;">&#x2713</b>' + data.services_availiable + '</b></li>';
        }

        html1 += '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Health Report</span></a><a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Men" class="myLangChange" style="text-decoration:none;">Health Report</a></li>';

        html1 += '</ul>';
        html1 += '</li>';
        html1 += '</ul>';
        html1 += '</div>';

        return html1;
    }


    function tabInfo2(data) {
        if (data.branch_manager == '' || data.branch_manager == 'undefined' || data.branch_manager == 'null') {
            $managerName = 'N/A';
        } else {
            $managerName = data.branch_manager;
        }

        var html1 = '';
        html1 = '<div style="top:5px;position:relative;" class="box span12">';
        html1 += '<ul class="dashboard-list" style="height:256px;"><li class=""><ul class="dashboard-sublist">';
        html1 += '</ul>';
        html1 += '</div>';

        return html1;
    }


    function getOtherServiceContent(servicesArray)
    {
        $htmlContent = '';
        for (var i in servicesArray) {
            $htmlContent += ' <b style="color:#3E576F;font-size:14px;"><b style="color:#3884B6;">&#x2713</b>' + servicesArray[i] + '</b> ';
        }
        return $htmlContent;
    }

    this.getBranchType = function (data) {

        var html = '';
        if (data.type == 'ATM') {
            html += "<div style='height:40px;width:100%'>";
            html += "<div class='title' align='center'>ATM Code&nbsp;:&nbsp;" + data.branchCode + "&nbsp;</div>";
            return html;
        } else {
            switch (data.branchType.toUpperCase()) {
                case 'LADIES'  :
                    html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title="Ladies Branch">Ladies</span>&nbsp;<img title="Ladies Branch" src="' + baseUrl + 'public/img/ladies.png"></a>';
                    break;
                case 'GENTS'   :
                    html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="Men Branch">Gents</span>&nbsp;<img title="Men Branch" src="' + baseUrl + 'public/img/men.png"></a>';
                    break;
                default        :
                    html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="Men Branch">Gents</span>&nbsp;<img title="Men Branch" src="' + baseUrl + 'public/img/men.png"></a>';
                    html += '&nbsp;&nbsp;<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title="Ladies Branch">Ladies</span>&nbsp;<img title="Ladies Branch" src="' + baseUrl + 'public/img/ladies.png"></a>';
            }
            return html;
        }
    };

    this.getOutletype = function (data) {
        var outletType = (data.outlet_type) ? data.outlet_type : "";
        var html = '';
        html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="">' + outletType + '</span>&nbsp;</a>';
        html += '&nbsp;&nbsp;<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title=""></span>&nbsp;</a>';
        return html;

    };

    this.getUghiOutletype = function (data) {
        var html = '';
        html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="">' + data.outlet_type + '</span>&nbsp;</a>';
        html += '&nbsp;&nbsp;<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title=""></span>&nbsp;</a>';
        return html;

    };

    this.getBranchTypeArabic = function (data) {
        var html = '';
        if (data.type == 'ATM') {
            html += "<div style='height:40px;width:100%'>";
            html += "<div class='title' align='center'>ATM Code&nbsp;:&nbsp;" + data.branchCode + "&nbsp;</div>";
            return html;
        } else {

            switch (data.branchType.toUpperCase()) {
                case 'LADIES'  :
                    html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Ladies"><img title="Ladies Branch" src="' + baseUrl + 'public/img/ladies.png">&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="' + ladiesArabic + ' ' + branchArabic + '">' + ladiesArabic + '</span></a>';
                    break;
                case 'GENTS'   :
                    html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Men"><img title="Men Branch" src="' + baseUrl + 'public/img/men.png">&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="' + gentsArabic + ' ' + branchArabic + '">' + gentsArabic + '</span></a>';
                    break;
                default        :
                    html += '<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Men"><img title="Men Branch" src="' + baseUrl + 'public/img/men.png">&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="' + gentsArabic + ' ' + branchArabic + '">' + gentsArabic + '</span></a>';
                    html += '&nbsp<a target="_blank" href="index.php?code=' + data.branchCode + '&amp;type=Ladies"><img title="Ladies Branch" src="' + baseUrl + 'public/img/ladies.png">&nbsp;&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="' + ladiesArabic + ' ' + branchArabic + '">' + ladiesArabic + '</span></a>';
            }

            return html;
        }
    };

    this.validateImage = function ($img) {
        if ($img == 'null' || $img == undefined || $img == '') {
            if(bankFolder == 'ncb' || bankFolder == 'ncbatm_v7') {
                return baseUrl + 'public/img/NCBProfileImage.png'
            }
            return baseUrl + 'public/img/img_issue_dummy.png'
        }
        if ($original_bank_name == 'Demo Bank') {
            var branchImg = baseUrl + 'public/img/x_image.png';

            return branchImg;
        }
        var branchImg = baseUrl + 'public/img/preview.png';
        if ($img !== '' || $img != undefined)
            branchImg = $img;
        return branchImg;
    };

    this.clearMarker = function () {
        if (this.markerArray) {
            for (var markerArrayIndex in this.markerArray) {
                if (this.markerArray[markerArrayIndex] != 'undefined') {
                    this.markerArray[markerArrayIndex].setMap(null);
                }
            }
        }
        this.markerArray = [];
    };

    this.createIcon = function (type, otherData) {
        var imagePosition = (type == 'atm') ? 1 : 0;
        if (bankFolder == "ncbrebranding") {
            imagePosition = (otherData.is_survey == true) ? 1 : 0;
        }
        return new google.maps.MarkerImage(markerIcons[imagePosition].image, null, null, null, new google.maps.Size(markerIcons[imagePosition].width, markerIcons[imagePosition].height));
    };

    this.createCluster = function () {
        if (this.cluster == undefined) {
            this.cluster = new MarkerClusterer(this.map, this.markerArray, this.clusterOptions);
        } else {
            this.cluster.addMarkers(this.markerArray, false);
        }
    };

    this.clearCluster = function () {
        if ((this.cluster != undefined) && (this.cluster)) {
            this.cluster.clearMarkers();
            this.cluster = null;
        }
    };

    this.getSpace = function ($data) {
        // if(Number($data) <= 9){
//            return "&nbsp;&nbsp;&nbsp;"
//        }
//        if(Number($data) <= 99){
//            return "&nbsp;&nbsp;"
//        }
//        return "&nbsp;"
        return "";
    };


    if (bankFolder == 'rbcapital') {

        this.createList = function (type) {

            $('#scrollbar_' + this.mapType + ' .scrollbar').show();
            $('#scrollbar_atm'.scrollbar).show();
            $content = "<ul class='dashboard-sublist'>";
            $contentATM = "<ul class='dashboard-sublist'>";
            var markerInfo = this.markerInfo;
            var liCount = 0;
            var liCountATM = 0;
            for (var markerInfoIndex in markerInfo) {
                liCount++;
                if (!markerInfo[markerInfoIndex][0]['branchName']) {
                    markerInfo[markerInfoIndex][0]['branchName'] = "";
                }
//           console.log(markerInfo[markerInfoIndex][0]);
                if (markerInfo[markerInfoIndex][0]['type'] == 'ATM') {
                    liCountATM++;
                    $contentATM += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ATM" target="_blank"><span style=" color:#4183C4;width:73%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                            '<i>' + markerInfo[markerInfoIndex][0]['branchCode'] + '</i>' +
                            '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                } else {

                    if (markerInfo[markerInfoIndex][0]['branchType']) {
                        switch ((markerInfo[markerInfoIndex][0]['branchType']).toUpperCase()) {

                            case 'LADIES':
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies&manager=' + markerInfo[markerInfoIndex][0]['managerId'] + '" target="_blank"><b class="ladiesRoundClass"> L </b> </a>' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                            case 'GENTS':
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men&manager=' + markerInfo[markerInfoIndex][0]['managerId'] + '" target="_blank"><b  class="manRoundClass"> G </b></a>' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                            case 'GENTS AND LADIES':
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men&manager=' + markerInfo[markerInfoIndex][0]['managerId'] + '" target="_blank"><b  class="manRoundClass"> G </b></a>&nbsp;<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><b  class="ladiesRoundClass"> L </b></a>' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                            default:
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men&manager=' + markerInfo[markerInfoIndex][0]['managerId'] + '" target="_blank"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                        }

                    }
                }
            }


            if (!liCount) {
                $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                if (this.mapType == 'atm') {
                    $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                }
            }

            $content += "</ul>";
            $contentATM += "</ul>";
            $("#result_counter_" + this.mapType).html((this.markerInfo).length);
            $("#overview_" + this.mapType).html($content);

            if (this.mapType == 'atm') {
                $("#overview_" + this.mapType).html($contentATM);
            }

            //initializeTinyscrollbar(this.mapType);
            initializeScrollbar(this.mapType);

            if (this.mapType == 'atm') {
                if (liCountATM > 18) {
                    $('#scrollbar_atm .scrollbar').show();
                }

            } else {
                if (liCount > 18) {
                    $('#scrollbar_' + this.mapType + ' .scrollbar').show();
                    $('#scrollbar_atm'.scrollbar).show();
                }

            }
        }
    } else if (bankFolder == 'ncbdash') {

        this.createList = function (type) {

            $('#scrollbar_' + this.mapType + ' .scrollbar').show();
            $('#scrollbar_atm'.scrollbar).show();
            $content = "<ul class='dashboard-sublist'>";
            $contentATM = "<ul class='dashboard-sublist'>";
            var markerInfo = this.markerInfo;
            var liCount = 0;
            var liCountATM = 0;
            for (var markerInfoIndex in markerInfo) {
                liCount++;
                if (!markerInfo[markerInfoIndex][0]['branchName']) {
                    markerInfo[markerInfoIndex][0]['branchName'] = "";
                }

                if (markerInfo[markerInfoIndex][0]['type'] == 'ATM') {
                    liCountATM++;
                    $contentATM += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ATM" target="_blank"><span style=" color:#4183C4;width:73%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                            '<i>' + markerInfo[markerInfoIndex][0]['branchCode'] + '</i>' +
                            '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                } else {

                    if (markerInfo[markerInfoIndex][0]['branchType']) {
                        switch ((markerInfo[markerInfoIndex][0]['branchType']).toUpperCase()) {
                            case 'LADIES':
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target = "_blank" class="myLangChange" style="text-decoration:none;"> Progress Report </a> </a>' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                            case 'GENTS':
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"class="myLangChange" style="text-decoration:none;">Progress Report</a>' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                            case 'GENTS AND LADIES':
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" class="myLangChange" style="text-decoration:none;">Progress Report </a>' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                            default:
                                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                        '' +
                                        '<div style="margin-top: -18px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                break;
                        }

                    }
                }
            }


            if (!liCount) {
                $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                if (this.mapType == 'atm') {
                    $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                }
            }

            $content += "</ul>";
            $contentATM += "</ul>";
            $("#result_counter_" + this.mapType).html((this.markerInfo).length);
            $("#overview_" + this.mapType).html($content);

            if (this.mapType == 'atm') {
                $("#overview_" + this.mapType).html($contentATM);
            }

            // initializeTinyscrollbar(this.mapType);
            initializeScrollbar(this.mapType);

            if (this.mapType == 'atm') {
                if (liCountATM > 18) {
                    $('#scrollbar_atm .scrollbar').show();
                }

            } else {
                if (liCount > 18) {
                    $('#scrollbar_' + this.mapType + ' .scrollbar').show();
                    $('#scrollbar_atm'.scrollbar).show();
                }

            }
        }

    } else if (bankFolder == "mobily") {
        this.createList = function (type) {

            $('#scrollbar_' + this.mapType + ' .scrollbar').show();
            $('#scrollbar_atm'.scrollbar).show();
            $content = "<ul class='dashboard-sublist'>";
            $contentATM = "<ul class='dashboard-sublist'>";
            var markerInfo = this.markerInfo;
            var liCount = 0;
            var liCountATM = 0;

            for (var markerInfoIndex in markerInfo) {
                liCount++;
                if (!markerInfo[markerInfoIndex][0]['branchName']) {
                    markerInfo[markerInfoIndex][0]['branchName'] = "";
                }
                $outltType = '';

                if (markerInfo[markerInfoIndex][0]['outlet_type'] == "FBO" || markerInfo[markerInfoIndex][0]['outlet_type'] == "FSO") {
                    $outltType = 'big_shop';
                }

                if (markerInfo[markerInfoIndex][0]['outlet_type'] == "MFBO") {
                    $outltType = 'small_shop';
                }

                if (markerInfo[markerInfoIndex][0]['outlet_type'] == "Kiosk") {
                    $outltType = 'kiosk';
                }
                $outletType = (markerInfo[markerInfoIndex][0]['outlet_type']) ? (markerInfo[markerInfoIndex][0]['outlet_type']).toUpperCase() : '';
                if (markerInfo[markerInfoIndex][0]['outletScore'] >= 91) {
                    $scoreColorClass = 'manColorRoundGreaterClass';
                } else if (markerInfo[markerInfoIndex][0]['outletScore'] >= 85) {
                    $scoreColorClass = 'manColorRoundMediumClass';
                } else {
                    $scoreColorClass = 'manColorRoundSmallerClass';
                }
                var $branchName = (((markerInfo[markerInfoIndex][0]['branchName']).trim()).length > 30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0, 30) + '...' : (markerInfo[markerInfoIndex][0]['branchName']).trim();
                if (bankVersion == '5') {
                    $closed = '';
                    $closedStyle = '';
                    if (markerInfo[markerInfoIndex][0]['is_active'] == '0') {
                        $closed = '<span class="closed"><b> Outlet is closed now.</b></span>';
                        $closed = '';
                        $closedStyle = 'closed-pstyle';
                    }
                    if ($.inArray(markerInfo[markerInfoIndex][0]['branchCode'], $closedOutletduringVisit) !== -1) {
                        $closed = '<span class="closed"><b> Outlet is closed during visit.</b></span>';
                        $closed = '';
                        $closedStyle = 'visit-closed-pstyle';
                    }
                    $content += '<li class="' + $closedStyle + '" id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="position:relative;height:36px; padding-bottom:20px;">' + $closed + '<span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&group_name=' + group_name + '&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>&nbsp;&nbsp;-&nbsp;<b title="' + $branchName + '">' + $outletType + ', ' + (markerInfo[markerInfoIndex][0]['city']) + '</b></span></a>';
                } else {
                    $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="position:relative;height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&shop_type=' + $outltType + '&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>&nbsp;&nbsp;-&nbsp;<b title="' + $branchName + '">' + $outletType + ', ' + (markerInfo[markerInfoIndex][0]['city']) + '</b></span></a>';
                }

                if ($outletType) {
                    switch (markerInfo[markerInfoIndex][0]['outlet_type'].toUpperCase()) {
                        case 'FBO':
                            if (markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined && markerInfo[markerInfoIndex][0]['outletScore'] == 0) {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + '--' + '</b></span>';
                            }
                            else {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + markerInfo[markerInfoIndex][0]['outletScore'] + '%</b></span>';
                            }
                            break;
                        case 'FSO':
                            //$content += '<span class="label label-inverse">'+markerInfo[markerInfoIndex][0]['outletScore']+'</span>';break;
                            //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['outletScore']+'%</b></span>';break;
                            if (markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined && markerInfo[markerInfoIndex][0]['outletScore'] == 0) {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + '--' + '</b></span>';
                            }
                            else {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + markerInfo[markerInfoIndex][0]['outletScore'] + '%</b></span>';
                            }
                            break;
                        case 'MFBO':
                            //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['outletScore']+'%</b></span>';break;
                            if (markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined && markerInfo[markerInfoIndex][0]['outletScore'] == 0) {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + '--' + '</b></span>';
                            }
                            else {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + markerInfo[markerInfoIndex][0]['outletScore'] + '%</b></span>';
                            }
                            break;
                        case 'KIOSK':
                            //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['outletScore']+'%</b></span>';break;
                            if (markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined && markerInfo[markerInfoIndex][0]['outletScore'] == 0) {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + '--' + '</b></span>';
                            }
                            else {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + markerInfo[markerInfoIndex][0]['outletScore'] + '%</b></span>';
                            }
                            break;
                        case 'OFFICES':
                            //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['outletScore']+'%</b></span>';break;
                            if (markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined && markerInfo[markerInfoIndex][0]['outletScore'] == 0) {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + '--' + '</b></span>';
                            }
                            else {
                                $content += '<span class="' + $scoreColorClass + '"><b>' + markerInfo[markerInfoIndex][0]['outletScore'] + '%</b></span>';
                            }
                            break;
                        default:
                            $content += '<span class="' + $scoreColorClass + '"><b>' + markerInfo[markerInfoIndex][0]['outletScore'] + '%</b></span>';
                            break;
                    }
                }
                if (markerInfo[markerInfoIndex][0]['lastVisitDate'] && markerInfo[markerInfoIndex][0]['lastVisitDate'] != "NA")
                    $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                else
                    $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-danger" style="background-color: #DC3333;">Survey Not Performed Yet.</span></div></li>';
            }

            if (!liCount) {
                $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                if (this.mapType == 'atm') {
                    $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                }
            }

            $content += "</ul>";
            $contentATM += "</ul>";
            $("#result_counter_" + this.mapType).html((this.markerInfo).length);
            $("#overview_" + this.mapType).html($content);

            if (this.mapType == 'atm') {
                $("#overview_" + this.mapType).html($contentATM);
            }

            // initializeTinyscrollbar(this.mapType);
            initializeScrollbar(this.mapType);

            if (this.mapType == 'atm') {
                if (liCountATM > 18) {
                    $('#scrollbar_atm .scrollbar').show();
                }

            } else {
                if (liCount > 18) {
                    $('#scrollbar_' + this.mapType + ' .scrollbar').show();
                    $('#scrollbar_atm'.scrollbar).show();
                }

            }
        }
    } else if (bankFolder == "ughi") {
        this.createList = function (type) {

            $('#scrollbar_' + this.mapType + ' .scrollbar').show();
            $('#scrollbar_atm'.scrollbar).show();
            $content = "<ul class='dashboard-sublist'>";
            $contentATM = "<ul class='dashboard-sublist'>";
            var markerInfo = this.markerInfo;
            var liCount = 0;
            var liCountATM = 0;

            for (var markerInfoIndex in markerInfo) {
                liCount++;
                if (!markerInfo[markerInfoIndex][0]['branchName']) {
                    markerInfo[markerInfoIndex][0]['branchName'] = "";
                }
                $outltType = markerInfo[markerInfoIndex][0]['outlet_type'];

                //$scoreColorClass =(markerInfo[markerInfoIndex][0]['outletScore']>=98)?'manColorRoundGreaterClass':'manColorRoundSmallerClass';
                var $branchName = (((markerInfo[markerInfoIndex][0]['branchName']).trim()).length > 30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0, 30) + '...' : (markerInfo[markerInfoIndex][0]['branchName']).trim();
                $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="ughiindex.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>&nbsp;&nbsp;-&nbsp;<b title="' + $branchName + '">' + ((markerInfo[markerInfoIndex][0]['branchName']!= '(NULL)') ? (markerInfo[markerInfoIndex][0]['branchName']): 'N/A').toUpperCase() + ', ' + ((markerInfo[markerInfoIndex][0]['region_name']!= '(NULL)') ? (markerInfo[markerInfoIndex][0]['region_name']) : 'N/A') + '</b></span></a>';
                if (markerInfo[markerInfoIndex][0]['stockTickets'] != 0 && typeof markerInfo[markerInfoIndex][0]['stockTickets'] != 'undefined') {
                    $content += '<span title="Total active stock tickets" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['stockTickets']) + '</b></span>';
                } else if (markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                    $content += '<span title="No active stock tickets" class="manColorRoundGreaterClass"><b>0</b></span>';
                } else {
                    $content += '<span title="No survey found" class="manColorRoundGreaterClass"><b>0</b></span>';
                }

                if (markerInfo[markerInfoIndex][0]['lastVisitDate'] && markerInfo[markerInfoIndex][0]['lastVisitDate'] != 'N/A')
                    $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                else
                    $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Survey Not Performed Yet.</span></div></li>';
            }

            if (!liCount) {
                $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                if (this.mapType == 'atm') {
                    $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                }
            }

            $content += "</ul>";
            $contentATM += "</ul>";
            $("#result_counter_" + this.mapType).html((this.markerInfo).length);
            $("#overview_" + this.mapType).html($content);

            if (this.mapType == 'atm') {
                $("#overview_" + this.mapType).html($contentATM);
            }

            // initializeTinyscrollbar(this.mapType);
            initializeScrollbar(this.mapType);

            if (this.mapType == 'atm') {
                if (liCountATM > 18) {
                    $('#scrollbar_atm .scrollbar').show();
                }

            } else {
                if (liCount > 18) {
                    $('#scrollbar_' + this.mapType + ' .scrollbar').show();
                    $('#scrollbar_atm'.scrollbar).show();
                }

            }
        }
    } else if (bankFolder == "hrdf") {
        this.createList = function (type) {

            $('#scrollbar_' + this.mapType + ' .scrollbar').show();
            $('#scrollbar_atm'.scrollbar).show();
            $content = "<ul class='dashboard-sublist'>";
            $contentATM = "<ul class='dashboard-sublist'>";
            var markerInfo = this.markerInfo;

            var liCount = 0;
            var liCountATM = 0;
            var url = document.URL;
            branchListObjForHRDF = {};
            for (var markerInfoIndex in markerInfo)
            {


                $listConent = '';
                if (!markerInfo[markerInfoIndex][0]['branchName']) {
                    markerInfo[markerInfoIndex][0]['branchName'] = "";
                }

                // We dont have mapping for group_name so we are using like this.

                //$scoreColorClass =(markerInfo[markerInfoIndex][0]['outletScore']>=98)?'manColorRoundGreaterClass':'manColorRoundSmallerClass';
                //  var $branchName=(((markerInfo[markerInfoIndex][0]['branchName']).trim()).length >30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0,30)+'...'  : (markerInfo[markerInfoIndex][0]['branchName']).trim() ;
//            $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b>&nbsp;&nbsp;-&nbsp;<b title="'+$branchName+'">'+(markerInfo[markerInfoIndex][0]['branchName']).toUpperCase()+', '+(markerInfo[markerInfoIndex][0]['region_name'])+'</b></span></a>';
//            markerInfo[markerInfoIndex][0]['outlet_type'].toUpperCase()

                if (markerInfo[markerInfoIndex][0]['branchType'] !== null && (markerInfo[markerInfoIndex][0]['branchType']).toUpperCase() == group_name.toUpperCase()) {

                    switch ((group_name).toUpperCase()) {
                        case "SMALL OUTLET" :

                            liCount++;
                            $visitType = (parseInt(markerInfo[markerInfoIndex][0]['monthly_visit'])) ? 'M' : 'Q';
                            var $branchName = (((markerInfo[markerInfoIndex][0]['branchName']).trim()).length > 30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0, 30) + '...' : (markerInfo[markerInfoIndex][0]['branchName']).trim();

                            $visit = '<span class="label label-success" style="background-color:#0061A7; color:white; width:12px; margin-left:5px; text-align:left !important;">' + $visitType + '</span>';
                            $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span>\n\
                                    <a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=">\n\
                                    <span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue">\n\
                                    <span style= "width: 40px; margin-left:-20px"><b style=" text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b></span>&nbsp;&nbsp; ' + $visit + '-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';
                            $listConent += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style=" color:black; padding-left:10px; padding-left:10px; text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>' + $visit + '</b>&nbsp;&nbsp;-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';


//                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';
//                        $listConent += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';


                            if (markerInfo[markerInfoIndex][0]['outletScore'] != 0 && markerInfo[markerInfoIndex][0]['outletScore'] < 90 && typeof markerInfo[markerInfoIndex][0]['outletScore'] != 'undefined') {
                                $content += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else if (markerInfo[markerInfoIndex][0]['outletScore'] >= 90) {
                                $content += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else {
                                if (!markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                }
                                else {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                }
                            }

                            if (markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                            } else {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                            }

                            fillBranchListObjectHRDF(markerInfo[markerInfoIndex][0]['outletScore'], markerInfo[markerInfoIndex][0]['branchCode'], $listConent);
                            break;

                        case "SHARE OFFICE" :

                            liCount++;
                            $visitType = (parseInt(markerInfo[markerInfoIndex][0]['monthly_visit'])) ? 'M' : 'Q';
                            var $branchName = (((markerInfo[markerInfoIndex][0]['branchName']).trim()).length > 30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0, 30) + '...' : (markerInfo[markerInfoIndex][0]['branchName']).trim();

                            $visit = '<span class="label label-success" style="background-color:#0061A7; color:white; width:12px; margin-left:5px; text-align:left !important;">' + $visitType + '</span>';
                            $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span>\n\
                                <a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=">\n\
                                <span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue">\n\
                                <span style= "width: 40px; margin-left:-20px"><b style="text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b></span>&nbsp;&nbsp; ' + $visit + '-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';
                            $listConent += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style=" color:black; padding-left:10px; text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>' + $visit + '&nbsp;&nbsp;-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';


//                    $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';
//                    $listConent += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';


                            if (markerInfo[markerInfoIndex][0]['outletScore'] != 0 && markerInfo[markerInfoIndex][0]['outletScore'] < 90 && typeof markerInfo[markerInfoIndex][0]['outletScore'] != 'undefined') {
                                $content += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else if (markerInfo[markerInfoIndex][0]['outletScore'] >= 90) {
                                $content += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else {
                                if (!markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                }
                                else {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                }
                            }

                            if (markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                            } else {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                            }

                            fillBranchListObjectHRDF(markerInfo[markerInfoIndex][0]['outletScore'], markerInfo[markerInfoIndex][0]['branchCode'], $listConent);
                            break;
                        case "BIG OUTLET" :

                            liCount++;
                            $visitType = (parseInt(markerInfo[markerInfoIndex][0]['monthly_visit'])) ? 'M' : 'Q';
                            var $branchName = (((markerInfo[markerInfoIndex][0]['branchName']).trim()).length > 30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0, 30) + '...' : (markerInfo[markerInfoIndex][0]['branchName']).trim();

                            $visit = '<span class="label label-success" style="background-color:#0061A7; color:white; width:12px; margin-left:5px; text-align:left !important;">' + $visitType + '</span>';
                            $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span>\n\
                                    <a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=">\n\
                                    <span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue">\n\
                                    <span style= "width: 40px; margin-left:-20px"><b style="text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b></span>&nbsp;&nbsp; ' + $visit + '-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';
                            $listConent += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase; padding-left:10px; color:black;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>' + $visit + '&nbsp;&nbsp;-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';


//                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';
//                        $listConent += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';


                            if (markerInfo[markerInfoIndex][0]['outletScore'] != 0 && markerInfo[markerInfoIndex][0]['outletScore'] < 90 && typeof markerInfo[markerInfoIndex][0]['outletScore'] != 'undefined') {
                                $content += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else if (markerInfo[markerInfoIndex][0]['outletScore'] >=90) {
                                $content += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else {
                                if (!markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                }
                                else {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                }
                            }

                            if (markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                            } else {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                            }

                            fillBranchListObjectHRDF(markerInfo[markerInfoIndex][0]['outletScore'], markerInfo[markerInfoIndex][0]['branchCode'], $listConent);
                            break;
                            case "MEDIUM OUTLET" :

                            liCount++;
                            $visitType = (parseInt(markerInfo[markerInfoIndex][0]['monthly_visit'])) ? 'M' : 'Q';
                            var $branchName = (((markerInfo[markerInfoIndex][0]['branchName']).trim()).length > 30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0, 30) + '...' : (markerInfo[markerInfoIndex][0]['branchName']).trim();

                            $visit = '<span class="label label-success" style="background-color:#0061A7; color:white; width:12px; margin-left:5px; text-align:left !important;">' + $visitType + '</span>';
                            $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span>\n\
                                    <a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=">\n\
                                    <span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue">\n\
                                    <span style= "width: 40px; margin-left:-20px"><b style="text-transform: lowercase;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b></span>&nbsp;&nbsp; ' + $visit + '-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';
                            $listConent += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase; padding-left:10px; color:black;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>' + $visit + '&nbsp;&nbsp;-&nbsp;<b title="' + $branchName + '">' + (markerInfo[markerInfoIndex][0]['branchName']).toUpperCase() + '</b></span></a>';


//                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';
//                        $listConent += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px; padding-bottom:20px;"><span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+ markerInfo[markerInfoIndex][0]['branchCode'] +'&type=outlet"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b></span></a>';


                            if (markerInfo[markerInfoIndex][0]['outletScore'] != 0 && markerInfo[markerInfoIndex][0]['outletScore'] < 90 && typeof markerInfo[markerInfoIndex][0]['outletScore'] != 'undefined') {
                                $content += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundSmallerClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else if (markerInfo[markerInfoIndex][0]['outletScore'] >= 90) {
                                $content += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';
                                $listConent += '<span title="Total Score" class="manColorRoundGreaterClass"><b>' + (markerInfo[markerInfoIndex][0]['outletScore']) + '</b></span>';

                            } else {
                                if (!markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>-</b></span>';
                                }
                                else {
                                    $content += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                    $listConent += '<span title="No survey found" class="manColorRoundSmallerClass"><b>0</b></span>';
                                }
                            }

                            if (markerInfo[markerInfoIndex][0]['lastVisitDate']) {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">' + markerInfo[markerInfoIndex][0]['lastVisitDate'] + '</span></div></li>';
                            } else {
                                $content += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                                $listConent += '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-important" style="color:#FFFFFF;">Visit not yet performed.</span></div></li>';
                            }

                            fillBranchListObjectHRDF(markerInfo[markerInfoIndex][0]['outletScore'], markerInfo[markerInfoIndex][0]['branchCode'], $listConent);
                            break;
                    }

                }

            }

            if (!liCount) {
                $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                if (this.mapType == 'atm') {
                    $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                }
            }

            $content += "</ul>";
            $contentATM += "</ul>";
            $("#result_counter_" + this.mapType).html((liCount));
            $("#overview_" + this.mapType).html($content);

            if (this.mapType == 'atm') {
                $("#overview_" + this.mapType).html($contentATM);
            }

            // initializeTinyscrollbar(this.mapType);
            initializeScrollbar(this.mapType);

            if (this.mapType == 'atm') {
                if (liCountATM > 18) {
                    $('#scrollbar_atm .scrollbar').show();
                }

            } else {
                if (liCount > 18) {
                    $('#scrollbar_' + this.mapType + ' .scrollbar').show();
                    $('#scrollbar_atm'.scrollbar).show();
                }
            }
        }
    } else if (bankFolder == "arbbranches_v2") {
        this.createList = function (type) {

            $('#scrollbar_' + this.mapType + ' .scrollbar').show();
            $('#scrollbar_atm'.scrollbar).show();
            $content = "";
            $content = "<ul class='dashboard-sublist'>";
            $content += '<li  style="height:36px;"><a href="#" target="_blank" style="margin-left:310px;"><img src="https://4c360.com/4c-dashboard/public/img/men.png" title="Men Branch"></a>&nbsp;<a href="#" style="margin-left: 18px;" target="_blank"><img src="https://4c360.com/4c-dashboard/public/img/ladies.png" title="Ladies Branch"></a></li>';
            var markerInfo = this.markerInfo;
            //console.log(markerInfo);
            var liCount = 0;
            var liCountATM = 0;
            branchListObj = {};
            $listItems = '';

            for (var markerInfoIndex in markerInfo) {
                liCount++;
                $listItems='';
                if (!markerInfo[markerInfoIndex][0]['branchName']) {
                    markerInfo[markerInfoIndex][0]['branchName'] = "";
                }



                $outletType = (markerInfo[markerInfoIndex][0]['branchType']) ? (markerInfo[markerInfoIndex][0]['branchType']).toUpperCase() : '';
                if (markerInfo[markerInfoIndex][0]['branchScore'] >= 91) {
                    $scoreColorClass = 'manColorRoundGreaterClass';
                } else if (markerInfo[markerInfoIndex][0]['branchScore'] >= 85) {
                    $scoreColorClass = 'manColorRoundMediumClass';
                } else {
                    $scoreColorClass = 'manColorRoundSmallerClass';
                }
                var $branchName = (((markerInfo[markerInfoIndex][0]['branchName']).trim()).length > 30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0, 30) + '...' : (markerInfo[markerInfoIndex][0]['branchName']).trim();

                $closed = '';
                $closedStyle = '';
                if (markerInfo[markerInfoIndex][0]['is_active'] == '0') {
                    $closed = '<span class="closed"><b> Outlet is closed now.</b></span>';
                    $closed = '';
                    $closedStyle = 'closed-pstyle';
                }
                if ($.inArray(markerInfo[markerInfoIndex][0]['branchCode'], $closedOutletduringVisit) !== -1) {
                    $closed = '<span class="closed"><b> Outlet is closed during visit.</b></span>';
                    $closed = '';
                    $closedStyle = 'visit-closed-pstyle';
                }
                $menScore = $.isNumeric( markerInfo[markerInfoIndex][0]['branchScoreGents'])?  markerInfo[markerInfoIndex][0]['branchScoreGents']+'%':  markerInfo[markerInfoIndex][0]['branchScoreGents'] ;
                $ladiesScore = $.isNumeric( markerInfo[markerInfoIndex][0]['branchScoreLadies'])?  markerInfo[markerInfoIndex][0]['branchScoreLadies']+'%': markerInfo[markerInfoIndex][0]['branchScoreLadies'];
                $menColorClass = (markerInfo[markerInfoIndex][0]['branchScoreGents'] >= 98) ? 'manColorRoundGreaterClass' : 'manColorRoundSmallerClass';
                $ladiesColorClass = (markerInfo[markerInfoIndex][0]['branchScoreLadies'] >= 98) ? 'ladiesRoundGreaterClass' : 'ladiesRoundSmallerClass';

                $listItems +='<li class="' + $closedStyle + '" id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="position:relative;padding-bottom:20px;">' + $closed + '<span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" ><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:-10px;" class="blue"><b style="text-transform: lowercase !important;">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</b>&nbsp;&nbsp;-&nbsp;<b style="text-transform: lowercase !important;" title="' + $branchName + '">' + $branchName + ', '+markerInfo[markerInfoIndex][0]['city'] +  '</b></span></a>';
                if((markerInfo[markerInfoIndex][0]['lastVisitDate'] && markerInfo[markerInfoIndex][0]['lastVisitDate'] != "NA") ||
                    (markerInfo[markerInfoIndex][0]['lastVisitDateL'] && markerInfo[markerInfoIndex][0]['lastVisitDateL'] != "NA") ) {
                    $listItems += '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a style="margin-left: -23px;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=MEN&survey_id=' + survey_id + '" target="_blank"><b class="' + $menColorClass + '" >' + (($menScore < 100) ?
                            ('&nbsp;' + $menScore + '&nbsp;') : ($menScore)) + '</b></a>' +

                        '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a style="margin-left: 8px;"  href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=LADIES&survey_id=' + survey_id + '" target="_blank"><b class="' + $ladiesColorClass + '" >' + (($ladiesScore < 100) ?
                            ('&nbsp;' + $ladiesScore + '&nbsp;') : ($ladiesScore)) + '</b></a>';
                }



                                        //fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreLadies'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
                                        //fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreGents'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);

//                                        $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;padding:5px 0px 9px 0px;">\n\
//                                        <span class="icon icon-blue icon-triangle-e" style="margin-top: -5px;" ></span>\n\
//                                        <a href="javascript:void()">\n\
//                                        <span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;margin-bottom:5; color: #706D6D;overflow:\n\
//                                        hidden;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' +  '</b>&nbsp;</span></a>' +
//
//                                                '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><b  class="' + $menColorClass + '">' + ((markerInfo[markerInfoIndex][0]['branchScoreGents'] < 100) ?
//                                                ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreGents'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreGents'])) + '</b></a>' +
//
//                                                '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><b  class="' + $menColorClass + '">' + ((markerInfo[markerInfoIndex][0]['branchScoreLadies'] < 100) ?
//                                                ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreLadies'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreLadies'])) + '</b></a>' +
//
//                                                '<div style="margin-top: -20px;margin-left: 37px;"><B>G</B>&nbsp;<span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDateG']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDateG']) ? markerInfo[markerInfoIndex][0]['lastVisitDateG'] : "NA") + '</span>'+
//                                        '&nbsp;&nbsp;&nbsp;<B>L</B>&nbsp;<span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDateL']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDateL']) ? markerInfo[markerInfoIndex][0]['lastVisitDateL'] : "NA") + '</span>'+'</li>';
//                                        $content += $listItems;
//                                        fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreGents'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
//                                         fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreLadies'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);

//                        case 'Ladies':
//
//                                        $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;padding:0px 0px 19px 0px;"><span class="icon icon-blue icon-triangle-e" style="margin-top: -5px;"></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #706D6D;overflow:hidden;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b>&nbsp</span></a>' +
//                                                '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><b class="' + $ladiesColorClass + '" style = "margin-left: 55px;">' + ((markerInfo[markerInfoIndex][0]['branchScoreLadies'] < 100) ?
//                                                ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreLadies'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreLadies'])) + '</b></a>' +
//                                                '<div style="margin-top: -10px ;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
//                                        $content += $listItems;
//
//                                        fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreLadies'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
//                                        break;
//



//                            if (markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined && !$.isNumeric(markerInfo[markerInfoIndex][0]['branchScore'])) {
//                                $content += '<span class="' + $scoreColorClass + '"><b>' + '--' + '</b></span>';
//                            } else {
//                                $content += '<span class="' + $scoreColorClass + '"><b>' + markerInfo[markerInfoIndex][0]['branchScore'] + '%</b></span>';
//                                break;
//                            }



                if((markerInfo[markerInfoIndex][0]['lastVisitDate'] && markerInfo[markerInfoIndex][0]['lastVisitDate'] != "NA") ||
                       (markerInfo[markerInfoIndex][0]['lastVisitDateL'] && markerInfo[markerInfoIndex][0]['lastVisitDateL'] != "NA") ){
                    $listItems += '<div style="margin-left: 37px;">';
                    if (markerInfo[markerInfoIndex][0]['lastVisitDate'] && markerInfo[markerInfoIndex][0]['lastVisitDate'] != "NA"){
                        $listItems += ' &nbsp;&nbsp;<span class="label label-success" style="color:#FFFFFF;"> '+' <span class="label label-info">M</span> '+ markerInfo[markerInfoIndex][0]['lastVisitDate'] + ' </span>';
                    }else{
                        $listItems += ' &nbsp;&nbsp;<span class="label label-success" style="color:#FFFFFF;"> '+' <span class="label label-info">M</span> '+' NA </span>';
                    }

                    if (markerInfo[markerInfoIndex][0]['lastVisitDateL'] && markerInfo[markerInfoIndex][0]['lastVisitDateL'] != "NA"){
                        $listItems += ' &nbsp;&nbsp;<span class="label label-success" style="color:#FFFFFF;"> '+' <span class="label " style="background-color:#DF2B6A">L</span> '+ markerInfo[markerInfoIndex][0]['lastVisitDateL'] + ' </span>';
                    }else{
                        $listItems += ' &nbsp;&nbsp;<span class="label label-success" style="color:#FFFFFF;"> '+' <span class="label label-info" style="background-color:#DF2B6A">L</span> '+' NA</span>';
                    }
                }else{
                    $listItems += '<div class="pull-right" style="margin-right: 50px;">';
                    $listItems += ' &nbsp;&nbsp;<span class="label label-danger" style="background-color: #DC3333;">No Audit.</span>';
                }

                $listItems+= '</div></li>';

            fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreGents'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
            $content += $listItems;
            }

            if (!liCount) {

                $listItems += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
            }

            $content += "</ul>";
            $("#result_counter_" + this.mapType).html((this.markerInfo).length);
            $("#overview_" + this.mapType).html($content);

            if (this.mapType == 'atm') {
                $("#overview_" + this.mapType).html($content);
            }

            initializeScrollbar(this.mapType);

            if (this.mapType == 'atm') {
                if (liCountATM > 18) {
                    $('#scrollbar_atm .scrollbar').show();
                }

            } else {
                if (liCount > 18) {
                    $('#scrollbar_' + this.mapType + ' .scrollbar').show();
                    $('#scrollbar_atm'.scrollbar).show();
                }

            }
        }
    } else {

        this.createList = function (type) {
            $('#scrollbar_' + this.mapType + ' .scrollbar').show();
            $('#scrollbar_atm'.scrollbar).show();
            $content = "<ul class='dashboard-sublist'>";
            $contentATM = "<ul class='dashboard-sublist'>";
            var markerInfo = this.markerInfo;
            var liCount = 0;
            var liCountATM = 0;
            var firstFlag = 0;
//        console.log(markerInfo);exit;
            var $profilePage= "index.php";
            if(bankFolder == "arbasset"){
              $profilePage = "assetIndex.php";
            }
            branchListObj = {};
            for (var markerInfoIndex in markerInfo) {
                liCount++;
                if (!markerInfo[markerInfoIndex][0]['branchName']) {
                    markerInfo[markerInfoIndex][0]['branchName'] = "";
                }

                if (markerInfo[markerInfoIndex][0]['type'] == 'ATM') {
                    liCountATM++;
                    $contentATM += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ATM" target="_blank"><span style=" color:#4183C4;width:73%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                            '<i>' + markerInfo[markerInfoIndex][0]['branchCode'] + '</i>' +
                            '</li>';
                } else {
                    if (markerInfo[markerInfoIndex][0]['branchType']) {

                        if (bankFolder == 'riyad' || bankFolder == 'ncb' || bankFolder == 'sabb' || bankFolder == 'alrajhi' || bankFolder == 'aljazeera' || bankFolder == 'tawuniya') {
                            if (bankFolder == 'tawuniya') {
                                $menColorClass = (markerInfo[markerInfoIndex][0]['branchScore'] >= 98) ? 'manColorRoundGreaterClass' : 'manColorRoundSmallerClass';

                            } else {
                                $menColorClass = (markerInfo[markerInfoIndex][0]['branchScoreGents'] >= 98) ? 'manColorRoundGreaterClass' : 'manColorRoundSmallerClass';
                                $ladiesColorClass = (markerInfo[markerInfoIndex][0]['branchScoreLadies'] >= 98) ? 'ladiesRoundGreaterClass' : 'ladiesRoundSmallerClass';
                            }

                            if (firstFlag == 0 && bankFolder != 'aljazeera' && bankFolder != 'tawuniya') {
                                $content += '<li  style="height:36px;"><a href="#" target="_blank" style="margin-left:310px;"><img src="https://4c360.com/4c-dashboard/public/img/men.png" title="Men Branch"></a>&nbsp;<a href="#" style="margin-left: 18px;" target="_blank"><img src="https://4c360.com/4c-dashboard/public/img/ladies.png" title="Ladies Branch"></a></li>';
                            }
                            ++firstFlag;
                            $listItems = "";



                            switch ((markerInfo[markerInfoIndex][0]['branchType']).toUpperCase()) {

                                case 'LADIES':

                                    if (bankFolder == 'aljazeera') {

                                        $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #706D6D;overflow:hidden;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                                '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><b class="' + $ladiesColorClass + '" style = "margin: 8px !important;">' + ((markerInfo[markerInfoIndex][0]['branchScoreLadies'] < 100) ?
                                                ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreLadies'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreLadies'])) + '</b></a>' +
                                                '<div style="margin-top: -2px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                        $content += $listItems;

                                        fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreLadies'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
                                        break;
                                    } else {

                                        $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;padding:0px 0px 19px 0px;"><span class="icon icon-blue icon-triangle-e" style="margin-top: -5px;"></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #706D6D;overflow:hidden;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b>&nbsp</span></a>' +
                                                '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><b class="' + $ladiesColorClass + '" style = "margin-left: 55px;">' + ((markerInfo[markerInfoIndex][0]['branchScoreLadies'] < 100) ?
                                                ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreLadies'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreLadies'])) + '</b></a>' +
                                                '<div style="margin-top: -10px ;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                        $content += $listItems;

                                        fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreLadies'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
                                        break;
                                    }/*
                                     $listItems = '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px;padding:5px 0px 5px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #706D6D;overflow:hidden;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchCode']+'   '+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                     ''+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><b class="'+$menColorClass+'">'+((markerInfo[markerInfoIndex][0]['branchScoreLadies']<100)?
                                     ('&nbsp;'+markerInfo[markerInfoIndex][0]['branchScoreLadies']+'&nbsp;'):(markerInfo[markerInfoIndex][0]['branchScoreLadies']))+'</b></a>'+
                                     '</li>';
                                     $content    += $listItems;

                                     fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreLadies'],markerInfo[markerInfoIndex][0]['branchCode'],$listItems);
                                     break;*/
                                case 'GENTS':
                                    if (bankFolder == "tawuniya") {
                                        $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" style="margin-top: -5px;" ></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;margin-bottom:5; color: #706D6D;overflow:hidden;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b>&nbsp;</span></a>' +
                                                '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><b  class="' + $menColorClass + '">' + ((markerInfo[markerInfoIndex][0]['branchScore'] < 100) ?
                                                ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScore'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScore'])) + '</b></a>' +
                                                '<div style="margin-top: -20px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                        $content += $listItems;
                                        fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScore'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
                                    } else {
                                        $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" style="margin-top: -5px;" ></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;margin-bottom:5; color: #706D6D;overflow:hidden;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b>&nbsp;</span></a>' +
                                                '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><b  class="' + $menColorClass + '">' + ((markerInfo[markerInfoIndex][0]['branchScoreGents'] < 100) ?
                                                ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreGents'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreGents'])) + '</b></a>' +
                                                '<div style="margin-top: -20px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                        $content += $listItems;
                                        fillBranchListObject(markerInfo[markerInfoIndex][0]['branchScoreGents'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
                                    }

                                    break;

                                case 'GENTS AND LADIES':
                                    $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;padding:5px 0px 19px 0px;"><span class="icon icon-blue icon-triangle-e" style="margin-top: -5px;" ></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #706D6D;overflow:hidden;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b>&nbsp;</span></a>' +
                                            '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><b class="' + $menColorClass + '">' + ((markerInfo[markerInfoIndex][0]['branchScoreGents'] < 100) ?
                                            ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreGents'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreGents'])) + '</b></a>&nbsp;<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><b style="margin-left:-10px" class="' + $ladiesColorClass + '">' + ((markerInfo[markerInfoIndex][0]['branchScoreLadies'] < 100) ?
                                            ('&nbsp;' + markerInfo[markerInfoIndex][0]['branchScoreLadies'] + '&nbsp;') : (markerInfo[markerInfoIndex][0]['branchScoreLadies'])) + '</b></a>' +
                                            '<div style="margin-top: -12px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                    $content += $listItems;
                                    fillBranchListObject((markerInfo[markerInfoIndex][0]['branchScoreGents']) ? markerInfo[markerInfoIndex][0]['branchScoreGents'] : markerInfo[markerInfoIndex][0]['branchScoreLadies'], markerInfo[markerInfoIndex][0]['branchCode'], $listItems);
                                    break;

                                default:
                                    $listItems = '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><span style=" color:#706D6D;width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>' + markerInfo[markerInfoIndex][0]['branchCode'] + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40) + '</b></span></a>' +
                                            '' +
                                            '</li>';
                                    $content += $listItems;
                                    //branchListObj[markerInfo[markerInfoIndex][0]['branchCode']] = $listItems;
                                    break;
                            }


                        } else {

                            if (bankFolder == 'medgulf') {

                                $contentToAppend = '<div style="margin-top: -4px;margin-left: 37px;"></div></li>';
                                // $contentToAppend =   '<div style="margin-top: -4px;margin-left: 37px;"><span class="label label-'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ?"success" : "important")+'" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA")+'</span></div></li>';

                            } else {
                                $contentToAppend = '<div style="margin-top: -4px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';

                            }

                            var branchCode = markerInfo[markerInfoIndex][0]['branchCode'];
                            if (bankFolder == 'ncbrebranding' && branchCode == 440123) {
                                branchCode = '440-Old';
                            }
                            //console.log(markerInfo);
                            switch ((markerInfo[markerInfoIndex][0]['branchType']).toUpperCase()) {

                                case 'LADIES':
                                    $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + branchCode + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b></span></a>' +
                                            '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="'+$profilePage+'?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><img src="' + baseUrl + 'public/img/ladies.png"  title="Ladies Branch"/></a>' +
                                            '<div style="margin-top: -4px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                    break;
                                case 'GENTS':
                                     if (bankFolder == 'arbasset') {
                                    $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:36px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + branchCode + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b></span></a>' +
                                            '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="'+$profilePage+'?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><img src="' + baseUrl + 'public/img/men.png"   title="Men Branch"/></a>' + $contentToAppend;
                                }
                                else {
                                     $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + branchCode + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b></span></a>' +
                                            '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="'+$profilePage+'?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><img src="' + baseUrl + 'public/img/men.png"   title="Men Branch"/></a>' + $contentToAppend;
                                }
                                    break;
                                case 'GENTS AND LADIES':
                                    $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:' + $listWidth + ';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>' + branchCode + '    ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b></span></a>' +
                                            '' + this.getSpace(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="'+$profilePage+'?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><img src="' + baseUrl + 'public/img/men.png"   title="Men Branch"/></a>&nbsp;<a href="'+$profilePage+'?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><img src="' + baseUrl + 'public/img/ladies.png"  title="Ladies Branch"/></a>' +
                                            '<div style="margin-top: -4px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';
                                    break;
                                default:
                                    $content += '<li id="bankCode_' + markerInfo[markerInfoIndex][0]['branchCode'] + '" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>' + branchCode + '   ' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 40)+ ', ' + (markerInfo[markerInfoIndex][0]['city'])  + '</b></span></a>' +
                                        '' +
                                        '<div style="margin-top: -4px;margin-left: 37px;"><span class="label label-' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? "success" : "important") + '" style="color:#FFFFFF;">' + ((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA") + '</span></div></li>';


                                    break;

                            }
                        }

                    }
                }

            }

            if (!liCount) {
                $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                if (this.mapType == 'atm') {
                    $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
                }
            }

            $content += "</ul>";
            $contentATM += "</ul>";
            $("#result_counter_" + this.mapType).html((this.markerInfo).length);
            $("#overview_" + this.mapType).html($content);

            if (this.mapType == 'atm') {
                $("#overview_" + this.mapType).html($contentATM);
            }

            initializeScrollbar(this.mapType);
            //initializeTinyscrollbar(this.mapType);

            if (this.mapType == 'atm') {
                if (liCountATM > 18) {
                    $('#scrollbar_atm .scrollbar').show();
                }

            } else {
                if (liCount > 18) {
                    $('#scrollbar_' + this.mapType + ' .scrollbar').show();
                    $('#scrollbar_atm'.scrollbar).show();
                }

            }
        }



    }



    function fillBranchListObject($score, $code, $content) {

        $score = Number($score);
        if (Number($score) == 'NaN') {
            $score = 0;
        }
        if (typeof branchListObj[$score] == 'undefined')
            branchListObj[$score] = {};

        branchListObj[$score][$code] = $content;
        // console.log(branchListObj);
    }

    function fillBranchListObjectHRDF($score, $code, $content) {

        $score = Number($score);
        if (Number($score) == 'NaN') {
            $score = 0;
        }
        if (typeof branchListObjForHRDF[$score] == 'undefined')
            branchListObjForHRDF[$score] = {};

        branchListObjForHRDF[$score][$code] = $content;
    }

    function checkBranchCode(branchCode) {

        if (adminBranches.length) {
            for (var i in adminBranches) {
                if (adminBranches[i] == branchCode)
                    return true;
            }

            return false;
        } else {
            return true;
        }

    }



    function initializeScrollbar($type) {
        //$(document).ready(function(){
        $(".nano").nanoScroller({alwaysVisible: true});
        //});
    }

    function calculateChar(mainStr) {
        if ((String(mainStr).split("")).length == 1) {
            return '&nbsp;&nbsp;&nbsp;&nbsp;';
        } else {
            if ((String(mainStr).split("")).length == 2)
                return '&nbsp;&nbsp;';
            else
                return '&nbsp;';
        }
    }

    var firstTime = 0;
    var flagAgent = false;
    this.createBranchReportList = function (type) {
        if (firstTime == 0) {
            var markerInfo = this.markerInfo;
            if (markerInfo) {
                var addressBranch;
                $reportContent = '<table id="branch-list-box-table" style="width:100%;padding-top: 5px;" class="table table-bordered"><tr>';
                $rowCounter = 1;
                var markerInfo = this.markerInfo;

                for (var markerInfoIndex in markerInfo) {
                    //console.log(markerInfo[markerInfoIndex][0]['name']);
                    if (markerInfo[markerInfoIndex][0]['name'] == 'Al-Rajhi Bank') {
                        markerInfo[markerInfoIndex][0]['name'] = 'arb';
                    }

                    if (markerInfo[markerInfoIndex][0]['name'] == 'Riyad Bank') {
                        if (bankVersion == 'V3') {
                            markerInfo[markerInfoIndex][0]['name'] = 'riyad_V3';
                        } else {
                            markerInfo[markerInfoIndex][0]['name'] = 'riyad';
                        }

                    }

                    if (markerInfo[markerInfoIndex][0]['name'] == 'Al Jazira Bank') {
                        markerInfo[markerInfoIndex][0]['name'] = 'aljazeera';
                    }

                    if (markerInfo[markerInfoIndex][0]['name'] == 'HRDF Bank') {
                        markerInfo[markerInfoIndex][0]['name'] = 'hrdf';
                    }

                    if (!markerInfo[markerInfoIndex][0]['branchName']) {
                        markerInfo[markerInfoIndex][0]['branchName'] = "";
                    }

                    if ($rowCounter > 4) {
                        $rowCounter = 1;
                        $reportContent += '</tr><tr>';
                    }
                    //console.log('oop' + markerInfo.length);
                     //console.log("here");exit;
                    if (checkBranchCode(markerInfo[markerInfoIndex][0]['branchCode'])) {

                        if (markerInfo[markerInfoIndex][0]['isCapitalBranches'] == '2' && bankFolder != 'riyad') {
                            markerInfo[markerInfoIndex][0]['name'] = 'rbcapital';
                        }

                        //console.log('gfgd -> ' + markerInfo[markerInfoIndex][0]['branchType']);
                        var branchTypeTemp = markerInfo[markerInfoIndex][0]['branchType'];
                        if(bankFolder == "arbbranches_v2"){
                            var branchTypeTemp = markerInfo[markerInfoIndex][0]['branchTypeServc'];

                        }

                        /*
                        if(bankFolder == "arbbranches_v2"){
                              branchTypeTemp = (branchTypeTemp == 'LADIES' || branchTypeTemp == 'Ladies Branch') ? "Ladies" : branchTypeTemp ;
                              branchTypeTemp = (branchTypeTemp != 'Ladies' &&  branchTypeTemp != "Mens and Ladies") ? "Gents" : branchTypeTemp ;
                              branchTypeTemp = (branchTypeTemp == "Mens") ? "Gents" : branchTypeTemp;
                              branchTypeTemp = (branchTypeTemp == "Mens and Ladies") ? "Gents and Ladies" : branchTypeTemp;
                        }

                        */
                        //console.log(branchTypeTemp);
                         //console.log(markerInfo);
                        switch (branchTypeTemp) {
                            case 'Ladies':
                                $rowCounter++;
                                flagAgent = true;

                                if(bankFolder == 'arbbranches_v2'){
                                    $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '" zone="' + markerInfo[markerInfoIndex][0]['zone_id'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] + '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+ '">' +
                                    '<span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()">'+
                                    '<span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                    '<br /><br /><span style="margin-left: 22px;"><i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                        '<!--<input type="checkbox" class="branchBasketReportClass" name="branchBasketReport" branchType="Ladies" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                        ' <i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a> '+
                                    '<span class="fa fa-file-pdf-o fa-lg" title="Download Profile Pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>&nbsp;&nbsp;'+
                                    '<span class="fa fa-file-excel-o fa-lg" title="Download Profile Excel"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'excel\')"></span>'+
                                    '<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Ladies" for-branch-report="true" style="cursor:pointer"></span></td>';


                                }else{
                                    if(bankFolder == 'riyad' || bankFolder == 'rbcapital' || bankFolder == 'sabb' || bankFolder == 'ncb' || bankFolder == 'aljazeera'){
                                        $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '" zone="' + markerInfo[markerInfoIndex][0]['zone_id'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] + '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+ '"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                            '<br /><br />' +
                                            '<span style="margin-left: 22px;">' +
                                            '<!--<input type="checkbox" disabled="true" class="branchBasketReportClass" name="branchBasketReport" branchType="Ladies" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                             '<i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>'+
                                            ' <i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] +
                                            '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) +
                                            '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank">' +
                                            '<img src="../public/img/ladies.png"  title="Ladies Branch"/></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>' +
                                            //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Ladies" for-branch-report="true" style="cursor:pointer"></span>' +
                                            '</td>';
                                    }else{
                                        $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '" zone="' + markerInfo[markerInfoIndex][0]['zone_id'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] + '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+ '">' +
                                            '<span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()">' +
                                            '<span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                            '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                            '<!--<input type="checkbox" disabled="true" class="branchBasketReportClass" name="branchBasketReport" branchType="Ladies" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                            '<i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) +
                                            '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a> ' +
                                            '<span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\')"></span>' +
                                            //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Ladies" for-branch-report="true" style="cursor:pointer"></span>' +
                                            '</td>';
                                    }
                                }
                                break;

                            case 'Gents':
                                $rowCounter++;
                                flagAgent = true;

                                $branchCode = markerInfo[markerInfoIndex][0]['branchCode'];
                                if (markerInfo[markerInfoIndex][0]['name'] == "Tawuniya Insurance") {
                                    $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '"  area="' + markerInfo[markerInfoIndex][0]['area_name'] + '"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                            '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                        '<!--<input type="checkbox" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] +'"> -->' +
                                        '<i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" style="margin-right: 3px;"><img src="../public/img/men.png"  title="Men Branch"/></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\')"></span><span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Men" for-branch-report="true" style="cursor:pointer"></span></td>';
                                } else {

                                    if(bankFolder == 'arbbranches_v2'){

                                        $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '"  zone="' + markerInfo[markerInfoIndex][0]['zone_id'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] +  '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+  '"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                        '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                            '<!--<input type="checkbox" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                            ' <i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank">'
                                            + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" style="margin-right: 3px;"><img src="../public/img/men.png"  title="Men Branch"/></a> '+
                                        '<span class="fa fa-file-pdf-o fa-lg" title="Download Profile Pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>&nbsp;&nbsp;' +
                                        '<span class="fa fa-file-excel-o fa-lg" title="Download Profile Excel"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'excel\')"></span>' +
                                        '<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Men" for-branch-report="true" style="cursor:pointer"></span></td>';

                                    }else{
                                        if(bankFolder == 'riyad' || bankFolder == 'rbcapital' || bankFolder == 'sabb' || bankFolder == 'ncb' || bankFolder == 'aljazeera'){
                                            $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '"  zone="' + markerInfo[markerInfoIndex][0]['zone_id'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] +  '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+  '"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                                '<br /><br />' +
                                                '<span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                                '<!--<input type="checkbox" disabled="true" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                                '<i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>'
                                                + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" style="margin-right: 3px;">' +
                                                '<img src="../public/img/men.png"  title="Men Branch"/></a>' +
                                                ' <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>' +
                                                //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Men" for-branch-report="true" style="cursor:pointer"></span>' +
                                                '</td>';
                                        }else{
                                            $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '"  zone="' + markerInfo[markerInfoIndex][0]['zone_id'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] +  '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+  '">' +
                                                '<span class="icon icon-blue icon-triangle-e" ></span>' +
                                                '<a href="javascript:void()">' +
                                                '<span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">'
                                                + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                                '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                                '<!--<input type="checkbox" disabled="true" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                                '<i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) +
                                                '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" style="margin-right: 3px;"><img src="../public/img/men.png"  title="Men Branch"/></a> ' +
                                                '<span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\')"></span>' +
                                                //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Men" for-branch-report="true" style="cursor:pointer"></span>' +
                                                '</td>';
                                        }

                                    }
                                }

                                break;


                            case 'Gents and Ladies':
                                //console.log('here 3');
                                //console.log("gents and ladies");
                                $rowCounter++;
                                flagAgent = true;

                                if(bankFolder == 'arbbranches_v2'){

                                    $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '"  zone="' + markerInfo[markerInfoIndex][0]['zone_id'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] +  '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+  '"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                    '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                        '<!--<input type="checkbox" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                        '<i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" style="margin-right: 3px;"><img src="../public/img/men.png"  title="Men Branch"/></a> '+
                                    '<span class="fa fa-file-pdf-o fa-lg" title="Download Profile Pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>&nbsp;&nbsp;' +
                                    '<span class="fa fa-file-excel-o fa-lg" title="Download Profile Excel"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'excel\')"></span>' +
                                    '<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Men" for-branch-report="true" style="cursor:pointer"></span>'+
                                    '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                    '<!--<input type="checkbox" class="branchBasketReportClass" name="branchBasketReport" branchType="Ladies" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                        ' <i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a> '+
                                    '<span class="fa fa-file-pdf-o fa-lg" title="Download Profile Pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>&nbsp;&nbsp;'+
                                    '<span class="fa fa-file-excel-o fa-lg" title="Download Profile Excel"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'excel\')"></span>'+
                                    '<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Ladies" for-branch-report="true" style="cursor:pointer"></span></td>';

                                }else if(bankFolder == 'riyad' || bankFolder == 'rbcapital' || bankFolder == 'sabb' || bankFolder == 'ncb' || bankFolder == 'aljazeera'){
                                    $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '"  zone="' + markerInfo[markerInfoIndex][0]['przone_idovince'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] + '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+'">' +
                                        '<span class="icon icon-blue icon-triangle-e" ></span>' +
                                        '<a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                        '<br /><br />' +
                                        '<span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                        '<!--<input type="checkbox" disabled="true" class="branchBasketReportClass" branchType="Ladies" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                        '<i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) +
                                        '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>' +
                                            //'<span id="branchReportEmail" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Ladies" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" for-branch-report="true" style="cursor:pointer"></span>' +
                                        '<br/><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                        '<!--<input type="checkbox" disabled="true" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> -->' +
                                        ' <i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) +
                                        '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" style="margin-right: 3px;"><img src="../public/img/men.png"  title="Men Branch"/></a> ' +
                                        '<span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\',\'pdf\')"></span>' +
                                            //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Men" title="Email" for-branch-report="true" style="cursor:pointer"></span>' +
                                        '</td>';
                                }else{
                                   $reportContent += '<td class="branchCode-' + markerInfo[markerInfoIndex][0]['branchCode'] + '"  zone="' + markerInfo[markerInfoIndex][0]['przone_idovince'] + '" area="' + markerInfo[markerInfoIndex][0]['area_id'] + '" branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                       '<br /><br />' +
                                       '<span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                       '<!--<input type="checkbox" disabled="true" class="branchBasketReportClass" branchType="Ladies" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> <i> -->' +
                                       '<a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) +
                                       '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a> ' +
                                       '<span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Ladies\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\')"></span>' +
                                       //'<span id="branchReportEmail" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Ladies" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" for-branch-report="true" style="cursor:pointer"></span>' +
                                       '<br/>' +
                                       '<span style="margin-left: 22px;">' +
                                       '<input type="checkbox" disabled="true" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '"> ' +
                                       '<i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank">' + markerInfo[markerInfoIndex][0]['branchCode'] + '</a></i>' + calculateChar(markerInfo[markerInfoIndex][0]['branchCode']) + '<a href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode'] + '&type=Men" target="_blank" style="margin-right: 3px;">' +
                                       '<img src="../public/img/men.png"  title="Men Branch"/></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\'' + markerInfo[markerInfoIndex][0]['branchCode'] + '\',\'Men\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\')"></span>' +
                                       //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" branch-code="' + markerInfo[markerInfoIndex][0]['branchCode'] + '" branch-type="Men" title="Email" for-branch-report="true" style="cursor:pointer"></span>' +
                                       '</td>';
                               }

                                if ($rowCounter > 4) {
                                    $rowCounter = 1;
                                    $reportContent += '</tr><tr>';
                                }
                                break;

                            default:

                                if (markerInfo[markerInfoIndex][0]['name'] == 'hrdf') {
                                    $rowCounter++;
                                    flagAgent = true;
                                    $branchCode = markerInfo[markerInfoIndex][0]['branchCode'];
                                    $reportContent += '<td class="branchCode-' + $branchCode + '">' +
                                            '<span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()">' +
                                            '<span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">' + markerInfo[markerInfoIndex][0]['branchName'].substring(0, 17) + '</span></a>' +
                                            '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                            '<!--<input type="checkbox" class="branchBasketReportClass" name="branchBasketReport" value="' + markerInfo[markerInfoIndex][0]['branchCode'] + '">-->' +
                                            '<i><a style="text-decoration: underline;" href="index.php?code=' + $branchCode + '&type= " target="_blank">' + $branchCode + '</a></i>' + calculateChar($branchCode) + '<a href="index.php?code=' + $branchCode + '&type=" target="_blank"></a>' +
                                            '<span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\'' + $branchCode + '\',\'\',\'' + markerInfo[markerInfoIndex][0]['name'] + '\',\'download\')"></span></td>';

                                    if ($rowCounter > 4) {
                                        $rowCounter = 1;
                                        $reportContent += '</tr><tr>';
                                    }
                                }
                                break;
                        }

                    }
                }
                $reportContent += "</tr></table>";
                if (flagAgent) {
                    $("#branch-list-box").html($reportContent);
                }
                firstTime = 1;
                flagAgent = false;
            }
        }
    };
    var counter = 0;
    $(document).on('click', '.branchBasketReportClass', function () {

        $this = $('.branchBasketReportClass');
        branchCode = [];
        $('#downloadBranchBasketReportButton').show();
        $('#mailBranchBasketReportButton').show();
        if (counter > 0) {
            $("#downloadConsolidatedBranchReportButton").show();
        }
        if (baseBank == "mobily") {
            inputId = $(this).val() + '_' + $(this).attr('branchType').replace("&", "-");
            inputId1 = $(this).val() + '_1' + $(this).attr('branchType');
        } else {

            inputId = $(this).val() + '_' + $(this).attr('branchType');
            inputId1 = $(this).val() + '_1' + $(this).attr('branchType');
        }

        if ($(this).is(":checked")) {
            counter++;
            $('#changeAgentTable').css('margin-left', '33%');
            formContent = '<input type="hidden" name="branches[' + $(this).val() + '][' + $(this).attr('branchType') + ']" value="' + $(this).attr('branchType') + '" id="' + inputId + '">';
            formContent1 = '<input type="hidden" name="branches[' + $(this).val() + '][' + $(this).attr('branchType') + ']" value="' + $(this).attr('branchType') + '" id="' + inputId1 + '">';
            $('#downloadBranchBasketReportForm').append(formContent);
            $("#downloadConsolidatedBranchProfileReportForm").append(formContent1);
        } else {
            counter--;
            $('#' + inputId).remove();
            $('#' + inputId1).remove();
        }
        if (counter == 0) {
            $('#changeAgentTable').css('margin-left', '63%');
        }
        if (counter == 1) {
            $("#downloadConsolidatedBranchReportButton").hide();
        }

        $checkBranchReportCheckbox = false;
        $.each($this, function (i, v) {
            if ($(this).is(":checked")) {
                $checkBranchReportCheckbox = true;
            }
        });
        if (!$checkBranchReportCheckbox) {
            $('#downloadBranchBasketReportButton').hide();
            $('#mailBranchBasketReportButton').hide();
            $("#downloadConsolidatedBranchReportButton").hide();
        }

    });

    this.clearMap = function () {
        this.clearCluster();
        this.clearMarker();
        this.markerInfo = [];
        if ($("#toggle_map_" + this.mapType).is(":checked")) {
            $("#toggle_map_" + this.mapType).trigger("click");
            $("#toggle_map_" + this.mapType).removeAttr('checked');
            initializeHeatmapSelector(this.mapType);
        }

    };

    this.createHeatmap = function (heatmapData) {
        this.heatmap = new google.maps.visualization.HeatmapLayer({
            data: heatmapData,
            radius: 85
        });
    };

    this.addListeners = function () {
        var me = this;
        google.maps.event.addListener(this.map, 'tilesloaded', function (event) {
            if (first == "") {
                changeIsrailLayer(this.getZoom());
                first = 1;
            }
            if (!previousMapType) {
                previousMapType = this.getMapTypeId();
            } else {
                if (this.getMapTypeId() != previousMapType) {
                    changeIsrailLayer(this.getZoom());
                    previousMapType = this.getMapTypeId();
                }
            }
        });
        google.maps.event.addListener(this.map, 'zoom_changed', function (event) {
            $zoomChanged = true;
            changeIsrailLayer(this.getZoom());
            if (this.getZoom() < 5) {
                if (first == 1)
                    this.setZoom(5);
                first = 2;
            }
        });
        google.maps.event.addListener(this.map, 'idle', function () {
            if ($zoomChanged) {
                if (me.heatmap)
                    me.heatmap.setOptions({radius: getNewRadius(this.getZoom())});
                $zoomChanged = false;
            }
        });
    };
    this.resizeMap = function () {
        google.maps.event.trigger(this.map, 'resize');
        this.map.setCenter(new google.maps.LatLng(22.268763552489748, 47.87841796875));
    }
}

 function getBranchTypeByCode(){

                            console.log("branch_type_123");

                $requestData = {
                    'requestFor'    :  'get-branch-type',
                    //'area-name'     :  $("#selected_area :selected").text()+" "+"Region"
                };
              // AJAX Request fro getting branch_type.------------
                $.ajax({
                  type: 'POST',
                  url: baseUrl+''+baseBank+'/request.php',
                  data: $requestData,
                  async:false,
                  success:function($result){
                    // $data = JSON.parse($result);
                    console.log($result);
                   /* $("#branch-list-box-table tr").find("td").each(function(){
                         for (var i in $data){
                            if('branchCode-'+$data[i]['branch_code'] == $(this).attr('class')){
                                $insertData = true;
                                if($counter%6 == 0){
                                     $content += '</tr><tr border= "1"><td>'+$(this).html()+'</td>';
                                }else{
                                    $content += '<td>'+$(this).html()+'</td>';
                                }
                                $counter++;
                                $foundFlag = true;
                            }
                         }*/
                    }
                    });

                }





function callMe(results) {

    if (firstMapRequestFlag) {

        $("#mapjscontent").html('<script type="text/javascript">' + results.mapfunction + '</script>');

        createMarkers(results);
    } else {

        myresults = JSON.parse(results);
        $(myresults['mapdata']).each(function (key, value) {


            myresults['mapdata'][key]['branchScoreGents'] = getIndivdualBranchScore(value['branchCode'], 'Men');
            myresults['mapdata'][key]['branchScoreLadies'] = getIndivdualBranchScore(value['branchCode'], 'Ladies');
            myresults['mapdata'][key]['branchScore'] = getIndivdualBranchScore(value['branchCode'], 'BRANCH');
            //myresults['mapdata'][key]['lastVisitDate'] =     getIndivdualLastVisit(value['branchCode']);
            //myresults['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate(value['branchCode']);

            if(bankFolder == "arbasset"){
                myresults['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDateArb((value['branchCode']).trim(), (value['branchType']).trim());
            }else{
                if(bankFolder == "ncbrebranding"){
                   myresults['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate((value['branchCode']));
                }else {
                   myresults['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate((value['branchCode']).trim());
                }

            }
            if (bankFolder == 'ncbrebranding') {

                if (typeof myresults['mapdata'][key]['lastVisitDate'] !== "undefined") {
                    myresults['mapdata'][key]['is_survey'] = true;
                } else {
                    myresults['mapdata'][key]['is_survey'] = false;
                }
            }
        });
        if(myresults['mapdata']){
            /* SORTING ON LAST VISIT BASIS */
            if(bankFolder == 'arbbranches_v2')

                myresults = sortBranchListByLastVisitDateAlrajhi(myresults);
            else
                myresults = sortBranchListByLastVisitDate(myresults);
        }
        $("#mapjscontent").html('<script type="text/javascript">' + myresults.mapfunction + '</script>');
        // console.log(myresults);
        if (bankFolder == 'ncbrebranding') {
            setTimeout(function () {
                createMarkers(JSON.stringify(myresults));
                $("#loading_screen_branch").hide();
            }, 3000);
        } else {
            setTimeout(function () {
                createMarkers(JSON.stringify(myresults));
                $("#loading_screen_branch").hide();
            }, 3000);
        }

    }
}

function getBankJsonFileName($bankName)
{
    switch ($bankName) {
        case "riyad-arabic"    :
            return "riyad";
        case "ncb-arabic" :
            return "ncb";
        case "alrajhi-arabic" :
            return "alrajhi";
        case "sabb-arabic" :
            return "sabb";
        case "shb-arabic" :
            return "shb";
        default              :
            return "null";
    }
}

var getBranchScores;

function getAllBranchScore(bankFolder)
{
    $requestData = {
        "requestFor": "all-branch-score",
        "bank-filters": {
            "bankFolder": bankFolder
        }
    };
    $requestObject.initialize($requestData, "getAllBranchScoreCallBack", null);

}

function getAllBranchScoreTest(bankFolder)
{
    $.ajax({
        type: 'POST',
        url: 'request.php',
        data: {
            'requestFor': 'all-branch-score',
            'bank-filters': {
                "bankFolder": bankFolder
            }
        },
        dataType: "json",
        success: function (data) {
            return data;
        }
    });

}

function getAllBranchScoreCallBack(results)
{
    $allBranchScore = JSON.parse(results);
    if ($allBranchScore) {

        return $allBranchScore;
    }
}

function getIndivdualBranchScore($branchCode, $branchType)
{
    if (typeof $_ALLBRANCHSCORE[$branchCode + '_' + $branchType] == 'undefined' || $_ALLBRANCHSCORE[$branchCode + '_' + $branchType] == '') {

        $_ALLBRANCHSCORE[$branchCode + '_' + $branchType] = '&nbsp;&nbsp;--&nbsp;&nbsp;';


    }

    return $_ALLBRANCHSCORE[$branchCode + '_' + $branchType];
}


function getIndivdualBranchVisitDate($branchCode)
{
    return $_ALLBRANCHSCORE[$branchCode + '_VISITDATE'];
}

function getIndivdualBranchVisitDateArb($branchCode, $branchType)
{
    if($branchType == "Gents and Ladies"){
        if($_ALLBRANCHSCORE[$branchCode + '_Gents_VISITDATE']){
            return $_ALLBRANCHSCORE[$branchCode + '_Gents_VISITDATE'];
        }
        if($_ALLBRANCHSCORE[$branchCode + '_Ladies_VISITDATE']){
            return $_ALLBRANCHSCORE[$branchCode + '_Ladies_VISITDATE'];
        }
    }
    return $_ALLBRANCHSCORE[$branchCode + '_'+$branchType+'_VISITDATE'];
}
function getIndivdualBranchVisitDateArbL($branchCode)
{
    return $_ALLBRANCHSCORE[$branchCode +'_VISITDATEL'];
}

function getIndivdualStockTickets($branchCode) {
    return $_ALLBRANCHSCORE[$branchCode + '_STOCKTICKETS'];
}

function getIndivdualOutletScore($branchCode) {
    return $_ALLBRANCHSCORE[$branchCode + '_OUTLET'];
}

function getIndivdualBranchType($branchCode){
    return $_ALLBRANCHSCORE[$branchCode + '_BRANCH_TYPE'];
}



function createMapRequest($type, $requestType) {
    //consol.log("search mapgjgjgfj");
//console.log($_ALLBRANCHSCORE);
    if (firstMapRequestFlag) {

        var url = baseUrl + "public/json/" + baseBank + ".json";
        $.getJSON(url, function (res) {

            if (res && res['mapdata']) {
                $(res['mapdata']).each(function (key, value) {

                    res['mapdata'][key]['branchScoreGents'] = getIndivdualBranchScore(value['branchCode'], 'Men');
                    res['mapdata'][key]['branchScoreLadies'] = getIndivdualBranchScore(value['branchCode'], 'Ladies');
                    res['mapdata'][key]['branchScore'] = getIndivdualBranchScore(value['branchCode'], 'BRANCH');

                    if(bankFolder == "ncbrebranding"){
                        res['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate(value['branchCode']);
                    }else {
                        res['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate(value['branchCode'].trim());
                    }

                    if(bankFolder == "arbatm" || bankFolder =="arbbranches_v2"){
                         res['mapdata'][key]['branch_type'] = getIndivdualBranchType((value['branchCode']).trim());
                    }

                    if(bankFolder == "arbasset"){

                        $branchTypeArb = value['branchType'];
                        if((value['branchType']) != null) {
                            $branchTypeArb = (value['branchType']).trim();
                        }

                        res['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDateArb((value['branchCode']).trim(), $branchTypeArb);
                    }else if(bankFolder == "arbbranches_v2"){
                        res['mapdata'][key]['lastVisitDateL'] = getIndivdualBranchVisitDateArbL((value['branchCode']).trim());
                    }else{

                        if(bankFolder == "ncbrebranding"){
                            res['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate(value['branchCode']);
                        }else {
                            res['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate(value['branchCode'].trim());
                        }
                    }
                    if (bankFolder == 'ncbrebranding') {
                        if (typeof res['mapdata'][key]['lastVisitDate'] !== "undefined") {
                            res['mapdata'][key]['is_survey'] = true;
                        } else {
                            res['mapdata'][key]['is_survey'] = false;
                        }
                    }

                    if (bankFolder == 'ughi') {
                        res['mapdata'][key]['stockTickets'] = getIndivdualStockTickets(value['branchCode']);
                    }

                    if (bankFolder == 'hrdf') {
                        res['mapdata'][key]['outletScore'] = getIndivdualOutletScore(value['branchCode']);
                    }

                });

            if(bankFolder == 'arbbranches_v2')
                /* SORTING ON LAST VISIT BASIS */
                res = sortBranchListByLastVisitDateAlrajhi(res);

            else
                /* SORTING ON LAST VISIT BASIS */
                res = sortBranchListByLastVisitDate(res);
            }
            /* -----ENDS HERE ----*/
            $("#loading_screen_branch").hide();

            callMe(res);

        });


    } else {

        if (this.check == 0) {
            this.mobilyResetCheck = 0;
        }
        if ($type == 'other' && province_branch.value == "" && check != 0) {
            alert("Please select atleast one field");
            $("#loading_screen_" + $requestType).hide();
            return;
        }
        this.check = 1;
        if ($type == 'code') {
            $digitRegx = '\[0-9\,]';
            if ($("#search_code_" + $requestType).val().match($digitRegx)) {

                $("#overview_" + $requestType).html($("#branch_loader").html());
                $requestData = {
                    "requestFor": "map-data",
                    "group_name": group_name,
                    "code-filters": {
                        "branchCode": $("#search_code_" + $requestType).val(),
                        "requestType": $requestType
                    }
                }
            } else {
                if (bankFolder == 'ughi') {
                    alert("Please provide a valid Outlet Code");
                } else {
                    alert("Please provide a valid Branch Code");
                }
                $("#loading_screen_" + $requestType).hide();
                return;
            }
        } else if ($type == 'name') {
            $wordRegx = '\[abc ABC\,]';
            if ($("#search_name_" + $requestType).val().match($wordRegx)) {
                $("#overview_" + $requestType).html($("#branch_loader").html());
                $requestData = {
                    "requestFor": "map-data",
                    "group_name": group_name,
                    "name-filters": {
                        "branchName": $("#search_name_" + $requestType).val(),
                        "requestType": $requestType
                    }
                }
            }/*else if(bankFolder == 'hrdf') {
             //alert("name2");
             $("#overview_"+$requestType).html($("#branch_loader").html());
             $requestData = {
             "requestFor"    :   "map-data",
             "group_name"        :   group_name,
             "name-filters"  :   {
             "branchName"    :  $("#search_name_"+$requestType).val(),
             "requestType"   :  $requestType
             }
             }
             alert("Please provide a valid Branch Name");
             }*/ else {
                if (bankFolder == 'ughi') {
                    alert("Please provide a valid Outlet Name");
                } else {
                    alert("Please provide a valid Branch Name");
                }
                $("#loading_screen_" + $requestType).hide();
                return;
            }
        } else if ($type == 'agent') {

            $("#overview_" + $requestType).html($("#branch_loader").html());
            $requestData = {
                "requestFor": "map-data",
                "group_name": group_name,
                "map-filters": {
                    "province": $("#province_" + $requestType).val(),
                    "city": $("#city_" + $requestType).val(),
                    "agent_name": $("#agent_" + $requestType).val(),
                    "area_name": $("#zone_" + $requestType).val(),
                    "requestType": $requestType
                }
            }

        } else if ($type == 'mobily') {
            var outlet_value;
            if (this.mobilyResetCheck) {
                if ($("#outletType").val() == "" && $("#province_" + $requestType).val() == "") {
                    alert("Please select atleast one field");
                    $("#loading_screen_" + $requestType).hide();
                    return;
                }
                $outlet_value = $("#outletType").val(); //group_name;
            }
            else {
                $outlet_value = group_name;
            }
            this.mobilyResetCheck = 1;
            if ($outlet_value == 'FS')
                $outlet_value = 'FSO';
            $("#overview_" + $requestType).html($("#branch_loader").html());
            $requestData = {
                "requestFor": "map-data",
                "group_name": group_name.toLowerCase(),
                "map-filters": {
                    "province": $("#province_" + $requestType).val(),
                    "city": $("#city_" + $requestType).val(),
                    "outlet_type": $outlet_value, // $("#outletType").val(),
                    "requestType": $requestType,
                    "branchCode": $("#search_code_" + $requestType).val()
                }
            }

        } else if ($type == 'outlet') {

            $("#overview_" + $requestType).html($("#branch_loader").html());
            $requestData = {
                "requestFor": "map-data",
                "group_name": group_name,
                "map-filters": {
                    "region_name": $("#region_name").val(),
                    "outlet_type": $("#" + $type + "_type").val(),
                    "branchName": $("#" + $type + "_name").val(),
                    "merchandiser_name": $("#" + $type + "_merchandiser").val(),
                    "checus_cust": $("#checus_cust").val(),
                    "survey_status": $("#survey_status").val(),
                    "requestType": $requestType
                }
            };
            if (bankFolder != 'ughi') {
                $requestData = $requestData.push({
                    "survey_id": survey_id
                });
            }

        } else {
            $branchServiceType = "";
            $("#overview_" + $requestType).html($("#branch_loader").html());
            if(bankFolder == 'arbbranches_v2'){
                $baanchType='';
            }else if(bankFolder == "arbasset"){
                $baanchType = (titleBox.getValue()) ? titleBox.getValue() : '';
                $branchServiceType = ($("#branchServiceTypeSelector").val()) ? $("#branchServiceTypeSelector").val() : '';
            }else{
                $baanchType = (bankFolder != 'hrdf') ? titleBox.getValue() : '';
            }

            $requestData = {
                "requestFor": "map-data",
                "survey_id": survey_id,
                "group_name": group_name,
                "branchTypeServc" : $branchServiceType,
                "map-filters": {
                    "province": $("#province_" + $requestType).val(),
                    "city": $("#city_" + $requestType).val(),
                    "branchType": $baanchType,
                    "branchStatus": $("#branchStatus").val(),
                    "requestType": $requestType
                }
            }
        }

        $requestObject.initialize($requestData, "callMe", null);
    }
}

function fillCityOptions($data) {
    mapObject = getActiveMapTab();
    $("#city_" + mapObject.mapType).html($data);
}

function getCityList($dataFor) {

    $requestData = {
        "requestFor": "city-data",
        "map-filters": {
            "region": $("#province_" + $dataFor).val(),
            "requestType": $dataFor
        }
    };
    $requestObject.initialize($requestData, "fillCityOptions", null);

}
function getCityListForReport($id, $dataFor) {

    $requestData = {
        "requestFor": "city-data",
        "map-filters": {
            "region": $($id).val(),
            "requestType": $dataFor
        }
    };
    $requestObject.initialize($requestData, "fillReportCityOption", null);
}

function fillReportCityOption($data) {
    mapObject = getActiveMapTab();
    $("#branch_city").html($data);
}

function resetMapFilter($type) {
    this.check = 0;
    $("#zone_" + $type).attr('value', "");
    $("#province_" + $type).attr('value', "");
    $("#city_" + $type).html("<option value=''>-- Select City --</option>");
    $("#branchStatus").attr('value', '');
    $("#search_code_" + $type).attr('value', '');
    $("#search_name_" + $type).attr('value', '');
    $("#agent_" + $type).attr('value', '');
    $("#outletType :selected").attr('selectedIndex', 0);
    $("#outlet_type").attr('value', '');
    $("#region_name").attr('value', '');
    
    $("#outlet_name").html("<option value=''> All outlet Codes </option>");
    $("#outlet_merchandiser").html("<option value=''> All merchandiser </option>");
    $('#outlet_type').val('').change();
    $("#checus_cust").attr('value', '');
    $("#checus_cust").html("<option value=''> All Custom Codes </option>");
    $("#survey_status").attr('value', '');
    if(bankFolder == "arbasset"){
        $("#branchServiceTypeSelector").attr('value', '');
    }
//    $("#outlet_name").html("<option value=''> All Outlets </option>");
//    $("#outlet_merchandiser").html("<option value=''> All Merchandisers </option>");
    if ($("#titleBox").length)
        titleBox.uncheckAll();
    if ($type == 'atm') {
        $("#search_tab_atm").find('li.active').each(function () {
            if ($(this).find('a').attr('href') == '#byCodeDivAtm') {
                $("#atmCodeSearchButton").trigger('click');
            } else {
                $("#atmCodeRegionButton").trigger('click');
            }
        })

    } else {

        $("#search_tab_branch").find('li.active').each(function () {
            if ($(this).find('a').attr('href') == '#byCodeDiv') {
                $("#branchCodeRegionButton").trigger('click');

            } else {
                $("#branchCodeRegionButton").trigger('click');
            }
        })
    }


}

function filterData($type, $filterFor) {

    $("#loading_screen_" + $filterFor).show();
    $("#CloseBubble").trigger("click");
    if (firstMapRequestFlag) {

        var url = baseUrl + "/public/json/" + baseBank + ".json";
        $.getJSON(url, function (res) {

            if (res && res['mapdata']) {
                $(res['mapdata']).each(function (key, value) {

                    res['mapdata'][key]['branchScoreGents'] = getIndivdualBranchScore(value['branchCode'], 'Men');
                    res['mapdata'][key]['branchScoreLadies'] = getIndivdualBranchScore(value['branchCode'], 'Ladies');
                    res['mapdata'][key]['branchScore'] = getIndivdualBranchScore(value['branchCode'], 'BRANCH');
                    if(bankFolder == "arbasset"){

                        $branchTypeArb = value['branchType'];
                        if((value['branchType']) != null) {
                            $branchTypeArb = (value['branchType']).trim();
                        }

                        res['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDateArb((value['branchCode']).trim(), $branchTypeArb);
                    }else{
                      res['mapdata'][key]['lastVisitDate'] = getIndivdualBranchVisitDate((value['branchCode']).trim());
                    }

                    if (bankFolder == 'ughi') {
                        res['mapdata'][key]['stockTickets'] = getIndivdualStockTickets(value['branchCode']);
                    }

                });

            if(bankFolder == 'arbbranches_v2')
                /* SORTING ON LAST VISIT BASIS */
                res = sortBranchListByLastVisitDateAlrajhi(res);

            else
                /* SORTING ON LAST VISIT BASIS */
                res = sortBranchListByLastVisitDate(res);

            }
            /* -----ENDS HERE ----*/
            callMe(res)

        });

    } else {
        if ($type == 'code') {

            createMapRequest('code', $filterFor);

        } else if ($type == 'agent') {

            createMapRequest('agent', $filterFor);

        } else if ($type == 'name') {

            createMapRequest('name', $filterFor);

        } else if ($type == 'outlet') {

            createMapRequest('outlet', $filterFor);

        } else if ($type == 'mobily') {

            createMapRequest('mobily', $filterFor);

        }
        else {
            createMapRequest('other', $filterFor);
        }
    }
}


function initializeHeatMap($result, $resultType) {
    //console.log($result);
    if ($resultType == 'atm')
        mapObject = mapObjectAtm;
    else
        mapObject = mapObjectBranch;

    var heatmapData = [];
    for (var i in $result) {
        if (Number($result[i]['weight'])) {
            testData.location = new google.maps.LatLng($result[i]['latitude'], $result[i]['longitude']);
            testData.weight = Number($result[i]['weight']);
            heatmapData.push(testData);
            testData = {
                location: null,
                weight: null
            };
        }
    }
    mapObject.createHeatmap(heatmapData);
}

function switchMap(mapType, requestType) {
    mapObject = getActiveMapTab();

    if (mapType == 'heat') {
        if (mapObject.heatmap)
            mapObject.heatmap.setMap(mapObject.map);
        else {
            getHeatMapDataset(requestType);

        }

        if (mapObject.cluster)
            mapObject.cluster.setMarkerMap(null);
        return "normal"
    } else {
        if (mapObject.heatmap)
            mapObject.heatmap.setMap(null);
        else {
            getHeatMapDataset(requestType);
        }

        if (mapObject.cluster)
            mapObject.cluster.setMarkerMap(true);
        return "heat"
    }
}


function initializeHeatmapSelector($checkBoxType) {

    $(document).ready(function () {
        if (isArabic) {
            $offLabelIhone = getArabicText('Off');
            $onLabelIhone = getArabicText('On');
        } else {
            $offLabelIhone = 'Off';
            $onLabelIhone = 'On';
        }



        $('#toggle_map_' + $checkBoxType + ':checkbox').iphoneStyle({
            uncheckedLabel: $offLabelIhone,
            checkedLabel: $onLabelIhone,
            onChange: function () {

                if ($checkBoxType == 'atm') {
                    mapType_atm = switchMap(mapType_atm, 'atm');
                } else {
                    mapType_branch = switchMap(mapType_branch, 'branch');
                }
            }
        });

    })
}

function initializeTinyscrollbar($type) {
    $(document).ready(function () {
        $('#scrollbar_' + $type).tinyscrollbar({
            sizethumb: 40
        });
    });
}

function getActiveMapTab() {
    $activeTab = "";
    $("#myTab").find("li.active").each(function () {
        $activeTab = $(this).find("a").attr('href');
    });

    switch ($activeTab) {
        case "#Dashboard"    :
            return mapObjectBranch;
        case "#AtmContainer" :
            return mapObjectAtm;
        default              :
            return mapObjectBranch;
    }
}

function resizeActiveMap($resultType) {
    if ($resultType == 'atm') {
        if (typeof mapObjectAtm == 'object')
            mapObjectAtm.resizeMap();
    } else {
        if (typeof mapObjectBranch == 'object')
            mapObjectBranch.resizeMap();
    }
}


function getHeatMapDataset($requestType) {
    $("#loading_screen_" + $requestType).show();
    $requestData = {
        "requestFor": "heat-map-data",
        "group_name": group_name,
        "survey_id": survey_id,
        "map-filters": {
            "requestType": $requestType
        }
    };
    $requestObject.initialize($requestData, "createHeatMap", null);
}

var mapObjectBranch = new MapCreator({
    mapContainer: "map_section_branch",
    type: "branch"
});
var mapObjectAtm = new MapCreator({
    mapContainer: "map_section_atm",
    type: "atm"
});
var mapObject = null;

function createMap() {
    mapObjectBranch.createMap();
    if ($("#loading_screen_atm").length) {
        mapObjectAtm.createMap();
    }

    createMapRequest('other', 'branch');

}

$("#overview_branch").html($("#branch_loader").html());

if ($("#overview_atm").length) {
    $("#overview_atm").html($("#atm_loader").html());
}

createMap();                                //Execution starts Here
initializeHeatmapSelector('branch');



