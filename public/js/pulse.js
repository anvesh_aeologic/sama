/*function getFilterData($element){
    var $startDate;
    var $endDate;
    if($($element).attr("id") == "searchScore" ){
        $("#scoreLoader").css('display', 'inline');
        $("#searchScoreDiv").hide();
        $region  = $("#selectMarkRegion option:selected").val();
        $brand   = $("#selectMarkBrand option:selected").val();
        $period  = $("#selectMarkPeriod option:selected").val();
        $dayTime = $("#selectMarkDayTime option:selected").val();
            if($period == "custom"){
                $startDate = $("#markPeriodStartDate").val();
                $endDate   = $("#markPeriodEndDate").val();
            }
            
            $requestData = {
            "requestFor"       :   "brand-score",
            "filter-data"      : {
                        
                        "region"      : $region,
                        "brand"       : $brand,
                        "period"      : $period,
                        "day-time"    : $dayTime,
                        "start-date"  : $startDate,
                        "end-date"    : $endDate
            }
      }; 
    }else if($($element).attr("id") == "cloudData"){
        $("#cloudLoader").css('display', 'inline');
        $("#cloudDiv").hide();
        $region  = $("#selectMapRegion option:selected").val();
        $brand   = $("#selectMapBrand option:selected").val();
        $period  = $("#selectMapPeriod option:selected").val();
        $dayTime = $("#selectMapDayTime option:selected").val();
            if($period == "custom"){
                $startDate = $("#mapPeriodStartDate").val();
                $endDate   = $("#mapPeriodEndDate").val();
            }
            $requestData = {
            "requestFor"       :   "cloud-data",
            "filter-data"      : {
                        
                        "region"      : $region,
                        "brand"       : $brand,
                        "period"      : $period,
                        "day-time"    : $dayTime,
                        "start-date"  : $startDate,
                        "end-date"    : $endDate
            }
      }; 
    }else if($($element).attr("id") == "regionGraph"){
        $("#regionGraphLoader").css('display', 'inline');
        $("#regionGraphDiv").hide();
        $region  = $("#selectRegion option:selected").val();
        $period  = $("#selectRegionPeriod option:selected").val();
            if($period == "custom"){
                $startDate = $("#regionPeriodStartDate").val();
                $endDate   = $("#regionPeriodEndDate").val();
            }
            $requestData = {
            "requestFor"       :   "region-graph-data",
            "filter-data"      : {
                        
                        "region"      : $region,
                        "period"      : $period,
                        "start-date"  : $startDate,
                        "end-date"    : $endDate
            }
      }; 
    }
    $requestObject.initialize($requestData,"fillData",null);    
}*/

// Global Varibales---------
var series;
var lastId;
// --------------------Global data filter ---------

$elementList = ['input','select','textarea'];
function getFilterRequestData($parentElement){
    if($parentElement == "#region-graph-group"){
        $("#regionGraphDiv").html();
         $("#brand_beat_loader").css('display','inline');
        $("#brand_beat_loader").html('<img src="https://4c360.com/4c-dashboard/public/img/ajax-loaders/bsfmap-loader1.gif" title="Loading..." style="height:50px;" >');
    }
    if($parentElement == "#cloud-data-group"){
        $("#cloudLoader").css('display', 'inline');
//        $("#cloudDiv").hide();
    }
    if($parentElement == "#brand-score-group"){
        //$("#searchScoreDiv").html('<img src="https://4c360.com/4c-dashboard/public/img/ajax-loaders/bsfmap-loader1.gif" title="Loading..." >');
        $("#scoreLoader").css('display', 'inline');
        $("#ticker01").hide();
    }
    if($parentElement == "#region-pie-chart-group"){
        $("#pieChartLoader").css('display', 'inline');
        $("#regionPieChartDiv").hide();
    }
    if($parentElement == "#region-heat-map-group"){
        $("#heatMapLoader").css('display', 'inline');
        $("#heatMapDiv").hide();
    }
    if($parentElement == "#service-time-group"){
//        $("#service_time_pie_chart_container").hide();
        $("#serviceTimeGraphLoader").css('display', 'inline');
    }
    if($parentElement == "#nps_group"){
        $("#nps_holder_loader").css('display','inline');
//        $("#nps_holder_loader").html('<img src="https://4c360.com/4c-dashboard/public/img/ajax-loaders/bsfmap-loader1.gif" title="Loading..." style="height:50px;width:50px">');
    }
    $requestData = {
            "requestFor"       :   $($parentElement).data('request-type'),
            "filter-data"      : {
            }
    };
   
    for(var i in $elementList){
        $($parentElement).find($elementList[i]).each(function(){
            $requestData["filter-data" ][$(this).attr('name')] = $(this).val();
        });
    }
    //console.log($requestData);
    $requestObject.initialize($requestData,"fillData",null); 
}
function fillData($result){

    $data = JSON.parse($result);

    $("#scoreLoader").css('display', 'none');
    $("#nps_holder_loader").css('display','none');
    $("#brand_beat_loader").css('display','none');
    $("#cloudLoader").css('display', 'none');
    $("#regionGraphLoader").css('display', 'none');
    $("#pieChartLoader").css('display', 'none');
    //$("#heatMapLoader").css('display', 'none');
    $("#serviceTimeGraphLoader").css('display', 'none');    
    $("#"+$data['responseDiv']).show();
    $("#"+$data['responseDiv']).html($data['html']);
//    $("#"+$data['rdiv']).remove();
    $("#"+$data['rdiv']).show();
    $("#"+$data['rdiv']).html($data['slider']);
    if($data['regionGraphData']){
        createRegionGraph($data);
    }
    if($data['serviceQualityData']){
        serviceQualityPulseChart($data);
    }
    if($data['responseDiv'] == "cloudDiv")
        createCloud();
    if($data['responseDiv'] == "regionPieChartDiv")
        createPieChart($data);
    if($data['responseDiv'] == "heatMapDiv")
        createHeatMap($data['regionHeatMapData']);
    if($data['responseDiv'] == "service_time_pie_chart_container")
        serviceTimePieChart($data);
    
}

// --------------Reset Filter-----------------
/*function resetFilters($element){
   if($($element).attr("id") == "resetMarkFilter"){
      $("#scoreLoader").css('display', 'inline');
      $("#searchScoreDiv").hide();
        $requestData = {
            "requestFor"  : "brand-score",
            
        }
   }else if($($element).attr("id") == "resetMapFilter"){
        $("#cloudLoader").css('display', 'inline');
        $("#cloudDiv").hide();
        $requestData = {
            "requestFor"  : "cloud-data",
            
        }
   }
   $requestObject.initialize($requestData,"fillData",null);
}*/
//--------------------------------------------------

function createCloud(){
    $("#wordcloud2").awesomeCloud({
    					"size" : {
    						"grid" : 5,
    						"factor" : 1,
    					},
    					"options" : {
    						"color" : "random-dark",
    						"rotationRatio" : 0.35
    					},
    					"font" : "'Times New Roman', Times, serif",
    					"shape" : "diamond"
        });
}

function pulseFilters(){
    
    this.data = {
        "region"     : "By Region",
        "brand"      : "By Brand",
        "period"     : "By Period",
        "day-time"   : "Day Time",
        "start-date" : "",
        "end-date"   : ""
     };
    this.setFilter = function($index,$value){
       switch($index){
            case "region"     : this.data.region  = $value;break;
            case "period"     : this.data.period  = $value;break;
            case "start-date" : this.data.startDate  = $value;break;
            case "end-date"   : this.data.endDate    = $value;break;
       } 
    }
    
    this.getFilter = function(){
        return this.data;
    }
}

function pointRequest(){
   //console.log($("#regionFilters").find("select"));
    var filterDataObj = new pulseFilters();
    
    $filter = $("#regionFilters").find("select");
    $.each($filter,function(i,j){
        filterDataObj.setFilter(j['name'],j['value']);
    })
    $filter = $("#regionFilters").find("input");
    $.each($filter,function(i,j){
        filterDataObj.setFilter(j['name'],j['value']);
    })
    $filterData = filterDataObj.getFilter();
   // console.log($filterData);
   getFilterRequestData
    $requestData = {
            "requestFor"  : "response-data",
            "lastId"      :  lastId,
            "filter-data" :  $filterData
        };
     
   $.post(baseUrl+''+baseBank+'/request.php',$requestData,
    function(data){
        
        if( data != "null"){
            var moveGraph = true;
            data = JSON.parse(data);
            if(typeof data['lastId'] != 'undefined'){
                if( !graphData.length ){
                    $data['regionGraphData'] = [{
                        "email_id" :data['x'],
                        "service_type" : Number(data['y'])
                    }];
                    createRegionGraph($data);
                    return;
                }
                lastId = data['lastId'];
                if(graphData.length < 5){
                   moveGraph =  false;
                   graphData.push([data['x'],Number(data['y'])]);
                }
                
                series.addPoint([data['x'],Number(data['y'])], true, moveGraph);
            }else{
                lastId = 0;
            }
        }else{
            lastId = 0;
        }
    }
    
  )
}

var graphData = [];
var previousInterval = null;
var myArray = ["Terrible","So So","I'm Happy"];
var splineImage = ['https://4c360.com/4c-dashboard/public/img/question1-smiley-terrible.png , https://4c360.com/4c-dashboard/public/img/question1-smiley-soso.png , https://4c360.com/4c-dashboard/public/img/question1-smiley-happy.png' ]

$(document).ready(function() {
    Highcharts.setOptions({
        global: {
        useUTC: false
        }
    });
});
function createRegionGraph($data){
    lastId = 0;
    var emptyGraphData = false;
    graphData = [];
        if(previousInterval){
            clearInterval(previousInterval);
        }

    if($data['regionGraphData'].length == 0){
        $title = " No Survey Found";
    }else{
       $title = "Region Wise Customer Review"; 
    }

    $.each($data['regionGraphData'],function($index,$value){
      graphData.push([$value['email_id'],Number($value['service_type'])]);
      lastId = Number($value['lastId']);
    });

    $plotOption = {
            series: {
            marker: {
                enabled: true
                }
            }
    };

    if(graphData.length == 0){
        $plotOption = {
                series: {
                marker: {
                    enabled: false
                    },
                lineWidth : 0
                }
        };
        emptyGraphData = true; 
        graphData = [
           [ "" , 1],
           [ "" , 2],
           [ "" , 3]
        ];
    }
    var chart;
    $('#container').highcharts({
        chart: {
            type: 'spline',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function() {
                    series = this.series[0];
                    previousInterval = setInterval(function(){
                        pointRequest();
                    },1000);
                }
            }
        },
        title: {
            text: $title
        },
        xAxis: {
            type : 'category'
        },
        yAxis: {
            lineWidth: 0,
            gridLineWidth:0,
            title: {
                text: 'Customer Response'
            },
            labels:
            {
                enabled: false
            },
            plotLines : [{
                value : 1.5,
                color : 'green',
                dashStyle : 'shortdash',
                width : 2,
                label : {
                    enabled: false
                }
            }],
            plotBands: [{ // Light air
                from: 0.5,
                to: 1.5,
                color: 'rgba(68, 170, 213, 0.1)',
                label: {
                    text: "<table><tr><td><img src='https://4c360.com/4c-dashboard/public/img/question1-smiley-terrible.png' style='height:40px;width:40px;'/></td><td v-align='middle'><b>Terrible</b></td></tr></table>",
                    useHTML : true,
                    style: {
                        color: '#606060'
                        }
                }
            }, { // Light breeze
            from: 1.5,
            to: 2.5,
            color: 'rgba(0, 0, 0, 0)',
            label: {
                text: "<table><tr><td><img src='https://4c360.com/4c-dashboard/public/img/question1-smiley-soso.png' style='height:40px;width:40px;'/></td><td v-align='middle'><b>So So</b></td></tr></table>",
                useHTML : true,
                style: {
                    color: '#606060'
                    }
            }
            }, { // Gentle breeze
            from: 2.5,
            to: 3.5,
            color: 'rgba(68, 170, 213, 0.1)',
            label: {
                text: "<table><tr><td><img src='https://4c360.com/4c-dashboard/public/img/question1-smiley-happy.png' style='height:40px;width:40px;'/></td><td v-align='middle'><b>I'm Happy</b></td></tr></table>",
                useHTML : true,
                style: {
                    color: '#606060'
                }
            }
            }]
        },
        tooltip: {
        formatter: function() {
            return '<b>'+ this.series.name +'</b><br/>'+
            this.x +'<br/>'+myArray[this.y-1];

            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Customer Review',
            data:  graphData 
        }],
        plotOptions: $plotOption      

      });
      if(emptyGraphData){
        graphData = [];
      }
}

function createPieChart($data){
    var $title = 'Customer Reviews';
    var pieChartData = [];
    var counter = 0;
    //console.log($data);
    $.each($data['regionPieChartData'], function($index, $value){
        if(Number($value) == 0.0){
            counter++;
        }
        if($index == "I m Happy"){
          pieChartData.push({
            name : $index,
            y    : Number($value),
            color: '#006600'
          })  
        }
        if($index == "So So"){
          pieChartData.push({
            name : $index,
            y    : Number($value),
            color: '#0033CC'
          })  
        }
        if($index == "Terible"){
          pieChartData.push({
            name : $index,
            y    : Number($value),
            color: '#FF0000'
          })  
        }
        // -------------for Tawuniya cloud-----------------
        if($index == "Did not receive a response"){
          pieChartData.push({
            name : $index,
            y    : Number($value),
            color: '#FF0000'
          })  
        }
        if($index == "Longer than expected"){
          pieChartData.push({
            name : $index,
            y    : Number($value),
            color: '#FF0000'
          })  
        }
        if($index == "About what I expected"){
          pieChartData.push({
            name : $index,
            y    : Number($value),
            color: '#0033CC'
          })  
        }
        if($index == "Shorter than expected"){
          pieChartData.push({
            name : $index,
            y    : Number($value),
            color: '#006600'
          })  
        }
       
    });
    
//   console.log(pieChartData);
    if(counter == (pieChartData.length)){
        $title = 'No Survey Found';
        pieChartData.length = 0;
         pieChartData.push({
            name : 'No Survey Found',
            y    : 100,
            color: '#000000'
          }) 
    }
    //console.log(pieChartData);
    $('#regionPieChartContainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        exporting: {
            enabled: false 
        },
        title: {
            text: $title
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'User count',
            data: pieChartData
        }]
    });
    
}

/*
 var heatmapData = new Array();
    for(var i in $result){
        if(Number($result[i]['weight'])){
            testData.location = new google.maps.LatLng($result[i]['latitude'],$result[i]['longitude']);
            testData.weight = Number($result[i]['weight']);
            heatmapData.push(testData);
            testData= {
                location:null,
                weight:null
            };
        }
    }
*/

var  testData={
            location:null,
            weight:null
        };
var limit = 0;
function createHeatMap($data){

var heatmapData = new Array();
$("#heatMapLoader").css('display', 'none');
$("#heatMapDiv").show();
limit = $data['lastId']; // setting limit for Next Bunch of data....

var map, pointarray, heatmap;
 for(var i in $data){
     if(!isNaN($data[i]['latitude']) &&  !isNaN($data[i]['longitude'])) {
          testData.location = new google.maps.LatLng($data[i]['latitude'],$data[i]['longitude']);
          testData.weight   = Number($data[i]['weight']); 
          testData.province = Number($data[i]['province']);
          heatmapData.push(testData);
          testData= {
                    location:null,
                    weight:null
                };
     }
   }
 
 var mapOptions = {
    zoom: 5,
    center: new google.maps.LatLng(22.268763552489748,47.87841796875),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
    setInterval(function(){    // Request For refreshing Map with newly Added Data.....
       // Getting Selected Filters.........
       
       var filterDataObj = new pulseFilters();
        $filter = $("#regionFilters").find("select");
        $.each($filter,function(i,j){
            filterDataObj.setFilter(j['name'],j['value']);
        });
        $filter = $("#regionFilters").find("input");
        $.each($filter,function(i,j){
            filterDataObj.setFilter(j['name'],j['value']);
        });
        
        $filterData = filterDataObj.getFilter(); 
        $filterData['limit'] = limit;
        $requestData = {
                "requestFor"  : "region-heat-map-data",
                "filter-data" :  $filterData
            };
         
       $.post(baseUrl+''+baseBank+'/request.php',$requestData,
       function($response){
            $data = JSON.parse($response);
            if($data['regionHeatMapData'].length != 0){
                var $resultArray = $data['regionHeatMapData'];
                for(var i in $resultArray){
                     if(!isNaN($resultArray[i]['latitude']) &&  !isNaN($resultArray[i]['longitude'])) {
                          $.each(heatmapData, function($index, $value){
                                if($value['province'] == $resultArray[i]['province']){
                                    $value['weight']   = $value['weight'] + 1;
                                }else{
                                    testData.location = new google.maps.LatLng($resultArray[i]['latitude'],$resultArray[i]['longitude']);
                                    testData.weight = 1;
                                    testData.province = $resultArray[i]['province'];
                                    heatmapData.push(testData);  
                                }
                                
                                // Pushing NEwly Added Data to heatmapData ARRAY------
                                    testData= {
                                    location:null,
                                    weight:null
                                };
                          });
                          
                          
                          
                      }
                     limit++;
                   }
                heatmap = new google.maps.visualization.HeatmapLayer({
                    data: heatmapData, // appending updated array to to Map..
                    radius:50
                  });
                 heatmap.setMap(map);
               
            }
       });
    },6000);
  map = new google.maps.Map(document.getElementById('heatMapContainer'),
      mapOptions);

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: heatmapData,
    radius:50
  });

  heatmap.setMap(map);
}
// Pagination FOr Drill Pages-----------------------------

function gotoDrilledPage($element){
    $pageNo = $("#gotoDrilledPage option:selected").val();
    changeDrilledPage('other', $pageNo);
    
}

function changeDrilledPage($type, $pageNo){
    $("#paginationData").hide();
    $("#drilledScoreLoader").css("display", "inline");
    $pageNo = $("#gotoDrilledPage option:selected").val();
    if($type == "next"){
        $pageNo = Number($pageNo) + 5;
        $("#gotoDrilledPage").val($pageNo).attr("selected", "selected");
        
        if($("#gotoDrilledPage option:selected").attr("pageClass") == "last"){
           $("#changeDrilledPageNextButton").hide();
           $("#changeDrilledPagePrevButton").show();
        }else if($("#gotoDrilledPage :selected").attr('value') == 0){
           $("#changeDrilledPageNextButton").show();
           $("#changeDrilledPagePrevButton").hide();
        }else{
           $("#changeDrilledPageNextButton").show();
           $("#changeDrilledPagePrevButton").show();
        }
    }else if($type == "prev"){
        $pageNo = Number($pageNo) - 5;
        $("#gotoDrilledPage").val($pageNo).attr("selected", "selected");
        if($("#gotoDrilledPage option:selected").attr("pageClass") == "last"){
           $("#changeDrilledPageNextButton").hide();
           $("#changeDrilledPagePrevButton").show();
        }else if($("#gotoDrilledPage :selected").attr('value') == 0){
           $("#changeDrilledPageNextButton").show();
           $("#changeDrilledPagePrevButton").hide();
        }else{
           $("#changeDrilledPageNextButton").show();
           $("#changeDrilledPagePrevButton").show();
        }
    }else{
        if($("#gotoDrilledPage option:selected").attr("pageClass") == "last"){
           $("#changeDrilledPageNextButton").hide();
           $("#changeDrilledPagePrevButton").show();
        }else if($("#gotoDrilledPage :selected").attr('value') == 0){
           $("#changeDrilledPageNextButton").show();
           $("#changeDrilledPagePrevButton").hide();
        }else{
           $("#changeDrilledPageNextButton").show();
           $("#changeDrilledPagePrevButton").show();
        }
    }
    
    $parentFilterDiv = $(".drillData").attr("filter-div");
    $requestData = {
            "requestFor"       :   $(".drillData").data("request-for"),
            "drill-for"        :   $(".drillData").attr('drill-for'),
            "filter-data"      : {
            }
    };
   
    for(var i in $elementList){
        $($parentFilterDiv).find($elementList[i]).each(function(){
            $requestData["filter-data" ][$(this).attr('name')] = $(this).val();
        });
        
    }
    $requestData["filter-data" ]['page-no'] = $pageNo;
    $requestObject.initialize($requestData,"fillDrillData",null); 
}

function fillDrillData($result){
    $("#drilledScoreLoader").css("display", "none");
    $("#paginationData").show();
    $("#paginationData").html($result);
}


function serviceTimePieChart($data){
    var $title = 'Service Time';
    var pieChartData = [];
    var counter = 0;
    
    $.each($data['serviceTimeData'], function($index, $value){
        if(Number($value) == 0.0){
            counter++;
        }
        pieChartData.push({
            name : $index,
            y    : Number($value['value']),
            color: $value['color']
        }) ;
    });
   
    if(counter == (pieChartData.length)){
        $title = 'No Survey Found';
        pieChartData.length = 0;
         pieChartData.push({
            name : 'No Survey Found',
            y    : 100,
            color: '#000000'
          }) 
    }
    //console.log(pieChartData);
    $('#service_time_pie_chart_container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        exporting: {
            enabled: false 
        },
        title: {
            text: $title
        },
        credits : {
            enabled : false
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        series: [{
            type: 'pie',
            name: 'User count',
            innerSize: '50%',
            data: pieChartData
        }]
    });
    
}
var series;
var globalCounter = 0;
var otherInfo = [];
var previousIntervals;
function getNextPointPackets(){
   //console.log($("#regionFilters").find("select"));
    var filterDataObj = new pulseFilters();
    
    $filter = $("#regionFilters").find("select");
    $.each($filter,function(i,j){
        filterDataObj.setFilter(j['name'],j['value']);
    })
    $filter = $("#regionFilters").find("input");
    $.each($filter,function(i,j){
        filterDataObj.setFilter(j['name'],j['value']);
    })
    $filterData = filterDataObj.getFilter();
   // console.log($filterData);
    getFilterRequestData;
    $requestData = {
            "requestFor"  : "response-data",
            "lastId"      :  lastId,
            "filter-data" :  $filterData
        };

        
   $.ajax({
       url     : baseUrl+''+baseBank+'/request.php',
       type    : 'POST',
       data    : $requestData,
       async   : false,
       success : function(data){
            if(data != null){

                data = JSON.parse(data);
                
                lastId = data['lastId'];
                var otherData = {
                    'email_id'         : data['email_id'],
                    'service_category' : data['category']
                }
                otherInfo.push(otherData);
                var pointData = {
                    x: globalCounter ,
                    y: Number(data['y']),
                    category_name : data['category'],
                    marker: {
                        symbol: 'url('+data['icon_url']+')',
                        height: 5,
                        width: 5
                    }
                }
                series.addPoint(pointData, true, true);
                globalCounter++;
            }else{
                console.log("no return");
            }
       }
       
   });
}

var serviceQualityPulseChart = function($data){
    series = "";
    globalCounter = 0;
    otherInfo = [];
    if(previousIntervals)
        clearInterval(previousIntervals);
    
    if($data['serviceQualityData'].length){
        $('#regionGraphDiv').highcharts({
            chart: {
                type: 'spline',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {
                        // set up the updating of the chart each second
                        series = this.series[0];
                        previousIntervals = setInterval(function(){
                            getNextPointPackets()
                       },2000);
                    }
                }
            },
            title: {
                text: 'Service Quality'
            },
            xAxis: {
                allowDecimals : false,
                 tickInterval: 1,
                labels : {
                    rotation:-45,
                    formatter : function(){
                        if(otherInfo[this.value].email_id)
                            return otherInfo[this.value].email_id;
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Value'
                },
                min : -3,
                max : 5,
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                      otherInfo[this.x].service_category   + '<br/> <b>Point  </b>' + this.y;
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    marker: {
                        radius: 4,
                        lineColor: '#666666',
                        lineWidth: 1
                    }
                }
            },
            series: [{
                name: 'Service Quality',
                data: (function () {
                    // generate an array of random data
                    var data = []
                    $.each($data['serviceQualityData'], function($index, $value){

                        lastId = $value['lastId'];

                        var otherData = {
                            'email_id' : $value['email_id'],
                            'service_category' : $value['category']
                        }
                        otherInfo.push(otherData);
                        data.push({
                            x: globalCounter ,
                            y: Number($value['service_type']),
                            marker: {
                                symbol: 'url('+$value['icon_url']+')',
                                height: 5,
                                width: 5
                            }

                        });
                        globalCounter++;
                    });
                    console.log(otherInfo);
                    return data;
                }())
            }]
        });
    }else{
        $("#regionGraphDiv").html('<b style="position: relative;top:185px;">No data under selected filters</b>');
    }
    
}
// Pagination ENDS HERE------------------------------