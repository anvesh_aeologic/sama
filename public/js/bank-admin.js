
function getTicketPopupDetail($element){
    $postData = {
        'requestFor'    :   'ticket-detail',
        'ticketNumber'  : $($element).attr('ticket-number')
    }
    
}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function branchProfileDisplayName($branchName, $region, $city) {
    var $branchName = $branchName.toLowerCase();
    var $region = $region.toLowerCase();
    var $city = $city.toLowerCase();

    var $branchProfileDisplayName = $branchName;

    if (!($branchProfileDisplayName.indexOf($region) >= 0)) {
        $branchProfileDisplayName += ', ' + $region;
    }

    if (!($branchProfileDisplayName.indexOf($city) >= 0)) {
        $branchProfileDisplayName += ', ' + $city;
    }

    return toTitleCase($branchProfileDisplayName);
}


function branchTypeToDisplay(branchType){
    if(branchType === 'gents'){
        return 'Men';
    } else if(branchType === 'gents and ladies'){
        return 'Men and Ladies';
    }
    return branchType;
}