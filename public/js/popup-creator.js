var PopupCreator = { 
    initialize : function($element,$popupElement,$callBackFunction) {
        var that = this;
        
        if($callBackFunction=='getPreviousDepartmentScoreDetail'||$callBackFunction=='getPreviousScoreDetail'){
             $($popupElement).html($("#scorePopupLoader").html());
        } else{
             $($popupElement).html($("#popupLoader").html());
        }
        
        $($element).colorbox({
            inline:     true,
            transition: "elastic",
            href:       $popupElement,
            title:      false,
            width:      this.getWidth(),
            height:     this.getHeight(),
        }, function() {
            if($callBackFunction){
                window[$callBackFunction]($element);
            }
        });
    },

    getWidth : function(){
        return this.width;
    },

    getHeight : function(){
        return this.height;
    },

    setWidth : function(width){
        this.width = width;
    },
    
    setClass : function(className){
        this.className = className;
    },


    setHeight : function(height){
        this.height = height;
    },

    open : function($popupElement, $callBackFunction, $param){
        var that = this;
        
        $($popupElement).html($("#popupLoader").html());
        $.colorbox({ 
                scalePhotos: true,
                inline:true,
                transition:"elastic",
                title:false,
                width:this.getWidth(),
                height:this.getHeight(),
                href:$popupElement
            }, function() {
            if($callBackFunction){
                window[$callBackFunction]($param);
            }
        });
    }  
};


