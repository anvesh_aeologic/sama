var pathArray = window.location.href.split( '/' );
var protocol = pathArray[0];
var host     = pathArray[2];
var hosturl      = protocol + '//' + host;


$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $(document).find('.popover').each(function(){
            $(this).remove();
        })
    } 
});
$(document).ready(function(){
    //themes, change CSS with JS
    //default theme(CSS) is cerulean, change it if needed
    var current_theme = 'spacelab';
    switch_theme(current_theme);
	
    $('#themes a[data-value="'+current_theme+'"]').find('i').addClass('icon-ok');
				 
    $('#themes a').click(function(e) {
        e.preventDefault();
        current_theme=$(this).attr('data-value');
        $.cookie('current_theme',current_theme,{
            expires:365
        });
        switch_theme(current_theme);
        $('#themes i').removeClass('icon-ok');
        $(this).find('i').addClass('icon-ok');
    });
	
	
    function switch_theme(theme_name)
    {
        $('#bs-css').attr('href','../css/bootstrap-'+theme_name+'.css');
    }
	
    //ajax menu checkbox
    $('#is-ajax').click(function(e){
        $.cookie('is-ajax',$(this).prop('checked'),{
            expires:365
        });
    });
    $('#is-ajax').prop('checked',$.cookie('is-ajax')==='true' ? true : false);
	
    //disbaling some functions for Internet Explorer
    if($.browser.msie)
    {
        $('#is-ajax').prop('checked',false);
        $('#for-is-ajax').hide();
        $('#toggle-fullscreen').hide();
        $('.login-box').find('.input-large').removeClass('span10');
		
    }
	
	
    //highlight current / active link
    $('ul.main-menu li a').each(function(){
        if($($(this))[0].href==String(window.location))
            $(this).parent().addClass('active');
    });
	
    //establish history variables
    var
    History = window.History, // Note: We are using a capital H instead of a lower h
    State = History.getState(),
    $log = $('#log');

    //bind to State Change
    History.Adapter.bind(window,'statechange',function(){ // Note: We are using statechange instead of popstate
        var State = History.getState(); // Note: We are using History.getState() instead of event.state
        $.ajax({
            url:State.url,
            success:function(msg){
                $('#content').html($(msg).find('#content').html());
                $('#loading').remove();
                $('#content').fadeIn();
                docReady();
            }
        });
    });
	
    //ajaxify menus
    $('a.ajax-link').click(function(e){
        if($.browser.msie) e.which=1;
        if(e.which!=1 || !$('#is-ajax').prop('checked') || $(this).parent().hasClass('active')) return;
        e.preventDefault();
        if($('.btn-navbar').is(':visible'))
        {
            $('.btn-navbar').click();
        }
        $('#loading').remove();
        $('#content').fadeOut().parent().append('<div id="loading" class="center">Loading...<div class="center"></div></div>');
        var $clink=$(this);
        History.pushState(null, null, $clink.attr('href'));
        $('ul.main-menu li.active').removeClass('active');
        $clink.parent('li').addClass('active');	
    });
	
    //animating menus on hover
    $('ul.main-menu li:not(.nav-header)').hover(function(){
        $(this).animate({
            'margin-left':'+=5'
        },300);
    },
    function(){
        $(this).animate({
            'margin-left':'-=5'
        },300);
    });
	
    //other things to do on document ready, seperated for ajax calls
    docReady();
    resizeController();
});
		
function resizeController(){
    $(".controls").find('div').each(function(){
        if($(this).attr('id')){
            var divId = ($(this).attr('id')).split("_");
            if(divId[1] == 'chzn')
                $(this).width('130px');
        }
    })
}

var isMapInitialized = false;	
var isMerchantMapInitialized = false;	
   
function docReady(){
     //merchantMapInitialize();
     //isMerchantMapInitialized = true;
   
    //prevent # links from moving to top
    
    $('a[href="#"][data-top!=true]').click(function(e){
        e.preventDefault();
    });
	
    //rich text editor
    $('.cleditor').cleditor();
	
    //datepicker
    $('.datepicker').datepicker();
	
    //notifications
    $('.noty').click(function(e){
        e.preventDefault();
        var options = $.parseJSON($(this).attr('data-noty-options'));
        noty(options);
    });


    //uniform - styler for checkbox, radio and file input
    $("input:checkbox, input:radio, input:file").not('[data-no-uniform="true"],#uniform-is-ajax').uniform();

    //chosen - improves select
    $('[data-rel="chosen"],[rel="chosen"]').chosen();

    //tabs
    if(currentPage == 'dashboard'){
       //$('.tickercontainer').css('display','none'); 
        $('#myTab a:first').tab('show');
    }else{
        //$('.tickercontainer').css('display','none'); 
        $('#myTab a:last').tab('show');
    }
    $('#myTab a').click(function (e) {
        $('#lastVisitDataDivId').toggle();
        $('#randomNewsId').toggle();
        e.preventDefault();
        $(document).find('.popover').each(function(){
            $(this).remove();
        })
        if($(this).attr('href') == '#MapGallery'){
            if(!isMapInitialized)
               //$('#loading_screen').show();
                setTimeout(function() { mapInitialize() }, 3000);
                isMapInitialized = true;
         }
        
        if($(this).attr('href') == '#ImageGallery'){
            testgetResult('merchantImage','','','');
        }
        $(this).tab('show');
    });
	
    //otherTab
    $('#mainTab a:first').tab('show');
    $('#mainTab a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    //makes elements soratble, elements that sort need to have id attribute to save the result
    $('.sortable').sortable({
        revert:true,
        cancel:'.btn,.box-content,.nav-header',
        update:function(event,ui){
        //line below gives the ids of elements, you can make ajax call here to save it to the database
        //console.log($(this).sortable('toArray'));
        }
    });

    //slider
    $('.slider').slider({
        range:true,
        values:[10,65]
        });

    //tooltip
    $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({
        "placement":"bottom",
        delay: {
            show: 400, 
            hide: 200
        }
    });

//auto grow textarea
$('textarea.autogrow').autogrow();

//popover
$('[rel="popover"],[data-rel="popover"]').popover();
	
//popupform
$('[rel="popupform"],[data-rel="popupform"]').popupform();

//file manager
var elf = $('.file-manager').elfinder({
    url : 'misc/elfinder-connector/connector.php'  // connector URL (REQUIRED)
}).elfinder('instance');

//iOS / iPhone style toggle switch
$('.iphone-toggle').iphoneStyle();

//star rating
$('.raty').raty({
    score : 4 //default stars
});

//uploadify - multiple uploads
$('#file_upload').uploadify({
    'swf'      : 'misc/uploadify.swf',
    'uploader' : 'misc/uploadify.php'
// Put your options here
});

$('.thumbnail a').colorbox({transition:"elastic", maxWidth:"95%", maxHeight:"95%"});

//gallery fullscreen
$('#toggle-fullscreen').button().click(function () {
    var button = $(this), root = document.documentElement;
    if (!button.hasClass('active')) {
        $('#thumbnails').addClass('modal-fullscreen');
        if (root.webkitRequestFullScreen) {
            root.webkitRequestFullScreen(
                window.Element.ALLOW_KEYBOARD_INPUT
                );
        } else if (root.mozRequestFullScreen) {
            root.mozRequestFullScreen();
        }
    } else {
        $('#thumbnails').removeClass('modal-fullscreen');
        (document.webkitCancelFullScreen ||
            document.mozCancelFullScreen ||
            $.noop).apply(document);
    }
});

//tour
if($('.tour').length && typeof(tour)=='undefined')
{
    var tour = new Tour();
    tour.addStep({
        element: ".span10:first", /* html element next to which the step popover should be shown */
        placement: "top",
        title: "Custom Tour", /* title of the popover */
        content: "You can create tour like this. Click Next." /* content of the popover */
    });
    tour.addStep({
        element: ".theme-container",
        placement: "left",
        title: "Themes",
        content: "You change your theme from here."
    });
    tour.addStep({
        element: "ul.main-menu a:first",
        title: "Dashboard",
        content: "This is your dashboard from here you will find highlights."
    });
    tour.addStep({
        element: "#for-is-ajax",
        title: "Ajax",
        content: "You can change if pages load with Ajax or not."
    });
    tour.addStep({
        element: ".top-nav a:first",
        placement: "bottom",
        title: "Visit Site",
        content: "Visit your front end from here."
    });
		
    tour.restart();
}

//datatable
$('.datatable').dataTable({
    "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span12'i><'span12 center'p>>",
    "sPaginationType": "bootstrap",
    "oLanguage": {
        "sLengthMenu": "_MENU_ records per page"
    }
} );
$('.btn-close').click(function(e){
    e.preventDefault();
    $(this).parent().parent().parent().fadeOut();
});
$('.btn-minimize').click(function(e){
    e.preventDefault();
    var $target = $(this).parent().parent().next('.box-content');
    if($target.is(':visible')) $('i',$(this)).removeClass('icon-chevron-up').addClass('icon-chevron-down');
    else 					   $('i',$(this)).removeClass('icon-chevron-down').addClass('icon-chevron-up');
    $target.slideToggle();
});
$('.btn-setting').click(function(e){
    e.preventDefault();
    $('#myModal').modal('show');
});



		
//initialize the external events for calender

$('#external-events div.external-event').each(function() {

    // it doesn't need to have a start or end
    var eventObject = {
        title: $.trim($(this).text()) // use the element's text as the event title
    };
		
    // store the Event Object in the DOM element so we can get to it later
    $(this).data('eventObject', eventObject);
		
    // make the event draggable using jQuery UI
    $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
    });
		
});


//initialize the calendar
$('#calendar').fullCalendar({
    header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
    },
    editable: true,
    droppable: true, // this allows things to be dropped onto the calendar !!!
    drop: function(date, allDay) { // this function is called when something is dropped
		
        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');
			
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
			
        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
			
        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
			
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            $(this).remove();
        }
			
    }
});
	
	
//chart with points
if($("#sincos").length){
    var sin = [], cos = [];

    for (var i = 0; i < 14; i += 0.5) {
        sin.push([i, Math.sin(i)/i]);
        cos.push([i, Math.cos(i)]);
    }

    var plot = $.plot($("#sincos"),
        [ {
            data: sin, 
            label: "sin(x)/x"
        }, {
            data: cos, 
            label: "cos(x)"
        } ], {
            series: {
                lines: {
                    show: true
                },
                points: {
                    show: true
                }
            },
            grid: {
                hoverable: true, 
                clickable: true, 
                backgroundColor: {
                    colors: ["#fff", "#eee"]
                }
            },
        yaxis: {
            min: -1.2, 
            max: 1.2
        },
        colors: ["#539F2E", "#3C67A5"]
    });

function showTooltip(x, y, contents){
    $('<div id="tooltip">' + contents + '</div>').css( {
        position: 'absolute',
        display: 'none',
        top: y + 5,
        left: x + 5,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#dfeffc',
        opacity: 0.80
    }).appendTo("body").fadeIn(200);
}

var previousPoint = null;
$("#sincos").bind("plothover", function (event, pos, item) {
    $("#x").text(pos.x.toFixed(2));
    $("#y").text(pos.y.toFixed(2));

    if (item) {
        if (previousPoint != item.dataIndex) {
            previousPoint = item.dataIndex;

            $("#tooltip").remove();
            var x = item.datapoint[0].toFixed(2),
            y = item.datapoint[1].toFixed(2);

            showTooltip(item.pageX, item.pageY,
                item.series.label + " of " + x + " = " + y);
        }
    }
    else {
        $("#tooltip").remove();
        previousPoint = null;
    }
});
		


$("#sincos").bind("plotclick", function (event, pos, item) {
    if (item) {
        $("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
        plot.highlight(item.series, item.datapoint);
    }
});
}
	
//flot chart
if($("#flotchart").length)
{
    var d1 = [];
    for (var i = 0; i < Math.PI * 2; i += 0.25)
        d1.push([i, Math.sin(i)]);
		
    var d2 = [];
    for (var i = 0; i < Math.PI * 2; i += 0.25)
        d2.push([i, Math.cos(i)]);

    var d3 = [];
    for (var i = 0; i < Math.PI * 2; i += 0.1)
        d3.push([i, Math.tan(i)]);
		
    $.plot($("#flotchart"), [
    {
        label: "sin(x)",  
        data: d1
    },

    {
        label: "cos(x)",  
        data: d2
    },

    {
        label: "tan(x)",  
        data: d3
    }
    ], {
        series: {
            lines: {
                show: true
            },
            points: {
                show: true
            }
        },
        xaxis: {
            ticks: [0, [Math.PI/2, "\u03c0/2"], [Math.PI, "\u03c0"], [Math.PI * 3/2, "3\u03c0/2"], [Math.PI * 2, "2\u03c0"]]
        },
        yaxis: {
            ticks: 10,
            min: -2,
            max: 2
        },
        grid: {
            backgroundColor: {
                colors: ["#fff", "#eee"]
            }
        }
    });
}
	
//stack chart
if($("#stackchart").length)
{
    var d1 = [];
    for (var i = 0; i <= 10; i += 1)
        d1.push([i, parseInt(Math.random() * 30)]);

    var d2 = [];
    for (var i = 0; i <= 10; i += 1)
        d2.push([i, parseInt(Math.random() * 30)]);

    var d3 = [];
    for (var i = 0; i <= 10; i += 1)
        d3.push([i, parseInt(Math.random() * 30)]);

    var stack = 0, bars = true, lines = false, steps = false;

    function plotWithOptions() {
        $.plot($("#stackchart"), [ d1, d2, d3 ], {
            series: {
                stack: stack,
                lines: {
                    show: lines, 
                    fill: true, 
                    steps: steps
                },
                bars: {
                    show: bars, 
                    barWidth: 0.6
                }
            }
        });
    }

    plotWithOptions();

    $(".stackControls input").click(function (e) {
        e.preventDefault();
        stack = $(this).val() == "With stacking" ? true : null;
        plotWithOptions();
    });
    $(".graphControls input").click(function (e) {
        e.preventDefault();
        bars = $(this).val().indexOf("Bars") != -1;
        lines = $(this).val().indexOf("Lines") != -1;
        steps = $(this).val().indexOf("steps") != -1;
        plotWithOptions();
    });
}

//pie chart
var data = [
{
    label: "Internet Explorer",  
    data: 12
},
{
    label: "Mobile",  
    data: 27
},
{
    label: "Safari",  
    data: 85
},
{
    label: "Opera",  
    data: 64
},
{
    label: "Firefox",  
    data: 90
},
{
    label: "Chrome",  
    data: 112
}
];
	
if($("#piechart").length)
{
    $.plot($("#piechart"), data,
    {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true,
            clickable: true
        },
        legend: {
            show: false
        }
    });
		
    function pieHover(event, pos, obj)
    {
        if (!obj)
            return;
        percent = parseFloat(obj.series.percent).toFixed(2);
        $("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' ('+percent+'%)</span>');
    }
    $("#piechart").bind("plothover", pieHover);
}
	
//donut chart
if($("#donutchart").length)
{
    $.plot($("#donutchart"), data,
    {
        series: {
            pie: {
                innerRadius: 0.5,
                show: true
            }
        },
        legend: {
            show: false
        }
    });
}




// we use an inline data source in the example, usually data would
// be fetched from a server
var data = [], totalPoints = 300;
function getRandomData() {
    if (data.length > 0)
        data = data.slice(1);

    // do a random walk
    while (data.length < totalPoints) {
        var prev = data.length > 0 ? data[data.length - 1] : 50;
        var y = prev + Math.random() * 10 - 5;
        if (y < 0)
            y = 0;
        if (y > 100)
            y = 100;
        data.push(y);
    }

    // zip the generated y values with the x values
    var res = [];
    for (var i = 0; i < data.length; ++i)
        res.push([i, data[i]])
    return res;
}

// setup control widget
var updateInterval = 30;
$("#updateInterval").val(updateInterval).change(function () {
    var v = $(this).val();
    if (v && !isNaN(+v)) {
        updateInterval = +v;
        if (updateInterval < 1)
            updateInterval = 1;
        if (updateInterval > 2000)
            updateInterval = 2000;
        $(this).val("" + updateInterval);
    }
});

//realtime chart
if($("#realtimechart").length)
{
    var options = {
        series: {
            shadowSize: 1
        }, // drawing is faster without shadows
        yaxis: {
            min: 0, 
            max: 100
        },
        xaxis: {
            show: false
        }
    };
    var plot = $.plot($("#realtimechart"), [ getRandomData() ], options);
    function update() {
        plot.setData([ getRandomData() ]);
        // since the axes don't change, we don't need to call plot.setupGrid()
        plot.draw();
			
        setTimeout(update, updateInterval);
    }

    update();
}
}


//additional functions for data table
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings ){
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
 }

$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };

            $(nPaging).addClass('pagination').append(
                '<ul>'+
                '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                '</ul>'
                );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', {
                action: "previous"
            }, fnClickHandler );
            $(els[1]).bind( 'click.DT', {
                action: "next"
            }, fnClickHandler );
        },

        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);

            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }

            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();

                // add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                    .insertBefore( $('li:last', an[i])[0] )
                    .bind('click', function (e) {
                        e.preventDefault();
                        oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                        fnDraw( oSettings );
                    } );
                }

                // add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }

                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }
        }
    }
});



function malfunctionReport(contianer,seriesValue,labels,title){
     Highcharts.setOptions({
     colors: ['#50B432','#B94846']
    });
    var chart2 = new Highcharts.Chart({
        chart: {
            renderTo: contianer,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: title
        },
        exporting: {
            buttons: {

                printButton:{
                    enabled:true
                },
                exportButton: {
                    enabled:true
                }
            }
        },
  
        subtitle: {
            text: null
        },
       /* tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            percentageDecimals: 1
        }, */
        
         tooltip: {
             formatter: function() {
                return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage) +' %';
             }
         },
         
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: labels,
                    color: '#000000',
                    connectorColor: '#000000',
                    formatter: function() {
                        return '<b>'+ this.point.name+'</b>: '+Math.round((this.point.percentage))+"%";
                    }
                },
                showInLegend: true
            }
        },
        credits : {
            enabled : false
        },
        series: seriesValue
               
    });

}


var chart6 
function resultReport(contianer,seriesValue,labels,title){
    chart6 = new Highcharts.Chart({
        chart: {
            renderTo: contianer,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: title,
            x: -30,
            y: 15
           
        },
        colors: ['#CC3C17','#82AC0C']
        ,
        exporting: {
            buttons: {
                printButton:{
                    enabled:true
                },
                exportButton: {
                    enabled:true
                }
            }
        },
  
                subtitle: {
                    text: null
                },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            percentageDecimals: 2
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: labels,
                    color: '#000000',
                    connectorColor: '#000000',
                    formatter: function() {
                        return '<b>'+ this.point.name +'</b>: '+(this.point.percentage).toFixed(2)+"%"
                    }
                },
               showInLegend: true
            }
        },
        credits : {
            enabled : false
        },
        legend: {
            labelFormatter: function() {
                if(this.percentage != undefined){
                    if(this.name == 'Total'){
                        return this.name;
                    }else{
                        return this.name + " (" + (this.percentage).toFixed(2) + "%)"
                    }
                }else{
                    return this.name + " (" + this.percentage + "%)";
                }
            }
        },
        series: seriesValue
               
    });
    return chart6;

}

function redrawLegend(){
    $.each(chart6.series[0].points, function(point) {
        delete chart6.series[0].points[point].legendItem;
    });
    chart6.legend.destroy();
    chart6.legend.render();
}
        
        
function terminalHealthReport(){
    var chart3 = new Highcharts.Chart({
        chart: {
            renderTo: 'terminalHealthReport',
            type: 'line',
            marginRight: 130,
            marginBottom: 25
        },
        title: {
            text: '',
            x: -20 //center
        },
        colors:['#FF0000'],
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul']
        },
        yAxis: {
            title: {
                text: 'Number of incidents'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#B41016'
            }]
        },
        tooltip: {
            formatter: function() {
                return '<b>'+ this.series.name +'</b><br/>'+
                this.x +': '+ this.y;
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        series: [{
            name: 'Incidents',
            data: [12.0, 12.3,13.1,13.4,7.8,10.3,13.4]
        }]
    });
}
    


var map1;
function initialize(container) {
    var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(23.388580097119107, 46.644671386718755),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        disableDefaultUI: false,
        panControl: false,
        zoomControl: false,
        scaleControl: false,
        streetViewControl: false,
        overviewMapControl:false,
    };
       
    map1 = new google.maps.Map(document.getElementById(container),mapOptions);
    if(currentPage == 'dashboard'){
        createMarker(new google.maps.LatLng(23.388580097119107, 46.644671386718755));
    }
}

var marker;
var infoWindow = new InfoBubble({
    minHeight:40,
    widthX:90,
});
function createMarker(position){
    marker = new google.maps.Marker({
        position:position,
        map: map1,
        icon:new google.maps.MarkerImage('https://gothere.sg/static/img/icons/api/markermed_blue.png',null, null, null, new google.maps.Size(20,32))
    })
    marker.setMap(map1);
    infoWindow.setContent('<div style="height:40px;width:90px;" ><div align="center"><img src="../img/banklogo/extralogo.png" width="90" height="45"/></div></div>');
    infoWindow.open(map1,marker);
}

function eventFire(el, etype){
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}


/*########################################################### >Common function to show terminal questions details'##########################################3 */
/*Coded by Dinesh */
 //Checks for valid email address
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}


 
 function printThis(){
   $('#inputSuccess').hide();
   $('#inputSuccessButton').hide();
   window.print();  
 }
 
 function mailThis(){
    $('#inputSuccess').show();
    $('#inputSuccessButton').show();
    $('#sendSuccessFuly').hide(); 
    
 }
 
 function sendMailLink(){
    var errorFlag = 1;
    var userMailAddress = $('#inputSuccess').val();
     if (!isValidEmailAddress(userMailAddress)) {
        alert('Please enter valid email address');
        errorFlag = 0;
    }  
    if(errorFlag){
     $('#mailSendingLoader').show();
     $.ajax({
        url: 'model/tbl_survey.php',
        type: 'POST',
        datatype: 'JSON',
        data: {action: 'SENDNCBMERCHANTMAIL',emailAddress:userMailAddress},
        success: execueSuccessMail,
        error: function() {
            alert('No Data Found');
        }
    });
   } 
 }

function execueSuccessMail(){
   $('#inputSuccess').hide();
   $('#inputSuccessButton').hide(); 
   $('#mailSendingLoader').hide(); 
   $('#sendSuccessFuly').show(); 
}
/*#########################*/
    /*Global variables */
      $qidLatest = '';
      $typeLatest = '';
      $tabTypeLatest = '';
      $sortbyLatest = '';
      $sortOrderLatest = ''; 

/*########################*/
 var headingMessage;
 $orderchangeAgent = 'DESC';
 $merchant_name = 'merchant_name';
 $branch_address = 'branch_address'; 
 function getTerminalList($qid, $type, $tabType, $sortOn, $sortOrder, $page){ 
    /*Setting the value for globla variables */
       $qidLatest = $qid;
       $typeLatest = $type;
       $tabTypeLatest = $tabType;
       $sortbyLatest = $sortOn;
       $sortOrderLatest = $sortOrder;
    /* End seting global variables  */
  
    $(".dashboard-list li a span").live("click", function () { 
        headingMessage = $(this).text();
     });
    
   
    var headerVal = $tabType;
        $total_pages = '';
        $selected = '';
        $previous = '';
        $next = '';
        $totalCount = '';
        
        if(!$page){
          $page ='';
         } 
        
     $(".dashboard-list li b").live("click", function () { 
        headingMessage = $(this).attr('todisplay');
     });
     
     /* $('#socialWidgets #emailSend').live("click", function () { 
        $("#emailSendContainer").colorbox({inline:true, width:"50%"});
     }); */
                                       
                                      
     $.colorbox({ inline: true,
                  transition: "elastic",
                  title: false,width: "750px",
                  height: "726px",
                  href: "#containerId",
                  onClosed:function(){
                     $("#containerId").html('');
                   }
                });
                  
      $('#containerId').html('<div align="center" style="margin-top:180px;margin-left:20px;">' +
            '<div align="center" style="font-weight: bold;padding-bottom:10px;">Instant Intelligence<br/></div>' +
            '<img title="../img/ajax-loaders/bsfmap-loader1.gif" src="../img/ajax-loaders/bsfmap-loader1.gif">' +
          '</div>'
         );            
      
        $limit = 10;
        
      $postpage = $page;
      if($postpage == 0){
      //do nothing
       } else {
         --$postpage;
       }
      $.ajax({  
        url : hosturl+"/Bank-Admin/merchant/model/tbl_survey.php",
        type: 'POST',
        datatype: 'JSON',
        data: {action:'GETTERMINALLIST', questionId:$qid, answerType:$type , page:$postpage, limit:$limit, tabType:$tabType, sortOn:$sortOn, sortOrder:$sortOrder},
        success: function (result) {
            var objListItem = $.parseJSON(result);
            var objList     = objListItem.data;
                $totalCount = objListItem.totalCount;
                $total_records = $totalCount;
                $total_pages = Math.ceil($total_records / 10);
                $('#containerId').html('');
            var responseHtml = '<div class="row-fluid sortable ui-sortable" style="">'+
                                 '<div class="box span12" style="">'+
                                '<div data-original-title="" class="box-header well">'+
                                    '<h2><i class="icon-user"></i>&nbsp;&nbsp;<span id="department_nameAge">'+headerVal+'</span></h2>'+
                                '</div>'+
                                '<div id="socialWidgets" class="addthis_toolbox addthis_default_style addthis_32x32_style" style="float:left;margin-left:2px;">'+
                                  '<a href="xlsfile/issueslist.xls" title="Click here, to download list." data-rel="tooltip" class="btn" style="margin-left:23px;height:23px;margin-top:1px;font-weight:bold;"><span title=".icon32  .icon-blue  .icon-archive " class="icon32 icon-blue icon-archive"></span></a>'+
                                   '<a title="Print">'+
                                   '</a>'+
                                  '<a title="Email" id="emailSend" >'+
                                       '<img src="../img/mail.png"  style="cursor:pointer;height:39px;width:53px;" id="emailImg" onclick="mailThis()" />'+
                                   '</a>'+
                                '<input type="text" id="inputSuccess"  placeholder="Please enter mail address." style="margin-left:20px;margin-top:5px;display:none;">&nbsp;<button id="inputSuccessButton" class="btn btn-small btn-success" onclick="sendMailLink()" style="display:none;margin-top:-4px;"><i class="icon-upload icon-white"></i>&nbsp;Send</button>&nbsp;<span id="mailSendingLoader" style="display:none;"><img title="../img/ajax-loaders/ajax-loader-1.gif" src="../img/ajax-loaders/ajax-loader-1.gif"></span><span title=".icon32  .icon-color  .icon-check " class="icon32 icon-color icon-check" style="display:none;" name="sendSuccessFuly" id="sendSuccessFuly"></span>'+
                                '</div>'+
                                '<div id="pagination" style="margin-left:7px; width:100%;">'+
                                  '<div>'+
                                     '<div style="float:right; margin-top:5px;">'+
                                      '<select name="page" id="page" onchange="reloadPopUp(this)" style="float:right; width:50px; margin-right:10px;">';
                                         if($page) {
                                             $selected = $page;
                                          }
                                          for (var i = 1; i <= $total_pages; i++) {
                                                if ($selected > 0 && $selected == i) {
                                                    responseHtml += "<option value="+i+" selected="+$selected+">"+i+"</option>";
                                                } else {
                                                    responseHtml += "<option value="+i+">"+i+"</option>";
                                                } 
                                          }
                                            
                                          responseHtml += '</select></div>';

                                          responseHtml += '<div style="float:right; margin-top:5px;">';   
                                     
                                          if ($page > 1) {
                                              $previous = $page - 1;
                                              responseHtml += '<button class="btn" onclick="reloadPopUpPrev(this)" value="'+$previous+'" style="margin-right:5px;">'+
                                                            '<i class="icon-circle-arrow-left"></i>Previous</button>';
                                           }   
                                         
                                          if ($total_records > 10 && $page * 10 < $total_records) {
                                                $next = $page + 1;
                                                responseHtml += '<button class="btn" onclick="reloadPopUpNext(this)" value="'+$next+'" style="margin-right:5px;">Next&nbsp;'+  
                                                                '<i class="icon-circle-arrow-right"></i>'+                    
                                                       '</button>';
                                           }                                          
                                       responseHtml += '</div>'+
                                                      '</div>'+ 
                                       '<div style="min-height:585px;" id="PopupContentAve" class="box-content">'+
                                         '<div style="margin-top:34px; width:100%;" id="terminalIssues">'+
                                                 '<table class="table table-striped table-bordered">'+
                                    				'<thead>'+
                                    				'<tr>'+
                                    					'<th colspan="4" style="background-color:#939393;"><span style="margin-left:250px;color:white;">'+headingMessage+'</span></th>'+
                                    				'</tr>'+
                                                    '<tr>';
                                                if($sortOn == 'branch_address'){   
                                                    responseHtml +=    '<th onclick="sortColumn(&quot;'+$merchant_name+'&quot;,&quot;'+$orderchangeAgent+'&quot;)" style="cursor:pointer;">';
                                        		         responseHtml +=    'Merchant Name<span id="merchantImageOrder" style="margin-left:10px;"><img src="../img/bg.gif"></span>';
                                   		    	      	 responseHtml +=  '</th>'+
                               				   	        '<th onclick="sortColumn(&quot;'+$branch_address+'&quot;,&quot;'+$orderchangeAgent+'&quot;)" style="cursor:pointer;">';
                            		                    if($orderchangeAgent == "DESC"){
                             					          responseHtml +=    'Branch Address<span id="branchImageOrder" style="margin-left:10px;"><img src="../img/desc.gif"></span>';
                                					    } else {
                                					      responseHtml +=    'Branch Address<span id="branchImageOrder" style="margin-left:10px;"><img src="../img/asc.gif"></span>';  
                                					    }   
                                                       	 responseHtml +=  '</th>'+              
                                                    '</tr>'; 
                                                 
                                                } else {
                                                        responseHtml +=   '<th onclick="sortColumn(&quot;'+$merchant_name+'&quot;,&quot;'+$orderchangeAgent+'&quot;)" style="cursor:pointer;">';
                                		                if($orderchangeAgent == "DESC"){
                             					          responseHtml +=    'Merchant Name<span id="merchantImageOrder" style="margin-left:10px;"><img src="../img/desc.gif"></span>';
                                					    } else {
                                					      responseHtml +=    'Merchant Name<span id="merchantImageOrder" style="margin-left:10px;"><img src="../img/asc.gif"></span>';  
                                					    }
                                               	   responseHtml +=  '</th>'+
                       				   	           '<th onclick="sortColumn(&quot;'+$branch_address+'&quot;,&quot;'+$orderchangeAgent+'&quot;)" style="cursor:pointer;">';
                        		                   responseHtml +=  'Branch Address<span id="branchImageOrder" style="margin-left:10px;"><img src="../img/bg.gif"></span>';
                        		    	           responseHtml += '</th>';    
                                                  }

                                                  var firstMerchantOccurence='';
                                                     for (i in objList) {        
                                                            var c = 0;
                                                            var obj = new NewObject();
                                                            for (j in obj) {
                                                                obj[j] = objList[i][c++];
                                                             }
                                                          if(firstMerchantOccurence.toUpperCase()  == obj.merchant_name.toUpperCase()){
                                                             responseHtml  += '<tr><td>'+obj.merchant_name+'</td><td>'+obj.branch_address+'</td></tr>';  
                                                             firstMerchantOccurence = obj.merchant_name.toUpperCase();
                                                           } else {
                                                             responseHtml  += '<tr style="background-color:#F5F5F5;"><td colspan="2" style="text-align:center;"><b>'+obj.merchant_name+'</b></td></tr>';   
                                                             responseHtml  += '<tr><td>'+obj.merchant_name+'</td><td>'+obj.branch_address+'</td></tr>'; 
                                                             firstMerchantOccurence = obj.merchant_name.toUpperCase(); 
                                                          }
                                                         
                                                     }
                                                     
                                                     responseHtml += '</thead>'+
                                                     '</table>' +
                                        '</div>'+
                                 '</div>'+
                             '</div>'+
                       '</div>';
                      
           $('#containerId').html(responseHtml);
        
        },
        error: function(){
            alert('No Data Found');
        }
    });
  }  
  
/* function getTerminalList($sortby,$sortOrder){
    $sortOn = $sortby;
    $order = $sortOrder;
    getTerminalList( $sortOn, $order)
} */

function reloadPopUp($vl){
    $page = $('#page').val();
    getTerminalList($qidLatest,$typeLatest,$tabTypeLatest,$sortbyLatest,$sortOrderLatest,$page);
}

function reloadPopUpNext(){
  $nextPage = $('#page').val();
  $nextPage = ++$nextPage;
  $('#page').val($nextPage);
  getTerminalList($qidLatest,$typeLatest,$tabTypeLatest,$sortbyLatest,$sortOrderLatest,$nextPage);
}

function reloadPopUpPrev(){
  $prevPage = $('#page').val();
  $prevPage = $prevPage-1;
  $('#page').val($prevPage);
  getTerminalList($qidLatest,$typeLatest,$tabTypeLatest,$sortbyLatest,$sortOrderLatest,$prevPage);  
}

function sortColumn($sortOn, $sortBy){
  $('#branchImageOrder').html('<img src="../img/bg.gif">'); 
  $page = $('#page').val();
  if($sortBy == 'DESC'){
     $orderchangeAgent = 'ASC';
     if($sortOn == "merchant_name"){
       $('#merchantImageOrder').html("<img src='../img/asc.gif'>");
     } if($sortOn == "branch_address"){
       $('#branchImageOrder').html("<img src='../img/asc.gif'>");  
     } 
  
  } else {
    $orderchangeAgent = 'DESC';
      if($sortOn == "branch_address"){
       $('#merchantImageOrder').html("<img src='../img/desc.gif'>");
     } if($sortOn == "branch_address"){
       $('#branchImageOrder').html("<img src='../img/desc.gif'>");  
     }
  } 
  getTerminalList($qidLatest,$typeLatest,$tabTypeLatest,$sortOn,$sortBy,$page);
}

function ArrayObject() {
    this._array = [];

    this.add = function (elm) { this._array.push(elm); }

    this.remove = function (elm, key) {
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { var a = this._array[i]; this._array.splice(i, 1); return a; } } return null; }
        else { for (i in this._array) { if (this._array[i][key] == elm) { var a = this._array[i]; this._array.splice(i, 1); return a; } } return null; }
    }

    this.clear = function () { this._array.length = 0; }

    this.count = function () { return this._array.length; }

    this.contains = function (elm, key) { return this.get(elm, key) != null; }

    this.get = function (elm, key) {
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { return this._array[i]; } } return null; }
        else { for (i in this._array) { if (this._array[i][key] == elm) { return this._array[i]; } } return null; }
    }

    this.getAll = function (elm, key) {
        var items = [];
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { return this._array[i]; } } return null; }
        else { for (i in this._array) { if (this._array[i][key] == elm) {items.push(this._array[i]); } } return items; }
    }

    this.update = function (elm, key) {
        if (key == undefined || key == null) { for (i in this._array) { if (this._array[i] == elm) { this._array[i] = elm; } } }
        else { for (i in this._array) { if (this._array[i][key] == elm) { this._array[i] = elm; } } }
    }

    this.All = function () {
        return this._array;
    }
}

function NewObject() {
    this.answer_text = null;
    this.merchant_name = null;
    this.branch_address = null;
    this.result_id = null;
}

function isBlank(str) { if (Number(str)) return false; return (str == undefined || $.trim(str) === ""); }

/*#########################################################>End code For Drilling Data>######################################################################*/