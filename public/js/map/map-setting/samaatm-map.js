var styles = [
    [{
        url: baseUrl + "public/img/cluster/unicharm/m1.png",
        height: 35,
        width: 35,
        anchor: [9, 0],
        textColor: "#FFFFFF",
        textSize: 10
    }, {
        url: baseUrl + "public/img/cluster/unicharm/m2.png",
        height: 40,
        width: 40,
        anchor: [12, 0],
        textColor: "#FFFFFF",
        textSize: 11
    }, {
        url: baseUrl + "public/img/cluster/unicharm/m3.png",
        height: 52,
        width: 52,
        anchor: [19, 0],
        textColor: "#FFFFFF",
        textSize: 12
    }, {
        url: baseUrl + "public/img/cluster/unicharm/m4.png",
        height: 62,
        width: 62,
        anchor: [24, 0],
        textColor: "#FFFFFF",
        textSize: 13
    }]
]

var CloseButtonImg = 'close-button.png';
var BankColor   = 'white';
var markerIcons = new Array({
    'AL_BILAD': {
        image: baseUrl + "public/img/heat-map-markers/al_bilad_logo.png",
        width: 58,
        height: 66
    },
    'NATIONAL_COMMERCIAL BANK': {
        image: baseUrl + "public/img/heat-map-markers/ncb_logo.png",
        width: 36,
        height: 60
    },
    'ALINMA': {
        image: baseUrl + "public/img/heat-map-markers/alinma_logo.png",
        width: 58,
        height: 66
    },
    'ALJAZIRA': {
        image: baseUrl + "public/img/heat-map-markers/aljazeera_logo.png",
        width: 58,
        height: 66
    },
    'ALRAJHI': {
        image: baseUrl + "public/img/heat-map-markers/alrajhi_logo.png",
        width: 58,
        height: 66
    },
    'ARAB_NATIONAL_BANK': {
        image: baseUrl + "public/img/heat-map-markers/anb_logo.png",
        width: 58,
        height: 66
    },
    'FRANSI': {
        image: baseUrl + "public/img/heat-map-markers/bsf_logo.png",
        width: 58,
        height: 66
    },
    'HOLLANDI': {
        image: baseUrl + "public/img/heat-map-markers/shb_logo.png",
        width: 58,
        height: 66
    },
    'RIYAD_BANK': {
        image: baseUrl + "public/img/heat-map-markers/riyad_logo.png",
        width: 58,
        height: 66
    },
    'SABB': {
        image: baseUrl + "public/img/heat-map-markers/sabb_logo.png",
        width: 58,
        height: 66
    },
    'SAMBA': {
        image: baseUrl + "public/img/heat-map-markers/samba_logo.png",
        width: 58,
        height: 66
    },
    'SAUDI_INVESTMENT_BANK': {
        image: baseUrl + "public/img/heat-map-markers/sib_logo.png",
        width: 58,
        height: 66
    },
    'DEFAULT': {
        image: baseUrl + "public/img/logo/survey.png",
        width: 58,
        height: 66
    }
 });