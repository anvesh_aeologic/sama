
function getSelectValues($query){
    var fusionQuery = 'https://www.google.com/fusiontables/api/query?sql=SELECT+';
    var selectQuery,whereCondition = null,orderBy;
    if($query.fields){
        fusionQuery = fusionQuery+""+$query.fields+"+FROM+";
        selectQuery = "SELECT "+$query.fields+" FROM ";
    }else{
        fusionQuery = "*+FROM+";
        selectQuery = "SELECT * FROM ";
    }
    
    if($query.table){
        fusionQuery += $query.table+"+";
    }
    
    if($query.where){
        fusionQuery += "+WHERE+" +$query.where+"+";
        whereCondition = " WHERE "+$query.where+" ";
    }
    if($query.groupBy){
         fusionQuery += "GROUP+BY+"+$query.groupBy+"+";
    }
    if($query.orderBy){
         fusionQuery += "ORDER+BY+"+$query.orderBy+"+ASC+";
         orderBy = " ORDER BY "+$query.orderBy+" ASC ";
    }
    
    fusionQuery +=  "&jsonCallback=?";
    /*
    $.getJSON(fusionQuery,function(data){
        var fusionRows = data.table.rows;
        var content = "";
        content += "<option value=''>"+($query.fieldName)+"</option>"
        for(var rowCounter in fusionRows){
                content += "<option value='"+fusionRows[rowCounter][0]+"'>"+(fusionRows[rowCounter][0]).toUpperCase()+"</option>";
        }
        $("#"+$query.selectId).html(content);
    });*/
    fusionTable(selectQuery,whereCondition,orderBy,"createListData",$query.table,null);
    
}
function createListData(data){
    var fusionRows = data.rows;
    var content = "";
    content += "<option value=''>"+($query.fieldName)+"</option>"
    for(var rowCounter in fusionRows){
            content += "<option value='"+fusionRows[rowCounter][0]+"'>"+(fusionRows[rowCounter][0]).toUpperCase()+"</option>";
    }
    $("#"+$query.selectId).html(content);
}

