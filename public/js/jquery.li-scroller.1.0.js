/*!
 * liScroll 1.0
 * Examples and documentation at: 
 * http://www.gcmingati.net/wordpress/wp-content/lab/jquery/newsticker/jq-liscroll/scrollanimate.html
 * 2007-2010 Gian Carlo Mingati
 * Version: 1.0.2.1 (22-APRIL-2011)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * Requires:
 * jQuery v1.2.x or later
 * 
 */

var $stripMain = null;
jQuery.fn.liScroll = function(settings) {
		settings = jQuery.extend({
		  travelocity: 0.05
		}, settings);		
		return this.each(function(){
				var $strip = $stripMain = jQuery(this);
                var stopFlag = false;
				$strip.addClass("newsticker")
				var stripWidth = 1;
                var stripWidthArray = [];
				$strip.find("li").each(function(i){
				    stripWidth += jQuery(this, i).outerWidth(true); // thanks to Michael Haszprunar and Fabien Volpi
                    stripWidthArray.push(jQuery(this, i).outerWidth(true));
                    
				});
                //console.log(stripWidthArray);
				var $mask = $strip.wrap("<div class='mask'></div>");
                
				var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer' style='width:97%;'></div>");	
                			
				var containerWidth = $strip.parent().parent().width();	//a.k.a. 'mask' width 	
				$strip.width(stripWidth);			
				var totalTravel = stripWidth+containerWidth;
				var defTiming = totalTravel/settings.travelocity;	// thanks to Scott Waye		
				function scrollnews(spazio,tempo){
				    $strip.animate({left: '+='+ spazio}, tempo, "linear", function(){$strip.css("left","-"+stripWidth+"px"); scrollnews(totalTravel, defTiming);});
				}
                $strip.css("left", "-"+stripWidth+"px");                
				scrollnews(totalTravel, defTiming);	
                var me = this;			
				$("#mainbutton").toggle(function(){
				    $(this).attr('class','green-icon-play');
                    jQuery(me).stop();
				},
				function(){
				    $(this).attr('class','green-icon-pause');
    				var offset = jQuery(me).offset();
    				var residualSpace = offset.left + stripWidth;
    				var residualTime = residualSpace/settings.travelocity;
    				scrollnews(residualSpace, residualTime);
				});	
                
                
                $("#nextbutton").hover(function(){
                    
				        jQuery(me).stop();
                        stopFlag = false;
                    
				},
				function(){
				    stopFlag = false;
                    $("#mainbutton").attr('class','green-icon-pause');
				    var offset = jQuery(me).offset();
    				var residualSpace = offset.left + stripWidth;
    				var residualTime = residualSpace/settings.travelocity;
    				scrollnews(residualSpace, residualTime);
				});	
                
                $("#previousbutton").hover(function(){
                   
				        jQuery(me).stop();
                        stopFlag = false;
                   
				},
				function(){
				    
                    $("#mainbutton").attr('class','green-icon-pause');
    				var offset = jQuery(me).offset();
    				var residualSpace = offset.left + stripWidth;
    				var residualTime = residualSpace/settings.travelocity;
    				scrollnews(residualSpace, residualTime);
				});	
                
                $("#nextbutton").click(function(){
                    stopFlag = true;
                    var stripWidthArray = [];
                    var counter = 0;
                    $stripHtml = '';
                    $leftPosition = $("#ticker01").css('left');
                    $portion = $leftPosition.split("-"); 
                    if($portion[1]){
                        $postion1 = $portion[1].split('px');
                    }else{
                        $postion1 = $portion[0].split('px');
                    }
                    
                    $currentwidth = 0;
                   $("#ticker01").find('li').each(function(){
                        if(counter == 0){
                            $stripHtml = $(this).html();
                            $(this).remove();
                        }else{
                            stripWidthArray.push($(this).html());
                            $(this).remove();
                        }
                        counter++;
                       
                        
                        
                   })
                   
                   stripWidthArray.push($stripHtml);
                   for(var i in stripWidthArray){
                     $("#ticker01").append("<li>"+stripWidthArray[i]+"</li>");
                   }
                   $("#ticker01").css('left','-3552px');
                   $("#mainbutton").attr('class','play-button');
                   jQuery(me).stop();
				});	
                
                $("#previousbutton").click(function(){
                    stopFlag = true;
                    var stripWidthArray = [];
                    var counter = 0;
                    $stripHtml = '';
                   $("#ticker01").find('li').each(function(){
                        if($stripHtml){
                            stripWidthArray.push($stripHtml);
                        }
                        $stripHtml = $(this).html();
                        $(this).remove();
                        counter++;
                   })
                   $("#ticker01").append("<li>"+$stripHtml+"</li>");
                   for(var i in stripWidthArray){
                     $("#ticker01").append("<li>"+stripWidthArray[i]+"</li>");
                   }
                   $("#ticker01").css('left','-3552px');
                   $("#mainbutton").attr('class','play-button');
                   jQuery(me).stop();
				});		
		});	
};