var bankCode, bankSurveyDate, bankLatitude, bankLongitude;

$(document).ready(function() {
    var where = " Name = 'National Commercial Bank' AND BranchCode='" + branchCode + "' AND Type='Bank' ";
    var select = "BranchName,Location,Province,City,Image,BranchType,IsAffluent,IsPrivateBanking,IsTahweel";
    getGISDataFrmDb(where, select);
    //fusionTable('SELECT BranchName,Location,Province,City,Image,BranchType,IsAffluent,IsPrivateBanking,IsTahweel FROM ', where, ' ORDER BY BranchCode ASC', 'executeFusionDataOnProfile');
});

function getGISDataFrmDb(where, select) {
    $.ajax({
        url: 'model/tbl_branch.php',
        type: 'POST',
        datatype: 'JSON',
        data: {action: 'GISDATAINDEX', select: select, where: where},
        success: executeFusionDataOnProfile,
        error: function() {
            alert('No Data Found');
        }
    });
}

function executeFusionDataOnProfile(data) {
    data = $.parseJSON(data);
    $foundFlag = false;
    var fusionRows = [];
    if (data.rows)
        fusionRows = data.rows;
    $foundType = branchType;
    $foundImage = false;
    for (i in fusionRows) {
        if (fusionRows[i][0]) {
            $address = '';
            if (fusionRows[i][0]) {
                $address += fusionRows[i][0];
            }
            if (fusionRows[i][2]) {
                if ($address) {
                    $address += ", ";
                }
                $address += fusionRows[i][2];
            }
            if (fusionRows[i][3]) {
                if ($address) {
                    $address += ", ";
                }
                $address += fusionRows[i][2];
            }
            //$("#branchName").html(branchProfileDisplayName(fusionRows[i][0], fusionRows[i][2], fusionRows[i][3]));
            var latLng = (fusionRows[i][1]).split(',');
            bankLatitude = latLng[0];
            bankLongitude = latLng[1];
            $branchType = [];

            if (fusionRows[i][5]) {
                if (fusionRows[i][5].toUpperCase() == 'GENTS')
                    $branchType.push('Men');
                else if (fusionRows[i][5].toUpperCase() == 'LADIES')
                    $branchType.push('Ladies');
                else if (fusionRows[i][5].toUpperCase() == 'GENTS AND LADIES')
                    $branchType.push('Men and Ladies');
            }
            if (fusionRows[i][6] == 'Y')
                $branchType.push('Affluent');
            if (fusionRows[i][7] == 'Y')
                $branchType.push('Private Banking');
            if (fusionRows[i][8] == 'Y')
                $branchType.push('Tahweel');
            createMarker(new google.maps.LatLng(bankLatitude, bankLongitude), fusionRows[i][4], $branchType);
            $foundFlag = true;
            $foundImage = fusionRows[i][4];
            $foundType = $branchType;
        }
    }
    checkBankData($foundImage, $foundType, $foundFlag);
}

function checkBankData(image, currentBrType, foundFlag) {
    $.post('databaseRequest.php', {
        'type': 'branchInfo',
        'branchCode': branchCode,
        'branchType': branchType
    }, function(result) {
        result = JSON.parse(result);
        if (result.found) {
            if ($foundFlag) {
                bankLatitude = result.latitude;
                bankLongitude = result.longitude;
                $foundFlag = true;
                //createMarker(new google.maps.LatLng(bankLatitude,bankLongitude),image,currentBrType);
            } else {
                createMarker(new google.maps.LatLng(24.570855, 46.911621), image, currentBrType);
            }
            getHealthReport();
        } else {
            createMarker(new google.maps.LatLng(24.570855, 46.911621), image, currentBrType);
            $("#scorecard_div").html('<div align="center" style="font-size:30px;font-weight:bold;color:red;height:133px;margin-top:100px;">nothing found!</div>');
            $.post('nothingFound.php', {
                'type': 'data'
            }, function(result) {
                $("#healthReport").html(result);
                gaugeValue = 0;
                drawGauge();
                $("#galleryImages").html("<div align='center'><span style='width:70%;text-align:left' class='green'><b>Nothing found</b></span></div>");
            });
        }
    });
}

function getHealthReport() {
    $.post('databaseRequest.php', {
        'type': 'data',
        'branchCode': branchCode,
        'branchType': branchType

    }, function(result) {
        result = JSON.parse(result);
        $("#healthReport").html(result.content);
        $("#scorecard_div").html(result.scorecard_div);
        if (result.code) {
            bankCode = result.code;
            bankSurveyDate = result.date;
            gaugeValue = result.pending;

            drawGauge();
            initializePopup();
            if (result.hasImage) {
                getGalleryImages();
            } else {
                $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');
            }
        } else {
            $("#galleryImages").html('<div class="green" align="center"><b>Nothing Found</b></div>');

        }
    });
}
function getGalleryImages() {
    $.post('databaseRequest.php', {
        'type': 'images',
        'bankCode': bankCode,
        'branchType': branchType,
        'bankSurveyDate': bankSurveyDate
    }, function(result) {
        $("#galleryImages").html(result);
        if ($('ul.gallery').attr('tooltip') == 'true') {
            $('ul.gallery li').hover(function() {
                $('img', this).fadeToggle(1000);
                $(this).find('.gallery-controls').remove();
                $(this).append('<div class="well gallery-controls">' +
                        '<p><a href="#" class="gallery-edit btn"><i class="icon-edit"></i></a> <a href="#" class="gallery-delete btn"><i class="icon-eye-open"></i></a></p>' +
                        '</div>');
                $(this).find('.gallery-controls').stop().animate({
                    'margin-top': '-1'
                }, 400, 'easeInQuint');
            }, function() {
                $('img', this).fadeToggle(1000);
                $(this).find('.gallery-controls').stop().animate({
                    'margin-top': '-30'
                }, 200, 'easeInQuint', function() {
                    $(this).remove();
                });
            });
        }

        //gallery delete
        $('.thumbnails').on('click', '.gallery-delete', function(e) {
            e.preventDefault();
        });
        //gallery edit
        $('.thumbnails').on('click', '.gallery-edit', function(e) {
            e.preventDefault();
        });

        if (isTouchDevice) {
            $('.openSample a').colorbox({
                inline: true,
                rel: 'openSample a',
                transition: "elastic",
                maxWidth: "95%",
                maxHeight: "95%"
            }, function() {//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');
            });
        } else {
            $('.thumbnails div.img_popup').colorbox({
                inline: true,
                rel: 'thumbnails div.img_popup',
                transition: "elastic",
                title: false,
                maxWidth: "100%",
                maxHeight: "100%"
            }, function() {//var divId = $(this).attr('href'); $(divId).find('img').height('500px');$(divId).find('img').width('480px');     
                // initializeScroll();
                //alert($(this).attr('departmentName'));
                //alert('here me.!');
                setTimeout(function() {
                    DepartmentNameForTransferOwnership($(this).attr('departmentName'));
                }, 500);
            });
        }
        clickTicketIssue();
       
    });
}
var prev_popup = null;

$(document).ready(function() {
    $('.thumbnails div.img_popup').live('click', function() {
        var departName = $(this).attr('departmentName');
        loadImageComments(this);
        setTimeout(function() {
            showResolveButtonOnImagePopup(departName);
            DepartmentNameForTransferOwnership(departName);
        }, 500);
    });
    $('.dashboard-list a.img_popup').live('click', function() {
        var departName = $(this).attr('departmentName');
        setTimeout(function() {
            showResolveButtonOnImagePopup(departName);
            DepartmentNameForTransferOwnership(departName);
        }, 500);
        var href = $(this).attr('href');
        var has_photoproof = $(href).find('.issue_img').attr('photoproof');
        if (has_photoproof != 0) {
            $('#galleryTab').trigger('click');
            loadImageComments(this);
        } else {
            $(href).find('.issue_img').attr('src', '../img/img_issue_dummy.png');
            loadImageComments(this);
        }
    });
});
function loadImageComments(thisone) {

    if (prev_popup) {
        prev_popup.html('');
    }
    prev_popup = $('#comments_' + $(thisone).attr('data-content'));
    var imageId = $(thisone).attr('data-content');
    var departName = $(thisone).attr('departmentName');
    setTimeout(function() {
        showResolveButtonOnImagePopup(departName);
    }, 500);
    var comments = loadComments($(thisone).attr('data-content'));
    $('#comments_' + $(thisone).attr('data-content')).html(comments);

    /*********************************Implemented By:- anugrah nath 22-july-2013****************************************************************
     *  here add the button for add-more comments
     */

    size_li = $('#comments_' + imageId).find('.dashboard-list li').size();
    var x = 5;
    showliSize = size_li - 1;

    if (size_li == 0) {
        $("#ControlButton_" + imageId).find("div#comment_count_div_").hide();
    }
    if (size_li >= 7) {
        $('#comments_' + imageId).find('.dashboard-list li').css('display', 'none')
        $('.parent_comment_contnr').find('.dashboard-list li:lt(' + x + ')').css('display', 'inline');
        $('.parent_comment_contnr').find('.dashboard-list li:last').show();
    }
    $("#loadCommentsButtonOnDashbord").live('click', function() {
        $(".viewport").scrollTop($(".viewport")[0].scrollHeight);
        $('#comment_loader_').show();
        setTimeout(function() {
            x = (x + 5 <= showliSize) ? x + 5 : showliSize;
            $("#loade_comments_span_").text(x);
            $('.parent_comment_contnr').find('.dashboard-list li:lt(' + x + ')').css('display', 'inline');
            $('#comment_loader_').hide();
            if (showliSize == x) {
                $('.parent_comment_contnr').find('.dashboard-list li:last').hide();
            }
        }, 2000);


    });
    /*********************************************************************************************************************************************************/


}

function initializePopup() {
    $('.dashboard-list a.img_popup').colorbox({
        inline: true,
        rel: 'dashboard-list a.img_popup',
        transition: "elastic",
        title: false,
//        maxWidth: "100%",
//        maxHeight: "100%"
        width: "800px",
        height: "680px"
    }, function() {
        //initializeScroll();
       // alert('here You.!');
        setTimeout(function() {
            DepartmentNameForTransferOwnership($(this).attr('departmentName'));
        }, 500);
    });
}

function showResolveButtonOnImagePopup(departmentName) {
    $.post('galleryActionDb.php', {
        'type': 'sendDepartment',
        'userName123': userName123
    }, function(responce) {
        var result = $.parseJSON(responce);
        var dpartName = result.sendDepartmentName;
        if (dpartName == null) {
            $("#resolve").show();
        }
        else if (departmentName == dpartName) {
            $("#resolve").show();
        } else {
            $("#resolve").hide();
            $('.tab_commenting_section li:last').hide();
        }

    });
}