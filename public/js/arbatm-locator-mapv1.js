function MapCreator(options) {
    this.cluster = null;
    this.markerArray = [];
    this.markerSets = [];
    this.defaultMarkerArray = [];
    this.map = null;
    this.clusterOptions = {styles: styles[0]};
    this.heatmap = null;
    this.heatmapData = [];
    this.mapContainer = options.mapContainer;
    this.autoCompleteInput = options.autoCompleteInput;
    this.geoCoder = new google.maps.Geocoder();
    this.cityCircle = null;
    this.oms = null;
    /*this.infowindow = new google.maps.InfoWindow({
     content: ""
     });*/
    this.infowindow = null;
    this.centerMarker = null;

    this.draggedPosition = null;
    this.draggable = true;
    this.loadingDefault = false;
    //this.selectedSearch = "radius";
    this.selectedSearch = "region";
    this.selectedLocation = new google.maps.LatLng(22.268763552489748, 47.87841796875);
    this.population = {};

    this.createMap = function () {

        var me = this;

        var mapOptions = {
            center: new google.maps.LatLng(24.716683,42.92785),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.TOP_LEFT
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            }
        };
        var filterHtml = document.getElementById('bank_filters');

        this.map = new google.maps.Map(document.getElementById(this.mapContainer), mapOptions);

        mapchange = this.map;
        changeIsrailLayer(this.map.getZoom(), this.map);
        this.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(filterHtml);

        this.oms = new OverlappingMarkerSpiderfier(this.map); //Spiderfied it here

        this.infowindow = new google.maps.InfoWindow();

        me.oms.addListener('click', function(marker, event) {

            var markerLocation = marker['atmDetail']['location'].split(',');

            this.map.setCenter(new google.maps.LatLng(markerLocation[0], markerLocation[1]));
            this.map.setZoom(10);
            marker.setAnimation(null);
            marker.setAnimation(google.maps.Animation.BOUNCE);
            
            atmIssueImage = '<img style="width: 20px; height: 20px;" src="https://4c360.com/4c-dashboard/public/img/map-markers/bank-logo/preview.png" class="cloud9-item", alt="Item #1"/>';

            $("#showcase-images").hide();
            $("#showcase-loader").show();
            $("#nav-atm-detail").trigger('click');
            loader = baseUrl+'/public/img/ajax-loaders/bsfmap-loader1.gif';
            $("#atmImage").html('<img style=" margin-left: 36%; margin-top: 7%; width: 20%; height: 50%;" src="'+loader+'">');
            var image = (marker["atmDetail"]["image"] ? marker["atmDetail"]["image"] : baseUrl+'/public/img/map-markers/bank-logo/preview.png');
            atmImage = '<img src="'+image+'" class="sidebar-refer-image"/>';
            var atmProfile= (marker["atmDetail"]['is_visited'] =='1') ? 'arbatmgis' : 'arbatm';

            emailAtmImage = '<img style="width: 300px; height: 400px;" src="'+image+'" class="sidebar-refer-image"/>';

            $("#emailDiv").attr('content',"<div style='float: left; width: 45%;'>"+emailAtmImage+"</div><div style='font-size: 16px; height: 400px; background-color: #F1F1F1; width: 50%; float: left;'><div><table style='margin-top: 5%; margin-left: 5%; '><tr><td colspan='2' style='padding: 10px; width: 117%; background-color: #A5A5A5; text-align: center;'>ATM Detail</td></tr><td><b>Terminal ID:</b></td><td><span>"+marker['atmDetail']['terminal_id']+"</span></td></tr><tr><td><b>Brand:</b></td><td> <span>"+marker['atmDetail']['terminal_brand']+"</span></td></tr><tr><td><b>Branch Type:</b></td> <td><span>"+marker['atmDetail']['branchType']+"</span></td></tr><tr><td><b>Branch Name:</b></td><td> <span>"+marker['atmDetail']['branch_name']+"</span></td></tr><tr><td><b>Group:</b></td><td> <span>"+marker['atmDetail']['group_name']+"</span></td></tr><tr><td><b>Address:</b></td><td> <span>"+marker['atmDetail']['address']+"</span></td></tr><tr><td><b>City:</b></td><td> <span>"+marker['atmDetail']['city']+"</span></td></tr></div></div>");
            
            setTimeout(function() {
                $("#atmImage").html(atmImage);
            }, 1000);

            $("#atmDetail").html("<p>Terminal ID:<span style='font-size: 20px;'>"+marker['atmDetail']['terminal_id']+"</span></p><p>Brand: <span>"+marker['atmDetail']['terminal_brand']+"</span></p><p>Address: <span>"+marker['atmDetail']['address']+"</span></p><p>City: <span>"+marker['atmDetail']['city']+"</span></p>");
            $("#profilePageNavigator").attr("href",baseUrl+"/"+atmProfile+"/index.php?profile=1&code="+marker['atmDetail']['atm_code']+"&group_name="+marker['atmDetail']['group_name']+"&type=");
            getObservationImage(marker["atmDetail"]);

            showCurrentLocationDetail(marker['atmDetail']);

        });
    };

    this.autoComplete = function(){
        map = this.map;
        var autoCompleterOptions = {
            types: ['(regions)'],
            componentRestrictions: {country: 'sa'}
        };
        var autocomplete = new google.maps.places.Autocomplete(this.autoCompleteInput, autoCompleterOptions);
        autocomplete.bindTo('bounds', this.map);
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            map: this.map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {

        });
    };
    this.createCluster = function(){
        console.log('Cluster Option');
        console.log(this.clusterOptions);
        if (this.cluster == undefined) {
            this.cluster = new MarkerClusterer(this.map, this.markerArray,this.clusterOptions);
        } else {
            this.cluster.addMarkers(this.markerArray, false);
        }
    };

    this.clearCluster = function(){
        if((this.cluster != undefined)&&(this.cluster)) {
            this.cluster.clearMarkers();
            this.cluster = null;
        }
    };


    this.setMapCenter = function(lat, long,zoom){
        if(this.map !==undefined)
            this.map.setCenter(new google.maps.LatLng(lat, long));
            this.map.setZoom(2);
    };


    this.geocodeAddress = function(address, callbackFunction){
        var me = this;
        map = this.map;
        geococder = this.geoCoder;
        markerArray = this.markerArray;

        var data = (this.draggedPosition) ? {'location' : this.draggedPosition} : {'address': address+"saudi arabia"};

        geococder.geocode(data, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                $("#map-data-go").find("i").attr('class', 'fa fa-search');

                var editedAddress = "";
                if(me.draggedPosition){
                    var draggedAddress = "";
                    $.each((results[0].formatted_address).split(","), function(index, value){

                        if(value.indexOf('Unnamed') < 0){

                            if(draggedAddress)
                                draggedAddress += ",";
                            draggedAddress += value;
                        }
                    });
                    if(draggedAddress)
                        address = draggedAddress;


                    editedAddress = "<i class='fa fa-edit fa-1x red blink_me'></i>";
                    $("#address-auto-complete").val(address);
                }

                map.setCenter(results[0].geometry.location);
                var marker = null;

                if( me.centerMarker)
                    me.centerMarker.setMap(null);
                var pinIcon = new google.maps.MarkerImage(
                    baseUrl + "public/img/Azure.png",
                    null, /* size is determined at runtime */
                    null, /* origin is 0,0 */
                    null, /* anchor is bottom center of the scaled image */
                    new google.maps.Size(33, 45)
                );

                me.centerMarker = new google.maps.Marker({
                    map: me.draggable ? map : null,
                    position: results[0].geometry.location,
                    icon :pinIcon,
                    draggable : me.draggable
                });


                google.maps.event.addListener(me.centerMarker, 'dragstart', function(evt){
                    me.draggedPosition = null;
                });

                google.maps.event.addListener(me.centerMarker, 'dragend', function(evt){
                    me.draggedPosition = this.getPosition();
                    $("#map-data-go").trigger('click');
                });

                var content = " ";

                me.removeMarkers();
                me.clearCircle();
                me.clearCluster();

                if(me.draggable){
                    content += "<html><div>";
                    content += "<h3 style='color:#4183C4;'>Center of search </h3>";
                    content += "<h4 style='color:#4183C4;'>"+(address)+" &nbsp;"+editedAddress+"</h4>";
                    content += "</div></html>";

                    me.infowindow.setContent(content);
                    me.centerMarker.infoContent = content;
                }


                google.maps.event.addListener(me.centerMarker, 'click', function() {
                    me.infowindow.setContent(content);

                    me.infowindow.open(me.draggable ? me.map : null, this);
                });

                if(me.draggable) {
                    me.infowindow.open(me.map, me.centerMarker);
                }
                me.oms.addMarker(me.centerMarker);
                markerArray.push(me.centerMarker);

                if(typeof callbackFunction != 'undefined'){
                    callbackFunction(results[0].geometry.location);
                }

                //
                //if(!me.draggable){
                //    me.centerMarker.setMap(null);
                //}
                //

            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    };

    this.formatAddress = function(){

    };

    this.createCircle = function(center, range, callbackFunction){

        range = range ? range : 2;

        this.cityCircle = new google.maps.Circle({
            strokeColor: '#B4D9EF',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#B4D9EF',
            fillOpacity: 0.35,
            map: map,
            center: center,
            radius: (range * 1000)
        });

        map.fitBounds(this.cityCircle.getBounds());

        if( this.centerMarker) {
            this.centerMarker.bindTo("position", this.cityCircle, "center");
        }

        if(typeof callbackFunction != 'undefined'){
            callbackFunction();
        }
    };

    this.clearCircle = function(){
        if(this.cityCircle){
            this.cityCircle.setMap(null);
        }
    };

    this.createMapMarker = function($dataRow){
        var detail= {};
        detail = $dataRow;
        var me = this;
        var latLng = new google.maps.LatLng($dataRow['latitude'], $dataRow['longitude']);
        var marker = new google.maps.Marker({
            position : latLng,
            map : me.map,
            atmDetail:detail,
            icon : this.getMarkerIcon($dataRow)
        });

        marker.infoContent = this.infoWindowContent($dataRow); /** -- Setting info window content ---  **/
        me.oms.addMarker(marker);

        this.markerArray.push(marker);

        return marker;


    };

    this.getMarkerIcon =function($dataRow){
        var locationCategory = ($dataRow['type']) ? ( ($dataRow['type']).toUpperCase().replace(/ /g,'_')) : "DEFAULT";
        if($dataRow['atm_code'] == $('#atm_code').val()){

        }else{
            if($dataRow['type'] == 'Bank' || $dataRow['type'] == "Outlet"){
                locationCategory =  ($dataRow['name']) ? ( ($dataRow['name']).toUpperCase().replace(/ /g,'_') ) : "DEFAULT";
                locationCategory =  locationCategory.replace("-", "");
            }
        }

        if(locatorMarkerIcons[0][locationCategory])
            return new google.maps.MarkerImage(locatorMarkerIcons[0][locationCategory].image, null, null, null, new google.maps.Size(locatorMarkerIcons[0][locationCategory].width, locatorMarkerIcons[0][locationCategory].height));
        else
            return new google.maps.MarkerImage(locatorMarkerIcons[0]['DEFAULT'].image, null, null, null, new google.maps.Size(locatorMarkerIcons[0]['DEFAULT'].width, locatorMarkerIcons[0]['DEFAULT'].height));
    };

    this.infoWindowContent = function(data){
        var otherDataSet = "";

        for(var index in data){
            otherDataSet += "data-" + index + "='"+data[index]+"' ";
        }

        var bankName   = (data['name']) ? data['name'] : 'NA';
        var ATMCode   = (data['atm_code']) ? data['atm_code'] : 'NA';
        var ATMGroupName   = (data['group_name']) ? data['group_name'] : '';
        var terminal_id   = (data['terminal_id']) ? data['terminal_id'] : '';
        var terminal_brand   = (data['terminal_brand']) ? data['terminal_brand'] : '';
        var atm_address   = (data['address']) ? data['address'] : 'Not Available';
        var atm_city   = (data['city']) ? data['city'] : '';
        var atmProfile= (data['is_visited'] =='1') ? 'arbatmgis' : 'arbatm';
        if(bankLogoList[data['name']]){
            var imagePath  = (data['image']) ? data['image'] : baseUrl+"/public/img/map-markers/bank-logo/"+bankLogoList[data['name']]['bank_logo'];
        }else{
            var imagePath  = baseUrl+"/public/img/preview.png";
        }
        var branchName = (data['branch_name']) ? data['branch_name'] : '';
        var distance   = (data['distance']) ? Number(data['distance']).toFixed(2)+" KMs" : "";
        var routeBtn   = (data['distance']) ? '<a class="modal-anchor" data-toggle="modal" style="text-decoration:none;cursor: pointer;" data-target=".route-map-modal" '+otherDataSet+' onclick="createRouteMap(this)" ><i class="fa fa-map-marker red">&nbsp;</i>View Route</a>' : "";
        var content    = " ";

        content += "<html> <body><div style='width: 520px;'>";
        content +=      "<table style='width: 100%' cellspacing='5' cellpadding='5'>";
        if(bankName == 'Al-Rajhi Bank' && ATMCode !=='NA' ) {
            content +=               "<tr>";
            content +=               "<td colspan='2'>";
            content +=                  "<h2 style='text-align: center;color:#4183C4;'><a target='_blank' href='"+baseUrl+atmProfile+'/'+'index.php?profile=1&code='+ATMCode+'&group_name='+ATMGroupName+'&type='+"'>"+bankName+"("+ATMCode+")</a></h2>";
            content +=               "</td>";
            content +=               "</tr>";
        }else {
            content +=               "<tr>";
            content +=               "<td colspan='2'>";
            content +=                  "<h2 style='text-align: center;color:#4183C4;'>"+bankName+"</h2>";
            content +=               "</td>";
            content +=               "</tr>";
        }
        content +=          "<tr>";
        content +=              "<td style='width:45%;text-align: center; vertical-align: top;' >";
        if(bankName == 'Al-Rajhi Bank' && ATMCode !=='NA' ) {
            content += "<a style='height: 300px;text-align:center; display:table-cell; vertical-align:top' target='_blank' href='"+baseUrl+atmProfile+'/'+'index.php?profile=1&code='+ATMCode+'&group_name='+ATMGroupName+'&type='+"'>"
                +"<img src='" + imagePath + "' style='width:100%;position: relative;padding: 0;'></a>";
        }else{
            content += "<div style='height: 300px;text-align:center;display:table-cell; vertical-align:top'><img src='" + imagePath + "' style='width:100%; height: auto;position: relative;padding: 0;'></div>";
        }
        content +=               "</td>";
        content +=              "<td valign='top'>";
        content +=               "<table class='table table-responsive'>";
        if(bankName == 'Al-Rajhi Bank' && ATMCode !=='NA' ){
            content +=               "<tr>";
            content +=               "<td nowrap> ATM Type";
            content +=               "</td>";
            content +=               "<td nowrap colspan='2'>";
            content +=                  "<h4 style='color:#4183C4;'>"+ATMGroupName+"</h4>";
            content +=               "</td>";
            content +=               "</tr>";
            content +=               "<tr>";
            content +=               "<td nowrap> ATM Code";
            content +=               "</td>";
            content +=               "<td nowrap>";
            content +=                  "<h4 style='color:#4183C4;'><a target='_blank' href='"+baseUrl+atmProfile+'/'+'index.php?profile=1&code='+ATMCode+'&group_name='+ATMGroupName+'&type='+"'>"+ATMCode+" </a> </h4>";
            content +=               "</td>";
            content +=               "<td>" +
                " <a  target='_blank' href='"+baseUrl+atmProfile+'/'+'index.php?profile=1&code='+ATMCode+'&group_name='+ATMGroupName+'&type='+"'>"+"  <strong class='blink'> View Profile</strong></a> ";
            content +=               "</td>";
            content +=               "</tr>";
        }
        content +=               "<tr>";
        content +=               "<td nowrap> Terminal ID";
        content +=               "</td>";
        content +=               "<td colspan='2'>";
        content +=                  "<h4 style='color:#4183C4;'>"+terminal_id+"</h4>";
        content +=               "</td>";
        content +=               "</tr>";
        content +=               "<tr>";
        content +=               "<td> Brand";
        content +=               "</td>";
        content +=               "<td colspan='2'>";
        content +=                  "<h4 style='color:#4183C4;'>"+terminal_brand+"</h4>";
        content +=               "</td>";
        content +=               "</tr>";
        content +=               "<tr>";
        content +=               "<td nowrap> Address";
        content +=               "</td>";
        content +=               "<td colspan='2'>";
        content +=                  "<h4 style='color:#4183C4;'>"+atm_address+"</h4>";
        content +=               "</td>";
        content +=               "</tr>";
        content +=               "<tr>";
        content +=               "<td> City";
        content +=               "</td>";
        content +=               "<td colspan='2'>";
        content +=                  "<h4 style='color:#4183C4;'>"+atm_city+"</h4>";
        content +=               "</td>";
        content +=               "</tr>";
        if(distance !=""){
            content +=               "<tr>";
            content +=               "<td nowrap> Distance";
            content +=               "</td>";
            content +=               "<td>";
            content +=                  "<h4 style='color:#4183C4;'>"+distance+" </h4>";
            content +=               "</td>";
            content +=               "<td nowrap>";
            content +=                  "<h5 style='color:#4183C4; text-align: center !important;'>"+routeBtn+" </h5>";
            content +=               "</td>";
            content +=               "</tr>";

        }
        content +=              "</table> ";
        content +=              "</td>";
        content +=          "</tr>";
        content +=       "</table> ";
        content += "</div></body></html>";
        return content;
    };

    this.removeMarkers = function(){

        if(this.markerArray.length){
            for (var i = 0; i < this.markerArray.length; i++) {
                this.markerArray[i].setMap(null);
            }
            this.markerArray = [];
        }
    };

    this.loadDefaultMarkers = function(){

        if(this.defaultMarkerArray && this.defaultMarkerArray['dataRowset']){
            this.loadingDefault = true;
            var me = this;
            me.markerSets = [];
            me.removeMarkers();
            $.each(this.defaultMarkerArray.dataRowset, function(index, value){
                var marker = me.createMapMarker(value);
                me.markerSets.push(marker);
            });
            this.createCluster();

            $("#record_count").html("Total: <strong id='total-count'> "+(this.defaultMarkerArray['dataRowset'].length)+"</strong> ATMs located in all region");
            // $('.conter-info').html(createCounterList(this.defaultMarkerArray['counter']));
        }

    };

    this.deactivateHeatMap = function() {
        if (this.heatmap)
            this.heatmap.setMap(null);

        $(".counterClass").each(function() {
            $(this).html("&nbsp(0)");
        })
    };
    this.activateHeatMap = function($data){
        var radiusWeight = 0;
        me = this;
        me.population = {};

        this.deactivateHeatMap();

        $.each($data, function(index, value){
            //console.log(me.getHeatMapWeight(value['population']));
            var point = {
                location : new google.maps.LatLng(value['latitude'], value['longitude']),
                weight : (Number(value['population'])) ? me.getHeatMapWeight(value['population']) : 100
                //weight : Number(me.getTotalHeatMapWeight(value))
            };
            me.heatmapData.push(point);
            if (point.weight > 0) {
                radiusWeight = 70
            }
        });

        this.fillPopulation();

        this.heatmap = new google.maps.visualization.HeatmapLayer({
            data: me.heatmapData,
            radius: radiusWeight
        });
        this.heatmap.setMap(this.map);

    };

    this.getTotalHeatMapWeight = function(row) {
        var totalPopulation = 0;
	    var ignoreFieldList = ['region', 'city', 'latitude', 'longitude'];

        for(var field in row) {
           if ( ignoreFieldList.indexOf(field) == -1 ) {
                totalPopulation += Number(row[field]);

                if (typeof me.population[field] == 'undefined')
                        me.population[field] = 0;

                me.population[field] += Number(row[field]);
            }
        }

        return totalPopulation;
    };
    this.fillPopulation = function() {
        var parentCounter = {};

        for(var field in this.population) {
            $("#" + field + "_count").html("&nbsp(" + this.population[field] + ")");

            if ($("#" + field + "_count").length) {
                parentClassName = $("#" + field + "_count").attr('class').replace("counterClass ", "");

                if (typeof parentCounter[parentClassName] == 'undefined')
                    parentCounter[parentClassName] = 0;

                parentCounter[parentClassName] += Number(this.population[field]);
            }
        }

        for(var field in parentCounter){
            $("#" + field + "_count").html("&nbsp(" + parentCounter[field] + ")");
        }
    };
    this.getHeatMapWeight = function(population){
        var populationDigitCount = Number((population).trim().length);
        var stanadardPopulation = 1;
        for(var i = 1; i < populationDigitCount; i++){
            stanadardPopulation = (stanadardPopulation * 10);
        }
        return Number((Number(population) / stanadardPopulation).toFixed(2));
    };

    this.clearHeatMap = function(){
        this.heatmap.setMap(null);
    }


}
var mapObject;
$(document).ready(function(){
    var options = {
        mapContainer : "map-container",
        autoCompleteInput: document.getElementById("address-auto-complete")
    };
    mapObject = new MapCreator(options);
    mapObject.createMap();

    mapObject.autoComplete();

    // filters = {};
    // filters['database'] = 'arbatmgis_db';
    //$requestObject.initialize({'requestFor' : 'population-data','filter-data' : filters}, "fillHeatMap", null);

    //$requestObject.initialize({'requestFor' : 'population-data'}, "fillHeatMap", null);


    $('.modal-anchor').on('show.bs.modal', function (e) {
        google.maps.event.trigger(MapInstance,'resize')
    });
});

var fillHeatMap = function(result){
    result = JSON.parse(result);
    if(result){
        mapObject.activateHeatMap(result['populationData']);
    }

};

function fillCityOptions($data) {
    $("#city_loader").hide();
    $("#city_branch").html($data);
}

function getCityList($dataFor) {
    $("#city_loader").show();
    $requestData = {
        "requestFor": "city-data",
        "map-filters": {
            "region": $("#province_" + $dataFor).val(),
            "requestType": $dataFor
        }
    };
    $requestObject.initialize($requestData, "fillCityOptions", null);

}
var filterElements = ["select", "input"];
/**
 * Reads all filters
 * @param $parentFilterElement
 * @returns {{data array of filters}}
 */
var getFilters = function($parentFilterElement){
    var filters = {};
    var pointOfInterest = [];
    var bankName = [];
    for(var i in filterElements ){
        $($parentFilterElement).find(filterElements[i]).each(function(){
            filters[$(this).attr('name')] = $(this).attr('value');

        });

    }

    for(var i in filterElements ){
        $("#bank_filters").find(filterElements[i]).each(function(){

            if($(this).attr('checked') == "checked" && $(this).attr('value')){
                if($(this).attr('class') == "pointOfInterestInput"){
                    pointOfInterest.push($(this).attr('value'));
                }else if($(this).attr('class') == "bankInput" ){
                    bankName.push($(this).attr('value'));
                }
            }


        });

    }
    filters['point_of_interest'] = pointOfInterest;
    filters['bank_name'] = bankName;

    return filters;
};
/**
 *  Resets all filters
 * @param $parentFilterElement
 */
var resetMapFlag = false;
var resetMapData = function($parentFilterElement, $requestFor){
    resetMapFlag = true;
    for(var i in filterElements ){
        $($parentFilterElement).find(filterElements[i]).each(function(){
            if(filterElements[i] == "select"){
                //$(this).attr('value', 0);
                $(this).prop('selectedIndex', 0);
                //$(this).attr('value', '0');
                $(this).trigger('onchange');
            }
            if(filterElements[i] == "input"){

                if($(this).attr('name') != "range" && $(this).attr('name') != "atm_code_range"){

                    $(this).attr('value', '');
                }
            }

        });
    }
    $('.sidebar-census-select').val('').trigger('change');
    $("#city_branch").removeAttr("disabled");

    /** -- removing previous data from map --**/
    $("#record_count").html(0);
    //toggleAllCheckBox(null, "#bank_filters", false);
    mapObject.removeMarkers();
    mapObject.clearCircle();
    mapObject.clearCluster();
    mapObject.map.setCenter(new google.maps.LatLng(23.268763552489748, 49.87841796875));
    mapObject.map.setZoom(6);
    mapObject.loadDefaultMarkers();
    $("#nav-network").trigger('click');
    getMapData('#map-filters-div', 'map-data-go', false);

    /**           ------             **/
};

this.setMapCenter = function(lat, long){
    if(this.map !==undefined) {
        this.map.setCenter(new google.maps.LatLng(lat, long));
        this.map.setZoom(8);
    }
};

/**
 * Filters validator
 * @param filters
 * @returns {boolean}
 */
var validateFilters = function(filters){
    var error = false;
    //** -- data validation for is search is Region wise -- **/
    if(mapObject.selectedSearch == 'region'){
        if((filters.province).length == 0){
            //alert("Please enter Province");
            //error = true;
        }/*else if((filters.city).length == 0){
         alert("Please enter City");
         error = true;
         }*/else if( (filters.point_of_interest).length == 0 && (filters.bank_name).length == 0){
            // alert("Please select any bank");
            // error = true;
        }

    }
    //** -- data validation for is search is radius wise -- **/
    if(mapObject.selectedSearch == "radius"){
        if((filters.address).length == 0){
            alert("Please enter Address");
            error = true;
        }else if( (filters.point_of_interest).length == 0 && (filters.bank_name).length == 0){
            alert("Please select any bank");
            error = true;
        }
    }
    //** -- data validation for is search is ATM code wise -- **/
    if(mapObject.selectedSearch == "atm_code"){
        if((filters.atm_code).length == 0){
            alert("Please enter ATM Code");
            error = true;
        }
    }


    return error;
};

var showRangeSlider = function(slidervalue){
    if(slidervalue){
        $("#range-slider-div").show();
    }else{
        $("#range-slider-div").hide();
    }
};

/**
 * gets map data rowset, as per filters
 * @param $parentFilterElement
 * @param $requestFor
 */

var getMapData = function($parentFilterElement, $requestFor, $resetFlag){

    showNetworkTab();
    var filters = getFilters($parentFilterElement);
    /** -- removing previous data from map --**/
    //mapObject.removeMarkers();
    mapObject.clearCircle();
    //mapObject.clearCluster();
    //$("#record_count").html(0);
    mapObject.draggable = true;
    /**           ------             **/
    if (!validateFilters(filters)) {
        //var address = filters['address']+","+filters['city']+","+filters['province'];

        var address = filters['address'];
        if(mapObject.selectedSearch == "radius" ){
            mapObject.geocodeAddress(address, function(position){  /**  To get Geocode from address **/
            var location = position.lat() +","+position.lng(); /** Address Geocode --- **/
            mapObject.draggedPosition = null;
                $("#"+$requestFor).find("i").attr('class', 'fa fa-spinner fa-spin');
                mapObject.createCircle(position, filters['range'], function(){
                    filters['location'] = location;
                    filters['database'] = 'arbatmgis_db';
                    filters['search-type'] = 'radius';
                    var requestData = {
                        'requestFor'  : 'locator-map-data',
                        'filter-data' : filters
                    };
                    mapDataRequest(requestData);
                });
            });
        } else if( mapObject.selectedSearch == "atm_code"){
            mapObject.draggable = false;
            mapObject.geocodeAddress(address, function(position){  /**  To get Geocode from address **/
            var location = position.lat() +","+position.lng(); /** Address Geocode --- **/
            mapObject.draggedPosition = null;
                $("#"+$requestFor).find("i").attr('class', 'fa fa-spinner fa-spin');

                //mapObject.createCircle(position, filters['range'], function(){
                filters['location'] = location;
                filters['search-type'] = 'atm_code';
                filters['database'] = 'arbatmgis_db';
                var requestData = {
                    'requestFor'  : 'locator-map-data',
                    'filter-data' : filters
                };
                mapDataRequest(requestData);
                //});
            });
        }else{
            $("#"+$requestFor).find("i").attr('class', 'fa fa-spinner fa-spin');
            filters['search-type'] = 'region';
            filters['database'] = 'arbatmgis_db';
            var requestData = {
                'requestFor'  : 'locator-map-data',
                'filter-data' : filters
            };
            mapDataRequest(requestData);
        }

    }

    if (!$resetFlag) {
        filters['field-selected'] = getSectionData();
        $requestObject.initialize({'requestFor': 'population-data', 'filter-data': filters}, "fillHeatMap", null);
    } else {
        mapObject.deactivateHeatMap();
    }

};

var mapDataRequest = function(requestData){
    //if($requestFor == "map-data-go" || $requestFor == "map-data-reset")

    if ( $('.sidebar-census').css('display') !== 'block' ) {
        $requestObject.initialize(requestData, 'mapRequestCallBack', null);
    } else {
        mapRequestCallBack(null);
    }
};

String.prototype.truncate = String.prototype.truncate ||
    function(n){
        return (this.length > n) ? this.substr(0,n-1)+'&hellip;' : this;
    };

var mapRequestCallBack = function(result){
    if( mapObject.selectedSearch == "region" ) {
        mapObject.removeMarkers();
        mapObject.clearCluster();
    }

    $("#map-data-reset").find("i").attr('class', 'fa fa-refresh');
    $("#map-data-go").find("i").attr('class', 'fa fa-search');
    if($("#bank_filters").attr('class') != "animated")
        $("#bank_filters").attr('class', 'animated');
    $(".animateFilterDiv").trigger("click");
    if(result != null){
        $data= JSON.parse(result);

        mapObject.loadingDefault = false;
        if(mapObject.selectedSearch == "region" ){
            mapObject.defaultMarkerArray = $data;
            mapObject.loadingDefault = true;

            var selected_province = '';
            var selected_city = '';

            if($('#city_branch').val() != '') {
                 selected_city = '<br/>'+$('#city_branch').val() + ' City';
            }
            if( $("#province_branch").val() !=""){
                selected_province = '<br/>'+$('#province_branch').val()+' Region ';
            }

            $("#surveyCount").html("<h3>  Total ATM : <strong id='total-count'>"+($data['dataRowset']).length+"</strong> "+selected_province + selected_city+'</h3>');
            if($data['dataRowset'].length && $data['dataRowset'][0]['latitude'] ){
                if($("#province_branch").val() ==""){
                    mapObject.map.setCenter(new google.maps.LatLng(23.268763552489748, 49.87841796875));
                    mapObject.map.setZoom(6);
                }else{
                    mapObject.map.setCenter(new google.maps.LatLng($data['dataRowset'][0]['latitude'], $data['dataRowset'][0]['longitude']));
                    mapObject.map.setZoom(8);
                }
                var myCity = $("#city_branch").val();
                if ($("#city_branch").val()!="" && myCity == $("#city_branch").val()){
                    mapObject.map.setZoom(10);
                }else if(mapObject.map.getZoom() <6) {
                    mapObject.map.setZoom(6);
                }
            }
        }else if( mapObject.selectedSearch == "atm_code"){
            var selected_atm_code = $('#atm_code').val()+' ATM code ';
            $("#record_count").html("Total: <strong id='total-count'> "+($data['dataRowset']).length+" </strong> ATMs located near "+selected_atm_code+'');
            if($data['detailFor'] && $data['detailFor']['latitude'] ){
                mapObject.selectedLocation =  new google.maps.LatLng($data['detailFor']['latitude'], $data['detailFor']['longitude']);
                //mapObject.map.setCenter(new google.maps.LatLng($data['detailFor']['latitude'], $data['detailFor']['longitude']));
                //mapObject.map.setZoom(7);
                mapObject.createCircle(mapObject.selectedLocation, $("#selectedAtmRange").val())
            }
        }else{
            var selected_address = $('#address-auto-complete').val();
            if(selected_address.length > 18 ){
                selected_address= selected_address.truncate(18);
            }
            $("#record_count").html("Total: <strong id='total-count'> "+($data['dataRowset']).length+"</strong> ATMs located near "+selected_address+'');
        }
        mapObject.markerSets = [];
        $.each($data['dataRowset'], function(index, value){
            var marker = mapObject.createMapMarker(value);
            mapObject.markerSets.push(marker);
        });

        // $('.conter-info').html(createCounterList($data['counter']));
        if( mapObject.selectedSearch == "region" ){
            mapObject.createCluster();
        }
        google.maps.event.addListener(mapObject.map, 'idle', function() {

            if( mapObject.selectedSearch == "region" || mapObject.loadingDefault== true ) {
                var totalVisibleMarkers = 0;
                var counterData = {};
                var visibleArray = [];
                for (var i = 0; i < mapObject.defaultMarkerArray['dataRowset'].length; i++) {
                    if (mapObject.map.getBounds().contains(mapObject.markerSets[i].getPosition())) {
                        visibleArray.push(mapObject.markerSets[i]);

                        totalVisibleMarkers++;
                        if (!counterData[mapObject.defaultMarkerArray['dataRowset'][i].name]) {
                            counterData[mapObject.defaultMarkerArray['dataRowset'][i].name] = 1;
                        } else {
                            counterData[mapObject.defaultMarkerArray['dataRowset'][i].name]++;
                        }
                        //mapObject.markerSets[i].setMap(mapObject.map);

                    } else {
                        mapObject.markerSets[i].setMap(null);
                    }
                }
                //$.each( visibleArray, function (i) {
                //    setTimeout(function(){
                //        if ( map.getBounds().contains(visibleArray[i].getPosition()) ){
                //            console.log(visibleArray[i].content);
                //            if( !visibleArray[i].content ){
                //                visibleArray[i].setMap(map);
                //                //markerCluster.repaint();
                //            }
                //        }
                //    }, i );
                //});

                if (mapObject.cluster) {
                    mapObject.clearCluster();
                    mapObject.createCluster();
                }

                //console.log(totalVisibleMarkers,counterData);
                //console.log(visibleArray);

                // $('.conter-info').html(createCounterList(counterData));
                $('#total-count').html(totalVisibleMarkers);
            }

        });
    }

    if(($data['dataRowset']).length <= 0 && $('.sidebar-census').css('display') !== 'block' ){
        if( mapObject.selectedSearch == "region" ) {
            mapObject.removeMarkers();
            mapObject.clearCircle();
            mapObject.clearCluster();
        }

        alert("No Result Found!");

    }
};
function createCounterList($data){

    if(Object.keys($data).length <=0){
        return "<strong> No Result Found! </strong>";
    }
    var $counterListContent = '<ul class="counter-nav ">';
    $.each($data, function(index, value){
        $counterListContent += '<li>';
        $counterListContent += '<strong> '+(index).replace("Bank","ATM")+': '+value+' </strong>';
        $counterListContent += '</li>';
    });
    $counterListContent += '</ul>';

    return $counterListContent;
}

var toggleFilters = function(that){

    /** -- reseting filters  ---  **/

    resetMapData('#map-filters-div', 'map-data-go');
    /** removing active class from li  -- **/
    $(that).parent().find("li").each(function(){
        $(this).removeAttr('class');
    });

    $(that).addClass('active');  /** setting active to the selected ;i -- **/

    $("#selected_filters_type").html($(that).find('a').html());

    $(".drag-info").hide();

    /** -- if radius search is selected  --  **/
    if($(that).find('i').text().trim() == "Radius search"){
        $("#region_search").hide();
        $("#atm_code_search").hide();
        $("#radius_search").show();
        $(".drag-info").show();
        mapObject.selectedSearch = "radius";
    }
    /** -- if region wise search is selected  --  **/
    if($(that).find('i').text().trim() == "Region search"){
        $("#radius_search").hide();
        $("#atm_code_search").hide();
        $("#region_search").show();
        mapObject.selectedSearch = "region";
    }
    if($(that).find('i').text().trim() == "ATM Code search"){
        $("#radius_search").hide();
        $("#region_search").hide();
        $("#atm_code_search").show();
        mapObject.selectedSearch = "atm_code";
    }

};
var routeMap = null;
var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
var directionsService = new google.maps.DirectionsService();
var icons;
var routeMarkersArray = [];
var routeInfoWindow = new google.maps.InfoWindow();
var createRouteMap = function(currentElement){
    var data = $(currentElement).data();


    if(!routeMap){
        routeMap = new google.maps.Map(document.getElementById('route-map-container'), {
            zoom: 15,
            center: {lat: mapObject.centerMarker.getPosition().lat() , lng: mapObject.centerMarker.getPosition().lng()}
        });
        directionsDisplay.setMap(routeMap);
    }

    /** Search center changes, we need to change the center of map -- **/
    if(routeMap.getCenter().lat() != mapObject.centerMarker.getPosition().lat()){
        routeMap.setCenter(new google.maps.LatLng(mapObject.centerMarker.getPosition().lat(),mapObject.centerMarker.getPosition().lng()));
    }

    if(routeMarkersArray.length){

        for (var i = 0; i < routeMarkersArray.length; i++) {
            routeMarkersArray[i].setMap(null);
        }
        routeMarkersArray = [];

        routeInfoWindow.setMap(null);
    }

    icons = {
        start: new google.maps.MarkerImage(
            // URL
            baseUrl + "public/img/Azure.png",
            null, /* size is determined at runtime */
            null, /* origin is 0,0 */
            null, /* anchor is bottom center of the scaled image */
            new google.maps.Size(15, 20)
        ),
        end: mapObject.getMarkerIcon(data)
    };

    // google.maps.event.addListener(routeMap, 'tilesloaded', function(evt) {
    calculateAndDisplayRoute(data);
    // });

};

function calculateAndDisplayRoute(data) {
    var bounds = new google.maps.LatLngBounds();

    directionsService.route({
        origin:  new google.maps.LatLng(mapObject.centerMarker.getPosition().lat(),mapObject.centerMarker.getPosition().lng()),
        destination:  new google.maps.LatLng(data['latitude'], data['longitude']),
        travelMode: google.maps.TravelMode.DRIVING
    }, function(response, status) {
        if (status === google.maps.DirectionsStatus.OK) {
            //console.log(response);

            directionsDisplay.setOptions({ preserveViewport: true });
            directionsDisplay.setDirections(response);

            var leg = response.routes[ 0 ].legs[ 0 ];
            makeMarker( leg.start_location, icons.start, "Journey starts here" );
            makeMarker( leg.end_location, icons.end, 'Destination' );

            var routePoints =  response.routes[ 0 ].overview_path;
            for(var routePointIndex in routePoints){
                // bounds.extend(routePoints[routePointIndex]);
            }
            var step = 1;
            var viaInfo =(response.routes[ 0 ].summary) ? response.routes[ 0 ].summary : '(<i class="fa fa-warning red"></i> &nbsp;Info not available)';
            var routeInfo = "<p>From center of search &nbsp;<i class='fa fa-exchange'></i>&nbsp; "+data['name']+" ( <b>"+leg.distance.text+"</b> )</p>";
            routeInfo += "<i class='fa fa-car'></i>&nbsp;At a drive of <strong>"+leg.duration.text+" </strong> via <strong>"+ viaInfo +"</strong>";
            $("#route-message").html(routeInfo);
            routeMap.setCenter(response.routes[0].legs[0].steps[step].end_location);
            /*
             var routeInfo = "<div style='width: 60px;text-align: right'><strong>" + leg.distance.text +"</strong></div>";
             routeInfoWindow.setContent(routeInfo);
             routeInfoWindow.setPosition(response.routes[0].legs[0].steps[step].end_location);
             routeInfoWindow.open(routeMap);
             */

            // routeMap.fitBounds(bounds);

        } else {
            $("#route-message").html('<i class="fa fa-warning red"></i> &nbsp;No Routes found');
            window.alert('Directions request failed due to ' + status);

        }
    });
}
function makeMarker( position, icon, title ) {
    var marker = new google.maps.Marker({
        position: position,
        map: routeMap,
        icon: icon,
        title: title
    });
    routeMarkersArray.push(marker);
}

function showCurrentLocationDetail(atmDetail){
    console.log(atmDetail);
    var destination = atmDetail['location'].split(',');
    var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(currentLat,currentLong), new google.maps.LatLng(destination[0],destination[1]));
    distance = Math.round((distance * 100)/100);
    kilometer = (distance/1000);
    distance = (distance/1000) + " Km ";
    if(kilometer < 1 || kilometer == 0){
        distance = (distance%1000) + " M ";
    }
    var groupName ='';
    if(atmDetail["group_name"]){
        groupName = ' . It is a '+ titleCase(atmDetail["group_name"]) + ' type';
    }
    $("#atm-comment").html('<p>This ATM is located in the '+ titleCase(atmDetail["province"])+' region, in the city of '+ titleCase(atmDetail["city"])+groupName+' and it '+distance+' away from you. There are xx ATMs in a 100m radius. In the city selected, there are a total of xxx ATMs and a ratio of xxx ATMs per capita. The ATM has xxx in-cidents and was last visited on xxxx.</p>');
}

google.maps.LatLng.prototype.distanceFrom = function(latlng) {
    var lat = [this.lat(), latlng.lat()]
    var lng = [this.lng(), latlng.lng()]
    var R = 6378137;
    var dLat = (lat[1]-lat[0]) * Math.PI / 180;
    var dLng = (lng[1]-lng[0]) * Math.PI / 180;
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat[0] * Math.PI / 180 ) * Math.cos(lat[1] * Math.PI / 180 ) *
        Math.sin(dLng/2) * Math.sin(dLng/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    return Math.round(d);
}


function getObservationImage(atmDetail){


    var requestData = {
        'requestFor'  : 'atm-observation-image',
        'filter-data' : atmDetail
    };

    $.post('request.php', requestData, function(result){
        setTimeout(function() {
            $("#showcase-images").show();
            $("#showcase-loader").hide();
            if(JSON.parse(result).length){

                result = JSON.parse(result);
                if(result.length){
                    atmIssueImage = '';
                    for (i = 0; i < result.length; i++) {
                        atmIssueImage += '<img style="width: 250px; height: 200px;" src="'+result[i]['image_path']+'" class="cloud9-item", alt="Item #'+i+'"/>'
                    }
                    $("#showcase").html(atmIssueImage);
                    $("#showcase").Cloud9Carousel( {
                        buttonLeft: $("#buttons > .left"),
                        buttonRight: $("#buttons > .right"),
                        autoPlay: 1,
                        bringToFront: true
                    });
                }
            }else{

                var image = baseUrl+'/public/img/map-markers/bank-logo/preview.png';
                atmIssueImage = '';
                atmIssueImage += '<img style="width: 250px; height: 200px;" src="'+image+'" class="cloud9-item", alt="Item #1"/><img style="width: 250px; height: 200px;" src="'+image+'" class="cloud9-item", alt="Item #1"/><img style="width: 250px; height: 200px;" src="'+image+'" class="cloud9-item", alt="Item #1"/><img style="width: 250px; height: 200px;" src="'+image+'" class="cloud9-item", alt="Item #1"/>';
                $("#showcase").html(atmIssueImage);
                $("#showcase").Cloud9Carousel({
                    autoPlay: 1,          // [ 0: off | number of items (integer recommended, positive is clockwise) ]
                    bringToFront: true,
                });
            }
        }, 1000);



    });

}
/*
 $(".close-route-map").on("click", function(){
 $("#route-map-holder").html('<div id="route-map-container" style="height: 400px;width: 100%;" ></div>');
 });*/

function getSectionData() {
    var searchSection = ['sidebar-network', 'sidebar-incident', 'sidebar-census', 'sidebar-contest'];
    var ignoreCheckbox = ['nationality', 'activity_status', 'marital_status', 'education_status', 'gender'];
    var selectedFields = $(".sidebar-census-select").val();

    if (!selectedFields) {
        selectedFields = [];

       /* for(var section in searchSection) {
            if( $("." + searchSection[section]).css('display') == 'block' ) {*/
                $("." + 'sidebar-census' + ' input:checked').each(function(){
                    if ( ignoreCheckbox.indexOf($(this).attr('id')) == -1 ) {
                        selectedFields.push($(this).attr('id'));
                    }
                });
           /* }
        }*/
    }

    return selectedFields;
}






