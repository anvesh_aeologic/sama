    
var ughiMap;
       
cluster = null;
clusterOptions = {  styles: styles[0] };

/* function for generation map */
function createUghiWorkForceMap(result) {
    mapCounter = "";
    result = JSON.parse(result);
    mapdata = result['data'];
    mapCounter = result['counter'];
    
    //console.log(mapCounter);
    $(".survey-count-total").css("display", "block");
    $("#work-force-counter").html(mapCounter);
    //$("#ughi_workforce_tracker_citydata").html(result['cityDataHtml']);
    ughiMap = new google.maps.Map(document.getElementById('ughi_workforce_tracker_container'), {
        zoom: 5,
        center: new google.maps.LatLng(22.268763552489748, 47.87841796875),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        panControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        }
    });
    createUghiMarker(mapdata); 
  }

 function bindInfoWindow(marker, map, infowindow, html) {
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(map, marker);
    });
} 

    
 function createUghiMarker(mapdata){
    var markerArray = new Array();
    var infowindow = new InfoBubble({maxWidth: 400, maxHeight:500, borderColor: '#CCCCCC',disableAutoPan: false});
    
  console.log(mapdata);
    for (var i = 0; i < mapdata.length; i++) {
        if(mapdata[i]['location']){
            var location =  mapdata[i]['location'].split(',');
            var latitude  = location[0];
            var longitude = location[1];
         }
        
        
        if(location === 'null' || location === '') {
          
        }else {
            
            var myLatLng  = new google.maps.LatLng(latitude, longitude);
            var marker    = new google.maps.Marker({
                position: myLatLng,
                map: ughiMap,
                icon: baseUrl+'/public/img/logo/unicharm-logo.png'
            });
            //console.log(mapdata);
            markerArray.push(marker);
            
            var markerCluster = new MarkerClusterer(ughiMap, markerArray, clusterOptions);
        
            var infoContent = "<div style='width: 100%;'><div style='border:10px solid white;background:white;padding:1px;border-radius:10px;height: 220px;'>\n\
                           <div><div style='top: 0px;position:relative;margin-left: 3px;height:210px' class='box span12'>\n\
                           <ul class='dashboard-list'><li class=''>\n\
                           <ul class='dashboard-sublist'>";
                if(latitude === '22.268763552489748' || longitude === '47.87841796875' || latitude === 22.268763552489748 || longitude === 47.87841796875) {                           
                    infoContent +="<li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Device Name</span></a><b style='color:#FA2408;font-size:14px;'>" + toTitleCase(mapdata[i]['device_name']) + "</b></li>";
                } else {
                    infoContent +="<li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Device Name</span></a><b style='color:#3E576F;font-size:14px;'>" + toTitleCase(mapdata[i]['device_name']) + "</b></li>";
                                    
                } 
                var merchendise_name = toTitleCase(mapdata[i]['merchandiser_name']);
                var merchent_name = merchendise_name.length;
                if(merchent_name > 25) {
                    var merchantName = merchendise_name.substr(0, 24)+ '...';
                } else {
                    merchantName = merchendise_name;
                }
                var outletLocation = toTitleCase(mapdata[i]['outlet_location']);
                var location = outletLocation.length;
                if(location > 25) {
                    var outlet_location = outletLocation.substr(0, 24)+ '...';
                } else {
                    outlet_location = outletLocation;
                }
                var date = mapdata[i]['latest_survey_date'].split('-');
                var LatestDate = date[2] + "/" + date[1] + "/" + date[0]; 
                    infoContent  +="<li><a href='#' title='"+ merchendise_name +"'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Merchandiser Name</span><b style='color:#3E576F;font-size:14px;'>" + merchantName + "</b></a></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Region Name</span></a><b style='color:#3E576F;font-size:14px;'>" + toTitleCase(mapdata[i]['region']) + "</b></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Outlet Code</span></a><b style='color:#3E576F;font-size:14px;'>" + mapdata[i]['outlet_code'] + "</b></li>\n\
                           <li><a href='#' title='"+ outletLocation +"'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Outlet Location</span><b style='color:#3E576F;font-size:14px;'>" + outlet_location + "</b></a></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Outlet Type</span></a><b style='color:#3E576F;font-size:14px;'>" + mapdata[i]['outlet_type'] + "</b></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Last Survey Date</span></a><b style='color:#3E576F;font-size:14px;'>" + LatestDate + "</b></li>\n\
                           </ul></li></ul></div><div style='height:165px;width:361px;'><div></div></div><div style='width:240px;line-height:10px;margin-top:10px;'></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div></div></div><div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div></div>";
       
        
                //  infowindow.setContent(infoContent);
                //  infowindow.open(ughiMap, this);
                    bindInfoWindow(marker, ughiMap, infowindow, infoContent);
            
        }
        

       
      }

 }
    
function createCluster(){
        if (cluster == undefined) { 
            cluster = new MarkerClusterer(ughiMap, markerArray,clusterOptions);
        } else { 
            this.cluster.addMarkers(markerArray, false); 
        }
        console.log('cluster');
    }

     function clearCluster(){
        if((cluster != undefined)&&(cluster)) {
           cluster.clearMarkers();
           cluster = null;
        }
    }