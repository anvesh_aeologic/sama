function changeTicketAction(selectObject) {
    
    var actionToActivate = $(selectObject).attr('value');
    $("#ticket_action_containor").find('.ticket-action').each(function() {
        $(this).hide();
    });
    if (actionToActivate) {
        $("#" + actionToActivate).show();
    }
}


function actionButtonVisibilityControl($ticketStatusList){
    $buttonList =   getActionButtons();
    $buttonVisibilityFlag = {};
    for(var buttonListIndex in $buttonList){
        $buttonVisibilityFlag[buttonListIndex] = true;
    }
    
    activateButton(['closeButton','resolveButton','transferButton','approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'');
     if(!($ticketStatusList.length === 0)){
        for(var ticketListIndex in $ticketStatusList){
            switch($ticketList[ticketListIndex].toUpperCase()){
                case "PENDING"          : $buttonVisibilityFlag = activateButton(['closeButton','resolveButton','transferButton'],$buttonVisibilityFlag,'activate');
                                          $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                                          break;  
                                          
                case "RESOLVED"          : $buttonVisibilityFlag = activateButton(['closeButton','reopenButton'],$buttonVisibilityFlag,'activate');
                                          $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','resolveButton','transferButton'],$buttonVisibilityFlag,'deactivate');
                                          break;
                                          
                case "WAITING TRANSFER" : $buttonVisibilityFlag = activateButton(['closeButton','approvalButton','rejectButton'],$buttonVisibilityFlag,'activate');
                                          $buttonVisibilityFlag = activateButton(['resolveButton','transferButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                                          break;
                                          
                case "TRANSFER APPROVED" : $buttonVisibilityFlag = activateButton(['closeButton','resolveButton','transferButton'],$buttonVisibilityFlag,'activate');
                                           $buttonVisibilityFlag = activateButton(['approvalButton','rejectButton','reopenButton'],$buttonVisibilityFlag,'deactivate');
                                           break;
            } 
        }
     }
}

function activateButton($buttonList,$buttonVisibilityFlag,$action){
    $availableButtonList =   getActionButtons();
    $classToAdd = 'btn btn-disabled';
    if($action == 'activate'){
     
      $classToAdd = 'btn btn-success';
   }
    
    for($buttonListIndex in $buttonList){
       
        if($buttonVisibilityFlag[ $buttonList[$buttonListIndex] ]){
            if($availableButtonList[ $buttonList[$buttonListIndex] ]){
                              
               $($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class',$classToAdd);
               
                $("#statusButtonSpan").find('a').each(function(){
                      if($(this).attr('status')=='Close'){
                         $(this).attr('class', 'btn btn-danger')
                      } 
                });  
     
                
               
               /* Change the button status */
               if($buttonList[$buttonListIndex] == 'transferButton'){
               
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).unbind('click');
                    $($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class',$($availableButtonList[ $buttonList[$buttonListIndex] ]).attr('class')+" btn-dashboard");
                    activateTransferButton();
               }
               if($action == 'activate'){
                   $($availableButtonList[ $buttonList[$buttonListIndex] ]).show(); 
               }else{
                   $($availableButtonList[ $buttonList[$buttonListIndex] ]).hide(); 
               }
            }
        }
        
        if($action == 'deactivate'){
            $buttonVisibilityFlag[ $buttonList[$buttonListIndex] ] = false;
        }
    }
    return $buttonVisibilityFlag;
}

function activeActionButtons(){
   
    $ticketList = getSelectedTickets('current-status');
    actionButtonVisibilityControl($ticketList);
    return;
}

function previousActionButtonsCode(){
    if($ticketList.length){
        for(var ticketListIndex in $ticketList){
            
            if($ticketList[ticketListIndex] == 'Resolved'){
                if($($buttonList.approvalButton).attr('class') == 'btn btn-success'){
                    if($buttonList.closeButton){
                        $($buttonList.closeButton).attr('class', 'btn disabled');
                        $($buttonList.reopenButton).attr('class', 'btn disabled');
                    }
                    
                    if($buttonList.resolveButton)
                        $($buttonList.resolveButton).attr('class', 'btn disabled');
                    
                    $($buttonList.approvalButton).hide();
                    $($buttonList.rejectButton).hide();
                    $($buttonList.approvalButton).attr('class', 'btn disabled');
                    $($buttonList.rejectButton).attr('class', 'btn disabled');
                    break;
                }else{
                   if($buttonList.closeButton){
                        $($buttonList.closeButton).attr('class', 'btn btn-danger');
                        $($buttonList.reopenButton).attr('class', 'btn btn-success');
                    }
                        
                    if($buttonList.resolveButton)    
                        $($buttonList.resolveButton).attr('class', 'btn btn-success');
                    
                    $($buttonList.approvalButton).hide();
                    $($buttonList.rejectButton).hide();
                    $($buttonList.approvalButton).attr('class', 'btn disabled');
                    $($buttonList.rejectButton).attr('class', 'btn disabled'); 
                }
            }
            
            if($ticketList[ticketListIndex] == 'Pending'){

                if($($buttonList.approvalButton).attr('class') == 'btn btn-success'){
                    
                    if($buttonList.closeButton){
                        $($buttonList.closeButton).attr('class', 'btn disabled');
                        $($buttonList.reopenButton).attr('class', 'btn disabled');
                    }
                    
                    if($buttonList.resolveButton)
                        $($buttonList.resolveButton).attr('class', 'btn disabled');
                    
                    $($buttonList.approvalButton).hide();
                    $($buttonList.rejectButton).hide();
                    $($buttonList.approvalButton).attr('class', 'btn disabled');
                    $($buttonList.rejectButton).attr('class', 'btn disabled');
                    break;
                }else{
                    if($buttonList.closeButton){
                        $($buttonList.closeButton).attr('class', 'btn btn-danger');
                        $($buttonList.reopenButton).attr('class', 'btn disabled');
                    }
                        
                    if($buttonList.resolveButton)    
                        $($buttonList.resolveButton).attr('class', 'btn btn-success');
                    
                    if($($buttonList.approvalButton)){
                        $($buttonList.approvalButton).hide();
                        $($buttonList.rejectButton).hide();
                        $($buttonList.approvalButton).attr('class', 'btn disabled');
                        $($buttonList.rejectButton).attr('class', 'btn disabled');
                    }
                }
            }
            
            if($ticketList[ticketListIndex] == 'Waiting Budget'){
                if($buttonList.closeButton){
                    $($buttonList.closeButton).attr('class', 'btn disabled');
                    $($buttonList.reopenButton).attr('class', 'btn disabled');
                }
                    
                if($buttonList.resolveButton)
                    $($buttonList.resolveButton).attr('class', 'btn disabled');
                    
                if($($buttonList.approvalButton)){
                        $($buttonList.approvalButton).hide();
                        $($buttonList.rejectButton).hide();
                        $($buttonList.approvalButton).attr('class', 'btn disabled');
                        $($buttonList.rejectButton).attr('class', 'btn disabled');
                    }
                break;
            }
            
            if($ticketList[ticketListIndex] == 'Supplier Quotation'){
                if($buttonList.closeButton){
                    $($buttonList.closeButton).attr('class', 'btn disabled');
                    $($buttonList.reopenButton).attr('class', 'btn disabled');
                }
                
                if($buttonList.resolveButton)
                    $($buttonList.resolveButton).attr('class', 'btn disabled');
                
                if($($buttonList.approvalButton)){
                        $($buttonList.approvalButton).hide();
                        $($buttonList.rejectButton).hide();
                        $($buttonList.approvalButton).attr('class', 'btn disabled');
                        $($buttonList.rejectButton).attr('class', 'btn disabled');
                    }
                break;
            }
            
            if($ticketList[ticketListIndex] == 'Waiting Transfer'){
                if($($buttonList.closeButton).attr('class') == 'btn btn-success'){
                    if($buttonList.closeButton){
                        $($buttonList.closeButton).attr('class', 'btn disabled');
                        $($buttonList.reopenButton).attr('class', 'btn disabled');
                    }
                    
                    if($buttonList.resolveButton)
                        $($buttonList.resolveButton).attr('class', 'btn disabled');
                    
                    if($($buttonList.approvalButton)){
                        $($buttonList.approvalButton).hide();
                        $($buttonList.rejectButton).hide();
                        $($buttonList.approvalButton).attr('class', 'btn disabled');
                        $($buttonList.rejectButton).attr('class', 'btn disabled');
                    }
                    
                    break;
                }else{
                    if($buttonList.closeButton){
                        $($buttonList.closeButton).attr('class', 'btn disabled');
                        $($buttonList.reopenButton).attr('class', 'btn disabled');
                    }
                    
                    if($buttonList.resolveButton)
                        $($buttonList.resolveButton).attr('class', 'btn disabled');
                        
                    if($($buttonList.approvalButton)){
                        $($buttonList.approvalButton).show();
                        $($buttonList.rejectButton).show();
                        $($buttonList.approvalButton).attr('class', 'btn btn-success');
                        $($buttonList.rejectButton).attr('class', 'btn btn-danger');
                    }
                    
                }
            }
        }
        
    }else{
        if($buttonList.resolveButton)
            $($buttonList.resolveButton).attr('class', 'btn disabled');
        
        if($buttonList.closeButton){
            $($buttonList.closeButton).attr('class', 'btn disabled');
            $($buttonList.reopenButton).attr('class', 'btn disabled');
        }
        
        $($buttonList.approvalButton).hide();
        $($buttonList.rejectButton).hide();
        $($buttonList.approvalButton).attr('class', 'btn disabled');
        $($buttonList.rejectButton).attr('class', 'btn disabled');
    }
}

function getActionButtons(){
    $buttonList =   {
        "closeButton"   :   0,
        "resolveButton" :   0,
        "reopenButton"  :   0,
        "approvalButton":   0,
        "rejectButton"  :   0,
        "reserveButton"  :   0,
        "transferButton" : 0
    }
    $("#statusButtonSpan").find('a').each(function(){
          switch($(this).attr('status')){
            case "Close"       :   $buttonList.closeButton = this; break;
            case "Resolve"      :   $buttonList.resolveButton = this; break;
            case "Pending"      :   $buttonList.reopenButton = this; break;
            case "Reject"     :   $buttonList.rejectButton = this; break;
            case "Reserved"     :   $buttonList.reserveButton = this; break;
            case "Approve"     :   $buttonList.approvalButton = this; break;
            case "Transfer"     : $buttonList.transferButton = this; break;
          }
    });
    return $buttonList;
}

function getSelectedTickets($attribute){
    $ticketList = new Array();
    $("#ticketingBody").find("input.ticket_panel_checkbox").each(function(){
        
        if($(this).is(":checked")){
            $ticketList.push($(this).data($attribute));
        }
    });
    return $ticketList;
}

function getSelectedNlTickets($attribute){
    $ticketList = new Array();
    $("#ticketingBody").find("input.ticket_panel_nl_checkbox").each(function(){

        if($(this).is(":checked")){
            $ticketList.push($(this).data($attribute));
        }
    });
    return $ticketList;
}

function changeStatusTo($element){
    console.log($element); 
   $isCloseAllowed = 0;
  if($($element).attr('status')=='Close'){
    if(confirm("Are you sure you want to close this ticket?")){
       $isCloseAllowed = 1;
       isClosedTicketExist = 1;
    }
  } else {
   
    switch( ($($element).attr('status')).toUpperCase() ){
        case "TRANSFER" : case "RESOLVE" : case "UNDO" : case "APPROVE" : case "REJECT" :case "PENDING" : $isCloseAllowed = 1; break;
        default: $isCloseAllowed = 0;
    }
  }
  
   if( $($element).hasClass('disabled') ){
        return;
   }
   
  if($isCloseAllowed){  
    
    $("#status_update_loader").show();
    $("#ticketFilterResultDiv").addClass('hide');
    if($($element).attr('status') == 'undo'){
        $ticketList =   new Array($($element).attr('ticket-number'));
    }else{
        $ticketList =   getSelectedTickets('ticket-number');
    }
    changeTicketStatus($ticketList,null,$($element).attr('status'));
 }
    
}

function requestMessages($requestType){
    switch($requestType.toUpperCase()){
        case "TRANSFER"     :   return "This action will tranfer this ticket to Engineering Department. Press OK to proceed or CANCEL to undo";
        case "BUDGET"       :   return "This action will send mail for Budget Approval. Press OK to proceed or CANCEL to undo";
        case "SUPPLIER"     :   return "This action will send mail for Supplier Quotation. Press OK to proceed or CANCEL to undo";
        case "FORWARD"      :   return "This action will Forward the ticket detail. Press OK to proceed or CANCEL to undo";
    }
}

 function checkActionEmail($element,$type) {
        if($element){
            $elementValue   =   $element;
            if($type == 'element'){
                $element    =   document.getElementById($element);
                $elementValue = $element.value;
            }
           
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        
            if($type == "other"){
                $elementValue = $elementValue.split(",");
            }else{
                $elementValue = new Array($elementValue);
            }
                    
            for(var i in $elementValue){
                if (!filter.test($elementValue[i])) {
                    alert('Please provide a valid email address');
                    return false;
                }
            }
            
            return true;
        }else{
            alert('Please provide a valid email address');
            return false;
        }
}

function createTicketStatusRequest($requestData){
    $sendTicketRequest = false;
    if($requestData.haveEmailId){
        if(checkActionEmail($requestData.userEmailId,"data")){
            if($requestData['ccEmailId']){
                if(!(checkActionEmail($requestData.ccEmailId,"other")) ){
                    $("#status_update_loader").hide();
                    return;
                }
            }
            
            var transferConfirmation = confirm(requestMessages($requestData.action));
            if(transferConfirmation){
                $sendTicketRequest = true;
            }else{
                $("#status_update_loader").hide();
                
            }
        }else{
             $("#status_update_loader").hide();
             
        }
    }else{
        if($requestData.action == 'TRANSFER'){
            if($requestData.deptId){
                $sendTicketRequest = true;
            }else{
                alert('Please Select a department to transfer');
            }
        }else{
            $sendTicketRequest = true;
        }
    }
    
    if($sendTicketRequest){
        $requestObject.initialize($requestData,"reloadTicketPanel",null);
        getTicketPopupContent($requestData.ticketNumber);
    }
}

//Function to reload the ticket panel after any action
function reloadTicketPanel(){
    $("#transfer-waiting-img").hide();
    $("#ticketFilterResultDiv").addClass('hide');
    $("#transfer-cancel-button").trigger('click');
    $("#status_update_loader").hide();
    
    filterTickets();
}

//Function to Change the status of the ticket accrodingly
function changeTicketStatus($ticketNumber,$imageId,$requestType){
    $requestData = null;
    switch($requestType.toUpperCase()){
        case "TRANSFER"  :   $requestData = {
                                "requestFor"    :   "ticket-status-change",
                                "action"        :   "TRANSFER",
                                "ticketNumber"  :   $ticketNumber,
                                "imageId"       :   $imageId,
                                "deptId"        :   $("#transferownership_dept").val(),
                                "haveEmailId"   :   false
                             };
                             break;
        case "BUDGET"    :   $requestData = {
                                "requestFor"    :   "ticket-status-change",
                                "action"        :   "BUDGET",
                                "ticketNumber"  :   $ticketNumber,
                                "imageId"       :   $imageId,
                                "language"      : $('#language').val(),
                                "haveEmailId"   :   true,
                                "userEmailId"   :   $("#actionemail").val(),
                                 "ccEmailId"    :   $('#submitBudgetForwardInfoCcEmail').val(),
                                "userContent"   :   $("#quotationText").val(),
                                "budgetAmount"  :   $("#budgetAmount").val(),
                                "userAttachment":   $("#forwardinfoAttachment").val()  
                             };
                             break;
        case "SUPPLIER"  :   $requestData = {
                                "requestFor"    :   "ticket-status-change",
                                "action"        :   "SUPPLIER",
                                "ticketNumber"  :   $ticketNumber,
                                "imageId"       :   $imageId,
                                "language"      : $('#language').val(),
                                "haveEmailId"   :   true,
                                "userEmailId"   :   $("#quotationemail").val(),
                                "ccEmailId"     :   $("#submitSupplierForwardInfoCcEmail").val(),
                                "userContent"   :   $("#supplierquotationText").val(),
                                "userAttachment":   $("#forwardinfoAttachment").val()   
                             };
                             break;
        case "FORWARD"   :   $requestData = {
                                "requestFor"    :   "ticket-status-change",
                                "action"        :   "FORWARD",
                                "ticketNumber"  :   $ticketNumber,
                                "imageId"       :   $imageId,
                                "language"      : $('#language').val(),
                                "haveEmailId"   :   true,
                                "userEmailId"   :   $("#forwardInfoEmail").val(),
                                "ccEmailId"     :   $("#forwardInfoCcEmail").val(),
                               // "userContent"   :   $("#forwardTicketForwardInfoCcEmailList").val(),
                                "userContent"   :   $('textarea#forwardinfoText').val(),
                                "userAttachment" :  $("#forwardinfoAttachment").val()       
                             };
                             break;
                             
        case "CATEGORY"  :  $requestData = {
                                "requestFor"    :   "ticket-status-change",
                                "action"        :   "ChangeCategory",
                                "ticketNumber"  :   $ticketNumber,
                                "category"        :   $("#category_type").val(),
                                "imageId"       :   $imageId
                             };
                             break;
        default          :   $requestData = {
                                "requestFor"    :   "ticket-status-change",
                                "action"        :   $requestType.toUpperCase(),
                                "ticketNumber"  :   $ticketNumber,
                                "imageId"       :   $imageId,
                                "haveEmailId"   :   false
                             };
                             break;   
    }
    
    if($requestData){
        $requestData['database_name']=$('#database_name').val();
        createTicketStatusRequest($requestData);
    }

    
}


//Function to verify that tickets are same
function verifySameTickets() {
        $pendingTicketFlag = true;
        $departmentTicketFlag = true;
        $statusList = getSelectedTickets('current-status');
        for(i in $statusList){
            if($statusList[i].toUpperCase() != 'PENDING'){
                if($statusList[i].toUpperCase() != 'TRANSFER APPROVED'){
                    $pendingTicketFlag = false;
                    break;
                }
            }
        }
        if($pendingTicketFlag){
            $ticketDepartment = getSelectedTickets('current-department');
            $departmentName = $ticketDepartment[0].toUpperCase();
            for(i in $ticketDepartment){
                if($ticketDepartment[i].toUpperCase() != $departmentName){
                    $departmentTicketFlag = false;
                    break;
                }
            }
        }
        
        if(!$pendingTicketFlag){
            alert("Selected tickets must have Pending status");
            return false;
        }else{
            if(!$departmentTicketFlag){
                alert("Selected tickets must belongs to same department");
                return false;
            }else{
                return true;
            }
        }
    }
    
//Function to activate and deactive transfer button
function activateTransferButton(){
        $('.btn-dashboard').click(function(e){
            
              if($(this).attr('status') != 'Transfer'){ 
                return;
              }
             
               $("#transfer-waiting-img").hide();
               if(verifySameTickets()){ 
                   e.preventDefault();
                   $("#transfer-department-list").html('<img title="Loading" src="'+baseUrl+'/public/img/ajax-loaders/bsfmap-loader1.gif">')
                   $('#dashboard-modal').modal('show');
                   resetDepartmentList();
               }
       
        });
}
    
//Function to reload the department list for transfer
function resetDepartmentList(){
     
    $ticketDepartment = getSelectedTickets('current-department');
    $requestData = {
        "requestFor"  :   "transfer-department-list",
        "department-name"  :   $ticketDepartment[0]  
    }
    $requestObject.initialize($requestData,null,'#transfer-department-list');
       
}

//Function to transfer multiple ticket 
function transferMultipleTicket(){
    $("#transfer-waiting-img").show();
    $requestData = {
        "requestFor"    :   "multiple-ticket-transfer",
        "action"        :   "TRANSFER",
        "ticketNumber"  :   getSelectedTickets('ticket-number'),
        "imageId"       :   null,
        "deptId"        :   $("#transfer-department").val(),
        "haveEmailId"   :   false
    }
    $requestObject.initialize($requestData,"reloadTicketPanel",null);
}


