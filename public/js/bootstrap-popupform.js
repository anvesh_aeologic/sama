/* ===========================================================
 * bootstrap-popupform.js v2.0.4
 * http://twitter.github.com/bootstrap/javascript.html#popupforms
 * ===========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================================================== */

var toolTipText = [];
!function ($) {

  "use strict"; // jshint ;_;


 /* popupform PUBLIC CLASS DEFINITION
  * =============================== */
  
    
  var Popupform = function ( element, options ) {
	 this.init('popupform', element, options)
  }


  /* NOTE: popupform EXTENDS BOOTSTRAP-TOOLTIP.js
     ========================================== */

  Popupform.prototype = $.extend({}, $.fn.customtooltip.Constructor.prototype, {

    constructor: Popupform

  , setContent: function () {
      $(document).find('.popover').each(function(){
            $(this).remove();
      });
      
      var $tip = this.tip()
        , title = this.getTitle()+"<span style='float:right;cursor:pointer;' class='icon-remove' onclick=removePictureTooltip('')></span>"
        , content = this.getContent()

      $tip.find('.popover-title')[this.isHTML(title) ? 'html' : 'text'](title)
      $tip.find('.popover-content > *')[this.isHTML(content) ? 'html' : 'text'](content)
	  	
      $tip.removeClass('fade top bottom left right in');
	  $("#dataHere").focus();
       
	  
    }

  , hasContent: function () {
	   return this.getTitle() || this.getContent()
    }

  , getContent: function () {
      var content
        , $e = this.$element
        , o = this.options

      content = $e.attr('data-content')
        || (typeof o.content == 'function' ? o.content.call($e[0]) :  o.content)
      if(!toolTipText){
        toolTipText = '';
      }  
	  var innerContent = '<div>';
      var hasTipFlag = false;
      $.ajaxSetup({async:false});
          toolTipText = [];
          $.post('soapRequest.php',{'type':'comments','id':content},function(result){
                result = JSON.parse(result);
                toolTipText = result.content;
                
          });
      $.ajaxSetup({async:true});
      hasTipFlag = 0;
      if(toolTipText.length){
        hasTipFlag =  1;
      }
      if(isTouchDevice){
          if(hasTipFlag){
            innerContent += '<div style="width:100%;padding-bottom:5px;"><div style="float:right;padding-bottom:2px;"><button class="btn btn-mini btn-success" onclick=resolve("'+content+'")>Resolve</button></div></div><div style="clear:both"></div><div id="scrollbar1" hasScroll="0"><div class="scrollbarx" style="height:100px;overflow-y:auto;overflow-x:hidden"><nav><ul class="dashboard-list">';
                for(var i in toolTipText){
                    if((toolTipText[i].comment).trim()){
                        innerContent += '<li  id="ContentPart_'+content+'_'+toolTipText[i].id+'"><span id="ContentBox_'+content+'_'+toolTipText[i].id+'" style="display:block"><span id="ContentSpan_'+content+'_'+toolTipText[i].id+'">'+toolTipText[i].comment+'</span><div>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:toggleTip("'+content+'","'+toolTipText[i].id+'")>Edit</a>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:removeTip("'+content+'","'+toolTipText[i].id+'")>Delete</a></div></span><span id="EditBox_'+content+'_'+toolTipText[i].id+'" style="display:none"><textarea id="TextBox_'+content+'_'+toolTipText[i].id+'" class="autogrow" style="width:220px;">'+toolTipText[i].comment+'</textarea>';
                        innerContent += '<div>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:saveTip("'+content+'","'+toolTipText[i].id+'")>Ok</a>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:toggleTip("'+content+'","'+toolTipText[i].id+'")>Cancel</a></div></span></li>';
                    }
                }
            innerContent += '</ul></nav></div></div>';
          }  
      } else {
          if(hasTipFlag){
            innerContent += '<div style="width:100%;padding-bottom:5px;border-bottom:1px solid #CCCCCC">&nbsp;<div style="float:right;padding-bottom:2px;"><button class="btn btn-mini btn-success" onclick=resolve("'+content+'")>Resolve</button></div></div><div style="clear:both"></div><div id="scrollbar1" hasScroll="0"><div class="scrollbar" style="display:none;"><div class="track"><div class="thumb"><div class="end"></div></div></div></div><div class="viewport"><div id="overview" class="overview"><ul class="dashboard-list">';
                for(var i in toolTipText){
                    if((toolTipText[i].comment).trim()){
                        innerContent += '<li  id="ContentPart_'+content+'_'+toolTipText[i].id+'"><span id="ContentBox_'+content+'_'+toolTipText[i].id+'" style="display:block"><span id="ContentSpan_'+content+'_'+toolTipText[i].id+'">'+toolTipText[i].comment+'</span><div>&nbsp;<span class="label label-yellow">'+toolTipText[i].comment_date+'</span></div><div>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:toggleTip("'+content+'","'+toolTipText[i].id+'")>Edit</a>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:removeTip("'+content+'","'+toolTipText[i].id+'")>Delete</a></div></span><span id="EditBox_'+content+'_'+toolTipText[i].id+'" style="display:none"><textarea id="TextBox_'+content+'_'+toolTipText[i].id+'" class="autogrow" style="width:220px;">'+toolTipText[i].comment+'</textarea>';
                        innerContent += '<div>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:saveTip("'+content+'","'+toolTipText[i].id+'")>Ok</a>&nbsp;<a style="width: 40px;font-size:9px" href=javascript:toggleTip("'+content+'","'+toolTipText[i].id+'")>Cancel</a></div></span></li>';
                    }
                }
            innerContent += '</ul></div></div></div>';
          }
      }
      
     
      
      innerContent +='<textarea id="dataHere_'+content+'" class="autogrow" cols="40" placeholder="Place Content Here" style="margin-top:10px;" onfocus=showHideDiv("ControlButton_'+content+'","show")></textarea>'+
               '<div id="ControlButton_'+content+'" align="right" style="display:none;"><a class="btn btn-mini" style="width: 40px;" href=javascript:checkPictureTooltip("HasTooltip_'+content+'")>Ok</a>&nbsp;<a class="btn btn-mini" style="width: 40px;" href=javascript:removePictureTooltip("HasTooltip_'+content+'")>Cancel</a></div>'+
            '</div>';
      return innerContent;
    }

  , tip: function () {
      if (!this.$tip) {
        this.$tip = $(this.options.template)
      }
      return this.$tip
    }

  })


 /* popupform PLUGIN DEFINITION
  * ======================= */

  $.fn.popupform = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('popupform')
        , options = typeof option == 'object' && option
      if (!data) $this.data('popupform', (data = new Popupform(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.popupform.Constructor = Popupform

  $.fn.popupform.defaults = $.extend({} , $.fn.customtooltip.defaults, {
    placement: 'right'
  , content: ''
  , template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
  , delay: 400
  })

}(window.jQuery);

var textBoxToFocus = null;

function removePictureTooltip(thisone){
     $(document).find('.popover').each(function(){
            $(this).remove();
     })
     if(thisone){
        hasTooltip(thisone);
     }
}


function getComments(recordId){
     
     $.post('soapRequest.php',{'type':'comments','id':recordId},function(result){
            result = JSON.parse(result);
            toolTipText = result.content;
            return (result.content).length;
     });
     
}
function toggleTip(thisone,pocket){
    textBoxToFocus = null;
     if($("#ContentBox_"+thisone+"_"+pocket).css('display')=='block'){
        $("#ContentBox_"+thisone+"_"+pocket).hide();
        $("#EditBox_"+thisone+"_"+pocket).show();
        textBoxToFocus = "TextBox_"+thisone+"_"+pocket;
     }else{
        $("#ContentBox_"+thisone+"_"+pocket).show();
        $("#EditBox_"+thisone+"_"+pocket).hide();
     }
     $('#scrollbar1').attr('hasScroll','0');
     initializeScroll();
}

function saveTip(pocket1,pocket2){
    if($("#TextBox_"+pocket1+"_"+pocket2).val().trim()){
         $.ajaxSetup({async:false});
         $.post('soapRequest.php',{'type':'UPDATECOMMENT','id':pocket2,'comment':$("#TextBox_"+pocket1+"_"+pocket2).val().trim()},function(result){
             $("#ContentSpan_"+pocket1+"_"+pocket2).html(result);
             hasTooltip("HasTooltip_"+pocket1);
             toggleTip(pocket1,pocket2);
         });
         $.ajaxSetup({async:true});
    }else{
        removeTip(pocket1,pocket2);
    }
}

function removeTip(pocket1,pocket2){
    if(confirm("Are you sure about your action?")){
        $.ajaxSetup({async:false});
        $.post('soapRequest.php',{'type':'REMOVECOMMENT','id':pocket2},function(result){
            $("#ContentPart_"+pocket1+"_"+pocket2).remove();
            removePictureTooltip("scrollbar1_"+pocket1);
        });
        $.ajaxSetup({async:true});
        
    }
}

function checkPictureTooltip(thisone,thisId){
     var pocket = thisone.split('_');
     
     if($("#dataHere").val().trim()){
        /*if(!toolTipText[pocket[1]])
            toolTipText[pocket[1]] = new Array();
            toolTipText[pocket[1]].push($("#dataHere").val().trim());*/
         $.ajaxSetup({async:false});
         $.post('soapRequest.php',{'type':'SAVECOMMENT','id':pocket[1],'comment':$("#dataHere").val().trim()},function(result){
             $(document).find('.popover').each(function(){
                 $(this).remove();
                 hasTooltip(thisone);
             })
         });
         $.ajaxSetup({async:true});
         
     }
     
    
}

function hasTooltip(thisone){
    textBoxToFocus = null;
    var pocket = thisone.split('_');
    var hasTipFlag = false;
    
    $.ajaxSetup({async:false});
    toolTipText = [];
    $.post('soapRequest.php',{'type':'comments','id':pocket[1]},function(result){
        result = JSON.parse(result);
        toolTipText = result.content;
        
    });
    $.ajaxSetup({async:true});
      
    if(toolTipText.length){
       hasTipFlag =  true;
    }
    
    if(hasTipFlag){
        $("#HasTooltip_"+pocket[1]).show();
    }else{
        $("#HasTooltip_"+pocket[1]).hide();
    }
    $('#scrollbar1').attr('hasScroll','0');
    initializeScroll(); 
}

function showHideDiv(thisone,action){
    if(action == 'show'){
        $("#"+thisone).show();
    }else{
        $("#"+thisone).hide();
    }
}

function initializeScroll(){
   
   if(isTouchDevice){
       if($("#scrollbar1").height()){
           if(($('#scrollbar1').height()>80)&&($('#scrollbar1').attr('hasScroll')=='0')){
                $(document).ready(function(){
                    $("#scrollbar").height(200);
                    $('nav').touchScroll();
                    $('#scrollbar1').attr('hasScroll','1');
                });
           } 
       }  
   }else{
       if($("#scrollbar1").height()){
        //console.log($('#overview').height());
           if(($('#overview').height()>80)&&($('#scrollbar1').attr('hasScroll')=='0')){
                $(document).ready(function(){
                    $(".scrollbar").height(200);
                    $(".viewport").height(100);
                    $(".scrollbar").show();
                    $('#scrollbar1').tinyscrollbar({ sizethumb: 15 });
                    $('#scrollbar1').attr('hasScroll','1');
                });
           } 
       }
   }
   if(textBoxToFocus){
        document.getElementById(textBoxToFocus).focus();
   }
}

function resolve(thisone){
   var pocket = thisone.split('_');
   $.ajaxSetup({async:false});
   toolTipText = [];
   $.post('soapRequest.php',{'type':'REMOVEIMAGE','id':pocket[0]},function(result){
        removePictureTooltip("scrollbar1_"+thisone);
        $("#HasTooltip_"+thisone).parent().parent().remove();
   });
   $.ajaxSetup({async:true});
   
}


String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}