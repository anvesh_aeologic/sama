 var d = new Date();
 var month = d.getMonth()+1;
 var day = d.getDate();
 var date = d.getFullYear() + '-' + ((''+month).length<2 ? '0' : '') + month;
 var date_year = d.getFullYear();
 var date_month = d.getMonth()+1;
    

 $(document).ready(function(){
        $('div #monhtlyClickAgentId').click(function() {
             $('#monthlyTableContainer').show();
             $('#tillDateTableContainer').hide();
             $('#monthActiveIcon').show();
             $('#tillDateActiveIcon').hide();
        });
        
        $('div #tillDateClickAgentId').click(function() {
              $('#monthlyTableContainer').hide();
              $('#tillDateTableContainer').show();
              $('#monthActiveIcon').hide();
              $('#tillDateActiveIcon').show();
         });
  });
  
  
  $(document).ready(function(){
        var monthValue = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        var monthValueShort = new Array('Jan', 'Feb', 'Mar', 'Aprl', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        var l = window.location;
        var baseUrlAddress = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];
        $("#monthYearPicker").monthpicker({
            selectedYear:date_year,
            startYear:date,
            finalYear: date_year,
            selectedMonthName: monthValue[month-2]
        }).bind('monthpicker-click-month', function (e, month) {
            console.log(month + ' - ' + date_month);
            if(month >= date_month || month < 4 ){
                $(this).val('');
                $('#reportContainerModal .modal-body').html('<div style="height:30px; padding:15px;font-size:20px;" class="alert alert-info">Sorry, Report is not available for this month!</div>');
                $('#reportContainerModal').modal('show');
            } else{
                var that =  this;
                var newSrc = '';
                var branchSrc = '';
                var dateTime = ($(that).val()).split("/");
                $("#reportForMonth").html("Department Report(s) for "+monthValueShort[Number(dateTime[0])-1]+"-"+dateTime[1]); 
                $(".reportSection").find("a").each(function(){
                    newSrc = $(this).attr('myval')+"&reportTime="+dateTime[1]+"-"+dateTime[0];
                    $(this).attr('href',newSrc);
                });
                $(".branchScorereportSection").find("a").each(function(){
                    branchSrc = $(this).attr('myval')+"&reportTime="+dateTime[1]+"-"+dateTime[0];
                    branchSrc = baseUrlAddress+"/crawler/download-branch-score.php?type=Men&month="+dateTime[0].replace(/^0+/, '');
                    $(this).attr('href',branchSrc);
                });
                $('#selectedMonthText').html("Department Report(s) for "+monthValueShort[Number(dateTime[0])-1]+"-"+dateTime[1]);
                setTimeout(function(){
                    $("#reportLoader").hide();
                },500);
            }
        });
    });
    
   $('#ReportSectionTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show');
    });