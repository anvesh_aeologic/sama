function load() {
    gapi.client.setApiKey('AIzaSyBpyBYENJDHoHa7oncaa6Ja9rSVnV_VeYo');
}

function fusionTable(select, where, orderby, returnFuction, fusionTableId,otherParam) {
    if (!fusionTableId || fusionTableId === '') {
        fusionTableId = '183rOs2p5h5EzFN57cYu4WwSU1ajV9qpDcbBiiu4';
    }
    var myTable1 = new FusionTableClass(fusionTableId,otherParam);
    myTable1.run(select, myTable1, where, orderby, returnFuction);
}

function FusionTableClass(table_id, otherParam)
{
    this.option = otherParam;
    this.counter = 0;
    this.table = table_id;
    var request = null;
    this.run = function(q, cls, where, order, returnFuction)
    {
        gapi.client.load('fusiontables', 'v1', function() {
            if (where)
                request = gapi.client.fusiontables.query.sqlGet({'sql': q + cls.table + where + order});
            else
                request = gapi.client.fusiontables.query.sqlGet({'sql': q + cls.table + order});
            return request.execute(function(DATA) {
                window[returnFuction](DATA,this.option);
            });
        });
    };

    this.exec = function(data) {
        console.log(data);
        //return data.result;
    };
}