
var map;
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

var testData  = {
    location:null,
    weight:null
};


function MapCreator(options) 
{
    this.cluster = null;
    this.markerArray = [];
    this.map    =   null;
    this.centerPoint    =   null;
    this.clusterOptions = {  styles: styles[0] };
    this.mapType = options.type;
    this.mapContainer   =  options.mapContainer;  
    this.markerInfo = [];
    this.reportInfo = [];
    this.heatmap = null;
    
    if(bankFolder!='rbcapital'){
      this.infowindowObj = new InfoBubble({maxWidth: 400,borderColor: '#CCCCCC'});    
    } else {
      this.infowindowObj =  null;
    }
    
    this.createMap = function(){
        var mapOptions = { 
                center: new google.maps.LatLng(22.268763552489748,47.87841796875),
                zoom: 5, 
                mapTypeId: google.maps.MapTypeId.ROADMAP 
        };
        
        this.map = new google.maps.Map(document.getElementById(this.mapContainer), mapOptions);
        changeIsrailLayer(this.map.getZoom(),this.map);
        
    };
    
    this.createMarker = function(latLng,markerType,otherData){

        var me = this;
        var marker = new google.maps.Marker({ 
                position    :   latLng, 
                icon        :   this.createIcon(markerType),
                flat        : true
        });
        if( latLng.lat()!=0 && latLng.lng()!=0 ){
            me.markerArray.push(marker);
        }
        me.markerInfo.push([otherData,
                    me.markerArray.length-1]
        );
        
        google.maps.event.addListener(marker, 'click', function (event) {
            me.openInfowindow(this,otherData);
        });
    };


    
   if(bankFolder=='rbcapital'){
      
         this.openInfowindow = function(marker,data){
         if(this.infowindowObj){
            this.infowindowObj.removeTab(0);
            this.infowindowObj.removeTab(1);
            this.infowindowObj.setMap(null);
         }
         
         this.infowindowObj = new InfoBubble({maxWidth: 400,borderColor: '#CCCCCC'});
         this.infowindowObj.open(this.map,marker);
         this.infowindowObj.addTab('<b>Profile Info</b>',this.createContent(data));
         this.infowindowObj.addTab('<b>Profile Image</b>',tabInfo1(data));
        
         }
    
       
   } else if(bankFolder=='rbatm'  || bankFolder=='desktopapp' || bankFolder=='ncbindependent'){

         this.openInfowindow = function(marker,data){
        
         if(this.infowindowObj){
            this.infowindowObj.removeTab(0);
            this.infowindowObj.removeTab(1);
            this.infowindowObj.setMap(null);
         }
         
         this.infowindowObj = new InfoBubble({maxWidth: 400,borderColor: '#CCCCCC'});
         this.infowindowObj.open(this.map,marker);
         this.infowindowObj.addTab('<b>ATM Image</b>',ncbImageInfo(data));
         this.infowindowObj.addTab('<b>ATM Details</b>',this.createContentNcbAtm(data));
        
        
        
         }
     
        
    } 
    else if(bankFolder=='ncbatm')
    {

        this.openInfowindow = function(marker,data){
         if(this.infowindowObj){
            this.infowindowObj.removeTab(0);
            this.infowindowObj.removeTab(1);
            this.infowindowObj.setMap(null);
         }
         
         this.infowindowObj = new InfoBubble({maxWidth: 460,borderColor: '#CCCCCC'});
         this.infowindowObj.open(this.map,marker);
         this.infowindowObj.addTab('<b>ATM Image</b>',ncbImageInfo(data));
         this.infowindowObj.addTab('<b>ATM Details</b>',this.createContentNcbAtm(data));
        
        
        
         }   
    }
    else if(bankFolder=='rbatm'){      
      
         this.openInfowindow = function(marker,data){
        
         if(this.infowindowObj){
            this.infowindowObj.removeTab(0);
            this.infowindowObj.removeTab(1);
            this.infowindowObj.setMap(null);
         }
         
         this.infowindowObj = new InfoBubble({maxWidth: 400,borderColor: '#CCCCCC'});
         this.infowindowObj.open(this.map,marker);
      
         this.infowindowObj.addTab('<b> Profile </b>',this.createContentRbAtm(data));
         this.infowindowObj.addTab('<b> Support </b>',this.rbSupportInfo(data));   
         this.infowindowObj.addTab('<b> Photo </b>',rbImageInfo(data));
        
         }
    } else {
      
         this.openInfowindow = function(marker,data){   
         this.openInfowindow = function(marker,data){
         
          if(bankFolder == 'ncbatm' || bankFolder == 'desktopapp' ||bankFolder == 'ncbatm_v5' ){
             this.infowindowObj.setContent(this.createContentNcbAtm(data));
          }else{
             this.infowindowObj.setContent(this.createContent(data));
          }
             this.infowindowObj.open(this.map,marker);
         }
      }
   }
   
    
   this.createContent = function(data){
         var genderIndicator;
         var gentsExist = false;
         var finalBranchName;
      
        if(data.type == 'ATM'){
        
            if(bankFolder == 'bsfatm'){
                html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                        "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                            "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.atmImage) + "' style='height:160px;width:235px;'/></div></div>" + 
                                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span></div>"+
                                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
            }
           else if(bankFolder == 'shbatm_v3'){
               html = "<div style='height:100%;width:250px;border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                        "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                            "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" + 
                                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span></div>"+
                                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>"; 
            } 
           else{
               html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                        "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                            "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" + 
                                "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span></div>"+
                                "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>"; 
            }
           
           return html; 
         } else { 
          if(branchTypeArabicDashboard){
              html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                        "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>"; 
                          if(data.branchType.toUpperCase() == 'GENTS'){
                          html +=  "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:115px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: "+branchTypeArabicDashboard+"</span></div>";
                          } else {
                          html +=  "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:48px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: "+branchTypeArabicDashboard+"</span></div>";  
                          }
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
          } else {
             html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                        "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>" + 
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Branch Type:</span>&nbsp;<span class='title'>" + this.getBranchType(data) + "</span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
          }
          
           return html;    
         }    
        
    };
    
    this.createContentNcbAtm = function(data){
        
        if(data.sqm > 0) {
            var sqm_data = data.sqm
        } else {
            var sqm_data = 'NA';
        }
        if(data.location){
            var location = data.location;
            var latLong = location.split(",");
            var lat     = latLong[0];
            var long    = latLong[1];
        }
        
         var htmlNcbAtm = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;margin-top:20px;/*overflow: hidden !important;*/'>";
             htmlNcbAtm += "<div align='center' class='title'><b>ATM Code:  "+data.branchCode+"</b></div>";
        
             htmlNcbAtm +='<div style="top: -7px;position:relative;margin-left: 3px;" class="box span12 clearfix">';
             htmlNcbAtm +='<ul class="dashboard-list"><li class=""><ul class="dashboard-sublist">';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">ATM Name</a></div><div><b style="color:#3E576F;font-size:14px;">'+ data.branchName +'</b></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">Province</a></div><div><b style="color:#3E576F;font-size:14px;">'+ data.province +'</b></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">City</a></div><div><b style="color: #3E576F;font-size:14px;">'+ data.city +'</b></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">Category</a></div><div><b style="color: #3E576F;font-size:14px;">'+ data.city_category +'</b></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">Geocode</a></div><div><div><b style="color: #3E576F;font-size:14px;">Lat:&nbsp&nbsp&nbsp&nbsp;'+ lat +'</b></div><div style="padding-left: 140px;"><b style="color: #3E576F;font-size:14px;">Long:&nbsp;'+ long +'</b></div></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">Lnction</a></div><div><b style="color: #3E576F;font-size:14px;">'+ (data.function=== undefined)? '': data.function +'</b></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">Plaocation</a></div><div><b style="color: #3E576F;font-size:14px;">'+ (data.locations=== undefined)? '': data.locations  +'</b></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">Fuce</a></div><div><b style="color: #3E576F;font-size:14px;">'+ (data.place === undefined)? '': data.place  +'</b></div></li>';
             htmlNcbAtm +='<li><a href="#"><div  style="float:left;margin-left: 5px;width:30%;font-size:14px;text-align:left;color:#808080;">SQM</a></div><div><b style="color: #3E576F;font-size:14px;">'+ sqm_data +'</b></div></li>';
             htmlNcbAtm +='</ul>';
             htmlNcbAtm +='</li>';
             htmlNcbAtm +='</ul>';
             htmlNcbAtm += '</div>';               
             htmlNcbAtm += "<div style='height:165px;width:361px;'><div>";

             htmlNcbAtm  += "</div></div>"+
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
           return htmlNcbAtm;    
         
    };
    
    this.createContentRbAtm = function(data){
         var htmlRbAtm = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>";
             htmlRbAtm += "<div align='center' class='title'><b>ATM Code:  "+data.branchCode+"</b></div>";
        
             htmlRbAtm +='<div style="top: -7px;position:relative;margin-left: 3px;" class="box span12">';
             
             htmlRbAtm +='<ul class="dashboard-list" style="height:100px;"><li class=""><ul class="dashboard-sublist">';
             htmlRbAtm +='<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Type of Branch</span></a><b style="color: #3E576F;font-size:14px;">'+ 
             ((data.type_of_branch_no) ? data.branchType : 'NA' )+ '</b></li>';
             htmlRbAtm +='<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Category</span></a><b style="color: #3E576F;font-size:14px;">'+
             ((data.branch_type) ? data.atm_category : 'NA' )+'</b></li>';
             htmlRbAtm +='<li class= "last"><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Location</span></a><b style="color: #3E576F;font-size:14px;">'+
             ((data.locations) ? data.locations : 'NA' )+'</b></li>';                                    
             htmlRbAtm +='</ul>';
             htmlRbAtm +='</li>';
             htmlRbAtm +='</ul>';
             htmlRbAtm += '</div>';               
             htmlRbAtm += "<div style='height:165px;width:361px;'><div>";
                             
             htmlRbAtm  += "</div></div>"+
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
           return htmlRbAtm;    
         
    };
    
    this.rbSupportInfo = function(data){
         var htmlRbAtm = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>";
             htmlRbAtm += "<div align='center' class='title'><b>ATM Code:  "+data.branchCode+"</b></div>";
        
             htmlRbAtm +='<div style="top: -7px;position:relative;margin-left: 3px;" class="box span12">';
             htmlRbAtm +='<ul class="dashboard-list" style="height:100px;"><li class=""><ul class="dashboard-sublist">';
             htmlRbAtm +='<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Mobile No.</span></a><b style="color: #3E576F;font-size:14px;">'+
             ((data.mobile_no) ? data.mobile_no : 'NA' ) +'</b></li>';
             htmlRbAtm +='<li class= "last"><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Team</span></a><b style="color: #3E576F;font-size:14px;">'+
             ((data.team) ? data.team : 'NA' ) +'</b></li>';                                    
             htmlRbAtm +='</ul>';
             htmlRbAtm +='</li>';
             htmlRbAtm +='</ul>';
             htmlRbAtm += '</div>';               
             htmlRbAtm += "<div style='height:165px;width:361px;'><div>";
                             
             htmlRbAtm  += "</div></div>"+
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span>&nbsp;<span class='title'></span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
           return htmlRbAtm;    
         
    };
    this.createContentRC = function(data){
         var genderIndicator;
         var gentsExist = false;
         var finalBranchName;
      
        if(data.type == 'ATM'){
           html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                        "<div style='height:0px;width:220px;'></div>" + 
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'></span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
           return html; 
         } else { 
          if(branchTypeArabicDashboard){
              html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                        "<div style='height:165px;width:220px;'><div><img src='" + this.validateImage(data.image) + "' style='height:160px;width:235px;'/></div></div>"; 
                          if(data.branchType.toUpperCase() == 'GENTS'){
                          html +=  "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:115px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: "+branchTypeArabicDashboard+"</span></div>";
                          } else {
                          html +=  "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='title' style='margin-left:48px;'>" + this.getBranchTypeArabic(data) + "</span>&nbsp;<span class='nameTitle' style='font-size:15px;float:right;margin-right:-85px;'>: "+branchTypeArabicDashboard+"</span></div>";  
                          }
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
          } else {
             html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>"+data.branchName+"("+data.branchCode+")</b></div>"+
                        "<div style='height: 207px;width: 220px;'><div style='padding-left:7px;'><img src='" + this.validateImage(data.image) + "' style='height: 204px;width: 377px;'/></div></div>" + 
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div><span class='nameTitle' style='font-size:15px;'>Branch Type:</span>&nbsp;<span class='title'>" + this.getBranchType(data) + "</span></div>"+
                            "<div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div>";
          }
          
           return html;    
         }    
    };
    
    
    function tabInfo1(data){
     if(data.branch_manager == '' || data.branch_manager=='undefined' || data.branch_manager=='null'){
        $managerName = 'N/A';
      } else {
        $managerName = data.branch_manager;
      }
      
      var html1 = '';
             html1 = '<div style="top:5px;position:relative;" class="box span12">';
             html1 +=        '<ul class="dashboard-list" style="height:256px;"><li class=""><ul class="dashboard-sublist">';
             html1 +=              '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Name</span></a><b style="color:#3E576F;font-size:14px;">'+ data.branchName +'</b></li>';
             html1 +=                       '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Code</span></a><b style="color:#3E576F;font-size:14px;">'+ data.branchCode +'</b></li>';
             html1 +=                       '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Manager Name</span></a><b style="color: #3E576F;font-size:14px;">'+ $managerName +'</b></li>';
             html1 +=                       '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Center Region</span></a><b style="color: #3E576F;font-size:14px;">'+ data.province +'</b></li>';
             html1 +=                      '<li><a href="#"><span  style="width:42%;font-size:14px;text-align:left;color:#808080;">Center City</span></a><b style="color: #3E576F;font-size:14px;">'+ data.city +'</b></li>';
             html1 +=                      '<li><a href="#"><span style="width:42%;font-size:14px;text-align:left;color:#808080;">Other Services</span></a><b style="color:#3E576F;font-size:14px;">'+ data.services_availiable +'</b></li>';
             html1 +=                '</ul>';
             html1 +=                '</li>';
             html1 +=            '</ul>';
             html1 +=       '</div>';
     
     return html1;
          
    }
    
    function ncbImageInfo(data){
        if(bankFolder == 'ncbindependent' || bankFolder == 'ncbatm'){
            $image = data.image;
        }else{
            $image = data.atmImage;
        }
        

      html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" +
                    "<div align='center' class='title'><b>ATM Code&nbsp;:&nbsp;" +data.branchCode+ "</b></div>"+
                        "<div style='height:249px;width: 220px;'><div style='padding-left:7px; margin-left:30px'><img src="+
                        (($image) ? $image : 'https://4c360.com/4c-dashboard/public/img/NoImage.jpg' ) +" style='height: 245px;width: 375px;'/></div></div>" +
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div></div>"+
                            "<div></div></div>";
     return html;      
    
    }

    function rbImageInfo(data){
      
     var html = "<div style='border:2px solid white;background:white;padding:1px;border-radius:5px;'>" + 
                    "<div align='center' class='title'><b>ATM Code&nbsp;:&nbsp;" +data.branchCode+ "</b></div>"+
                        "<div style='height:214px;width: 100px;'><div style='padding-left:7px;'><img src="+
                        ((data.image) ? data.image : 'https://4c360.com/4c-dashboard/public/img/NoImage.jpg' ) +" style='height: 204px;width: 377px;'/></div></div>" +
                            "<div style='width:240px;line-height:10px;margin-top:10px;'><div></div>"+
                            "<div></div></div>";
     return html;      
    
    }

    this.getBranchType = function(data){
       var html = '';
       if(data.type == 'ATM'){
            html += "<div style='height:40px;width:100%'>";
            html += "<div class='title' align='center'>ATM Code&nbsp;:&nbsp;" +data.branchCode+ "&nbsp;</div>";        
            return html;
       } else {
        switch(data.branchType.toUpperCase()){
             case 'LADIES'  : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title="Ladies Branch">Ladies</span>&nbsp;<img title="Ladies Branch" src="'+baseUrl+'public/img/ladies.png"></a>'; break;
             case 'GENTS'   : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="Men Branch">Gents</span>&nbsp;<img title="Men Branch" src="'+baseUrl+'public/img/men.png"></a>'; break;
             default        : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Men"><span style="font-size:15px;background-color:none;" class="title" title="Men Branch">Gents</span>&nbsp;<img title="Men Branch" src="'+baseUrl+'public/img/men.png"></a>';
                              html += '&nbsp;&nbsp;<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Ladies"><span style="font-size:15px;background-color:none;" class="title" title="Ladies Branch">Ladies</span>&nbsp;<img title="Ladies Branch" src="'+baseUrl+'public/img/ladies.png"></a>';  
          } 
         return html;  
       } 
    };
    
    this.getBranchTypeArabic = function(data){
       var html = '';
       if(data.type == 'ATM'){
            html += "<div style='height:40px;width:100%'>";
            html += "<div class='title' align='center'>ATM Code&nbsp;:&nbsp;" +data.branchCode+ "&nbsp;</div>";        
            return html;
       } else {
      
        switch(data.branchType.toUpperCase()){
             case 'LADIES'  : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Ladies"><img title="Ladies Branch" src="'+baseUrl+'public/img/ladies.png">&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="'+ladiesArabic+' '+branchArabic+'">'+ladiesArabic+'</span></a>'; break;
             case 'GENTS'   : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Men"><img title="Men Branch" src="'+baseUrl+'public/img/men.png">&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="'+gentsArabic+' '+branchArabic+'">'+gentsArabic+'</span></a>'; break;
             default        : html += '<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Men"><img title="Men Branch" src="'+baseUrl+'public/img/men.png">&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="'+gentsArabic+' '+branchArabic+'">'+gentsArabic+'</span></a>';
                              html += '&nbsp<a target="_blank" href="index.php?code='+data.branchCode+'&amp;type=Ladies"><img title="Ladies Branch" src="'+baseUrl+'public/img/ladies.png">&nbsp;&nbsp;<span style="font-size:15px;background-color:none;" class="title" title="'+ladiesArabic+' '+branchArabic+'">'+ladiesArabic+'</span></a>';  
          } 

         return html;  
       } 
    };
    
    this.validateImage = function($img){
        if($img == 'null' || $img == undefined || $img == ''){
            if(bankFolder == 'arbatm')
                return baseUrl + 'public/img/AlRajhiProfileImage.png'
            return baseUrl + 'public/img/img_issue_dummy.png'
        }
        if($original_bank_name == 'Demo Bank') {
            var branchImg = baseUrl + 'public/img/x_image.png';
            
            return branchImg;
        }
        var branchImg = baseUrl + 'public/img/preview.png';
        if ($img !== '' || $img != undefined)
            branchImg = $img;
        return branchImg;
    };
    
    this.clearMarker = function(){
        if(this.markerArray){
            for(var markerArrayIndex in this.markerArray){
                this.markerArray[markerArrayIndex].setMap(null);
            }
        }
        this.markerArray = [];
    };
    
    this.createIcon = function(type){
        var imagePosition = (type == 'atm') ? 1 : 0;
        return new google.maps.MarkerImage(markerIcons[imagePosition].image, null, null, null, new google.maps.Size(markerIcons[imagePosition].width, markerIcons[imagePosition].height));
    };
    
    this.createCluster = function(){
        if (this.cluster == undefined) { 
            this.cluster = new MarkerClusterer(this.map, this.markerArray,this.clusterOptions);
        } else { 
            this.cluster.addMarkers(this.markerArray, false); 
        }
    };
    
    this.clearCluster = function(){
        if((this.cluster != undefined)&&(this.cluster)) {
            this.cluster.clearMarkers();
            this.cluster = null;
        }
    };
    
    this.getSpace = function($data){
        if(Number($data) <= 9){
            return "&nbsp;&nbsp;&nbsp;"
        }
        if(Number($data) <= 99){
            return "&nbsp;&nbsp;"
        }
        return "&nbsp;"
       
    };
    
    
  if(bankFolder != "ncbatm" && bankFolder != "desktopapp" && bankFolder != "ncbindependent" && bankFolder != "arbatm" ){

    this.createList = function(type){
       
        $('#scrollbar_'+this.mapType+' .scrollbar').show();
         $('#scrollbar_atm' .scrollbar).show();
        $content = "<ul class='dashboard-sublist'>";
        $contentATM = "<ul class='dashboard-sublist'>";
        var markerInfo = this.markerInfo;
        var liCount = 0;
        var liCountATM = 0;
         
          for(var markerInfoIndex in markerInfo){
            liCount++;
            if(!markerInfo[markerInfoIndex][0]['branchName']){
                markerInfo[markerInfoIndex][0]['branchName'] = ""; 
            }
           if(markerInfo[markerInfoIndex][0]['type'] == 'ATM'){
                liCountATM++;
                if(bankFolder == 'samaatm'){
                    $contentATM += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:36px;padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:62%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #706D6D;overflow:hidden;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchCode']+'   '+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                           ''+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=" target="_blank">';   
                                console.log(markerInfo[markerInfoIndex][0]['branchCode']+" => "+markerInfo[markerInfoIndex][0]['atmScore']);
                             if(markerInfo[markerInfoIndex][0]['atmScore']<98){
                                 $contentATM += '<b class="btn btn-round btn-danger pull-right" style = "margin: 8px !important;">'+(markerInfo[markerInfoIndex][0]['atmScore'])+'</b></a>';
                             }else{
                                 $contentATM += '<b class="btn btn-round btn-success pull-right" style = "margin: 8px !important;">'+(markerInfo[markerInfoIndex][0]['atmScore'])+'</b></a>';
                             }
                       $contentATM += '<div style="margin-top: -2px;margin-left: 37px;"><span class="label label-'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ?"success" : "important")+'" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA")+'</span></div></li>';
               }else{
                   if((markerInfo[markerInfoIndex][0]['branchName'])){
                       $branchName = markerInfo[markerInfoIndex][0]['branchName'].substring(0,40);
                       $textTransform='lowercase';
                   }else {
                       $branchName = "N/A";
                       $textTransform = 'uppercase';
                   }
                    
                    $contentATM += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=" target="_blank"><span style=" color:#4183C4;width:73%;text-align:left;font-size:12px; text-transform:'+ $textTransform +' ;margin-left:10px;" class="blue"><b>'+ $branchName +'</b></span></a>'+
                                 '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+    
                               '<div style="margin-top: -2px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA")+'</span></div></li>';
                }
                
           } else {
           
         if(markerInfo[markerInfoIndex][0]['branchType']){
            switch((markerInfo[markerInfoIndex][0]['branchType']).toUpperCase()){
                    case 'LADIES':
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:'+$listWidth+';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                       '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="'+baseUrl+'public/img/ladies.png"  title="Ladies Branch"/></a>'+    
                                    '<div style="margin-top: -2px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA")+'</span></div></li>';
                                     break;
                     case 'GENTS':
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:'+$listWidth+';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                      '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><img src="'+baseUrl+'public/img/men.png"   title="Men Branch"/></a>'+    
                                    '<div style="margin-top: -2px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA")+'</span></div></li>';
                                      break;
                     case 'GENTS AND LADIES':
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:'+$listWidth+';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #4183C4;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                      '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><img src="'+baseUrl+'public/img/men.png"   title="Men Branch"/></a>&nbsp;<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="'+baseUrl+'public/img/ladies.png"  title="Ladies Branch"/></a>'+    
                                    '<div style="margin-top: -2px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA")+'</span></div></li>';
                                      break;
                     default:
                        $content += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:25px; padding:5px 0px 9px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,40)+'</b></span></a>'+
                                      '<i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+    
                                    '<div style="margin-top: -2px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "NA")+'</span></div></li>';
                                      break;
                  } 
               
                } 
             }
          }
           
          
          if(!liCount){
            $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
             if(this.mapType=='atm'){
                 $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
             }
          } 

          $content += "</ul>"; 
          $contentATM += "</ul>"; 
          $("#result_counter_"+this.mapType).html((this.markerInfo).length);
          $("#overview_"+this.mapType).html($content);
          
          if(this.mapType=='atm'){
            $("#overview_"+this.mapType).html($contentATM);
          }
          
         initializeScrollbar(this.mapType);
         
         if(this.mapType == 'atm'){
             if(liCountATM>18){
               $('#scrollbar_atm .scrollbar').show();
             }

         } else {
             if(liCount>18){
               $('#scrollbar_'+this.mapType+' .scrollbar').show();
                $('#scrollbar_atm' .scrollbar).show();
             }

         }
       }
    } else if (bankFolder == "arbatm"){
      this.createList = function(type){

          $('#scrollbar_'+this.mapType+' .scrollbar').show();
          $('#scrollbar_atm' .scrollbar).show();
          $content = "<ul class='dashboard-sublist'>";
          var markerInfo = this.markerInfo;
          var liCount = 0;
          var liCountATM = 0;


          for(var markerInfoIndex in markerInfo){
              liCount++;
              if(!markerInfo[markerInfoIndex][0]['branchName']){
                  markerInfo[markerInfoIndex][0]['branchName'] = "";
              }



              $outletType = (markerInfo[markerInfoIndex][0]['branchType']) ? (markerInfo[markerInfoIndex][0]['branchType']).toUpperCase() : '' ;
              if(markerInfo[markerInfoIndex][0]['atmScore']>=91){
                  $scoreColorClass ='manColorRoundGreaterClass';
              }else if(markerInfo[markerInfoIndex][0]['atmScore']>=85){
                  $scoreColorClass ='manColorRoundMediumClass';
              }else{
                  $scoreColorClass='manColorRoundSmallerClass';
              }
              var $branchName=(((markerInfo[markerInfoIndex][0]['branchName']).trim()).length >30) ? ((markerInfo[markerInfoIndex][0]['branchName']).trim()).substr(0,30)+'...'  : (markerInfo[markerInfoIndex][0]['branchName']).trim() ;

              $closed='';
              $closedStyle='';
              if(markerInfo[markerInfoIndex][0]['is_active']=='0'){
                  $closed = '<span class="closed"><b> Outlet is closed now.</b></span>';
                  $closed='';
                  $closedStyle='closed-pstyle';
              }
              if($.inArray(markerInfo[markerInfoIndex][0]['branchCode'],$closedOutletduringVisit) !== -1){
                  $closed = '<span class="closed"><b> Outlet is closed during visit.</b></span>';
                  $closed='';
                  $closedStyle='visit-closed-pstyle';
              }
              $content += '<li class="'+$closedStyle+'" id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="position:relative;height:36px; padding-bottom:20px;">'+$closed+'<span class="icon icon-blue icon-triangle-e" ></span><a target="_blank" href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&group_name='+ group_name +'&type="><span style=" color:#4183C4;width:65%;text-align:left;font-size:12px;margin-left:10px;" class="blue"><b style="text-transform: lowercase !important;">'+markerInfo[markerInfoIndex][0]['branchCode']+'</b>&nbsp;&nbsp;-&nbsp;<b style="text-transform: lowercase !important;" title="'+$branchName+'">'+ $branchName +', '+(markerInfo[markerInfoIndex][0]['city'])+'</b></span></a>';


              if($outletType){
                  switch(markerInfo[markerInfoIndex][0]['branchType'].toUpperCase()){
                      case 'ISLAND TYPE ATM':
                          if(markerInfo[markerInfoIndex][0]['lastVisitDate']  === undefined  && markerInfo[markerInfoIndex][0]['atmScore'] ==0) {
                              $content += '<span class="' + $scoreColorClass + '"><b>' +'--'+ '</b></span>';
                          }
                          else{
                              $content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';
                          }
                          break;
                      case 'ROOM TYPE ATM':
                          console.log(markerInfo);
                          //$content += '<span class="label label-inverse">'+markerInfo[markerInfoIndex][0]['atmScore']+'</span>';break;
                          //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';break;
                          if(markerInfo[markerInfoIndex][0]['lastVisitDate']  === undefined  && markerInfo[markerInfoIndex][0]['atmScore'] ==0) {
                              $content += '<span class="' + $scoreColorClass + '"><b>' +'--'+ '</b></span>';
                          }
                          else{
                              $content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';
                          }
                          break;
                      case 'ROOM OLD TYPE ATM':
                          //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';break;
                          if( markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined && markerInfo[markerInfoIndex][0]['atmScore'] ==0) {
                              $content += '<span class="' + $scoreColorClass + '"><b>' +'--'+ '</b></span>';
                          }
                          else{
                              $content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';
                          }
                          break;
                      case 'KIOSK TYPE ATM':
                          //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';break;
                          if( markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined  && markerInfo[markerInfoIndex][0]['atmScore'] ==0) {
                              $content += '<span class="' + $scoreColorClass + '"><b>' +'--'+ '</b></span>';
                          }
                          else{
                              $content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';
                          }
                          break;
                      case 'LOBBY TYPE ATM':
                          //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';break;
                          if( markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined  && markerInfo[markerInfoIndex][0]['atmScore'] ==0) {
                              $content += '<span class="' + $scoreColorClass + '"><b>' +'--'+ '</b></span>';
                          }
                          else{
                              $content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';
                          }
                          break;
                      case 'WINDOW TYPE ATM':
                          //$content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';break;
                          if( markerInfo[markerInfoIndex][0]['lastVisitDate'] === undefined  && markerInfo[markerInfoIndex][0]['atmScore'] ==0) {
                              $content += '<span class="' + $scoreColorClass + '"><b>' +'--'+ '</b></span>';
                          }
                          else{
                              $content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';
                          }
                          break;
                      default:
                          $content += '<span class="'+$scoreColorClass+'"><b>'+markerInfo[markerInfoIndex][0]['atmScore']+'%</b></span>';break;
                  }
              }
              if(markerInfo[markerInfoIndex][0]['lastVisitDate'] && markerInfo[markerInfoIndex][0]['lastVisitDate'] != "NA")
                  $content +=   '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-success" style="color:#FFFFFF;">'+ markerInfo[markerInfoIndex][0]['lastVisitDate'] +'</span></div></li>';
              else
                  $content +=   '<div style="margin-top: -10px;margin-left: 37px;"><span class="label label-danger" style="background-color: #DC3333;">No Audit</span></div></li>';
          }



          if(!liCount){
              $content += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';

          }

          $content += "</ul>";
          $("#result_counter_"+this.mapType).html((this.markerInfo).length);
          $("#overview_"+this.mapType).html($content);

          if(this.mapType=='atm'){
              $("#overview_"+this.mapType).html($content);
          }

          initializeScrollbar(this.mapType);

          if(this.mapType == 'atm'){
              if(liCountATM>18){
                  $('#scrollbar_atm .scrollbar').show();
              }

          } else {
              if(liCount>18){
                  $('#scrollbar_'+this.mapType+' .scrollbar').show();
                  $('#scrollbar_atm' .scrollbar).show();
              }

          }
      }
  } else{
    
      this.createList = function(type){
       
        $('#scrollbar_'+this.mapType+' .scrollbar').show();
        $('#scrollbar_atm' .scrollbar).show();
        $contentATM = "<ul class='dashboard-sublist'>";
        var markerInfo = this.markerInfo;
      
        var liCount    = 0;
        var liCountATM = 0;
        var firstFlag  = 0; 
         
        for(var markerInfoIndex in markerInfo){
            liCount++;
            if(bankFolder == "ncbatm" && markerInfo[markerInfoIndex][0]['atmScore'] == 0)
                markerInfo[markerInfoIndex][0]['atmScore'] = 100;
            if(!markerInfo[markerInfoIndex][0]['branchName']){
                markerInfo[markerInfoIndex][0]['branchName'] = ""; 
            }
            $lastVistColor = 'success';
            if(typeof(markerInfo[markerInfoIndex][0]['lastVisitDate']) == 'undefined' || markerInfo[markerInfoIndex][0]['lastVisitDate'] == 'NA'){
                $lastVistColor = 'important';
                markerInfo[markerInfoIndex][0]['lastVisitDate'] = "No Audit";
            }
                
               $menColorClass =(markerInfo[markerInfoIndex][0]['atmScore']>=98)?'manColorRoundGreaterClass':'manColorRoundSmallerClass';
            var profileType='ATM';
            if(bankFolder=='desktopapp'){
                profileType='';
               }
            var $lastVisitLabel='<div style="margin-top: -16px;margin-left: 37px;"><span class="label label-'+$lastVistColor+'" style="color:#FFFFFF;">'+((markerInfo[markerInfoIndex][0]['lastVisitDate']) ? markerInfo[markerInfoIndex][0]['lastVisitDate'] : "Survey is yet to be performed")+'</span></div>';
           
            if(bankFolder=='desktopapp' || bankFolder=='ncbindependent' ){
                profileType="";
                if(!markerInfo[markerInfoIndex][0]['lastVisitDate']){
                    $lastVisitLabel="";

                }   
                
              if(clientType == 'v6publicaudits' || clientType == 'v6manualaudits'){
                 $customUrl = baseUrl+"ncbatm_v6/index.php?code="+markerInfo[markerInfoIndex][0]['branchCode']+"&type=ATM";
               } else {
                $customUrl = baseUrl+"ncbatm_v7/index.php?code="+markerInfo[markerInfoIndex][0]['branchCode']+"&type=ATM";
              }  
                
            } else {
                
                $customUrl = "index.php?code="+markerInfo[markerInfoIndex][0]['branchCode']+"&type="+profileType+"";
            }
           if(!markerInfo[markerInfoIndex][0]['city']){
               markerInfo[markerInfoIndex][0]['city'] = 'N/A';
           }
           $contentATM += '<li id="bankCode_'+markerInfo[markerInfoIndex][0]['branchCode']+'" style="height:38px; padding:0px 0px 12px 0px;"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:'+$listWidth+';text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px; color: #706D6D;" class="blue"><b>'+markerInfo[markerInfoIndex][0]['branchCode']+'  '+markerInfo[markerInfoIndex][0]['city']+'</b></span></a>'+
                          ''+this.getSpace(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="'+$customUrl+'" target="_blank"><b style="margin-left:-25px"; class="'+$menColorClass+'">'+markerInfo[markerInfoIndex][0]['atmScore']+'</b></a>&nbsp;'+
                        $lastVisitLabel+'</li>';
        }
            
         if(!liCount){
            $contentATM += '<li><div style="width:100%;font-weight:bold;color:#808080;" align="center">Nothing Found</div></li>';
             
          }
          
          $contentATM += "</ul>"; 
        
          $("#result_counter_"+this.mapType).html((this.markerInfo).length);
          $("#overview_"+this.mapType).html($contentATM);
          $("#overview_"+this.mapType).html($contentATM);
          
          initializeScrollbar(this.mapType);
       
         
         if(this.mapType == 'atm'){
             if(liCountATM>18){
               $('#scrollbar_atm .scrollbar').show();
             }

         } else {
             if(liCount>18){
               $('#scrollbar_'+this.mapType+' .scrollbar').show();
                $('#scrollbar_atm' .scrollbar).show();
             }

         }
       }
   }
   
   function initializeScrollbar($type){
        //$(document).ready(function(){
            $(".nano").nanoScroller({ alwaysVisible: true });
        //});
   }
   
   function calculateChar(mainStr){
       if((String(mainStr).split("")).length==1){
         return '&nbsp;&nbsp;&nbsp;&nbsp;';
        }else{
       if((String(mainStr).split("")).length==2)
         return '&nbsp;&nbsp;';
       else
         return '&nbsp;';
      } 
   }
    
   var firstTime = 0; 
   var flagAgent = false; 
   
   this.createBranchReportList = function(type){
        if(firstTime == 0){

            if(bankFolder =='arbatm'){
                var markerInfo = $.makeArray(this.reportInfo);
            }else{
                var markerInfo = this.markerInfo;        
            }
        
        if(markerInfo){
        var addressBranch;   
        $reportContent = '<table id="branch-list-box-table" style="width:100%;padding-top: 5px;" class="table table-bordered"><tr>';  
        $rowCounter = 1;  
        //var markerInfo = this.markerInfo;
        
        if(bankFolder != 'rbatm'){
            var markerInfo = this.reportInfo;    
        }
        
          //console.log(markerInfo);
          for(var markerInfoIndex in markerInfo){
            
            if(markerInfo[markerInfoIndex][0]['name'] == 'Al-Rajhi Bank'){
              markerInfo[markerInfoIndex][0]['name'] = 'arb';
            }
            
            if(markerInfo[markerInfoIndex][0]['name'] == 'National Commercial Bank'){
              markerInfo[markerInfoIndex][0]['name'] = 'ncbatm';
              markerInfo[markerInfoIndex][0]['type'] = "ATM";
            }
            
            if(bankFolder=='rbatm'){ 
                if(markerInfo[markerInfoIndex][0]['name'] == 'Riyad Bank'){
                  markerInfo[markerInfoIndex][0]['name'] = 'rbatm';
                }
            }
              if(bankFolder=='bajatm'){
                if(markerInfo[markerInfoIndex][0]['name'] == 'Bank AL Jazira'){
                  markerInfo[markerInfoIndex][0]['name'] = 'bajatm';
                    markerInfo[markerInfoIndex][0]['type'] = "ATM";
                }
            }
            
            if(bankFolder=='bsfatm'){
                if(markerInfo[markerInfoIndex][0]['name'] == 'Banque Saudi Fransi'){
                  markerInfo[markerInfoIndex][0]['name'] = 'bsfatm';
                    markerInfo[markerInfoIndex][0]['type'] = "ATM";
                }
            }
            
            if(bankFolder=='riyad'){
                if(markerInfo[markerInfoIndex][0]['name'] == 'Riyad Bank'){
                  markerInfo[markerInfoIndex][0]['name'] = 'riyad';
                }
            }
            if(bankFolder=='samaatm'){
                if(markerInfo[markerInfoIndex][0]['name'] == 'SAMA Bank'){
                  markerInfo[markerInfoIndex][0]['name'] = 'samaatm';
                    markerInfo[markerInfoIndex][0]['type'] = "ATM";
                }
            }
            
            if(!markerInfo[markerInfoIndex][0]['branchName']){
                markerInfo[markerInfoIndex][0]['branchName'] = ""; 
            }
            
            if(baseBank == 'ncbatm_v5'){
                markerInfo[markerInfoIndex][0]['name'] = 'ncbatm_v5';
                markerInfo[markerInfoIndex][0]['type'] = "ATM";
            }
            //console.log(markerInfo[markerInfoIndex][0]);
             if($rowCounter>4){
                $rowCounter = 1;
                $reportContent += '</tr><tr>';
             }            
             //console.log(markerInfo[markerInfoIndex][0]['branch_type']);
             //console.log(markerInfo);
            switch(markerInfo[markerInfoIndex][0]['type']){
                    case 'Ladies':
                        
                            $rowCounter++;
                            flagAgent = true;
                           
                           /* $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                              '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\'riyad\')"></span></td>';
                              break; */
                            
                            $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                            '<br /><br /><span style="margin-left: 22px;">  <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                            '<!--<input type="checkbox" class="branchBasketReportClass" name="branchBasketReport" branchType="Ladies" value="'+markerInfo[markerInfoIndex][0]['branchCode']+'"> -->' +
                            ' <i><a style="text-decoration: underline;" href="index.php?code=' + markerInfo[markerInfoIndex][0]['branchCode']+ '&type=ladies" target="_blank">'+markerInfo[markerInfoIndex][0]['branchCode']+'</a></i>'+markerInfo[markerInfoIndex][0]['branchCode']+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="'+markerInfo[markerInfoIndex][0]['branchCode']+'" branch-type="Ladies" for-branch-report="true" style="cursor:pointer"></span></td>';
                            break;
                        
                    case 'ATM':
                            $rowCounter++; 
                            flagAgent = true;
                            /* $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                              '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\'riyad\')"></span></td>';
                              break;*/
                            $branchCode = markerInfo[markerInfoIndex][0]['branchCode'];
                            
                            if(markerInfo[markerInfoIndex][0]['name'] == 'ncbatm'){
                                $reportContent += '<td class="branchCode-'+$branchCode+'" area="'+markerInfo[markerInfoIndex][0]['area_name']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"></a>'+
                                ' <span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                    '<!--<input type="checkbox" class="branchBasketReportClass" branchType="ATM" name="branchBasketReport" value="'+markerInfo[markerInfoIndex][0]['branchCode']+'"> -->' +
                                    '<i><a style="text-decoration: underline;" href="index.php?code=' + $branchCode + '&type=ATM" target="_blank">'+$branchCode+'</a></i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=ATM" target="_blank" style="margin-right: 3px;"></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\''+$branchCode+'\',\'ATM\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="'+$branchCode+'" branch-type="ATM" for-branch-report="true" style="cursor:pointer"></span></td>';
                                break;
                                
                            }else if(bankFolder == 'arbatm'){

                                $reportContent += '<td class="branchCode-'+$branchCode+'" zone="'+markerInfo[markerInfoIndex][0]['zone_id']+'" area="'+markerInfo[markerInfoIndex][0]['area_id']+ '"branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type'] +'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                                '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                    '<!--<input type="checkbox" class="branchBasketReportClass" branchType="ATM" name="branchBasketReport" value="'+markerInfo[markerInfoIndex][0]['branchCode']+'">-->' +
                                    ' <i><a style="text-decoration: underline;" href="index.php?code=' + $branchCode + '&type=ATM" target="_blank">'+$branchCode+'</a></i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=ATM" target="_blank" style="margin-right: 3px;"></a>'+
                                '<span class="fa fa-file-pdf-o fa-lg" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\''+$branchCode+'\',\'ATM\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\',\'pdf\')"></span>&nbsp;&nbsp;'+
                                '<span class="fa fa-file-excel-o fa-lg" title="Download Profile Excel"  style="cursor:pointer;" onclick="downloadProfileExcel(\''+$branchCode+'\',\'ATM\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\',\'excel\')"></span>'+
                                //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="'+$branchCode+'" branch-type="ATM" for-branch-report="true" style="cursor:pointer"></span>
                                '</td>';
                                break;

                            }else{
                                
                                $reportContent += '<td class="branchCode-'+$branchCode+'" zone="'+markerInfo[markerInfoIndex][0]['zone_id']+'" area="'+markerInfo[markerInfoIndex][0]['area_id']+ '"branch_type_arb="'+markerInfo[markerInfoIndex][0]['branch_type'] +'">' +
                                '<span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+
                                '</span></a>'+
                                '<br /><br /><span style="margin-left: 22px;">  <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                '<!--<input type="checkbox" disabled class="branchBasketReportClass" branchType="ATM" name="branchBasketReport" value="'+markerInfo[markerInfoIndex][0]['branchCode']+'"> <i> -->' +
                                '<a style="text-decoration: underline;" href="index.php?code=' + $branchCode + '&type=ATM" target="_blank">'+$branchCode+'</a>' +
                                '</i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=ATM" target="_blank" style="margin-right: 3px;"></a> ' +
                                '<span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfileExcel(\''+$branchCode+'\',\'ATM\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\',\'pdf\')"></span>' +
                                //'<span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" branch-code="'+$branchCode+'" branch-type="ATM" for-branch-report="true" style="cursor:pointer"></span>' +
                                '</td>';
                                break; 
                            }    
                                                
                    case 'Gents and Ladies':
                            $rowCounter++;
                            flagAgent = true;
                            /*$reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                             '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Ladies\',\'riyad\')"></span></td>';
                             */
                            $branchCode = markerInfo[markerInfoIndex][0]['branchCode'];
                            $reportContent += '<td class="branchCode-'+$branchCode+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:17px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                            '<br /><br /><span style="margin-left: 22px;"> <i class="fa fa-chevron-circle-right fa-lg" style="color:gray;margin-right: 5px;"></i>' +
                                '<!--<input type="checkbox" class="branchBasketReportClass" branchType="Ladies" name="branchBasketReport" value="'+markerInfo[markerInfoIndex][0]['branchCode']+'"> -->' +
                                '<i><a style="text-decoration: underline;" href="index.php?code=' +$branchCode+ '&type=ladies" target="_blank">'+$branchCode+'</a></i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=ladies" target="_blank"><img src="../public/img/ladies.png"  title="Ladies Branch"/></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\''+$branchCode+'\',\'Ladies\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" branch-code="'+$branchCode+'" branch-type="Ladies" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" title="Email" for-branch-report="true" style="cursor:pointer"></span><br/><span style="margin-left: 22px;"><input type="checkbox" class="branchBasketReportClass" branchType="Men" name="branchBasketReport" value="'+markerInfo[markerInfoIndex][0]['branchCode']+'"> <i><a style="text-decoration: underline;" href="index.php?code=' +$branchCode+ '&type=Men" target="_blank">'+$branchCode+'</a></i>'+calculateChar($branchCode)+'<a href="index.php?code='+$branchCode+'&type=Men" target="_blank" style="margin-right: 3px;"><img src="../public/img/men.png"  title="Men Branch"/></a> <span class="icon icon-red icon-pdf" title="Download Profile pdf"  style="cursor:pointer;" onclick="downloadProfilePdf(\''+$branchCode+'\',\'Men\',\''+markerInfo[markerInfoIndex][0]['name']+'\',\'download\')"></span><span id="branchReportEmail" related-box="myModal" class="btn-setting icon icon-red icon-envelope-closed" branch-code="'+$branchCode+'" branch-type="Men" title="Email" for-branch-report="true" style="cursor:pointer"></span></td>';
                            
                            if($rowCounter>4){
                               $rowCounter = 1;
                               $reportContent += '</tr><tr>';
                             }
                            
                            /*$rowCounter++;
                             $reportContent += '<td class="branchCode-'+markerInfo[markerInfoIndex][0]['branchCode']+'"><span class="icon icon-blue icon-triangle-e" ></span><a href="javascript:void()"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+markerInfo[markerInfoIndex][0]['branchName'].substring(0,17)+'</span></a>'+
                             '</td><td><i>'+markerInfo[markerInfoIndex][0]['branchCode']+'</i>'+calculateChar(markerInfo[markerInfoIndex][0]['branchCode'])+'<a href="index.php?code='+markerInfo[markerInfoIndex][0]['branchCode']+'&type=Men" target="_blank"><img src="../public/img/men.png"  title="Ladies Branch"/></a><span class="icon32 icon-green  icon-pdf" title="Download Profile pdf" onclick="downloadProfilePdf(\''+markerInfo[markerInfoIndex][0]['branchCode']+'\',\'Men\',\'riyad\')"></span></td>';
                             break;*/
                    default:
                       break;
              } 
         } 
        $reportContent += "</tr></table>"; 
        if(flagAgent){
          $("#branch-list-box").html($reportContent);
        }
        firstTime = 1;
        flagAgent = false;
       } 
     }
   };
    
    var counter = 0;
    $(document).on('click', '.branchBasketReportClass', function() { 
        
        $this = $('.branchBasketReportClass');
        branchCode = [];
        $('#downloadBranchBasketReportButton').show();
        $('#mailBranchBasketReportButton').show();
        if(counter > 0){
            $("#downloadConsolidatedBranchReportButton").show();
        }
        inputId = $(this).val()+'_'+$(this).attr('branchType');
        inputId1 = $(this).val()+'_1'+$(this).attr('branchType');
        if($(this).is(":checked")) {
            counter++;
            $('#changeAgentTable').css('margin-left','33%');                        
            formContent = '<input type="hidden" name="branches['+$(this).val()+']['+$(this).attr('branchType')+']" value="'+$(this).attr('branchType')+'" id="'+inputId+'">';
            formContent1 = '<input type="hidden" name="branches['+$(this).val()+']['+$(this).attr('branchType')+']" value="'+$(this).attr('branchType')+'" id="'+inputId1+'">';
            $('#downloadBranchBasketReportForm').append(formContent);
            $("#downloadConsolidatedBranchProfileReportForm").append(formContent1);
         } else {
             counter--;  
             $('#'+inputId).remove();
             $('#'+inputId1).remove();
         }
        if(counter == 0){
            $('#changeAgentTable').css('margin-left','63%');
        }
        if(counter == 1){
            $("#downloadConsolidatedBranchReportButton").hide();
        }
        
        $checkBranchReportCheckbox = false;
        $.each($this,function(i,v) {
            if($(this).is(":checked")) {
                $checkBranchReportCheckbox = true;
             }
        });
        if(!$checkBranchReportCheckbox) {
            $('#downloadBranchBasketReportButton').hide();
            $('#mailBranchBasketReportButton').hide();
            $("#downloadConsolidatedBranchReportButton").hide();
        }
        
    });

    this.clearMap = function(){
        this.clearCluster();
        this.clearMarker();
        this.markerInfo = [];
        if($("#toggle_map_"+this.mapType).is(":checked")){
            $("#toggle_map_"+this.mapType).trigger("click");
            $("#toggle_map_"+this.mapType).removeAttr('checked');
            initializeHeatmapSelector(this.mapType);
        }
        
    };
    
    this.createHeatmap = function(heatmapData){
        this.heatmap = new google.maps.visualization.HeatmapLayer({
            data:heatmapData, 
            radius:85
        });
    };
    
    this.addListeners = function(){
        var me = this;
         google.maps.event.addListener(this.map, 'tilesloaded', function (event) { 
              if(first == ""){
                changeIsrailLayer(this.getZoom());
                first = 1;
              }
              if(!previousMapType){
                previousMapType = this.getMapTypeId();
              }else{
                  if(this.getMapTypeId() != previousMapType){
                        changeIsrailLayer(this.getZoom());
                        previousMapType = this.getMapTypeId();
                  }
              }
          });
          
          google.maps.event.addListener(this.map, 'zoom_changed', function (event) {
                $zoomChanged = true;
                changeIsrailLayer(this.getZoom());
                if(this.getZoom() < 5){
                    if(first == 1)
                        this.setZoom(5);
                    first = 2;
                }
          });
          
          google.maps.event.addListener(this.map, 'idle', function () {
            
            if($zoomChanged){
                if(me.heatmap)
                    me.heatmap.setOptions({radius:getNewRadius(this.getZoom())});
                $zoomChanged = false;
            }
            
          });
    };
    this.resizeMap = function() {
        google.maps.event.trigger(this.map, 'resize');
        this.map.setCenter(new google.maps.LatLng(22.268763552489748,47.87841796875));
    }
}
function getReportJson(){
   
    if(bankFolder !='arbatm'){
        return false;
    }
    var url = baseUrl+"public/json/"+baseBank+"_report.json";    
        $.getJSON(url, function(res) {
            var dataToReturn=[];
            $(res['mapdata']).each(function(key, value ){
                  
                 res['mapdata'][key]['atmScore'] = getIndivdualATMDetail(value['branchCode'],'ATM');
                 
                 res['mapdata'][key]['lastVisitDate'] =     getIndivdualBranchVisitDate(value['branchCode']);
                 
                 res['mapdata'][key]['branch_type'] = getIndivdualBranchType(value['branchCode']);  
                 dataToReturn.push($.makeArray(res['mapdata'][key]));
               });
               // console.log('---Result---');
                 //   console.log(res);
                 mapObject.reportInfo= dataToReturn;
                      
        });

}
function callMe(results){
    //console.log($_ALLBRANCHSCORE);
 if(firstMapRequestFlag){
    $("#mapjscontent").html('<script type="text/javascript">'+results.mapfunction+'</script>');
    
    createMarkers(results);
 } else {
        myresults  = JSON.parse(results);
        $(myresults['mapdata']).each(function(key, value ){
          myresults['mapdata'][key]['atmScore'] = getIndivdualATMDetail(value['branchCode'],'ATM');
         
         if(bankFolder== 'bsfatm' || bankFolder== 'arbatm'){
          myresults['mapdata'][key]['atmImage'] = getIndivdualImageDetail(value['branchCode'],'IMAGE');
          
         }
        if(bankFolder=='ncbatm' || bankFolder== 'desktopapp'){
            myresults['mapdata'][key]['atmImage'] = getNcbIndivdualImageDetail(value['branchCode'],'IMAGE');
        }
        
         
         myresults['mapdata'][key]['lastVisitDate'] =     getIndivdualBranchVisitDate(value['branchCode']);

        });
        /* SORTING ON LAST VISIT BASIS */
            sortBranchListByLastVisitDate(myresults);
       /* -----ENDS HERE ----*/
   
    $("#mapjscontent").html('<script type="text/javascript">'+myresults.mapfunction+'</script>');
    setTimeout(function(){createMarkers(JSON.stringify(myresults));},3000);
 }
}

function getIndivdualATMDetail($branchCode,$branchType)
{
    if(typeof $_ALLBRANCHSCORE[$branchCode+'_'+$branchType] =='undefined' || $_ALLBRANCHSCORE[$branchCode+'_'+$branchType] ==''){
    
      $_ALLBRANCHSCORE[$branchCode+'_'+$branchType] = '--';
    
     
   }
   
    return $_ALLBRANCHSCORE[$branchCode+'_'+$branchType];
}

function getIndivdualBranchType($branchCode){
   
    return $_ALLBRANCHSCORE[$branchCode + '_BRANCH_TYPE'];
}

function getIndivdualImageDetail($branchCode,$branchType)
{
     if(typeof $_ALLBRANCHSCORE[$branchCode+'_IMAGE'] =='undefined' || $_ALLBRANCHSCORE[$branchCode+'_IMAGE']=='' || $_ALLBRANCHSCORE[$branchCode+'_IMAGE']=='0')
     {
    
      return baseUrl + 'public/img/NoImage.jpg';
    
     }
   
    return $_ALLBRANCHSCORE[$branchCode+'_IMAGE'];
}

function getNcbIndivdualImageDetail($branchCode,$branchType)
{
     if(typeof $_ALLBRANCHSCORE[$branchCode+'_IMAGE'] =='undefined' || $_ALLBRANCHSCORE[$branchCode+'_IMAGE']=='' || $_ALLBRANCHSCORE[$branchCode+'_IMAGE']=='0')
     {
    
      return baseUrl + 'public/img/NCBProfileImage.png';
    
     }
   
    return $_ALLBRANCHSCORE[$branchCode+'_IMAGE'];
}



function getIndivdualBranchVisitDate($branchCode)
{
    return $_ALLBRANCHSCORE[$branchCode+'_VISITDATE'];
    //return '12/20/2012';
}



/*function createMarkers(results){
    results     = JSON.parse(results);
    if(results.resultType == 'atm')
        mapObject   =  mapObjectAtm;
    else
        mapObject   = mapObjectBranch;
        mapObject.clearMap();
    
    var mapdata     = results.mapdata;
    var heatmapdata = results.heatmapdata;
    
    for(var resultIndex in mapdata){
        mapObject.createMarker(new google.maps.LatLng(mapdata[resultIndex].latitude, mapdata[resultIndex].longitude),results.resultType,mapdata[resultIndex]);
    }
    
    if(!mapObject.heatmap)
        initializeHeatMap(heatmapdata,results.resultType);
        
    mapObject.createCluster();
    mapObject.createList();
    mapObject.createBranchReportList();
    $("#loading_screen_"+(results.resultType).toLowerCase()).hide();
 }*/

function createMapRequest($type,$requestType){
  
  if(firstMapRequestFlag){
    getReportJson();
    
       var url = baseUrl+"public/json/"+baseBank+".json";
  
        $.getJSON(url, function(res) {
            $(res['mapdata']).each(function(key, value ){
                  
                 res['mapdata'][key]['atmScore'] = getIndivdualATMDetail(value['branchCode'],'ATM');
                 
                 if(bankFolder=='ncbatm' || bankFolder== 'desktopapp' || bankFolder == 'bsfatm'){
                  res['mapdata'][key]['atmImage'] = getNcbIndivdualImageDetail(value['branchCode'],'IMAGE');
                 }
                 res['mapdata'][key]['lastVisitDate'] =     getIndivdualBranchVisitDate(value['branchCode']);
                 if(bankFolder== 'arbatm'){
                    res['mapdata'][key]['branch_type'] = getIndivdualBranchType(value['branchCode']);
                 }

                 
               });
               
               
        /* SORTING ON LAST VISIT BASIS */
            sortBranchListByLastVisitDate(res);
       /* -----ENDS HERE ----*/
       
         $("#loading_screen_branch").hide();
          callMe(res)         
        });
     
  } else {
   
    if($type == 'code'){
       $digitRegx = '\[0-9\,]';
       if($("#search_code_"+$requestType).val().match($digitRegx)) {
           
           $("#overview_"+$requestType).html($("#branch_loader").html());
           $requestData = {
                "requestFor"    :   "map-data",
                 "group_name"        :   group_name,
                "code-filters"  :   {
                                        "branchCode"    :  $("#search_code_"+$requestType).val(),
                                        "requestType"   :  $requestType 
                 }
            } 
        }else{
             if(bankFolder == 'bsfatm' || bankFolder == 'arbatm' || bankFolder == 'samaatm'){
             alert("Please provide a valid ATM Code");   
            } else {
             alert("Please provide a valid Branch Code");
            }
             $("#loading_screen_"+$requestType).hide();
             return;
        }
    }else if($type == 'name'){
       $wordRegx = '\[abc ABC\,]';
       if($("#search_name_branch").val().match($wordRegx)) {
           
           $("#overview_"+$requestType).html($("#branch_loader").html());
           $requestData = {
                "requestFor"    :   "map-data",
                "group_name"        :   group_name,
                "name-filters"  :   {
                                        "branchName"    :  $("#search_name_branch").val(),
                                        "requestType"   :  $requestType 
                 }
            } 
        }else{
            if(bankFolder == 'bsfatm'|| bankFolder == 'arbatm' || bankFolder == 'samaatm'){
             alert("Please provide a valid ATM Name");   
            } else {
             alert("Please provide a valid Branch Name");
            }
             $("#loading_screen_"+$requestType).hide();
             return;
        }
      
    } else if(bankFolder=='ncbatm' || bankFolder=='bajatm' || bankFolder== 'bsfatm' || bankFolder == 'samaatm'){


        $("#overview_"+$requestType).html($("#atm_loader").html());
        $requestData = {
        "requestFor"    :   "map-data",
        "map-filters"       :   {
                                "province"          :   $("#province_"+$requestType).val(),
                                "city"              :   $("#city_"+$requestType).val(),
                                "function"          :   $("#function_"+$requestType).val(),
                                "place"             :   $("#place_"+$requestType).val(),
                                "locations"          :   $("#locations_"+$requestType).val(),
                                "requestType"       :   $requestType,
                                "branchType"        :   group_name
                            }
        }
        
    }else if($type=='rbatm_v3'){

        $("#overview_"+$requestType).html($("#branch_loader").html());
        $requestData = {
        "requestFor"    :   "map-data",
        "map-filters"       :   {
                                "province"          :   $("#province_"+$requestType).val(),
                                "city"              :   $("#city_"+$requestType).val(),
                                "function"          :   $("#function_"+$requestType).val(),
                                "place"             :   $("#place_"+$requestType).val(),
                                "locations"          :   $("#locations_"+$requestType).val(),
                                "requestType"       :   $requestType,
                                "branchType"        :   group_name
                            }
        }

    }else if($type=='shbatm_v3'){

        $("#overview_"+$requestType).html($("#branch_loader").html());
        $requestData = {
        "requestFor"    :   "map-data",
        "map-filters"       :   {
                                "province"          :   $("#province_"+$requestType).val(),
                                "city"              :   $("#city_"+$requestType).val(),
                                "function"          :   $("#function_"+$requestType).val(),
                                "place"             :   $("#place_"+$requestType).val(),
                                "locations"          :   $("#locations_"+$requestType).val(),
                                "requestType"       :   $requestType,
                                "branchType"        :   group_name
                            }
        }

    }else{

        $("#overview_"+$requestType).html($("#branch_loader").html());
        $requestData = {
        "requestFor"    :   "map-data",
        "map-filters"       :   {
                                "province"          :   $("#province_"+$requestType).val(),
                                "city"              :   $("#city_"+$requestType).val(),
                                "branchType"        :   titleBox.getValue(),
                                "requestType"       :   $requestType 
                            }
        }
     }
    $requestObject.initialize($requestData,"callMe",null);
  }
}

function fillCityOptions($data){
   mapObject   =  getActiveMapTab();
   // $("#city_"+mapObject.mapType).html($data);
  $("#city_atm").html($data);
}

function fillObservationCityOptions($data){
  $("#city_observation").html($data);
}

function getCityList($dataFor){
    $requestData = {
        "requestFor"    :   "city-data",
        "map-filters"       :   {
                                    "region"        :   $("#province_"+$dataFor).val(),
                                    "requestType"   :   $dataFor
                                }
    };
    if($dataFor =='observation'){

        $requestObject.initialize($requestData,"fillObservationCityOptions",null);
    }else{

        $requestObject.initialize($requestData,"fillCityOptions",null);
    }

}

function resetMapFilter($type){
    $("#province_"+$type).attr('value',"");
    $("#city_"+$type).html("<option value=''>-- Select City --</option>");
    $("#function_"+$type).attr('value',"");
    $("#place_"+$type).attr('value',"");
    $("#locations_"+$type).attr('value',"");
    $("#search_code_"+$type).attr('value','');
    $("#search_name_branch").attr('value','');
    $("#search_name_"+$type).attr('value','');
    titleBox.uncheckAll();
    if($type == 'atm'){
     $("#search_tab_atm").find('li.active').each(function(){
        if($(this).find('a').attr('href') == '#byCodeDivAtm'){
            $("#atmCodeSearchButton").trigger('click');
        }else{
            $("#atmCodeSearchButton").trigger('click');
        }
      })  
    
    } else {
          $("#search_tab_branch").find('li.active').each(function(){
        if($(this).find('a').attr('href') == '#byCodeDiv'){
            $("#branchCodeSearchButton").trigger('click');
        }else{
            $("#branchCodeRegionButton").trigger('click');
        }
      }) 
        
    }
    
}

function filterData($type,$filterFor){
  
    $("#loading_screen_"+$filterFor).show();
    $("#CloseBubble").trigger("click");
    if($type == 'code'){
        createMapRequest('code',$filterFor);
    
    } else if($type == 'ncbatm'){
        createMapRequest('ncbatm',$filterFor);
    
    }
    else if($type == 'rbatm' && bankVersion=='3'){
        createMapRequest('rbatm_v3',$filterFor);

    }else if($type == 'bajatm'){
        createMapRequest('bajatm',$filterFor);

    }else if($type == 'bsfatm'){
        createMapRequest('bsfatm',$filterFor);
        
    }else if($type == 'shbatm_v3' ){
        createMapRequest('shbatm_v3',$filterFor);

    } else if($type == 'name'){
     
        createMapRequest('name',$filterFor);
         
    }else{
        createMapRequest('other',$filterFor);
    }
}


function initializeHeatMap($result,$resultType){
    if($resultType == 'atm')
        mapObject   =  mapObjectAtm;
    else
        mapObject   = mapObjectBranch;
    
    var heatmapData = [];
    for(var i in $result){
        if(Number($result[i]['weight'])){
            testData.location = new google.maps.LatLng($result[i]['latitude'],$result[i]['longitude']);
            testData.weight = Number($result[i]['weight']);
            heatmapData.push(testData);
            testData= {
                location:null,
                weight:null
            };
        }
    }
    mapObject.createHeatmap(heatmapData);
}

function switchMap(mapType,requestType){
    mapObject   =  getActiveMapTab();
    
    if(mapType == 'heat'){
        if(mapObject.heatmap)
            mapObject.heatmap.setMap(mapObject.map);
        else{
            getHeatMapDataset(requestType);
        }
          
        if(mapObject.cluster)
            mapObject.cluster.setMarkerMap(null);
        return "normal";
    }else{
	    if(mapObject.heatmap)
		mapObject.heatmap.setMap(null);
            else{
                getHeatMapDataset(requestType);
            }

            if(mapObject.cluster)
                mapObject.cluster.setMarkerMap(true);
            return "heat";
	}
}


function initializeHeatmapSelector($checkBoxType){
    $(document).ready(function(){
       if(isArabic){
          $offLabelIhone = getArabicText('Off');
          $onLabelIhone = getArabicText('On');
       } else {
          $offLabelIhone = 'Off';
          $onLabelIhone = 'On';
       }
       
        $('#toggle_map_'+$checkBoxType+':checkbox').iphoneStyle({
            uncheckedLabel: $offLabelIhone,
            checkedLabel: $onLabelIhone,
            onChange: function(){
              //$("#loading_screen_"+$checkBoxType).show();
              if($checkBoxType == 'atm'){
                mapType_atm = switchMap(mapType_atm,'atm');
              }else{
                mapType_branch = switchMap(mapType_branch,'branch');
              }
              //$("#loading_screen_"+$checkBoxType).hide();
            }
        });
    })
}

function initializeTinyscrollbar($type){
    $(document).ready(function(){
        $('#scrollbar_'+$type).tinyscrollbar({
                    sizethumb: 40
        });
    });
}

function getActiveMapTab(){
 $activeTab =   "";
 $("#myTab").find("li.active").each(function(){
        $activeTab = $(this).find("a").attr('href');
 });
 switch($activeTab){
    case "#Dashboard"    :   return mapObjectBranch;
    case "#AtmContainer" :   return mapObjectAtm;
    default              :   return mapObjectBranch;
 }  
}

function resizeActiveMap($resultType){
    if($resultType == 'atm'){
        if(typeof mapObjectAtm == 'object')
            mapObjectAtm.resizeMap();
    }else{
        if(typeof mapObjectBranch == 'object')
             mapObjectBranch.resizeMap();
    }
}


function getHeatMapDataset($requestType){
    $("#loading_screen_"+$requestType).show();
    $requestData = {
        "requestFor"    :   "heat-map-data",
        "group_name"    :   group_name,
        "map-filters"   :   {
                               "requestType"       :   $requestType 
                            }
    };
    $requestObject.initialize($requestData,"createHeatMap",null);
}

var mapObjectBranch = new MapCreator({
                    mapContainer    :   "map_section_branch",
                    type            :   "branch"
                });
var mapObjectAtm = new MapCreator({
                    mapContainer    :   "map_section_atm",
                    type            :   "atm"
                });
var mapObject = null;

function createMap(){
    
    if($("#loading_screen_atm").length){
        mapObjectAtm.createMap();
    }

    createMapRequest('other','atm');
   
}

$("#overview_branch").html($("#branch_loader").html());

 if($("#overview_atm").length){
    $("#overview_atm").html($("#atm_loader").html());
} 

createMap();                                //Execution starts Here
initializeHeatmapSelector('atm');