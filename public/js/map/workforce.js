
function loadWorkForceMap(result) {

    clusterOptions = {  styles: styles[0] };
    mapCounter = "";
    result = JSON.parse(result);
    wfmapdata = result['data'];
    mapCounter = result['counter'];

    $(".survey-count-total").css("display", "block");
    $("#work-force-counter").html(mapCounter);

    var locations = [];
     var markers=[];
    var infoContent = [];
    var infowindow = new InfoBubble({maxWidth: 400, maxHeight:500, borderColor: '#CCCCCC',disableAutoPan: false});

    for (var i = 0; i < wfmapdata.length; i++) {

        locations.push({
            lat: parseFloat(wfmapdata[i].lat),
            lng: parseFloat(wfmapdata[i].lng),
            device_name: (wfmapdata[i].device_name),
            merchandiser_name: (wfmapdata[i].merchandiser_name),
            region: (wfmapdata[i].region),
            outlet_code: (wfmapdata[i].outlet_code),
            outlet_location: (wfmapdata[i].outlet_location),
            outlet_type: (wfmapdata[i].outlet_type),
            latest_survey_date: (wfmapdata[i].latest_survey_date),
        });
    }

    var map = new google.maps.Map(document.getElementById('ughi_workforce_tracker_container'), {

        zoom: 5,
        center: new google.maps.LatLng(22.268763552489748, 47.87841796875),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        panControl: false,
        zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
        }
    });

   // var oms = new OverlappingMarkerSpiderfier(map,{markersWontMove: true, markersWontHide: true,keepSpiderfied:true});






    // Create an array of alphabetical characters used to label the markers.

    // Add some markers to the map.
    // Note: The code uses the JavaScript Array.prototype.map() method to
    // create an array of markers based on a given "locations" array.
    // The map() method here has nothing to do with the Google Maps API.

    $.each(locations, function(i, location) {
        var marker =  new google.maps.Marker({
            position: location,
            icon: baseUrl+'/public/img/logo/unicharm-logo.png'

        });


        infoContent[i] = "<div style='width: 100%;'><div style='border:10px solid white;background:white;padding:1px;border-radius:10px;height: 220px;'>\n\
                           <div><div style='top: 0px;position:relative;margin-left: 3px;height:210px' class='box span12'>\n\
                           <ul class='dashboard-list'><li class=''>\n\
                           <ul class='dashboard-sublist'>";
        infoContent[i] +="<li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Device Name</span></a><b style='color:#3E576F;font-size:14px;'>" + toTitleCase(location.device_name) + "</b></li>";

        infoContent[i]  += "<li><a href='#' title='merchendise_name'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Merchandiser Name</span><b style='color:#3E576F;font-size:14px;'>"+location.merchandiser_name+"  </b></a></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Region Name</span></a><b style='color:#3E576F;font-size:14px;'>" + toTitleCase(location.region) + "</b></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Outlet Code</span></a><b style='color:#3E576F;font-size:14px;'>"+location.outlet_code+"</b></li>\n\
                           <li><a href='#' title='outletLocation'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Outlet Location</span><b style='color:#3E576F;font-size:14px;'>"+location.outlet_location+"</b></a></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Outlet Type</span></a><b style='color:#3E576F;font-size:14px;'>"+location.outlet_type+"</b></li>\n\
                           <li><a href='#'><span style='width:42%;font-size:14px;text-align:left;color:#808080;'>Last Survey Date</span></a><b style='color:#3E576F;font-size:14px;'>"+location.latest_survey_date+"</b></li>\n\
                           </ul></li></ul></div><div style='height:165px;width:361px;'><div></div></div><div style='width:240px;line-height:10px;margin-top:10px;'></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div></div></div><div></div></div><div style='position: relative; left: 55px; width: 73%; margin-top:7px;'></div></div>";


        bindWFInfoWindow(marker, map, infowindow, infoContent[i]);
        markers.push(marker);
      //  oms.addMarker(marker);
    });

    // Add a marker clusterer to manage the markers.

    var markerCluster = new MarkerClusterer(map, markers,clusterOptions
    //    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    //    {imagePath: baseUrl+'/public/img/cluster/unicharm/'}
    );

   // oms.addListener('spiderfy', function(markers) {

    //});

}

function bindWFInfoWindow(marker, map, infowindow, html) {

    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(map, marker);
    });
}