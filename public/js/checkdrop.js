(function($){ 
    $.fn.checkdrop = function(options) { 
        options = $.extend({}, $.fn.checkdrop.defaultOptions, options);
        var me = this;
        var element;
        var mainTime;
        var selectedElements = [];
        var elementId ;
        this.init = function(){
            elementId = $(me).attr("id");
            if(elementId == 'undefined' || !elementId)
                return;
            //alert(elementId);    
            var mainElement = document.createElement('DIV');
            mainElement.style['width'] = '140px'; 
            mainElement.style['cursor'] = 'pointer'; 
            $(mainElement).bind( "mouseover", function( event ){ 
                me.setTimer(0);
            });
            $(mainElement).bind( "mouseout", function( event ){ 
                me.setTimer(3600);
            });
            var subElement = document.createElement('DIV');
            subElement.id = "checkboxValues_"+$(me).attr("id");
            subElement.style['width'] = 'auto'; 
            subElement.style['min-height'] = '20px'; 
            subElement.style['height'] = 'auto'; 
            subElement.style['cursor'] = 'pointer'; 
            subElement.style['display'] = 'none'; 
            subElement.style['zIndex'] = '99999'; 
            subElement.style['position'] = 'absolute'; 
            subElement.style['margin-top'] = '-10px'; 
            subElement.style['border'] = '1px solid black'; 
            subElement.style['background'] = 'white';
            var subList = document.createElement('ul');
            $(subList).attr('class',"dashboard-sublist");
            subList.style['width'] = '140px';
               
             
            this.each(function() {
                element = $(this);
                element.find('option').each(function(){
                    var optionId = this;
                    $(this).css('display','none');
                    if($(this).attr('value') && ($(this).attr('value') != 'Multiple selection')){
                        var subListElement = document.createElement('li');
                        var checkbox = document.createElement('input');
                        checkbox.id = $(this).attr('value')+"_checkbox";
                        checkbox.type = "checkbox";
                        checkbox.value = $(this).attr('value');
                        subListElement.appendChild(checkbox);
                        var spanElement = document.createElement('span');
                        spanElement.innerHTML =  $(this).text();
                        subListElement.appendChild(spanElement);
                       
                        $(subListElement).bind( "click", function( event ){ 
                            me.selectedValue($(optionId).attr('value'),$(optionId).attr('value')+"_checkbox",subElement.id);
                        });
                        subList.appendChild(subListElement);
                    }
                })
            });        
            subElement.appendChild(subList);
            mainElement.appendChild(subElement);
            element.parent().append(mainElement);
            $(this).bind( "click", function( event ){ 
                me.showHideDropDown();
            });
            return this;
        }
        
        
        this.showHideDropDown = function($type){
            
            if(!$type){
                if($("#checkboxValues_"+$(this).attr("id")).css('display') == 'none'){
                    $("#checkboxValues_"+$(this).attr("id")).css('display','block');
                }else{
                    $("#checkboxValues_"+$(this).attr("id")).css('display','block');
                     
                }
            }
            if($type == 'hide'){
                $("#checkboxValues_"+$(this).attr("id")).css('display','none');
            }
        };
        
        
        this.checkCheckboxStatus = function($type){
            $counter = 0;
            selectedElements = [];
            $("#checkboxValues_"+$(me).attr("id")).find('input').each(function(){
                if($(this).is((":checked"))){
                    selectedElements.push($(this).attr('value'));
                    if($type == 'counter')
                        $counter++;
                    else{
                        $counter = $(this).attr('value');
                       
                    }
                }
            })
            
            return $counter;
           
        };
        
        
        this.selectedValue = function(value,parentId,divId){
            selectedElements = [];
            if(parentId){
                if($("#"+parentId).is(":checked")){
                    if(me.checkCheckboxStatus('counter')>1)
                        $(this).attr('value','Multiple selection');
                    else{
                           
                        $(this).attr('value',value);
                    }  
                    
                  if(bankFolder=='ncbatm'){
                      ncbATMV5CheckDropClickEvent(value,divId,'pop');  
                   } 
                        
                }else{
                    $totalComment = me.checkCheckboxStatus('counter');
                        
                    if($totalComment){
                        if($totalComment == 1)
                            $(this).attr('value',me.checkCheckboxStatus('value'));
                        else
                            $(this).attr('value','Multiple selection');
                    }else{
                        $(this).attr('value','');
                        selectedElements = [];
                    }
                    
                    if(bankFolder=='ncbatm'){
                        ncbATMV5CheckDropClickEvent(value,divId,'pop');  
                    }  
                }
            }
               
            
        };
        this.setTimer = function(currentTime){
            mainTime = currentTime;
            if(mainTime){
                setTimeout(function(){
                    if(mainTime){
                        me.showHideDropDown('hide');
                    }
                },currentTime);
            }
        };
            
        this.uncheckAll = function(){
            selectedElements.length = 0;
            selectedElements = [];
            $("#checkboxValues_"+$(me).attr("id")).find('input').each(function(){
                        
                if($(this).is((":checked"))){
                    $(this).trigger('click');
                    $(this).trigger('click');
                }
                $(this).removeAttr('checked');
                $(this).removeAttr('checked');
                        
            });
            $(this).attr('value','');
        };
            
        this.getValue = function(){
            selectedElements.length = 0;
            selectedElements = [];
            $("#checkboxValues_"+$(me).attr("id")).find('input').each(function(){
                if($(this).is((":checked"))){
                    selectedElements.push($(this).attr('value'));
                }
            });
            if(selectedElements)
                return selectedElements;
            else
                return null;
        };
        return this.init();
    }
    $.fn.checkdrop.defaultOptions = {
        myclass: 'checkdrop'
    } 
})(jQuery);