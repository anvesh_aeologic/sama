var activity = new Array('Food retailer','Medical provider','Non-Food retailer','Automotive','Electronics','Service provider','Restaurant','Hospitality','Home improvement','Luxury','Fashion','Travel','Education','Other');
var subactivity = [
    ['SUPERMARKETS','MIS. FOOD STORES','BAKERIES','CONFECTIONERY'],
    ['HOSPITALS','MEDICAL SERVICES','PHARMACIES'],
    ['GIFT CARDS','OFFICE STATIONARY','SPORTING GOODS STORES'],
    ['AUTOMOBILE','CAR RENTAL and LEASING','MOTORCYCLES'],
    ['RADIO and TV','COMPUTER','ELECTRICITY'],
    ['MALE TAILORING','BANKING BRANCHES','BEAUTY'],
    ['EATING PLACES'],
    ['HILTON INTERNATIONAL','HOTEL','RADISSON SAS','MERIDIEN'],
    ['BUILDING MATERIAL','CARPET AND RUGS','HOUSEHOLD','HARDWARE','GLASS'],
    ['COSMATICS','JEWELLRY','SHOE'],
    ['CHILDREN CLOTHING','FAMILY CLOTHING','FEMALE CLOTHING','MALE CLOTHING'],
    ['TRAVEL AGENCY'],
    ['SCHOOL'],
    ['GENERAL MERCHANDISE','OTHER UNCLASSIFIDE SERVICES']
];
                    
Number.prototype.formatMoney = function(c, d, t){
var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 
var bankCode,bankSurveyDate,bankLatitude,bankLongitude,globalWhere="";
        function calculateChar(mainStr){
            if((String(mainStr).split("")).length==1){
                return '&nbsp;&nbsp;&nbsp;&nbsp;';
            }else{
                if((String(mainStr).split("")).length==2)
                    return '&nbsp;&nbsp;';
                else
                    return '&nbsp;';
            }
        } 
        
       var totalResults = -1; 
       var totalRent = 0;  
       var totalRevenue = 0;
       var totalCardshare = 0;  
       function getBankBranches(where){
           var url = "https://www.google.com/fusiontables/api/query?sql=SELECT+RETAILER_NAME,TERMINAL_ID,TOTAL_RENT,TERM_REVENUE,AMEX_AMT_YTD,MC_AMT_YTD,VISA_AMT_YTD,SPAN_AMT_YTD,DM_AMT_YTD+FROM+1ePhJAoRhQOqyAk4WvX4B7-CC-CYUYlE_zrw8Cj8+";
           $mywhere = null;
           if (where != null && where != undefined && where != ""){ 
                url += "WHERE+" + where+"+";
                $mywhere = " WHERE "+where+" ";
           }
           url += "ORDER+BY+RETAILER_NAME+ASC+&jsonCallback=?";
           
           fusionTable("SELECT RETAILER_NAME,TERMINAL_ID,TOTAL_RENT,TERM_REVENUE,AMEX_AMT_YTD,MC_AMT_YTD,VISA_AMT_YTD,SPAN_AMT_YTD,DM_AMT_YTD FROM ",$mywhere,' ORDER BY RETAILER_NAME ASC',"createMerchantChart",'1ePhJAoRhQOqyAk4WvX4B7-CC-CYUYlE_zrw8Cj8',null);
           
       }
       
       function isNumber (o) {
          return ! isNaN (o-0) && o != null;
        }
       
       function createMerchantChart(data){
        var addressMerchant;
        var searchRent = 0;  
        var searchRevenue = 0;
        var searchCardshare = [0,0,0,0,0];  
          
        var fusionRows = data.rows;
                $content = '<ul class="dashboard-sublist">';
				var counter = 0;
                for(i in fusionRows){
                    if(fusionRows[i][0]){
                            addressMerchant = fusionRows[i][0];
                            if(addressMerchant.length>30){
                                addressMerchant = addressMerchant.substring(0,30);
                                addressMerchant = addressMerchant+"..."
                            }
                            
                            if(totalResults == -1){
                                if(isNumber(fusionRows[i][2]))
                                    totalRent += Number(fusionRows[i][2]);
                                if(isNumber(fusionRows[i][3]))
                                    totalRevenue += Number(fusionRows[i][3]);
                                
                            }
                            
                            if(isNumber(fusionRows[i][2]))
                                searchRent += Number(fusionRows[i][2]);
                            
                            
                            if(isNumber(fusionRows[i][3]))
                                searchRevenue += Number(fusionRows[i][3]);
                            
                            searchCardshare[0] += Number(fusionRows[i][4]);
                            searchCardshare[1] +=  Number(fusionRows[i][5]);
                            searchCardshare[2] +=  Number(fusionRows[i][6]);
                            searchCardshare[3] +=  Number(fusionRows[i][7]);
                            searchCardshare[4] +=  Number(fusionRows[i][8]);
                            $content += '<li id="bankCode_'+fusionRows[i][3]+'" style="height:25px;"><a href="home.php?terminalId='+fusionRows[i][1]+'"><span style="width:65%;text-align:left;font-size:12px; text-transform: lowercase;margin-left:10px;" class="blue">'+addressMerchant+'</span></a>'+
                                       '<i>'+fusionRows[i][1]+'</i>'+    
                                       '</li>';
                       counter++; 
                    } 
                    
                }
              $content += '</ul>';
			  
              $("#totalResultCounter").html(counter);
              $("#overview").html($content);
              
              
            if(totalResults==-1){
                 totalResults = counter;
            }
            
            
            $data = {
                    'name':'Share of rent money',
                    'result':toFixed((searchRent),2),
                    'total':toFixed((totalRent-searchRent),2),
                    'container':'shareofRentMoneyReport',
                    'title':'Share of rent money'
            };
            
            createOtherChart($data);
            redrawLegend();
            $data = {
                'name':'Share of revenue',
                'result':toFixed((searchRevenue),2),
                'total':toFixed((totalRevenue-searchRevenue),2),
                'container':'shareOfRevenueReport',
                'title':'Share of revenue '
            };
            createOtherChart($data);
            redrawLegend();
            $chartObject = {
                  container : 'shareOfCardsReport',
                  title : 'Share of cards',  
                  categories : ['Amex','Master Card','Visa','Span','DM'],  
                  series : [{
                            showInLegend:false,
                            name: 'Card Share',
                            data: [{y:toFixed(searchCardshare[0],2), color: '#D03A19'},{y:toFixed(searchCardshare[1],2), color: '#7AA300'},{y:toFixed(searchCardshare[2],2), color: '#E8B410'},{y:toFixed(searchCardshare[3],2), color: '#81A90A'},{y:toFixed(searchCardshare[4],2), color: '#E4DAE2'}]
                
                        }]
            }
            cardSharingBar($chartObject);
            $("#loader").hide();
            
       }
       
       function toFixed(num, fixed) {
            fixed = fixed || 0;
            fixed = Math.pow(10, fixed);
            return Math.floor(num * fixed) / fixed;
        }
/************************************************************************************************************************************************************/       
       function getSelectList(containerId)
        {  
          var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
          var where = '';
          var province = '';
          if(containerId == 'selectCity'){
              province = $("#selectRegion").attr('value');
              where = {"province":province,"cityList":true,"browser":is_chrome}
              filterType = 'selectRegion';
           } else {
              where = {"provinceList":true,"browser":is_chrome};
              filterType = 'selectRegion';
           }
        
           $('#'+containerId).html('');
           var selectName = '';
           if(containerId == 'selectRegion'){
               selectName = 'Region';
           }else{
               selectName = 'City';
           }
           
          
           var select = '<option value="">--'+selectName+'--</option>';
           $('#'+containerId).append(select);
           if(is_chrome){
              var url = bankAdminBaseUrl+"/aerial/readDb.php?result=data&browser=true";
              if(containerId == 'selectCity'){
                 province = $("#selectRegion").attr('value');
                 url +="&province="+province+"&cityList=true";
                 filterType = 'selectRegion';
              } else {
                 url +="&provinceList=true";
                 filterType = 'selectRegion';
              }
              var xhr = new XMLHttpRequest();
              xhr.open("get",url, true);
              xhr.onload = function(data){  //instead of onreadystatechange
                 $('#'+containerId).append(xhr.responseText);
                 if(containerId == 'selectCity'){
                    var where = '';
                    if($("#selectRegion option:selected").attr('value'))
                       where = "Province = '"+$("#selectRegion option:selected").text()+"'";
                    if(where)
                       if(globalWhere)
                          getAllData(globalWhere+" AND "+where);
                    else
                       getAllData(where);
                    else
                       getAllData(globalWhere);
                 }
              };
        
              xhr.send();
           }else {
              $.ajax({
                 url : bankAdminBaseUrl+"/aerial/readDb.php",
                 type: "GET", 
                 contentType: "application/json",
                 dataType: 'jsonp',
                 jsonp: 'result',
                 data: where,
                 success: function (data) {  
                   
                    if(data.list){
                       if(is_chrome)
                          $('#'+containerId).append(decodeURIComponent(data.list));
                       else
                          $('#'+containerId).append(data.list);
                    }
        
                    if(containerId == 'selectCity'){
                       var where = '';
                       if($("#selectRegion option:selected").attr('value'))
                          where = "Province = '"+$("#selectRegion option:selected").text()+"'";
                       if(where)
                          if(globalWhere)
                             getAllData(globalWhere+" AND "+where);
                       else
                          getAllData(where);
                       else
                          getAllData(globalWhere);
        
                    }
                 },
                 error:function(        req, error      ){
                 }
              }); 
           }
        }
        
        function getAllData(where){
           _global.inventory.WHERE = where;
           
           //initialize();
        }
        
        function searchMerchants(){
            _global.inventory.WHERE = "";
            $condition = "";
            $("#totalResultCounter").html('<img title="Loading.." src="../img/ajax-loaders/ajax-loader-1.gif"/>');
            $("#searchingSpan").find('select').each(function(){
                if(($(this).attr('value'))&&($(this).attr('searchType'))){
                    if($condition){
                        $condition += "+AND+";
                    }
                    switch($(this).attr('searchType')){
                        case "equal":
                            $condition += $(this).attr('fieldName')+"+=+'"+($(this).attr('value')).trim()+"'";
                            break;
                        case "like":
                            $condition += $(this).attr('fieldName')+"+CONTAINS+'"+$(this).attr('value').trim()+"'";
                            break;
                        case "find":
                            $condition += $(this).attr('fieldName')+"+IN+"+makeCondition($(this).attr('value').trim());
                            break;
                        case "check":
                            $condition += $(this).attr('value')+"+=+'X'";
                            break;
                        case "text":
                            $condition += $(this).attr('fieldName')+"+IN+('"+($("#selectRegion option:selected").text()).trim()+"')";
                            break;
                    }
                }
            });
            _global.inventory.WHERE = $condition;
            $("#loading_screen").show();
            $("#overview").html($("#loaderScreen").html());
            console.log(_global.inventory.WHERE);
            mapInitialize();
        }

        function makeCondition(value){
            $condition = "";
            for(var i in activity){
                if(activity[i] == value){
                    $condition = "("
                    for(var j in subactivity[i]){
                        if(j>0){
                            $condition += ",";
                        }
                        $condition += "'"+subactivity[i][j]+"'";
                    }
                    $condition += ")";
                    break;
                }
            }
            return $condition;
        }
        
String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};

String.prototype.ltrim=function(){return this.replace(/^\s+/,'');};

String.prototype.rtrim=function(){return this.replace(/\s+$/,'');};

String.prototype.fulltrim=function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};



function cardSharingBar($chartObject){
    
       var chart = new Highcharts.Chart({
            chart: {
                renderTo: $chartObject.container,
                type: 'column'
            },
            title: {
                text: $chartObject.title,
                x: -30,
                y: 15
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: $chartObject.categories
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Card Sharing'
                }
            },
            legend: {
                showInLegend:false
            },
            
            tooltip: {
                formatter: function() {
                    return ''+
                        this.x +': '+ Number(this.y).formatMoney(2, '.', ',') +'';
                }
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            credits : {
                enabled : false
            },
           series: $chartObject.series
        });


} 

function resetMerchants(){
    _global.inventory.WHERE = null;
            
            $("#totalResultCounter").html('<img title="Loading.." src="../img/ajax-loaders/ajax-loader-1.gif"/>');
            $("#searchingSpan").find('select').each(function(){
                
                if($(this).attr('selectType')=='searchBox'){
                    $counter = 0;
                    $(this).parent().find('button').each(function(){
                        if($counter == 1){
                            $(this).trigger('click');
                        }
                        $counter++;
                    });
                    $(this).attr('value','');
                }else{
                    $(this).attr('value','');
                }
            });
           
            $("#loading_screen").show();
            $("#overview").html($("#loaderScreen").html());
           
            mapInitialize();
}

